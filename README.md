## Description

[Newton-X](http://newtonx.org/) is a general-purpose program package for simulating the dynamics of
electronically excited molecules and molecular assemblies.

This repository contains everything needed to propagate a dynamics with the trajectory 
surface-hopping method, with energies and gradients available through the following models:

- Analytic models: spin-boson Hamiltonian, recoherence model, 1-dimensional models
- Columbus 7: MRCI, MCSCF
- Gaussian 16: TD-DFT
- Turbomole 7.3: TD-DFT, ADC(2), CC2

## Contact

The preferred way to get help is through our mailling
list `newtonx [at] freelists.org`. You can subscribe to this list by
sending email to `newtonx-request [at] freelists.org` with 'subscribe' in the
Subject field OR by visiting the list page at <https://www.freelists.org/list/newtonx>.

For bug reports, please fill an issue here on Gitlab, or contact us through the mailling list.
