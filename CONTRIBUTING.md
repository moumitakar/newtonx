title: Contributing to Newton-X

# How to contribute to Newton-X

## Resources

The relevant resources for coding in Newton-X are are in the FORD-generated documentation, in 
particular in the `README` section.  There you will find information about preferred style, guidelines,
and how to implement a new interface in particular.

When in doubt, please refer to the existing codebase, and feel free to ask questions on our 
mailling-list !

## Git

The preferred way of contributing to Newton-X is through branching from the `master` branch.

It is best to first discuss with the maintainers on how to implement
the features you want. Preferrably, this should be done by [opening an
issue](https://gitlab.com/light-and-molecules/newtonx/-/issues) on the
project Gitlab.  There we can discuss about how to proceed.

0. Fork the project from the Gitlab page, to have a version on your own account.
   
1. Clone the repository on your local machine:
   
        git clone https://gitlab.com/your-gitlab-account/newtonx.git
		
2. To keep the code up to date with the latest changes on the `master`
   branch, you need to track the upstream repository.  This is done by adding a remote branch
   pointing to the upstream project:
   
        git remote add upstream https://gitlab.com/light-and-molecules/newtonx.git

2. Create a branch and start working there. The name of the branch should be self-documenting (with
   either your name or the feature you are implementing).
   
        git checkout -b my-new-feature
  
3. Open a merge request on the Gitlab project, and prefix the title with either `WIP` or `Draft` to
   indicate a work in progress. Possibly you can link in the description the number of the
   corresponding issue (for issue number 54 for instance, type `#54` in the description).
   
4. Set an upstream for your local branch
   
        git pull --set-upstream origin my-new-feature
   
5. You can start to make changes in your branch, commit them, and push them
   
        git add ...
        git commit ...
        git push ...

6. Update the merge request when your work is ready to be merged.
   
### Keep in mind

Do not forget to update regularly your branch with the latest changes from `master` to ease the
merge !

```bash
git pull --rebase upstream master
```

### Git hygiene

Please try as much as possible to have individual commits that don't break the program. If that
happens, you can always use `git rebase` to group some commits together.

Also, please try to have insightful commit messages.  The best way to ensure that is to not use the
`-m` flag to `git commit`, and instead use your editor to format the commits with a meaningful
title, followed by a description, like:

```
This is my first commit.

Here I have modified feature A so that function foo is not called anymore.
```

