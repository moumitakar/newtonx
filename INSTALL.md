# Installation

## Pre-requisites

### Obtaining the code

The code can be obtained by downloading the selected release in the "Release" section, or
by cloning the this repository. In this case, please make sure to do a recursive cloning:

    git clone https://gitlab.com/light-and-molecules/newtonx
	
Please note that the released tarballs do not contain the sources for compiling the `cioverlap`
programs. Those are available as separate downloads on the [release page](https://gitlab.com/light-and-molecules/newtonx/-/releases).

### Compiler and libraries

-   Newton-X is built using `autotools` and `make`.
-   A reasonably recent version (version 10 or higher) of `gfortran` is required to compile the code.
-   BLAS and Lapack are also needed.
-   **Optional**:  [HDF5 libraries](https://portal.hdfgroup.org/display/support/Downloads)

### External programs (time-derivatives)

- The derivative couplings can be obtained through the state overlap matrix if they can't
  be derived from non-adiabatic coupling vectors (for instance in TD-DFT computations).
  We provide a set of static binaries in the [release page](https://gitlab.com/light-and-molecules/newtonx/-/releases).

- Newton-X will look for these program in a directory aliased by the `CIOVERLAP` environment:
  
        export CIOVERLAP=/path/to/cioverlap/binaries/

- The `cioverlap.od` binary provided is compiled without parallelization.  The source code for this program
  can be obtain through request at `baptiste.demoulin [at] univ-amu.fr`.
	
### Perl (only for the input generator)

- Perl is only used for the input generator `nxinp`.

- Newton-X requires at least Perl 5.20.

- The following Perl libraries are necessary for Newton-X to work properly:
  - `YAML::Tiny`
  - `Math::VectorReal`
  - `File::Path`
  - `File::Copy::Recursive`
  - `Getopt::Long`
  - `Sys::Hostname`

- A convenient way to install Perl libraries is to use  [`cpanminus`](https://metacpan.org/pod/App::cpanminus) (packaged in most Linux
  distribution). If you do not have `sudo` permission on your system, you can download the executable
  with:
    
        cd ~/bin
        curl -L https://cpanmin.us/ -o cpanm
        chmod +x cpanm
    
  To install `YAML::Tiny` for instance, you can execute the following command:
    
        cpanm YAML::Tiny
    
  The libraries will be installed under `~/perl5/lib/perl5/`. For Perl to find these libraries, add
  the following line to `.bashrc`:
    
        export PERL5LIB=$HOME/perl5/lib/perl5
    
  or to `.cshrc`:
    
        setenv PERL5LIB $HOME/perl5/lib/perl5


## Building Newton-X

- In this part we assume that Newton-X will be installed in `$HOME/softs/newtonx/`, with the
  source code in `/path/to/nx/source/`.

- Newton-X relies on the `NXHOME` environment variable for running. `NXHOME` should be set
  to the path where it is installed, so in our example:
    
        export =NXHOME=$HOME/softs/newtonx/=

- We recommend to build Newton-X out-of-tree:
    
        cd /path/to/nx/source/
		./autogen.sh
        mkdir build && cd build
        ../configure --prefix=$NXHOME
        make && make install

- A complete list of options for the `configure` script is available with `../configure --help`.

- The programs `nx_moldyn`, `nx_test` and `nx_restart` will be found in `$(prefix)/bin`, while the 
  other data (mainly `examples`, `data` and `perllib`) will be found in `$(prefix)/share/newtonx/`.


### Testing

- A testsuite is available and can be run after build with `make check`.

- By default, only the tests for the analytical models will be done.  Others will simply be
  skipped (please refer to the manual for information about setting up other interfaces).

- All tests will run under `tests/testdir` in the build directory.

- The main logfile is `test-suite.log`, and the individual tests are logged in `tests/test-interface.log`,
  where `interface` is replaced by the name of the interface.

- In case of failure, you can inspect the content of the dynamics logfile `md.out` in each individual
  test folder.
  
- Individual tests can also be performed with the `nx_test` program. To see how to use the program please
  refer to the help provided:
  
        nx_test --help


## HDF5 integration

- Newton-X can be built with support for HDF5 for producing its main output. You can enable this
  feature by setting `--with-hdf5` in the `configure` step.

- HDF5 libraries and compiler wrapper (either `h5fc` or `h5pfc`) will be required. They will be
  searched under `$HDF5` and under `$PATH`.  Please note that Newton-X will not use the parallel
  versions of HDF5.

- The libraries can be either installed via the package manager or manually compiled. If you choose
  to install them with the package manager, please make sure to use the same `gfortran` compiler to
  compile Newton-X after, as `mod` files in Fortran are compiler specific !


### Ubuntu

- On Ubuntu you need find the `hdf5-tools` and `libhdf5-dev` packages:
    
        sudo apt-get install hdf5-tools libhdf5-dev
    
  The executables will then be found in `/usr/bin/`, so you can set your environement:
    
        export HDF5=/usr/


### Manual installation

- Newton-X will not make use of the parallel version of HDF5. So you just need to compile the serial
  version (you can use the parallel version as well of course).
    
        export HDF5=/path/to/install/directory
        ./configure --enable-fortran --prefix=$HDF5
        make && make install


## Documentation

A manual is available in the `manual` folder. It contains a tutorial as well as a documentation
for the different interfaces and methods available.

The documentation is generated by [FORD](https://github.com/Fortran-FOSS-Programmers/ford), which can be installed using `pip`, and requires at
least `python 3.7`:

    pip install ford

If you are in the build directory, building the documentation is done with:

    ford -o ../doc ../doc.md

You can then open `../doc/index.html` with a web browser.
