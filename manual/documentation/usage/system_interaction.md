title: Usage of the `mod_interfaces` module

# Introduction

The module `mod_interfaces` defines several subroutine and functions that either interface with
Unix C-functions (`mkdir`, `setenv`), or that call coreutils programs (`cp`, `rm`), to simplify
their usage in the main code. 

This document describes some examples of how to use the different functions. For more information,
please refer to the documentation of the `mod_interfaces` module, or to the implementation of 
the individual functions.

# Dealing with files

## Reading a directory

Reading the content of a directory is done with the `get_list_of_files` subroutine, that will
open a directory, list all elements therein, and populate an array with the name of each element.  It 
is also possible to exclude elements based on the name, and the `.` and `..` elements won't be 
considered (current and parent directory respectively).  It is also possible to append the name
of the original directory to the elements of the resulting array (for instance, to copy those files
around).

Considering the following folder:

```bash
$ ls examples/
  file1  file2  file3  dir1/  dir2/  dummy
```

This directory can be read in the following ways:

```fortran
character(len=256), allocatable :: filelist(:)
integer :: ierr, i
character(len=256), allocatable :: exclude(:)

! Basic usage: read the directory content
call get_list_of_files('./examples', filelist, ierr)
if (ierr == 0) then
  print *, 'Basic usage'
  do i=1, size(filelist)
    print *, trim(filelist(i))
  end do
  print *, ''
else
  print *, 'ERROR: Cannot read directory'
end if

! Append the directory name to the content
call get_list_of_files('./examples', filelist1, ierr, full_path=.true.)
if (ierr == 0) then
  print *, 'Append directory name'
  do i=1, size(filelist)
    print *, trim(filelist(i))
  end do
  print *, ''
else
  print *, 'ERROR: Cannot read directory'
end if

! Exclude some files
allocate( exclude(2) )
exclude(1) = 'file1'
exclude(2) = 'dir1'
call get_list_of_files('./examples', filelist1, ierr, exclude=exclude)
if (ierr == 0) then
  print *, 'Exclude file1 and dir1'
  do i=1, size(filelist)
    print *, trim(filelist(i))
  end do
  print *, ''
else
  print *, 'ERROR: Cannot read directory'
end if

! Exclusion is based on patterns
exclude(1) = 'file'
exclude(2) = 'dir'
call get_list_of_files('./examples', filelist1, ierr, exclude=exclude)
if (ierr == 0) then
  print *, 'Exclude file and dir'
  do i=1, size(filelist)
    print *, trim(filelist(i))
  end do
  print *, ''
else
  print *, 'ERROR: Cannot read directory'
end if
```

The code should produce the following output:

```bash
    Basic usage
	file1
	file2
	file3
	dir1
	dir2
	dummy
	
	Append directory name
	example/file1
	example/file2
	example/file3
	example/dir1
	example/dir2
	example/dummy
	
	Exclude file1 and dir1
	file2
	file3
	dir2
	dummy
	
	Exclude file and dir
	dummy
```

Under the hood, the subroutine first calls an interface to the `opendir` C-function to get a stream, then
repeateadly calls the C function `readdir` on this stream until all the content is read.  A temporary list 
is populated (a maximum of 4000 files and directories can be listed).

When all the stream has been read, the final array is allocated with a size corresponding to the total number 
of elements actually wanted (taking into account the elements from `exclude` if the argument is present), 
and the `closedir` C-function is called to close the stream.

## Copying files

Copying files is handled through the `copy` function interface.  Several possibilities are provided by this
interface:

- Copy one file into another:
```fortran
integer :: ierr
ierr = copy('origin', 'target')
```
is equivalent to:
```bash
$ cp -f origin target
```

- Copy a list of files to a directory:
```fortran
integer :: ierr

ierr = copy(['file1', 'file2', 'dir1/ '], 'target')
```
is equivalent to:
```bash
$ cp -f -r file1 file2 dir1 target
```

- Copy a list of files with different target names:
```fortran
integer :: ierr

ierr = copy(['file1', 'file2', 'dir1/ '], &
            ['target1', `target2', 'dir2   '])
```
is equivalent to:
```bash
$ cp -f file1 target1
$ cp -f file2 target2
$ cp -f -r dir1 dir2
```

The returned value `ierr` will be 0 on succesful termination, and different from 0 otherwise (code returned by
the last `cp` command).  It is a good practice to examine this value with the `check_error` subroutine 
afterwards, with the `system` flag set to `.true.`:
```fortran
integer :: ierr

ierr = copy(['file1', 'file2', 'dir1/ '], 'target')
call check_error(ierr, 20, 'Cannot copy files', system=.true.)
```

The `system` flag will print additional information as returned by the `errno` and `perror` functions.

