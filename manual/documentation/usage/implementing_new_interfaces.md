title: Implementation of new interfaces

In this page we give some indications about implementing new interfaces in Newton-X.  We
assume that all routines specific to the new interface will be prefaced by `prog_` (for 
instance, all Columbus routines are prefixed with `col_`). 

These routines should go to the module `mod_prog.f90`.

# Useful types defined in Newton-X

## `nx_qm_t` object

This object is defined in the module `mod_qm_t`, and handles the parameters for running
the energy and gradient computation.  All parameters for a new interface belong to this
object.  An initial value can be given to scalar data at declaration, while all 
allocatable data (or values for which no initial value can be provided) have to be 
initialized in the routine `init`.  A good practice is to allocate all arrays with 
dimension 1, and resize them as needed later, with a default value of `0` for instance.

If data has been allocated in the `init` routine, then it must be deallocated in the 
`destroy` routine.

The `nx_qm_t` object is aimed at allowing communication between the QM computation, the
double molecule computation for overlap evaluation, and the `cioverlap` part if required.

## `nx_orb_t` object

The `nx_qm_t` type has a `nx_orb_t` component.  This component is used to store information
related to the orbitals in the computation, and is used mostly to create inputs for the 
`cioverlap` programs.

# Routines

## Required routines

The only routines that are strictly required by Newton-X are the following:

1. `prog_setup`: Read the set of parameters from the main input (default `user_config.nml`)
   into a `nx_qm_t` object.  This routine will only be called once, at the initialization
   step.  This routine should, for instance, allocate arrays and read from a parameter
   file (through the `parser_t` object usually).
   
2. `prog_update`: Update (or write) the QM input, from the information contained in the 
   `nx_qm_t` object, the current state of the trajectory `nx_traj_t`, the main configuration
   `nx_config_t`, and the user-provided QM inputs (from the directories `JOB_AD/` or 
   `JOB_NAD`.  This routine will be called at every step.
   
3. `prog_run`: Run the QM program, through calling the routine `call_external`.

4. `prog_read`: Read the outputs from the QM computation for energies, gradients, ...
   This routine should return at least the arrays `repot` and `rgrad`.  Optionnally, 
   it can return also `rnad` (non adiabatic couplings).  Oscillator strengths will be 
   read directly into `nx_qm_t` (member `osc_str`).  For other data from the QM 
   computation (usually wavefunction components information), the array `dataread` can
   be used to store the relevant lines from the output.  In that case, the integer 
   `dim_dataread` should be incremented for **every** line that is stored.  Newton-X
   will automatically transfer the arrays to the main `nx_traj_t` object, and print
   to `STDOUT` all information from `dataread`.  If needed, phase correction is also
   handled automatically.  In summary: **the interface routine should just parse their
   output, and populate the `repot` and `rgrad` (and possibly `nx_qm_t%osc_str`, `rnad`
   and `dataread` arrays**.
   
5. `prog_backup`: Copy some relevant data from the QM computation (inputs, starting MO
   if relevant for instance).  This routine is called after `cioverlap` (if required)
   has been called, and before the surface hopping routines.
   
6. (Optional) `prog_print`: This routine is called during initialization to give a
   feedback of the parameters that are used by Newton-X.
   
Once implemented, these routines have to be called from the corresponding routines in
`mod_qm_general`, with a `select case` statement over the `qmcode` member of the `nx_qm_t`
object.

## Overlap specific routines

Newton-X will often need to perform a double-molecule computation, for checking the overlap
between two time steps, or to use `cioverlap` for obtaining time-derivatives.  In that case,
the following routines should also be defined:

1. `prog_init_double_molecule`: Create a general input for a computation that just needs
   to print AO integrals, usually in the folder `double_molecule_input`. This routine will
   be called only once in the whole dynamics.  This routine will be called from the 
   module `mod_overlap` (routine `ovl_prepare_run`).

2. `prog_prepare_double_molecule`: Prepare an input for the overlapping molecule (superposition
   of the molecule).  The coordinates for the double system will be printed by the routine
   `write_merged_coordinates` from the `nx_traj_t` object. This routine will be called from the 
   module `mod_overlap` (routine `ovl_run`).
   
3. `prog_extract_overlap`: From the outputs of the double molecule, extract the AO overlap
   matrix. This routine will be called from the module `mod_overlap` (routine 
   `ovl_extract_ao_ovl_and_lcao`).
   
4. `prog_get_lcao`: Get the AO to MO transformation matrix, in `(AO,
   MO)` format. This routine will be called from the module
   `mod_overlap` (routine `ovl_extract_ao_ovl_and_lcao`).

The program to run the double molecule computation is specified by the `ovl_cmd` member of
`nx_qm_t`.  Optionnally, a `ovl_post` member also exists for post-treatment of the double 
molecule output (for instance, Gaussian needs to call `rwfdump`).

## `cis_casida` routines
   
Eventually, some methods will require generating a CIS-like wavefunction to be used with `cioverlap`
to generate time-derivatives (for instance, methods that do not have analytic non-adiabatic 
couplings).  In that case, we need the following routines:

1. `prog_get_singles`: Parse the relevant output file to extract the single excitation 
   amplitudes.
   
2. `prog_get_mos_energies`: Parse the relevant output file to extract the MO energies.

These routines will be called from the module `mod_cioverlap` in routine `cio_get_singles_and_mos`.
If some preparation is needed before singles or MOs can be parsed, some routines can
also be called from `cio_prepare_files`.

