title: Coding style

# General style

- Try to keep the lines at 90 characters when possible, and not longer than 132
  characters.
  
- The line continuation symbol `&`, if used, should be indented as the rest of the code:
        
		write(*, '(A)') &
		    & 'This is a message !!'

- Before committing changes, always clear unnecessary whitespaces !

# Naming conventions

- Modules should be written in a file with name beginning with `mod_`.

- Source files must contain at most one program, module or submodule.

- It is better to reduce the numer of types defined in source files (3 at most
  should be a good rule of thumb).
  
- All types defined should begin with `nx_` and end with `_t` to ensure that the
  names will not conflict with external modules.
  
- For subroutines that are specific to an electronic structure code (Turbomole,
  Columbus, ...), the name should begin with a prefix (`tm_`, `col_`, ...)
  
# Cases

- Global variables in each module should be written in `CAPITAL LETTERS` (*this for easier
  reading of the code only, Fortran is case insensitive !*)

- All other variable and routines should only contain lower case characters.

- Always prefer explicit names for variables: `check_mo_ovl` is better than
  `cmoovl`.
  
