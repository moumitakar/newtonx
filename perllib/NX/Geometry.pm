# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Geometry;

use strict;
use warnings;
use NX::Tools qw( units );

require Exporter;
our @ISA = qw( Exporter );
our @EXPORT_OK = qw( nx2xyz xyz2zmat read_z_matrix write_internal_coordinates md_to_tm );

our %atoms = (
    h => 'H ', he => 'He', li => 'Li', be => 'Be', b  => 'B ', c  => 'C ',
    n  => 'N ', o  => 'O ', f  => 'F ', ne => 'Ne', na => 'Na', mg => 'Mg',
    al => 'Al', si => 'Si', p  => 'P ', s  => 'S ', cl => 'Cl', ar => 'Ar',
    k  => 'K ', ca => 'Ca', sc => 'Sc', ti => 'Ti', v  => 'V ', cr => 'Cr',
    mn => 'Mn', fe => 'Fe', co => 'Co', ni => 'Ni', cu => 'Cu', zn => 'Zn',
    ga => 'Ga', ge => 'Ge', as => 'As', se => 'Se', br => 'Br', kr => 'Kr',
    rb => 'Rb', sr => 'Sr', y  => 'Y ', zr => 'Zr', nb => 'Nb', mo => 'Mo',
    tc => 'Tc', ru => 'Ru', rh => 'Rh', pd => 'Pd', ag => 'Ag', cd => 'Cd',
    in => 'In', sn => 'Sn', sb => 'Sb', te => 'Te', i  => 'I ', xe => 'Xe',
    cs => 'Cs', ba => 'Ba', la => 'La', ce => 'Ce', pr => 'Pr', nd => 'Nd',
    pm => 'Pm', sm => 'Sm', eu => 'Eu', gd => 'Gd', tb => 'Tb', dy => 'Dy',
    ho => 'Ho', er => 'Er', tm => 'Tm', yb => 'Yb', lu => 'Lu', hf => 'Hf',
    ta => 'Ta', w  => 'W ', re => 'Re', os => 'Os', ir => 'Ir', pt => 'Pt',
    au => 'Au', hg => 'Hg', tl => 'Tl', pb => 'Pb', bi => 'Bi', po => 'Po',
    at => 'At', rn => 'Rn', fr => 'Fr', ra => 'Ra', ac => 'Ac', th => 'Th',
    pa => 'Pa', u  => 'U ', np => 'Np', pu => 'Pu', am => 'Am', cm => 'Cm',
    bk => 'Bk', cf => 'Cf', es => 'Es', fm => 'Fm', md => 'Md', no => 'No',
    lr => 'Lr', rf => 'Rf', db => 'Db', sg => 'Sg', bh => 'Bh', hs => 'Hs',
    mt => 'Mt', ds => 'Ds', rg => 'Rg', cn => 'Cn', nh => 'Nh', fl => 'Fl',
    mc => 'Mc', lv => 'Lv', ts => 'Ts', og => 'Og',
   );

sub nx2xyz {
  my ($file, $output) = @_;
  $file //= 'geom';
  $output //= 'geom.xyz';

  my $au2ang = units("au2ang");

  my @coords = ();
  my $Natoms = 0;

  # First extract coordinates and symbols from geom
  open (my $fh, '<:encoding(UTF-8)', $file) or die "Couldn't read $file $!, stopped";
  while (<$fh>) {
    chomp;
    s/^\s+|\s+$//g;
    my @temp = split /\s+/;
    my $at = lc($temp[0]);
    $at = $atoms{$at};
    push @coords, [$at, $au2ang*$temp[2], $au2ang*$temp[3], $au2ang*$temp[4]];
    $Natoms++;
  }
  close $fh;

  # Then write the new xyz file
  open ($fh, '>:encoding(UTF-8)', $output) or die "Couldn't write geom.xyz $!, stopped";
  print $fh "$Natoms\n\n";
  while (@coords) {
    my $ele = shift @coords;
    printf $fh ("%7s  %15.6f  %15.6f   %15.6f\n", @$ele[0], @$ele[1], @$ele[2], @$ele[3]);
  }
  close $fh;
}


sub xyz2zmat {

  use Chemistry::InternalCoords::Builder 'build_zmat';
  use Chemistry::File::XYZ;
  use Chemistry::File::InternalCoords;

  my ($xyz_file, $output) = @_;
  $xyz_file //= 'geom.xyz';
  $output //= 'geom.zmat';

  # read an XYZ file
  my $mol = Chemistry::Mol->read($xyz_file);
  $mol->write($output, format => 'zmat');
}


sub read_z_matrix {

  my ($Natoms, $input) = @_;
  $input //= 'geom.zmat';

  # Returns the list of stretches, angles and torsions from a zmatrix file.
  my @stretch = ();
  my @angles = ();
  my @torsions = ();

  open (my $fh, '<encoding(UTF-8)', $input) or die "Couldn't open $input $!, stopped";
  # Skip first atom
  $_ = <$fh>;

  # For the second atom, get the stretch
  $_ = <$fh>;
  chomp;
  s/^\s+|\s+$//g;
  push @stretch, (split(/\s+/, $_))[1];

  # Third atom: stretch and angle
  if ($Natoms > 2) {
    $_ = <$fh>;
    chomp;
    s/^\s+|\s+$//g;
    push @stretch, (split(/\s+/, $_))[1];
    push @angles, (split(/\s+/, $_))[3];
  }

  # Then for the remaining atoms, get stretch, angles, and torsions
  if ($Natoms > 3) {
    for (my $atom = 4; $atom <= $Natoms; $atom++) {
      $_ = <$fh>;
      chomp;
      s/^\s+|\s+$//g;
      push @stretch, (split(/\s+/, $_))[1];
      push @angles, (split(/\s+/, $_))[3];
      push @torsions, (split(/\s+/, $_))[5];
    }
  }

  close $fh;

  return (\@stretch, \@angles, \@torsions);
}


sub write_internal_coordinates {
  my ($Natoms, $stretch, $angles, $torsions) = @_;

  my $output = 'intcfl';
  open (my $fh, '>:encoding(UTF-8)', $output) or die "Couldn't open $output $!, stopped";

  print $fh "TEXAS\n";
  my $mode = 1;
  my $atom_index = 0;
  foreach (@$stretch) {
    my $str = "K                   STRE";
    printf $fh "%s%6d%5d.    %5d.    \n", $str, $mode, $atom_index+2, $_;
    $mode++;
    $atom_index++;
  }

  $atom_index = 0;
  my $dummy = 0;
  foreach (@$angles) {
    my $str="K                   BEND";
    printf $fh "%s%6d%5d.    %5d.    %5d.    \n", $str, $mode, $atom_index+3, $_, @$stretch[$dummy+1];
    $dummy++;
    $atom_index++;
    $mode++;
  }

  $atom_index = 0;
  $dummy = 0;
  foreach (@$torsions) {
    my $str = "K                   TORS";
    printf $fh "%s%6d%5d.    %5d.    %5d.    %5d.    \n", $str, $mode, $atom_index+4, @$stretch[$dummy+2], @$angles[$dummy+1], $_;
    $dummy++;
    $atom_index++;
    $mode++;
  }

  close $fh;
}


sub md_to_tm {
  my $md_geom = 'geom';
  my $tm_geom = 'coord';

  # Extract coordinates from NX geom
  open (my $fh, '<:encoding(UTF-8)', $md_geom) or die "Could not open $md_geom, stopped";
  my @coords;
  while (<$fh>) {
    chomp;
    s/^\s+|\s+$//g;
    my @temp = split /\s+/;
    push @coords, [$temp[0], $temp[2], $temp[3], $temp[4]];
  }
  close $fh;

  # And now copy them to Turbomole coord
  open ($fh, '>:encoding(UTF-8)', $tm_geom) or die "Could not open $md_geom, stopped";
  print $fh "\$coord\n";
  foreach my $atom (@coords) {
    printf $fh "%20.14f %20.14f %20.14f %s\n", @{$atom}[1], @{$atom}[2], @{$atom}[3], @{$atom}[0];
  }
  print $fh "\$user-defined bonds\n";
  print $fh "\$end\n";
  close $fh;

  return 0;
}

1;
