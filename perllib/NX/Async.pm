# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Async;

use strict;
use warnings;
use diagnostics;

use Cwd;
use POSIX qw( floor ); 

BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( main run_async );
}

our %MAIN_SETUP = (
    exc_mopac => ['exc_mop', '', 'mopac2002.x', 'MOPAC', 'mopac_tmp.out'],
    exc_gaussian => ['exc_gau', '.com', '/g16/g16', 'g16root', ''],
    exc_gaussian_ovl => ['exc_gau', '_ovl.com', '/g16/g16', 'g16root', ''],
    );


sub main {
    my ($qmcode, $nprocs, $njobs) = @_;
    
    my ($basename, $ext, $exe, $path, $output) = set_info($qmcode);
    my $job_list = build_job_list($basename, $ext, $njobs);

    my $env = '';
    if (defined $path) { $env = $ENV{$path}; }
    my $program = "$env"."/"."$exe";
    run_async($job_list, $program, $nprocs, $output);
}


sub run_async {
    my ($job_list, $program, $nproc, $output) = @_;

    my $nJobs = scalar @$job_list;
    my $nBatches = floor($nJobs / $nproc);
    my $remainder = $nJobs % $nproc;

    for (my $nn = 0; $nn < $nBatches; $nn++) {
	divide($job_list, $nproc, $nn, $program, $output);
    }

    if ($remainder != 0) {
	divide($job_list, $remainder, $nBatches, $program);
    }
}


sub set_info {
    my ($qmcode) = @_;

    my $basename = $MAIN_SETUP{$qmcode}[0];
    my $ext = $MAIN_SETUP{$qmcode}[1];
    my $exe = $MAIN_SETUP{$qmcode}[2];
    my $path = $MAIN_SETUP{$qmcode}[3];
    my $output = $MAIN_SETUP{$qmcode}[4];

    return ($basename, $ext, $exe, $path, $output);
}


sub build_job_list {
    my ($basename, $ext, $njobs) = @_;

    my @job_list;
    my $extension = '';
    if (defined $ext) { $extension = $ext; }

    for (my $j = 1; $j <= $njobs ; $j++){
    	push @job_list , "$basename"."$j"."$extension";
    }

    return \@job_list;
}


sub run_prog {
    my ($program, $input, $output) = @_;
    if (defined $output) {
	exec("$program $input > $output 2>&1");
    } else {
	exec("$program $input");
    }
}


sub divide {
    my ($job_list, $nproc, $start, $program, $output) = @_;
    my %jobs_status = ();
    my @commands = ();
    
    # Each batch has $nproc jobs running
    for (my $i = 0; $i < $nproc; $i++) {
	my $index = $i  + ($start * $nproc);
	push @commands, sub {run_prog($program, @$job_list[$index])};
    }

    # Now we loop over @commands to create one fork per job to execute
    my $i = 1;
    foreach my $cmd (@commands) {
	my $index = $i  + ($start * $nproc);
	$i++;
	
	my $pid = fork();
	die "Fork failed for @$job_list[$index]" unless defined $pid;

	if ($pid == 0) { # Child process
	    eval {
		$cmd->();
	    };
	    $@ and die "Failure in cmd: $@\n"; # If eval has failed
	}

	# Now this is the parent process
	if ($pid <= 0) {
	    warn "Job $cmd failed (pid: $pid): $!";
	} else {
	    $jobs_status{$pid} = {
		cmd => $cmd,
		status => 'running'
	    };
	}
    }

    # At this point all jobs have been launched.
    while(%jobs_status) {
	my $pid = wait();
	last if ($pid == -1); # No more children to wait for
	next unless exists $jobs_status{$pid}; # We only care about the job we are watching

	my $job = $jobs_status{$pid};
	print STDOUT "$pid ($job->{cmd}) finished with code $?\n";
	delete $jobs_status{$pid};
    }
}

1;
