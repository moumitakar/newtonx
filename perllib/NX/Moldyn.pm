# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Moldyn;

use strict;
use warnings;
use diagnostics;

use NX::Version qw( nx_version );
use NX::Configuration;
use NX::InputGenerator::SetValues;
use NX::Tools qw( prog_name pad_str);
use NX::Columbus qw( col_complete_configuration col_check_inputs);

use Data::Dumper;
use File::Copy::Recursive qw( fcopy dircopy dirmove);
use File::Path qw ( remove_tree );

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT = qw();
    @EXPORT_OK = qw( exit_error generate_missing_defaults require_nad
		     require_cioverlap test_geom_and_veloc title_message
		     moldyn_generate_configuration check_inputs create_folders
	             );
    %EXPORT_TAGS = ();
}


sub moldyn_generate_configuration {
    # Export 'configuration.inp' from minimal 'user_config.nml'
    #
    my ($configuration, $user_conf) = @_;

    my $imported_parameters = $configuration->import_nml("$user_conf");

    #  -----------------------------------------------------
    #  1. Which section should we export in the main input ?
    #  -----------------------------------------------------
    my $has_sh = 0;
    my $has_cioverlap = 0;
    my $has_auxnac = 0;
    my $has_nad = 0;
    my $has_h5md = 0;
    my $has_adapt_dt = 0;
    my $has_zpe_correction = 0;

    # This section should be exported for any dynamics.
    my @to_export = ( 'nxconfig' );
    generate_missing_defaults($configuration, 'nxconfig', $imported_parameters);

    push @to_export, 'h5md';
    $has_h5md = 1;

    push @to_export, 'adapt_dt';
    $has_h5md = 1;

    # Export the section corresponding to the program
    my ($progname, $methodname) = prog_name($configuration->value('nxconfig', 'prog'));

    if ($progname eq 'analytical') {
	push @to_export, "$methodname";
    } else {
	push @to_export, "$progname";
    }

    if ($progname eq 'exc_mopac' or $progname eq 'exc_gaussian') {
       push @to_export, 'exc_inp';
    }


    # Decide on wether to export 'sh' if it makes sense, i.e. if thres is not 0,
    # and if more than one state is computed.
    if ($configuration->value('nxconfig', 'thres') != 0) {
	if ($configuration->value('nxconfig', 'nstat') > 1) {
	    push @to_export, 'sh';
	    $has_sh = 1;
	}
    }

    # If local diabatization is requested, set the derivative coupling method if necessary
    my $locdiab = ($configuration->value('sh', 'integrator') == 2);
    if ($locdiab) {
	my $coupling_method = $configuration->value('nxconfig', 'dc_method');
	if ($coupling_method != 2) {
	    $configuration->value('nxconfig', 'dc_method', 2);
	    push @{%$imported_parameters{nxconfig}}, 'dc_method'
		unless grep /dc_method/, @{%$imported_parameters{nxconfig}};
	}
    }

    # Decide if an external program should be used for time-derivatives
    if ($configuration->value('nxconfig', 'dc_method') == 2) {
	push @to_export, 'cioverlap';
	$has_cioverlap = 1;
    }

    if ($configuration->value('nxconfig', 'dc_method') == 3) {
	push @to_export, 'auxnac';
	$has_auxnac = 1;
    }

    # Decide if NAD setup should be exported
    if (grep /sh/, @to_export) {
	push @to_export, 'nad_setup';
	$has_nad = 1;
    }

    # # Adaptive time step
    # if (grep /adapt_dt/, keys(%$imported_parameters)) {
    # 	push @to_export, 'adapt_dt';
    # 	$has_adapt_dt = 1;
    # 
    # 	# Here we have to set check_mo_ovl to 1 !!
    # 	$configuration->value('nxconfig', 'check_mo_ovl', 1); 
    # }

    # ZPE correction
    if (grep /zpe_correction/, keys(%$imported_parameters)) {
	push @to_export, 'zpe_correction';
	$has_zpe_correction = 1;
    }

    #  -----------------------------------------------------
    #  2. Fill the missing parts
    #  -----------------------------------------------------
    # $qm_input: Where to get the inputs from ?
    my $qm_input = '';
    my $coupling_method = $configuration->value('nxconfig', 'dc_method');
    if ($coupling_method == 1) {
	# We want to read the NAC from the QM job, so we should have JOB_NAD
	$qm_input = 'JOB_NAD';
    } else {
	# We either want time-derivatives from other methods, or we don't need them
	$qm_input = 'JOB_AD';
    }

    # For hybrid setups
    $configuration->value('nxconfig', 'init_input', "\"$qm_input\"");


    # Loop over each exported section, to look for any parameter that was not
    # imported, and that has a defined set function, to apply this function.
    foreach my $sec (@to_export) {
	next if $sec =~ m/nxconfig/;
	# print STDOUT "Generating missing defaults for section: $sec\n";
        if ($sec eq 'cioverlap') {
	    my $tmp = $configuration->value($sec, 'read_ovl_matrix');
            if (! $tmp =~ m/\/cioverlap\/cioverlap\.out/) {
                next;
            }
        }
	generate_missing_defaults($configuration, $sec, $imported_parameters);
    }

    # QM specific setup
    if ($progname eq "columbus") {
	col_complete_configuration($configuration, $qm_input);
    }

    return \@to_export;
}


sub check_inputs {
    my ($configuration, $imported) = @_;

    # Check if geometry and velocity files are present and match with configuration
    my $gini = $configuration->value('nxconfig', 'init_geom');
    my $vini = $configuration->value('nxconfig', 'init_veloc');
    my $nat = $configuration->value('nxconfig', 'nat');
    my ($progname, $methodname)  = prog_name($configuration->value('nxconfig', 'prog'));

    $gini =~ s/\"//g;
    $vini =~ s/\"//g;

    if ($configuration->value('nxconfig', 'nxrestart') == 1) {
	$gini = 'INFO_RESTART/'."$gini";
	$vini = 'INFO_RESTART/'."$vini";
    }

    my $err = 0;
    my $errmsg = '';

    my ($nlines_geom, $nlines_veloc) = test_geom_and_veloc($configuration, $gini, $vini);

    if ($nlines_geom != $nat) {
	$err = 1;
	$errmsg = $errmsg."\nMismatch in number of atoms:\n";
	$errmsg = $errmsg."  Number of atoms set: $nat\n";
	$errmsg = $errmsg."  Number of atoms in $gini: $nlines_geom\n";
	return ($err, $errmsg);
    }

    if ($nlines_veloc != $nat) {
	$err = 1;
	$errmsg = $errmsg."\nMismatch in number of atoms:\n";
	$errmsg = $errmsg."  Number of atoms set: $nat\n";
	$errmsg = $errmsg."  Number of atoms in $vini: $nlines_veloc\n";
	return ($err, $errmsg);
    }

    # QM specific check
    if ($progname eq "columbus") {

	my $has_nad = 0;
	if (grep /nad_setup/, (keys %$imported) ) {
	    $has_nad = 1;
	}
	my $qm_input = $configuration->value('nxconfig', 'init_input');
	$qm_input =~ s/\"//g;

	if ($configuration->value('nxconfig', 'nxrestart') == 1) {
	    $qm_input = 'INFO_RESTART/'."$qm_input";
	}

	my $cirestart = $configuration->value("columbus", "cirestart");
	my $mode = $configuration->value("columbus", "mode");
	my $mocoef = $configuration->value("columbus", "mocoef");

	($err, $errmsg) = col_check_inputs($qm_input, $has_nad, $cirestart, $mocoef, $mode);
	if ($err != 0) {
	    return $err, $errmsg;
	}
    }
    elsif ($progname eq "analytical") {
	if ($methodname eq "onedim_model") {
	    my $init_input = $configuration->value('nxconfig', 'init_input');
	    $init_input =~ s/\"//g;
	    if (! -e "$init_input"."/onedim_parameters.inp") {
	    	$err = 1;
	    	$errmsg = "No file onedim_parameters.inp found in $init_input\n";
	    }
	}
    }

    return $err, $errmsg;
}


sub create_folders {
    my ($configuration) = @_;

    my $gini = $configuration->value('nxconfig', 'init_geom');
    my $vini = $configuration->value('nxconfig', 'init_veloc');
    my $output_path = $configuration->value('nxconfig', 'output_path');
    my $debug_path = $configuration->value('nxconfig', 'debug_path');

    $gini =~ s/\"//g;
    $vini =~ s/\"//g;
    $output_path =~ s/\"//g;
    $debug_path =~ s/\"//g;
    my $config_file = 'configuration.inp';
    my $wfini = 'wf.inp';
    my $rnd_file = 'rndseed';
    my $JAD = "JOB_AD";
    my $JNAD = "JOB_NAD";

    my @files_to_copy = (
	"$gini",
	"$vini",
	"$wfini",
	"$config_file",
	"$rnd_file",
	);
    my @dir_to_copy = (
	"$JAD",
	"$JNAD",
	);

    print STDOUT "\nCreating TEMP directory ... ";

    if (-d 'TEMP.old') {
	remove_tree('TEMP.old');
    }
    dirmove('TEMP', 'TEMP.old');

    mkdir('TEMP')
	or die "Couldn't create TEMP ($!), stopped";
    if (! -d "$debug_path") {
	mkdir("$debug_path")
	    or die "Couldn't create DEBUG ($!), stopped";
    }

    foreach my $ff (@files_to_copy) {
	my $origin = $ff;
	my $target = 'TEMP/'."$ff";
	if ($configuration->value('nxconfig', 'nxrestart') == 1) {
	    $origin = 'INFO_RESTART/'."$ff";
	}
	if (-e "$origin") {
	    fcopy("$origin", "$target")
		or die "Couldn't copy '$ff' to TEMP ($!), stopped";
	}
    }

    foreach my $dd (@dir_to_copy) {
	my $origin = $dd;
	my $target = 'TEMP/'."$dd";
	if ($configuration->value('nxconfig', 'nxrestart') == 1) {
	    $origin = 'INFO_RESTART/'."$dd";
	}
	if (-d "$origin") {
	    mkdir("$target")
		or die "Couldn't create 'TEMP/$dd' ($!), stopped";
	    dircopy("$origin", "$target")
		or die "Couldn't copy '$dd' to TEMP ($!), stopped";
	}
    }

    print STDOUT "done\n";

    # Create the RESULTS directory
    if ($configuration->value('nxconfig', 'nxrestart') == 0) {
	print STDOUT "Creating $output_path directory ... ";
	mkdir("$output_path")
	    or die "Couldn't create '$output_path' ($!), stopped";
	print STDOUT "done\n";
    }
}


sub title_message {

    my $header_length = 80;
    my $tmpmsg = "=" x $header_length;

    my $license = <<"END_MESSAGE";
Copyright (C) 2022  Light and Molecules Group

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

END_MESSAGE

    my ($version, $git_version) = build_nx_version();
    my $title = pad_str("NEWTON-X New Series 3", $header_length);
    my $v_title = pad_str("Version $version - $git_version", $header_length);
    my $subtitle = pad_str("Newtonian dynamics close to the crossing seam", $header_length);
    my $website = pad_str("www.newtonx.org", $header_length);

    $tmpmsg = $tmpmsg."\n".$title."\n".$subtitle."\n".$v_title."\n\n".$website."\n";
    $tmpmsg = $tmpmsg."="x80;
    $tmpmsg = $tmpmsg."\n\n".$license."\n\n";
    $tmpmsg = $tmpmsg." Please report any bugs as issues in our Gitlab:\n";
    $tmpmsg = $tmpmsg."   https://gitlab.com/light-and-molecules/newtonx/\n\n";
    $tmpmsg = $tmpmsg." Or to our mailling list:\n";
    $tmpmsg = $tmpmsg."   newtonx [at] freelists.org\n\n";
    $tmpmsg = $tmpmsg."-"x80;
    $tmpmsg = $tmpmsg."\n\n";

    return $tmpmsg;
}


sub build_nx_version {
    my $nxenv = $ENV{NXHOME};

    # # Main version
    # my $version_file = "$nxenv/version.txt";
    # if (! -f "$version_file") {
    # 	$version_file = "$nxenv/share/newtonx/version.txt";
    # }
    # open(my $fh, '<:encoding(UTF-8)', "$version_file")
    # 	or die "Couldn't open $version_file ($!), stopped";
    # my $version = '';
    # while (<$fh>) {
    # 	chomp;
    # 	$version = $version.$_;
    # }
    # close($fh);

    my $version = nx_version();

    # If it exists, GIT version
    my $git_version = 'release';
    if (-d "$nxenv/.git") {
	$git_version = '';
	system("git --git-dir=$nxenv/.git describe --dirty --always --tags > git_version.txt");
	open(my $fh, '<:encoding(UTF-8)', "git_version.txt")
	    or die "Couldn't open git_version.txt ($!), stopped";
	while (<$fh>) {
	    chomp;
	    $git_version = $git_version.$_;
	}
	close($fh);
	unlink "git_version.txt";
	$git_version =~ s/$version//;
	$git_version =~ s/^-//;
    }

    return $version, $git_version;
}


sub test_geom_and_veloc {
  my ($configuration, $geom, $veloc) = @_;
  my $nat = $configuration->value('nxconfig', 'nat');

  my $nlines_geom = 0;
  open (my $fh, '<:encoding(UTF-8)', $geom)
      or die "Couldn't open $geom ($!), stopped";
  while (<$fh>) {
    $nlines_geom++;
  }
  close $fh;

  my $nlines_veloc = 0;
  open ($fh, '<:encoding(UTF-8)', $veloc)
      or die "Couldn't open $veloc ($!), stopped";
  while (<$fh>) {
    $nlines_veloc++;
  }
  close $fh;

  return $nlines_geom, $nlines_veloc;

}


sub exit_error {
    # Exit the program with the given error message.
    #
    my ($msg) = @_;
    my $tmp = '*' x 80;
    $tmp = $tmp."\n\tERROR: ".$msg."\n";
    $tmp = $tmp.'*' x 80;
    $tmp = $tmp."\n";

    print STDOUT "$tmp\n";
    die "  \nError termination of moldyn !!!\n ";
}


sub require_nad {
    # Check wether nad_setup should be there.
    #
    # Return 1 if NAD are requested, else 0. NAD computations are requested if
    # an analytical method is used (e.g. if a 'JOB_NAD' folder is present) or
    # if cioverlap is required.
    my ($config) = @_;

    # Checks for the presence of JOB_NAD
    if (-d 'JOB_NAD') {
	return 1;
    }

    # Checks for the presence of cioverlap;
    if ( require_cioverlap($config) > 0 ) {
	return 1;
    }

    return 0;
}


sub require_cioverlap {
    # Check wether cioverlap program should be called.
    #
    # Return 1 if cioverlap should be called, else return 0. The cioverlap
    # program has to be called in the following situations:
    # - No JOB_NAD folder is present AND
    #   - nstat > 1 OR
    #   - thres > 0
    my ($config, $imported_parameters) = @_;

    if (($config->value('nxconfig', 'progname') eq "exc_mopac")
	or ($config->value('nxconfig', 'progname') eq "exc_gaussian")) {
        return 1;
    }
    elsif (-d 'JOB_NAD') {
        return 0;
    }


#    if (-d 'JOB_NAD') {
#	return 0;
#    }
    else {
	my $nstat = $config->value('nxconfig', 'nstat');
	my $thres = $config->value('nxconfig', 'thres');
	if ($nstat == 1) {
	    return 0;
	}
	elsif ($thres == 0) {
	    return 0;
	}
    }

    return 1;
}


sub get_files_to_save {
    my ($qm_code) = @_;

    my @files = ();
}


sub generate_missing_defaults {
    my ($configuration, $sec, $imported_parameters) = @_;


    my $imported_section = %{ $imported_parameters }{$sec};
    my $conf_section = $configuration->section($sec);

    foreach my $param (keys %{ $conf_section }) {
	if (! grep /$param/, @$imported_section) {
	    if (defined $conf_section->{$param}->{func}) {
		my $func = $conf_section->{$param}->{func};
		require NX::InputGenerator::SetValues;
		$func = $NX::InputGenerator::safe_set{$func};
		if (defined $func) {
		    my $value = $func->($configuration);
		    $configuration->value($sec, $param, $value);
		}
	    }
	}
    }
}

return 1;
