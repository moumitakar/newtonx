# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Columbus;

use strict;
use warnings;
use diagnostics;

use File::Copy qw( copy move );
use File::Copy::Recursive qw( fcopy dircopy dirmove );
use File::Path qw( make_path remove_tree );

use Cwd qw( getcwd );

use NX::Configuration;
use Data::Dumper;

BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( col_complete_configuration col_check_inputs
		     col_clean_work_dir         col_update_inputs
		     col_post_run               col_init_overlap
		     col_prepare_fake_double    col_prepare_cioverlap
		     col_finish_cioverlap
		  );
}

# Functions to export:
# load_file, col_get_mode, col_get_maximum_excitation_level,
# col_delete_keyword, col_change_keywords_nml

sub col_complete_configuration {
    # Complete the main configuration with Columbus parameters.
    #

    my ($config, $homedir) = @_;
    my $control = "$homedir/control.run";

    my $ci_type = col_ci_type("$control");
    $config->add("columbus", "ci_type", $ci_type);

    my $mode = col_get_mode("$control");
    $config->add("columbus", "mode", "\"$mode\"");

    my $mel = 0;
    if (($mode eq 'ms') or ($mode eq 'ss')) {
	my $input = '';
	if ($mode eq 'ss') {
	    $input = 'cidrtin';
	}
	elsif ($mode eq 'ms') {
	    $input = 'cidrtmsin';
	}
	$input = "$homedir/$input";
	$mel = col_get_maximum_excitation_level($input);
    }

    $config->add("columbus", "mel", $mel);
}


sub col_update_inputs {
    # Update the Columbus inputs.
    #
    # General function wrapper for modifying the various inputs for Columbus,
    # depending on the parameters given as input. In general this function
    # will be called (not directly though) by the Fortran main program.

    my ($nstatdyn, $type_of_dyn, $step, $compute_nac) = @_;

    my $NXHOME=$ENV{NXHOME};
    my $default_config_file = "$NXHOME"."/data/default.yml";
    if (! -f $default_config_file) {
	$default_config_file = "$NXHOME"."/share/newtonx/data/default.yml";
    }
    my $main_config = NX::Configuration->new($default_config_file);
    $main_config->import_nml('configuration.inp');

    my $nstat = $main_config->value('nxconfig', 'nstat');
    my $mode = $main_config->value('columbus', 'mode');
    my $ci_iter = $main_config->value('columbus', 'ci_iter');
    my $ivmode = $main_config->value('columbus', 'ivmode');
    my $citol = $main_config->value('columbus', 'citol');
    my $reduce_tol = $main_config->value('columbus', 'reduce_tol');
    my $quad_conv = $main_config->value('columbus', 'quad_conv');
    my $cirestart = $main_config->value('columbus', 'cirestart');
    my $ci_type = $main_config->value('columbus', 'ci_type');
    my $mocoef = $main_config->value('columbus', 'mocoef');
    my $all_grads = $main_config->value('columbus', 'all_grads');
    my $grad_lvl = $main_config->value('columbus', 'grad_lvl');

    my $derivative_couplings = $main_config->value('nxconfig', 'dc_method');

    # Mode has '"' inside, so we have to strip them now !!
    $mode =~ s/\"//g;

    my $init_step = $main_config->value('nxconfig', 'init_step');

    my $tol = col_wrt_redtol($citol, $type_of_dyn, $nstat, $nstatdyn, $reduce_tol);

    # Set ncoupl option in MCSCF
    col_change_keywords_nml('mcscfin', 'mcscfin', {ncoupl => $quad_conv});

    # If required (MR-CI computation), change the CI inputs
    if ($mode eq 'ms') {
	my %ms_keywords = col_set_ms_keywords($nstat, $ivmode, $tol);
	col_change_keywords_nml('ciudgin.drt1', 'ciudgin.drt1', \%ms_keywords);
    }
    elsif ($mode eq 'ss') {
	my %ss_keywords = col_set_ss_keywords($nstat, $ci_iter, $ivmode, $tol);
	col_change_keywords_nml('ciudgin', 'ciudgin', \%ss_keywords);
	col_change_keywords_nml('ciudgin', 'ciudgin', {nroot => $nstat});
	col_change_keywords_nml('cidenin', 'cidenin', {lroot => $nstatdyn});
    }

    # After the first step, remove SCF part
    if ($step != $init_step) {
	col_delete_keyword('control.run', 'scf');
    }

    # If CI-restart is requested, prepare it now
    if ($ci_type >= 1) {
	if ($cirestart == 1) {
	    if ($step != $init_step) {
		col_ci_restart_1($mode);
	    }
	}
    }

    if ($mocoef == 1) {
	if (-e "mocoef.old") {
	    copy("mocoef.old", "mocoef");
	}
    }

    # Now produce the 'transmomin' file for gradient computation.
    # By default, compute all gradients.
    # if ($compute_nac == 1) {
    #	copy('transmomin', 'transmomin.nx');
    #
    #	open(my $fh, '>>:encoding(UTF-8)', "transmomin")
    #	    or die "Could not open transmomin, $! stopped ";
    #	for (my $i = 1; $i <= $nstat; $i++) {
    #	    my $tm_line = sprintf("%3d%3d%3d%3d\n", 1, $i, 1, $i);
    #	    print $fh $tm_line;
    #	}
    #	close($fh);
    # } else {
    #	open(my $fh, '>:encoding(UTF-8)', "transmomin")
    #	    or die "Could not open transmomin, $! stopped ";
    #	print $fh "  CI \n";
    #	my $tm_line = sprintf("%3d%3d%3d%3d\n", 1, $nstatdyn, 1, $nstatdyn);
    #	print $fh $tm_line;
    #	# for (my $i = 1; $i <= $nstat; $i++) {
    #	#     my $tm_line = sprintf("%3d%3d%3d%3d\n", 1, $i, 1, $i);
    #	#     print $fh $tm_line;
    #	# }
    #	close($fh);
    # }
    # Create Gradient line for transmomin
    my $tm_line = "";
    if ($all_grads == 0) { # Calc 1 gradient
	$tm_line = sprintf("%3d%3d%3d%3d\n",1,$nstatdyn,1,$nstatdyn);
    } elsif ($all_grads == 1) {
	for (my $n = 1; $n <= $nstat; $n++) { # Calc all gradients
	    $tm_line = $tm_line.sprintf("%3d%3d%3d%3d\n",1,$n,1,$n);
	}
    }

    # Change CI by MCSCF in transmomin if needed
    # if($lvprt>=2){print_STDOUT("$mdle replacing CI by MCSCF in transmomin\n",$istep,$kt);}
    copy('transmomin', 'transmomin.nx');
    open(my $TM, '<:encoding(UTF-8)', "transmomin.nx");
    open(my $TM2, '>:encoding(UTF-8)', "transmomin");
    while(<$TM>){
	my $line = $_;
	if ($line =~ /MCSCF/){
	    if ($grad_lvl == 2){ # CI gradients
		print $TM2 "CI\n";
	    } else {
		print $TM2 $_;
	    }
	} elsif ($line =~ /CI/) {
	    if ($grad_lvl == 1) { # MCSCF gradients
		print $TM2 "   MCSCF\n";
	    } else {
		print $TM2 $_;
	    }
	} else {
	    chomp;
	    s/^\s*//;
	    s/\s*$//;
	    my ($drt1,$st1,$drt2,$st2) = split(/\s+/,$_);
	    #if (($vdoth == 1) or ($vdoth < 0) or ($vdoth == 2)){ # If NAC shouldn't be computed, add "T"
	    if ($derivative_couplings != 1) {
		if ($st1 != $st2) {
		    printf $TM2 "%3d%3d%3d%3d T\n",$drt1,$st1,$drt2,$st2;
		}
	    } else {
		if ($st1 != $st2){
		    printf $TM2 "%3d%3d%3d%3d\n",$drt1,$st1,$drt2,$st2;
		}
	    }
	}
    }
    close($TM);
    # close($TM);
    #unlink "transmomin.nx";

    # Append gradient lines to transmomin
    #if($lvprt>=2){print_STDOUT("$mdle appending gradient line to transmomin\n",$istep,$kt);}
    # open(TM,">>transmomin");
    print $TM2 $tm_line;
    close($TM2);
}


sub col_clean_work_dir {
    # Clean the current working directory.
    #
    # The function removes some directories and files created by a Columbus job,
    # while optionnally copying them as backup if requested ($debug >= 2). If
    # $debug is >= 2, then the $step parameter should also be given, to create
    # the right backup directory.
    #
    # usage:
    #    col_clean_work_dir($debug, $step);

    my ($debug, $step) = @_;

    my @files_to_clean = (
	'curr_iter', 'runc.error', 'runls',
       );

    my @dir_to_clean = (
	'COSMO'  , 'GRADIENTS', 'LISTINGS',
	'MOCOEFS', 'MOLDEN'   , 'RESTART',
	'WORK'   ,
       );


    my $db_folder = "debug_col/step_$step/";

    # Save MOCOEF for later
    if ($step >= 1) {
	copy("MOCOEFS/mocoef_mc.sp", "mocoef.old")
	    or die "Couldn't copy MOCOEFS/mocoef_mc.sp ($!), stopped";
    }

    # Backup files
    foreach my $ff (@files_to_clean) {
	if (-e "$ff") {
	    if ($debug >= 2) {
		if (! -d "$db_folder/") {
		    mkdir("$db_folder/");
		}
		fcopy("$ff", "$db_folder/$ff");
	    }
	    unlink "$ff";
	}
    }

    # Backup directories
    foreach my $dir (@dir_to_clean) {
	if (-d "$dir") {
	    if ($debug >= 2) {
		dircopy("$dir", "$db_folder/$dir/");
	    }
	    remove_tree("$dir");
	}
    }
}


sub col_set_ss_keywords {
    # Set the SS keywords that should be modified during the dynamics.
    #
    # The function takes as argument the number of states, the maximum number
    # of CI iterations, the ivmode parameter, and a tolerance string, an
    # return a hash with keys corresponding to the keywords that should be
    # modified during the dynamics.
    #
    # The keywords contained in the hash are the following:
    #   - nvbkmn;
    #   - nvbkmx;
    #   - niter ;
    #   - nvcimn;
    #   - nvcimx;
    #   - ivmode;
    #   - rtolbk;
    #   - rtolci;
    #
    # usage:
    #    my %ss_keys = col_set_ss_keywords($nstat, $ci_iter, $ivmode, $tol);

    my ($nstat, $ci_iter, $ivmode, $tol) = @_;

    my $nstd = $nstat;
    my %res = ();
    $res{nvbkmn} = $nstd;
    $res{nvbkmx} = $nstd + 5;
    $res{niter } = $nstd * $ci_iter;
    $res{nvcimn} = $nstd + 2;
    $res{nvcimx} = $nstd + 5;
    $res{ivmode} = $ivmode;
    $res{rtolbk} = $tol;
    $res{rtolci} = $tol;

    return %res;
}


sub col_set_ms_keywords {
    # Set the MS keywords that should be modified during the dynamics.
    #
    # The function takes as argument the number of states, the ivmode
    # parameter, and a tolerance string, an return a hash with keys
    # corresponding to the keywords that should be modified during the
    # dynamics.
    #
    # The keywords contained in the hash are the following:
    #   - nroot;
    #   - ivmode;
    #   - rtolbk;
    #   - rtolci;
    #
    # usage:
    #    my %ms_keys = col_set_ms_keywords($nstat, $ivmode, $tol);

    my ($nstat, $ivmode, $tol) = @_;

    my %res = ();
    $res{ivmode} = $ivmode;
    $res{nroot } = $nstat;
    $res{rtolbk} = $tol;
    $res{rtolci} = $tol;

    return %res;
}


sub load_file {
    # Load the given file in memory.
    #
    # The file is loaded as an array containing all lines, stripped from
    # trailing / starting whitespaces.
    #
    # usage:
    #    my @content = load_file('my_file.whatever');

    my ($filename) = @_;

    my @content = ();
    open(my $inp, "<:encoding(UTF-8)", "$filename")
	or die "Could not open $filename, $! stopped";
    while (<$inp>) {
	chomp;
	s/^\s*//;
	s/\s*$//;
	push @content, lc "$_";
    }
    close($inp);

    return @content;
}


sub col_delete_keyword {
    # Delete the selected keyword from the file.
    #
    # This function will remove a single keyword from the given file, which
    # should be either a Fortran namelist or 'control.run'.
    #
    # usage:
    #    col_delete_keyword('my_file.whatever', 'keyword');

    my ($filename, $keyword) = @_;

    my @content = load_file($filename);
    my @new = ();
    foreach my $ele (@content) {
	if (! "$ele" eq "$keyword" ) {
	    push @new, "$ele";
	}
    }

    open(my $fh, '>:encoding(UTF-8)', "$filename")
	or die "Could not open $filename, $! stopped ";
    foreach (@content) {
	print $fh "$_\n";
    }
    close($fh);

}


sub col_change_keywords_nml {
    # Change keywords in the selected namelist-type file.
    #
    # The file is loaded, then parsed to find out if the given keywords
    # exist. If it is the case, the value is simply changed. If not, the key /
    # value pair is appended to the list.  When all key / value pairs have
    # been integrated, the results is written as a namelist with the same name
    # as in the original file, in $outfile.
    #
    # Usage :
    #    col_change_keywords_nml(
    #        infile, outfile, {key1 => val1, key2 => val2, ... }
    #    );

    my ($infile, $outfile, $changes) = @_;

    # Load the content of the file in memory
    my @content = load_file($infile);

    # Now modify it !
    my @new = ();
    foreach my $line (@content) {

	my @temp = split /=/, $line;

	foreach my $ele (@temp) {
	    $ele =~ s/^\s+//;
	    $ele =~ s/\s+$//;
	}

	my $found = 0;
	foreach my $key (keys(%$changes)) {
	    if ("$key" eq "$temp[0]") {
		# print STDOUT "FOUND $key !\n";
		push @new, "  $key = $changes->{$key}";
		$found = 1;
	    }
	}

	if ($found == 0) {
	    push @new, "  $line";
	}
    }

    # And print in the right file
    open(my $out, ">:encoding(UTF-8)", $outfile)
	or die "Could not open $outfile for write, $! stopped";
    foreach my $ll (@new) {
	print $out "$ll\n";
    }
    close($out);

    return 0;
}


sub col_ci_type {
    # Determine if CI computation is required, and which type.
    #
    # The function return either 0 (no CI computation), 1 (serial CI) or 2
    # (for parallel CI). The decision is made by looking at control.run. If
    # pciudg is found, then 2 is returned. Else, if ciudg, cigrad or nadcoupl
    # is there, 1 is returned. In other cases, 0 is returned.
    #
    # usage:
    #    my $ci_type = col_ci_type('control.run');

    my ($infile) = @_;
    my $pciudg = 0;
    my $ciudg = 0;
    my $nadcoupl = 0;
    my $mcscf = 0; # By default this is a MCSCF job

    my @content = load_file("$infile");
    foreach (@content) {
	if (/\bpciudg\b/) {
	    $pciudg = 1;
	}
	if ((/\bciudg\b/) or (/\bcigrad\b/)) {
	    $ciudg = 1;
	}
	if (/\bnadcoupl\b/) {
	    $nadcoupl = 1;
	}
	if (/\bmcscf\b/) {
	    $mcscf = 1;
	}
    }

    if ($pciudg) {
	return 2;
    }
    elsif ($ciudg) {
	return 1;
    }
    elsif (($nadcoupl) and ! ($mcscf)) {
	return 1;
    }
    else {
	return 0;
    }

}


sub col_get_mode {
    # Determine the type of calculation being run.
    #
    # The mode is determined by looking at the 'control.run' input file, and
    # reading which programs are being called.
    #
    # This mode can be either:
    # - 'ms': Multi-state (ciudgav or pciudgav)
    # - 'ss': Single-state (ciudg or pciudg)
    # - 'mcs': Multi-configurational gradients (mcscfgrad)
    # - 'mc': Simple multi-configurational
    #
    # usage: my $mode = col_get_mode('control.run');

    my ($input_file) = @_;
    my $mode;

    open(my $fh, '<:encoding(UTF-8)', "$input_file");
    while (<$fh>) {
	chomp;
	s/^\s+//;
	s/\s*$//;
	if (/ciudgav/) {
	    return "ms";
	} elsif (/ciudg/) {
	    return "ss";
	} elsif (/mcscfgrad/) {
	    return "mcs";
	}
    }
    close($fh);

    return "mc";
}


sub col_get_maximum_excitation_level {
    # Get maximum excitation level option from Columbus.
    #
    # This parameter is extracted from the CI input (either cidrtin or
    # cidrtmsin, depending on the current "mode").
    #
    # usage:
    #     my $mel = col_get_maximum_excitation_level($file);

    my ($file) = @_;
    my $mel = 0;

    open(my $fh, '<:encoding(UTF-8)', "$file")
	or die "Couldn't open $file, stopped ($!) at";
    while (<$fh>) {
	if (/maximum excitation level/) {
	    my @temp = split /\//;
	    $mel = $temp[0];
	    $mel =~ s/^\s+//;
	    $mel =~ s/\s+$//;
	}
    }
    close($fh);

    return $mel;
}


sub col_wrt_redtol {
    # Write a string of reduced tolerances.
    #
    # The string consists in a number of nstat statements, giving the
    # tolerance in the CI iteration for the corresponding state. The basis is
    # given by the $citol parameter. This value is always set for the state of
    # interest (i.e. $nstatdyn). The value for the other states depend on
    # $reduce_tol, $type_of_dynamics, and on the content of the transmomin
    # file.
    #
    # With $reduce_tol = 1, all tolerances will be set to $citol * 10. With
    # $reduce_tol = 2, the tolerance is set to $citol * 10 if $type = 1
    # (i.e. the state of interest is not "too far" from the surrounding
    # states) or if a coupling between this state and the state of interest is
    # to be computed. In all other cases, the tolerance is set to $citol * 1000.
    #
    # Usage:
    #    $tol = col_wrt_redtol(
    #        $citol, $type_of_dyn, $nstat, $nstatdyn, $reduce_tol
    #    );
    #
    # Ex. $tol=redtol('1E-4', 1,3,2,1): $tol = "1E-3, 1E-4, 1E-3"
    #     $tol=redtol('1E-4', 1,3,2,0): $tol = "1E-4, 1E-4, 1E-4"
    #     $tol=redtol('1E-4', 1,3,3,1): $tol = "1E-3, 1E-3, 1E-4"
    #     $tol=redtol('1E-4', 2,4,2,2): $tol = "1E-3, 1E-4, 1E-3, 1E-1" (kross = 0)
    #     $tol=redtol('1E-4', 2,4,2,2): $tol = "1E-3, 1E-4, 1E-3, 1E-3" (kross = 1)

    my ($citol, $type, $nst, $nstd, $reduce_tol) = @_;
    my $tol="";
    my $tol0;

    for (my $istring = 1 ; $istring <= $nst ; $istring++) {
	if ($istring == $nstd) {
	    $tol0 = $citol;
	}
	else {
	    if ($reduce_tol == 0) {
		$tol0=$citol;
	    }
	    elsif ($reduce_tol == 1) {
		$tol0= $citol * 10;
	    }
	    elsif ($reduce_tol == 2) {
		if ($type == 1) {
		    $tol0 = $citol * 10;
		}
		else {
		    $tol0 = $citol * 1000;
		}

		my @tm_st = col_read_transmomin("CI");
		foreach (@tm_st) {
		    if ($istring == $_) {
			# print STDOUT "$istring\n";
			$tol0 = $citol * 10;
		    }
		}
	    }
	}
	my $prt = sprintf("%.1e", $tol0);
	$tol = $tol."$tol0, ";
    }

    return $tol;
}


sub col_read_transmomin {
    # Read a Columbus transmomin file.
    #
    # The function returns an array containing the pairs of couplings that
    # will be computed, for the given level of computation ('CI' or 'MCSCF').
    #
    # usage:
    #    my @pairs = col_read_transmomin('transmomin');

    my ($infile, $level_calc) = @_;
    open(my $fh, '<:encoding(UTF-8)',"$infile")
	or warn "Cannot open transmomin!";

    my $i = 0;
    my @tm_st = ();

    while (<$fh>) {
	if (/$level_calc/) {
	    while (<$fh>) {
		chomp;
		$_ =~ s/^\s*//; # remove leading blanks
		$_ =~ s/\s*$//; # remove trailing blanks
		my ($d1,$s1,$d2,$s2) = split(/\s+/,$_);
		if ($d1 != 1) {
		    last;
		}
		push @tm_st, ($s1, $s2);
	    }
	}
    }
    close($fh);
    return \@tm_st;
}


sub col_check_inputs {
    # Check the Columbus inputs.
    #
    # The directory containing the different inputs is given as arguments, as
    # well as the number of states in the computation.
    #
    # usage:
    #

    my ($homedir, $compute_nac, $cirestart, $mocoef, $mode) = @_;

    my $err = 0;
    my $errmsg = '';

    # Check if Columbus is available
    my $colenv = $ENV{"COLUMBUS"};
    if (! defined $colenv) {
	$err = 1;
	$errmsg = "\$COLUMBUS environment not set !\n\n";
    }
    elsif (! -e "$colenv/runc") {
	$err = 1;
	$errmsg = "Cannot find Columbus program. Please check if \$COLUMBUS is properly set !\n";
	$errmsg = $errmsg."    Current value: $colenv\n\n";
    }

    # Check the presence of mocoef
    if (! -e "$homedir/mocoef") {
	$err = 1;
	$errmsg = $errmsg."No mocoef file could be found in $homedir\n\n";
    }

    # Check niter value
    my @control = load_file("$homedir/control.run");
    my $niter = col_get_niter(\@control);
    if ($niter != 1) {
	$err = 1;
	$errmsg = $errmsg."NITER must be equal to 1 in control.run (Columbus input)\n\n";
    }

    # Check if NAD computation is setup or not.
    if ($compute_nac == 1) {
	# First, check if cigrad is there
	my $cigrad = col_find_in_control(\@control, "cigrad");
	if ($cigrad == 0) {
	    $cigrad = col_find_in_control(\@control, "nadcoupl");
	}
	if ($cigrad == 0) {
	    $err = 1;
	    $errmsg = $errmsg."Analytical NAC computation requested, but 'nadcoupl' or 'cigrad'\n";
	    $errmsg = $errmsg."was not found in the input\n\n";
	}
    }

    # Check if cirestart can actually be performed
    if ($cirestart == 1) {
	if (($mode eq 'mc') or ($mode eq 'mcs')) {
	    $err = 1;
	    $errmsg = $errmsg."CI restart is requested, but no CI program has been defined\n\n";
	}
    }

    return ($err, $errmsg);
}

sub col_post_run {
    # Execute some action after Columbus has been run.
    #
    # The following actions will be done:
    # - Preparing cirestart for the next time-step if required (cirestart = 1);
    # - Saving mocoef if required (mocoef = 1 or higher)

    my ($cirestart, $mocoef) = @_;

    if ($cirestart == 1) {
	# Save the CI information from this step into WORK1.
	if (-e "WORK1") {
	    remove_tree("WORK1");
	}
	mkdir("WORK1");
	if (-e "WORK/civfl.drt1"){
	    copy("WORK/civfl.drt1","WORK1/civfl_restart.drt1")
		or die "Cannot copy civfl to civfl_restart.drt1 ($!), stopped at!";
	    copy("WORK/civout.drt1","WORK1/civout_restart.drt1")
		or die "Cannot copy civout to civout_restart.drt1 ($!), stopped at!";
	}
	else{
	    copy("WORK/civfl", "WORK1/civfl_restart")
		or die "Cannot copy civfl to civfl_restart.drt1 ($!), stopped at!";
	    copy("WORK/civout","WORK1/civout_restart")
		or die "Cannot copy civout to civout_restart.drt1 ($!), stopped at!";
	}
    }

    # if ($mocoef == 1) {
	# print STDOUT "Saving mocoef for next step ... ";
	# copy("MOCOEFS/mocoef_mc.sp", "mocoef.old")
	#     or die "Could not copy mocoef file ($!), stopped at";
	# print STDOUT "done\n";
    # }
}


sub col_copy_mocoef {
    # Save the current MOCOEF for next step.
}


sub col_ci_restart_1 {
    # Prepare restart of the CI computation.
    #
    my ($mode) = @_;

    my $ciu = ""; # ciudg input file
    my $cif = ""; # Input formatted vector file
    my $cio = ""; # header information on all calc roots
    my $cifbad = "";
    my $ciubad = "";
    my $ciobad = "";

    if ($mode eq 'ss') {
	$ciu = "ciudgin";
	$cif = "civfl_restart";
	$cio = "civout_restart";
	$cifbad = "civout_restart.drt1";
	$ciobad = "civout_restart.drt1";
    }
    elsif ($mode eq 'ms') {
	$ciu = "ciudgin.drt1";
	$cif = "civfl_restart.drt1";
	$cio = "civout_restart.drt1";
	$cifbad = "civfl_restart";
	$ciobad = "civout_restart";
    }


    if (-s "WORK1") {
	# Change ivmode to reuse previous civfl
	col_change_keywords_nml("$ciu", "$ciu", { ivmode => 4 });
	if (-e "WORK1/$cifbad") {
	    move("WORK1/$cifbad", "WORK1/$cif")
		or die "Cannot move ci vector files $! \n";
	    move("WORK1/$ciobad", "WORK1/$cio")
		or die "Cannot move ci vector files $! \n";
	}
	dirmove("WORK1", "WORK");
    }
}


sub col_ci_restart_2 {
    # Prepare the restart of the CI for the next step.
    #
    # The function copies the generated input vectors and output vectors from
    # WORK to WORK1. It takes as argument the Columbus $mode, to decide on the
    # names of the files to be copied.

    my ($mode) = @_;

    # Reinitialize WORK1 (directory that will contain the restart info).
    if (-e "WORK1") {
	remove_tree("WORK1");
    }
    mkdir("WORK1");

    if ($mode eq 'ms') {
	copy("WORK/civfl.drt1", "WORK1/civfl_restart.drt1");
	copy("WORK/civout.drt1", "WORK1/civout_restart.drt1");
    }
    elsif ($mode eq 'ss') {
	copy("WORK/civfl", "WORK1/civfl_restart");
	copy("WORK/civout", "WORK1/civout_restart");
    }
}


sub col_find_in_control {
    # Find a keyword in control.run.
    #
    # The function takes an array containing the lines of the given input as
    # argument (typically, the output of the ``load_file`` function). It
    # returns 1 if the keyword has been found, and 0 otherwise.
    #
    # usage:
    #   my $found = col_find_in_control(\@control, 'keyword');

    my ($control, $key) = @_;

    my $res = 0;
    foreach my $ele (@$control) {
	if ($ele =~ /\b($key)\b/i) {
	    $res = 1;
	}
    }

    return $res;
}


sub col_get_niter{
    # Find the number of iterations in control.run.
    #
    # The function takes an array containing the lines of the given input as
    # argument (typically, the output of the ``load_file`` function). It
    # returns the number of iterations if it has been found, and -1 otherwise.
    #
    # usage:
    #   my $niter = col_get_niter(\@control);

    my ($control) = @_;

    my $niter = -1;
    foreach my $ele (@$control) {
	if ($ele =~ /niter/) {
	    my @tmp = split /=/, $ele;
	    $niter = $tmp[1];
	}
    }

    return $niter;
}


sub col_prepare_fake_double {
    # Prepare the computation on overlapping molecules.
    #
    my $orig = "WORK/daltaoin";
    my $oldinp = "overlap.old/daltaoin_single.old";
    my $newinp = "overlap/daltaoin";

    if (! -e "$orig") {
	die "Couldn't find WORK/daltaoin, required for overlapping molecules computation !\n";
    }

    # These instructions are taken directly from legacy
    # run_cioverlap_col.pl
    system("head -3 $orig > $newinp");
    system("head -4 $orig |tail -1 |awk '{printf(\"%1s%4d %4d                  %13.2e\\n\",\$1,2*\$2,\$3,\$4);}' >>$newinp");
    system("tail -n +5 $oldinp >> $newinp");
    system("tail -n +5 $orig >> $newinp");

    # Now write the daltcomm file
    my $maxpri = undef;
    open(my $fh, '<:encoding(UTF-8)', 'WORK/daltcomm')
	or die "Could't open WORK/daltcomm ($!), stopped at";
    while (<$fh>) {
	if (/.MAXPRI/) {
	    $maxpri = <$fh>;
	}
    }
    close($fh);

    open($fh, '>:encoding(UTF-8)', 'overlap/daltcomm')
	or die "Could't open WORK/daltcomm ($!), stopped at";
    print $fh "**DALTONINPUT\n";
    print $fh ".INTEGRALS\n";
    print $fh ".PRINT\n";
    print $fh "   2\n";
    print $fh "**INTEGRALS\n";
    print $fh ".PRINT\n";
    print $fh "   2\n";
    print $fh ".NOSUP\n";
    print $fh ".NOTWO\n";
    print $fh "*READIN\n";
    if (defined $maxpri) {
	print $fh ".MAXPRI\n";
	print $fh "$maxpri";
    }
    print $fh "**END OF INPUTS\n";
    close($fh);

    copy("WORK/daltaoin", "overlap.old/daltaoin_single.old");
}


sub col_init_overlap {
    # Initialize a directory "overlap" for preparing the overlap computation.
    #
    # This routine is used at step 1 to prepare the files required for later.
    if (-e "overlap.old") {
	remove_tree("overlap.old");
    }

    if (-e "overlap") {
	dirmove("overlap", "overlap.old");
    }
    else {
	mkdir("overlap.old");
    }

    if (-e "WORK/daltaoin") {
	copy("WORK/daltaoin", "overlap.old/daltaoin_single.old");
    }
}

sub col_finish_cioverlap {

    # Start by copying come necessary files
    my @files = (
	'WORK/phases', 'WORK/slaterpermfile', 'WORK/consolidatefile',
	'WORK/slaterfile', 'WORK/daltoin', 'WORK/eivectors2',
	'WORK/lcao'
       );

    foreach my $ff (@files) {
	if (-e "$ff") {
	    copy("$ff", "cioverlap/");
	}
    }

}


sub col_prepare_cioverlap {
    my ($cmd) = @_;

    my $pwd = getcwd();

    # The first time this is run, there is only cioverlap.old in the working
    # directory, containing only slaterfile and eivectors2.
    # if (-e "cioverlap.old/cioverlap.out") {
    #     remove_tree("cioverlap.old")
    #         or die "Couldn't delete 'cioverlap.old' ($!), stopped";
    # }
    #
    # if (-e "cioverlap") {
    #     dirmove("cioverlap", "cioverlap.old")
    #         or die "Couldn't rename 'cioverlap/' ($!), stopped";
    # }
    #
    mkdir("cioverlap")
	or die "cioverlap/ already exists, check the computation !";

    copy('cioverlap.old/eivectors2', 'cioverlap/eivectors1')
	or die "Couldn't copy cioverlap.old/eivectors2 ($!), stopped";

    copy('cioverlap.old/slaterfile', 'cioverlap/')
	or die "Couldn't copy cioverlap.old/slaterfile ($!), stopped";

    if (-e 'cioverlap.old/slaterpermfile') {
	copy('cioverlap.old/slaterpermfile', 'cioverlap/')
	    or die "Couldn't copy cioverlap.old/slaterpermfile ($!), stopped";
    }
    if (-e 'cioverlap.old/excitlistfile') {
	symlink("$pwd/cioverlap.old/excitlistfile", "$pwd/cioverlap/excitlistfile")
	    or die "Couldn't symlink cioverlap.old/excitlistfile ($!), stopped";
    }

    copy('cioverlap.input', 'cioverlap/')
	or die "Couldn't copy cioverlap.input ($!), stopped";

    if (-e 'cioverlap.old/phases') {
	copy('cioverlap.old/phases', 'cioverlap/phases.old')
	    or die "Couldn't copy cioverlap.old/phases ($!), stopped";
    }

    copy('WORK/eivectors2', 'cioverlap/')
	or die "Couldn't copy WORK/eivectors2 ($!), stopped";


    # Required for NEXT overlap / cioverlap computation
    copy('WORK/consolidatefile', 'cioverlap/')
	or die "Couldn't copy WORK/consolidatefile ($!), stopped";
}



sub col_check_error{
    my ($err, $errmsg) = @_;
    if ($err != 0) {
	my $msg = "*" x 80;
	$msg = $msg."\n\tERROR: "."$errmsg \n";
	$msg = $msg."*" x 80;
	die "$errmsg";
    }
}

return 1;

__END__

=head1 NAME

NX::Columbus - Perl interaction layer between Columbus and Newton-X

=head1 SYNOPSIS

use NX::Columbus
...

=head1 DESCRIPTION

This package handles the interaction between the Columbus quantum chemistry
program and Newton-X.  It is intended to be used in the following cases:

=over

=item In the I<moldyn> script for checking the Columbus input files.

=item During dynamics for modifying / cleaning input files.

=head1 AUTHOR

Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
