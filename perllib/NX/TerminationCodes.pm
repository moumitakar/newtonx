# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::TerminationCodes;

use strict;
use warnings;
use diagnostics;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION     = '0.01';
    @ISA         = qw(Exporter);
    #Give a hoot don't pollute, do not export more than needed by default
    @EXPORT_OK = qw ();

  }

%NX::TerminationCodes::Hash = ();
my $msg;
my $code;

# Cioverlap
$code = '21'; # CIO_ERROR_PREPARE
$msg = " Preparation of cioverlap (prepare_cioverlap.pl) has encountered an error.";
$NX::TerminationCodes::Hash{$code} = "$msg";

$code = '22'; # CIO_ERROR_EXE
$msg = " The execution of cioverlap encountered an error.\n";
$msg = $msg." Please check cioverlap.input and cioverlap.out in cioverlap/";
$NX::TerminationCodes::Hash{$code} = "$msg";

# Cioverlap utilities
$code = '41'; # CIO_COL_CIPC
$msg = " The 'cipc' (or 'mycipc') program has encountered an error.\n";
$msg = $msg." Please check the relevant output file ('cipcls' or 'mycipcls') !";
$NX::TerminationCodes::Hash{$code} = "$msg";

$code = '42'; # CIO_COL_MCPC
$msg = " The 'mcpc' program has encountered an error.\n";
$msg = $msg." Please check the 'mcpcls.eivec' output file !";
$NX::TerminationCodes::Hash{$code} = "$msg";

$code = '43'; # CIO_COL_MISC
$msg = " The cioverlap / columbus interface has encountered an error.\n";
$NX::TerminationCodes::Hash{$code} = "$msg";

$code = '44'; # CIO_COL_CONSOLIDATE
$msg = " The 'civecconsolidate' program has encountered an error.\n";
$msg = $msg." Please check the 'civecconsolidate.out' output file !";
$NX::TerminationCodes::Hash{$code} = "$msg";

# Overlap
$code = '51'; # OVL_ERROR_PREPARE
$msg = " Preparation of overlap computation (prepare_fake_double.pl) failed.";
$NX::TerminationCodes::Hash{$code} = "$msg";

$code = '52'; # OVL_ERROR_RUN
$msg = " Execution of overlap computation failed.";
$NX::TerminationCodes::Hash{$code} = "$msg";

# Adaptive time step
$code = '61'; # ADT_ERROR_NTRAJ
$msg = " Too many subtrajectories required !";
$NX::TerminationCodes::Hash{$code} = "$msg";

$code = '62'; # ADT_ERROR_PERL
$msg = " The adaptive time step module encountered a problem\n";
$msg = $msg." in one of its helper Perl script...";
$NX::TerminationCodes::Hash{$code} = "$msg";

$code = '63'; # ADT_ERROR_SWITCHING
$msg = " Could not change directory !";
$NX::TerminationCodes::Hash{$code} = "$msg";


return 1;
