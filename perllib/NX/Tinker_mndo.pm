# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Tinker_mndo;

use strict;
use warnings;
use diagnostics;

use File::Copy qw( copy move );
use File::Path qw( remove_tree );
use File::Copy::Recursive qw( fcopy dircopy dirmove );

use Data::Dumper qw( Dumper );


BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( tmndo_init_copy tmndo_clean_dir tmndo_update_input
                  );
}

sub tmndo_init_copy {
    my ($job_folder, $basename) = @_;
    # print "Init Copy $job_folder, $basename\n";
    copy("$job_folder/$basename.key", "tinker_mndo.key")
        or die "Could not find $job_folder/$basename.key, stopped";
    copy("$job_folder/template.inp", "template.inp")
        or die "Could not find $job_folder/template.inp, stopped";
}
sub tmndo_update_input {
    # Update the input for QM computation.
    #
    my ($job_folder, $basename, $nstatdyn, $nstat) = @_;

    # print "Update Input $job_folder, $basename, $nstatdyn, $nstat\n";
    system("sed -i -e '/^\\s*mndocurrentstate.*/Id' tinker_mndo.key");
    system("echo mndocurrentstate $nstatdyn >> tinker_mndo.key");
    
    system("sed -i -e '/^\\s*mndostates.*/Id' tinker_mndo.key");
    if($nstat > 1 ){
        system("echo mndostates $nstat >> tinker_mndo.key");
    }

    open(my $fout, '>', "tinker_mndo.xyz")
        or die "Could not open tinker_mndo.xyz ($!), stopped";
    open(my $fin, "$job_folder/$basename.xyz")
        or die "Could not open $job_folder/$basename.xyz ($!), stopped";
    open(my $geom, "geom")
        or die "Could not open geom ($!), stopped";
    my @geom_lines = <$geom>;
    
    my $iline = 0;
    while (my $line = <$fin>){
        # Removes heading white chars
        $line =~  s/^\s+//;
        # ... and tailing newline.
        chomp($line);

        if ($iline == 0){
            print($fout "$line\n");
        }
        else{
            my $newcoord = $geom_lines[$iline-1];
            chomp($newcoord);
            $newcoord =~  s/^\s+//;
            my ($x, $y, $z) = split(/\s+/, $newcoord);
            
            my @tok = split(/\s+/, $line, 6);
            
            printf($fout "%5d %-4s %12.6f %12.6f %12.6f %s\n", 
                   $tok[0], $tok[1], 
                   $x*0.52918, $y*0.52918, $z*0.52918, $tok[5]);
        }
        $iline++;
    }

    close($fin);
    close($fout);
    close($geom);
}


sub tmndo_clean_dir {
    # Clean the working directory.
    #
    # Optionnally, the function also backs up the deleted/overwritten 
    # files in a debug directory.

    my ($debug, $step) = @_;
    # print "Clean Dir: $debug $step";

    my @backup_list = ('tinker_mndo.xyz', 'tinker_mndo.key',
                       'tmp_mndo.inp', 'tmp_mndo.out', 
                       'interface.dat', 'fort.15',
                       'tinker.out', 'fort.11', 
                       'imomap.dat', 'interface_nac.dat'
                      );

    # These files are not useful for the next step.
    my @delete_list = ('tinker_mndo.xyz', 
                       'tmp_mndo.inp', 'tmp_mndo.out', 
                       'interface.dat', 'fort.15',
                       'tinker.out', 'interface_nac.dat'
                      );
    
    # if ($debug >= 4) {
    #     my $glob_db_folder = "debug_tinker_mndo";
    #     my $db_folder = "debug_tinker_mndo/step_$step";
    #     if (not -e "$glob_db_folder"){
    #         mkdir("$glob_db_folder") 
    #              or die "Could not create $glob_db_folder ($!), stopped";
    #     }
    # 
    #     if ( -e "$db_folder"){
    #         my $db_folder = "debug_tinker_mndo/step_${step}_bis";
    #     }
    # 
    #     mkdir("$db_folder")
    #         or die "Couldn't created $db_folder ($!), stopped";
    #     foreach my $ff (@backup_list) {
    #         if (-e "$ff") {
    #             copy("$ff", "$db_folder/$ff")
    #                 or die "Couldn't backup $ff ($!), stopped";
    #         }
    #     }
    # }
    
    # This is needed when we whant to back in time to reduce the
    # time step without changing the initial guess.
    if (-e "fort.11") {
        copy("fort.11", "old_fort.11")
            or die "Couldn't backup fort.11 ($!), stopped";
    }
    
    foreach my $ff (@delete_list) {
        if (-e "$ff"){
            unlink("$ff");
        }
    }
}

return 1;
