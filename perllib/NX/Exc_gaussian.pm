# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::Exc_gaussian;

use strict;
use warnings;
use diagnostics;

use Cwd;
use File::Copy qw( copy );

use NX::Async qw( run_async );

BEGIN {
    use Exporter ();
    use vars qw( $VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS );
    $VERSION = '0.01';
    @ISA = qw( Exporter );
    @EXPORT_OK = qw( calc_excgau copy_excgau calc_exc_ovl );
}

sub copy_excgau {
   opendir my $dh, "JOB_AD"
      or die "Couldn't open JOB_AD/ ($!), stopped";
   my @files = grep {!/^\.\.?$/} readdir $dh;
   foreach my $ff (@files) {
     copy("JOB_AD/$ff", "$ff")
        or die "Couldn't copy JOB_AD/$ff ($!), stopped";
   }
   close($dh);
}


sub calc_excgau {
    my ($nchrom, $nproc) = @_;

    my $filename = "exc_gau";
    my (@gaussian_jobs);
    for (my $j = 1; $j <= $nchrom ; $j++){
    	push @gaussian_jobs , "$filename"."$j".".com";
    }

    my $egau = $ENV{"g16root"};
    my $program = "$egau/g16/g16";

    run_async(\@gaussian_jobs, $program, $nproc);

    system("$egau/g16/g16 exc_gau_MM.com");
}

sub calc_exc_ovl {
    my ($nchrom, $nproc) = @_;

    my $filename = "exc_gau";
    my (@gaussian_jobs);
    for (my $j = 1; $j <= $nchrom ; $j++){
    	push @gaussian_jobs , "$filename"."$j"."_ovl.com";
    }

    my $egau = $ENV{"g16root"};
    my $program = "$egau/g16/g16";

    run_async(\@gaussian_jobs, $program, $nproc);
}

return 1;
