# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::InputGenerator::Menu;

use strict;
use warnings;
use diagnostics;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION     = '0.01';
    @ISA         = qw(Exporter);
    #Give a hoot don't pollute, do not export more than needed by default
    @EXPORT      = qw();
    @EXPORT_OK   = qw();
    %EXPORT_TAGS = ();
  }

sub new {
  my ($proto, $title) = @_;
  my $class = ref($proto) || $proto;

  if (! defined $title) {
    $title = '';
  }

  my $self = bless {
		    _title => $title,
		    _items => [],
		    _exit_option => 1,
		   }, $class;

  return $self;
}


# Accessors
sub title {
  my ($self, $title) = @_;
  $self->{_title} = $title if defined $title;
  return $self->{_title};
}

sub exit_option {
  my ($self, $exit_option) = @_;
  $self->{_exit_option} = $exit_option if defined $exit_option;
  return $self->{_exit_option};
}

# Add list of items to menu
sub add {
  my ($self, $items) = @_;
  foreach (@$items) {
    push @{$self->{_items}}, $_;
  }
  return 0;
}

# Print menu
sub print {
  my $self = shift;

  my $spacer = ' ' x 4;
  for (;;) {
    system('clear');
    # $i counts the number of items printed on screen
    my $i = 1;

    # Start by printing a title, and then all options
    print STDOUT $self->{_title};
    print "\n\n\n";

    if (!@{$self->{_items}}) {
      return 0;
    }

    foreach (@{$self->{_items}}) {
      printf("%s%d. %s\n\n", $spacer, $i, $_->{_title});
      $i++;
    }

    # Now get user input
    if ($self->exit_option()) {
      print "$spacer$i. Exit\n\n";
    }
    print "Enter your choice: ";
    chomp (my $choice = <STDIN>);

    if ($choice =~ m/\d+/ && $choice >= 1 && $choice < $i) {
      @{$self->{_items}}[$choice-1]->{_action}->();
      # system('sleep 1');
    }

    elsif ($choice =~ m/\d+/ && $choice == $i) {
      exit 0;
    }

    else {
      print "Invalid input !\n\n";
      system('sleep 1');
    }
  }
}

1;

