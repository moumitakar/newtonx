# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::InputGenerator::SetValues;

use strict;
use warnings;
use diagnostics;
use YAML::Tiny;

use NX::Tools qw( prog_name );
use Sys::Hostname qw( hostname );

use Data::Dumper;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION     = '0.01';
    @ISA         = qw(Exporter);
    #Give a hoot don't pollute, do not export more than needed by default
    @EXPORT_OK = qw (set_number_of_atoms
		     define_threshold
		     set_vdoth
		     set_integrator
		     set_ms
		     set_adjmom
		     set_never_state
		     set_cio_options
		     set_cisc_options
		     set_progname
		     set_method
		     set_npre
		     set_nstart
		     h5_set_user_email
		     h5_set_user_name
		     set_check_overlap
		     set_dc_method
		     set_ovl_prog
		     set_generate_wf
		     set_read_ovl_matrix
		     set_tinker_mndo_interface_path
		     set_tinker_g16_interface_path
		     set_anmod
		     set_grad_lvl
		     set_save_cwd
		   );
  }

%NX::InputGenerator::safe_set = (
				 'set_number_of_atoms' => \&set_number_of_atoms,
				 'define_threshold' => \&define_threshold,
				 'set_vdoth' => \&set_vdoth,
				 'set_integrator' => \&set_integrator,
				 'set_ms' => \&set_ms,
				 'set_adjmom' => \&set_adjmom,
				 'set_never_state' => \&set_never_state,
				 'set_cio_options' => \&set_cio_options,
				 'set_cisc_options' => \&set_cisc_options,
				 'set_progname' => \&set_progname,
				 'set_method' => \&set_method,
				 'set_npre' => \&set_npre,
				 'set_nstart' => \&set_nstart,
				 'h5_set_user_email' => \&h5_set_user_email,
				 'h5_set_user_name' => \&h5_set_user_name,
				 'set_check_overlap' => \&set_check_overlap,
				 'set_derivative_couplings' => \&set_derivative_couplings,
				 'set_ovl_prog' => \&set_ovl_prog,
				 'set_generate_wf' => \&set_generate_wf,
				 'set_read_ovl_matrix' => \&set_read_ovl_matrix,
				 'set_tinker_mndo_interface_path' => \&set_tinker_mndo_interface_path,
				 'set_tinker_g16_interface_path' => \&set_tinker_g16_interface_path,
				 'set_anmod' => \&set_anmod,
                                 'set_grad_lvl' => \&set_grad_lvl,
                                 'set_save_cwd' => \&set_save_cwd,
				);

sub load_dc_configuration {
    # Load the default configuration for derivative couplings.
    #
    # We read the content of $NXHOME/data/derivative_couplings.yml and
    # return the default configuration for the selected code.
    my ($progname) = @_;

    my $dc_file = $ENV{NXHOME}."/data/derivative_couplings.yml";
    if (! -f $dc_file) {
	$dc_file = $ENV{NXHOME}."/share/newtonx/data/derivative_couplings.yml";
    }
    my $qm_dc_config = YAML::Tiny->read("$dc_file")
	or die "Couldn't process '$dc_file' ($!), stopped";

    my $res = $qm_dc_config->[0]->{$progname};
    return $res;
}


sub get_section_dc_conf {
    # Get the section corresponding toe the right method
    # from the derivative_couplings configuration.

    my ($dc_conf, $method) = @_;

    my $sec_to_get = "";
    if (! defined $method) { $sec_to_get = 'default' ;}
    elsif (! defined $dc_conf->{$method}) { $sec_to_get = 'default' ;}
    else { $sec_to_get = "$method" ;}

    return $dc_conf->{$sec_to_get};
}

sub set_number_of_atoms {
  my ($config) = @_;

  # If geom is present in the current of the parent directory,
  # count the number of lines.
  # Else, if either final_output or final_output.old is found,
  # extract the number of atoms from this file.
  # If no file is found, return a warning message.
  my $Natoms = $config->value('nxconfig', 'nat');

  my $final_out = undef;
  if (-s 'final_output') {
    $final_out = 'final_output';
  } elsif (-s 'final_output.old') {
    $final_out = 'final_output.old';
  }

  my $geom = undef;
  if (-s 'geom.orig') {
      $geom = 'geom.orig';
  } elsif (-s 'geom') {
      $geom = 'geom';
  } elsif (-s '../geom.orig') {
      $geom = '../geom.orig';
  } elsif (-s '../geom') {
      $geom = '../geom';
  }

  # Check if 'geom' file is present
  # Interestingly, using a while loop here breaks the
  # parameter list from nxinp_set_parameter, resulting in
  # 'nat' being undef at the end of the function...
  # Why ???
  # ANSWER: This is because $_ is a global variable. In the
  # foreach loop in nxinp.pl (see basic_menu function), it is
  # designed so that it iterates with the $_ default variable. So,
  # $_ KEEPS the value for ALL steps inside the foreach loop. Now,
  # and most importantly, 'while' will ACT on $_, so that at the end
  # of a loop, the value of $_ will be the last read by 'while',
  # most probably an undef... So we have to design a block
  # where 'while' uses a LOCAL version of $_.
  if (defined $geom) {
    $Natoms = 0;
    open(my $fh, '<:encoding(UTF-8)', $geom)
      or die "Couldn't open '$geom' $!, stopped";
    {
      local $_;
      while (<$fh>) {
	if (! /^\s*$/) {
	  $Natoms++;
	}
      }
    }
    close $fh;
  }
  # print "\$_ is now $_\n";
  elsif (defined $final_out) {
    $Natoms = 0;
    open(my $fh, '<:encoding(UTF-8)', $final_out)
      or die "Couldn't open '$final_out' $!, stopped";
    {
      local $_;
      COUNT_LINES: while (<$fh>) {
	  if (/geometry in/i) {
	    while (<$fh>) {
	      $Natoms++;
	      if (/velocity in/i) {
		$Natoms--;
		last COUNT_LINES;
	      }
	    }
	  }
	}
    }
    close $fh;
  }
  # If none of the above was the case, return a warning
  # my $def = $config->value('dynamics', 'nat');
  else {
    print STDOUT "WARNING: could not determine nat from 'geom' or 'final_output' files !!\n";
    print STDOUT "         I will use value from current default: $Natoms\n";
  }

  return $Natoms;
}

sub define_threshold {
  my ($config) = @_;
  my $thres = 0;
  my @default_is_100 = qw / analytical columbus turbomole 
		       gaussian tinker_mndo tinker_g16mmp
		       mopac exc_gaussian exc_mopac /;

  my ($name, $dum) = prog_name($config->value('nxconfig', 'prog'));

  if (grep /$name/, @default_is_100) {
    $thres = 100;
    return $thres;
  } else {
    $thres = 0;
    return $thres;
  }
}


sub set_vdoth {
  my ($config) = @_;
  my ($name, $dum) = prog_name($config->value('nxconfig', 'prog'));

  if ($config->value('nxconfig', 'dc_method') == 1) {
      return 0;
  } else {
      return 1;
  }

  # Based on program name
  # if ($name eq "g09" || $name eq "turbomole" ) {
  #   return 1;
  # }
  #
  # # Based on presence of JOB_AD
  # if ((-e 'JOB_AD') and (! -e 'JOB_NAD')) {
  #     return 1;
  # }
  #
  # return 0;
}

sub set_integrator {
  my ($config) = @_;
  my $vdoth = $config->value('sh', 'vdoth');
  if ($vdoth == -1) {
    return 2;
  }

  return 1;
}


sub set_ms {
    my ($config) = @_;
    my $integrator = $config->value('sh', 'integrator');

    if ($integrator == 2) {
	return 1;

    } else {
	return 20;
    }
  # my $vdoth = $config->value('sh', 'vdoth');
  # if ($vdoth == -1) {
  #   return 1;
  # }
  #
  # return 20;
}


sub set_adjmom {
  my ($config) = @_;

  my $dc_method = $config->value('nxconfig', 'dc_method');
  if ($dc_method != 1) {
    return 1;
  }

  return 2;
}


sub set_never_state {
  my ($config) = @_;
  my $dc_coupling = $config->value('nxconfig', 'dc_method');

  if ($dc_coupling == 1) {
    return 0;
  }

  return 1;
}


sub set_cio_options {
  my ($config) = @_;
  my ($name, $method) = prog_name($config->value('nxconfig', 'prog'));
  
  my $dc_config = load_dc_configuration($name);
  my $dc = get_section_dc_conf($dc_config, $name, $method);

  if (ref($dc->{'cioverlap'}) eq 'HASH') {
      my $cio_option = $dc->{'cioverlap'}->{'cio_options'};
      return $cio_option;
  }
  else {
      return "\"UNDEF\"";
  }
}

sub set_cisc_options {
  my ($config) = @_;
  my ($name, $method) = prog_name($config->value('nxconfig', 'prog'));

  my $dc_config = load_dc_configuration($name);
  my $dc = get_section_dc_conf($dc_config, $name, $method);

  if (ref($dc->{'cioverlap'}) eq 'HASH') {
      my $cisc_option = $dc->{'cioverlap'}->{'cisc_options'};
      return $cisc_option;
  }
  else {
      return "\"UNDEF\"";
  }
}


sub set_progname {
    my ($config) = @_;
    my ($progname, $dum) = prog_name( $config->value('nxconfig', 'prog') );

    return "\"$progname\"";
}


sub set_method {
    my ($config) = @_;
    my ($dum, $method) = prog_name( $config->value('nxconfig', 'prog') );

    return "\"$method\"";
}


sub set_npre {
    my ($config) = @_;
    my $nstat = $config->value('nxconfig', 'nstat');

    my $res = $nstat - 1;
    return $res;
}


sub set_nstart {
    my ($config) = @_;
    my $npre = $config->value('turbomole', 'npre');

    return $npre;
}


sub h5_set_user_name {
    my ($config) = @_;

    # The following function gets the line from /etc/passwd corresponding
    # to the current user. The 6th field (at least on Debian) seems to be the
    # full user name (GCOS field). We just need the part before the comas though,
    # so we split the result to get just the user name.
    my $user_field = (getpwuid($<))[6];
    my @tmp = split ',', $user_field;

    return $tmp[0];
}


sub h5_set_user_email {
    my ($config) = @_;
    my $username = $ENV{USER};
    my $host = hostname();

    my $res = "$username"."@"."$host";

    return $res;
}

sub set_check_overlap {
    my ($config) = @_;
    my ($prog, $dum) = prog_name( $config->value('nxconfig', 'prog') );

    my $res = 0;
    if ($prog eq "columbus") {
	$res = 1;
    }

    return $res;
}

sub set_derivative_couplings {
    my ($config) = @_;
    my ($prog, $method) = prog_name( $config->value('nxconfig', 'prog') );

    # If adiabatic dynamics is requested, there is no need for derivative
    # couplings to be computed...
    if ($config->value('nxconfig', 'thres') == 0) {
	return 0;
    }

    my $dc_file = $ENV{NXHOME}."/data/derivative_couplings.yml";
    if (! -f $dc_file) {
	$dc_file = $ENV{NXHOME}."/share/newtonx/data/derivative_couplings.yml";
    }
    my $dc_config = load_dc_configuration($prog);
    my $dc_method = get_section_dc_conf($dc_config, $method);

    my $def = $dc_method->{'coupling_method'};
    my $cur = $config->value('nxconfig', 'dc_method');

    if (defined $cur) {
	# if ($def != $cur) {
	    return $cur;
	# }
    } else {
	return $def;
    }
}

sub set_ovl_prog {
    my ($config) = @_;
    my ($prog, $method) = prog_name( $config->value('nxconfig', 'prog') );


    my $dc_file = $ENV{NXHOME}."/data/derivative_couplings.yml";
    if (! -f $dc_file) {
	$dc_file = $ENV{NXHOME}."/share/newtonx/data/derivative_couplings.yml";
    }
    my $dc_config = load_dc_configuration($prog);
    my $dc_method = get_section_dc_conf($dc_config, $method);

    return $dc_method->{'ovl_prog'};
}


sub set_generate_wf {
    my ($config) = @_;

    my ($prog, $method) = prog_name( $config->value('nxconfig', 'prog') );

    my $dc_file = $ENV{NXHOME}."/data/derivative_couplings.yml";
    if (! -f $dc_file) {
	$dc_file = $ENV{NXHOME}."/share/newtonx/data/derivative_couplings.yml";
    }
    my $dc_config = load_dc_configuration($prog);
    my $dc_method = get_section_dc_conf($dc_config, $method);

    if (ref($dc_method->{'cioverlap'}) eq 'HASH') {
	return $dc_method->{'cioverlap'}->{'generate_wf'};
    }
    else {
	return 0;
    }
}

sub set_read_ovl_matrix {
    my ($config) = @_;

    my ($prog, $method) = prog_name( $config->value('nxconfig', 'prog') );

    my $dc_file = $ENV{NXHOME}."/data/derivative_couplings.yml";
    if (! -f $dc_file) {
	$dc_file = $ENV{NXHOME}."/share/newtonx/data/derivative_couplings.yml";
    }
    my $dc_config = load_dc_configuration($prog);
    my $dc_method = get_section_dc_conf($dc_config, $method);

    return $dc_method->{'read_ovl_matrix'};
}


sub set_tinker_mndo_interface_path {
    my ($config) = @_;

    my $env = $ENV{TINKER_MNDO};

    if (defined $env) {
	return "$env"."/gradinterf.x";
    } else {
	return "gradinterf.x";
    }
}


sub set_tinker_g16_interface_path {
    my ($config) = @_;

    my $env = $ENV{TINKER_G16};

    if (defined $env) {
	return "$env"."/gradinterf.x";
    } else {
	return "gradinterf.x";
    }
}


sub set_anmod {
    my ($config) = @_;

    my ($dum, $method) = prog_name( $config->value('nxconfig', 'prog') );

    if ($method eq 'sbh') {
	return 2;
    } elsif ($method eq 'recohmodel') {
	return 3;
    } elsif ($method eq 'onedim_model') {
	return 1;
    }

    return '';
}


sub set_grad_lvl {
    my ($config) = @_;

    my ($dum, $method) = prog_name($config->value('nxconfig', 'prog'));

    if ($method eq 'mrci') {
	return 2;
    } else {
	return 1;
    }

}


sub set_save_cwd {
    my ($config) = @_;

    my $lvprt = $config->value('nxconfig', 'lvprt');

    if ($lvprt > 3) {
	return 10;
    } else {
	return -1;
    }
}

1;
