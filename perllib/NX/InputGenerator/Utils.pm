# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::InputGenerator::Utils;

use strict;
use warnings;
use diagnostics;

use NX::Geometry qw( nx2xyz xyz2zmat read_z_matrix write_internal_coordinates );
use NX::Tools qw( prog_name );

use Data::Dumper qw( Dumper );

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION     = '0.01';
    @ISA         = qw(Exporter);
    #Give a hoot don't pollute, do not export more than needed by default
    @EXPORT_OK   = qw( nxinp_set_parameter ask_y_or_n generate_internal_coords
		       parse_internal_coords );
}


sub nxinp_set_parameter {
  my ($config, $section, $parameter) = @_;
  my $description = $config->description($section, $parameter);
  my $extended_description = $config->extended_description($section, $parameter);
  my $current = $config->value($section, $parameter);
  my $options = $config->options($section, $parameter);
  my $unit = $config->unit($section, $parameter);

  # Define the unit (for printing)
  if (! defined $unit) {
    $unit = '';
  } else {
    $unit = " (in $unit)";
  }

  # Check if a function to guess the default was provided.
  # This function must return the default value !
  if (defined $config->[0]->{$section}->{$parameter}->{func}) {
    my $func = $config->[0]->{$section}->{$parameter}->{func};
    require NX::InputGenerator::SetValues;
    my $function = $NX::InputGenerator::safe_set{$func};

    if (defined $function) {
      $current = $function->($config);
    } else {
      print "Cannot use function $func (not defined as safe !!)\n";
      print "Reverting to global default\n";
      print "!!! PLEASE CHECK THIS VALUE CAREFULLY !!!";
    }
  }

  # This function will be used to check if the input is valid or not
  # usually based on other parts of the configuration.
  my $check_func = undef;
  if (defined $config->[0]->{$section}->{$parameter}->{check}) {
    $check_func = $config->[0]->{$section}->{$parameter}->{check};
    require NX::InputGenerator::CheckValues;
    $check_func = $NX::InputGenerator::safe_checks{$check_func};
  }

  # See if the user's input should match a certain pattern
  my $regex = undef;
  if (defined $config->[0]->{$section}->{$parameter}->{regex}) {
    $regex = $config->[0]->{$section}->{$parameter}->{regex};
  }

  # See if information where provided to give the user a hint about
  # what kind of input is expected
  my $must_be = undef;
  if (defined $config->[0]->{$section}->{$parameter}->{must_be}) {
    $must_be = '(must be ';
    $must_be = $must_be.$config->[0]->{$section}->{$parameter}->{must_be};
    $must_be = $must_be.') ';
  }

  # See if a set of options has been provided
  my @available_choices = ();
  foreach (@$options) {
    my @temp = split / - /;
    $temp[0] =~ s/^\s+|\s+$//g;
    push @available_choices, $temp[0];
  }

  # Define what will be printed on screen
  my $result = "$parameter: $description\n";
  foreach (@$extended_description) {
    $result = $result."     $_\n";
  }
  foreach (@$options) {
    $result = $result."     $_\n";
  }
  $result = $result."The current value of $parameter is$unit: $current\n";
  if ("$current" eq 'NULL') {
      $result = $result." !!! The current 'NULL' value indicates that this is probably not possible !!!\n";
      $result = $result." !!! Check the documentation, or proceed at your own risk !!!\n";
  }
  $result = $result."Enter the new value of $parameter$unit: ";

  # Now get user's input, and set the value accordingly.
  # Check the input if 'regex' field exists
  my $ok = 0;
  my $new_value = '';
  my $check = 0;
  while ($ok == 0) {
    print STDOUT "\n$result";
    chomp (my $input = <STDIN>);
    # 1. The user just enters 'Enter': the new value is the current one, no modification
    if ($input eq '') {
      $new_value = $current;
      $check = 1;
    }
    # 2. Whatever else is entered by the user
    else {
      # 2.a A specific set of options are available
      if (@available_choices) {
	if (! grep /$input/, @available_choices) {
	  print STDOUT "Invalid choice: must be in the provided list of options !!\n";
	} else {
	  $new_value = $input;
	  $check = 1;
	}
      }
      # 2.b No regex has been defined: accept input as new value
      elsif (! defined $regex) {
	$new_value = $input;
	$check = 1;
      }
      # 2.c A regex is defined: check if the input corresponds
      else {
	# 2.c.1 the input matches the regex: accept it as new value
	if ($input =~ m/$regex/) {
	  $new_value = $input;
	  $check = 1;
	}
	# 2.c.2 the input does not match: Ask for something else !
	else {
	  print STDOUT "Cannot process $must_be!!\n";
	}
      }
    }

    # If a valid input was entered and a function check is required, do it !
    if ($check == 1) {
      if (defined $check_func) {

	my $res = $check_func->($config, $new_value);
	if ($res == 1) {
	  $config->value($section, $parameter, $new_value);
	  $ok = 1;
	}
      } else {
	$config->value($section, $parameter, $new_value);
	$ok = 1;
      }
    }
  }

  return 0;
}

sub ask_y_or_n {
  my ($question) = @_;
  print STDOUT "$question (y or n) ?  ";

  my $dummy = 1;
  my $answer = '';
  while ($dummy == 1) {
    chomp ($answer = <STDIN>);
    if ($answer eq 'n') {
      print STDOUT "\n";
      $dummy = 0;
      $answer = 0;
    } elsif ($answer eq 'y') {
      print STDOUT "\n";
      $dummy = 0;
      $answer = 1;
    } else {
      print STDOUT "Please answer with \'y\' or \'n\'  ";
    }
  }
  return $answer;
}

sub generate_internal_coords {

  my ($config) = @_;
  my $Natoms = $config->value('nxconfig', 'nat');

  my $original_geom = 'geom';
  if (-s 'geom.orig') {
      $original_geom = 'geom.orig';
  } elsif (-s '../geom.orig') {
      $original_geom = '../geom.orig';
  } elsif (-s '../geom') {
      $original_geom = '../geom';
  }
  if (! -s $original_geom) {
    if (! -s 'final_output') {
      my $error = " Cannot find \"geom\" or \"final_output\" files. One of those is necessary\n";
      $error = $error." to generate the internal coordinates file. Internal coordinates will not\n";
      $error = $error." be generated.";
      die $error."$!";
    } else {
      extract_geom_from_final_output();
    }
  }

  nx2xyz("$original_geom", 'geom.tmp.xyz');
  xyz2zmat('geom.tmp.xyz', 'geom.tmp.zmat');
  my (@stretch, @angles, @torsions) = read_z_matrix($Natoms, 'geom.tmp.zmat');
  write_internal_coordinates(10, @stretch, @angles, @torsions);

  # Remove leftover files
  unlink('geom.tmp.xyz');
  unlink('geom.tmp.zmat');
}


sub parse_internal_coords {

  my $geom = 'geom';
  my $intcfl = 'intcfl';

  my @atom_list = ();
  my @stretch = ();
  my @temp = ();

  my @ah = ();
  my @bc = ();

  # Get the list of atoms from geom
  open(my $fh, '<:encoding(UTF-8)', "$geom") or
    die "Could not open $geom, stopped";
  while (<$fh>) {
    chomp;
    s/^\s+//;
    @temp = split /\s+/;
    push @atom_list, "$temp[0]";
  }
  close($fh);

  # Now get the list of stretch
  open($fh, '<:encoding(UTF-8)', "$intcfl") or
    die "Could not open $geom, stopped";
  while (<$fh>) {
    if ("$_" =~ m/STRE/) {
      chomp;
      s/^\s+//;
      @temp = split /\s+/;
      push @stretch, [$temp[3], $temp[4]];
    }
  }
  close($fh);

  my $i = 0;
  foreach my $bond (@stretch) {

    my $at1 = int(@$bond[0]-1);
    my $at2 = int(@$bond[1]-1);

    if (("$atom_list[$at1]" eq 'H') or
	("$atom_list[$at2]" eq 'H')) {
      # +1 for Fortran !!!
      push @ah, ($at1+1, $at2+1);
    }

    else {
     push @bc, ($at1+1, $at2+1);
    }

    $i += 1;
  }

  return (\@ah, \@bc);

}

1;
