# Copyright (C) 2022  Light and Molecules Group

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

package NX::InputGenerator::CheckValues;

use strict;
use warnings;
use diagnostics;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    $VERSION     = '0.01';
    @ISA         = qw(Exporter);
    #Give a hoot don't pollute, do not export more than needed by default
    @EXPORT_OK = qw ( check_nstatdyn check_ms check_lts );
  }

%NX::InputGenerator::safe_checks = (
				    'check_nstatdyn' => \&check_nstatdyn,
				    'check_lts' => \&check_lts,
				    'check_ms' => \&check_ms,
				   );

sub check_nstatdyn {
  my ($config, $test_value) = @_;
  my $nstat = $config->value('nxconfig', 'nstat');
  if ($test_value > $nstat) {
    print STDOUT "\nERROR: Invalid choice for nstatdyn, must be inferior to nstat !!\n";
    return 0;
  } else {
    return 1;
  }
}


sub check_lts {
  my ($config, $test_value) = @_;
  my $kts = $config->value('thermostat', 'kts');
  if ($test_value <= $kts) {
    print STDOUT "\nERROR: Invalid choice for lts, must be superior to kts !!\n";
    return 0;
  } else {
    return 1;
  }
}


sub check_ms {
  my ($config, $test_value) = @_;
  my $integrator = $config->value('sh', 'integrator');
  # if ($vdoth == -1) {
  #   if ($test_value != 1) {
  #     print STDOUT "\nERROR: Invalid choice for ms, must be 1 with vdoth = -1 !!\n";
  #   return 0;
  #   }
  #   else {
  #     return 1;
  #   }
  # } else {
  #   return 1;
  # }
}

1;
