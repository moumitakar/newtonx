#!/bin/bash

subfiles='subfiles.mk'

# Perl directories
echo 'perllib_dir = \' > subfiles.mk
find perllib -type d -print | sed 's/^/  /;$q;s/$/ \\/' >> ${subfiles}
echo '' >> ${subfiles}

# Perl library files
echo 'perllib_files = \' >> ${subfiles}
find perllib \( -name '*.pm' \) -type f -print | sed 's/^/  /;$q;s/$/ \\/' >> ${subfiles}
echo '' >> ${subfiles}

# Examples directory
echo 'examples_dir = \' >> ${subfiles}
find examples -type d -print | sed 's/^/  /;$q;s/$/ \\/' >> ${subfiles}
echo '' >> ${subfiles}

# Examples files
echo 'examples_files = \' >> ${subfiles}
find examples -type f -print | sed 's/^/  /;$q;s/$/ \\/' >> ${subfiles}
echo '' >> ${subfiles}

# Utilities
echo 'utils_files = \' >> ${subfiles}
find utils -type f -print | sed 's/^/  /;$q;s/$/ \\/' >> ${subfiles}
echo '' >> ${subfiles}

# Misc data
echo 'misc_data_files = \' >> ${subfiles}
find data -type f -print | sed 's/^/  /;$q;s/$/ \\/' >> ${subfiles}
echo '' >> ${subfiles}

# # cioverlap
# echo 'cioverlap_dir = \' >> ${subfiles}
# find $CIOVERLAP_SRC -type d -print | sed 's/^/  /;$q;s/$/ \\/' >> ${subfiles}
# echo '' >> ${subfiles}
# 
# echo 'cioverlap_files = \' >> ${subfiles}
# find $CIOVERLAP_SRC -type f -print | sed 's/^/  /;$q;s/$/ \\/' >> ${subfiles}
# echo '' >> ${subfiles}

sed -i '/#/d' ${subfiles}

# cio_path=$CIOVERLAP_SRC
# 
# sed -i "s#$cio_path#cioverlap/#" ${subfiles}
