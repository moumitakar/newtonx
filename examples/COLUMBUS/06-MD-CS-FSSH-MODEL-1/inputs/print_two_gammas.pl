#!/usr/bin/env perl 

my ($g1, $g2, $file);
$g1 = 0.005;
$g2 = 0.010;
$file = 'gamma';

open(GF,">$file") || die "Cannot open $file to write.";
  print GF "$g1 \n";
  print GF "$g2 \n";
close(GF);
