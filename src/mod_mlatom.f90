! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_mlatom
  use mod_configuration, only: nx_config_t
  use mod_constants, only: MAX_STR_SIZE
  use mod_input_parser, only: parser_t, set => set_config
  use mod_interface, only: rm
  use mod_logger, only: print_conf_ele, call_external, check_error
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_tools, only: to_str
  use mod_trajectory, only: nx_traj_t
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: mlat_setup, mlat_print
  public :: mlat_run, mlat_read_outputs

contains

  subroutine mlat_setup(nx_qm, parser, config)
    type(nx_qm_t), intent(inout) :: nx_qm
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: config

    character(len=MAX_STR_SIZE) :: buf, env, control
    integer :: ierr
    
    call set(parser, 'mlatom', nx_qm%mlat_use_nx_interface, 'use_nx_interface')
    call get_environment_variable("MLatom", env)
    if (nx_qm%mlat_use_nx_interface) then
       nx_qm%mlat_exe = trim(env)//'/MLatom.py'
    else
       nx_qm%mlat_exe = trim(env)//'/MLatomF'
    end if
    
    call set(parser, 'mlatom', nx_qm%mlat_exe, 'mlatom_exe')

  end subroutine mlat_setup


  subroutine mlat_print(nx_qm, out)
    !! Print the MLatom configuration.
    !!
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM configuration.
    integer, intent(in), optional :: out

    integer :: output
    character(len=MAX_STR_SIZE) :: env

    output = stdout
    if (present(out)) output = out

    call get_environment_variable("MLatom", env)
    call print_conf_ele(trim(env), 'MLatom path', unit=output)
    call print_conf_ele(nx_qm%mlat_exe, 'MLatom EXE', unit=output)
    call print_conf_ele(&
         & nx_qm%mlat_use_nx_interface, 'Using new NX interface', unit=output)
  end subroutine mlat_print


  subroutine mlat_run(nx_qm, conf, traj)
    type(nx_qm_t), intent(in) :: nx_qm
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj

    integer :: i, ierr
    character(len=MAX_STR_SIZE) :: cmd

    if (traj%step == conf%init_step) then
       ! Copy the input files at setup time
       if (nx_qm%mlat_use_nx_interface) then
          call execute_command_line('cp -f '//trim(nx_qm%job_folder)//'/* .', exitstat=ierr)
          call check_error(ierr, 101, 'Problem in copying model files', system=.true.)
       end if
    end if

    if (nx_qm%mlat_use_nx_interface) then
       write(cmd, '(A)') &
            & trim(nx_qm%mlat_exe)//' callNXinterface useMLforNX'//&
            & ' natoms='//to_str(conf%nat)//' nstat='//to_str(conf%nstat)//&
            & ' nstatdyn='//to_str(traj%nstatdyn)
       call call_external(cmd, ierr, outfile='MLatom.out')

    else
       do i=1, conf%nstat
          ierr = 0
          write(cmd, '(A)') &
               & trim(nx_qm%mlat_exe)//' useMLmodel XYZfile=xyz.dat '//&
               & 'MLmodelIn='//trim(nx_qm%job_folder)//'/mlmod_engrad'//to_str(i)//'.unf '//&
               & 'eqXYZfileIn='//trim(nx_qm%job_folder)//'/eq.xyz '//&
               & 'YestFile=yest_en'//to_str(i)//'.dat '//&
               & 'YgradXYZestFile=yest_grad'//to_str(i)//'.dat'
          call call_external(cmd, ierr, outfile='MLatom'//to_str(i)//'.out')
       end do
    end if
  end subroutine mlat_run


  subroutine mlat_read_outputs(nx_qm, traj, conf, qminfo)
    type(nx_qm_t), intent(inout) :: nx_qm
    !! General information about the QM job.
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf
    type(nx_qminfo_t), intent(inout) :: qminfo

    integer :: i, u, ierr, j, k

    do i=1, conf%nstat
       open(newunit=u, file='yest_en'//to_str(i)//'.dat', action='read')
       read(u, *) qminfo%repot(i)
       close(u)

       open(newunit=u, file='yest_grad'//to_str(i)//'.dat', action='read')
       ! The first two lines are useless for us
       read(u, *)
       read(u, *)
       do j=1, conf%nat
          read(u, *) (qminfo%rgrad(i, k, j), k=1, 3)
       end do
       close(u)
    end do
  end subroutine mlat_read_outputs
  
  
end module mod_mlatom
