! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_zpecorrect
  !! author: Saikat Mukherjee <saikat.mukherjee@univ-amu.fr>
  !! date: 26-01-2021
  !!
  use mod_kinds, only: dp
  use mod_constants, only: &
       & MAX_STR_SIZE, timeunit
  use mod_tools, only: &
       & norm, scalar_product, compute_ekin
  use mod_trajectory, only: nx_traj_t
  use mod_configuration, only: nx_config_t
  use mod_logger, only: &
       & nx_log, &
       & LOG_DEBUG, LOG_INFO, LOG_WARN, &
       & print_conf_ele
  use mod_input_parser, only: &
       & parser_t, set => set_config
  implicit none

  private

  public :: nx_zpe_t
  public :: zpecorrect_model1

  type nx_zpe_t
     integer :: kmodel = 1
       !! ZPE correction scheme (Input).
     integer :: kcheck = 1
       !! 0: only print the bond KE for each time step (Input).
       !! 1: enables ZPE correction scheme (Input).

     real(dp) :: tcheck = 2.0_dp
       !! Duration of time for checking ZPE leakage (Input).
     real(dp) :: tcycle = 2.0_dp
       !! ZPE correction will take place after every this time (Input).
     real(dp) :: zthresh = 0.001_dp
       !! Threshold value to invoke velocity correction (Input).

     logical :: biascorrect = .false.
     !! If true, allow bias correction while changing velocities (Input).

     integer, dimension(:), allocatable :: ind_ah
       !! Index of atoms containing A-H bonds.
     integer, dimension(:), allocatable :: ind_bc
       !! Index of atoms containing the remaining B-C bonds.
     integer :: kahbond = -1
       !! Number of A-H bonds.
     integer :: kbcbond = -1
       !! Number of B-C bonds.
     integer :: kcount = -1

     logical, allocatable, dimension(:, :) :: hbond
       !! Truth Table for H-bonds.

     real(dp) :: random = 0.0_dp
       !! Random number generated at each correction step.

     real(dp), allocatable, dimension(:, :) :: tvel
       !! Copy of Velocity array for working only in this module.

     real(dp), dimension(:), allocatable :: Ek_ah
     real(dp), dimension(:), allocatable :: Ek_ah_store


   contains

     procedure :: init
     procedure :: get_ahbond
     procedure :: get_bcbond
     procedure :: print
     procedure :: destroy
     procedure :: parallel_ekin
     procedure :: bond_prop
     procedure :: check_zpe_leak
     procedure :: change_atom_velocity
     procedure :: correct_velocity_bcbonds
     procedure :: weight_bcbonds
     procedure :: write_velocity
     procedure :: calc_total_prop

  endtype nx_zpe_t

contains

  subroutine init(this, parser, conf)
    class(nx_zpe_t) :: this
    type(parser_t) :: parser
    type(nx_config_t) :: conf

    ! integer :: kmodel
    ! integer :: kcheck
    ! 
    ! real(dp) :: tcheck
    ! real(dp) :: tcycle
    ! real(dp) :: zthresh
    ! 
    ! logical :: biascorrect
    ! 
    ! integer :: u
    ! 
    ! namelist /zpe_correction/ kmodel, kcheck, tcheck, tcycle, zthresh, &
    !  &                        biascorrect
    ! 
    ! open(newunit=u, file=filename, status='old', action='read')
    ! read(u, nml=zpe_correction)
    ! close(u)

    call set(parser, 'zpe_correction', this%kmodel , 'kmodel')
    call set(parser, 'zpe_correction', this%kcheck , ' kcheck')
    call set(parser, 'zpe_correction', this%tcheck , 'tcheck')
    call set(parser, 'zpe_correction', this%tcycle , 'tcycle')
    call set(parser, 'zpe_correction', this%zthresh, 'zthresh')
    call set(parser, 'zpe_correction', this%biascorrect, 'biascorrect')

    this%kcount = 0

    this%random = -1.0_dp

    allocate( this%tvel(3, conf%nat) )
    this%tvel(:, :) = 0.0_dp

    allocate( this%Ek_ah(1) )
    allocate( this%Ek_ah_store(1) )
    this%Ek_ah(:)  = 0.0_dp
    this%Ek_ah_store(:)  = 0.0_dp

    !! This write will be deleted eventually, let's keep it for now.
    if (conf%lvprt >= 1) then
    write(80, *)"# Time (fs)      Avg. Parallel KE of A-H Bonds "
    write(90, *)"# Time (fs)      Parallel KE of A-H Bonds at specific Tiemstep"
    write(91, *)"# Time (fs)      Total KE          Total Pot          Total Enr "
    write(92, *)"# Time (fs)      Total Linear Momentum  "
    write(93, *)"# Time (fs)      Total Angular Momentum  "
    end if

  end subroutine init

  subroutine print(this)
    class(nx_zpe_t),intent(in) :: this

    write(*, *) ' '
    write(*, '(A80)') repeat('*', 80)
    write(*, *) 'ZPE correction parameters:'
    write(*, *) ''
    call print_conf_ele(this%kmodel, 'kmodel')
    call print_conf_ele(this%kcheck, 'kcheck')
    call print_conf_ele(this%tcheck, 'tcheck')
    call print_conf_ele(this%tcycle, 'tcycle')
    call print_conf_ele(this%zthresh, 'zthresh')
    write(*, *) 'biascorrect =', this%biascorrect

    write(*, *) 'initialization of random number : ', this%random

    write(*, '(A80)') repeat('*', 80)
    write(*, *) ' '

  end subroutine print


  subroutine zpecorrect_model1(traj, conf, zpe)
    type(nx_traj_t), intent(inout) :: traj
    type(nx_config_t), intent(inout) :: conf
    type(nx_zpe_t), intent(inout) :: zpe

    integer :: i
    integer :: kstep1, kstep2, nst

    real(dp) :: t_start, t_end
    real(dp), dimension(:), allocatable :: ekin_bond
    character(len=MAX_STR_SIZE) :: msg

    allocate( ekin_bond(zpe%kahbond) )

    t_start = zpe%kcount * zpe%tcycle
    t_end   =(zpe%kcount * zpe%tcycle) + zpe%tcheck

    kstep1 = NINT(t_start / (traj%dt*timeunit) )
    kstep2 = NINT(t_end   / (traj%dt*timeunit) )

    write(msg, '(A8, F12.4, A4, A8, I12)') &
         & 'ZPE: Start =', t_start, 'fs.', 'Step1 =', kstep1
    call nx_log%log(LOG_DEBUG, msg)
    write(msg, '(A8, F12.4, A4, A8, I12)') &
         & 'ZPE: End =', t_end, 'fs.', 'Step1 =', kstep2
    call nx_log%log(LOG_DEBUG, msg)
    write(msg, '(A8, F12.4, A4, A8, I12)') &
         & 'ZPE: Curr =', traj%t, 'fs.', 'Step1 =', traj%step
    call nx_log%log(LOG_DEBUG, msg)

    ! if (conf%lvprt >= 1) then
    !    write(*, '(A8, F12.4, A4, A8, I12)') &
    !  &          'Start =',t_start, 'fs.', 'Step1 =',kstep1
    !    write(*, '(A8, F12.4, A4, A8, I12)') &
    !  &          'End   =',t_end,   'fs.', 'Step2 =',kstep2
    !    write(*, '(A8, F12.4, A4, A8, I12)') &
    !  &          'Curr  =',traj%t,  'fs.', 'Step  =',traj%step
    ! end if
!!!
!   Copy the velocity to this temporary working array.
    zpe%tvel = traj%veloc
!!!

    if ( traj%step > kstep1 .and. traj%step < kstep2 ) then

       call zpe% parallel_ekin(traj, ekin_bond)

       do i = 1, zpe%kahbond
          zpe%Ek_ah(i) = zpe%Ek_ah(i) + ekin_bond(i)
       end do

       call nx_log%log(LOG_INFO, ekin_bond, title='ZPE: AH Bond KE')
       call nx_log%log(LOG_INFO, zpe%Ek_ah, title='ZPE: Cumulative AH Bond KE')
       ! if (conf%lvprt >= 1) then
       !    write(*, *) 'AH Bond KE:'
       !    call print_vector(ekin_bond)
       !    write(*, *) 'Cumulative AH Bond KE:'
       !    call print_vector(zpe%Ek_ah)
       ! end if

    end if
!
!
    if ( traj%step == kstep2 ) then

       nst = kstep2 - kstep1
       do i = 1, zpe%kahbond
          zpe%Ek_ah(i) = zpe%Ek_ah(i) / nst
       enddo

       call nx_log%log(LOG_DEBUG, &
            & 'ZPE: correction needed or not will be determined at this timestep')
     !   write(*, *)
     !   write(*, *)'ZPE correction needed or not will be determined at this timestep'
     !   write(*, '(A8, F12.4, A4, A8, I12)') &
     ! &          'Start =',t_start, 'fs.', 'Step1 =',kstep1
     !   write(*, '(A8, F12.4, A4, A8, I12)') &
     ! &          'End   =',t_end,   'fs.', 'Step2 =',kstep2
     !   write(*, '(A8, F12.4, A4, A8, I12)') &
     ! &          'Curr  =',traj%t,  'fs.', 'Step  =',traj%step
       !   write(*, '(A12, I12, A12, I12)') 'total step=',nst, 'kcount=',zpe%kcount
       write(msg, '(A12, I12, A12, I12)') &
            & 'total step=', nst, 'kcount=', zpe%kcount
       call nx_log%log(LOG_DEBUG, msg)
       ! write(*, '(A16, 999F20.12)') 'AVG Bond KE: ', (zpe%Ek_ah(i), i = 1, zpe%kahbond)
       call nx_log%log(LOG_INFO, zpe%ek_ah, title='AVG Bond KE')

       ! write(*, *)

       !! This write will be deleted eventually, let's keep it for now.
       if (conf%lvprt >= 1) then
          write(80, '(F10.4, 999F20.12)') traj%t, &
     &         (zpe%Ek_ah(i), i = 1, zpe%kahbond)
       end if

       call zpe% calc_total_prop(traj, conf)

       if (zpe%kcheck == 0) then
          ! write(*, *)' ***  ONLY CHECKING RUN: NO ZPE CORRECTION  ***'
          call nx_log%log(LOG_INFO, ' ***  ONLY CHECKING RUN: NO ZPE CORRECTION  ***')
       else
          if (zpe%kcount == 0) then
             ! write(*, *)'Store Avg KE as reference ZPE: NO ZPE Correction'
             call nx_log%log(LOG_INFO, 'Store Avg KE as reference ZPE: NO ZPE Correction')
             do i = 1, zpe%kahbond
                zpe%Ek_ah_store(i) = zpe%Ek_ah(i)
             enddo
          else
             call random_number(zpe%random)  ! Random number uniform-distributed between 0 and 1
             ! write(*, *) traj%t, 'fs ;      ', 'Random no at this correction step: ',
             ! zpe%random
             write(msg, '(A, F26.12)') 'ZPE: Random no at this correction step: ', zpe%random
             call nx_log%log(LOG_DEBUG, msg)

             call zpe% check_zpe_leak(traj, conf)

             !! This call below is only for debug purpose.
             if (conf%lvprt >= 1) then
                call zpe% calc_total_prop(traj, conf)
             end if
          end if
       end if

       do i = 1, zpe%kahbond
          zpe%Ek_ah(i) = 0.0_dp
       end do
       zpe%kcount = zpe%kcount + 1

    end if

    deallocate( ekin_bond )

  end subroutine zpecorrect_model1


  subroutine get_ahbond(this, traj, conf)
    class(nx_zpe_t), intent(inout) :: this
    type(nx_traj_t), intent(inout) :: traj
    type(nx_config_t), intent(inout) :: conf

    integer :: i, j, l, ii

    real(dp) :: dist
    character(len=MAX_STR_SIZE) :: msg

    allocate( this%hbond(conf%nat, conf%nat) )

    this%hbond = .false.
    do i = 1, conf%nat
       do j = 1, conf%nat

          if (i.lt.j) then
             dist = 0.0_dp
             do l = 1, 3
                dist = dist + (traj%geom(l, i) - traj%geom(l, j))**2
             end do
             dist = sqrt(dist)

             if ( (traj%atoms(i).eq.'H') .or. (traj%atoms(j).eq.'H') .or. &
     &            (traj%atoms(i).eq.'h') .or. (traj%atoms(j).eq.'h') ) then

                if ( (traj%atoms(i).eq.'C') .or. (traj%atoms(j).eq.'C') .or. &
     &               (traj%atoms(i).eq.'c') .or. (traj%atoms(j).eq.'c') ) then
                   if (dist .lt. 2.50_dp) this%hbond(i,j) = .true.

                else if ( (traj%atoms(i).eq.'N') .or. (traj%atoms(j).eq.'N') .or. &
     &               (traj%atoms(i).eq.'n') .or. (traj%atoms(j).eq.'n') ) then
                   if (dist .lt. 2.50_dp) this%hbond(i,j) = .true.

                else if ( (traj%atoms(i).eq.'O') .or. (traj%atoms(j).eq.'O') .or. &
     &               (traj%atoms(i).eq.'o') .or. (traj%atoms(j).eq.'o') ) then
                   if (dist .lt. 2.50_dp) this%hbond(i,j) = .true.

                end if
             end if

          end if

       end do
    end do

    this%kahbond = ( count(this%hbond) )

    write(msg, '(A,I0)') 'No of AH Bond: ', this%kahbond
    call nx_log%log(LOG_INFO, msg)
    ! write(*, *) 'No of AH Bond: ', this%kahbondp

    allocate( this%ind_ah(2 * this%kahbond) )

    ii = 1
    do i = 1, conf%nat
       do j = 1, conf%nat

          if ( this%hbond(i,j) ) then

             if ( (traj%atoms(i).eq.'H') .or. (traj%atoms(i).eq.'h') )then
                this%ind_ah(2*ii-1) = i
                this%ind_ah(2*ii)   = j
             else
                this%ind_ah(2*ii-1) = j
                this%ind_ah(2*ii)   = i
             end if
             ii = ii + 1

          end if

       end do
    end do

    call nx_log%log(LOG_DEBUG, this%ind_ah, title='Indices of AH bond atoms')
    ! if (conf%lvprt >= 1) then
    !    write(*, *)'Indices of AH bond atoms'
    !    write(*,'(6I3)') this%ind_ah
    ! end if

    if (allocated(this%Ek_ah)) deallocate(this%Ek_ah)
    if (allocated(this%Ek_ah_store)) deallocate(this%Ek_ah_store)
    allocate( this%Ek_ah(this%kahbond) )
    allocate( this%Ek_ah_store(this%kahbond) )
    this%Ek_ah(:)  = 0.0_dp
    this%Ek_ah_store(:)  = 0.0_dp

  end subroutine get_ahbond


  subroutine get_bcbond(this, conf)
    class(nx_zpe_t), intent(inout) :: this
    type(nx_config_t), intent(inout) :: conf

    integer :: i, j, jj

    logical, allocatable, dimension(:, :) :: bcpair
    character(len=MAX_STR_SIZE) :: msg

    allocate( bcpair(conf%nat, conf%nat) )

    bcpair = .false.
    do i = 1, conf%nat
       do j = 1, conf%nat

          if (i.lt.j) then
!             if ( (i.ne.iatom1) .and. (i.ne.iatom2) .and. &
!     &            (j.ne.iatom1) .and. (j.ne.iatom2) ) then
                if (this%hbond(i,j) .neqv. .true.) bcpair(i,j) = .true.
!             end if
          end if

       end do
    end do

    this%kbcbond = ( count(bcpair) )
!   Test with only O1-O4 as BC bond
!    this%kbcbond = 1
    ! write(*, *)'No of BC Bond =', this%kbcbond
    write(msg, '(A,I0)') 'No of BC Bond =', this%kbcbond
    call nx_log%log(LOG_INFO, msg)

    allocate( this%ind_bc(2*this%kbcbond) )

    jj = 1
    do i = 1, conf%nat
       do j = 1, conf%nat
          if ( bcpair(i,j) ) then
             this%ind_bc(2*jj-1) = i
             this%ind_bc(2*jj)   = j
             jj = jj + 1
          end if
       end do
    end do

!    this%ind_bc(1) = 1
!    this%ind_bc(2) = 4

    ! if (conf%lvprt >= 1) then
    !    write(*, *)'Indices of BC bond atoms'
    !    write(*,'(10I5)')this%ind_bc
    ! end if

    call nx_log%log(LOG_DEBUG, this%ind_bc, title='Indices of BC bond atoms')

    deallocate( bcpair )

  end subroutine get_bcbond


  subroutine calc_total_prop(this, traj, conf)
    class(nx_zpe_t), intent(inout) :: this
    type(nx_traj_t), intent(inout) :: traj
    type(nx_config_t), intent(inout) :: conf

    integer :: i, j
    real(dp) :: total_ke, total_pot, total_enr
    real(dp) :: anorm_linmom, anorm_angmom
    real(dp), dimension(:), allocatable :: ekin_bond

    allocate( ekin_bond(this%kahbond) )

    anorm_linmom = norm(traj%slinmom)
    anorm_angmom = norm(traj%sangmom)

    total_ke  = compute_ekin(traj%veloc, traj%masses)
!    total_ke  = compute_ekin(this%tvel, traj%masses)
    total_pot = traj%epot(traj%nstatdyn)
    total_enr = total_ke + total_pot

    call this% parallel_ekin(traj, ekin_bond)

    !! This write will be deleted eventually, let's keep it for now.
    if (conf%lvprt >= 1) then
       write(90, '(F10.4, 999F20.12)') traj%t, &
     &      (ekin_bond(i), i = 1, this%kahbond)
       write(91, '(F10.4, 999F20.12)') traj%t, &
     &       total_ke, total_pot, total_enr
       write(92, '(F10.4, 999F20.12)') traj%t, &
     &      (traj%slinmom(j), j = 1, 3), anorm_linmom
       write(93, '(F10.4, 999F20.12)') traj%t, &
     &      (traj%sangmom(j), j = 1, 3), anorm_angmom
    end if

    deallocate( ekin_bond )
  end subroutine calc_total_prop


  subroutine parallel_ekin(this, traj, ekin_bond)
    class(nx_zpe_t), intent(inout) :: this
    type(nx_traj_t), intent(inout) :: traj

    integer :: i
    integer :: iatom1, iatom2

    real(dp) :: r_m, rel_vel, ekin
    real(dp), dimension(3) :: umat
    real(dp), dimension(:), allocatable :: ekin_bond

    if (.not. allocated(ekin_bond)) allocate(ekin_bond( this%kahbond ))

    do i = 1, this%kahbond
       iatom1 = this%ind_ah(2*i-1)
       iatom2 = this%ind_ah(2*i)

       call this% bond_prop(traj, iatom1, iatom2, r_m, umat, rel_vel, ekin)

       ekin_bond(i) = ekin
    end do

  end subroutine parallel_ekin


 subroutine bond_prop(this, traj, iatom1, iatom2, r_m, umat, rel_vel, ekin)
    class(nx_zpe_t), intent(inout) :: this
    type(nx_traj_t), intent(inout) :: traj

    integer, intent(in) :: iatom1, iatom2
    integer :: j

    real(dp), intent(out) :: r_m, rel_vel, ekin
    real(dp), intent(out), dimension(3) :: umat
    real(dp), dimension (3) :: rel_dist, vel1, vel2
    real(dp) :: anorm
    real(dp) :: vel1_para, vel2_para

    r_m = traj%masses(iatom1)*traj%masses(iatom2) &
     &  /(traj%masses(iatom1)+traj%masses(iatom2))

    do j = 1, 3
!       rel_dist(j) = traj%veloc(j, iatom1) - traj%veloc(j, iatom2)
       rel_dist(j) = this%tvel(j, iatom1) - this%tvel(j, iatom2)
    enddo

    anorm = norm(rel_dist)

    do j = 1, 3
       umat(j) = rel_dist(j)/anorm
    enddo

    do j = 1, 3
!       vel1(j) = traj%veloc(j, iatom1)
!       vel2(j) = traj%veloc(j, iatom2)
       vel1(j) = this%tvel(j, iatom1)
       vel2(j) = this%tvel(j, iatom2)
    enddo

    vel1_para = scalar_product(vel1, umat)
    vel2_para = scalar_product(vel2, umat)

    rel_vel  = vel1_para - vel2_para
    ekin = 0.5_dp * r_m * rel_vel**2

!    This is excessive writing for extreme debug
!    write(*, *) 'iatom1=',iatom1, 'mass=', traj%masses(iatom1), &
!     &          'velocity:', (traj%veloc(j, iatom1), j= 1, 3)
!    write(*, *) 'iatom2=',iatom2, 'mass=', traj%masses(iatom2), &
!     &          'velocity:', (traj%veloc(j, iatom2), j= 1, 3)
!    write(*, *) 'red mass =', r_m, 'norm =', anorm
!    write(*, *) 'Relative velocity:', (rel_dist(j), j= 1, 3)
!    write(*, *) 'U matrix:', (umat(j), j= 1, 3)
!    write(*, *) 'Parallel Velocity:', vel1_para, vel2_para
!    write(*, *) 'Relative Parallel Velocity =', rel_vel
!    write(*, *) 'Parallel Kinetic energy =', ekin

  end subroutine bond_prop


  subroutine check_zpe_leak(this, traj, conf)
    class(nx_zpe_t), intent(inout) :: this
    type(nx_traj_t), intent(inout) :: traj
    type(nx_config_t), intent(inout) :: conf

    integer :: i
    integer :: iatom1, iatom2
    integer :: kswitch

    real(dp) :: r_m, rel_vel, ekin
    real(dp), dimension(3) :: umat
    real(dp) :: eleak, afac, enr_adjust
    character(len=MAX_STR_SIZE) :: msg

    logical :: scheck

    do i = 1, this%kahbond

       iatom1 = this%ind_ah(2*i-1)
       iatom2 = this%ind_ah(2*i)

       eleak = this%Ek_ah_store(i) - this%Ek_ah(i)

     !   write(*, '(A16, F20.12, A16, F12.6)') &
       ! &          'energy leak =', eleak, 'threshold =', this%zthresh
       write(msg, '(A16, F20.12, A16, F12.6)')&
            & 'ZPE: energy leak =', eleak, 'threshold =', this%zthresh
       call nx_log%log(LOG_INFO, msg)

       this%tvel = traj%veloc

       if (eleak > this%zthresh) then
          ! write(*, *) 'ZPE spilling detected between atoms ', iatom1, iatom2
          write(msg, '(A,I5,I5)') 'ZPE: ZPE spilling detected between atoms ', iatom1, iatom2
          call nx_log%log(LOG_INFO, msg)

          call this% bond_prop(traj, iatom1, iatom2, r_m, umat, rel_vel, ekin)

          afac = 1.0_dp
          kswitch = 0
          call this% change_atom_velocity(traj, iatom1, iatom2, &
     &              eleak, afac, r_m, umat, rel_vel, ekin, kswitch, enr_adjust, scheck)

          call this% get_bcbond(conf)

          call this% correct_velocity_bcbonds(traj, eleak)
       else
          ! write(*, *) 'Bond',i,'   no significant Energy leak: No ZPE Correction'
          ! write(*, *)
          write(msg, '(A,I5,A)') &
               & 'ZPE: Bond', i, ' no significant Energy leak: No ZPE Correction'
          call nx_log%log(LOG_DEBUG, msg)
       end if

    end do

  end subroutine check_zpe_leak


  subroutine change_atom_velocity(this, traj, iatom1, iatom2, &
     &       eleak, afac, r_m, umat, rel_vel, ekin, kswitch,  &
     &       enr_adjust, scheck)
    class(nx_zpe_t), intent(inout) :: this
    type(nx_traj_t), intent(inout) :: traj

    integer, intent(inout) :: iatom1, iatom2
    integer, intent(in) :: kswitch
    integer :: j

    real(dp), intent(in) :: eleak, afac
    real(dp), intent(inout) :: r_m, rel_vel, ekin
    real(dp), intent(inout), dimension(3) :: umat
    real(dp) :: arg, dele
    real(dp) :: old_ekin, new_ekin, enr_adjust

    logical :: corr_scheme1, corr_scheme2, scheck
    character(len=MAX_STR_SIZE) :: msg

    corr_scheme1 = .false.
    corr_scheme2 = .false.

    if (this%biascorrect) then
       if (this%random >= 0.5_dp) then
          corr_scheme1 = .true.
          ! add energy to H, subtract from A => subtract energy from del_AH
          ! add energy to B, subtract from C => subtract energy from del_BC
       else
          corr_scheme2 = .true.
          ! subtract energy from H, add to A => add to del_AH
          ! subtract energy from B, add to C => add to del_BC
       end if
    else
       corr_scheme1 = .true.
       corr_scheme2 = .false.
    end if

    ! if (conf%lvprt >= 1) then
    ! write(*, *) 'Correcting atom velocities', iatom1,'-',iatom2, 'at',traj%t, 'fs.'
    ! write(*, *) 'corr_scheme1:', corr_scheme1, 'corr_scheme2:', corr_scheme2
    ! end if
    write(msg, '(A,I0,A,I0,A,F12.3,A)') &
         & 'ZPE: Correcting atom velocities', iatom1,'-',iatom2, ' at ', traj%t, ' fs.'
    call nx_log%log(LOG_DEBUG, msg)

    scheck = .false.
    dele = 0.0_dp

    arg = 2.0_dp * eleak * afac / r_m

    if (kswitch == 0) then

            if (corr_scheme1) dele = sqrt( rel_vel**2 + arg ) - rel_vel

            if (corr_scheme2) dele = sqrt( rel_vel**2 + arg ) + rel_vel

    else if (kswitch == 1) then

            if ( (rel_vel**2 - arg) .ge. 0.0_dp ) then

               if (corr_scheme1) dele = sqrt( rel_vel**2 - arg ) - rel_vel

               if (corr_scheme2) dele = sqrt( rel_vel**2 - arg ) + rel_vel

               scheck = .true.
            else
               write(*, '(A,I0,I0,A,F12.3,A)') &
                    & "ZPE: Imaginary term appears for bond", iatom1,'-',iatom2, &
                    & ' at ',traj%t, ' fs.'
               call nx_log%log(LOG_WARN, msg)
    !            write(*, *)"CAUTION:"
    !            write(*, *)"Imaginary term appears for bond", &
    !  &                     iatom1,'-',iatom2, 'at',traj%t, 'fs.'
               dele = 0.0_dp
               scheck = .false.
            end if
    end if

    ! if (conf%lvprt >= 1) then
    !    write(*, *) "old parallel KE =", ekin
    ! end if
    write(msg, '(A,F26.12)') "ZPE: old parallel KE =", ekin
    call nx_log%log(LOG_DEBUG, msg)
    old_ekin = ekin


    if (corr_scheme1) then
       do j = 1, 3
          this%tvel(j, iatom1) = this%tvel(j, iatom1) &
     &                         + (r_m / traj%masses(iatom1)) * dele * umat(j)
          this%tvel(j, iatom2) = this%tvel(j, iatom2) &
     &                         - (r_m / traj%masses(iatom2)) * dele * umat(j)
       end do
    else if (corr_scheme2) then
       do j = 1, 3
          this%tvel(j, iatom1) = this%tvel(j, iatom1) &
     &                         - (r_m / traj%masses(iatom1)) * dele * umat(j)
          this%tvel(j, iatom2) = this%tvel(j, iatom2) &
     &                         + (r_m / traj%masses(iatom2)) * dele * umat(j)
       end do
       end if

    call this% bond_prop(traj, iatom1, iatom2, r_m, umat, rel_vel, ekin)
    new_ekin = ekin

    write(msg, '(A,F26.12)') "ZPE: newparallel KE =", ekin
    call nx_log%log(LOG_DEBUG, msg)
    ! if (conf%lvprt >= 1) then
    !    write(*, *) "new parallel KE =", ekin
    ! end if

    if (kswitch == 0) then
       enr_adjust = new_ekin - old_ekin
       write(msg, '(A,F26.12)') 'ZPE: Energy added in A-H bond', enr_adjust
       call nx_log%log(LOG_DEBUG, msg)
            ! if (conf%lvprt >= 1) then
            !    write(*,*)'Energy add in A-H bond', enr_adjust
            ! end if
    else if (kswitch == 1) then
       enr_adjust = old_ekin - new_ekin
       write(msg, '(A,F26.12)') 'ZPE: Energy removed from BC bond', enr_adjust
       call nx_log%log(LOG_DEBUG, msg)
            ! if (conf%lvprt >= 1) then
            !    write(*,*)'Energy remove from BC bond', enr_adjust
            ! end if
    end if

  end subroutine change_atom_velocity


  subroutine weight_bcbonds(this, traj, goodpair, wfac)
    class(nx_zpe_t), intent(inout) :: this
    type(nx_traj_t), intent(inout) :: traj

    integer :: i
    integer :: iatom1, iatom2

    real(dp) :: sumd, sumv, swf
    real(dp) :: r_m, rel_vel, ekin
    real(dp), dimension(3) :: umat
    real(dp), dimension(:), allocatable :: rm_bc
    real(dp), dimension(:), allocatable :: rvel_bc
    real(dp), dimension(:), allocatable :: ek_para_bc
    real(dp), dimension(:), allocatable :: wfac

    logical, dimension(:), allocatable :: goodpair
    character(len=MAX_STR_SIZE) :: msg

    allocate( rm_bc(this%kbcbond) )
    allocate( rvel_bc(this%kbcbond) )
    allocate( ek_para_bc(this%kbcbond) )

    if (.not. allocated(wfac))     allocate( wfac(this%kbcbond) )
    if (.not. allocated(goodpair)) allocate(goodpair(this%kbcbond))

    rm_bc(:) = -1.0
    rvel_bc(:) = -1.0
    ek_para_bc(:) = -1.0
    wfac(:) = -1.0

    sumd = 0.0_dp
    sumv = 0.0_dp
    do i = 1, this%kbcbond

       if ( goodpair(i) ) then

          iatom1 = this%ind_bc(2*i-1)
          iatom2 = this%ind_bc(2*i)

          call this% bond_prop(traj, iatom1, iatom2, r_m, umat, rel_vel, ekin)

          rm_bc(i) = r_m
          rvel_bc(i) = rel_vel
          ek_para_bc(i) = ekin

          sumd = sumd + ek_para_bc(i)
          sumv = sumv + rvel_bc(i)**2

       end if

    end do

    swf = 0.0_dp
    do i = 1, this%kbcbond
       if ( goodpair(i) ) then
          wfac(i) = ek_para_bc(i) / sumd
!          wfac(i) = rvel_bc(i)**2 / sumv
          swf = swf + wfac(i)
       end if
    end do

    call nx_log%log(LOG_DEBUG, wfac, title='ZPE: weight factors of good BC pairs:')
    write(msg, '(A, F26.12)') 'ZPE: sum of weights = ', swf
    call nx_log%log(LOG_DEBUG, msg)
    ! if (conf%lvprt >= 1) then
    !    write(*, *) 'weight factors of good BC pairs:'
    !    call print_vector(wfac)
    !    write(*, *) 'sum of weights = ', swf
    ! end if

    deallocate( rm_bc )
    deallocate( rvel_bc )
    deallocate( ek_para_bc )

  end subroutine weight_bcbonds


  subroutine correct_velocity_bcbonds(this, traj, eleak)
    class(nx_zpe_t), intent(inout) :: this
    type(nx_traj_t), intent(inout) :: traj

    integer :: i
    integer :: iatom1, iatom2
    integer :: kswitch

    real(dp) :: eleak, afac, arg, enr_adjust, sume
    real(dp) :: r_m, rel_vel, ekin
    real(dp), dimension(3) :: umat

    real(dp), dimension(:), allocatable :: wfac

    logical :: scheck
    logical, dimension(:), allocatable :: sanity
    logical, dimension(:), allocatable :: goodpair
    character(len=MAX_STR_SIZE) :: msg

    allocate( wfac(this%kbcbond) )
    allocate( goodpair(this%kbcbond) )
    allocate( sanity(this%kbcbond) )

    sanity(:) = .false.

    ! if (conf%lvprt >= 1) then
    !    write(*, *) 'Correcting all BC bonds at time =', traj%t, 'fs.'
    ! end if
    call nx_log%log(LOG_DEBUG, 'ZPE: Correcting all BC bonds')

    goodpair(:) = .true.
    call this% weight_bcbonds(traj, goodpair, wfac)

    do i = 1, this%kbcbond

       iatom1 = this%ind_bc(2*i-1)
       iatom2 = this%ind_bc(2*i)

       call this% bond_prop(traj, iatom1, iatom2, r_m, umat, rel_vel, ekin)

       arg = rel_vel**2 - (2.0_dp * eleak * wfac(i) / r_m)

       if ( arg .ge. 0.0_dp ) then
          goodpair(i) = .true.
          ! if (conf%lvprt >= 1) then
          !    write(*, *)"good pair:", i, "atoms:", iatom1, iatom2
          ! end if
          write(msg, '(A,I0,A,I0,I0)')&
               & "ZPE: good pair: ", i, " atoms: ", iatom1, iatom2
          call nx_log%log(LOG_DEBUG, msg)
       else
          goodpair(i) = .false.
          ! if (conf%lvprt >= 1) then
          !    write(*, *)"CAUTION: "
          !    write(*, *)"bad pair:", i, "atoms:", iatom1, iatom2
          ! end if
          write(msg, '(A,I0,A,I0,I0)')&
               & "ZPE: bad pair: ", i, " atoms: ", iatom1, iatom2
          call nx_log%log(LOG_WARN, msg)
       end if

    end do

    if (.NOT. ANY(goodpair) ) then
       sanity(:) = .false.
       ! write(*,*)"CAUTION: No Good Pair"
       ! write(*,*)"Sanity Check Failed. No Velocity correction at this step"

       ! write(*,*)
       call nx_log%log(LOG_WARN, &
            & "ZPE: Sanity Check Failed (no good pair). No Velocity correction at this step")
       sume = 0.0_dp

    elseif ( ALL(goodpair) ) then

       sume = 0.0_dp
       do i = 1, this%kbcbond

          iatom1 = this%ind_bc(2*i-1)
          iatom2 = this%ind_bc(2*i)
          call this% bond_prop(traj, iatom1, iatom2, r_m, umat, rel_vel, ekin)

          afac = wfac(i)
          kswitch = 1
          call this% change_atom_velocity(traj, iatom1, iatom2, &
     &         eleak, afac, r_m, umat, rel_vel, ekin, kswitch, enr_adjust, scheck)
          sume = sume + enr_adjust

          sanity(i) = scheck
       end do

    else

       ! if (conf%lvprt >= 1) then
       !    write(*, *)"CAUTION: bad pair detected."
       !    write(*, *)"Let's try again with only good pairs."
       ! end if
       call nx_log%log(LOG_WARN, 'ZPE: Try again with only good pairs (bad pairs detected)')

       call this% weight_bcbonds(traj, goodpair, wfac)

       sume = 0.0_dp
       sanity(:) = .true.
       do i = 1, this%kbcbond

          if ( goodpair(i) ) then

             iatom1 = this%ind_bc(2*i-1)
             iatom2 = this%ind_bc(2*i)

             call this% bond_prop(traj, iatom1, iatom2, r_m, umat, rel_vel, ekin)

             afac = wfac(i)
             kswitch = 1
             call this% change_atom_velocity(traj, iatom1, iatom2, &
     &            eleak, afac, r_m, umat, rel_vel, ekin, kswitch, enr_adjust, scheck)
             sume = sume + enr_adjust

             sanity(i) = scheck

          end if

       end do

    end if

    if ( ALL(sanity) ) then
         traj%veloc = this%tvel
    else
         ! write(*,*)"CAUTION: No Good Pair at second check"
         ! write(*,*)"Sanity Check Failed. No Velocity correction at this step"
       ! write(*,*)
       call nx_log%log(LOG_WARN, &
            & 'ZPE: Sanity Check Failed (no good pair at second check). No Velocity correction at this step')
         sume = 0.0_dp
    end if

    ! write(*,*)'Total Energy Removed from all BC bonds=', sume, 'eleak=', eleak
    ! write(*,*)
    write(msg, '(A,F26.12,A,F26.12)')&
         & 'Total Energy Removed from all BC bonds=', sume, ' eleak=', eleak
    call nx_log%log(LOG_INFO, msg)

    deallocate(this%ind_bc)
    deallocate(wfac)
    deallocate(goodpair)

  end subroutine correct_velocity_bcbonds


  subroutine write_velocity(this, iatom1, iatom2)
    class(nx_zpe_t), intent(inout) :: this
    integer, intent(in) :: iatom1, iatom2
    
    character(len=MAX_STR_SIZE) :: msg

    ! if (conf%lvprt >= 1) then
    !    write(*, *) "atom1 index:", iatom1,&
    !  &             "velocity:", (this%tvel(j, iatom1), j=1, 3)
    !    write(*, *) "atom2 index:", iatom2,&
    !  &             "velocity:", (this%tvel(j, iatom2), j=1, 3)
    ! end if
    write(msg, '(A,I0,A)') 'atom1 index ', iatom1, ' velocity'
    call nx_log%log(LOG_DEBUG, this%tvel(:, iatom1), title=msg)
    write(msg, '(A,I0,A)') 'atom2 index ', iatom2, ' velocity'
    call nx_log%log(LOG_DEBUG, this%tvel(:, iatom2), title=msg)

  end subroutine write_velocity

  subroutine destroy(this)
    class(nx_zpe_t), intent(inout) :: this

    deallocate( this%hbond )
    deallocate( this%ind_ah )
    deallocate( this%Ek_ah )
    deallocate( this%Ek_ah_store )
    deallocate( this%tvel )

  end subroutine destroy


endmodule mod_zpecorrect
