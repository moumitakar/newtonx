! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_analytical
  ! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  ! date: 2020-08-27
  !!
  !! # Analytical models
  !!
  !! This module contains all routines and functions related to
  !! running electronic structure computations with analytical
  !! models. The models currently implemented are:
  !!
  !! 1. Spin Boson Hamiltonian (prefix ``sbh_``);
  !! 2. Model potentials to study recohrence, implemented by Saikat
  !! Mukherjee <saikat.mukherjee@univ-amu.fr> (prefix ``recoh_``);
  !! 3. Collection of 1D models (prefix ``onedim_``).
  !! 4. 2D conical intersection model (prefix ``conint_``), from Ferretti et al., /JCP/
  !! (1996) (DOI: [10.1063/1.471791](https://doi.org/10.1063/1.471791) ).
  !!
  !! ## Interface
  !!
  !! The functions exported are intended to be used by the
  !! ``mod_qm_general`` module.
  !!
  !! ## Implementation of 1D models
  !!
  !! The implementation of 1D models is general, and only relies on
  !! an arbitrary size ``onedim_parm`` array, generated from a
  !! namelist ``onedim_parameters`` with components ``parm``. The
  !! allocation of this array is controlled by the variables
  !! ``ONED_``, used throughout the ``onedim_`` routines to decide
  !! the operations to handle.
  !!
  !! Implementing a new model thus only requires adding a ``ONED_``
  !! variable corresponding to the ``onedim_mod`` parameter, and to
  !! properly modify ``onedim_get_v_dv`` the with the corresponding
  !! expressions for the potential and its derivatives, and for the
  !! memory allocation in ``onedim_allocate``. The computation of
  !! energies, gradients and NAD is then carried out automatically by
  !! ``onedim_compute``.
  !!
  !! Running the 1D models is done in two steps:
  !!
  !! 1. Obtain the potential \(V\) and its derivative \(dV\). For two
  !! states these quantities are \(2x2\) matrices, with \( V_{12} =
  !! V_{21}\). For simplicity, the implementation will store \(V\)
  !! and \(dV\) as 1D arrays ``v`` and ``dv`` respectively, with
  !! ``v(1)`` corresponding to \(V_{11}\), ``v(2)`` to \(V_{12}\) and
  !! ``v(3)`` to \(V_{22}\) (same indices for ``dv``).
  !! 2. Compute the energy, gradient and non-adiabatic couplings.
  !!
  use mod_kinds, only: dp
  use mod_constants, only: &
       & proton, cm2au, MAX_STR_SIZE, timeunit
  use mod_configuration, only: nx_config_t
  use mod_logger, only: &
       & nx_log, &
       & LOG_DEBUG, LOG_ERROR, &
       & print_conf_ele
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_tools, only: to_str, split_blanks, norm
  use mod_input_parser, only: &
       & parser_t, set => set_config, set_alloc => set_config_realloc
  use mod_trajectory, only: nx_traj_t
  use mod_sbh, only: nx_sbh_t
  use mod_analytical_complex, only: nx_cs_fssh_t
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: am_setup, am_print
  public :: am_read
  public :: am_run
  public :: am_update_input
  public :: am_diabatic_population

contains

  subroutine am_setup(qm, parser, config)
    type(nx_qm_t), intent(inout) :: qm
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: config

    integer :: ncoupl, i
    integer :: u
    real(dp) :: dum
    character(len=2) :: dumc

    if (allocated(qm%am_m)) deallocate(qm%am_m)
    if (allocated(qm%am_r1)) deallocate(qm%am_r1)
    if (allocated(qm%am_v1)) deallocate(qm%am_v1)
    if (allocated(qm%am_epot)) deallocate(qm%am_epot)
    if (allocated(qm%am_grad1)) deallocate(qm%am_grad1)
    if (allocated(qm%am_nad1)) deallocate(qm%am_nad1)
    if (allocated(qm%am_diabpop)) deallocate(qm%am_diabpop)
    if (allocated(qm%am_gamma)) deallocate(qm%am_gamma)
    if (allocated(qm%am_nad1_i)) deallocate(qm%am_nad1_i)

    allocate(qm%am_m(config%nat))
    allocate(qm%am_r1(config%nat))
    allocate(qm%am_v1(config%nat))
    allocate(qm%am_epot(config%nstat))
    allocate(qm%am_grad1(config%nstat, config%nat))
    ncoupl = config%nstat * (config%nstat - 1) / 2
    allocate(qm%am_nad1(ncoupl, config%nat))
    allocate(qm%am_diabpop(3, config%nstat))
    allocate(qm%am_gamma(config%nstat))
    allocate(qm%am_nad1_i(ncoupl, config%nat))

    qm%am_epot = 0.0_dp
    qm%am_grad1 = 0.0_dp
    qm%am_nad1 = 0.0_dp
    qm%am_diabpop = 0.0_dp

    open(newunit=u, file='geom.orig', action='read')
    do i=1, config%nat
       read(u, *) dumc, dum, qm%am_r1(i), dum, dum, qm%am_m(i)
    end do
    close(u)
    qm%am_m(:) = qm%am_m(:) * proton

    open(newunit=u, file='veloc.orig', action='read')
    do i=1, config%nat
       read(u, *) qm%am_v1(i), dum, dum
    end do
    close(u)

    select case(qm%qmmethod)
    case('sbh')
       qm%sbh = nx_sbh_t( config%nat )
       call qm%sbh%setup( parser, trim(qm%job_folder)//'/user_sd_sbh.inp', qm%am_m)
    case('recohmodel')
       call qm%recohmod%setup(parser)
    case('onedim_model')
       call qm%onedim%setup(parser, trim(qm%job_folder)//'/onedim_parameters.inp')
    case('conint')
       call qm%conint%setup(parser, qm%am_m, qm%am_r1, qm%am_v1)
    case('cs_fssh')
       call qm%csfssh%setup(parser, trim(qm%job_folder)//'/constants.dat')
    end select
  end subroutine am_setup


  subroutine am_update_input(nx_qm, geom, veloc)
    type(nx_qm_t), intent(inout) :: nx_qm
    real(dp), dimension(:, :), intent(in) :: geom
    real(dp), dimension(:, :), intent(in) :: veloc

    nx_qm%am_r1(:) = geom(1, :)
    nx_qm%am_v1(:) = veloc(1, :)

    if (nx_qm%qmmethod == 'con_int') then
       call nx_qm%conint%update(nx_qm%am_r1, nx_qm%am_v1)
    end if
  end subroutine am_update_input


  subroutine am_run(nx_qm)
    type(nx_qm_t), intent(inout) :: nx_qm

    select case(nx_qm%qmmethod)
    case ('sbh')
       call nx_log%log(LOG_DEBUG, 'Running SBH model')
       call nx_qm%sbh%run( nx_qm%am_r1, nx_qm%am_m, &
            & nx_qm%am_epot, nx_qm%am_grad1, nx_qm%am_nad1)
    case ('recohmodel')
       call nx_log%log(LOG_DEBUG, 'Running recoherence model')
       call nx_qm%recohmod%run( nx_qm%am_r1(1), &
            & nx_qm%am_epot, nx_qm%am_grad1(:, 1), nx_qm%am_nad1(1, 1))
    case('onedim_model')
       call nx_log%log(LOG_DEBUG, 'Running recoherence model')
       call nx_qm%onedim%run( nx_qm%am_r1(1), &
            & nx_qm%am_epot, nx_qm%am_grad1(:, 1), nx_qm%am_nad1(:, 1))
    case('con_int')
       call nx_log%log(LOG_DEBUG, 'Running 2D conical intersection')
       call nx_qm%conint%run( &
            & nx_qm%am_epot, nx_qm%am_grad1(:, :), nx_qm%am_nad1(1, :))
    case('cs_fssh')
       call nx_log%log(LOG_DEBUG, 'Running CS-FSSH')
       call nx_qm%csfssh%run( &
            & nx_qm%am_r1(1), &
            & nx_qm%am_epot, nx_qm%am_grad1(:, 1), nx_qm%am_nad1(1, :), &
            & nx_qm%am_nad1_i(1, :), nx_qm%am_gamma)
    end select
  end subroutine am_run


  subroutine am_print(nx_qm, out)
    type(nx_qm_t), intent(in) :: nx_qm
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out

    select case(nx_qm%qmmethod)
    case ('sbh')
       call nx_qm%sbh%print(output)
    case('recohmodel')
       call nx_qm%recohmod%print(output)
    case('onedim_model')
       call nx_qm%onedim%print(output)
    case('con_int')
       call nx_qm%conint%print(output)
    case('cs_fssh')
       call nx_qm%csfssh%print(output)
    end select
  end subroutine am_print


  subroutine am_read(nx_qm, qminfo)
    !! Read information from the SBH run into the trajectory.
    type(nx_qm_t), intent(in) :: nx_qm
    !! Nx_Qm object.
    type(nx_qminfo_t), intent(inout) :: qminfo

    ! Transfer potential energy
    qminfo%repot(:) = nx_qm%am_epot(:)

    ! Transfer gradients
    qminfo%rgrad(:, 1, :) = nx_qm%am_grad1(:, :)

    ! Transfer NAD
    qminfo%rnad(:, 1, :) = nx_qm%am_nad1(:, :)

    ! CS-FSSH
    if (nx_qm%qmmethod == 'cs_fssh') then
       qminfo%rnad_i(:, 1, :) = nx_qm%am_nad1_i(:, :)
       qminfo%rgamma(:) = nx_qm%am_gamma(:)
    end if
  end subroutine am_read


  subroutine am_diabatic_population(qm, conf, traj)
    type(nx_qm_t), intent(in) :: qm
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(inout) :: traj

    real(dp), dimension(2, 2) :: vmat, umat

    if (qm%qmmethod == 'sbh') then
       umat = qm%sbh%get_umatrix(qm%am_m, qm%am_r1)
    end if

    call am_calculate_diabpop(conf, umat, traj)
  end subroutine am_diabatic_population


  subroutine am_calculate_diabpop(conf, umat, traj)
    type(nx_config_t), intent(in) :: conf
    real(dp), intent(in) :: umat(:, :)
    type(nx_traj_t), intent(inout) :: traj


    real(dp) :: res( size(traj%diabatic_pop) )

    integer :: i, j, ii
    integer :: lamda, kdel

    real(dp) :: sum2, sum3

    complex(dp) :: sum1, c_i, c_j, sigma 

    lamda = traj%nstatdyn          
    kdel = 0          

    !   Method 1
    do ii = 1, conf%nstat  
       traj%diabatic_pop(1, ii) = umat(ii, lamda)**2 
    end do

    !   Method 2
    do ii = 1, conf%nstat  
       sum1 = (0.0, 0.0)
       do i = 1, conf%nstat
          sum1 = sum1 + umat(ii, i) * traj%wf(i)
       enddo
       traj%diabatic_pop(2, ii) = real( conjg(sum1) * sum1 )
    end do

    !   Method 3
    do ii = 1, conf%nstat  

       sum2 = 0.0_dp
       sum3 = 0.0_dp
       do i = 1, conf%nstat

          if (i .eq. lamda) then 
             kdel = 1
          else 
             kdel = 0
          end if

          sum2 = sum2 + umat(ii, i)**2 * kdel

          do j = 1, conf%nstat
             c_i = traj%wf(i)
             c_j = traj%wf(j)

             if (i < j) then 
                sigma = c_i * conjg(c_j)
                sum3 = sum3 + 2.0_dp* umat(ii, i)* umat(ii, j)* real(sigma)
             end if
          end do
       end do
       traj%diabatic_pop(3, ii) = sum2 + sum3
    end do
  end subroutine am_calculate_diabpop
end module mod_analytical
