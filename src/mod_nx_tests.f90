! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_nx_tests
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr
  !!
  !! Trajectories test module for Newton-X
  !!
  !! This module handles the Newton-X testsuite, consisting in running trajectories from
  !! the different interfaces for which examples are defined in the ``examples`` folder.
  !!
  !! ## ``examples`` directory structure
  !!
  !! Each directory in ``examples`` must have the name of the interface tested (/i.e/
  !! ``TURBOMOLE``, ``GAUSSIAN``, ...), and contain one or more individual tests.
  !!
  !! Each individual test is defined in its own subdirectory, with a name starting with
  !! an index number, and a short description:
  !!
  !!     01-MD-TDDFT-NAD-CIO -> test number 1, dynamics with TDDFT and cioverlap
  !!
  !! Finally, each individual directory contains two subdirectories:
  !!
  !! - ``inputs`` contains the set of inputs required ;
  !! - ``outputs`` must contain at least the files ``energies.dat`` and
  !!   ``populations.dat``.
  !!
  !! A ``README`` file is also mandatory to give a short description of the dynamics that
  !! is being tested (one line is enough).

  use mod_kinds, only: dp
  use mod_constants, only: &
       & MAX_STR_SIZE
  use mod_interface, only: &
       & rm, copy, mkdir, get_list_of_files, setenv
  use mod_tools, only:&
       & split_pattern, remove_blanks, split_blanks, to_str
  use mod_print_utils, only: &
       & to_lower, to_upper
  implicit none

  public :: main

  type :: nx_test_t
     !! Individual tests from Newton-X.
     !!
     !! This object holds information, and can run, one individual test (/i.e/ one
     !! trajectory), and then compare the results to the reference given in the
     !! ``examples`` folder.
     !!
     !! This object will always assume that the test can be run under the directory given
     !! by ``dest``, relatively to the current working directory.  If this folder already
     !! exists, it is deleted before recreating it.  The input file are taken from
     !! ``origin//'/inputs/``.
     !!
     !! For instance, if we want to run the test defined in
     !! ``$NXHOME/examples/GAUSSIAN/01-MD-TDDFT-NAD-CIO`` with the executable just
     !! compiled in ``$NXHOME/build/md`` then the object will hold the following elements:
     !!
     !! - ``origin``: ``$NXHOME/examples/GAUSSIAN/01-MD-TDDFT-NAD-CIO`` ;
     !! - ``name``: ``01-MD-TDDFT-NAD-CIO`` ;
     !! - ``progname``: ``gaussian`` ;
     !! - ``command``: ``$NXHOME/build/md`` ;
     !! - ``dest``: ``01-MD-TDDFT-NAD-CIO``.
     !!
     character(len=:), allocatable :: origin
     !! Origin folder, where input and output files are taken, for either running the
     !! computation or comparing the results.
     character(len=:), allocatable :: name
     !! Test name.
     character(len=:), allocatable :: progname
     !! Program (interface) being tested.
     character(len=:), allocatable :: command
     !! Command to run NewtonX.
     integer :: status = 0
     !! Status of the test:
     !!
     !! - 0 means the test has passed ;
     !! - >0 means something went wrong (abnormal termination, or inconsistent results).
     real(dp) :: eps_pop = 1E-8
     !! Acceptable difference in electronic populations.
     real(dp) :: eps_en = 1E-8
     !! Acceptable difference in electronic energies (in a.u.).
   contains
     procedure :: prepare => test_prepare_run
     procedure :: run => test_run_md
     procedure :: print => test_print
     procedure :: compare => test_compare_results
     ! procedure :: report => test_report
  end type nx_test_t
  interface nx_test_t
     module procedure test_new
  end interface nx_test_t


  type :: nx_test_interface_t
     !! Object for testing whole interfaces.
     !!
     !! This object will handle the full test of the interface, from determining if the
     !! tests can be run or not (by looking at a set of required environment variables
     !! ``env`` or a list of commands to test ``commands``), to running each individual
     !! ``nx_test_t`` elements from the array ``testlist``.
     !!
     !! The ``status`` element will be set to :
     !!
     !! - 77 When the conditions for running the interface are not fulfilled, when missing
     !!   environment or command for instance (77 means ``skip`` when using ``make
     !!   check``) ;
     !! - 0 for normal termination of the test, /i.e/ all test run fine and the results
     !!   agree with the reference data ;
     !! - 1 when any test finishes with an error status, due to abnormal termination or
     !!   to mismatch in results.
     character(len=:), allocatable :: name
     !! Name of the interface being tested.
     character(len=256), allocatable :: env(:)
     !! List of environment variables required for this interface.
     character(len=256), allocatable :: prerun(:)
     !! List of commands that should be run before testing the list of ``commands``.
     character(len=256), allocatable :: postrun(:)
     !! List of commands that should be run after testing the list of ``commands``.
     character(len=256), allocatable :: commands(:)
     !! List of commands that must be tested for before running the test.
     character(len=:), allocatable :: basedir
     !! Directory containing the reference individual tests for this interface.
     character(len=:), allocatable :: mdexe
     !! Command used to run the dynamics.
     type(nx_test_t), allocatable :: testlist(:)
     !! Array of individual tests that must be performed.
     integer :: status = 0
     !! Status of the ensemble of tests.
     !!
     !! - 0 means that everything is alright ;
     !! - 77 means the tests for this interface will be skipped (missing environment or
     !!   mandatory command not found) ;
     !! - Any other positive value means that something went wrong in /one or more/ tests.
     logical :: skip_test = .false.
     !! This indicates if the tests for this interface should be skipped or not.
     integer :: nfail = 0
     !! Number of individual tests that failed for this interface.
   contains
     procedure :: prepare => test_interface_prepare
     procedure :: populate => test_interface_populate
     procedure :: create_dir => test_interface_create_dir
  end type nx_test_interface_t
  interface nx_test_interface_t
     module procedure test_interface_init
  end interface nx_test_interface_t

  private

contains

  function main(progs, tests, mdexe, dir, make_env) result(res)
    !! Program for running the testsuite.
    !!
    character(len=*), intent(in) :: progs
    !! Comma-separated list of program interfaces to test. If empty, all interfaces from
    !! ``$NXHOME/examples`` will be tested.
    character(len=*), intent(in) :: tests
    !! List of tests to carry out. If empty, all tests for every interfaces will be done.
    !! Else, the list can only be provided if ``progs`` is not empty. In that case, it
    !! must a string, composed of the comma-separated indices of the tests to be
    !! performed for each interface, each interface in turn separated by "|".  The
    !! interfaces must be specified in the same order as in ``progs``.
    character(len=*), intent(in) :: mdexe
    !! Full path to the Newton-X program to use for testing, /e.g/ ``$NXHOME/bin/md``.
    character(len=*), intent(in) :: dir
    !! Directory where the tests will be carried out.
    integer, intent(in) :: make_env
    !! Indicate if the program is invoked through ``make check`` or not.

    integer :: res

    character(len=MAX_STR_SIZE) :: env, orig
    character(len=256), allocatable :: group(:)

    character(len=:), allocatable :: proglist
    character(len=:), allocatable :: title
    integer :: ierr, ierr1, ierr2, i, j
    integer :: ntests, nfail
    logical :: ext
    type(nx_test_interface_t), allocatable :: testint(:)

    ! Set NX_TEST_ENV
    ierr = setenv("NX_TEST_ENV", "1")

    call get_environment_variable('NXHOME', value=env)

    ! Create the different tests interfaces, and populate them with the chosen tests
    if (progs == '') then
       proglist = build_program_list(env)
    else
       proglist = progs
    end if

    if (proglist == '') then
       write(*, '(A)') 'ERROR: Cannot obtain list of programs !'
       error stop
    end if

    testint = build_test_interfaces(proglist, tests, mdexe)
    if (check_test_interface(testint) /= 0) then
       write(*, '(A)') 'ERROR: Test interface list creation failed !'
       error stop
    end if

    inquire(file=dir, exist=ext)
    if (.not. ext) then
       res = mkdir(dir)
       if (res /= 0) then
          write(*, '(A)') 'ERROR: Cannot create '//trim(dir)
          res = -1
          return
       end if
    end if

    res = chdir(dir)
    if (res /= 0) then
       write(*, '(A)') 'ERROR: Cannot change dir to '//trim(dir)
       res = -1
       return
    end if

    res = getcwd( orig )
    if (res /= 0) then
       write(*, '(A)') 'ERROR: Cannot get name of current directory '
       res = -1
       return
    end if

    TEST_LOOP: do i=1, size(testint)

       ntests = size(testint(i)%testlist)
       title = 'Testing interface '//to_upper( testint(i)%name )

       print *, repeat('-', len(title))
       print *, title
       print *, ''
       print *, '   '//to_str(ntests)//' tests to be done'
       print *, ''

       ! Check if the test can be run or not
       call testint(i)%prepare()
       if (testint(i)%status /= 0) then

          if (make_env == 1) then
             ! Skip this in ``make check``
             res = 77
             exit TEST_LOOP
          else
             print *, '   SKIPPING TEST FOR '//testint(i)%name
             testint(i)%skip_test = .true.
             print *, repeat('-', len(title))
             print *, ''
             cycle
          end if
       else
          call testint(i)%create_dir()
       end if

       do j=1, size( testint(i)%testlist )

          res = chdir( testint(i)%name )
          if (res /= 0) then
             write(*, '(A)') 'ERROR: Cannot change dir to '//trim(testint(i)%name)
             res = -1
             exit TEST_LOOP
          end if

          call testint(i)%testlist(j)%print()
          call testint(i)%testlist(j)%run()

          if (testint(i)%testlist(j)%status /= 0) then
             print *, 'ERROR: abnormal termination'
             print *, ''
             testint(i)%nfail = testint(i)%nfail + 1
          else
             ierr1 = testint(i)%testlist(j)%compare('energies.dat', 3)
             if (ierr1 < 0) then
                print *, 'ERROR: Data mismatch (energies)'
             end if

             ierr2 = testint(i)%testlist(j)%compare('populations.dat', 3)
             if (ierr2 < 0) then
                print *, 'ERROR: Data mismatch (populations)'
             end if

             if (ierr1 < 0 .or. ierr2 < 0) then
                testint(i)%nfail = testint(i)%nfail + 1
             else
                print *, '   OK !'
             end if
          end if

          res = chdir( orig )
          if (res /= 0) then
             write(*, '(A)') 'ERROR: Cannot change dir to '//trim(dir)
             res = -1
             exit TEST_LOOP
          end if
       end do

       print *, ''
       print *, 'SUMMARY: '//to_str(ntests - testint(i)%nfail)//' / '//to_str(ntests)//' tests passed.'
       print *, ''
       print *, repeat('-', len(title))

       if (testint(i)%nfail /= 0) then
          res = testint(i)%nfail
       end if

       print *, ''
    end do TEST_LOOP

    if (res < 0) then
       return
    else if (res >= 0 .and. res /= 77) then

       res = 0
       print *, 'FINAL SUMMARY'

       do i=1, size(testint)
          if (testint(i)%nfail > 0) res = -1
          if (testint(i)%skip_test) then
             print '(A20,A)', adjustl('  - '//testint(i)%name//': '), adjustl('SKIPPED')
             cycle
          end if

          ntests = size(testint(i)%testlist)
          print '(A20,I0,A,I0,A)', &
               adjustl('  - '//testint(i)%name//': '), ntests - testint(i)%nfail, '/', ntests, ' tests passed'
       end do
    end if

  end function main


  function build_program_list(env) result(res)
    character(len=*), intent(in) :: env

    character(len=:), allocatable :: res

    character(len=256), allocatable :: group(:), dir
    character(len=MAX_STR_SIZE) :: tmp
    integer :: ierr, i
    logical :: ext

    dir = trim(env)//'/examples/'
    inquire(file=trim(env)//'/share/', exist=ext)
    if (ext) then
       dir = trim(env)//'/share/newtonx/examples/'
    end if

    call get_list_of_files(dir, group, ierr, exclude=['analysis'])
    if (ierr /= 0) then
       write(*, '(A)') 'ERROR: Cannot get list of files in '//trim(env)//'examples/'
       res = ''
       return
    end if

    tmp = trim(to_lower(group(1)))
    do i=2, size(group)
       tmp = trim(tmp)//','//trim(to_lower(group(i)))
    end do

    res = trim(tmp)
  end function build_program_list


  function build_test_interfaces(progs, tests, mdexe) result(res)
    character(len=*), intent(in) :: progs
    character(len=*), intent(in) :: tests
    character(len=*), intent(in) :: mdexe

    type(nx_test_interface_t), allocatable :: res(:)

    character(len=256), allocatable :: group(:)
    integer :: i

    group = split_pattern(progs, ',')
    allocate( res( size(group) ))
    do i=1, size(group)
       res(i) = nx_test_interface_t(group(i), mdexe)
    end do

    if (tests /= '') then
       group = split_pattern( tests, '|' )
       do i=1, size(group)
          call res(i)%populate(group(i))
          if (res(i)%status /= 0) then
             return
          end if
       end do
    else
       do i=1, size(group)
          call res(i)%populate()
          if (res(i)%status /= 0) then
             return
          end if
       end do
    end if

  end function build_test_interfaces


  function check_test_interface(testint) result(res)
    type(nx_test_interface_t), intent(in) :: testint(:)

    integer :: res

    integer :: i

    res = 0
    do i=1, size(testint)
       if (testint(i)%status /= 0) then
          write(*, '(A)') 'Creation of interface '//testint(i)%name//' failed.'
          res = 1
       end if
    end do
  end function check_test_interface


  function test_new(origin, name, progname, command) result(res)
    !! Creator for the ``nx_test_t`` object.
    !!
    character(len=*), intent(in) :: origin
    !! Directory containing the reference data for the test.
    character(len=*), intent(in) :: name
    !! Name of the test being run.
    character(len=*), intent(in) :: progname
    !! Program (interface) being tested.
    character(len=*), intent(in), optional :: command
    !! Command used to run the test (default: ``$NXHOME/bin/md``)

    type(nx_test_t) :: res
    character(len=MAX_STR_SIZE) :: env

    call get_environment_variable('NXHOME', value=env)

    res%origin = trim(origin)
    res%name = trim(name)
    res%progname = trim(progname)

    if (present(command)) then
       res%command = trim(command)
    else
       res%command = trim(env)//'/bin/md'
    end if
  end function test_new


  subroutine test_print(self)
    !! Print information about the current test.
    !!
    class(nx_test_t), intent(in) :: self
    !! Current test.

    print *, self%progname//' test '//self%name//': '//&
         &   'files from '//self%origin//' ('//self%command//')'
  end subroutine test_print


  subroutine test_prepare_run(self)
    !! Prepare running the test.
    !!
    !! This routine will create the folder ``self%name`` (first, it deletes it if it
    !! already exists), and copy all input files from ``self%origin//'/inputs/`` to this
    !! new folder.
    !!
    class(nx_test_t), intent(inout) :: self
    !! Current test.

    character(len=256), allocatable :: filelist(:)
    logical :: ext
    integer :: ierr

    inquire(file=self%name, exist=ext)
    if (ext) ierr = rm([self%name])

    self%status = mkdir( self%name )
    if (self%status /= 0) then
       print *, 'ERROR: Cannot create '//trim( self%name )
       return
    end if

    call get_list_of_files(self%origin//'/inputs/', filelist, self%status, full_path=.true.)

    self%status = copy(filelist, self%name, cpflags='-r')
  end subroutine test_prepare_run


  subroutine test_run_md(self)
    !! Runs the current test.
    !!
    !! The routine runs the test by executing the command:
    !!
    !!     self%command > md.log 2>&1
    !!
    !! It then checks the file ``md.log`` for the line ``Normal termination of
    !! Newton-X``, and sets ``self%status`` to 0 if the file is found and contains this
    !! line, and to 1 in any other cases.
    class(nx_test_t), intent(inout) :: self
    !! Current test.

    character(len=MAX_STR_SIZE) :: curdir, buf
    character(len=:), allocatable :: cmd, output
    integer :: ierr, u, st

    output = 'md.log'
    cmd = self%command//' > '//output//' 2>&1'

    self%status = getcwd( curdir )
    self%status = chdir( self%name )
    call execute_command_line( cmd, exitstat=self%status )

    if (self%status /= 0) then
       self%status = 1
       ierr = chdir( curdir )
       return
    end if

    st = 1
    open(newunit=u, file=output, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Normal termination of Newton-X') /= 0) then
          st = 0
          exit
       end if
    end do
    close(u)
    ierr = chdir( curdir )
    self%status = st
  end subroutine test_run_md


  function test_compare_results(self, file, skipcols)
    !! Compare the results obtained with reference.
    !!
    !! The routine reads the ``energies.dat`` and ``populations.dat`` from the current
    !! test directory, and from the reference directory ``self%origin//'/outputs/'``.
    !! The routine reports if the files don't have the same dimensions, and if both files
    !! have differences of more than ``self%eps_pop`` (or ``self%eps_en``).
    !!
    !! If the results are the same for reference and current tests, the function returns
    !! 0. In other cases, it returns 1.
    class(nx_test_t), intent(inout) :: self
    !! Current test.
    character(len=*), intent(in) :: file
    !! Name of the file to compare (``energies.dat`` or ``populations.dat``)
    integer, intent(in) :: skipcols
    !! Number of columns to skip in the chosen file.

    integer :: test_compare_results

    character(len=:), allocatable :: ref_file, test_file
    real(dp), allocatable :: test(:, :), ref(:, :)
    integer :: ierr

    ref_file = self%origin//'/outputs/'//file
    test_file = self%name//'/RESULTS/'//file

    call read_result_file_d2(ref_file, ref, skipcols=skipcols)
    call read_result_file_d2(test_file, test, skipcols=skipcols)

    ierr = 0

    if (        size(ref, 1) /= size(test, 1) &
         & .or. size(ref, 2) /= size(test, 2) ) then
       print *, 'ERROR: '//ref_file//' and '//test_file//' don''t have the same dimensions'
       print *, '       '//ref_file//' has '//to_str(size(ref, 1))//' lines '//' and '//to_str(size(ref, 2))//' columns.'
       print *, '       '//test_file//' has '//to_str(size(test, 1))//' lines '//' and '//to_str(size(test, 2))//' columns.'

       ierr = 1
    end if

    if (ierr /= 1) then
       call compare_results_d2(ref, test, self%eps_en, file, ierr)
    end if

    test_compare_results = ierr
  end function test_compare_results


  function test_interface_init(progname, mdexe) result(res)
    !! Creator for the ``nx_test_interface_t`` object.
    !!
    !! This routine initializes the components of the ``env``, ``commands``, ``prerun``
    !! and ``postrun`` members according to the interface being tested.  It also sets the
    !! default value for the ``mdexe`` member (``$NXHOME/bin/md``).
    character(len=*), intent(in) :: progname
    !! Name of the interface being tested.
    character(len=*), intent(in), optional :: mdexe
    !! Command to be run for the dynamics (default: ``$NXHOME/bin/md``)

    type(nx_test_interface_t) :: res

    integer :: ierr
    character(len=MAX_STR_SIZE) :: env
    logical :: ext

    res%name = trim(progname)

    call get_environment_variable('NXHOME', env, status=ierr)
    if (ierr == 1) then
       write(*, '(A)') 'ERROR: NXHOME environment not defined !!'
       error stop
    end if

    res%basedir = trim(env)//'/examples/'//to_upper(res%name)//'/'
    inquire(file=trim(env)//'/share/', exist=ext)
    if (ext) then
       res%basedir = trim(env)//'/share/newtonx/examples/'//to_upper(res%name)//'/'
    end if

    if (present(mdexe)) then
       res%mdexe = trim(mdexe)
    else
       res%mdexe = trim(env)//'/bin/nx_moldyn'
    end if

    select case(res%name)
    case('columbus')
       allocate(res%env(1))
       allocate(res%prerun(1))
       allocate(res%postrun(1))
       allocate(res%commands(1))

       res%env(1) = 'COLUMBUS'
       res%commands(1) = '$COLUMBUS/runc'
       res%prerun(1) = 'NULL'
       res%postrun(1) = 'NULL'
    case('gaussian','exc_gaussian')
       allocate(res%env(2))
       allocate(res%prerun(2))
       allocate(res%postrun(1))
       allocate(res%commands(1))

       res%env(1) = 'g16root'
       res%env(2) = 'GAUSS_EXEDIR'
       res%commands(1) = '$g16root/g16/g16 foo.inp'
       res%prerun(1) = 'touch foo.inp'
       res%prerun(2) = '/bin/bash -c "source $g16root/g16/bsd/g16.profile"'
       res%postrun(1) = 'rm foo.inp foo.log'
    case('turbomole')
       allocate(res%env(1))
       allocate(res%prerun(1))
       allocate(res%postrun(1))
       allocate(res%commands(1))

       res%env(1) = 'TURBODIR'
       res%commands(1) = 'dscf'
       res%prerun(1) = 'NULL'
       res%postrun(1) = 'NULL'
    case('orca')
       allocate(res%env(1))
       allocate(res%prerun(1))
       allocate(res%postrun(1))
       allocate(res%commands(1))
       res%env(1) = 'ORCA'
       res%commands(1) = 'orca'
       res%prerun(1) = 'NULL'
       res%postrun(1) = 'NULL'
    case('tinker_mndo')
       allocate(res%env(2))
       allocate(res%prerun(1))
       allocate(res%postrun(1))
       allocate(res%commands(2))

       res%env(1) = 'TINKER_MNDO'
       res%env(2) = 'MNDO'
       res%commands(1) = '$MNDO/mndo2020 < foo.inp'
       res%commands(2) = '$TINKER_MNDO/gradinterf.x < foo.inp'
       res%prerun(1) = 'touch foo.inp'
       res%postrun(1) = 'rm foo.inp'
    case('exc_mopac')
       allocate(res%env(2))
       allocate(res%prerun(1))
       allocate(res%postrun(1))
       allocate(res%commands(1))

       res%env(1) = 'TINKER'
       res%env(2) = 'MOPAC'
       res%commands(1) = '$MOPAC/mopac2002.x'
       res%prerun(1) = 'NULL'
       res%postrun(1) = 'NULL'
    case('mopac')
       allocate(res%env(1))
       allocate(res%prerun(1))
       allocate(res%postrun(1))
       allocate(res%commands(1))

       res%env(1) = 'MOPAC'
       res%commands(1) = '$MOPAC/mopac2002.x'
       res%prerun(1) = 'NULL'
       res%postrun(1) = 'NULL'
    case default
       allocate(res%env(1))
       allocate(res%prerun(1))
       allocate(res%postrun(1))
       allocate(res%commands(1))

       res%env(1) = 'NULL'
       res%commands(1) = 'NULL'
       res%prerun(1) = 'NULL'
       res%postrun(1) = 'NULL'
    end select
  end function test_interface_init


  subroutine test_interface_prepare(self)
    !! Prepare running the tests for the current interface.
    !!
    !! The routine will check for the presence of the required environments and commands,
    !! and set ``skip_test`` to ``.true.`` if any condition is not met.  It is then up to
    !! the main driver to skip the test.
    class(nx_test_interface_t), intent(inout) :: self
    !! Current interface.

    integer :: i, ierr, ll ! , ind
    character(len=MAX_STR_SIZE) :: env

    if (self%env(1) /= 'NULL') then
       do i=1, size(self%env)
          call get_environment_variable(self%env(i), value=env, length=ll, status=ierr)
          if (ierr == 1 .or. ll == 0) then
             self%status = 77
             print *, trim(self%env(i))//': NOT FOUND'
          else
             print *, trim(self%env(i))//': found as '//trim(env)
          end if
       end do
       if (self%status /= 0) then
          return
       end if
    end if

    if (self%prerun(1) /= 'NULL') then
       do i=1, size(self%prerun)
          print *, 'RUNNING: '//trim(self%prerun(i))
          call execute_command_line(self%prerun(i), exitstat=ierr)
          if (ierr /= 0) then
             print *, 'Cannot execute: '//trim(self%prerun(i))
             self%status = ierr
          end if
       end do
       if (self%status /= 0) then
          return
       end if
    end if

    if (self%commands(1) /= 'NULL') then
       do i=1, size(self%commands)
          call execute_command_line(trim(self%commands(i))//' > /dev/null 2>&1', exitstat=ierr)
          if (shiftr(ierr, 8) == 127) then
             self%status = 77
          end if
       end do
       if (self%status /= 0) then
          return
       end if
    end if

    if (self%postrun(1) /= 'NULL') then
       do i=1, size(self%postrun)
          call execute_command_line(self%postrun(i), exitstat=ierr)
          if (ierr /= 0) then
             print *, 'Cannot execute: '//trim(self%postrun(i))
             self%status = ierr
          end if
       end do
       if (self%status /= 0) then
          return
       end if
    end if

    if (self%status /= 0) self%skip_test = .true.
  end subroutine test_interface_prepare


  subroutine test_interface_populate(self, testlist)
    !! Populate the list of tests to be run.
    !!
    !! ``testlist`` should be a string of comma-separated list of tests (for instance,
    !! ``"1,3,4"`` for running tests number 1, 3 and 4).  If not provided, the full list
    !! of tests will be taken from the directory ``origin``.
    class(nx_test_interface_t), intent(inout) :: self
    character(len=*), intent(in), optional :: testlist

    integer :: i, j, k, ierr, ind_dir, ind_list
    character(len=256), allocatable :: dirlist(:), tmp(:), tlist(:)

    if (allocated(self%testlist)) deallocate(self%testlist)

    call get_list_of_files(self%basedir, dirlist, ierr)
    if (ierr /= 0) then
       write(*, '(A)') 'ERROR: Cannot get list of files in '//trim(self%basedir)
       self%status = ierr
       return
    end if

    if (present(testlist)) then
       tlist = split_pattern( testlist, ',')
       allocate(self%testlist(size(tlist)))

       k = 1
       do i=1, size(dirlist)
          tmp = split_pattern(dirlist(i), '-')
          read(tmp(1), *) ind_dir
          do j=1, size(tlist)
             read(tlist(j), *) ind_list
             if ( ind_dir == ind_list ) then
                self%testlist(k) = nx_test_t( &
                     & self%basedir//trim(dirlist(i)), trim(dirlist(i)), self%name, self%mdexe&
                     & )
                k = k+1
             end if
          end do
       end do
    else
       allocate(self%testlist(size(dirlist)))
       do i=1, size(dirlist)
          self%testlist(i) = nx_test_t( &
               & self%basedir//trim(dirlist(i)), trim(dirlist(i)), self%name, self%mdexe&
               & )
       end do
    end if
  end subroutine test_interface_populate



  subroutine test_interface_create_dir(self)
    !! Create the directory structure for current interface.
    !!
    !! The routine creates a directory with the name of the interface, then create a
    !! subdirectory for each test defined in ``testlist`` by calling the corresponding
    !! ``testlist%prepare`` routine.
    class(nx_test_interface_t), intent(inout) :: self
    !! Current test interface.

    integer :: ierr, i
    logical :: ext
    character(len=MAX_STR_SIZE) :: orig

    ierr = getcwd(orig)

    inquire(file=self%name, exist=ext)
    if (.not. ext) then
       ierr = mkdir(self%name)
    end if

    ierr = chdir(self%name)
    do i=1, size( self%testlist )
       call self%testlist(i)%prepare()
    end do
    ierr = chdir(orig)
  end subroutine test_interface_create_dir


  ! =======================
  ! Private helper routines
  ! =======================

  subroutine read_result_file_d2(filename, res, skiprows, skipcols)
    !! Read the content from of a file into a 2D array (real).
    !!
    !! The routine will skip a number of rows equal to ``skiprows``, and ignore any line
    !! with ``#`` as a first non-blank character.  The number of columns is determined
    !! automatically, and ``skipcols`` will be skipped when reading the file.
    !!
    !! The reading is done in two passes: the first determines the dimensions of the
    !! resulting array, the second actually reads the content of the file.
    character(len=*), intent(in) :: filename
    real(dp), allocatable, intent(out) :: res(:, :)
    integer, intent(in), optional :: skiprows
    integer, intent(in), optional :: skipcols

    integer :: nlines, ncols, skipc, ncom
    integer :: u, ierr, i
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    character(len=:), allocatable :: tmp
    real(dp), allocatable :: junk(:)

    open(newunit=u, file=filename, action='read')

    if (present(skiprows)) then
       do i=1, skiprows
          read(u, '(A)') buf
       end do
    end if

    nlines = 0
    ncols = 0
    ncom = 0
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       tmp = remove_blanks(buf)
       if (index(tmp, '#') == 1) then
          ncom = ncom + 1
          nlines = nlines + 1
          cycle
       end if

       call split_blanks(buf, group)
       if (size(group) > ncols) ncols = size(group)
       nlines = nlines + 1
    end do
    rewind(u)

    skipc = 0
    if (present(skipcols)) skipc = skipcols
    ncols = ncols - skipc

    nlines = nlines - ncom
    allocate(res(nlines, ncols))
    allocate(junk(skipc))

    if (present(skiprows)) then
       do i=1, skiprows
          read(u, '(A)') buf
       end do
    end if

    do i=1, ncom
       read(u, '(A)') buf
    end do

    do i=1, nlines
       read(u, '(A)') buf

       read(buf, *) junk(:skipc), res(i, :ncols)
    end do
    close(u)

  end subroutine read_result_file_d2


  subroutine compare_results_d2(arr1, arr2, eps, file, status)
    !! Compare the values of ``arr1`` and ``arr2``.
    !!
    !! The comparison is done element by element. If any element differ (in absolute
    !! value) by more than ``eps`` then the corresponding elements, as well as the
    !! difference, is printed.  If any error is encountered, ``status`` is set to -1.  A
    !! success sets ``status`` to 0.
    real(dp), intent(in) :: arr1(:, :)
    real(dp), intent(in) :: arr2(:, :)
    real(dp), intent(in) :: eps
    character(len=*), intent(in) :: file
    integer, intent(out) :: status

    integer :: ll, cc, nerror
    real(dp) :: diff

    status = 0
    nerror = 0
    do ll=1, size(arr1, 1)
       do cc=1, size(arr1, 2)
          diff = abs(arr1(ll, cc) - arr2(ll, cc))
          if ( diff > eps ) then
             status = -1
             if (nerror == 0) then
                print *, 'Mismatch found: ('//file//')'
                print '(2A10,3A20)', 'Step', 'Field', 'ref', 'test', 'abs(ref - test)'
                nerror = nerror + 1
             end if
             print '(I10,I10,3F20.12)', ll, cc, arr1(ll,cc), arr2(ll,cc), diff
             nerror = nerror + 1
          end if
       end do
    end do
  end subroutine compare_results_d2



end module mod_nx_tests
