! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_sh_traj_t
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2021-06-11
  !!
  !! This module defines the SH trajectory type ``nx_sh_traj_t`` for
  !! handling the evolution of the electronic population during the
  !! integration of the time-dependant Schrödinger equation (TDSE),
  !! and for computing the surface hopping probabilities.
  !!
  !! The evolution of the wavefunction is implemented as
  !! described in Ferretti, Granucci et al. J. Chem. Phys., 104, (1996)
  !! [10.1063/1.471791](http://dx.doi.org/10.1063/1.471791), with the
  !! following coupled equations to solve:
  !!
  !! \[ \dot{A_K}(t) = -\sum_{K \neq L} A_L(t) e^{i\gamma_{KL}}
  !! \sigma_{KL}^{NAD}, \]
  !! \[ \gamma_K(t) = \int_{0}^{t} E_K(t')dt'. \]
  !!
  !! In addition, the probabilities \(g_{KL}\) of hopping from state \(K\) to
  !! \(L\) are computed with the Tully method:
  !!
  !! \[ b_{KL}(t) = -2 \mathrm{Re} (A_j(t)A_j^{*}(t)
  !!    e^{i\gamma_{KL}} \sigma^{NAD}_{KL}(t)) \]
  !! \[ g_{KL} = \frac{\int_{t}^{t+dt} dt b_{LK}(t)}{|A_K(t)|^2}.\]
  !!
  !! The components of ``nx_sh_traj_t`` can be split into the
  !! ``fgeneric_c`` and ``fgeneric_r`` derived types ``nx_coeff_t``,
  !! ``nx_phase_t`` and ``nx_shprob_t`` to handle the evolution of
  !! the coefficients, phases, and probabilities respectively, with
  !! the functions defined in ``mod_integrators.f08``.

  ! !! with decoherence
  ! !! corrections from Granucci, Persico and Toniolo, J. Chem. Phys. 114
  ! !! (2001) [10.1063/1.1376633](http://dx.doi.org/10.1063/1.1376633).

  !! In this documentation we adopt the following notations:
  !!
  !! - \(T\): time of the classical trajectory ;
  !! - \(\Delta T\): time-step of the classical trajectory ;
  !! - \(t\): time of the SH trajectory ;
  !! - \(\delta t\): time-step of the SH trajectory ;
  !! - \(t_0 = T - \Delta T\): starting time for the SH trajectory ;
  !! - \(t_1 = T\): ending time for the SH trajectory.
  use mod_kinds, only: dp
  use mod_tools, only: find_index
  use mod_integrators, only: &
       & fgeneric_c, fgeneric_r
  implicit none

  private

  type, public, extends(fgeneric_c) :: nx_coeff_t
     !! Function for determining the electronic coefficient evolution.
     !!
     real(dp), allocatable, dimension(:) :: sigma
     !! Derivative couplings, generated from ``nad`` and ``veloc``, or
     !! read directly from ``nx_traj`` if ``cioverlap`` has been used.
     real(dp), allocatable, dimension(:) :: gamma
     !! Phases.
     logical :: run_complex
     !! Indicate if CS-FSSH should be used.
     real(dp), allocatable, dimension(:) :: epot_i
     !! (CS-FSSH ONLY) Imaginary part of the energy.
     real(dp), allocatable, dimension(:) :: sigma_i
     !! (CS-FSSH ONLY) Imaginary part of the derivative couplings.
   contains
     procedure, pass :: init => initialize_coeff
     procedure, pass :: ctor => constructor_coeff
     procedure, pass :: get_deriv => deriv_coeff
     procedure, pass :: rebuild => rebuild_coef
     procedure, pass :: print => print_coef
     procedure, pass :: destroy => destroy_coef
  end type nx_coeff_t


  type, public, extends(fgeneric_r) :: nx_phase_t
     !! Function for determining the phase evolution
     !!
     real(dp), allocatable, dimension(:) :: epot
     !! Current potential energies.
   contains
     procedure, pass :: init => initialize_phase
     procedure, pass :: ctor => constructor_phase
     procedure, pass :: get_deriv => deriv_phase
     procedure, pass :: rebuild => rebuild_phase
     procedure, pass :: print => print_phase
     procedure, pass :: destroy => destroy_phase
  end type nx_phase_t


  type, public, extends(fgeneric_r) :: nx_shprob_t
     !! Probabilities in the Tully - Hammes-Schiffer method.
     !!
     !! The probabilities are given as the derivatives of this function.
     complex(dp), pointer, dimension(:) :: atraj
     !! Electronic coefficients.
     real(dp), pointer,  dimension(:) :: sigma
     !! Derivative couplings, generated from ``nad`` and ``veloc``, or
     !! read directly from ``nx_traj`` if ``cioverlap`` has been used.
     real(dp), pointer, dimension(:) :: gamma
     !! Phases.
     integer :: isurf
     !! Current potential energy surface.
     logical :: run_complex
     !! Indicate if CS-FSSH should be used.
     real(dp), allocatable, dimension(:) :: epot_i
     !! (CS-FSSH ONLY) Imaginary part of the energy.
     real(dp), allocatable, dimension(:) :: sigma_i
     !! (CS-FSSH ONLY) Imaginary part of the derivative couplings.     
   contains
     procedure, pass :: init => initialize_shprob
     procedure, pass :: ctor => constructor_shprob
     procedure, pass :: get_deriv => deriv_shprob
     procedure, pass :: rebuild => rebuild_shprob
     procedure, pass :: print => print_shprob
     procedure, pass :: destroy => destroy_shprob
  end type nx_shprob_t


  type, public :: nx_sh_traj_t
     !! Object handling the TDSE trajectory.
     !!
     !! The main member is the electronic coefficient ``acoef``, and the
     !! corresponding derivative.  The electronic population can be obtained
     !! by squaring ``acoef``.  As stated in Ferretti, Granucci et al., the
     !! coefficients follow:
     !!
     !! \[\dot{A}_L = - \sum_{K \neq L}
     !!         A_K e^{-i\gamma_{KL}} \sigma^{NAD}_{KL}
     !! \],
     !!
     !! where \(\gamma_{KL}\) is the phase between states \(K\) and
     !! \(L\), and \(\sigma^{NAD}_{KL}\) is the dot product of the
     !! atomic velocity and the non-adiabatic coupling vector between
     !! states states \(K\) and \(L\).  These two quantities are also part of
     !! the object.
     !!
     !! The velocity, potential energy and non-adiabatic vectors are
     !! obtained by interpolating between the corresponding values from
     !! the main ``nx_traj`` object.  For example, at each micro-iteration,
     !! ``nx_sh_traj_t%veloc`` is the result of a linear interpolation
     !! between ``nx_traj%old_veloc`` and ``nx_traj%veloc``.
     integer :: step
     !! Microiteration step

     ! Interpolated non-adiabatic coupling vectors, velocities
     ! and energies obtained from the classical trajectory
     real(dp), dimension(:, :), allocatable :: veloc
     !! Current velocity.
     real(dp), dimension(:), allocatable :: epot
     !! Current potential energies.

     ! Electronic populations and derivative
     complex(dp), dimension(:), allocatable :: adot
     !! Time-derivative of the state coefficient.
     complex(dp), dimension(:), allocatable :: acoef
     !! State coefficient.

     integer :: isurf
     !! Current active potential energy surface

     integer :: irk
     !! Indicate the type of hopping encountered, if any:
     !!
     !! - 0 : No hopping
     !! - 1 : Surface hopping
     !! - 2 : Frustrated hopping (not enough kinetic energy)
     !! - 3 : Frustrated hopping (velocity condition)

     real(dp), dimension(:), allocatable :: sigma
     !! Derivative couplings, generated from ``nad`` and ``veloc``, or
     !! read directly from ``nx_traj`` if ``cioverlap`` has been used.
     real(dp), dimension(:), allocatable :: gamma
     !! Phases.

     ! CS-FSSH
     logical :: run_complex
     !! Indicate if complex surfaces are used or not.
     real(dp), dimension(:), allocatable :: sigma_i
     !! (CS-FSSH ONLY) Imaginary part of the derivative couplings
     real(dp), dimension(:), allocatable :: epot_i
     !! (CS-FSSH ONLY) Imaginary part of the energy (\( \Gamma \) in the equations from
     !! paper Kossoski and Barbatti, Chem. Science (2020), 11
     
     ! QM/MM
     logical :: is_qmmm
     !! If all atoms are QM = False, if there is an MM part = True
     logical, allocatable, dimension(:) :: is_qm_atom
     !! For each atom True = QM, False = MM

     logical :: initialized = .false.
     !! Indicate if the object has already been initialized or not.

   contains
     procedure :: init
     procedure :: init_qmmm
     procedure :: set_data => set_data_from_traj
     procedure :: destroy
     procedure :: copy_traj
     generic :: assignment(=) => copy_traj
     procedure :: split => split_in_components
     procedure :: compute_adot
     procedure :: build_sigma
  end type nx_sh_traj_t


contains

  subroutine init(this, nat, nstat, run_complex)
    !! Initialize a SH trajectory object.
    !!
    !! The routine sets the step number to 0, the ``isurf`` component
    !! to -1, allocates the memory for all arrays, and sets all
    !! elements to 0,
    class(nx_sh_traj_t), intent(inout) :: this
    !! This object.
    integer, intent(in) :: nat
    !! Number of atoms in the system.
    integer, intent(in) :: nstat
    !! Number of states included in the computation.
    logical, intent(in) :: run_complex
    !! Indicate if complex surfaces are used or not.

    integer :: ncoupl

    ncoupl = nstat * (nstat-1) / 2

    this%step = 0

    this%run_complex = run_complex

    !! All QM/MM quantities are dummy during the first allocation
    this%is_qmmm = .false.

    if (.not. this%initialized) then
       allocate( this%veloc(3, nat) )
       allocate( this%epot(nstat) )
       allocate( this%adot(nstat) )
       allocate( this%acoef(nstat) )
       allocate( this%gamma(ncoupl) )
       allocate( this%sigma(ncoupl) )
       allocate( this%is_qm_atom(1) )

       if (run_complex) then
          allocate( this%epot_i(nstat) )
          allocate( this%sigma_i(ncoupl) )
       else
          allocate( this%epot_i(1) )
          allocate( this%sigma_i(1) )
       end if
    end if

    this%veloc(:, :) = 0.0_dp
    this%epot(:) = 0.0_dp
    this%adot(:) = 0.0_dp
    this%acoef(:) = 0.0_dp
    this%gamma(:) = 0.0_dp
    this%sigma(:) = 0.0_dp
    this%isurf = -1
    this%irk = 0

    this%epot_i = 0.0_dp
    this%sigma_i = 0.0_dp

    this%initialized = .true.
  end subroutine init

  subroutine init_qmmm(this, is_qm_atom) 
    !! This routine enable the QM/MM part of the SH module.
    class(nx_sh_traj_t), intent(inout) :: this
    !! This object.
    logical, dimension(:), intent(in) :: is_qm_atom
    !! For each atom True = QM, False = MM
    
    integer :: nat

    this%is_qmmm = .true.
    nat = size(this%veloc, 2)

    deallocate( this%is_qm_atom )
    allocate( this%is_qm_atom(nat) )

    this%is_qm_atom(:) = is_qm_atom(:)
  end subroutine init_qmmm


  subroutine compute_adot(this)
    !! Compute the derivative of the coefficients.
    !!
    !! This function first creates a ``nx_coeff_t`` object, compute
    !! the derivative with the associated function, and then returns
    !! the result to the parent object.
    class(nx_sh_traj_t), intent(inout) :: this

    type(nx_coeff_t) :: coeff

    call coeff%ctor(size(this%epot), this%run_complex)
    call coeff%init( this%gamma, this%sigma, this%acoef, this%adot, this%epot_i, this%sigma_i )
    coeff%adot = coeff%get_deriv()

    this%adot(:) = coeff%adot(:)
  end subroutine compute_adot


  subroutine build_sigma(this, nad, nad_i, veloc, vdoth)
    !! Construct the time derivatives.
    !!
    !! The construction will depend on the ``vdoth`` keyword.  If
    !! ``vdoth`` is 0, then `` nad`` should contain non-adiabatic
    !! couplings, so ``sigma`` is obtained by making the product of
    !! these vectors with the velocity.  Else, ``nad`` already
    !! containd the time-derivatives, so they are simply transferred
    !! to the right member.
    class(nx_sh_traj_t), intent(inout) :: this
    !! SH traj object.
    real(dp), intent(in) :: nad(:, :, :)
    !! ``nad`` member from ``nx_traj_t`` object (either non-adiabatic
    !! coupling vectors or time derivatives).
    real(dp), intent(in) :: nad_i(:, :, :)
    !! ``nad_i`` member from ``nx_traj_t`` object (either non-adiabatic
    !! coupling vectors or time derivatives).
    real(dp), intent(in) :: veloc(:, :)
    !! Atomic velocities.
    integer, intent(in) :: vdoth
    !! Indicator of wether ``nad`` contains NAD vectors (``vdoth =
    !! 0``) or time derivatives (all other values).

    integer :: i, ncoupl

    ncoupl = size(nad, 1)
    if (vdoth == 0) then
       do i=1, ncoupl
          this%sigma(i) = sum(veloc(:, :) * nad(i, :, :))

          if (this%run_complex) then
             this%sigma_i(i) = sum(veloc(:, :) * nad_i(i, :, :))
          end if
          
       end do
    else
       do i=1, ncoupl
          this%sigma(i) = nad(i, 1, 1)

          if (this%run_complex) then
             this%sigma_i(i) = nad_i(i, 1, 1)
          end if
          
       end do
    end if
  end subroutine build_sigma


  subroutine set_data_from_traj(this, wf, surf, veloc)
    !! Set the initial state of the SH trajectory.
    !!
    !! The SH trajectory starts at \(t_0\), which means we should initially
    !! have the data corresponding to the classical trajectory at
    !! \(T - \Delta T\).
    class(nx_sh_traj_t), intent(inout) :: this
    !! SH trajectory.
    complex(dp), intent(in) :: wf(:)
    !! Wavefunction.
    integer, intent(in), optional :: surf
    !! Current surface.
    real(dp), intent(in), optional :: veloc(:, :)
    !! Velocities of atoms.

    this%veloc(:, :) = veloc(:, :)
    this%acoef(:) = wf(:)
    this%isurf = surf
  end subroutine set_data_from_traj


  subroutine destroy(this)
    !! Finalizer for the ``sh_traj_t_new`` object.
    !!
    class(nx_sh_traj_t), intent(inout) :: this
    !! Current object.

    deallocate( this%veloc )
    deallocate( this%epot )
    deallocate( this%adot )
    deallocate( this%acoef )
    deallocate( this%gamma )
    deallocate( this%sigma )
    deallocate( this%is_qm_atom )

    deallocate( this%sigma_i )
    deallocate( this%epot_i )
  end subroutine destroy


  subroutine copy_traj(outtraj, intraj)
    !! Copy of ``shtraj`` objects (overload the ``=`` operator).
    !!
    class(nx_sh_traj_t), intent(inout) :: outtraj
    type(nx_sh_traj_t), intent(in) :: intraj

    if (.not. outtraj%initialized) then
       call outtraj%init(size(intraj%veloc, 1), size(intraj%epot), intraj%run_complex)
    end if

    outtraj%acoef(:) = intraj%acoef(:)
    outtraj%adot(:) = intraj%adot(:)
    outtraj%sigma(:) = intraj%sigma(:)
    outtraj%epot(:) = intraj%epot(:)
    outtraj%veloc(:, :) = intraj%veloc(:, :)
    outtraj%gamma(:) = intraj%gamma(:)
    outtraj%epot_i(:) = intraj%epot_i(:)
    outtraj%sigma_i(:) = intraj%sigma_i(:)
    outtraj%isurf = intraj%isurf
    outtraj%step = intraj%step
    
    outtraj%is_qmmm = intraj%is_qmmm
    if(outtraj%is_qmmm) then
      call outtraj% init_qmmm(intraj%is_qm_atom)
    end if
  end subroutine copy_traj


  subroutine split_in_components(this, coeff, phase, shprob)
    !! Split a ``nx_sh_traj_t`` object into ``fgeneric`` components.
    !!
    !! This function is used just before integrating the TDSE, to
    !! make use of the integration schemes defined in
    !! ``mod_integrators.f08``.
    class(nx_sh_traj_t), intent(in) :: this
    !! Current SH straj object.
    type(nx_coeff_t), intent(out) :: coeff
    !! Electronic coefficients.
    type(nx_phase_t), intent(out) :: phase
    !! Phases.
    type(nx_shprob_t), intent(out) :: shprob
    !! Probabilities of hopping.

    integer :: nstat

    nstat = size(this%acoef)

    ! 1. Build phase
    call phase%ctor(nstat)
    call phase%init( this%epot, this%gamma )

    ! 2. Build coefficient
    call coeff%ctor(nstat, this%run_complex)
    call coeff%init( this%gamma, this%sigma, this%acoef, this%adot, this%epot_i, this%sigma_i )

    ! 3. Build Tully coefficient
    call shprob%ctor(nstat, this%run_complex)
    call shprob%init( this%gamma, this%sigma, this%acoef, this%isurf, this%epot_i, this%sigma_i)
  end subroutine split_in_components

  ! =============================
  ! Coefficient-related functions
  ! =============================
  subroutine constructor_coeff(func, nstat, run_complex)
    !! Constructor for the ``nx_coeff_t`` type.
    !!
    !! This routine allocates memory and initializes all members to 0.
    class(nx_coeff_t), intent(inout) :: func
    !! ``nx_coeff_t`` object constructed.
    integer, intent(in) :: nstat
    !! Number of states.
    logical, intent(in) :: run_complex
    !! Indicate if CS-FSSH is used.

    integer :: ncoupl
    ncoupl = nstat * (nstat - 1) / 2
    allocate(func%acoef(nstat))
    allocate(func%adot(nstat))
    allocate(func%gamma(ncoupl))
    allocate(func%sigma(ncoupl))

    if (run_complex) then
       allocate(func%sigma_i(ncoupl))
       allocate(func%epot_i(nstat))
    else
       allocate(func%sigma_i(1))
       allocate(func%epot_i(1))
    end if
    func%run_complex = run_complex

    func%acoef(:) = 0.0_dp
    func%adot(:) = 0.0_dp
    func%gamma(:) = 0.0_dp
    func%sigma(:) = 0.0_dp
    func%sigma_i(:) = 0.0_dp
    func%epot_i(:) = 0.0_dp
  end subroutine  constructor_coeff


  subroutine initialize_coeff(func, gamma, sigma, acoef, adot, epot_i, sigma_i)
    !! Initializer for the ``nx_coeff_t``object.
    !!
    !! This routine assign values to the members of the object
    !! according to the input arguments.
    class(nx_coeff_t), intent(inout) :: func
    !! ``nx_coeff_t`` object.
    real(dp), intent(in) :: gamma(:)
    !! Array of phases.
    real(dp), intent(in) :: sigma(:)
    !! Array of time-derivative couplings.
    complex(dp), intent(in) :: acoef(:)
    !! Electronic coefficients.
    complex(dp), intent(in) :: adot(:)
    !! Derivative of the electronic coefficients.
    real(dp), allocatable, dimension(:) :: epot_i
    !! (CS-FSSH ONLY) Imaginary part of the energy.
    real(dp), allocatable, dimension(:) :: sigma_i
    !! (CS-FSSH ONLY) Imaginary part of the derivative couplings

    func%gamma(:) = gamma(:)
    func%sigma(:) = sigma(:)
    func%acoef(:) = acoef(:)
    func%epot_i(:) = epot_i(:)
    func%sigma_i(:) = sigma_i(:)

    ! Due to decoherence corrections, we cannot relibly recompute the
    ! derivatives with the information here, as the adot obtained (which will
    ! include the correction) won't correspond to the one contained in the
    ! parent function (which do not include the correction).
    func%adot(:) = adot(:)
  end subroutine initialize_coeff


  subroutine destroy_coef(func)
    class(nx_coeff_t), intent(inout) :: func
    !! ``nx_coeff_t`` object.

    deallocate(func%gamma)
    deallocate(func%sigma)
    deallocate(func%acoef)
    deallocate(func%adot)
    deallocate(func%sigma_i)
    deallocate(func%epot_i)
  end subroutine destroy_coef


  function deriv_coeff(func, x) result (res)
    !! Compute the derivative of the coefficients.
    !!
    !! The function returns an array of the same size as the ``adot`` array.
    !! The derivatives are computed as:
    !!
    !! \[ \dot{A}_L = -\sum_K A_K \mathrm{e}^{-i\gamma_{KL}}
    !! \sigma^{NAD}_{KL} \]
    !!
    !!
    !! Usage:
    !!
    !!     this%adot(:) = this%get_deriv()
    !!
    !! ``x`` is a dummy variable necessary for general compatibility.
    class(nx_coeff_t), intent(in) :: func
    !! Current object.
    real(dp), intent(in), optional :: x
    !! Dummy variable

    complex(dp), dimension(size(func%adot)) :: res

    integer :: nsurf
    integer :: k, kl, l
    complex(dp) :: ak
    complex(dp) :: phase_kl
    real(dp) :: sigma_kl, sigma_i_kl

    nsurf = size(func%acoef)
    sigma_i_kl = 0.0_dp
    do k=1, nsurf
       ak = complex(0.0_dp, 0.0_dp)
       do l=1, nsurf
          ! kl is the index of func%nad where is found the coupling
          ! (k, l). If kl = -1, then k = l and we keep going !
          kl = find_index(k, l)

          if (k .gt. l) then
             ! print '(A,I0,A,I0,A,I0)', 'l = ', l, '; k = ', k, '; kl = ', kl
             phase_kl = complex(0.0_dp, func%gamma(kl))
             sigma_kl = func%sigma(kl)
             if (func%run_complex) then
                sigma_i_kl = func%sigma_i(kl)
             end if
             
          else if (k .lt. l) then
             ! print '(A,I0,A,I0,A,I0)', 'l = ', l, '; k = ', k, '; kl = ', kl
             phase_kl = -complex(0.0_dp, func%gamma(kl))
             sigma_kl = -func%sigma(kl)
             if (func%run_complex) then
                sigma_i_kl = func%sigma_i(kl)
             end if
             
          else
             cycle
          end if
          ! print *, 'phase_kl = ', phase_kl, ' ; sigma_kl = ', sigma_kl, ' ; sigma_i_kl = ', sigma_i_kl

          if (func%run_complex) then
             ! CS-FSSH: Add contribution from inmaginary part of derivative coupling.
             ak = ak - func%acoef(l) * exp(phase_kl) * complex(sigma_kl, sigma_i_kl)
          else
             ak = ak - func%acoef(l) * exp(phase_kl) * sigma_kl
          end if
          ! print *, 'ak = ', ak
       end do

       if (func%run_complex) then
          ! CS-FSSH: Add contribution from the dissipative term to `ak`
          res(k) = ak - complex( func%epot_i(k) / 2.0_dp, 0.0_dp) * func%acoef(k)
       else
          res(k) = ak
       end if
       
       ! print *, 'final ak = ', ak
    end do
  end function deriv_coeff


  subroutine rebuild_coef(func, prev, step)
    !! Perform a linear interpolation in ``step`` steps.
    !!
    !! The parameter ``sigma`` of ``func`` will be interpolated betwee
    !! the original set and the parameters of ``prev`` in ``step``
    !! steps.
    class(nx_coeff_t), intent(inout) :: func
    !! Object for which we want to modify the parameters.
    class(fgeneric_c), intent(in) :: prev
    !! Starting point for the interpolation.
    integer, intent(in) :: step
    !! Number of step for the interpolation.

    select type (prev)
    type is(nx_coeff_t)
       func%sigma = (func%sigma + prev%sigma) / step
       if (func%run_complex) then
          func%sigma_i = (func%sigma_i + prev%sigma_i) / step
       end if
    end select
  end subroutine rebuild_coef


  subroutine print_coef(func)
    !! Print the parameters of ``func``.
    !!
    class(nx_coeff_t), intent(in) :: func
    !! Object to print.

    write(*, *) 'GAMMA: ', func%gamma
    write(*, *) 'SIGMA: ', func%sigma
    write(*, *) 'EPOT_I: ', func%epot_i
    write(*, *) 'SIGMA_I: ', func%sigma_i
  end subroutine print_coef

  ! =======================
  ! Phase-related functions
  ! =======================
  subroutine constructor_phase(func, nstat)
    !! Constructor for the ``nx_phase_t`` type.
    !!
    !! This routine allocates memory and initializes all members to 0.
    class(nx_phase_t), intent(inout) :: func
    !! ``nx_phase_t`` object constructed.
    integer, intent(in) :: nstat
    !! Number of states.

    integer :: ncoupl

    ncoupl = nstat * (nstat - 1) / 2
    allocate(func%acoef(ncoupl))
    allocate(func%adot(ncoupl))
    allocate(func%epot(nstat))

    func%acoef(:) = 0.0_dp
    func%adot(:) = 0.0_dp
    func%epot(:) = 0.0_dp
  end subroutine  constructor_phase


  subroutine initialize_phase(func, epot, acoef)
    !! Initializer for the ``nx_phase_t``object.
    !!
    !! This routine assign values to the members of the object
    !! according to the input arguments.
    class(nx_phase_t), intent(inout) :: func
    !! ``nx_phase_t`` object.
    real(dp), intent(in) :: epot(:)
    !! Potential energies.
    real(dp), intent(in) :: acoef(:)
    !! Electronic coefficients.

    func%epot(:) = epot(:)
    func%acoef(:) = acoef(:)
    func%adot(:) = func%get_deriv()
  end subroutine initialize_phase


  subroutine destroy_phase(func)
    class(nx_phase_t), intent(inout) :: func
    !! ``nx_phase_t`` object.

    deallocate(func%epot)
    deallocate(func%acoef)
    deallocate(func%adot)
  end subroutine destroy_phase


  function deriv_phase(func, x) result(res)
    !! Compute the derivative of the coefficients.
    !!
    !! The function returns an array of the same size as the ``adot`` array.
    !! The derivatives are computed as:
    !!
    !! \[ \dot{\gamma{KL}(t)} = E_K(t) - E_L(t) \]
    !!
    class(nx_phase_t), intent(in) :: func
    !! Current object.
    real(dp), intent(in), optional :: x
    !! Dummy variable (abstract type compliance).

    real(dp), dimension(size(func%adot)) :: res

    integer :: k, l, kl

    kl = 1
    do k=1, size(func%epot)
       do l=1, k-1
          res(kl) = func%epot(k) - func%epot(l)
          kl = kl + 1
       end do
    end do
  end function deriv_phase


  subroutine rebuild_phase(func, prev, step)
    !! Dummy function (compliance with parent class).
    class(nx_phase_t), intent(inout) :: func
    class(fgeneric_r), intent(in) :: prev
    integer, intent(in) :: step

    continue
  end subroutine rebuild_phase

  subroutine print_phase(func)
    !! Print the parameters of ``func``.
    !!
    class(nx_phase_t), intent(in) :: func
    !! Object to print.

    write(*, *) 'EPOT: ', func%epot
  end subroutine print_phase


  ! =======================
  ! Tully-related functions
  ! =======================

  subroutine constructor_shprob(func, nstat, run_complex)
    !! Constructor for the ``nx_shprob_t`` type.
    !!
    !! This routine allocates memory and initializes all members to 0.
    class(nx_shprob_t), intent(inout) :: func
    !! ``nx_shprob_t`` object constructed.
    integer, intent(in) :: nstat
    !! Number of states.
    logical, intent(in) :: run_complex
    !! Indicate if CS-FSSH is used.

    integer :: ncoupl
    ncoupl = nstat * (nstat - 1) / 2

    allocate(func%acoef(nstat))
    allocate(func%adot(nstat))
    allocate(func%atraj(nstat))
    allocate(func%sigma(ncoupl))
    allocate(func%gamma(ncoupl))
    if (run_complex) then
       allocate(func%sigma_i(ncoupl))
       allocate(func%epot_i(nstat))
    else
       allocate(func%sigma_i(1))
       allocate(func%epot_i(1))
    end if
    func%run_complex = run_complex

    func%acoef(:) = 0.0_dp
    func%adot(:) = 0.0_dp
    func%atraj(:) = 0.0_dp
    func%sigma(:) = 0.0_dp
    func%gamma(:) = 0.0_dp
    func%isurf = 0
    func%sigma_i(:) = 0.0_dp
    func%epot_i(:) = 0.0_dp
  end subroutine  constructor_shprob


  subroutine initialize_shprob(func, gamma, sigma, atraj, isurf, epot_i, sigma_i)
    !! Initializer for the ``nx_shprob_t``object.
    !!
    !! This routine assign values to the members of the object
    !! according to the input arguments.
    class(nx_shprob_t), intent(inout) :: func
    real(dp), intent(in) :: gamma(:)
    real(dp), intent(in) :: sigma(:)
    complex(dp), intent(in) :: atraj(:)
    integer, intent(in) :: isurf
    real(dp), allocatable, dimension(:) :: epot_i
    !! (CS-FSSH ONLY) Imaginary part of the energy.
    real(dp), allocatable, dimension(:) :: sigma_i
    !! (CS-FSSH ONLY) Imaginary part of the derivative couplings

    func%gamma(:) = gamma(:)
    func%sigma(:) = sigma(:)
    func%atraj(:) = atraj(:)
    func%isurf = isurf
    func%epot_i(:) = epot_i(:)
    func%sigma_i(:) = sigma_i(:)

    ! If isurf is -1 it means that the corresponding shtraj object has not yet
    ! been used, so don't compute derivatives here, as it will raise an error.
    if (isurf /= -1) func%adot(:) = func%get_deriv()
  end subroutine initialize_shprob


  subroutine destroy_shprob(func)
    class(nx_shprob_t), intent(inout) :: func

    deallocate(func%gamma)
    deallocate(func%sigma)
    deallocate(func%atraj)
    deallocate(func%adot)
    deallocate(func%acoef)
    deallocate(func%sigma_i)
    deallocate(func%epot_i)
  end subroutine destroy_shprob


  function deriv_shprob(func, x) result(res)
    !! Compute the derivative of the coefficients.
    !!
    !! The function returns an array of the same size as the ``adot`` array.
    !! The derivatives are computed as:
    !!
    !! \[ \dot{A}_{KL}(t) = -2 \mathrm{Re} (A_j(t)A_j^{*}(t)
    !! e^{i\gamma_{KL}} \sigma^{NAD}_{KL}(t)) \]
    !!
    class(nx_shprob_t), intent(in) :: func
    real(dp), intent(in), optional :: x

    real(dp), dimension(size(func%adot)) :: res

    integer :: k, ki, isurf
    complex(dp) :: phase_ki
    real(dp) :: sigma_ki, sigma_i_ki

    res(:) = 0.0_dp
    sigma_ki = 0.0_dp
    sigma_i_ki = 0.0_dp
    phase_ki = 0.0_dp
    isurf = func%isurf
    res(isurf) = 0.0_dp

    do k=1, size(func%adot)
       if (k == isurf) then
          cycle
       else
          ki = find_index(k, isurf)
          if (k .gt. isurf) then
             phase_ki = complex(0.0_dp, func%gamma(ki))
             sigma_ki = func%sigma(ki)

             if (func%run_complex) then
                sigma_i_ki = func%sigma_i(ki)
             end if
             
          else if (k .lt. isurf) then
             phase_ki = -complex(0.0_dp, func%gamma(ki))
             sigma_ki = -func%sigma(ki)
             if (func%run_complex) then
                sigma_i_ki = func%sigma_i(ki)
             end if
          end if
          ! print *, 'k = ', k, '; i = ', isurf, '; ki = ', ki
          ! print *, 'sigma = ', sigma_ki, '; phase = ', phase_ki
          ! print *, 'coef = ', func%atraj(isurf), '; conj = ', conjg(func%atraj(k))
          res(k) = real(&
               & func%atraj(isurf) * conjg(func%atraj(k)) *&
               & exp(phase_ki)&
               & )
          ! print *, 'res(k = )', res(k)
          res(k) = -res(k) * sigma_ki * 2.0_dp

          if (func%run_complex) then
             res(k) = res(k) &
                  & + aimag(&
                  &     func%atraj(isurf) * conjg(func%atraj(k)) * exp(phase_ki)&
                  &   ) * sigma_i_ki * 2.0_dp
          end if

          if (res(k) < 0.0_dp) then
             res(k) = 0.0_dp
          end if
       end if
    end do
  end function deriv_shprob

  subroutine rebuild_shprob(func, prev, step)
    !! Dummy function (compliance with parent class).
    class(nx_shprob_t), intent(inout) :: func
    class(fgeneric_r), intent(in) :: prev
    integer, intent(in) :: step

    continue
  end subroutine rebuild_shprob


  subroutine print_shprob(func)
    !! Dummy function (compliance with parent class).
    class(nx_shprob_t), intent(in) :: func

    continue
    ! write(*, *) 'EPOT: ', func%epot
  end subroutine print_shprob

end module mod_sh_traj_t
