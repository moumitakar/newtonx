! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_cs_fssh
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Implementation of CS-FSSH
  !!
  use mod_configuration, only: nx_config_t
  use mod_interface, only: mkdir, copy
  use mod_kinds, only: dp
  use mod_logger, only: &
       & nx_log, &
       & LOG_INFO, LOG_ERROR, LOG_DEBUG, &
       & call_external, print_conf_ele
  use mod_numerics, only: uvip3p
  use mod_qm_general, only: qm_update_input, qm_run, qm_extract_from_output
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_trajectory, only: nx_traj_t
  use mod_tools, only: to_str
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  type, public :: nx_csfssh_params_t
     real(dp), allocatable :: ref_resonance(:, :, :)
     integer :: gamma_model = 0
     character(len=:), allocatable :: path_gamma_model
     integer :: stat = 0
     real(dp) :: ress_shift = 0.0_dp
   contains
     procedure :: init => csfssh_init_type
     procedure :: run_reference => csfssh_run_reference_job
     procedure :: get_resonance => csfssh_compute_gamma
     procedure :: print => csfssh_print
  end type nx_csfssh_params_t
  
contains

  subroutine csfssh_init_type(self, conf)
    class(nx_csfssh_params_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf

    self%gamma_model = conf%gamma_model
    self%path_gamma_model = ''

    if (self%gamma_model == 1) then
       self%path_gamma_model = trim(conf%path_gamma_model)
    else if (self%gamma_model == 2) then
       call csfssh_init_gamma_model_2(self, conf)
    end if
  end subroutine csfssh_init_type


  subroutine csfssh_print(self, out)
    class(nx_csfssh_params_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output, i, j, k

    output = stdout
    if (present(out)) output = out

    write(output, '(A80)') repeat('*', 80)
    write(output, *) 'CS-FSSH CONFIGURATION'
    write(output, *) ''

    call print_conf_ele(self%gamma_model, 'Gamma Model', unit=out)
    if (self%gamma_model == 1) then
       call print_conf_ele('./'//self%path_gamma_model, 'Gamma computed with', unit=output)
    else if (self%gamma_model == 2) then
       do i=1, size(self%ref_resonance, 1)
          call print_conf_ele(i, 'Gamma reference for state ', unit=output)
          do j=1, size(self%ref_resonance, 3)
             write(output, '(2F20.12)') &
                  & self%ref_resonance(i, 1, j), self%ref_resonance(i, 2, j)
          end do
       end do
    end if
    write(output, '(A80)') repeat('*', 80)
    write(output, '(A)') ''
  end subroutine csfssh_print


  subroutine csfssh_init_gamma_model_2(self, conf)
    type(nx_csfssh_params_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf

    integer :: u, n, nlines, i, j, k, ierr

    nlines = -1

    ! Number of points in each file (must be the same !)
    do i=1, conf%nstat
       n = 0
       open(newunit=u, file='gamma_state'//to_str(i), action='read')
       do
          read(u, *, iostat=ierr)
          if (ierr /= 0) exit
          n = n + 1
       end do
       close(u)

       if (nlines < 0) nlines = n

       if (n /= nlines) then
          call nx_log%log(LOG_ERROR, &
               'CS-FSSH: Error in parsing gamma_state: the files must have the same size' &
               & )
          self%stat = -1
          return
       end if
    end do

    allocate( self%ref_resonance( conf%nstat, 2, nlines ))
    do i=1, conf%nstat
       open(newunit=u, file='gamma_state'//to_str(i), action='read')
       do j=1, nlines
          read(u, *, iostat=ierr) (self%ref_resonance(i, k, j), k=1, 2)
       end do
       close(u)
    end do

    self%ress_shift = conf%ress_shift
  end subroutine csfssh_init_gamma_model_2
    

  function csfssh_interpolate_gamma(ref_resonance, scaled_en) result(res)
    real(dp), intent(in) :: ref_resonance(:, :, :)
    real(dp), intent(in) :: scaled_en(:)

    real(dp) :: res( size(scaled_en) )
    
    real(dp) :: xa( size(ref_resonance, 3) ), ya( size(ref_resonance, 3) )
    ! real(dp) :: x( size(epot) )
    integer :: n, i
    
    n = size(ref_resonance, 3)

    do i=1, size(scaled_en)
       ya(:) = ref_resonance(i, 2, :)
       xa(:) = ref_resonance(i, 1, :)
       call uvip3p(3, n, xa, ya, 1, [scaled_en(i)], res(i))
    end do
  end function csfssh_interpolate_gamma


  subroutine csfssh_compute_gamma(self, traj)
    class(nx_csfssh_params_t), intent(inout) :: self
    type(nx_traj_t), intent(inout) :: traj

    integer :: i, ierr, u
    real(dp), allocatable :: scaled_en(:)

    ! This is only necessary when the QM job alone does not provide gamma. Else, this is
    ! already done in the `qm_update_traj` routine from `mod_qm_general`.
    if (self%gamma_model /= 0) then
       traj%old_gamma(3, :) = traj%old_gamma(2, :)
       traj%old_gamma(2, :) = traj%old_gamma(1, :)
       traj%old_gamma(1, :) = traj%gamma(:)

       if (self%gamma_model == 1) then
          call call_external('./'//self%path_gamma_model, self%stat, outfile='gamma_model.log')

          open(newunit=u, file='gamma', action='read')
          do i=1, size( traj%epot )
             read(u, *) traj%gamma(i)
          end do
          close(u)
       else if (self%gamma_model == 2) then
          allocate( scaled_en( size(traj%epot) ) )
          scaled_en(:) = traj%epot(:) - traj%epot_ref - self%ress_shift
          call nx_log%log(LOG_DEBUG, 'CS-FSSH: Reference energy = '//to_str(traj%epot_ref, fmt='(F20.12)'))
          call nx_log%log(LOG_DEBUG, 'CS-FSSH: Resonance shift  = '//to_str(self%ress_shift, fmt='(F20.12)'))
          call nx_log%log(LOG_DEBUG, scaled_en, title='Scaled energies for interpolation')
          traj%gamma(:) = csfssh_interpolate_gamma(self%ref_resonance, scaled_en)
       end if
    end if
    call traj%print_gamma_all()
  end subroutine csfssh_compute_gamma
  

  subroutine csfssh_run_reference_job(self, nx_qm, conf, traj)
    class(nx_csfssh_params_t), intent(in) :: self
    type(nx_qm_t), intent(inout) :: nx_qm
    type(nx_config_t), intent(inout) :: conf
    type(nx_traj_t), intent(inout) :: traj

    logical :: ext

    integer :: nstatdyn_bck, nstat_bck, read_nac_bck, compute_nac_bck, ierr
    character(len=:), allocatable :: job_folder_bck, msg

    type(nx_qminfo_t) :: qminfo

    ! The folder `ref_calc` is created by the routine `create_directories` from
    ! `mod_md_utils`.

    job_folder_bck = nx_qm%job_folder
    nstat_bck = conf%nstat
    nstatdyn_bck = traj%nstatdyn
    read_nac_bck = nx_qm%read_nac
    compute_nac_bck = nx_qm%compute_nac

    traj%nstatdyn = 1
    conf%nstat = 1
    nx_qm%job_folder = '../REF/'
    nx_qm%read_nac = 0
    nx_qm%compute_nac = 0

    ierr = mkdir('ref_calc')
    ierr = chdir('ref_calc')

    call nx_log%log(LOG_DEBUG, 'CS-FSSH: Updating inputs for reference job (ref_calc/)')
    call qm_update_input(nx_qm, traj, conf)

    call nx_log%log(LOG_DEBUG, 'CS-FSSH: Running QM for reference job (ref_calc/)')
    ierr = copy('mocoef', 'mocoef.start')
    call qm_run(nx_qm, conf, traj)

    call nx_log%log(LOG_DEBUG, 'CS-FSSH: Extracting info for reference job (ref_calc/)')
    qminfo = nx_qminfo_t(1, conf%nat, conf%dc_method, .true.)
    qminfo%is_reference = .true.
    call qm_extract_from_output(nx_qm, traj, conf, qminfo)

    msg = 'Epot from Reference job: '//to_str(qminfo%repot(1), fmt='(F20.12)')
    call nx_log%log(LOG_INFO, msg)

    if (conf%same_mo) then
       ierr = copy('MOCOEFS/mocoef_mc.sp', '../mocoef_ref')
    end if

    ! Report
    if (qminfo%dim_dataread > 0) then
       call nx_log%log(&
            & LOG_INFO, qminfo%dataread(2:qminfo%dim_dataread), title=qminfo%dataread(1))
    end if

    ierr = chdir('../')

    traj%epot_ref_old(3, 1) = traj%epot_ref_old(2, 1)
    traj%epot_ref_old(2, 1) = traj%epot_ref_old(1, 1)
    traj%epot_ref_old(1, 1) = traj%epot_ref
    traj%epot_ref = qminfo%repot(1)

    ! Restore variables
    traj%nstatdyn = nstatdyn_bck
    conf%nstat = nstat_bck
    nx_qm%job_folder = job_folder_bck
    nx_qm%read_nac = read_nac_bck
    nx_qm%compute_nac = compute_nac_bck
  end subroutine csfssh_run_reference_job
  
end module mod_cs_fssh
