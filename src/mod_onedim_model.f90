module mod_onedim_model
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Collection of 1D-models
  !!
  use mod_constants, only: &
       & pi
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_kinds, only: dp
  use mod_logger, only: &
       & nx_log, &
       & LOG_DEBUG, LOG_ERROR, &
       & print_conf_ele
  use mod_tools, only: to_str
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  type, public :: nx_onedim_t
     integer :: onedim_mod = 1
     real(dp), allocatable :: parm(:)
   contains
     procedure :: setup => onedim_setup
     procedure :: print => onedim_print
     procedure :: get_model_name => onedim_model_name
     procedure :: run => onedim_run
     procedure, private :: get_v_dv => onedim_get_v_dv
     procedure, private :: compute => onedim_compute
     procedure, private :: allocate => onedim_allocate
  end type nx_onedim_t


  integer, parameter :: ONED_SAC = 1
  !! Simple avoided crossing model
  integer, parameter :: ONED_DAC = 2
  !! Dual avoided crossing model
  integer, parameter :: ONED_EXT_REFL = 3
  !! Extended coupling with reflection
  integer, parameter :: ONED_DOUBLE_ARCH = 4
  !! Double arch
  integer, parameter :: ONED_NIKITIN = 4
  !! Nikitin Hamiltonian

contains

  subroutine onedim_setup(self, parser, control)
    !! Setup the 1D model.
    !!
    !! The model index is found in ``filename``, with the namelist
    !! ``onedim_model``. The ``onedim_parm`` array is then populated
    !! according to the content of ``control``, containing the
    !! namelist ``onedim_parameters``.
    class(nx_onedim_t), intent(inout) :: self
    !! QM setup.
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in), optional :: control
    !! File with the model parameters.

    real(dp), allocatable :: parm(:)
    namelist /onedim_parameters/ parm

    integer :: u
    real(dp) :: dum
    character(len=2) :: dumc

    call set(parser, 'onedim_model', self%onedim_mod, 'onedim_mod')

    if (allocated(self%parm)) deallocate(self%parm)
    call self%allocate()

    allocate( parm( size(self%parm) ) )
    open(newunit=u, file=control, status='old', action='read')
    read(u, nml=onedim_parameters)
    close(u)

    self%parm(:) = parm(:)
  end subroutine onedim_setup


  subroutine onedim_print(self, out)
    !! Print the configuration of the model.
    !!
    !! All paramters are printed on screen.
    class(nx_onedim_t), intent(in) :: self
    integer, intent(in), optional :: out

    character(len=:), allocatable :: name
    integer :: i

    integer :: output

    output = stdout
    if (present(out)) output = out

    write(output, *) 'ENTERING HERE'

    write(output, '(A)') self%get_model_name()
    write(output, *) ''

    do i=1, size(self%parm)
       call print_conf_ele(self%parm(i), 'Parameter '//to_str(i), unit=output)
    end do
  end subroutine onedim_print


  subroutine onedim_run(self, r, epot, grad, nad)
    !! Run the 1D model.
    !!
    !! First, the potential ``v`` and its derivative ``dv`` are
    !! computed, and used for computing the energies, gradients and
    !! non-adiabatic couplings.  These quantities are stored in
    !! ``nx_qm``.
    class(nx_onedim_t), intent(inout) :: self
    !! QM setup.
    real(dp), intent(in) :: r
    real(dp), intent(inout) :: epot(:)
    !! Potential derivative.
    real(dp), intent(inout) :: grad(:)
    !! Potential derivative.
    real(dp), intent(inout) :: nad(:)
    !! Potential derivative.

    real(dp) :: v(3), dv(3)

    v(:) = 0.0_dp
    dv(:) = 0.0_dp
    call self%get_v_dv(r, v, dv)
    call nx_log%log(LOG_DEBUG, v, &
         & labels=['V11', 'V12', 'V22'])
    call nx_log%log(LOG_DEBUG, dv, &
         & labels=['dV11', 'dV12', 'dV22'])

    
    call self%compute(v, dv, epot, grad, nad)
  end subroutine onedim_run


  ! subroutine onedim_read(nx_qm, repot, rgrad, rnad)
  !   !! Transfer the quantities from ``nx_qm``.
  !   !!
  !   !! The routine populates the ``repot``, ``rgrad`` and ``rnad``
  !   !! with the corresponding content from ``nx_qm``.
  !   type(nx_qm_t), intent(in) :: nx_qm
  !   !! QM setup.
  !   real(dp), dimension(:), intent(inout) :: repot
  !   !! Potential energy.
  !   real(dp), dimension(:, :, :), intent(inout) :: rgrad
  !   !! Gradient.
  !   real(dp), dimension(:, :, :), intent(inout) :: rnad
  !   !! Non-adiabatic couplings.
  ! 
  !   ! Transfer potential energy
  !   repot(:) = nx_qm%onedim_epot(:)
  ! 
  !   ! Transfer gradients
  !   rgrad = 0.0_dp
  !   rgrad(1, 1, 1) = nx_qm%onedim_grad(1)
  !   rgrad(2, 1, 1) = nx_qm%onedim_grad(2)
  ! 
  !   ! Transfer NAD
  !   rnad(:, :, :) = 0.0_dp
  !   rnad(1, 1, 1) = nx_qm%onedim_nad(2)
  ! end subroutine onedim_read


  subroutine onedim_allocate(self)
    !! Allocate the memory for the ``parm`` array.
    !!
    !! The allocation is based on the value of ``nx_qm%onedim_mod``.
    !! If no model has been defined (i.e. ``onedim_mod`` does not
    !! correspond to any defined ``ONED_`` models), the computation
    !! is aborted.
    class(nx_onedim_t), intent(inout) :: self


    if (self%onedim_mod == ONED_SAC) then
       allocate(self%parm(4))
    else if (self%onedim_mod == ONED_DAC) then
       allocate(self%parm(5))
    else if (self%onedim_mod == ONED_EXT_REFL) then
       allocate(self%parm(3))
    else if (self%onedim_mod == ONED_DOUBLE_ARCH) then
       allocate(self%parm(4))
    else if (self%onedim_mod == ONED_NIKITIN) then
       allocate(self%parm(5))
    else
       allocate(self%parm(0))
       call nx_log%log(LOG_ERROR, &
            & 'ONEDIM: Cannot allocate parameters (unknowm model '&
            & //to_str( self%onedim_mod )//')')
       error stop
    end if
    self%parm(:) = 0.0_dp
  end subroutine onedim_allocate


  pure function onedim_model_name(self) result(res)
    !! Get the name of the model.
    !!
    class(nx_onedim_t), intent(in) :: self
    !! QM setup.
    character(len=:), allocatable :: res
    !! Name of the model.

    if (self%onedim_mod == ONED_SAC) then
       res = 'Simple avoided crossing'
    else if (self%onedim_mod == ONED_DAC) then
       res = 'Double avoided crossing'
    else if (self%onedim_mod == ONED_EXT_REFL) then
       res = 'Extended coupling with reflection'
    else if (self%onedim_mod == ONED_DOUBLE_ARCH) then
       res = 'Double arch'
    else if (self%onedim_mod == ONED_NIKITIN) then
       res = 'Nikitin Hamiltonian'
    end if
  end function onedim_model_name


  subroutine onedim_get_v_dv(self, r, v, dv)
    !! Compute the potential and its derivative.
    !!
    !! As different models will have different number of parameters
    !! with different names, a ``block`` is assigned to each method
    !! for clarity.
    class(nx_onedim_t), intent(in) :: self
    real(dp), intent(in) :: r
    !! QM setup.
    real(dp), intent(out) :: v(3)
    !! Potential.
    real(dp), intent(out) :: dv(3)
    !! Potential derivative.

    if (self%onedim_mod == ONED_SAC) then
       block
         real(dp) :: a, b, c, d
         a = self%parm(1)
         b = self%parm(2)
         c = self%parm(3)
         d = self%parm(4)
         if (r >= 0) then
            v(1) = a * (1.0_dp - exp(-b * r))
            dv(1) = a * b * exp(-b * r)
         else
            v(1) = -a * (1.0_dp - exp(b * r))
            dv(1) = a * b * exp(b * r)
         end if
         v(3) = -v(1)
         dv(3) = -dv(1)
         v(2) = c * exp(-d * r**2)
         dv(2) = -2.0_dp * d * r * v(2)
       end block

    else if (self%onedim_mod == ONED_DAC) then
       block
         real(dp) :: a, b, c, d, e0
         a = self%parm(1)
         b = self%parm(2)
         c = self%parm(3)
         d = self%parm(4)
         e0 = self%parm(5)

         v(1) = 0.0_dp
         v(3) = -a * exp(-b * r**2) + e0
         v(2) = c * exp(-d * r**2)

         dv(1) = 0.0_dp
         dv(3) = -2.0_dp * b * r * (v(3)-e0)
         dv(2) = -2.0_dp * d * r * v(2)
       end block

    else if (self%onedim_mod == ONED_EXT_REFL) then
       block
         real(dp) :: a, b, c
         a = self%parm(1)
         b = self%parm(2)
         c = self%parm(3)

         v(1) = a
         v(3) = -a
         dv(1) = 0.0_dp
         dv(3) = 0.0_dp

         if (r <= 0) then
            v(2) = b*exp(c*r)
            dv(2) = c*v(2)
         else
            v(2) = b*(2.0_dp - exp(-c*r))
            dv(2) = b*c*exp(-c*r)
         end if
       end block

    else if (self%onedim_mod == ONED_DOUBLE_ARCH) then
       block
         real(dp) :: a, b, c, z
         a = self%parm(1)
         b = self%parm(2)
         c = self%parm(3)
         z = self%parm(4)

         v(1) = -a
         v(3) = a
         dv(1) = 0.0_dp
         dv(3) = 0.0_dp

         if (r <= -z) then
            v(2) = b * (-exp( c*(r-z) ) + exp( c*(r+z) ))
            dv(2) = b*c*(-exp(c*(r-z)) + exp(c*(r+z)))
         else if ((r > -z) .and. (r <= z)) then
            v(2) = b * (-exp( c*(r-z) ) - exp( -c*(r+z) )) + 2*b
            dv(2) = b*c*(-exp(c*(r-z)) + exp(-c*(r+z)))
         else
            v(2) = b * (exp( -c*(r-z) ) - exp( -c*(r+z) ))
            dv(2) = b*c*(-exp(-c*(r-z)) + exp(-c*(r+z)))
         end if
       end block

    else if (self%onedim_mod == ONED_NIKITIN) then
       block
         real(dp) :: a, b, N, alpha, de, theta
         a = self%parm(1)
         b = self%parm(2)
         N = self%parm(3)
         alpha = self%parm(4)
         de = self%parm(5)

         theta = pi / N

         v(1) = b*exp(-alpha*r) &
              & + de*0.5_dp &
              & - a*0.5_dp*cos(theta)*exp(-alpha*r)
         v(3) = b*exp(-alpha*r) &
              & - de*0.5_dp &
              & + a*0.5_dp*cos(theta)*exp(-alpha*r)
         v(2) = -a*0.5_dp * sin(theta) * exp(-alpha*r)

         dv(1) = (-b + a*0.5_dp * cos(theta)) &
              & * alpha * exp(-alpha*r)
         dv(3) = (-b - a*0.5_dp * cos(theta)) &
              & * alpha * exp(-alpha*r)
         dv(2) = -alpha*v(2)
       end block
    end if
  end subroutine onedim_get_v_dv


  subroutine onedim_compute(self, v, dv, epot, grad, nad)
    !! Compute the energies, gradients and NAC.
    !!
    !! The computation is carried out according to:
    !!
    !! \[ E_{1,2} = \frac{1}{2} (V_{11} + V_{22}) \mp \left (
    !! \frac{1}{4} (V_{22}-V_{11})^2 + V_{12}^2 \right )^{1/2} \]
    !!
    !! \[ G_{1,2}(x) = \frac{1}{2} \left (
    !!   \frac{dV_{11}}{dx} + \frac{dV_{22}}{dx} \right )
    !! \mp \left ( \frac{1}{4} (V_{22}-V_{11}) \left (
    !!   \frac{dV_{22}}{dx} - \frac{dV_{11}}{dx} \right ) + V_{12} \frac{dV_{12}}{dx}\right ) \left (
    !! \frac{1}{4} (V_{22}-V_{11})^2 + V_{12}^2) \right )^{-1/2} \]
    !!
    !! \[ F_{12} = \frac{1}{
    !!               1 + \left ( \frac{2V_{12}}{V_{22}-V_{11}} \right )^2 }
    !!            \left (
    !!                \frac{1}{(V_{22}-V_{11})} \frac{dV_{12}}{dx}
    !!                - \frac{V_{12}}{(V_{22}-V_{11})^2}
    !!                  \left (
    !!                     \frac{dV_{22}}{dx}
    !!                     - \frac{dV_{11}}{dx}
    !!                  \right )
    !!           \right )
    !! \]
    class(nx_onedim_t), intent(inout) :: self
    !! QM setup.
    real(dp), intent(in) :: v(:)
    !! Potential.
    real(dp), intent(in) :: dv(:)
    !! Potential derivative.
    real(dp), intent(inout) :: epot(:)
    !! Potential derivative.
    real(dp), intent(inout) :: grad(:)
    !! Potential derivative.
    real(dp), intent(inout) :: nad(:)
    !! Potential derivative.

    real(dp) :: temp

    temp = v(2)**2 + 0.25_dp * (v(3) - v(1))**2
    temp = sqrt(temp)
    epot(1) = 0.5_dp * (v(1) + v(3)) - temp
    epot(2) = 0.5_dp * (v(1) + v(3)) + temp

    temp = 1.0_dp / temp
    temp = temp &
         & * ( (v(2)*dv(2)) + 0.25_dp*(v(3)-v(1))*(dv(3)-dv(1)) )
    grad(1) = 0.5_dp * (dv(1) + dv(3)) - temp
    grad(2) = 0.5_dp * (dv(1) + dv(3)) + temp

    ! Here we compute the coupling 1/2, and we store 2/1
    temp = 1.0_dp / (1 + (2*v(2) / (v(3)-v(1)))**2) &
         & * (&
         & 1.0_dp / (v(3)-v(1)) * dv(2) &
         & - (v(2) / (v(3)-v(1))**2) * (dv(3)-dv(1))&
         & )
    nad(1) = -temp
  end subroutine onedim_compute


end module mod_onedim_model
