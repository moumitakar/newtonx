! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_diabpop
  !! author: Saikat Mukherjee <saikat.mukherjee@univ-amu.fr>
  !! date: November 1, 2020 
  !!
  !! This module calculates the dibataic population form
  !! the adiabatic population and ADT matrix as prescribed
  !! by Subotnik et. al. [J, Chem. Phys. 139, 211101 (2013)]
  !!
  use mod_kinds, only: dp
  use mod_constants, only: MAX_CMD_SIZE
  use mod_logger, only: &
       & nx_log, &
       & LOG_DEBUG
  use mod_configuration, only: nx_config_t
  use mod_trajectory, only: nx_traj_t
  use mod_qm_t, only: nx_qm_t
  implicit none

  private

  public :: nx_diabpop_t

  public :: diabpop_init_txt_files
  public :: diabpop_write_txt_files

  type nx_diabpop_t

     ! Diabatic Population 
     real(dp), allocatable :: diapop_kind1(:)
     real(dp), allocatable :: diapop_kind2(:)
     real(dp), allocatable :: diapop_kind3(:)

   contains
     procedure :: init
     procedure :: destroy
     procedure :: diabatic_population 
     procedure :: diab_potential
     procedure :: adt_matrix
     procedure :: calculate_diabpop
  endtype nx_diabpop_t


contains


  subroutine init(this, conf)
    class(nx_diabpop_t) :: this
    type(nx_config_t) :: conf

    allocate( this%diapop_kind1(conf%nstat) )
    allocate( this%diapop_kind2(conf%nstat) )
    allocate( this%diapop_kind3(conf%nstat) )

    ! Diabatic Population 
    this%diapop_kind1(:) = 0.0_dp
    this%diapop_kind2(:) = 0.0_dp
    this%diapop_kind3(:) = 0.0_dp
  end subroutine init


  subroutine destroy(this)
    class(nx_diabpop_t), intent(inout) :: this
    
    deallocate( this%diapop_kind1 )
    deallocate( this%diapop_kind2 )
    deallocate( this%diapop_kind3 )
  end subroutine destroy
  

  subroutine diabatic_population(this, traj, nx_qm, conf)
    class(nx_diabpop_t), intent(inout) :: this

    type(nx_traj_t), intent(in) :: traj
    type(nx_qm_t), intent(in) :: nx_qm
    type(nx_config_t), intent(in) :: conf

    real(dp), dimension(2, 2) :: vmat, umat

    call this% diab_potential(nx_qm, vmat)

    call this% adt_matrix(vmat, umat)

    call this% calculate_diabpop(conf, traj, umat)

  end subroutine diabatic_population


  subroutine diab_potential(this, nx_qm, vmat)
    class(nx_diabpop_t), intent(inout) :: this
    type(nx_qm_t), intent(in) :: nx_qm

    integer :: i

    real(dp) :: h11, h22, eb, esb 

    real(dp), dimension(2, 2) :: vmat

    h11 = 0.0_dp
    h22 = 0.0_dp 
    do i = 1, nx_qm%sbh_n
       eb  = 0.5_dp * (nx_qm%sbh_m(i) * nx_qm%sbh_w(i)**2 * nx_qm%sbh_r(i)**2)
       esb = nx_qm%sbh_g(i) * nx_qm%sbh_r(i)

       h11 = h11 + (eb + esb)
       h22 = h22 + (eb - esb)
    enddo

    vmat(1, 1) = h11 + nx_qm%sbh_e0 
    vmat(2, 2) = h22 - nx_qm%sbh_e0
    vmat(1, 2) = nx_qm%sbh_v0
    vmat(2, 1) = vmat(1, 2)
  end subroutine diab_potential


  subroutine adt_matrix(this, vmat, umat)
    class(nx_diabpop_t), intent(inout) :: this

    real(dp) :: theta
    real(dp), dimension(2, 2) :: vmat, umat

    character(len=MAX_CMD_SIZE) :: msg

    theta = 0.5_dp * atan(2.0_dp * vmat(1, 2) / (vmat(2, 2)-vmat(1, 1)))
    
    umat(1, 1) = cos(theta)
    umat(1, 2) = sin(theta)
    umat(2, 1) = -umat(1, 2)
    umat(2, 2) = umat(1, 1)

    write(msg, *) 'Mixing Angle Theta =', theta
    call nx_log%log(LOG_DEBUG, msg)
    call nx_log%log(LOG_DEBUG, vmat, title='Diabatic Potential Matrix')
    call nx_log%log(LOG_DEBUG, umat, title='ADT Matrix')

    ! write(*, *)'**********'
    ! write(*, *)'Mixing Angle Theta =', theta
    ! write(*, *)'Diabatic Potential Matrix:', vmat
    ! write(*, *)'ADT Matrix:', umat
  end subroutine adt_matrix


  subroutine calculate_diabpop(this, conf, traj, umat)
    class(nx_diabpop_t), intent(inout) :: this
    
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf

    integer :: i, j, ii
    integer :: lamda, kdel

    real(dp) :: sum2, sum3
    real(dp), dimension(2, 2) :: umat

    complex(dp) :: sum1, c_i, c_j, sigma 

    lamda = traj%nstatdyn          
    kdel = 0          

!   Method 1
    do ii = 1, conf%nstat  
       this%diapop_kind1(ii) = umat(ii, lamda)**2 
    end do 

!   Method 2
    do ii = 1, conf%nstat  
       sum1 = (0.0, 0.0)
       do i = 1, conf%nstat
          sum1 = sum1 + umat(ii, i) * traj%wf(i)
       enddo
       this%diapop_kind2(ii) = real( conjg(sum1) * sum1 )
    end do 

!   Method 3
    do ii = 1, conf%nstat  

       sum2 = 0.0_dp
       sum3 = 0.0_dp
       do i = 1, conf%nstat

          if (i .eq. lamda) then 
             kdel = 1
          else 
             kdel = 0
          end if

          sum2 = sum2 + umat(ii, i)**2 * kdel

          do j = 1, conf%nstat
             c_i = traj%wf(i)
             c_j = traj%wf(j)

             if (i .lt. j) then 
                sigma = c_i * conjg(c_j)
                sum3 = sum3 &
     &               + 2.0_dp* umat(ii, i)* umat(ii, j)* real(sigma)
             end if
          end do 
       end do 
       this%diapop_kind3(ii) = sum2 + sum3
    end do 

  end subroutine calculate_diabpop


  subroutine diabpop_init_txt_files()
    character(len=24), parameter :: diapopf = 'diabatic_populations.dat'

    integer :: u

    open(newunit=u, file=diapopf, action='write', status='new')
    write(u, '(A10, A20, A20, A20, A20, A20, A20)') &
         & 'Time', 'Kind1 State 1', 'Kind1 State 2', &
         & 'Kind2 State 1', 'Kind2 State 2', 'Kind3 State 1', 'Kind3 State 2'
    close(u)
  end subroutine diabpop_init_txt_files


  subroutine diabpop_write_txt_files(diabpop, traj)
    type(nx_diabpop_t), intent(in) :: diabpop
    type(nx_traj_t), intent(in) :: traj

    character(len=24), parameter :: diapopf = 'diabatic_populations.dat'

    integer :: u, i

    ! Write diabatic populations
    open(newunit=u, file=diapopf, action='write', position='append')
    write(u, '(F10.3, 999F20.12)') &
         & traj%t, (diabpop%diapop_kind1(i), i=1, size(diabpop%diapop_kind1)), &
         &         (diabpop%diapop_kind2(i), i=1, size(diabpop%diapop_kind2)), &
         &         (diabpop%diapop_kind3(i), i=1, size(diabpop%diapop_kind3))
    close(u)
  end subroutine diabpop_write_txt_files

endmodule mod_diabpop 
