! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_nad_setup
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-10-14
  !!
  !! Module for setting up NAD coupling computations in Newton-X.
  !!
  !! This module is there to decouple the setup of NAD couplings
  !! computations from the ``cioverlap`` program.
  use mod_kinds, only: dp
  use mod_configuration, only: nx_config_t
  use mod_constants, only: MAX_CMD_SIZE
  use mod_trajectory, only: nx_traj_t
  use mod_logger, only: &
       & nx_log, &
       & LOG_DEBUG, LOG_INFO, print_conf_ele
  use mod_input_parser, only: &
       & parser_t, &
       & set => set_config, set_with_alloc => set_config_with_alloc
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nad_write_pairs

  type, public ::  nx_nad_t
     integer :: kross = 1
     !! Include states that are not consecutive.
     integer :: cascade = 0
     !! Include states higher than the current one.
     integer :: current = 1
     !! Include pairs with two non-active state.
     integer, dimension(:), allocatable :: never_state
     !! Never compute couplings between the active state and these states.
     integer, dimension(:), allocatable :: include_pair
     !! Always compute couplings between the active state and these
     !! states.
   contains
     procedure :: init
     procedure :: destroy
     procedure :: print
     procedure :: set_never_state => nad_set_never_state
     final :: finalizer
  end type nx_nad_t

  ! integer, parameter :: MAX_STATE = 256

  interface nx_nad_t
     module procedure constructor
  end interface nx_nad_t
contains

  function constructor() result(res)
    type(nx_nad_t) :: res

    allocate(res%never_state(1))
    allocate(res%include_pair(1))
    res%include_pair(:) = 0
    res%never_state(:) = 0
  end function constructor
  

  subroutine init(this, parser, conf)
    class(nx_nad_t), intent(inout) :: this
    !! Cioverlap object.
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(in) :: conf

    call this%set_never_state(conf)
    
    call set(parser, 'nad_setup', this%kross, 'kross')
    call set(parser, 'nad_setup', this%cascade, 'cascade')
    call set(parser, 'nad_setup', this%current, 'current')
    call set_with_alloc(parser, 'nad_setup', this%never_state, 'never_state')
    call set_with_alloc(parser, 'nad_setup', this%include_pair, 'include_pair')
  end subroutine init


  subroutine print(this, out)
    class(nx_nad_t), intent(in) :: this
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out
    
    write(output, '(A80)') repeat('*', 80)
    write(output, *) ' Setup for NAD computation '
    write(output, *) ''
    call print_conf_ele(this%kross, 'kross', unit=output)
    call print_conf_ele(this%cascade, 'cascade', unit=output)
    call print_conf_ele(this%current, 'current', unit=output)
    write(output, '(A80)') repeat('*', 80)
    write(output, *) ' '
  end subroutine print


  subroutine destroy(this)
    class(nx_nad_t), intent(inout) :: this

    deallocate(this%never_state)
    deallocate(this%include_pair)
  end subroutine destroy


  subroutine finalizer(this)
    type(nx_nad_t), intent(inout) :: this

    deallocate(this%never_state)
    deallocate(this%include_pair)
  end subroutine finalizer


  subroutine nad_set_never_state(self, conf)
    class(nx_nad_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf

    ! if (allocated(self%never_state)) deallocate(self%never_state)
    ! allocate(self%never_state(1))
    if (conf%dc_method == 1) then
       self%never_state(1) = 0
    else
       self%never_state(1) = 1
    end if
  end subroutine nad_set_never_state
  

  subroutine nad_write_pairs(nad, conf, traj)
    !! Write the ``transmomin`` file.
    !!
    !! Determine the list of pairs of state for which the couplings
    !! will be computed by, based on informations found in the inputs.
    type(nx_nad_t), intent(in) :: nad
    !! NAD setup.
    type(nx_config_t), intent(in) :: conf
    !! Configuration object,
    type(nx_traj_t), intent(inout) :: traj
    !! Trajectory object.

    ! Pair handling
    integer :: npairs
    integer, allocatable :: pairs(:, :)
    integer, allocatable :: compute(:)

    integer :: n, i, j, u, naux

    ! Number of unique pairs
    npairs = (conf%nstat * (conf%nstat-1)) / 2
    allocate(pairs(2, npairs))
    allocate(compute(npairs))
    compute = 1

    ! Get all pairs of state (lower triangle)
    traj%couplings(:, :) = 0
    traj%couplings(1, 1) = 1
    n = 1
    do i=2, conf%nstat
       do j=1, i-1
          pairs(1, n) = i
          pairs(2, n) = j
          n = n+1
       enddo
       traj%couplings(i, i) = 1
    enddo

    call exclude_pairs_energy(compute, pairs, traj%epot, conf%thres)

    if (nad%kross == 0) then
       call exclude_pairs_kross(compute, pairs)
    end if

    if (nad%cascade > 0) then
       call exclude_pairs_cascade(compute, pairs, traj%nstatdyn)
    end if

    if (nad%current > 0) then
       call exclude_pairs_current(compute, pairs, traj%nstatdyn)
    end if

    if ((size(nad%never_state) > 1) .or. nad%never_state(1) /= 0) then
       call exclude_pairs_never_state(compute, pairs, nad%never_state)
    end if

    if (size(nad%include_pair) > 1) then
       call include_pairs(compute, pairs, nad%include_pair, traj%nstatdyn)
    end if

    ! Now report !
    open(newunit=u, file='transmomin', action='write')
    write(u, '(A2)') 'CI'
    naux = 0
    do i=1, size(compute)
       if (compute(i) == 1) then
          write(u, '(I1, 3I3)') 1, pairs(1, i), 1, pairs(2, i)
          traj%couplings(pairs(1, i), pairs(2, i)) = 1
          traj%couplings(pairs(2, i), pairs(1, i)) = 1
          naux = naux + 1
       end if
    end do
    close(u)
  endsubroutine nad_write_pairs


  subroutine exclude_pairs_energy(compute, pairs, epot, thres)
    !! Exclude pairs from ``compute`` based on energy criterion.
    integer, intent(inout), dimension(:) :: compute
    !! Vector indicating for each couple of state if the coupling
    !! will be computed (1) or not (0).
    integer, intent(in), dimension(:, :) :: pairs
    !! Full list of pairs of state.
    real(dp), intent(in), dimension(:) :: epot
    !! List of potential energies.
    real(dp), intent(in) :: thres
    !! Energy threshold.

    integer :: i
    real(dp) :: de
    character(len=MAX_CMD_SIZE) :: msg

    do i=1, size(compute)
       de = abs( epot(pairs(1, i)) - epot(pairs(2, i)) )
       if (de >= thres) then
          compute(i) = 0
          write(*, '(A40, 2I4)') &
               & '  NAD SETUP: THRES exclude pair ', pairs(1, i), pairs(2, i)
          call nx_log%log(LOG_DEBUG, msg)
       endif
    enddo
  endsubroutine exclude_pairs_energy


  subroutine exclude_pairs_kross(compute, pairs)
    !! Exclude pairs if the states are not consecutive.
    integer, intent(inout), dimension(:) :: compute
    !! Vector indicating for each couple of state if the coupling
    !! will be computed (1) or not (0).
    integer, intent(in), dimension(:, :) :: pairs
    !! Full list of pairs of state.

    integer :: i
    character(len=1024) :: msg

    do i=1, size(compute)
       if ( abs(pairs(1, i) - pairs(2, i)) > 1 ) then
          compute(i) = 0
          write(msg, '(A40, 2I4)') &
               & '  NAD SETUP: KROSS exclude pair ', pairs(1, i), pairs(2, i)
          call nx_log%log(LOG_DEBUG, msg)
       end if
    end do
  endsubroutine exclude_pairs_kross


  subroutine exclude_pairs_cascade(compute, pairs, nstatdyn)
    !! Exlude pairs if one of the state is higher than the current
    !! active state.
    integer, intent(inout), dimension(:) :: compute
    !! Vector indicating for each couple of state if the coupling
    !! will be computed (1) or not (0).
    integer, intent(in), dimension(:, :) :: pairs
    !! Full list of pairs of state.
    integer, intent(in) :: nstatdyn
    !! Current active state.

    integer :: i
    character(len=1024) :: msg

    do i=1, size(compute)
       if ( pairs(1, i) > nstatdyn) then
          compute(i) = 0
          write(msg, '(A40, 2I4)') &
               & '  NAD SETUP: CASCADE exclude pair ', pairs(1, i), pairs(2, i)
          call nx_log%log(LOG_DEBUG, msg)
       end if
    end do
  endsubroutine exclude_pairs_cascade


  subroutine exclude_pairs_current(compute, pairs, nstatdyn)
    !! Exclude pairs if one of them is not active.
    integer, intent(inout), dimension(:) :: compute
    !! Vector indicating for each couple of state if the coupling
    !! will be computed (1) or not (0).
    integer, intent(in), dimension(:, :) :: pairs
    !! Full list of pairs of state.
    integer, intent(in) :: nstatdyn
    !! Current active state.

    integer :: i
    character(len=1024) :: msg

    do i=1, size(compute)
       if ( (pairs(1, i) /= nstatdyn) .and. (pairs(2, i) /= nstatdyn)) then
          compute(i) = 0
          write(msg, '(A40, 2I4)') &
               & '  NAD SETUP: CURRENT exclude pair ',&
                  & pairs(1, i), pairs(2, i)
          call nx_log%log(LOG_DEBUG, msg)
       end if
    end do
  endsubroutine exclude_pairs_current


  subroutine exclude_pairs_never_state(compute, pairs, never_state)
    !! Exclude pairs containing elements of ``never_state``.
    integer, intent(inout), dimension(:) :: compute
    !! Vector indicating for each couple of state if the coupling
    !! will be computed (1) or not (0).
    integer, intent(in), dimension(:, :) :: pairs
    !! Full list of pairs of state.
    integer, dimension(:), intent(in) :: never_state
    !! List of state for which the coupling are not computed.

    integer :: i, k
    logical :: exclude
    character(len=1024) :: msg

    do k=1, size(never_state)
       do i=1, size(compute)
          exclude = ( (pairs(1, i) == never_state(k))&
               .or. (pairs(2, i) == never_state(k)) )
          if (exclude) then
             compute(i) = 0
             write(msg,'(A40,2I4)') &
                  "  NAD SETUP: NEVER_STATE exclude pair ", &
                  & pairs(1, i), pairs(2, i)
             call nx_log%log(LOG_DEBUG, msg)
          end if
       end do
    end do

  endsubroutine exclude_pairs_never_state


  subroutine include_pairs(compute, pairs, include_pair, nstatdyn)
    !! Include pairs containing elements of ``include_pair`` if one
    !! of the state is currently active.
    integer, intent(inout), dimension(:) :: compute
    !! Vector indicating for each couple of state if the coupling
    !! will be computed (1) or not (0).
    integer, intent(in), dimension(:, :) :: pairs
    !! Full list of pairs of state.
    integer, intent(in), dimension(:) :: include_pair
    !! List of states for which the coupling are always computed,
    !! if one of them is active.
    integer, intent(in) :: nstatdyn
    !! Current active state.

    integer :: k, i
    character(len=1024) :: msg

    do k=1, size(include_pair)-1, 2
       do i=1, size(compute)
          if (compute(i) == 0) then
             ! Check wether the (i, j) corresponds to the current state
             if ((pairs(1, i) == nstatdyn)&
                  .or. (pairs(2, i) == nstatdyn)) then
                ! Check wether the (i, j) corresponds to the required pair
                if (pairs(1, i) == include_pair(k) &
                     .and. (pairs(2, i) == include_pair(k+1))) then
                   compute(i) = 1
                   write(msg,'(A40,2I4)') &
                        "  NAD SETUP: INCLUDE_PAIR include pair ", &
                        & pairs(1, i), pairs(2, i)
                   call nx_log%log(LOG_DEBUG, msg)
                end if
                ! Check wether the (i, j) corresponds to the required pair
                if (pairs(2, i) == include_pair(k) &
                     .and. (pairs(1, i) == include_pair(k+1))) then
                   compute(i) = 1
                   write(msg,'(A40,2I4)') &
                        "  NAD SETUP: INCLUDE_PAIR include pair ", &
                        & pairs(2, i), pairs(1, i)
                   call nx_log%log(LOG_DEBUG, msg)
                end if
             end if
          end if
       end do
    end do
  endsubroutine include_pairs



  
  
end module mod_nad_setup
