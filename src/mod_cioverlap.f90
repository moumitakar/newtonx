! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_cioverlap
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-07-10
  !!
  !! This module handles the execution of the ``cioverlap`` codes to
  !! produce time-derivative for methods that do not have
  !! non-adiabatic couplings, as well as the treatment of the data it
  !! outputs. It defines the ``nx_cioverlap_t`` type.
  use mod_kinds, only: dp
  use mod_interface, only: &
       & mkdir, setenv, copy, rm
  use mod_tools, only: to_str
  use mod_constants, only: &
       & MAX_CMD_SIZE, MAX_STR_SIZE
  use mod_logger, only: &
       & nx_log, &
       & LOG_DEBUG, LOG_INFO, LOG_ERROR, &
       & print_conf_ele, check_error, call_external
  use mod_orbspace, only: nx_orbspace_t
  use mod_configuration, only: nx_config_t
  use mod_trajectory, only: nx_traj_t
  use mod_qm_t, only: nx_qm_t
  use mod_gaussian, only: &
       & gau_get_single_amplitudes, gau_get_mos_energies, gau_dump_rwf
  use mod_columbus, only: col_prepare_cio
  use mod_turbomole, only: &
       & tm_get_singles_tddft, tm_get_singles_cc2, &
       & tm_get_mos_energies, tm_ricc2_bin2matrix
  use mod_orca, only: &
       & orca_prepare_cio_files, orca_get_singles_amplitudes, &
       & orca_get_mos_energies
  use mod_overlap, only: ovl_extract_ao_ovl_and_lcao
  use mod_exash, only: calc_ovl
  use mod_input_parser, only: parser_t, &
       & set => set_config, set_realloc => set_config_realloc
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: nx_cioverlap_t
  public :: do_cioverlap

  integer, parameter :: MAX_STATE = 256
  integer, parameter :: MAX_COMMAND_SIZE = 512
  integer, parameter :: MAX_FILEN_SIZE = 512

  ! Termination codes
  integer, parameter :: CIO_ERROR_PREPARE = 21
  integer, parameter :: CIO_ERROR_EXE = 22
  integer, parameter :: CIO_ERROR_SLATERGEN = 23

  type nx_cioverlap_t
     !! This object contains information about the execution of
     !! ``cioverlap`` (executable, command line options, etc.) and
     !! about the couplings that are to be computed. It also contains
     !! the current coupling vector.
     integer :: ci_cons = 1
     !! COLUMBUS: Consolidate the wavefunction of not.
     character(len=:), allocatable :: cio_options
     !! Command line options for the ``cioverlap`` program.
     character(len=:), allocatable :: cisc_options
     !! Command line options for the ``cis_casida`` program.
     integer :: ncore = 0
     !! ``cis_casida``: Number of core orbitals to discard.
     integer :: ndisc = 0
     !! ``cis_casida``: Number of excluded virtual orbitals.
     integer :: ovl_prog = 1
     !! Program used to compute the overlap:
     !!
     !! - 1 - ``cioverlap`` by Jiri Pittner
     !! - 2 - ``cioveralp-od``
     !! - 3 - ``wfoverlap`` by Felix Plasser
     integer :: coptda = 1
     !! GAUSSIAN: Linear response vectors used in the evaluation of
     !! NAD.
     !!
     !! - 0 - |X>
     !! - 1 - |X+Y>
     integer :: blasthread = 1
     !! Number of threads for BLAS-related operations.
     character(len=:), allocatable :: read_ovl_matrix
     !! Name of the file to read the state overlap matrix from.
     integer :: generate_wf = 1
     !! Generate a CIS-like wavefunction with ``cis_casida`` program.

     character(len=:), allocatable :: cio_exe
     !! Full path of the ``ovl_prog`` program.
     !! TODO: Put this in the configuration ?

     character(len=:), allocatable :: cio_cmd
     !! Command to run cioverlap programs
     character(len=:), allocatable :: cisc_cmd
     !! Command to run cis_casida program

     ! Couplings
     real(dp), dimension(:), allocatable :: sig0
     !! Derivative coupling at t-1.5dt
     real(dp), dimension(:), allocatable :: sig1
     !! Derivative coupling at t-0.5dt

     ! NAD vectors
     real(dp), dimension(:), allocatable :: vh
     !! Current NAD couplings
     !! Not necessary to store.

   contains

     procedure :: init
     procedure :: set_ovlprog_1 => cio_set_ovlprog_1
     procedure :: set_ovlprog_2 => cio_set_ovlprog_2
     procedure :: print
     procedure :: destroy
     procedure :: prepare
     procedure :: crude_derivative_approx
     procedure :: extrapolate
     procedure :: update_sig1
     procedure :: save_nad_vectors
     procedure :: build_cmdline => cio_build_cmdline
     procedure :: build_cmdline_cis => cio_build_cmdline_cis
  endtype nx_cioverlap_t

contains

  ! -----------------------
  ! Derived-type procedures
  ! -----------------------

  subroutine init(this, config, parser)
    !! Initialize a ``nx_cioverlap_t`` object with the content of the
    !! file ``filename``, that should contain the namelist
    !! ``cioverlap``.
    class(nx_cioverlap_t), intent(inout) :: this
    !! Cioverlap object.

    type(nx_config_t), intent(in) :: config
    !! General Newton-X configuration.
    type(parser_t), intent(in) :: parser

    character(len=MAX_CMD_SIZE) :: temp_exe
    integer :: ierr
    integer :: dim

    if (config%dc_method == 1 .or. config%dc_method == 0) then
       this%ovl_prog = 0
    else if (config%dc_method == 2) then
       this%ovl_prog = 1
    end if

    ! Directly read from configuration file, if they exists
    call set(parser, 'cioverlap', this%ci_cons, 'ci_cons')
    call set(parser, 'cioverlap', this%ncore, 'ncore')
    call set(parser, 'cioverlap', this%ndisc, 'ndisc')
    call set(parser, 'cioverlap', this%ovl_prog, 'ovl_prog')
    call set(parser, 'cioverlap', this%coptda, 'coptda')
    call set(parser, 'cioverlap', this%blasthread, 'blasthread')

    if (config%progname == 'exc_mopac') then
       this%read_ovl_matrix =  'exc_overlap.out'
    else if (config%progname == 'mopac') then
       this%read_ovl_matrix =  'mop_overlap.out'
    else
       this%read_ovl_matrix =  './cioverlap/cioverlap.out'
    end if

    call set(parser, 'cioverlap', this%read_ovl_matrix, 'read_ovl_matrix')

    ! Those can depend on the value of `ovl_prog`
    if (this%ovl_prog == 1) then
       call this%set_ovlprog_1(config%progname, config%methodname)

       call set(parser, 'cioverlap', this%generate_wf, 'generate_wf')
       call set_realloc(parser, 'cioverlap', this%cisc_options, 'cisc_options')
       call set_realloc(parser, 'cioverlap', this%cio_options, 'cio_options')

    else if (this%ovl_prog == 2) then
       call this%set_ovlprog_2(config%progname, config%methodname)
       call set(parser, 'cioverlap', this%generate_wf, 'generate_wf')

       if (parser%has_key('cioverlap', 'cisc_options')) deallocate(this%cisc_options)
       call set(parser, 'cioverlap', this%cisc_options, 'cisc_options')
       this%cio_options = ''
    end if

    if (this%ovl_prog == 1 .or. this%ovl_prog == 2) then

       ! Now set the executable to use
       call get_environment_variable("CIOVERLAP", value=temp_exe, status=ierr)
       if (ierr == 1) then
          call nx_log%log(LOG_ERROR, 'CIOVERLAP environment not found !')
          error stop
       end if
       this%cio_exe = trim(temp_exe)//'/'

       ! Set OPENBLAS_NUM_THREADS
       ierr = setenv( 'OPENBLAS_NUM_THREADS', to_str(this%blasthread) )
       call check_error(ierr, CIO_ERROR_EXE, "Setting OPENBLAS_NUM_THREADS failed !")
    end if

    ! Allocate memory for couplings
    ! The dimension is the total number of couplings excluding the GS
    dim = (config%nstat - 1) * (config%nstat) / 2

    if (.not. allocated(this%sig0)) allocate( this%sig0(dim) )
    if (.not. allocated(this%sig1)) allocate( this%sig1(dim) )
    if (.not. allocated(this%vh)) allocate( this%vh(dim) )

    this%sig0 = 0.0_dp
    this%sig1 = 0.0_dp
    this%vh = 0.0_dp
  end subroutine init


  subroutine cio_set_ovlprog_1(self, progname, methodname)
    class(nx_cioverlap_t), intent(inout) :: self
    character(len=*), intent(in) :: progname
    character(len=*), intent(in) :: methodname

    if (progname == 'columbus') then
       self%cio_options = "-t 5e-4 -e 2 -i"
       self%cisc_options = ''
       self%generate_wf = 0
    else
       self%cio_options = "-s transmomin -a -t 5e-4 -e -1"

       if (progname == 'turbomole' .and. methodname /= 'tddft') then
          self%cisc_options = ''
       else
          self%cisc_options = '-o'
       end if
       self%generate_wf = 1
    end if
  end subroutine cio_set_ovlprog_1


  subroutine cio_set_ovlprog_2(self, progname, methodname)
    class(nx_cioverlap_t), intent(inout) :: self
    character(len=*), intent(in) :: progname
    character(len=*), intent(in) :: methodname

    self%cio_options = ""
    self%coptda = 1
    self%generate_wf = 1
    if (progname == 'turbomole' .and. methodname == 'tddft') then
       self%cisc_options = '-o'
    else
       self%cisc_options = '-o'
    end if
  end subroutine cio_set_ovlprog_2


  subroutine print(this, out)
    !! Print the components of the ``nx_cioverlap_t`` class.
    !!
    class(nx_cioverlap_t), intent(in) :: this
    !! ``nx_cioverlap_t`` object.
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out

    write(output, '(A80)') repeat('*', 80)
    write(output, *) 'CIOVERLAP Configuration: '
    write(output, *) ''

    call print_conf_ele(this%ci_cons, 'ci_cons', unit=output)
    call print_conf_ele(this%ncore, 'ncore', unit=output)
    call print_conf_ele(this%ndisc, 'ndisc', unit=output)
    call print_conf_ele(this%ovl_prog, 'ovl_prog', unit=output)
    call print_conf_ele(this%coptda, 'coptda', unit=output)
    call print_conf_ele(this%blasthread, 'blasthread', unit=output)
    call print_conf_ele(this%cio_options, 'cio_options', unit=output)
    call print_conf_ele(this%cisc_options, 'cisc_options', unit=output)
    call print_conf_ele(this%read_ovl_matrix, 'read_ovl_matrix', unit=output)
    call print_conf_ele(this%generate_wf, 'generate_wf', unit=output)

    ! write(*, *) ''
    ! write(*, '(a20, A)') 'cioverlap found in ', trim(this%cio_exe)
    ! write(*, *) ''
    write(output, '(A80)') repeat('*', 80)
    write(output, *) ''

  end subroutine print


  subroutine destroy(this)
    class(nx_cioverlap_t), intent(inout) :: this
    !! This object.

    deallocate( this%sig0 )
    deallocate( this%sig1 )
    deallocate( this%vh )
  end subroutine destroy


  subroutine prepare(cio, qm, conf, traj)
    !! Prepare the environment for the state overlap matrix computation.
    !!
    !! In this routine we will:
    !!
    !! 1. Create the ``cioverlap/`` folder, containing a dummy
    !! ``cioverlap.out`` file (to be used as a flag for the state
    !! overlap computation in the next step) ;
    !!
    !! 2. Generate the files required to compute the state overlap
    !! matrix or the CIS-like WF, like MO files, single amplitudes,
    !! etc.
    !!
    !! 3. If required, generate the list of Slater determinants with
    !! ``cis_slatergen``;
    !!
    !! 4. If required, generate a CIS-like WF with ``cis_casida``.
    !!
    class(nx_cioverlap_t), intent(inout) :: cio
    !! Cio object.
    type(nx_qm_t), intent(inout) :: qm
    !! ``nx_qm_t`` object corresponding to the computation.
    type(nx_config_t), intent(inout) :: conf
    !! General NX configuration.
    type(nx_traj_t), intent(inout) :: traj
    !! Current trajectory.

    logical :: is_uhf
    character(len=MAX_COMMAND_SIZE) :: env
    integer :: ierr

    integer :: u, id

    integer :: ch
    integer :: nstat_save
    real(dp), dimension(conf%nstat) :: epot_save
    character(len=1024) :: name_ind

    logical :: need_call_cio
    ! Do we need to call a program, or should the matrix be there already ? We check that
    ! by inspecting the content of `read_ovl_matrix`, as the name
    ! `cioverlap/cioverlap.out` is reserved.
    need_call_cio = .true.
    id = index(cio%read_ovl_matrix, 'cioverlap/cioverlap.out')
    if (id == 0) then
       need_call_cio = .false.
    end if

    is_uhf = .false.
    if (qm%gau_type == 1) then
       is_uhf = .true.
    end if

    call cio%build_cmdline('cioverlap.input', qm%qmcode, qm%orb)
    if (cio%generate_wf == 1) then
       call cio%build_cmdline_cis('cis_casida.input', is_uhf)
    end if

    ! At this step we also need a "dummy" ./cioverlap/cioverlap.out
    ! file so that the program knows that we need to generate the
    ! state overlap matrix. This is a bit hacky, but it is done only
    ! once !!
    if (need_call_cio) then
       ! status = mkdir('cioverlap')
       ! if (status /= 0) then
       !    write(msg, *) "Couldn't create cioverlap/, status = ", status
       !    status = ierrno()
       !    write(msg, *) trim(msg)//NEW_LINE('a')//"errno = ", status
       !    call nx_log%log(LOG_ERROR, msg)
       !    call perror('MKDIR error : ')
       !    error stop
       ! end if
       call cio_build_directories()
       if (qm%qmcode == 'exc_gaussian') then
          nstat_save = conf%nstat
          epot_save = traj%epot
          call get_environment_variable(name='NXHOME', value=env)
          do ch=1, qm%nchromexc
             write(name_ind,'(i0)') ch
             conf%nstat = qm%nstatexc(ch) + 1
             traj%epot(:) = qm%chrom_enexc(ch, :)

             block
               character(len=:), allocatable :: prefix, dir
               character(len=MAX_COMMAND_SIZE) :: to_delete(5), copy_from(2)

               prefix = 'exc_gau'//to_str(ch)
               dir = 'chrom_'//to_str(ch)
               ierr = mkdir(dir)
               ierr = copy(&
                  & [prefix//'.orbinf', prefix//'.rwf   ', prefix//'.log   '], &
                  & ['orbinf      ', 'gaussian.rwf', 'gaussian.log']&
                  & )

               call qm%orb%import_orb('orbinf')
               call cio_first_step(cio, qm, conf, traj)

               to_delete(1) = "MO_coefs_a.old"
               to_delete(2) = "TDDFT"
               to_delete(3) = "eigenvalues"
               to_delete(4) = "cis_slatergen.input"
               to_delete(5) = dir//'/cioverlap'

               ierr = rm(to_delete)
               call check_error(ierr, CIO_ERROR_PREPARE)

               copy_from(1) = 'MO_coefs_a'
               copy_from(2) = 'cioverlap/'
               ierr = copy(copy_from, dir)

               ! cmd = trim(env)//'/utils/cioverlap_exc_gaussian.pl finish '//trim(name_ind)
               ! call execute_command_line(cmd, exitstat=ierr)
               ! call check_error(ierr, CIO_ERROR_PREPARE)
             end block
          end do
          conf%nstat = nstat_save
          traj%epot = epot_save
       else
          call cio_first_step(cio, qm, conf, traj)
       end if

       open(newunit=u, file='./cioverlap/cioverlap.out', action='write')
       write(u, *) '!! First step: this is a dummy file !!'
       close(u)
    end if
  end subroutine prepare


  subroutine cio_first_step(cio, qm, conf, traj)
    type(nx_cioverlap_t), intent(inout) :: cio
    !! Cio object.
    type(nx_qm_t), intent(in) :: qm
    !! ``nx_qm_t`` object corresponding to the computation.
    type(nx_config_t), intent(in) :: conf
    !! General NX configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Current trajectory.

    logical :: is_uhf

    real(dp), allocatable, dimension(:) :: mos
    real(dp), allocatable, dimension(:, :, :) :: tia

    is_uhf = .false.
    if (qm%gau_type == 1) then
       is_uhf = .true.
    end if
    ! Prepare the files required
    call cio_prepare_files(cio, qm, conf, traj)

    if (cio%generate_wf == 1) then
       ! If we use single reference methods (i.e. not Columbus), we
       ! will need either a CIS-like wavefunction (``cioverlap``), or
       ! access to the singles amplitudes (``cioverlap-od``).
       ! The routine ALLOCATES ``mos`` and ``tia`` arrays.
       call cio_get_singles_and_mos(qm, conf%nstat, cio%coptda, .false.,&
            & mos, tia)

       if (cio%ovl_prog == 2) then
          call cio_write_tddft_all(qm, conf%nstat, tia, 'cioverlap/tddft.current')
       end if

       ! Generate the list of Slater determinants
       call cis_slatergen(cio, qm%orb, is_uhf)
       call cio_generate_wf(cio, qm%orb, conf%nstat, traj%epot, mos, tia, .false.)
    end if
  end subroutine cio_first_step


  subroutine extrapolate(this)
    !! Extrapolate the derivative couplings.
    !!
    !! This subroutine also updates \(\sigma(t)\) with the content of
    !! \(\sigma(t-dt)\).
    !! The extrapolation is carried out from the following
    !! expression, with \(vh\) being the derivative coupling:
    !! \[ vh = \frac{3 \sigma(t) - \sigma(t-dt)}{2}. \]
    class(nx_cioverlap_t), intent(inout) :: this
    !! Cioverlap object.

    this%vh = (3.0_dp*this%sig1 - this%sig0) / 2.0_dp

    ! Backup into sig0
    this%sig0 = this%sig1

  end subroutine extrapolate


  subroutine crude_derivative_approx(this, dt, cio_matrix)
    !! Approximate the derivative couplings.
    !!
    !! This subroutine is used only at the first simulation step, or
    !! when a surface hopping has occured. It also updates
    !! \(sigma(t)\) with the content of \(sigma(t-dt)\).
    !! The derivative couplings are obtained from the coefficient of
    !! the overlap matrix \(C\) as:
    !! \[ vh(ij) = -\frac{C(i, j)}{dt}, \]
    !! where \(ij\) is an index referring to the overlap between
    !! state \(i\) and state \(j\).
    class(nx_cioverlap_t), intent(inout) :: this
    !! Cioverlap object.
    real(dp), intent(in) :: dt
    !! Time-step in fs.
    real(dp), intent(in) :: cio_matrix(:, :)

    integer :: i, j, k
    ! real(dp) :: dt_conv

    ! Convert dt from fs to au
    ! dt_conv = dt / timeunit
    k = 1
    do i=2, size(cio_matrix, 1)
       do j=1, i-1
          this%vh(k) = -cio_matrix(i, j) / dt
          k = k + 1
       end do
    end do

    ! Backup into sig0
    this%sig0 = this%sig1
  end subroutine crude_derivative_approx


  subroutine save_nad_vectors(this, traj)
    !! Export the derivative couplings in the current trajectory.
    !!
    !! The content of the ``vh`` member is transformed to the shape
    !! of a non-adiabatic coupling vector with dimensions \((3, nat,
    !! ncoupl)\), and copied to the corresponding member ``nad`` from
    !! the ``nx_traj_t`` object. This subroutine also copies the
    !! current ``nad`` to ``all_nad`` in ``traj``.
    class(nx_cioverlap_t), intent(in) :: this
    !! Cioverlap object.
    type(nx_traj_t), intent(inout) :: traj
    !! Trajectory object.

    integer :: i, j, k

    ! Backup nad
    traj%old_nad = traj%nad

    ! Save new nad
    do i=1, size(traj%nad, 1)
       do k=1, size(traj%nad, 3)
          if(traj%is_qm_atom(k)) then
             do j=1, 3
                traj%nad(i, j, k) = this%vh(i)
             end do
          else
             traj%nad(i, :, k) = 0.0
          end if
       end do
    end do
  end subroutine save_nad_vectors

  ! -----------------------
  ! General public routines
  ! -----------------------

  subroutine do_cioverlap(conf, qm, cio, traj)
    !! General routine to handle cioverlap execution.
    !!
    !! This routine gathers the workflow for generating derivative
    !! couplings with cioverlap. Everything related to specific QM
    !! code is handled internally with information from the ``qm`` object.
    !! The following steps are performed sequentially:
    !!
    !! - Generation of the Casida wavefunction;
    !! - Execution of the ``cioverlap`` program ;
    !! - Extraction of the state overlap matrix ;
    !! - Generation of the derivative couplings ;
    !! - Transfer of the derivative couplings to the ``traj`` object.
    type(nx_config_t), intent(inout) :: conf
    type(nx_qm_t), intent(inout) :: qm
    type(nx_traj_t), intent(inout) :: traj
    type(nx_cioverlap_t), intent(inout) :: cio

    integer :: u
    character(len=1024) :: msg
    logical :: ext
    integer :: ierr
    real(dp), dimension(:, :), allocatable :: cio_matrix

    ! Handling exciton models
    integer :: ch, nstat_save, ncoupl, i, n, j
    integer, dimension(:, :), allocatable :: pairs
    character(len=10) :: name_ind
    real(dp), dimension(conf%nstat) :: epot_save

    character(len=MAX_CMD_SIZE) :: env

    ! We need to decide wether we have to generate the state overlap
    ! matrix or not. This is done by checking for the presence of the
    ! ``cioverlap/cioverlap.out`` file. This is the (hardcoded)
    ! output file from cioverlap or wfoverlap.
    !
    ! In the first step, we have to create a dummy file, which is
    ! done in the ``prepare`` subroutine in this file.
    inquire(file='./cioverlap/cioverlap.out', exist=ext)
    if (ext) then

       QMMM_EXC: if (qm%qmcode == 'exc_gaussian') then
         epot_save(:) = traj%epot(:)
         nstat_save = conf%nstat
         call get_environment_variable(name='NXHOME', value=env)
         do ch=1, qm%nchromexc
            write(name_ind,'(i0)') ch
            conf%nstat = qm%nstatexc(ch) + 1
            traj%epot(:) = qm%chrom_enexc(ch, :)
            ncoupl = (qm%nstatexc(ch)+1) * qm%nstatexc(ch) / 2
            allocate(pairs(2, ncoupl))
            pairs(:, :) = 0
            n = 1
            do i=2, conf%nstat
               do j=1, i-1
                  pairs(1, n) = i
                  pairs(2, n) = j
                  n = n+1
               enddo
            enddo

            open(newunit=u, file='transmomin', action='write')
            write(u, *) 'CI'
            do i=2, ncoupl
                write(u, *) '1 ', pairs(1, i), 1, pairs(2, i)
            end do
            close(u)

            block
              character(len=:), allocatable :: dir, prefix
              character(len=MAX_CMD_SIZE) :: copy_from(5), copy_to(5), to_delete(5), backup(2)
              dir = 'chrom_'//to_str(ch)
              prefix = 'exc_gau'//to_str(ch)

              copy_from(1) = dir//'/MO_coefs_a'
              copy_to(1)   = 'MO_coefs_a'
              copy_from(2) = prefix//'.rwf'
              copy_to(2)   = 'gaussian.rwf'
              copy_from(3) = prefix//'.orbinf'
              copy_to(3)   = 'orbinf'
              copy_from(4) = 'overlap/S_matrix'//to_str(ch)
              copy_to(4)   = 'overlap/S_matrix'
              copy_from(5) = dir//'/cioverlap/'
              copy_to(5)   = './cioverlap.old/'

              ierr = copy(copy_from, copy_to)
              call check_error(ierr, CIO_ERROR_PREPARE, 'CIO: Error in copy from chrom'//to_str(ch))
              ! cmd = trim(env)//'/utils/cioverlap_exc_gaussian.pl init '//trim(name_ind)
              ! call execute_command_line(cmd, exitstat=ierr)
              ! call check_error(ierr, CIO_ERROR_PREPARE)

              call qm%orb%import_orb('orbinf')
              call cio_get_state_ovl(cio, qm, conf, traj)

              to_delete(1) = "MO_coefs_a.old"
              to_delete(2) = "TDDFT"
              to_delete(3) = "eigenvalues"
              to_delete(4) = "cis_slatergen.input"
              to_delete(5) = dir//'/cioverlap'

              ierr = rm(to_delete)
              call check_error(ierr, CIO_ERROR_PREPARE)

              backup(1) = 'MO_coefs_a'
              backup(2) = 'cioverlap/'
              ierr = copy(backup, dir)

              ! cmd = trim(env)//'/utils/cioverlap_exc_gaussian.pl finish '//trim(name_ind)
              ! call execute_command_line(cmd, exitstat=ierr)
              ! call check_error(ierr, CIO_ERROR_PREPARE)
            end block

            deallocate(pairs)
         end do
         conf%nstat = nstat_save
         traj%epot(:) = epot_save(:)
      else
         call cio_get_state_ovl(cio, qm, conf, traj)
       end if QMMM_EXC

    end if

    !
    ! => Duda:
    !
    !if (qm%qmcode .eq. 'exc_gaussian' .or. &
    !    qm%qmcode .eq. 'exc_mopac') then
    !   call calc_ovl(qm, traj)
    !end if
    if (qm%qmcode .eq. 'exc_gaussian') then
       call calc_ovl(qm, traj)
    end if

    !!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!

    ! It is now time to extract information from the overlap matrix
    ! curstep = 'Reading state overlap matrix'
    ! write(msg, '(A30, A5)') curstep, ' ... '
    ! res = message(msg, advance='no')
    call nx_log%log(LOG_DEBUG, 'Reading state overlap matrix')
    call cio_read_output( cio%read_ovl_matrix, cio_matrix )
    ! res = message(' done')

    ! res = message('State overlap matrix')

    if (cio%ovl_prog == 2) then
       call nx_log%log(LOG_DEBUG, &
            & cio_matrix, title='State overlap matrix (before screening)')
       cio_matrix(:, :) = cio_matrix(:, :) * traj%couplings(:, :)
    end if
    call nx_log%log(LOG_INFO, cio_matrix, title='State overlap matrix')
    ! if (mod(traj%step, conf%kt) == 0) then
    !    call print_matrix(cio_matrix)
    ! end if

    ! And to generate the derivative couplings.
    ! res = message('Generation of derivative couplings:')
    msg = 'Generation of derivative couplings using'
    call cio% update_sig1(traj%dt, cio_matrix)
    if ((traj%step .eq. 1) &
         & .or. (traj%nstatdyn /= traj%old_nstatdyn)) then
       ! In this case we have changed the surface for the dynamics
       ! The crude derivative approximation for the NAD will be used
       ! res = message('Using crude derivative approximation')
       msg = trim(msg)//' crude derivative approximation'
       call nx_log%log(LOG_DEBUG, msg)
       call cio% crude_derivative_approx(traj%dt, cio_matrix)
    else
       ! No change of surface has occured, we can do a proper extrapolation
       ! res = message('Using extrapolation')
       msg = trim(msg)//' extrapolation'
       call nx_log%log(LOG_DEBUG, msg)
       call cio% extrapolate()
    end if

    call nx_log%log(LOG_INFO, cio%vh, title='Time-derivative couplings')

    call cio% save_nad_vectors(traj)
    traj%cio(:, :) = cio_matrix(:, :)

    ! write(*, *) ' '

    ! res = message('Couplings:')
    ! if (mod(traj%step, conf%kt) == 0) then
    !    call print_vector(cio% vh)
    ! end if
  end subroutine do_cioverlap


  ! -----------------------
  ! Private routines
  ! -----------------------

  subroutine cio_get_state_ovl(cio, qm, conf, traj)
    !! Compute the state overlap matrix with external programs.
    !!
    !! This routine hanlde the communication with any program
    !! (defined in ``ovl_prog``) to compute the state overlap matrix.
    !! It will:
    !!
    !! 1. Prepare the required files (dumping MOs on disk, ...)
    !! 2. If required, generate a CIS-like wavefunction.
    !! 3. If required, write the singles amplitudes on disk.
    !! 4. Write the input to the ``ovl_program`` chosen
    !! 5. Run ``ovl_program``.
    type(nx_cioverlap_t), intent(in) :: cio
    type(nx_qm_t), intent(in) :: qm
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj

    real(dp), allocatable, dimension(:) :: mos
    real(dp), allocatable, dimension(:, :, :) :: tia

    ! Prepare the files required
    call cio_build_directories()
    call cio_prepare_files(cio, qm, conf, traj)

    if (cio%generate_wf == 1) then
       ! If we use single reference methods (i.e. not Columbus), we
       ! will need either a CIS-like wavefunction (``cioverlap``), or
       ! access to the singles amplitudes (``cioverlap-od``).
       ! The routine ALLOCATES ``mos`` and ``tia`` arrays.

       call cio_get_singles_and_mos(qm, conf%nstat, cio%coptda, .false.,&
            & mos, tia)

       if (cio%ovl_prog == 2) then
          call cio_write_tddft_all(qm, conf%nstat, tia, 'cioverlap/tddft.current')
       end if


       call cio_generate_wf(cio, qm%orb, conf%nstat, traj%epot, mos, tia, .false.)
    end if

    ! We now have all the necessary information to
    call cio_write_ciovl_input(cio, qm%orb, qm%qmcode)
    call cio_run_ciovl(cio)
  end subroutine cio_get_state_ovl


  subroutine cio_prepare_files(cio, qm, conf, traj)
    !! Prepare the required files to compute the state overlap matrix.
    !!
    !! The exact process depends on the QM code used. We will always
    !! need a file containing the MOs.
    !!
    !! - COLUMBUS: Generate list of Slater determinants with
    !! ``mycipc`` (or ``cipc``), consolidate the wavefunction with
    !! ``civecconsolidate`` from the ``cioverlap`` distribution.
    !!
    !! - GAUSSIAN: Dump the MO coefficients from ``rwf`` file.
    !!
    !! - TURBOMOLE: if using RI-CC2 or ADC(2), we need to dump the singles
    !!  amplitudes from the file ``CCRE0-1--...`` to a text file,
    !!  with the !! ``bin2matrix`` program (from ``cioverlap``
    !!  distribution).
    !!
    type(nx_cioverlap_t), intent(in) :: cio
    type(nx_qm_t), intent(in) :: qm
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj

    integer :: u

    select case(qm%qmcode)
    case("turbomole")
       if ((qm%qmmethod == 'ricc2') .or. (qm%qmmethod == 'adc2')) then
          call tm_ricc2_bin2matrix(conf%nstat, qm%tm_mult, cio%cio_exe, &
               & qm%orb%nocc, qm%orb%nfrozen, qm%orb%nvirt)
       end if

    case("tinker_g16mmp")
       call gau_dump_rwf(.false.)

    case("gaussian", "exc_gaussian")
       call gau_dump_rwf(.false.)

    case("columbus")
       call col_prepare_cio(cio%cio_exe, qm%col_ci_type, cio%ci_cons, qm%orb%nelec)

    case("orca")
       call orca_prepare_cio_files( (traj%step > conf%init_step) )
    end select

    ! Supplementary file for cioverlap-od program
    if (cio%ovl_prog == 2) then
       open(newunit=u, file='tstep', action='write')
       write(u, '(I4, F21.12)') traj%step, traj%dt
       close(u)
    end if
  end subroutine cio_prepare_files


  subroutine cio_write_tddft_all(qm, nstat, tia, filename)
    !! Write the singles amplitude into ``tddft.all``.
    !!
    !! This file is required for using ``cioverlap.od``, and has the
    !! following shape:
    !!
    !!     nocc-nfrozen    nvirt
    !!     tia(1, 1, state1) tia(2, 1, state1) ...
    !!     ...
    !!     nocc-nfrozen    nvirt
    !!     tia(1, 1, state2) tia(2, 1, state2) ...
    !!     ...
    !!
    !! The routine also writes the file ``nstates`` with the
    !! following content:
    !!
    !!     nfrozen   nstat
    !!
    type(nx_qm_t), intent(in) :: qm
    integer, intent(in) :: nstat
    real(dp), intent(in) :: tia(:, :, :)
    character(len=*), intent(in) :: filename

    integer :: i, j, k
    integer :: u
    character(len=24) :: fmt_spec

    open(newunit=u, file=filename, action='write')
    write(fmt_spec, '(A1, I0, A7)') '(', nstat, 'F20.14)'
    do i=1, size(tia, 3)
       write(u, '(I0, A2, I0)') qm%orb%nocc - qm%orb%nfrozen, '  ', qm%orb%nvirt
       do k=1, size(tia, 2)
          write(u, fmt=fmt_spec) (tia(j, k, i), j=1, size(tia, 1))
       end do
    end do
    close(u)

    ! We also need the 'nstates' file
    open(newunit=u, file='nstates', action='write')
    write(u, '(I10,I10)') qm%orb%nfrozen, nstat
    close(u)
  end subroutine cio_write_tddft_all


  subroutine cio_generate_wf(cio, orb, nstat, epot, mos, tia, is_uhf, tia_b)
    !! Generate a CIS-like wavefunction with the program
    !! ``cis_casida``.
    !!
    !! The routine first writes the required input, then run the
    !! program with the help of the ``run_cis_casida.pl`` helper
    !! script. The program is run in the ``cioverlap/`` directory.
    !!
    !! ``run_cis_casida.pl`` will handle the renaming of
    !! ``cioverlap/`` into ``cioverlap.old``, and the copy of all
    !! required files.
    type(nx_cioverlap_t), intent(in) :: cio
    type(nx_orbspace_t), intent(in) :: orb
    integer, intent(in) :: nstat
    real(dp), intent(in) :: epot(:)
    real(dp), intent(in) :: mos(:)
    real(dp), intent(in) :: tia(:, :, :)
    logical, intent(in) :: is_uhf
    real(dp), intent(in), optional :: tia_b(:, :, :)

    integer :: ierr
    character(len=MAX_STR_SIZE) :: casinp
    logical :: ext

    casinp = 'cioverlap/cis_casida.input'
    if (is_uhf) then
       if (.not. present(tia_b)) then
          write(*, *) &
               & 'FATAL ERROR: tia_b should be provided for UHF !'
          error stop CIO_ERROR_PREPARE
       end if

       call write_cis_casida_input( &
            & orb, mos, tia, nstat, epot, casinp, tia_b &
            &)
    else
       call write_cis_casida_input(&
            & orb, mos, tia, nstat, epot, casinp &
            &)
    end if

    ierr = chdir('cioverlap')
    call check_error(ierr, CIO_ERROR_PREPARE, &
         & "CIO: Couldn't change directory to cioverlap/")

    call call_external(cio%cisc_cmd, ierr, outfile='cis_casida.out')
    call check_error(ierr, CIO_ERROR_PREPARE, &
         & "CIO: Error in cis_casida execution.")

    inquire(file='casidawf', exist=ext)
    if (ext) then
       ierr = rename('casidawf', 'eivectors2')
       call check_error(ierr, CIO_ERROR_PREPARE, &
            & "CIO: Error in renaming casidawf.")
    end if

    ierr = chdir('../')
    call check_error(ierr, CIO_ERROR_PREPARE, &
         & "CIO: Couldn't change directory to TEMP")

    ! call execute_command_line('rm '//trim(casinp), exitstat=ierr)
    call check_error(ierr, CIO_ERROR_PREPARE, &
         & "CIO: Couldn't delete "//trim(casinp))
  end subroutine cio_generate_wf


  subroutine cio_write_ciovl_input(cio, orb, qmcode)
    !! Write the input for the chosen ``ovl_prog``.
    !!
    !! Implemented inputs:
    !! - ``cioverlap``
    !! - ``cioverlap.od``
    !!
    type(nx_cioverlap_t), intent(in) :: cio
    type(nx_orbspace_t), intent(in) :: orb
    character(len=*), intent(in) :: qmcode

    ! MO energies, amplitude, AO to MO conversion and overlap storage
    real(dp), dimension(:, :), allocatable :: lcao
    real(dp), dimension(:, :), allocatable :: lcao_old
    real(dp), dimension(:), allocatable :: ovl

    ! Helper values
    integer :: nao, dim, dim_ovl

    nao = orb%nbas
    dim = nao*2
    dim_ovl = (dim**2 + dim) / 2

    select case (cio%ovl_prog)
    case(1:2)
       ! ``cioverlap`` and ``cioveralp.od``
       allocate( ovl(dim_ovl) )
       allocate( lcao(nao, nao) )
       allocate( lcao_old(nao, nao) )

       ovl = 0.0_dp
       lcao = 0.0_dp
       lcao_old = 0.0_dp
       select case(qmcode)
       case("columbus")
          call ovl_extract_ao_ovl_and_lcao(qmcode, lcao, lcao_old, ovl, cio%cio_exe)
       case DEFAULT
          call ovl_extract_ao_ovl_and_lcao(qmcode, lcao, lcao_old, ovl)
       end select

       ! Write the input and clean the memory
       call write_cioverlap_input(cio, orb, ovl, lcao, lcao_old, 'cioverlap.input')

       deallocate(ovl)
       deallocate(lcao)
       deallocate(lcao_old)
    end select
  end subroutine cio_write_ciovl_input


  subroutine cio_run_ciovl(cio)
    !! Run the chosen ``ovl_prog``.
    !!
    !! The routine starts by calling the helper script
    !! ``prepare_cioverlap.pl``, which will handle the directory
    !! structure if required (i.e. if it hasn't been previously taken
    !! care of by ``run_cis_casida.pl``). It will then execute
    !! ``ovl_prog`` in the ``cioverlap/`` folder.
    !!
    !! Finally, it will also delete the copy of ``cioverlap.input``
    !! present in the main working directory.
    type(nx_cioverlap_t), intent(in) :: cio

    logical :: ext
    integer :: id, ierr

    ! Create the directory structure if it doesn't exist yet, and copy everything there.
    call cio_copy_files(cio)

    !! Run the program
    id = chdir('./cioverlap/')
    call call_external(cio%cio_cmd, ierr, outfile='cioverlap.out')
    call check_error(ierr, CIO_ERROR_EXE)
    id = chdir('../')

    ! Remove leftover files
    inquire(file='cioverlap.input', exist=ext)
    if (ext) then
       call execute_command_line('rm cioverlap.input')
    end if
  end subroutine cio_run_ciovl


  subroutine cis_slatergen(this, orb, is_uhf)
    !! Wrapper around the ``cis_slatergen`` program.
    !!
    !! This subroutine writes the ``cis_slatergen`` input based on
    !! the info contained in the orbital space ``orb``, and calls
    !! ``init_slatergen.pl`` to handle the execution of the program.
    class(nx_cioverlap_t), intent(in) :: this
    !! This object.
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space.
    logical, intent(in) :: is_uhf
    !! Is it a UHF computation ?

    character(len=MAX_COMMAND_SIZE) :: cmd
    character(len=:), allocatable :: slaterinp, slaterdir

    integer :: ierr
    logical :: ext

    ! Write the input
    slaterinp = 'cis_slatergen.input'
    slaterdir = 'slatergen'
    call write_slatergen_inp(orb, slaterinp, is_uhf)

    ! Construct the command to run
    cmd = trim(this%cio_exe)//'/cis_slatergen '
    if (this%ncore /= 0) then
       write(cmd, '(A, A3, I0)') cmd, '-j ', this%ncore
    end if
    if (this%ndisc /= 0) then
       write(cmd, '(A, A3, I0)') cmd, '-I ', this%ndisc
    end if

    if (is_uhf) then
       cmd = trim(cmd)//' -U'
    end if

    ! Call the perl helper
    inquire(file=slaterdir//'/', exist=ext)
    if (.not. ext) then
       ierr = mkdir(slaterdir)
       call check_error(ierr, CIO_ERROR_SLATERGEN, msg='CIO: Cannot create slatergen/')
    end if

    ierr = copy('cis_slatergen.input', slaterdir//'/cis_slatergen.input')
    call check_error(ierr, CIO_ERROR_SLATERGEN, msg='CIO: Cannot copy cis_slatergen.input')
    cmd = trim(cmd)//' < '//slaterinp//' > cis_slatergen.out'

    ierr = chdir(slaterdir)
    call call_external(cmd, ierr)
    call check_error(ierr, CIO_ERROR_SLATERGEN, msg='CIO: Error in cis_slatergen')
    ierr = chdir('../')
  end subroutine cis_slatergen


  subroutine cio_get_singles_and_mos(qm, nstat, coptda,&
       & is_uhf, mos,&
       & tia, tia_b)
    !! Extract the singles amplitudes and MOs coefficients.
    !!
    !! The routine is used to extract information from the output of
    !! the QM job, namely the single excitations amplitudes and the
    !! MO coefficients. This information is used for creating the
    !! input to ``cis_casida`` for instance, or for producing
    !! ``tddft.all`` needed by ``cioverlap.od``.
    !!
    !! For UHF computations it will also output the excitations of
    !! $\beta$ electrons in ``tia_b``.
    ! character(len=*), intent(in) :: qmcode
    ! !! Name of the QM code used.
    ! type(nx_orbspace_t), intent(in) :: orb
    ! !! Orbital space considered.
    type(nx_qm_t), intent(in) :: qm
    integer, intent(in) :: nstat
    !! Total number of states.
    integer, intent(in) :: coptda
    !! Linear response vector used (0 - |X>, 1 - |X+Y>).
    logical, intent(in) :: is_uhf
    !! Is it a UHF computation or not ?
    real(dp), allocatable, intent(inout) :: mos(:)
    !! MO coefficients.
    real(dp), allocatable, intent(inout) :: tia(:, :, :)
    !! Single excitation amplitudes.
    real(dp), allocatable, optional, intent(inout) :: tia_b(:, :,&
         & :)
    !! Single excitation amplitudes (for $\beta$ electrons).

    logical :: ext
    integer :: i

    ! In UHF computation we need two matrices (one for alpha, and one for
    ! beta electrons.) In RHF, one is enough.
    if (is_uhf) then
       allocate( mos(qm%orb%nocc + qm%orb%nvirt + qm%orb%nocc_b + qm%orb%nvirt_b) )
       allocate( tia(qm%orb%nvirt, qm%orb%nocc - qm%orb%nfrozen, nstat-1) )
       allocate( tia_b(qm%orb%nvirt_b, qm%orb%nocc_b, nstat-1) )
    else
       allocate( mos(qm%orb%nocc + qm%orb%nvirt) )
       allocate( tia(qm%orb%nvirt, qm%orb%nocc - qm%orb%nfrozen, nstat-1) )
    end if

    select case(qm%qmcode)
    case ("turbomole")
       ! Some hack: if tddft, then we have sing_a, and for cc2 and adc2 we
       ! have bin2matrix.out...
       inquire(file='sing_a', exist=ext)
       if (ext) then
          call tm_get_singles_tddft(qm%orb, 'sing_a', tia)

       else
          inquire(file='bin2matrix.out', exist=ext)
          if (ext) then
             call tm_get_singles_cc2(qm%orb, 'bin2matrix.out', tia)
          else
             call check_error(1, CIO_ERROR_PREPARE, 'File not found !!')
          end if
       end if

       call tm_get_mos_energies('mos', mos)

    case ("gaussian","exc_gaussian")
       call gau_get_mos_energies('eigenvalues', mos, is_uhf)
       if (is_uhf) then
          call gau_get_single_amplitudes(qm%orb, 'TDDFT', coptda, tia, tia_b)
       else
          call gau_get_single_amplitudes(qm%orb, 'TDDFT', coptda, tia)
       end if

    case("orca")
       call orca_get_mos_energies('orca.molden.input', mos)
       call orca_get_singles_amplitudes(qm%orb, 'orca.cis', qm%orca_is_tda, tia)
    end select

    ! Correct the amplitudes in for the testsuite
    if (qm%is_test) then
       do i=1, size(tia, 3)
          if (tia(1, 1, i) < 0) then
             tia(:, :, i) = -1 * tia(:, :, i)
          end if
       end do
    end if
  end subroutine cio_get_singles_and_mos


  subroutine write_cioverlap_input(this, orb, ovl, lcao,&
       & lcao_old, input)
    !! Write an input for ``cioverlap`` and ``cioverlap.od`` programs.
    !!
    class(nx_cioverlap_t), intent(in) :: this
    !! This object.
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space.
    real(dp), dimension(:), intent(in) :: ovl
    !! AO overlap matrix, in vector form.
    real(dp), dimension(:, :), intent(in) :: lcao
    !! MO coefficient at the current step.
    real(dp), dimension(:, :), intent(in) :: lcao_old
    !! MO coefficient at the previous step.
    character(len=*), intent(in) :: input
    !! Name of the input file produced.

    real(dp), dimension(:, :), allocatable :: ovl_mat

    integer :: nao, id, i, j, nbas, nelec, u
    character(len=12) :: fmt_spec

    nao = orb%nbas

    

    ! Build ovl matrix
    allocate( ovl_mat(nao*2, nao*2) )
    ovl_mat = 0.0_dp
    ! call nx_log%log(LOG_DEBUG, ovl_mat, title='OVL_MATRIX')
    ! call nx_log%log(LOG_DEBUG, ovl, title='OVL_VECTOR')
    id = 1
    do i=1, nao*2
       do j=1, i
          ovl_mat(i, j) = ovl(id)
          ovl_mat(j, i) = ovl(id)
          id = id+1
       end do
    end do

    ! call nx_log%log(LOG_DEBUG, ovl_mat, title='OVL_MATRIX')

    ! Write general info
    nbas = orb%nbas
    nelec = orb%nelec
    open(newunit=u, file=input, status='new', action='write')
    write(u, '(I5, I5, I5, I5)') nbas, this%ncore, this%ndisc, nelec

    ! Write OVL matrix
    write(fmt_spec, '(A1, I0, A7)') '(', nao*2, 'F24.18)'
    write(u, '(I6, I6)') nao*2, nao*2
    do i=1, nao*2
       write(u, fmt=fmt_spec) (ovl_mat(i, j), j=1, nao*2)
    end do
    deallocate(ovl_mat)

    ! Write LCAO and LCAO old
    write(fmt_spec, '(A1, I0, A7)') '(', nao, 'F24.18)'
    write(u, '(I6, I6)') nao, nao
    do i=1, nao
       write(u, fmt=fmt_spec) (lcao_old(i, j), j=1, nao)
    end do
    write(u, '(I6, I6)') nao, nao
    do i=1, nao
       write(u, fmt=fmt_spec) (lcao(i, j), j=1, nao)
    end do
    close(u)
  end subroutine write_cioverlap_input


  subroutine cio_read_output(cio_output, cio_matrix)
    !! Read the output from ``cioverlap``.
    !!
    !! The information about overlap is stored in ``cio_matrix``.
    character(len=*), intent(in) :: cio_output
    !! Name of the output to read.
    real(dp), allocatable, intent(inout) :: cio_matrix(:, :)
    !! State overlap matrix read from ``cio_output``

    integer :: u, ierr, id, i, j
    character(len=512) :: line
    integer :: nlines, ncols

    open(newunit=u, file=cio_output, status='old', action='read')
    do
       read(u, '(A)', iostat=ierr) line
       if (ierr /= 0) exit

       id = index(line, 'CI overlap matrix')
       if (id /= 0) then

          read(u, *, iostat=ierr) nlines, ncols
          allocate(cio_matrix(nlines, ncols))
          do i=1, nlines
             read(u, *) (cio_matrix(i, j), j=1, ncols)
          end do
       end if
    end do
    close(u)
  end subroutine cio_read_output


  subroutine update_sig1(this, dt, cio_matrix)
    !! Compute ``sigma`` at \(t-0.5dt\).
    !!
    !! Store this information in ``sig1``.
    class(nx_cioverlap_t), intent(inout) :: this
    !! Cioverlap object.
    real(dp), intent(in) :: dt
    !! Time-step in fs
    real(dp), intent(in) :: cio_matrix(:, :)

    integer :: m, n, i
    ! real(dp) :: dt_conv

    ! Convert dt from fs to au
    ! dt_conv = dt / timeunit
    i = 1
    do m=2, size(cio_matrix, 1)
       do n=1, m-1
          this%sig1(i) = cio_matrix(n, m) - cio_matrix(m, n)
          this%sig1(i) = this%sig1(i) / (2.0_dp * dt)
          i = i + 1
       end do
    end do
  end subroutine update_sig1


  subroutine write_cis_casida_input(orb, mos, tia, nstat&
       &, epot, input, tia_b)
    !! Write an input for ``cis_casida``.
    !!
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space.
    real(dp), dimension(:), intent(in) :: mos
    !! MO energies.
    real(dp), dimension(:, :, :), intent(in) :: tia
    !! Single excitations amplitudes.
    integer, intent(in) :: nstat
    !! Number of states.
    real(dp), dimension(:), intent(in) :: epot
    !! Electronic potential energies
    character(len=*), intent(in) :: input
    !! Name of the input file write.
    real(dp), dimension(:, :, :), optional, intent(in) :: tia_b
    !! Single excitations amplitudes (UHF case).

    integer :: u, i, j, k
    character(len=24) :: fmt_spec
    open(newunit=u, file=input, action='write')

    ! Write info on orbital space
    if (present(tia_b)) then
       write(u, '(I4, I4, I4, I4, I4)') &
            & orb%nfrozen, orb%nocc - orb%nfrozen, orb%nvirt, &
            & orb%nocc_b, orb%nvirt_b
    else
       write(u, '(I4, I4, I4)') &
            & orb%nfrozen, orb%nocc - orb%nfrozen, orb%nvirt
    end if

    write(u, '(I4)') orb%nbas

    ! Print content of mos
    do i=1, size(mos)
       write(u, '(ES21.14)') mos(i)
    end do

    ! Print nstat and energies
    write(u, '(I3)') nstat
    write(fmt_spec, '(A1, I0, A7)') '(', nstat, 'F20.14)'
    ! write(u, fmt=fmt_spec) (epot(i), i=1, size(epot))
    write(u, fmt=fmt_spec) (epot(i), i=1, nstat)

    ! Print amplitudes
    write(fmt_spec, '(A1, I0, A7)') '(', orb%nvirt, 'F24.18)'
    do i=1, size(tia, 3)
       write(u, '(I0, A2, I0)') orb%nocc - orb%nfrozen, '  ', orb%nvirt
       do k=1, size(tia, 2)
          write(u, fmt=fmt_spec) (tia(j, k, i), j=1, size(tia, 1))
       end do
    end do

    close(u)
  end subroutine write_cis_casida_input


  subroutine write_slatergen_inp(orb, input, is_uhf)
    !! Write an input for ``cis_slatergen``.
    !!
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space.
    character(len=*), intent(in) :: input
    !! Name of the generated input.
    logical, optional :: is_uhf

    integer :: u

    open(newunit=u, file=input, action='write', status='new')
    if (is_uhf) then
       write(u, '(I5,I5,I5,I5,I5)')&
            & orb%nfrozen, orb%nocc - orb%nfrozen, orb%nvirt, &
            & orb%nocc_b, orb%nvirt_b
    else
       write(u, '(I0, A2, I0, A2, I0)')&
            & orb%nfrozen, '  ', orb%nocc - orb%nfrozen, '  ', orb&
            &%nvirt
       ! write(*, '(I0, A2, I0, A2, I0)')&
       !      & orb%nfrozen, '  ', orb%nocc - orb%nfrozen, '  ', orb%nvirt
    end if

    close(u)
  end subroutine write_slatergen_inp


  subroutine cio_copy_files(cio)
    !! Handle the directory structure for running cioverlap.
    !!
    !! The behaviour of the routine depends on wether a WF has been generated. If so, it
    !! just copies to ``cioverlap/`` directory the following files:
    !!
    !! - ``cioverlap.input``,
    !! - ``transmomin``,
    !! - ``slatergen/slaterfile``,
    !!
    !! as well as ``cioverlap.old/eivectors2`` as ``cioverlap/eivectors1``.  In this
    !! case, the handling of the different ``cioverlap/`` folders has already been taken
    !! care of at the WF generation step (``cis_casida``).
    !!
    !! If no WF is generated, then we delete ``cioverlap.old``, rename ``cioverlap`` as
    !! ``cioverlap.old``, and create a new ``cioverlap`` directory.  We then copy the
    !! following files in ``cioverlap``:
    !!
    !! - 'cioverlap.old/slaterpermfile'
    !! - 'cioverlap.old/excitlistfile'
    !! - 'cioverlap.input'
    !!
    !! Finally, for ``cioverlap.od`` we also need the files ``nstates`` and ``tstep``,
    !! and ``cioverlap.old/tddft.current`` as ``cioverlap/tddft.old``.
    type(nx_cioverlap_t), intent(in) :: cio
    !! Cioverlap configuration

    integer :: ierr
    character(len=MAX_COMMAND_SIZE), allocatable :: files_to_copy(:)
    logical :: ext

    if (cio%generate_wf == 1) then
       allocate( files_to_copy(3) )
       files_to_copy(1) = 'cioverlap.input'
       files_to_copy(2) = 'transmomin'
       files_to_copy(3) = 'slatergen/slaterfile'
       ierr = copy('cioverlap.old/eivectors2', 'cioverlap/eivectors1')
       call check_error(ierr, CIO_ERROR_PREPARE, 'CIO: Error in copy eivectors2')
    else

       ierr = copy( 'cioverlap.old/eivectors2', 'cioverlap/eivectors1' )
       call check_error(ierr, CIO_ERROR_PREPARE, 'CIO: Error in copy wavefunction')

       allocate(files_to_copy(3))
       files_to_copy(1) = 'cioverlap.old/slaterpermfile'
       files_to_copy(2) = 'cioverlap.old/excitlistfile'
       files_to_copy(3) = 'cioverlap.input'
    end if

    inquire(file='cioverlap.old/phases', exist=ext)
    if (ext) then
       ierr = copy( 'cioverlap.old/phases', 'cioverlap/phases.old' )
       call check_error(ierr, CIO_ERROR_PREPARE, 'CIO: Error in copy phases')
    end if

    ierr = copy( files_to_copy, 'cioverlap/' )
    call check_error(ierr, CIO_ERROR_PREPARE, 'CIO: Error in copy files')

    if (cio%ovl_prog == 2) then
       ierr = copy([ 'nstates', 'tstep  ' ], 'cioverlap/')
       ierr = copy('cioverlap.old/tddft.current', 'cioverlap/tddft.old')
    end if
  end subroutine cio_copy_files


  subroutine cio_build_directories()
    logical :: ext
    integer :: ierr

    inquire(file='cioverlap/cioverlap.out', exist=ext)
    if (ext)  then
       ! Delete old folder
       call execute_command_line('rm -rf cioverlap.old', exitstat=ierr)
       call check_error(ierr, CIO_ERROR_PREPARE, 'CIO: Error in deleting directory')

       ! Rename the current directory
       call execute_command_line('mv cioverlap cioverlap.old', exitstat=ierr)
       call check_error(ierr, CIO_ERROR_PREPARE, 'CIO: Error in renaming directory')
    end if

    ! Finally, create the new one
    ierr = mkdir('cioverlap')
    call check_error(ierr, CIO_ERROR_PREPARE, 'CIO: Error in making directory')
  end subroutine cio_build_directories


  subroutine cio_build_cmdline(cio, input, qmcode, orb)
    !! Construct the command to run the chosen program.
    !!
    !! For ``cioverlap.od`` (``ovl_prog = 2``), we simply check the value of
    !! ``blasthread`` to decide if we want to run in the OMP version.
    !!
    !! For ``cioverlap``, the ``-b`` flag is added (if not already present) to the option
    !! if Columbus is used (to produce the ``phase`` file).  The routine also checks if
    !! the ``-i`` flag is present in the set of options.  If so, the number of inactive
    !! orbitals ``orb%ninact`` is inserted after the ``-i`` flag.
    !!
    !! Finally, the command is completed by setting ``input`` as the standard input, and
    !! is set as ``cio%cio_cmd``.
    !!
    !! The result in the following:
    !!
    !!     cioverlap $CIO_OPTIONS [-i ninact] [-b] < input
    !!
    class(nx_cioverlap_t), intent(inout) :: cio
    !! Cioverlap configuration.
    character(len=*), intent(in) :: input
    !! Name of the cioverlap input file.
    character(len=*), intent(in) :: qmcode
    !! QM code used.
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space.

    character(len=MAX_STR_SIZE) :: tmp
    integer :: id

    tmp = trim(cio%cio_exe)

    if (cio%ovl_prog == 2) then
       ! cioverlap.od program
       if (cio%blasthread > 1) then
          tmp = trim(tmp)//'/cioverlap.od_omp'
       else
          tmp = trim(tmp)//'/cioverlap.od'
       end if
    else if (cio%ovl_prog == 1) then
       ! cioverlap program

       ! Some more processing of the command line
       if (qmcode == 'columbus') then
          ! The '-b' flag is there to produce the 'phases' file.
          id = index(cio%cio_options, '-b')
          if (id == 0) then
             tmp = trim(tmp)//'/cioverlap -b '//trim(cio%cio_options)
          end if
       else
          tmp = trim(tmp)//'/cioverlap '//trim(cio%cio_options)
       end if

       ! Check if inactive orbitals are to be included in the cioverlap command
       id = index(tmp, '-i')
       if (id /= 0) then
          block
            character(len=MAX_STR_SIZE) :: tmp_before, tmp_after
            tmp_before = tmp(1:id+1)
            tmp_after = tmp(id+2:len(tmp))

            write(tmp_before, '(A, A, I0)') &
                 & trim(tmp_before), ' ', orb%ninact
            tmp = trim(tmp_before)//' '//trim(tmp_after)
          end block
       end if
    end if

    tmp = trim(tmp)//' < '//trim(input)

    cio%cio_cmd = tmp
  end subroutine cio_build_cmdline


  subroutine cio_build_cmdline_cis(self, casinp, is_uhf)
    !! Build the command line for ``cis_casida``.
    !!
    !! The command concatenate the content of ``cio_exe`` with the command ``cis_casida``
    !! and the options from ``cisc_options``.  If ``ncore`` or ``ndisc`` are non-zero, the
    !! corresponding flags are added, as well as the ``-U`` flag for UHF compuations.
    class(nx_cioverlap_t), intent(inout) :: self
    !! Cioverlap configuration.
    character(len=*), intent(in) :: casinp
    !! Name of the input file for ``cis_casida``.
    logical, intent(in) :: is_uhf
    !! Flag to indicate if the computation is UHF or not.

    character(len=MAX_CMD_SIZE) :: tmp

    tmp = trim(self%cio_exe)//'cis_casida '//trim(self%cisc_options)
    if (self%ncore /= 0) then
       write(tmp, '(A, A4, I0)') trim(tmp), ' -i ', self%ncore
    end if
    if (self%ndisc /= 0) then
       write(tmp, '(A, A4, I0)') trim(tmp), ' -I ', self%ndisc
    end if

    if (is_uhf) then
       tmp = trim(tmp)//' -U'
    end if

    tmp = trim(tmp)//' < '//trim(casinp)

    self%cisc_cmd = tmp
  end subroutine cio_build_cmdline_cis


endmodule mod_cioverlap
