! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_qm_t
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-08-27
  !!
  !! This module defines the ``nx_qm_t`` type, which is used for
  !! handling the electronic structure computation, and to transfer
  !! information to other parts of the program (in particular, to the
  !! ``cioverlap`` part when required). The only public element this
  !! module exposes is the ``nx_qm_t`` type, and this object is only
  !! meant to be used in conjunction with the methods described in
  !! the module ``mod_qm_general``.
  !!
  !! For clarity in the code base, the electronic structure handling
  !! is divided into several different modules:
  !!
  !! - ``mod_qm_t`` just defines the main type, with internal
  !! procedures for initialization and destruction ;
  !! - ``mod_qm_general`` is the general interface between the ``nx_qm_t``
  !! type and the different programs and / or methods ;
  !! - ``mod_turbomole`` handles computations with TURBOMOLE ;
  !! - ``mod_analytical`` handles analytical models.
  use mod_kinds, only: dp
  use mod_orbspace, only: nx_orbspace_t
  use mod_configuration, only: nx_config_t
  use mod_sbh, only: nx_sbh_t
  use mod_recoherence_model, only: nx_recohmodel_t
  use mod_onedim_model, only: nx_onedim_t
  use mod_conical_intersection, only: nx_conint_t
  use mod_analytical_complex, only: nx_cs_fssh_t
  implicit none

  private

  public :: nx_qm_t

  integer, parameter :: MAX_STR_SIZE = 512

  type nx_qm_t
     !! Object containing general information about the electronic
     !! structure problem.
     !!
     !! It contains general properties such as the QM program name
     !! and (in the future) the method used, as well as some general
     !! flags useful for handling various update of info (such as
     !! reading the potential energy, see below).
     character(len=:), allocatable :: qmcode
     !! Electronic structure program name
     character(len=:), allocatable :: qmmethod
     !! QM method used (in case the handling is different, for example
     !! Turbomole with CC2, TD-DFT, ADC2, ...)
     character(len=:), allocatable :: debug_path
     !! Path to folder where to store backups (MOs, input files, ...).
     type(nx_orbspace_t) :: orb
     !! Orbital space
     integer :: update_epot = 0
     !! Indicate if the potential energies have to be updated or not.
     !! This is used in case of surface hopping, when the QM code is
     !! called for the second time.
     logical :: request_adapt_dt = .false.
     !! This is set if the QM code requires a reduction of time step for
     !! some reason
     character(len=MAX_STR_SIZE) :: job_folder = 'JOB_AD'
     !! Where to find the actual input files for the current QM
     !! computation.
     integer :: compute_nac = 0
     !! Indicate wether the NAC computation is handled by the QM code (=1)
     !! or by another mean (like cioverlap, local diabatization, ...) (=0)
     integer :: read_nac = 0
     !! Indicate wether NX should read the NAC computed by the QM code or not. This
     !! is useful for Columbus, in case we decide to use cioverlap or an auxiliary
     !! model to compute the derivatives. In Columbus, the files containing the
     !! gradients for instance can be dependent on the presence of a NAC computation.
     integer :: compute_nac_mm = 0
     !! If NAC are handled by QM code, and this is a QM/MM trajectory we
     !! can decide to compute NAC on MM atoms (1) or not (0). In the
     !! second case they are forced to zero by NX for consistency.
     real(dp), allocatable, dimension(:) :: osc_str
     !! Here we will save the oscillator strengths.

     ! For testsuite only
     logical :: is_test = .false.
     !! This is necessary to ensure that the testsuite is not affected by arbitrary phase
     !! computation (for singles amplitudes in for instance, see ``cio_get_singles_and_mos``)

     ! Overlap computation
     integer :: qmproc
     character(len=MAX_STR_SIZE), allocatable :: ovl_async(:)
     character(len=:), allocatable :: ovl_cmd
     character(len=:), allocatable :: ovl_out
     character(len=:), allocatable :: ovl_post

     ! Code specific attributes
     ! TURBOMOLE
     integer :: tm_typedft = 1
     !! Type of DFT:
     !!
     !! - 1: Trajectory is on an excited state;
     !! - 2: Trajectory is on the ground state, but excited states
     !! are requested;
     !! - 3: Full ground state trajectory.
     integer :: tm_nnodes = 1
     !! Number of CPU cores to use.
     integer :: tm_mult = 1
     !! Spin multiplicity
     integer :: tm_npre = 1
     !! ???
     integer :: tm_nstart = 1
     !! ???
     character(len=10) :: tm_grad_prog = 'grad'
     !! Name of the program used for computing gradients.
     character(len=10) :: tm_en_prog = 'dscf'
     !! Name of the program used for computing energies.
     character(len=10) :: tm_scf_prog = 'dscf'
     !! Name of the program used for SCF.
     integer :: tm_vers(2) = [0, 0]
     !! Version of Turbomole, given as an array: version 7.3 is defined by
     !! ``tm_vers = (7, 3)``.

     ! ANALYTICAL MODELS
     logical :: am_get_diabpop = .false.
     real(dp), allocatable :: am_m(:)
     !! Reduced masses of the system
     real(dp), allocatable :: am_r1(:)
     !! 1D Coordinates of the system.
     real(dp), allocatable :: am_v1(:)
     !! 1D Velocity of the system.
     real(dp), allocatable :: am_grad1(:, :)
     !! 1D gradients.
     real(dp), allocatable :: am_nad1(:, :)
     !! 1D non-adiabatic couplings.
     real(dp), allocatable :: am_epot(:)
     !! Potential energies.
     real(dp), allocatable :: am_diabpop(:, :)
     !! Diabatic populations. The computation of these populations is based on Subotnik
     !! et. al. [J, Chem. Phys. 139, 211101 (2013)], and the first dimension of the array
     !! corresponds to the three different models proposed in this paper.
     real(dp), allocatable :: am_nad1_i(:, :)
     !! (CS-FSSH ONLY) Imaginary part of the 1D non-adiabatic couplings.
     real(dp), allocatable :: am_gamma(:)
     !! (CS-FSSH ONLY) Imaginary part of potential energy.

     type(nx_sbh_t) :: sbh
     type(nx_recohmodel_t) :: recohmod
     type(nx_onedim_t) :: onedim
     type(nx_conint_t) :: conint
     type(nx_cs_fssh_t) :: csfssh

     ! COLUMBUS
     integer :: col_mem = 200
     integer :: col_cirestart = 0
     integer :: col_reduce_tol =  1
     real(dp) :: col_citol = 1E-4_dp
     integer :: col_quad_conv = 60
     integer :: col_mc_conv = 0
     integer :: col_ci_conv = 0
     integer :: col_ivmode = 8
     integer :: col_mocoef = 1
     integer :: col_ci_iter = 30
     character(len=:), allocatable :: col_mode
     !! Columbus mode: ms (multi-state), ss (single-state), mc
     !! (multi-configuratiion) or sc (single configuration)
     integer :: col_mel = 0
     !! Maximum excitation level
     integer :: col_ci_type = -1
     !! Type of CI computation:
     !!
     !! - 0: No CI (typically MCSCF only jobs);
     !! - 1: Serial version of CI programs will be called;
     !! - 2: Parallel version of CI programs.
     integer :: col_all_grads = 1
     integer :: col_grad_lvl = -1
     integer :: col_prt_mo = -1

     !! TINKER-G16-MMP
     character(len=:), allocatable :: tg16mmp_interface
     !! TINKER-MNDO
     character(len=:), allocatable :: tmndo_interface

     ! GAUSSIAN
     integer :: gau_mocoef = 1
     integer :: gau_prt_mo = 20
     integer :: gau_td_st = 0
     integer :: gau_type = 0
     integer :: gau_ld_thr = 14
     integer :: gau_typedft = -1
     integer :: gau_no_fc = -1
     !! If > 0 do a full correlation computation (don't freeze any core orbitals).  This
     !! amounts to having an instruction ``TD(Full,Nstates=...)``.

     ! ORCA
     logical :: orca_is_tda = .true.
     !! Indicate if the computation is TDA (``.true.``) or TD-DFT (``.false.``) (Default: ``.true.``).
     integer :: orca_prt_mo = 20
     !! Backup MOs (``gbw`` file) every ``orca_prt_mo`` step (Default: 20).
     integer :: orca_mocoef = 1
     !! Decide which initial guess to use:
     !!
     !! - 0: compute the guess at every time step ;
     !! - 1: use the guess from previous step ;
     !! - 2: use the same guess for all steps.

     !! EXCITON DYNAMICS
     real(dp), allocatable, dimension(:) :: diapop
     !! Save the diabatic populations
     real(dp), allocatable, dimension(:) :: diaen
     !! Save the diabatic energies
     real(dp), allocatable, dimension(:, :) :: diaham
     !! Save the excitonic hamiltonian
     real(dp), allocatable, dimension(:, :) :: eigenvectorsexc
     !! Save the excitonic eigenvectors
     real(dp), allocatable, dimension(:, :) :: chrom_enexc
     !! Save the energies of the individual chromophores
     real(dp), allocatable, dimension(:, :) :: trdipexc
     !! Save the transition dipole moments (Debye) for the exciton model

     ! MLatom
     logical :: mlat_use_nx_interface = .false.
     !! Indicate if we should use the new interface to NX, or if we need to do things
     !! manually.
     character(len=:), allocatable :: mlat_exe
     !! Path to the MLatom executable

     integer :: nchromexc = 2
     integer :: nprocexc = 1
     integer :: gen_fileexc = 1
     integer :: nstattdexc = -1
     integer :: nproc_gauexc = 2
     character(len=200) :: functionalexc = ''
     character(len=200) :: basisexc = ''
     character(len=200) :: force_fieldexc = ''

     !! Namelist "exc_inp" infos:
     integer :: trespexc = 1
     integer :: gsexc = 0
     integer :: dipexc = 0
     integer :: verboseexc = 2
     integer, allocatable, dimension(:) :: nat_arrayexc
     integer, allocatable, dimension(:) :: nstatexc
     character(len=150) :: coup_fileexc = ''

     !! External generic script (python)
     character(len=:), allocatable :: ext_path

   contains
     ! ===============================================================
     ! This part should not be modified, as these procedures will
     ! just be used for initializing and destroying the main object !!
     ! ===============================================================
     procedure :: init
     procedure :: destroy
     final :: finalizer
  end type nx_qm_t

contains

  subroutine init(this, conf)
    !! Constructor for the ``nx_qm_t`` class corresponding to QM code ``qmcode``.
    !!
    !! Initializes all values to -1, and initializes all arrays to
    !! dummy values. The proper assignement will be made in the
    !! ``setup`` function, that can handle QM-code specific instructions.
    class(nx_qm_t), intent(inout) :: this
    type(nx_config_t), intent(in) :: conf

    character(len=MAX_STR_SIZE) :: env
    integer :: stat
    ! if (conf%progname /= 'analytical') then
    !    this%qmcode = trim(conf%progname)w
    ! else
    !    if (conf%methodname /= 'sbh') then
    !       this%qmcode = trim(conf%methodname)
    !    else
    !       this%qmcode = trim(conf%progname)
    !    end if
    ! end if

    this%qmcode = trim(conf%progname)

    if (conf%compute_diabpop) this%am_get_diabpop = .true.

    this%debug_path = '../'//trim(conf%debug_path)

    this%qmmethod = trim(conf%methodname)
    this%job_folder = trim(conf%init_input)
    this%update_epot = 1
    this%request_adapt_dt = .false.

    ! Is is a test ?
    call get_environment_variable("NX_TEST_ENV", status=stat)
    if (stat /= 1) then
       if (stat >= 0) then
          this%is_test = .true.
       end if
    end if

    ! The initial values come from the conf%dc_method parameters
    this%compute_nac = 0
    this%read_nac = 0
    if (conf%dc_method == 1) then
       this%compute_nac = 1
       this%read_nac = 1
    end if
    this%compute_nac_mm = this%compute_nac

    call this%orb%init

    ! For analytical methods we don't have oscillator strengths.
    if (conf%progname /= 'analytical') then
       allocate( this%osc_str( conf%nstat - 1))
       this%osc_str(:) = 0.0_dp
    end if

    ! Overlap computation
    allocate(this%ovl_async(1))
    this%ovl_async = 'NULL'
    this%qmproc = 1
    this%ovl_cmd = ''
    this%ovl_out = ''
    this%ovl_post = ''

    ! ANALYTICAL: SPIN-BOSON HAMILTONIAN
    ! Temporary allocation, the real one will be carried out in
    ! ``setup``, so that in the case we don't use them, they don't
    ! waste memory.
    allocate(this%am_m(1))
    allocate(this%am_r1(1))
    allocate(this%am_v1(1))
    allocate(this%am_grad1(1, 1))
    allocate(this%am_nad1(1, 1))
    allocate(this%am_epot(1))
    allocate(this%am_diabpop(1, 1))
    allocate(this%am_gamma(1))
    allocate(this%am_nad1_i(1, 1))
    this%am_m(1) = 0.0_dp
    this%am_r1(1) = 0.0_dp
    this%am_v1(1) = 0.0_dp
    this%am_grad1(1, 1) = 0.0_dp
    this%am_nad1(1, 1) = 0.0_dp
    this%am_epot(1) = 0.0_dp
    this%am_diabpop(1, 1) = 0.0_dp
    this%am_gamma(1) = 0.0_dp
    this%am_nad1_i(1, 1) = 0.0_dp

        
    ! ! ANALYTICAL: Recohrence model by Subotnik et al.
    ! this%recoh_r = 0.0_dp
    ! this%recoh_m = 0.0_dp
    ! this%recoh_nad = 0.0_dp
    ! this%recoh_en(:) = 0.0_dp
    ! this%recoh_grad(:) = 0.0_dp
    ! 
    ! ! ANALYTICAL: 1d tully models
    ! allocate(this%onedim_parm(1))
    ! this%onedim_parm(1) = 0.0_dp
    ! this%onedim_epot(:) = 0.0_dp
    ! this%onedim_grad(:) = 0.0_dp
    ! this%onedim_nad(:) = 0.0_dp

    ! Exciton models
    allocate(this% diaham(1, 1))
    allocate(this% diapop(1))
    allocate(this% diaen(1))
    allocate(this% nstatexc(1))
    allocate(this% nat_arrayexc(1))
    allocate(this% eigenvectorsexc(1, 1))
    allocate(this% chrom_enexc(1, 1))
    allocate(this% trdipexc(1, 3))
    this%diaham(:,:) = 0.0_dp
    this%diapop(:) = 0.0_dp
    this%diaen(:) = 0.0_dp
    this%nstatexc(:) = 0
    this%nat_arrayexc(:) = 0
    this%eigenvectorsexc(:,:) = 0.0_dp
    this%chrom_enexc(:,:) = 0.0_dp
    this%trdipexc(:,:) = 0.0_dp

    ! TINKER MNDO
    this%tmndo_interface = '$TINKER_MNDO/gradinterf.x'
    this%compute_nac_mm = 1

    ! External
    this%ext_path = ''

    ! MLatom
    this%mlat_exe = ''
  end subroutine init


  subroutine destroy(this)
    !! Detroy the current object.
    !!
    !! This is done by deallocating all variables that have been
    !! allocated before.
    class(nx_qm_t), intent(inout) :: this
    !! This object.
    deallocate(this%am_m)
    deallocate(this%am_r1)
    deallocate(this%am_v1)
    deallocate(this%am_epot)
    deallocate(this%am_grad1)
    deallocate(this%am_nad1)

    deallocate(this%debug_path)
    ! deallocate(this%onedim_parm)
    deallocate(this%diaham)
    deallocate(this%diapop)
    deallocate(this%diaen)
    deallocate(this%nstatexc)
    deallocate(this%nat_arrayexc)
    deallocate(this%eigenvectorsexc)
    deallocate(this%chrom_enexc)
    deallocate(this%trdipexc)

    if (allocated(this%osc_str)) deallocate(this%osc_str)

    deallocate(this%tmndo_interface)

    deallocate(this%qmcode)
    deallocate(this%qmmethod)

    deallocate(this%ovl_cmd)
    deallocate(this%ovl_post)
    deallocate(this%ovl_out)
    deallocate(this%ext_path)
    deallocate(this%mlat_exe)
  end subroutine destroy

  subroutine finalizer(this)
    !! Detroy the current object.
    !!
    !! This is done by deallocating all variables that have been
    !! allocated before.
    type(nx_qm_t), intent(inout) :: this
    !! This object.

    deallocate(this%am_m)
    deallocate(this%am_r1)
    deallocate(this%am_v1)
    deallocate(this%am_epot)
    deallocate(this%am_grad1)
    deallocate(this%am_nad1)
    deallocate(this%debug_path)
    ! deallocate(this%onedim_parm)
    deallocate(this%diaham)
    deallocate(this%diapop)
    deallocate(this%diaen)
    deallocate(this%nstatexc)
    deallocate(this%nat_arrayexc)
    deallocate(this%eigenvectorsexc)
    deallocate(this%chrom_enexc)
    deallocate(this%trdipexc)

    if (allocated(this%osc_str)) deallocate(this%osc_str)

    deallocate(this%tmndo_interface)

    deallocate(this%qmcode)
    deallocate(this%qmmethod)

    deallocate(this%ovl_cmd)
    deallocate(this%ovl_post)
    deallocate(this%ovl_out)
    deallocate(this%ext_path)
    deallocate(this%mlat_exe)
  end subroutine finalizer


end module mod_qm_t
