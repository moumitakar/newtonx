! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_kinds
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2022-03-30
  !!
  !! Kind definition for Newton-X.
  !!
  !! In this module we define the different kinds for integers and
  !! floating point numbers to be used in Newton-X.  To ensure later
  !! compatibility with C interfaces, we make use of the definitions
  !! from the ``iso_c_binding`` module, and export the following:
  !!
  !! - Single precision ``sp`` corresponds to ``c_float`` ;
  !! - Double precision ``dp`` corresponds to ``c_double`` ;
  !! - Quadruple precision ``qp`` corresponds to ``c_float128`` ;
  !!
  !! Single and quadruple precision are not used in Newton-X.
  use iso_c_binding, only: &
       & sp => c_float, dp => c_double, qp => c_float128, &
       & int8 => c_int8_t, int16 => c_int16_t
  implicit none

  private

  public :: sp, dp, qp
  public :: int8, int16
end module mod_kinds
