module mod_sbh
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Spin Boson Hamiltonian Model
  !!
  use mod_constants, only: &
       & cm2au, hbar, pi, MAX_STR_SIZE
  use mod_input_parser, only: &
       & parser_t, set => set_config, set_alloc => set_config_realloc
  use mod_kinds, only: dp
  use mod_logger, only: &
       & nx_log, LOG_DEBUG, &
       & print_conf_ele
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  type, public :: nx_sbh_t
     real(dp) :: e0 = 12000.0_dp
     !! Energy separation between the 2 electronic states (cm-1)
     real(dp) :: v0 = 800.0_dp
     !! Coupling between the 2 electronic states (cm-1)
     character(len=:), allocatable :: jw
     !! Type of spectral density: ``user``, ``debye`` or ``ohmic``.
     real(dp) :: wc = 0.0_dp
     !! If ``jw`` is ``debye`` or ``ohmic``: resonance energy.
     real(dp) :: wmax = 0.0_dp
     !! If ``jw`` is ``debye`` or ``ohmic``: Maximum energy.
     real(dp) :: xi = 0.0_dp
     !! If ``jw`` is ``ohmic``: \(xi\) parameter.
     real(dp) :: er = 0.0_dp
     !! If ``jw`` is ``debye``: \(E_R\) parameter.
     real(dp), dimension(:), allocatable :: w
     !! Vibrational level.
     real(dp), dimension(:), allocatable :: g
     !! Oscillator strengths.

   contains
     procedure :: setup => sbh_setup
     procedure :: run => sbh_run
     procedure :: print => sbh_print
     procedure :: get_umatrix => sbh_get_umatrix
     procedure, private :: read_g_w => sbh_read_g_w
     procedure, private :: debye_g_w => sbh_debye_g_w
     procedure, private :: ohmic_g_w => sbh_ohmic_g_w
     procedure, private :: compute_eta => sbh_compute_eta
     procedure, private :: compute_fac => sbh_compute_fac
     procedure, private :: compute_energies => sbh_compute_energies
     procedure, private :: compute_gradients => sbh_compute_gradients
     procedure, private :: compute_nac => sbh_compute_nac
  end type nx_sbh_t
  interface nx_sbh_t
     module procedure nx_sbh_construct
  end interface nx_sbh_t

contains

  function nx_sbh_construct(n) result(res)
    integer, intent(in) :: n
    !! Number of harmonic oscillators

    type(nx_sbh_t) :: res

    allocate(res%w(n))
    allocate(res%g(n))

    res%w = 0.0_dp
    res%g = 0.0_dp


  end function nx_sbh_construct


  subroutine sbh_setup(self, parser, paramfile, masses, parse)
    class(nx_sbh_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in), optional :: paramfile
    real(dp), intent(in), optional :: masses(:)
    logical, intent(in), optional :: parse
    !! Indicate wether to parse the input files or not

    logical :: do_file_parsing

    self%jw = 'user'
    
    ! Start by configuring the local `sbh` object
    call set(parser, 'sbh', self%e0, 'e0')
    call set(parser, 'sbh', self%v0, 'v0')
    call set_alloc(parser, 'sbh', self%jw, 'jw')

    call set(parser, 'sbh', self%wc, 'wc')
    call set(parser, 'sbh', self%wmax, 'wmax')
    call set(parser, 'sbh', self%xi, 'xi')
    call set(parser, 'sbh', self%er, 'er')
    self%e0 = self%e0 * cm2au
    self%v0 = self%v0 * cm2au
    self%wc = self%wc * cm2au
    self%wmax = self%wmax * cm2au
    self%er = self%er * cm2au

    do_file_parsing = .true.
    if (present(parse)) do_file_parsing = .false.

    if (do_file_parsing) then
       ! Populate g and w
       if (self%jw == 'user') then
          ! TODO: Nx_Qm condition should be inside the Perl driver
          call self%read_g_w(paramfile)

       else if (self%jw == 'debye') then
          call self%debye_g_w(masses)
       else if (self%jw == 'ohmic') then
          call self%ohmic_g_w(masses)
       end if
    end if
  end subroutine sbh_setup


  subroutine sbh_print(self, out)
    !! Print the SBH configuration.
    class(nx_sbh_t), intent(in) :: self
    !! Nx_Qm object.
    integer, intent(in), optional :: out

    integer :: i
    integer :: output

    output = stdout
    if (present(out)) output = out

    call print_conf_ele(self%e0, 'e0', unit=output)
    call print_conf_ele(self%v0, 'v0', unit=output)
    call print_conf_ele(self%jw, 'jw', unit=output)

    if (self%jw /= 'user') then
       call print_conf_ele(self%wc, 'wc',  unit=output)
       call print_conf_ele(self%wmax, 'wmax', unit=output)
    end if

    if (self%jw == 'ohmic') then
       call print_conf_ele(self%xi, 'xi', unit=output)
    end if

    if (self%jw == 'debye') then
       call print_conf_ele(self%er, 'er', unit=output)
    end if

    write(output, *) ''
    write(output, '(A10, 2A30)') 'i', 'w[i] (cm-1)', 'g[i] (hartree/bohr)'
    do i=1, size(self%w)
       write(output, '(I10, 2F30.12)') i, self%w(i) / cm2au, self%g(i)
    end do

  end subroutine sbh_print


  subroutine sbh_run(self, r, masses, epot, grad, nad)
    !! Run the SBH model.
    !!
    !! The run itself is carried out in three steps:
    !!
    !! - Compute the energies;
    !! - Compute the gradients;
    !! - Compute the non-adiabatic couplings.
    class(nx_sbh_t), intent(inout) :: self
    !! Nx_Qm object.
    real(dp), intent(in) :: r(:)
    !! Coordinates.
    real(dp), intent(in) :: masses(:)
    !! Masses.
    real(dp), intent(inout) :: epot(:)
    !! Energies.
    real(dp), intent(inout) :: grad(:, :)
    !! Gradients.
    real(dp), intent(inout) :: nad(:, :)
    !! Non adiabatic couplings.
    

    real(dp) :: eta
    real(dp) :: fac

    eta = self%compute_eta(r)
    fac = self%compute_fac(eta)

    epot = self%compute_energies(masses, r, fac)
    grad = self%compute_gradients(masses, r, fac, eta)
    nad = self%compute_nac(fac)
  end subroutine sbh_run


  function sbh_compute_energies(self, m, r, fac) result(epot)
    !! Compute the SBH energies with the precomputed factor.
    !!
    !! We have:
    !! \[ E_i = \frac{1}{2} \sum_j^N M_j \omega_j^2 R_j^2 + (-1)^i f
    !!  (i=1, 2)\]
    !! \[ f = \left (\eta^2 + \nu_0^2 \right )^{1/2} \]
    class(nx_sbh_t), intent(in) :: self
    !! Nx_Qm object.
    real(dp), intent(in) :: m(:)
    !! Reduced masses.
    real(dp), intent(in) :: r(:)
    !! Coordinates.
    real(dp), intent(in) :: fac
    !! Precomputed \(f \) factor.

    real(dp) :: epot(2)
    !! Resulting potential energies.

    integer :: i, j
    real(dp) :: temp

    do i=1, 2
       temp = 0.0_dp
       do j=1, size(self%w)
          temp = temp + &
               & m(j) * self%w(j)**2 * r(j)**2
       end do
       epot(i) = 0.5_dp * temp
       epot(i) = epot(i) + (-1)**i * fac
    end do
  end function sbh_compute_energies


  function sbh_compute_gradients(self, m, r, fac, eta) result(grad)
    !! Compute the SBH energies with the precomputed factor.
    !!
    !! We have:
    !! \[ \frac{\partial E_i}{\partial Q_k} = M_k \omega_k^2 R_k +
    !! (-1)^i g_k \left [ \frac{\eta}{f} \right ] (k=1, ..., N) \]
    !! \[ f = \left (\eta^2 + \nu_0^2 \right )^{1/2} \]
    class(nx_sbh_t), intent(in) :: self
    real(dp), intent(in) :: m(:)
    !! Reduced masses.
    real(dp), intent(in) :: r(:)
    !! Coordinates.
    real(dp), intent(in) :: fac
    real(dp), intent(in) :: eta

    real(dp) :: grad( 2, size( self%w) )

    integer :: i

    do i=1, size(grad, 1)
       grad(i, :) = &
            & m(:) * self%w(:)**2 * r(:) &
            & + (-1)**i * self%g(:) * eta / fac
    end do

  end function sbh_compute_gradients


  function sbh_compute_nac(self, fac) result(nac)
    !! Compute the SBH non-adiabatic couplings.
    !!
    !! We have:
    !! \[ F_{12}^k = -\frac{1}{2} g_k \left [ \frac{\nu_0}{f^2}
    !! \right] \]
    !! \[ f = \left (\eta^2 + \nu_0^2 \right )^{1/2} \]
    class(nx_sbh_t), intent(in) :: self
    real(dp), intent(in) :: fac

    real(dp) :: nac(1, size(self%w) )

    ! We actually need F21 and not F12, so the minus sign cancels
    ! out from the expressions.
    nac(1, :) = self%g(:) * self%v0 / fac**2
    nac(1, :) = 0.5_dp * nac(1, :)
  end function sbh_compute_nac


  function sbh_get_umatrix(self, masses, r) result(umat)
    class(nx_sbh_t), intent(in) :: self
    real(dp), intent(in) :: masses(:)
    real(dp), intent(in) :: r(:)

    real(dp) :: umat(2, 2)

    real(dp) :: vmat(2, 2)
    real(dp) :: h11, h22, eb, esb, theta
    character(len=MAX_STR_SIZE) :: msg
    integer :: i

    h11 = 0.0_dp
    h22 = 0.0_dp 
    do i = 1, size(self%w)
       eb  = 0.5_dp * (masses(i) * self%w(i)**2 * r(i)**2)
       esb = self%g(i) * r(i)

       h11 = h11 + (eb + esb)
       h22 = h22 + (eb - esb)
    enddo

    vmat(1, 1) = h11 + self%e0 
    vmat(2, 2) = h22 - self%e0
    vmat(1, 2) = self%v0
    vmat(2, 1) = vmat(1, 2)

    theta = 0.5_dp * atan(2.0_dp * vmat(1, 2) / (vmat(2, 2)-vmat(1, 1)))

    umat(1, 1) = cos(theta)
    umat(1, 2) = sin(theta)
    umat(2, 1) = -umat(1, 2)
    umat(2, 2) = umat(1, 1)

    write(msg, '(A,F20.12)') 'Mixing Angle Theta =', theta
    call nx_log%log(LOG_DEBUG, msg)
    call nx_log%log(LOG_DEBUG, vmat, title='Diabatic Potential Matrix')
    call nx_log%log(LOG_DEBUG, umat, title='ADT Matrix')
  end function sbh_get_umatrix


  ! ========================
  ! Compute some pre-factors
  ! ========================
  function sbh_compute_eta(self, r) result(eta)
    !! Compute the \(\eta\) factor.
    !!
    !! \[ \eta = \left ( \sum_N g_j R_j \right ) + \epsilon_0 \]
    class(nx_sbh_t), intent(in) :: self
    !! Nx_Qm object.
    real(dp), intent(in) :: r(:)

    real(dp) :: eta

    integer :: j

    eta = self%e0
    do j=1, size(self%w)
       eta = eta + self%g(j) * r(j)
    end do

  end function sbh_compute_eta


  function sbh_compute_fac(self, eta) result(fac)
    !! Compute a useful factor in SBH.
    !!
    !! \[ f = \left (\eta^2 + \nu_0^2 \right )^{1/2} \]
    class(nx_sbh_t), intent(in) :: self
    !! Nx_Qm object.
    real(dp), intent(in) :: eta
    !! \(\eta\) parameter.

    real(dp) :: fac

    fac = sqrt(eta**2 + self%v0**2)
  end function sbh_compute_fac

  ! ==========================================
  ! Generate oscillator energies and intensity
  ! ==========================================
  subroutine sbh_read_g_w(self, control)
    !! Read user provided harmonic oscillator energies and intensity.
    class(nx_sbh_t), intent(inout) :: self
    !! Nx_Qm object.
    character(len=*), intent(in) :: control
    !! File to read.

    integer :: u, i

    open(newunit=u, file=control, action='read')
    do i=1, size(self%w)
       read(u, *) self%w(i), self%g(i)
    end do
    close(u)

    self%w(:) = self%w(:) * cm2au
  end subroutine sbh_read_g_w

  subroutine sbh_debye_g_w(self, m)
    !! Generate a Debye spectral density.
    !!
    !! The spectral density is generated according to:
    !! \[ \omega_j = \mathrm{tan} \left ( \frac{j}{N}
    !! \mathrm{tan^{-1}} \left ( \frac{\omega_{max}}{\omega_c}
    !! \right ) \right ) \omega_c \]
    !! \[ g_j = \left [ \frac{M_j E_r}{\pi N} \mathrm{tan^{-1}} \left (
    !! \frac{\omega_{max}}{\omega_c} \right )  \right ]^{1/2}
    !! \omega_j \]
    class(nx_sbh_t), intent(inout) :: self
    !! Nx_Qm object.
    real(dp), intent(in) :: m(:)

    integer :: i, n
    real(dp) :: temp

    temp = atan(self%wmax / self%wc)

    n = size(self%w)

    do i=1, n
       ! First construct the energy \omega
       self%w(i) = real(i, dp) / real(n, dp)
       self%w(i) = tan(self%w(i) * temp)
       self%w(i) = self%w(i) * self%wc

       ! And then the strength g
       self%g(i) = m(i) * self%er / (pi * n)
       self%g(i) = sqrt(self%g(i) * temp)
       self%g(i) = self%g(i) * self%w(i)
    end do
  end subroutine sbh_debye_g_w

  subroutine sbh_ohmic_g_w(self, m)
    !! Generate an ohmic spectral density.
    !!
    !! The spectral density is generated according to:
    !! \[ \omega_0 = \frac{\omega_c}{N} \left ( 1 -
    !! \mathrm{e}^{-\omega_{max}/\omega_c} \right ) \]
    !! \[ \omega_j = -\omega_c \mathrm{ln} \left ( 1 -
    !! j\frac{\omega_0}{\omega_c} \right ) \]
    !! \[ g_j = \left ( \xi \hbar \omega_0 M_j \right )^{1/2}
    !! \omega_c \]
    class(nx_sbh_t), intent(inout) :: self
    real(dp), intent(in) :: m(:)

    integer :: i, n
    real(dp) :: w0

    n = size(self%w)
    w0 = self%wc / n
    w0 = w0 * (1 - exp( -self%wmax / self%wc ))

    do i=1, n
       ! First construct the energy \omega
       self%w(i) = log( 1 - i * w0 / self%wc)
       self%w(i) = - self%w(i) * self%wc

       ! And then the strength g
       self%g(i) = sqrt(hbar * w0 * self%xi * m(i))
       self%g(i) = self%g(i) * self%w(i)
    end do

  end subroutine sbh_ohmic_g_w


end module mod_sbh
