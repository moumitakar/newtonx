! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_columbus
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-10-06
  !!
  use iso_c_binding, only: c_ptr
  use mod_kinds, only: dp
  use mod_constants, only: &
       & MAX_STR_SIZE, MAX_CMD_SIZE
  use mod_interface, only: &
       & copy, rm, mkdir, gzip, &
       & get_list_of_files
  use mod_logger, only: &
       & nx_log,&
       & LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_ERROR, &
       & call_external, print_conf_ele, check_error
  use mod_tools, only: &
       & split_blanks, to_str, remove_blanks, split_pattern
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_print_utils, only: to_lower
  use mod_trajectory, only: nx_traj_t
  use mod_configuration, only: nx_config_t
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  ! Run the program for energies, gradients and NAD
  public :: col_setup, col_print
  public :: col_update_input
  public :: col_initial_phase
  public :: col_run
  public :: col_read_outputs
  public :: col_backup

  ! Cioverlap-related functions
  public :: col_get_lcao
  public :: col_extract_overlap
  public :: col_prepare_cio

  ! Double-molecule related functions
  public :: col_init_double_molecule
  public :: col_prepare_double_molecule


  integer, parameter :: COL_ERR_MISC = 101
  integer, parameter :: COL_ERR_MAIN = 102
  integer, parameter :: CIO_COL_CIPC = 103
  integer, parameter :: CIO_COL_MCPC = 104
  integer, parameter :: CIO_COL_MISC = 105
  integer, parameter :: CIO_COL_CONSOLIDATE = 106

contains

  subroutine col_setup(nx_qm, parser)
    !! Set up the Columbus parameters.
    !!
    !! The parameters are read from the ``columbus`` namelist in file ``configfile``.  If
    !! the ``nadcoupl`` instruction is present in ``control.run``, then the
    !! ``compute_nac`` and ``compute_nac_mm`` parameters from the ``nx_qm`` object are set
    !! to 1.
    type(nx_qm_t), intent(inout) :: nx_qm
    !! QM configuration.
    type(parser_t), intent(in) :: parser

    ! integer :: mem
    ! integer :: cirestart
    ! integer :: reduce_tol
    ! real(dp) :: citol
    ! integer :: quad_conv
    ! integer :: mc_conv
    ! integer :: ci_conv
    ! integer :: ivmode
    ! integer :: mocoef
    ! integer :: ci_iter
    ! character(len=3) :: mode
    ! integer :: mel
    ! integer :: ci_type
    ! integer :: all_grads
    ! integer :: grad_lvl
    ! integer :: prt_mo
    !
    ! integer :: u, ierr, id
    ! character(len=1024) :: buf, env
    !
    ! namelist /columbus/ mem, cirestart, reduce_tol, citol,&
    !      & quad_conv, mc_conv, ci_conv, ivmode, mocoef, &
    !      & ci_iter, mode, mel, ci_type, all_grads, grad_lvl, &
    !      & prt_mo
    !
    ! open(newunit=u, file=configfile, status='old', action='read')
    ! read(u, nml=columbus)
    ! close(u)

    integer :: ierr, u
    character(len=MAX_STR_SIZE) :: buf, env, control

    if (nx_qm%qmmethod == 'mrci') then
       nx_qm%col_grad_lvl = 2
    else
       nx_qm%col_grad_lvl = 1
    end if

    call set(parser, 'columbus', nx_qm%col_mem, 'mem')
    call set(parser, 'columbus', nx_qm%col_cirestart, 'cirestart')
    call set(parser, 'columbus', nx_qm%col_reduce_tol, 'reduce_tol')
    call set(parser, 'columbus', nx_qm%col_citol, 'citol')
    call set(parser, 'columbus', nx_qm%col_quad_conv, 'quad_conv')
    call set(parser, 'columbus', nx_qm%col_mc_conv, 'mc_conv')
    call set(parser, 'columbus', nx_qm%col_ci_conv, 'ci_conv')
    call set(parser, 'columbus', nx_qm%col_ivmode, 'ivmode')
    call set(parser, 'columbus', nx_qm%col_mocoef, 'mocoef')
    call set(parser, 'columbus', nx_qm%col_ci_iter, 'ci_iter')
    call set(parser, 'columbus', nx_qm%col_all_grads, 'all_grads')
    call set(parser, 'columbus', nx_qm%col_prt_mo, 'prt_mo')
    call set(parser, 'columbus', nx_qm%col_grad_lvl, 'grad_lvl')

    control = trim(nx_qm%job_folder)//'/control.run'

    nx_qm%col_ci_type = col_ci_type(control)
    nx_qm%col_mode = col_get_mode(control)

    if (nx_qm%col_mode == 'ms' .or. nx_qm%col_mode == 'ss') then
       block
         character(len=:), allocatable :: input

         if (nx_qm%col_mode == 'ms') input = trim(nx_qm%job_folder)//'/cidrtmsin'
         if (nx_qm%col_mode == 'ss') input = trim(nx_qm%job_folder)//'/cidrtin'

         nx_qm%col_mel = col_get_maximum_excitation_level(input)
       end block
    end if

    ! Adjust compute_nac if required
    open(newunit=u, file=control, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'nadcoupl') /= 0) then
          nx_qm%compute_nac = 1
       end if
    end do
    close(u)
    nx_qm%compute_nac_mm = nx_qm%compute_nac

    call get_environment_variable("COLUMBUS", env)
    nx_qm%ovl_cmd = trim(env)//'/dalton.x -m '//to_str(8 * nx_qm%col_mem)
    nx_qm%ovl_out = 'daltonls'

    if (nx_qm%col_prt_mo > 0 .and. nx_qm%col_mocoef == 0) then
       block
         character(len=:), allocatable :: msg

         msg = 'prt_mo = '//to_str(nx_qm%col_prt_mo)//' and mocoef = 0 : &
              & the orbitals will not be saved (using original orbitals for all steps)'
         call nx_log%log(LOG_WARN, msg)
         nx_qm%col_prt_mo = -1
       end block
    end if
  end subroutine col_setup


  subroutine col_print(nx_qm, out)
    !! Print the Columbus configuration.
    !!
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM configuration.
    integer, intent(in), optional :: out

    integer :: output
    character(len=MAX_STR_SIZE) :: env

    output = stdout
    if (present(out)) output = out

    call get_environment_variable("COUMBUS", env)
    call print_conf_ele(trim(env), 'COLUMBUS path', unit=output)    

    call print_conf_ele(nx_qm%col_reduce_tol, 'reduce_tol', unit=output)
    call print_conf_ele(nx_qm%col_mem, 'mem', unit=output)
    call print_conf_ele(nx_qm%col_ci_type, 'type of CI', unit=output)
    call print_conf_ele(nx_qm%col_grad_lvl, 'grad_lvl', unit=output)
    call print_conf_ele(nx_qm%col_all_grads, 'all_grads', unit=output)

    if (nx_qm%col_cirestart /= -1) then
       call print_conf_ele(nx_qm%col_cirestart, 'cirestart', unit=output)
    end if

    if (nx_qm%col_citol /= -1) then
       call print_conf_ele(nx_qm%col_citol, 'citol', elefmt='(e10.2)', unit=output)
    end if

    if (nx_qm%col_quad_conv /= -1) then
       call print_conf_ele(nx_qm%col_quad_conv, 'quad_conv', unit=output)
    end if

    if (nx_qm%col_mc_conv /= -1) then
       call print_conf_ele(nx_qm%col_mc_conv, 'mc_conv', unit=output)
    end if

    if (nx_qm%col_ci_conv /= -1) then
       call print_conf_ele(nx_qm%col_ci_conv, 'ci_conv', unit=output)
    end if

    if (nx_qm%col_ivmode /= -1) then
       call print_conf_ele(nx_qm%col_ivmode, 'ivmode', unit=output)
    end if

    if (nx_qm%col_mocoef /= -1) then
       call print_conf_ele(nx_qm%col_mocoef, 'mocoef', unit=output)
    end if

    if (nx_qm%col_ci_iter /= -1) then
       call print_conf_ele(nx_qm%col_ci_iter, 'ci_iter', unit=output)
    end if

    if (nx_qm%col_mode /= 'nul') then
       call print_conf_ele(nx_qm%col_mode, 'mode', unit=output)
    end if

    if (nx_qm%col_mel /= -1) then
       call print_conf_ele(nx_qm%col_mel, 'mel', unit=output)
    end if

  end subroutine col_print


  subroutine col_backup(nx_qm, conf, traj, dbg_dir)
    !! Backup information about previous Columbus run.
    !!
    !! The file ``mocoef_mc.sp`` is backed up every ``nx_qm%col_prt_mo`` step.  For
    !! ``conf%lvprt > 3``, all ``LISTINGS`` and ``GRADIENTS`` folders, as well as
    !! ``runls`` and ``runc.err`` files, are saved every ``conf%save_cwd``.
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM configuration.
    type(nx_config_t), intent(in) :: conf
    !! Main NX configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Trajectory object.
    character(len=*), intent(in) :: dbg_dir
    !! Name of the directory where the information is saved.

    integer :: ierr
    logical :: mo, bck

    mo = (nx_qm%col_prt_mo > 0) .and. (mod(traj%step, nx_qm%col_prt_mo) == 0)
    bck = (conf%lvprt > 3) .and. (mod(traj%step, conf%save_cwd) == 0)

    if (mo .or. bck) then
       ierr = mkdir(dbg_dir)
       call check_error(ierr, COL_ERR_MAIN, &
            & 'Cannot create '//dbg_dir, system=.true.)
    end if

    if (mo) then
       ierr = copy('MOCOEFS/mocoef_mc.sp', dbg_dir)
       call check_error(ierr, COL_ERR_MAIN, &
            & 'Cannot copy MOCOEFS/mocoef_mc.sp to '//dbg_dir, system=.true.)

       ierr = gzip([dbg_dir//'/mocoef_mc.sp'])
       call check_error(ierr, COL_ERR_MAIN, &
            & 'Cannot compress '//dbg_dir//'/mocoef_mc.sp', system=.true.)
    end if

    if (bck) then
       call execute_command_line(&
            & 'cp -rf runls runc.er* LIST* G* '//dbg_dir,  &
            & exitstat=ierr)
       call check_error(ierr, COL_ERR_MAIN, &
            & 'Cannot copy backup content', system=.true.)
    end if

  end subroutine col_backup


  subroutine col_update_input(nx_qm, traj, conf)
    !! Update the Columbus input.
    !!
    !! The function prepares the files required to run Columbus at the current step, by
    !! performing the following tasks:
    !!
    !! 1. First the MO coefficients from
    !!    ``MOCOEFS/mocoef_mc.sp`` are backed up as ``mocoef.old`` (if the directory exists,
    !!    *i.e.* after the first step), to be used as starting orbitals (if
    !!    ``col_mocoef = 1``) and / or in the computation of MO overlap.
    !! 2. Then the working directory is cleaned, by deleting some files and folders
    !!    (namely, the files ``runls``, ``curr_iter`` and
    !!    ``runc.error``, and the ``COSMO``, ``GRADIENTS``, ``LISTINGS``, ``MOCOEFS``,
    !!    ``MOLDEN``, ``RESTART`` and ``WORK`` directories).
    !! 3. The input files are copied from ``job_folder`` (``JOB_AD`` or ``JOB_NAD``) into
    !! the current directory, while excluding the files ``geom``, ``transmomin`` and
    !! ``runc.error`` to avoid collision with files created by Newton-X.
    !! 4. The ``mcscfin``  file is updated, with the ``col_quad_conv`` parameter being
    !! used to reset ``ncoupl``.
    !! 5. The ``redtol`` parameter is computed as a string, depending on the value of the
    !! ``col_reduce_tol`` and ``col_citol`` parameters (see [[col_wrt_redtol]] documentation).
    !! 6. For multi-state computations, in the ``ciudgin.drt1`` file, the ``ivmode``,
    !! ``nroot``, ``rtolbk`` and ``rtolci`` parameters are then modified with the content
    !! of ``col_ivmode``, ``nstat``, ``redtol`` and ``redtol`` respectively.
    !! 7. For single-state computations, the ``lroot`` parameter from file ``cidenin`` is
    !! set to ``nstatdyn``, and the following parameters are set in ``ciudgin``:
    !!     - ``nvkbmn = nstat``
    !!     - ``nvkbmx = nstat + 5``
    !!     - ``niter  = nstat * col_ci_iter``
    !!     - ``nvcimn = nstat + 2``
    !!     - ``nvcimx = nstat + 5``
    !!     - ``ivmode = col_ivmode``
    !!     - ``rtolbk = redtol``
    !!     - ``rtolci = redtol``
    !!     - ``nroot  = nstat``
    !! 8. After the first step of the dynamics, the ``scf`` keyword is deleted from
    !! ``control.run``.
    !! 9. If required (*i.e.* if ``col_cirestart = 1``, the we prepare the restart of the
    !! CI computation by calling the ``col_ci_restart_setup`` routine (see
    !! [[col_ci_restart_setup]] documentation).
    !! 10. If required (/i.e/ if ``col_mocoef = 1``), the ``mocoef.old`` file is copied
    !! as ``mocoef`` to be used as starting orbitals.
    !! 11. Finally, the ``transmomin`` file is written by the ``col_write_transmomin``
    !! routine (see [[col_write_transmomin]] documentation).
    type(nx_qm_t), intent(inout) :: nx_qm
    !! QM configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Main trajectory object.
    type(nx_config_t), intent(in) :: conf
    !! General NX configuration.

    integer :: nstatdyn
    integer :: nstat
    integer :: step, init_step
    integer :: lvprt
    integer :: typeofdyn

    integer :: ierr
    logical :: ext
    character(len=256), allocatable :: filelist(:)
    character(len=:), allocatable :: redtol

    character(len=10), parameter :: files_to_clean(10) = [ &
         & 'curr_iter ', 'runc.error', 'runls     ', &
         & 'COSMO     ', 'GRADIENTS ', 'LISTINGS  ', &
         & 'MOCOEFS   ', 'MOLDEN    ', 'RESTART   ', &
         & 'WORK      '&
         & ]

    character(len=10), parameter :: files_to_exclude(3) = [ &
         & 'geom      ', 'transmomin', 'runc.error'&
         & ]

    ierr = 0

    nstatdyn = traj%nstatdyn
    nstat = conf%nstat
    step = traj%step
    init_step = conf%init_step
    lvprt = conf%lvprt
    typeofdyn = traj%typeofdyn

    if (traj%step > conf%init_step) then
       ! Here we backup the previous molecular orbitals as mocoef.old, for use with the
       ! ``overlap`` module, or for using them as starting orbitals later if col_mocoef
       ! == 1.  The file is not copied as mocoef directly for easier usage in the overlap
       ! module (we just have to deal with one name !)
       ierr = copy('MOCOEFS/mocoef_mc.sp', 'mocoef.old')
       call check_error(ierr, COL_ERR_MAIN, &
            & 'COLUMBUS: Cannot copy MOCOEFS/mocoef_mc.sp')
    end if

    ! Remove all leftover files from the previous computation
    ierr = rm(files_to_clean)
    call check_error(ierr, COL_ERR_MAIN, &
         & 'COLUMBUS: Problem in cleaning working directory.')

    ! Copy the original input files (except those we do not want, like geom or transmomin
    ! for instance).
    call get_list_of_files(&
         & trim(nx_qm%job_folder), filelist, ierr, &
         & exclude=files_to_exclude, full_path=.true.)
    call check_error(ierr, COL_ERR_MAIN, &
         & 'COLUMBUS: Cannot obtain list of files in input folder.')
    ierr = copy(filelist, './')
    call check_error(ierr, COL_ERR_MAIN, &
         & 'COLUMBUS: Cannot copy files from input folder.')

    ! Now we start updating the different input files.
    redtol = col_wrt_redtol(&
         & nx_qm%col_citol, typeofdyn, nstat, nstatdyn, nx_qm%col_reduce_tol)

    call col_change_keyword_nml('mcscfin', 'mcscfin', &
         & ['ncoupl'], [to_str(nx_qm%col_quad_conv)])

    if (nx_qm%col_mode == 'ms') then
       block
         character(len=MAX_STR_SIZE) :: ms_conf_change(4), ms_conf_keys(4)
         ms_conf_keys = ['ivmode', 'nroot ', 'rtolbk', 'rtolci']
         ms_conf_change(1) = to_str(nx_qm%col_ivmode)
         ms_conf_change(2) = to_str(nstat)
         ms_conf_change(3) = redtol
         ms_conf_change(4) = redtol
         call col_change_keyword_nml('ciudgin.drt1', 'ciudgin.drt1', &
              & ms_conf_keys, ms_conf_change)
       end block
    else if (nx_qm%col_mode == 'ss') then
       block
         character(len=MAX_STR_SIZE) :: ss_conf_change(9), ss_conf_keys(9)
         ss_conf_keys = [&
              & 'nvbkmn', 'nvbkmx', 'niter ', 'nvcimn', &
              & 'nvcimx', 'ivmode', 'rtolbk', 'rtolci', &
              & 'nroot ' &
              & ]
         ss_conf_change(1) = to_str(nstat)
         ss_conf_change(2) = to_str(nstat + 5)
         ss_conf_change(3) = to_str(nstat * nx_qm%col_ci_iter)
         ss_conf_change(4) = to_str(nstat + 2)
         ss_conf_change(5) = to_str(nstat + 5)
         ss_conf_change(6) = to_str(nx_qm%col_ivmode)
         ss_conf_change(7) = redtol
         ss_conf_change(8) = redtol
         ss_conf_change(9) = to_str(nstat)
         call col_change_keyword_nml('ciudgin', 'ciudgin', &
              & ss_conf_keys, ss_conf_change)
         call col_change_keyword_nml('cidenin', 'cidenin', &
              & ['lroot'], [to_str(nstatdyn)])
       end block
    end if

    ! Delete the scf keyword after the first step
    if (traj%step > conf%init_step) then
       call col_delete_keyword('control.run', 'control.run', ['scf'])
    end if

    ! If required, prepare the CI restart computation
    if (nx_qm%col_ci_type >= 1) then
       if (nx_qm%col_cirestart == 1) then
          if (traj%step > conf%init_step) then
             call col_ci_restart_setup(nx_qm%col_mode)
          end if
       end if
    end if

    ! If required, copy the previous mocoef file as the starting coefficients for the new
    ! computation.
    if (nx_qm%col_mocoef == 1) then
       if (traj%step /= conf%init_step) then
          inquire(file='mocoef.old', exist=ext)
          if (ext) then
             ierr = copy('mocoef.old', 'mocoef')
             call check_error(ierr, COL_ERR_MAIN, &
                  & 'COLUMBUS: Cannot copy mocoef.old as mocoef')
          else
             call nx_log%log(LOG_ERROR, &
                  & 'COLUMBUS: mocoef is set to 1, but no mocoef file can be found.')
          end if
       end if
    end if

    if (conf%same_mo) then
       inquire(file='ref_calc/mocoef', exist=ext)
       if (ext) then
          ierr = copy('ref_calc/mocoef', 'mocoef')
          call check_error(ierr, COL_ERR_MAIN, &
               & 'COLUMBUS: Cannot copy ref_calc/mocoef as mocoef', system=.true.)
       end if
    end if

    ! Finally, rewrite the transmomin file for usage with Columbus.
    call col_write_transmomin(nx_qm, conf%dc_method, nstatdyn, nstat)

  end subroutine col_update_input


  subroutine col_run(nx_qm)
    !! Run the Columbus computation.
    !!
    !! The Columbus computation is called with:
    !!
    !!     $COLUMBUS/runc -m col_meme > runls
    !!
    !! Optionally, if ``col_cirestart = 1``, the routine col_ci_restart_preparation is called to
    !! prepare the directory structure (see [[col_ci_restart_preparation]] documentation).
    type(nx_qm_t) :: nx_qm
    !! QM configuration.

    character(len=MAX_STR_SIZE) :: cmd
    integer :: ierr

    call get_environment_variable(name='COLUMBUS', value=cmd)
    write(cmd, '(a, i10)') &
         & trim(cmd)//'/runc -m ', nx_qm%col_mem
    call call_external(cmd, ierr, outfile='runls')

    if (nx_qm%col_cirestart == 1) then
       call nx_log%log(LOG_DEBUG, 'COLUMBUS: Start CI restart preparation')
       call col_ci_restart_preparation()
       call nx_log%log(LOG_DEBUG, 'COLUMBUS: Finished CI restart preparation')
    end if
  end subroutine col_run


  subroutine col_read_outputs(nx_qm, traj, conf, qminfo)
    !! Read the outcome of Columbus job.
    !!
    !! This routine is a wrapper around specific routines for reading
    !! the different files that are the outputs of Columbus jobs.
    type(nx_qm_t), intent(inout) :: nx_qm
    !! General information about the QM job.
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf
    type(nx_qminfo_t), intent(inout) :: qminfo

    character(len=MAX_STR_SIZE) :: enfile
    character(len=MAX_STR_SIZE) :: gradfile
    character(len=MAX_STR_SIZE) :: nadfile
    character(len=MAX_STR_SIZE) :: oscfile

    integer :: step, init_step
    integer :: nstatdyn

    integer :: i, j, nc
    logical :: iext
    integer :: niter, converged
    real(dp), dimension(:, :), allocatable :: components
    real(dp), dimension(:), allocatable :: osc_str
    integer :: nstat, ncoupl
    integer, dimension(:), allocatable :: couplings
    logical :: print_osc
    logical :: ext

    character(len=1024) :: msg
    integer :: nread

    step = traj%step
    nstatdyn = traj%nstatdyn
    init_step = conf%init_step

    nstat = size(qminfo%repot)
    ncoupl = nstat*(nstat - 1) / 2
    allocate(couplings(ncoupl))

    ! FIRST STEP ONLY: populate orbital space
    if (step == init_step .and. .not. qminfo%is_reference) then
       call col_read_orb(nx_qm)
       call nx_qm%orb%print()
    end if

    ! Start by reading MCSCF information
    enfile = 'WORK/mcscfsm'
    call col_read_mcscf_energies(enfile, qminfo%repot, niter, converged)
    if (converged == 0) then
       call nx_log%log(LOG_WARN, 'MCSCF DID NOT CONVERGE')
    end if

    if (nx_qm%col_grad_lvl == 2) then
       ! In this case we have a non-zero maximum excitation level, so we have
       ! to read the CI-related files (MRCI computation).

       enfile = 'WORK/ciudgsm'

       ! Read energies
       call col_read_mrci_energies(enfile, qminfo%repot, niter, converged)
       if (converged == 0) then
          call nx_log%log(LOG_WARN, 'MCSCF DID NOT CONVERGE')
       end if

       ! Read contributions
       enfile = 'WORK/cipcls'
       call col_read_mrci_contributions(enfile, qminfo)

       enfile = 'WORK/ciudgls'
       call col_read_mrci_ciudg(enfile, 0.85_dp, qminfo)

    else
       ! MCSCF only
       ! The contributions can be found either in WORK/mcpcls or in a series
       ! of LISTINGS/mcpclc.drt1.stateI.sp. We test for the presence of the
       ! first, and if it is not there then we use the series of files under
       ! LISTINGS.
       enfile = 'WORK/mcpcls'
       inquire(file=enfile, exist=ext)
       nread = 0
       if (.not. ext) then
          do i=1, nstat
             nread = nread + 1
             write(enfile, '(a,i0,a)') &
                  & 'LISTINGS/mcpcls.drt1.state', i, '.sp'
             write(msg, '(A,I4)') 'Dominant contributions for state ', nread
             call qminfo%append_data(msg)
             call col_read_mcscf_contributions(enfile, qminfo)
          end do
       else
          call col_read_mcscf_contributions(enfile, qminfo)
       end if
    end if


    ! Gradients
    if (nx_qm%compute_nac /= 0) then
       do i=1, size(qminfo%rgrad, 1)
          write(gradfile, '(a,i0,a)') &
               & 'GRADIENTS/cartgrd.drt1.state', i, '.all'
          call col_read_gradients(gradfile, qminfo%rgrad, i)
       end do
    else
       qminfo%rgrad(:, :, :) = 0.0_dp
       gradfile = 'GRADIENTS/cartgrd.all'
       call col_read_gradients(gradfile, qminfo%rgrad, nstatdyn)
    end if

    ! NAD
    if (nx_qm%read_nac == 1) then

       nc = 1
       do i=2, nstat
          do j=1, i-1
             write(nadfile, '(a,i0,a,i0,a)') &
                  & 'GRADIENTS/cartgrd.nad.drt1.state', &
                  & i, '.drt1.state', j, '.sp'
             call col_read_nad(nadfile, qminfo%rnad, nc)
             nc = nc + 1
          end do
       end do

    end if

    ! Osc str
    allocate(components(3, ncoupl))
    components(:, :) = 0.0_dp
    allocate(osc_str(ncoupl))
    osc_str(:) = 0.0_dp
    print_osc = .false.

    nc = 1
    do i=2, nstat
       do j=1, i-1

          ! A little bit cumbersome, but necessary (Columbus does not
          ! necessarily prints oscillator strengths).
          write(oscfile, '(a,i0,a,i0)') &
               & 'LISTINGS/trncils.FROMdrt1.state', &
               & i, 'TOdrt1.state', j
          inquire(file=oscfile, exist=iext)
          if (iext) then
             print_osc = .true.
             call col_read_osc_str(oscfile, components(:, nc),&
                  & osc_str(nc))
          end if

          nc = nc + 1
       end do
    end do

    if (print_osc) then
       call qminfo%append_data('')
       call qminfo%append_data('Oscillator strengths and transition moments')
       call qminfo%append_data('')
       write(msg, '(5a15)') 'Excitation', 'Osc. str.', 'dx', 'dy', 'dz'
       call qminfo%append_data(msg)
       call qminfo%append_data(repeat(' --------------', 5))
       do i=1, ncoupl
          write(msg, '(i15, 4F15.6)') &
               & i, osc_str(i), (components(j, i), j=1, 3)
          call qminfo%append_data(msg)
       end do
    end if

    ! Now we transfer the oscillator strengths to nx_qm object, but
    ! there we can only save the couplings from the GS (to remain
    ! consistent with other codes).
    nc = 1
    do i=1, nstat - 1
       nx_qm%osc_str(i) = osc_str(nc)
       nc = nc + i
    end do

    deallocate(components)
    deallocate(osc_str)
  end subroutine col_read_outputs


  subroutine col_get_ninact(nx_qm)
    !! Find the number of inactive (doubly occupied) orbitals.
    !!
    !! The routine will read either the ``CIDRT`` input file (for MR-CI
    !! computation) or the ``MCDRT`` output file (MCSCF) to find the number
    !! of inactive orbitals.  This information will be useful for running
    !! ``cioverlap``.  This routine is intended to be run once, just after
    !! the first run of Columbus.
    type(nx_qm_t), intent(inout) :: nx_qm
    !! ``nx_qm`` object where ``ninact`` will be set.

    integer :: i, j, u, io, id1, id2, id3, istep
    integer :: ninact, nlast
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    character(len=MAX_STR_SIZE), allocatable :: mainbuf(:)
    ! All the file until we encounter the 'step masks' flag
    character(len=MAX_STR_SIZE), allocatable :: mask(:)
    character(len=MAX_STR_SIZE), allocatable :: temp(:)
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE) :: infile
    logical :: ext

    ninact = 0
    allocate(mainbuf(512))
    istep = 0

    ! 1. Find the number of inactive orbitals
    if (nx_qm%col_ci_type == 0) then
       open(newunit=u, file='LISTINGS/mcdrtls.drt1.sp', action='read')
       do
          read(u, '(a)', iostat=io) buf
          if (io /= 0) exit

          id1 = index(buf, 'number of doubly-occupied orbitals:')
          if (id1 /= 0) then
             call split_blanks(buf, split)
             nlast = size(split)
             read(split(nlast), *) ninact
          end if
       end do
       close(u)
    else
       ! 0. Find the file to read
       inquire(file='cidrtin', exist=ext)
       if (ext) then
          infile = 'cidrtin'
       else
          inquire(file='cidrtmsin', exist=ext)
          if (ext) then
             infile = 'cidrtmsin'
          else
             inquire(file='mcdrtin.1', exist=ext)
             if (ext) then
                infile = 'mcdrtin.1'
             else
                STOP 'Error: impossible to read either cidrtin or cidrtmsin, w&
                     &hile ciudg or ciudgav is used !!'
             end if

          end if

       end if

       ! 1. Load all the file until the flag 'step masks'
       i = 1
       open(newunit=u, file=infile, action='read')
       do
          read(u, '(a)', iostat=io) buf
          if (io /= 0) exit

          mainbuf(i) = buf

          id1 = index(buf, 'step masks r')
          id3 = index(buf, 'step masksr')
          id2 = index(buf, 'step masks')

          if ((id2 /= 0) .and. (id1 == 0) .and. (id3 == 0)) then
             istep = i
             exit
          end if

          i = i + 1
       end do
       close(u)

       ! 2. Now keep only the lines with the relevant masks
       allocate(mask(istep))
       mask(:) = ''
       mask(istep) = mainbuf(istep)

       do i=1, istep+1
          j = istep - i
          id1 = index(mainbuf(j), '/')
          if (id1 == 0) then
             mask(j) = mainbuf(j)
          else
             exit
          end if
       end do

       ! 3. Among the lines kept, find the '1000'
       do i=1, size(mask)
          if (mask(i) /= '') then
             ! Count the number of blanks (i.e. the number of elements in the line)
             call split_blanks(mask(i), temp)

             do j=1, size(temp)
                id1 = index(temp(j), '1000')
                if (id1 /= 0) then
                   ninact = ninact + 1
                end if
             end do

          end if
       end do
    end if
    nx_qm%orb%ninact = ninact
  end subroutine col_get_ninact


  subroutine col_read_orb(nx_qm)
    !! Populate the ``orb`` member of ``nx_qm``.
    !!
    type(nx_qm_t), intent(inout) :: nx_qm

    integer :: u, io, id
    integer :: nbas, nelec, ncore, ndisc
    integer :: nlast
    character(len=MAX_STR_SIZE) :: infile
    character(len=MAX_CMD_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(:), allocatable :: split

    nbas = 0
    nelec = -1
    ncore = 0
    ndisc = 0

    ! First, find the number of inactive orbitals
    call col_get_ninact(nx_qm)

    if (nx_qm%col_ci_type == 0) then
       infile = 'LISTINGS/mcscfls.sp'
    else
       infile = 'WORK/mcscfls'
    end if

    nelec = -1
    open(newunit=u, file=infile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'Total number of basis functions:')
       if (id /= 0) then
          call split_blanks(buf, split)
          nlast = size(split)
          read(split(nlast), *) nbas
       end if

       id = index(buf, 'Total number of electrons:')
       if ((id /= 0) .and. (nelec == -1)) then
          call split_blanks(buf, split)
          nlast = size(split)
          read(split(nlast), *) nelec
       end if

       id = index(buf, 'number of frozen core orbitals')
       if (id /= 0) then
          call split_blanks(buf, split)
          nlast = size(split)
          read(split(nlast), *) ncore
       end if

       id = index(buf, 'number of frozen virtual orbitals')
       if (id /= 0) then
          call split_blanks(buf, split)
          nlast = size(split)
          read(split(nlast), *) ndisc
       end if
    end do
    close(u)

    if (nx_qm%col_ci_type == 0) then
       ncore = nx_qm%orb%ninact
       nelec = nelec - 2 * nx_qm%orb%ninact
       ndisc = 0
    end if

    nx_qm%orb%nbas = nbas
    nx_qm%orb%nfrozen = ncore
    nx_qm%orb%nelec = nelec
    nx_qm%orb%ndisc = ndisc
  end subroutine col_read_orb


  subroutine col_read_gradients(gradfile, rgrad, state)
    !! Read Columbus gradient files.
    !!
    !! In Columbus gradient files, the first three lines have to be
    !! skipped.
    character(len=*), intent(in) :: gradfile
    real(dp), dimension(:, :, :), intent(inout) :: rgrad
    integer, intent(in):: state

    integer :: u, i, j
    character(len=MAX_CMD_SIZE) :: line

    open(newunit=u, file=gradfile, action='read')
    do i=1, 3
       read(u, '(a)') line
    end do
    do i=1, size(rgrad, 3)
       read(u, *) (rgrad(state, j, i), j=1, 3)
    end do
    close(u)

  end subroutine col_read_gradients


  subroutine col_read_nad(nadfile, rnad, coupl)
    !! Read Columbus NAD files.
    !!
    character(len=*), intent(in) :: nadfile
    real(dp), dimension(:, :, :), intent(inout) :: rnad
    integer, intent(in):: coupl

    integer :: u, i, j

    open(newunit=u, file=nadfile, action='read')
    do i=1, size(rnad, 3)
       read(u, *) (rnad(coupl, j, i), j=1, 3)
    end do
    close(u)
  end subroutine col_read_nad


  subroutine col_initial_phase(rnad, nstat)
    !! Modify the couplings with the phase obtained from the CI vectors.
    !!
    !! The phase is obtained from the file ``WORK/cipcls``, from the sign of the
    !! coefficient of the main contribution to the wavefunction.
    real(dp), intent(inout) :: rnad(:, :, :)
    !! Non-adiabatic couplings as read from Columbus.
    integer, intent(in) :: nstat
    !! Number of states.

    integer, allocatable :: couplings(:)

    integer, dimension(:), allocatable :: ci_sign

    integer :: u, i, io, id, j, cc
    character(len=MAX_CMD_SIZE) :: buf
    character(len=MAX_CMD_SIZE), dimension(5) :: split
    real(dp) :: tmp
    integer :: state

    allocate(ci_sign(nstat))
    allocate(couplings(size(rnad, 1)))
    ci_sign(:) = 1
    state = 0

    open(newunit=u, file='WORK/cipcls', action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'indcsf')
       if (id /= 0) then
          state = state + 1
          read(u, '(a)') buf
          read(u, *) split
          read(split(2), *) tmp
          if (tmp < 0.0_dp) ci_sign(state) = -1
       end if
    end do
    close(u)

    cc = 1
    couplings(:) = 1
    do i=2, size(ci_sign)
       do j=1, i-1
          if (ci_sign(i)*ci_sign(j) < 0) couplings(cc) = -1
          cc = cc + 1
       end do
    end do

    do i=1, size(rnad, 1)
       rnad(i, :, :) = rnad(i, :, :) * couplings(i)
    end do

  end subroutine col_initial_phase


  subroutine col_read_osc_str(oscfile, components, osc_str)
    !! Read oscillator strength and transition components.
    !!
    character(len=*), intent(in) :: oscfile
    !! File to read.
    real(dp), dimension(3), intent(out) :: components
    !! x, y and z components of the transition moment.
    real(dp), intent(out) :: osc_str
    !! OScillator strength

    integer :: u, id, io, i
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(5) :: split
    character(len=MAX_STR_SIZE), dimension(4) :: split2

    open(newunit=u, file=oscfile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'Transition moment components')
       if (id /= 0) then
          do i=1, 3
             read(u, '(a)') buf
          end do

          read(u, *) split
          ! read(buf, *) split
          do i=1, 3
             read(split(i+1), *) components(i)
          end do

       end if

       id = index(buf, 'Oscillator strength')
       if (id /=0) then
          read(buf, *) split2
          read(split2(4), *) osc_str
       end if
    end do

    close(u)

  end subroutine col_read_osc_str


  subroutine col_read_mcscf_energies(mcscffile, repot, niter, converged)
    !! Read the MCSCF energies from a Columbus computation.
    !!
    !! The routine parses the file ``mscsffile``, and populates the ``repot`` array with
    !! the obtained MCSCF energies.  It also returns the number of iterations as
    !! ``niter``, and reports if the computation has converged or not (``converged = 1 or
    !! 0`` respectively).
    character(len=*), intent(in) :: mcscffile
    !! Output of the MCSCF job.
    real(dp), dimension(:), intent(out) :: repot
    !! MCSCF potential energies.
    integer, intent(out) :: niter
    !! Number of iterations done.
    integer, intent(out) :: converged
    !! If 1, the job has converged. Else, no convergence.

    integer :: u, io, id
    integer :: istate
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(8) :: split

    converged = 0
    istate = 1

    open(newunit=u, file=mcscffile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'final mcscf')
       if (id /= 0) then
          read(u, *) split
          read(split(1), *) niter
          id = index(split(8), 'converged')
          if (id /= 0) converged = 1
       end if

       id = index(buf, 'total energy=')
       if (id /= 0) then
          read(buf(id+13:id+13+18), '(F19.9)') repot(istate)
          ! write(*, *) trim(buf)
          istate = istate + 1

          ! Exit if we read enough energies already
          if (istate > size(repot)) exit
       end if
    end do
    close(u)
  end subroutine col_read_mcscf_energies


  subroutine col_read_mcscf_contributions(mcscffile, qminfo)
    !! Read the contributions to the MCSCF wavefunction.
    !!
    !! The routine parses the file ``mcscffile``, and populates ``qminfo%datread`` with
    !! the contributions to the wavefunction.
    character(len=*), intent(in) :: mcscffile
    !! Output of the MCSCF job.
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Extracted information from QM job.

    integer :: u, io, id, i
    character(len=MAX_STR_SIZE) :: buf

    if (qminfo%dim_dataread == 0) then
       call qminfo%append_data('Dominant contributions to the wavefunction')
    end if

    open(newunit=u, file=mcscffile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'csf       coeff       coeff')
       if (id /= 0) then
          call qminfo%append_data(buf)
          do i=1, 4
             read(u, '(a)') buf
             call qminfo%append_data(buf)
          end do
       end if
    end do
    call qminfo%append_data('')
    close(u)
  end subroutine col_read_mcscf_contributions


  subroutine col_read_mrci_energies(mrcifile, repot, niter, converged)
    !! Read MRCI energies from Columbus output file.
    !!
    !! The routine parses ``mrcifile`` and populates the ``repot`` array with the
    !! electronic MRCI energies found.  It also reports the total number of iteration in
    !! the computation as ``niter``, and if the computation has converged or not
    !! (``converged = 1 or 0`` respectively).
    character(len=*), intent(in) :: mrcifile
    !! Output of the MCSCF job.
    real(dp), dimension(:), intent(out) :: repot
    !! MCSCF potential energies.
    integer, intent(out) :: niter
    !! Number of iterations done.
    integer, intent(out) :: converged
    !! If 1, the job has converged. Else, no convergence.

    integer :: u, io, id
    integer :: istate
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(7) :: split

    converged = 0
    istate = 1

    open(newunit=u, file=mrcifile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'convergence criteria satisfied')
       if (id /= 0) then
          read(buf, *) split
          read(split(6), *) niter
          converged = 1
       end if

       id = index(buf, 'convergence information')
       if (id /= 0) then

          do istate=1, size(repot)
             read(u, *) split
             read(split(5), *) repot(istate)
          end do
       end if
    end do
    close(u)
  end subroutine col_read_mrci_energies


  subroutine col_read_mrci_contributions(mrcifile, qminfo)
    !! Read the contributions to the MRCI wavefunction.
    !!
    !! The routine parses the file ``mrcifile``, and populates ``qminfo%datread`` with
    !! the contributions to the wavefunction.
    character(len=*), intent(in) :: mrcifile
    !! Output of the MCSCF job.
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Extracted information from QM job.

    integer :: u, io, id, i
    integer :: istate
    character(len=MAX_STR_SIZE) :: buf
    character(len=1024) :: msg

    istate = 1
    open(newunit=u, file=mrcifile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'indcsf')
       if (id /= 0) then
          write(msg, '(a,i4)') 'Dominant contributions for state ', istate
          call qminfo%append_data(msg)
          call qminfo%append_data(buf)

          do i=1, 4
             read(u, '(a)') buf
             call qminfo%append_data(buf)
          end do
          call qminfo%append_data('')
          istate = istate + 1
       end if
    end do
    close(u)
  end subroutine col_read_mrci_contributions


  subroutine col_read_mrci_ciudg(mrcifile, refwgt, qminfo)
    !! TODO: Update !!
    character(len=*), intent(in) :: mrcifile
    !! Output of the MCSCF job.
    real(dp), intent(in) :: refwgt
    !! Reference weight: if the weight found is smaller than this
    !! reference a warning is issued
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Extracted information from QM job.

    integer :: u, io, id
    integer :: istate
    real(dp) :: weight
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(7) :: split
    character(len=1), parameter :: nl = NEW_LINE('c')

    istate = 1
    open(newunit=u, file=mrcifile, action='read')
    do
       read(u, '(a)', iostat=io) buf
       if (io /= 0) exit

       id = index(buf, 'relaxed')
       if (id /= 0) then
          read(buf, *) split
          read(split(7), *) weight
          if (weight < refwgt) then
             write(buf, '(a)') &
                  & 'LOW WEIGHT OF REFERENCE WAVE FUNCTION for state '//to_str(istate)//&
                  & ': cnot**2 = '//to_str(weight, fmt='(F20.12)')
             ! write(buf, '(a, i0, a, F6.2, a)') &
             !      & trim(buf)//"   FOR STATE ", istate, " WEIGHT IS SMALLER THAN ", &
             !      & refwgt, " !!  "//nl
             ! write(buf, '(a, F20.12,a)') trim(buf)//"   cnot**2 = ", weight, nl
             ! call qminfo%append_data(buf)
             call nx_log%log(LOG_WARN, buf)
          end if
       end if
    end do
    close(u)
  end subroutine col_read_mrci_ciudg


  subroutine col_load_file(filename, loaded)
    !! Load a file in memory.
    !!
    !! The file is loaded as an array ``loaded`` of strings of size ``MAX_STR_SIZE``.
    !! The size of the ``loaded`` array is determined by a first go over the file, so the
    !! routine parses the file twice.
    character(len=*), intent(in) :: filename
    !! File to read.
    character(len=MAX_STR_SIZE), allocatable, intent(inout) :: loaded(:)
    !! Loaded file.

    integer :: u, ierr, nlines, i
    character(len=MAX_STR_SIZE) :: buf

    open(newunit=u, file=filename, action='read')
    nlines = 0
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       nlines = nlines + 1
    end do

    rewind(u)
    allocate(loaded(nlines))

    do i=1, nlines
       read(u, '(a)') buf
       loaded(i) = to_lower(trim(buf))
    end do
    close(u)
  end subroutine col_load_file


  subroutine col_change_keyword_nml(origin, newfile, keys, values)
    !! Change the value of one or several keywords in namefile.
    !!
    !! The routine starts by loading ``origin`` with ``col_load_file`` (see
    !! [[col_load_file]] documentation), and parses the content of the loaded array.
    !! Whenever a line contains any of the keywords from array ``keys``, with index ``j``,
    !! it replaces the corresponding value with ``values(j)``.  The resulting array is
    !! then written as file ``newfile``.
    !!
    !! Usage:
    !!
    !! ```fortran
    !! call col_change_keyword_nml('file1.nml', 'file2.nml', ['param1'], ['val1'])
    !! ```
    !!
    !! After the call:
    !!
    !! ```shell
    !! $ cat file1.nml
    !!  &namelist
    !!    param1 = 2
    !!    param2 = 4
    !!    param3 = 'temp'
    !!  /
    !! $ cat file2.nml
    !!  &namelist
    !!    param1 = val1
    !!    param2 = 4
    !!    param3 = 'temp'
    !!  /
    !! ```
    !!
    character(len=*), intent(in) :: origin
    !! File to be modified.
    character(len=*), intent(in) :: newfile
    !! File written with modified content (usually the same name as ``origin``).
    character(len=*), intent(in) :: keys(:)
    !! List of keys to modify.
    character(len=*), intent(in) :: values(:)
    !! For each key, the corresponding modified value.

    character(len=MAX_STR_SIZE), allocatable :: loaded(:)
    character(len=:), allocatable :: buf
    integer :: i, j, id, u

    call col_load_file(origin, loaded)

    do i=1, size(loaded)
       id = index(loaded(i), '=')
       if (id /= 0) then
          ! We found a line with a keyword on it, we are only interested in the part
          ! before the equal sign
          buf = remove_blanks( loaded(i)(1:id-1) )
          do j=1, size(keys)
             if (trim(buf) == trim(keys(j))) then
                loaded(i) = trim(keys(j))//' = '//trim(values(j))//','
             end if
          end do
          if (loaded(i)(len_trim(loaded(i)):len_trim(loaded(i))) == ',') &
               & loaded(i)(len_trim(loaded(i)):len_trim(loaded(i))) = ''
          deallocate(buf)
       end if
    end do

    open(newunit=u, file=newfile, action='write')
    do i=1, size(loaded)
       write(u, '(a)') trim(loaded(i))
    end do
    close(u)
  end subroutine col_change_keyword_nml


  subroutine col_delete_keyword(oldfile, newfile, key)
    !! Delete a keyword from the given file.
    !!
    !! The routine deletes a keyword from a namelist, or from a file containing simply a
    !! list of keywords (in particular ``control.run``).
    !!
    !! Usage:
    !!
    !! ```fortran
    !! call col_delete_keyword('myfile.nml', 'modfile.nml', ['key1', 'key2'])
    !! ```
    !!
    !! The result is:
    !! ```shell
    !! $ cat myfile.nml
    !!  &namelist
    !!    key1 = 2
    !!    key2 = 4
    !!    key3 = 'temp'
    !!  /
    !! $ cat modfile.nml
    !!  &namelist
    !!    key3 = 'temp'
    !!  /
    !! ```
    !!
    character(len=*), intent(in) :: oldfile
    !! File to read.
    character(len=*), intent(in) :: newfile
    !! Modified file (usually the same as ``oldfile``).
    character(len=*), intent(in) :: key(:)
    !! List of keys to delete.

    integer :: u, id, i, j
    character(len=MAX_STR_SIZE), allocatable :: loaded(:), new_array(:)
    character(len=MAX_STR_SIZE) :: buf

    call col_load_file(oldfile, loaded)

    allocate( new_array(size(loaded)) )
    new_array(:) = ''

    id = 1
    do i=1, size(loaded)
       if (index('=', loaded(i)) /= 0) then
          ! We have a namelist with 'key = value'
          buf = trim(loaded(i)(1:id))
       else
          ! We have simply one key per line, as in control.run
          buf = loaded(i)
       end if

       buf = remove_blanks(buf)

       do j=1, size(key)
          if (trim(buf) /= key(j)) then
             new_array(id) = trim(buf)
             id = id + 1
          end if
       end do
    end do

    open(newunit=u, file=newfile, action='write')
    do i=1, size(new_array)
       write(u, '(a)') trim(new_array(i))
    end do
    close(u)
  end subroutine col_delete_keyword


  subroutine col_ci_restart_setup(mode)
    !! Set up up the CI restart procedure.
    !!
    !! If the backup folder ``WORK1`` exists (*i.e.* we are after step 0), then the
    !! keyword ``ivmode`` is set to 4 in ``ciudgin`` (``ciudgin.drt1`` for ``mode =
    !! ms``).   Then the ``drt1`` suffix is either added or removed from the files in
    !! ``WORK1``, depending on the ``mode`` value.  Finally, the ``WORK1`` directory is
    !! renamed as ``WORK`` to be read by Columbus.
    character(len=*), intent(in) :: mode
    !! Columbus mode of the computation.

    character(len=MAX_STR_SIZE) :: ciu, cif, cio, ciubad, cifbad, ciobad
    logical :: ext
    integer :: ierr

    ciu = ''
    cif = ''
    cio = ''
    ciubad = ''
    cifbad = ''
    ciobad = ''

    if (mode == 'ss') then
       ciu = 'ciudgin'
       cif = 'civfl_restart'
       cio = 'civout_restart'
       cifbad = 'civout_restart.drt1'
       ciobad = 'civout_restart.drt1'
    else if (mode == 'ms') then
       ciu = 'ciudgin.drt1'
       cif = 'civfl_restart.drt1'
       cio = 'civout_restart.drt1'
       cifbad = 'civfl_restart'
       ciobad = 'civout_restart'
    end if

    inquire(file='WORK1/', exist=ext)
    if (ext) then
       call col_change_keyword_nml(ciu, ciu, ['ivmode'], ['4'])

       inquire(file='WORK1/'//trim(cifbad), exist=ext)
       if (ext) then
          ierr = rename('WORK1/'//trim(cifbad), 'WORK1/'//trim(cif))
          call check_error(ierr, COL_ERR_MAIN, &
               & 'COLUMBUS: Cannot move ci vector files !')
          ierr = rename('WORK1/'//trim(ciobad), 'WORK1/'//trim(cio))
          call check_error(ierr, COL_ERR_MAIN, &
               & 'COLUMBUS: Cannot move ci vector files !')
       end if

       call execute_command_line('mv WORK1 WORK', exitstat=ierr)
       call check_error(ierr, COL_ERR_MAIN, &
            & 'COLUMBUS: Cannot rename WORK1 directory !')
    end if
  end subroutine col_ci_restart_setup


  subroutine col_ci_restart_preparation()
    !! Prepare the restart of the CI computation at the next step.
    !!
    !! The routine starts by creagin a ``WORK1`` folder, and copies the following files:
    !!
    !! - ``WORK/civfl.drt1`` as ``WORK1/civfl_restart.drt1`` ;
    !! - ``WORK/civout.drt1`` as ``WORK1/civout_restart.drt1`` ;
    !!
    !! (If the files with the ``drt1`` suffix don't exist, then the same operation are carried
    !! out on the files without the suffix.)
    !!
    !! The folder ``WORK1`` is used to back up the files requires for the restart, and
    !! this folder will be renamed at the beginning of the next computation step (see
    !! [[col_ci_restart_1]] and [[col_update_input]] documentations).
    integer :: ierr
    logical :: ext
    inquire(file='WORK1/', exist=ext)
    if (ext) then
       ierr = rm(['WORK1/'])
       call check_error(ierr, COL_ERR_MAIN, 'COLUMBUS: Cannot delete WORK1/')
    end if
    ierr = mkdir('WORK1')
    call check_error(ierr, COL_ERR_MAIN, 'COLUMBUS: Cannot create WORK1/')

    inquire(file='WORK/civfl.drt1', exist=ext)
    if (ext) then
       ierr = copy(&
            & ["WORK/civfl.drt1 ", "WORK/civout.drt1"], &
            & ["WORK1/civfl_restart.drt1 ", "WORK1/civout_restart.drt1"]&
            & )
       call check_error(ierr, COL_ERR_MAIN, 'COLUMBUS: (cirestart) Cannot backup files ')
    else
       ierr = copy(&
            & ["WORK/civfl ", "WORK/civout"], &
            & ["WORK1/civfl_restart ", "WORK1/civout_restart"]&
            & )
       call check_error(ierr, COL_ERR_MAIN, 'COLUMBUS: (cirestart) Cannot backup files ')
    end if
  end subroutine col_ci_restart_preparation


  subroutine col_write_transmomin(nx_qm, dc, nstatdyn, nstat)
    !! Write the ``transmomin`` file.
    !!
    !! This routine writes a ``transmomin`` file that can be parsed by Columbus.  It
    !! starts by backing up the existing ``transmomin`` file as ``transmomin.nx``, then
    !! it parses ``transmomin.nx``.
    !!
    !! First, we check if the right gradient level is set (MCSCF or CI, depending on the
    !! value of ``col_grad_lvl``).  If needed, we modify the value found in
    !! ``transmomin.nx``.  Then the lines containing the couplings to be computed are
    !! copied from ``transmomin.nx``, adding a ``T`` label at the end if the coupling
    !! should not be computed (for ``derivative_coupling`` different than one, so for
    !! dynamics where the couplings are either not needed (local diabatization) or
    !! computed by different means).
    !!
    !! Finally, depending on the value of ``col_all_grads``, the lines for the gradient
    !! computation are added.
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM configuration.
    integer, intent(in) :: dc
    !! Derivative couplings mode (``conf%dc_coupling`` parameter).
    integer, intent(in) :: nstatdyn
    !! Current dynamic state.
    integer, intent(in) :: nstat
    !! Total number of states.

    integer :: u, v, ierr, i
    integer :: drt1, st1, drt2, st2
    character(len=MAX_STR_SIZE) :: buf
    logical :: ext

    inquire(file='transmomin', exist=ext)
    if (.not. ext) then
       open(newunit=v, file='transmomin', action='write')
       if (nx_qm%col_grad_lvl == 2) then
          write(v, '(a)') 'CI'
       else if (nx_qm%col_grad_lvl == 1) then
          write(v, '(a)') 'MCSCF'
       end if
       write(v, '(a)') ' 1 1 1 1'
       close(v)
       return
    end if

    ierr = copy('transmomin', 'transmomin.nx')
    call check_error(ierr, COL_ERR_MISC, &
         & 'COLUMBUS: Cannot copy transmomin !')

    open(newunit=v, file='transmomin', action='write')
    open(newunit=u, file='transmomin.nx', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if ( index(buf, 'MCSCF') /= 0 ) then
          if (nx_qm%col_grad_lvl == 2) then
             ! CI gradients
             write(v, '(a)') 'CI'
          else
             write(v, '(a)') trim(buf)
          end if
       else if ( index(buf, 'CI') /= 0 ) then
          if (nx_qm%col_grad_lvl == 1) then
             ! MCSCF gradients
             write(v, '(a)') 'MCSCF'
          else
             write(v, '(a)') trim(buf)
          end if
       else
          read(buf, *) drt1, st1, drt2, st2

          if (dc /= 1) then
             if (st1 /= st2) then
                write(v, '(4I3,A)') drt1, st1, drt2, st2, ' T'
             end if
          else
             if (st1 /= st2) then
                write(v, '(4I3)') drt1, st1, drt2, st2
             end if
          end if
       end if
    end do
    close(u)

    if (nx_qm%col_all_grads == 0) then
       write(v, '(4I3)') 1, nstatdyn, 1, nstatdyn
    else if (nx_qm%col_all_grads == 1) then
       do i=1, nstat
          write(v, '(4I3)') 1, i, 1, i
       end do
    end if
    close(v)
  end subroutine col_write_transmomin



  function col_wrt_redtol(&
       & citol, type_of_dyn, nstat, nstatdyn, reduce_tol &
       & ) &
       & result(res)
    !! Write a string of reduced tolerances.
    !!
    !! The string consists in a number of ``nstat`` statements, giving the
    !! tolerance in the CI iteration for the corresponding state. The basis is
    !! given by the ``citol`` parameter. This value is always set for the state of
    !! interest (i.e. ``nstatdyn``). The value for the other states depend on
    !! ``reduce_tol``, ``type_of_dyn``, and on the content of the transmomin
    !! file.
    !!
    !! With ``reduce_tol = 1``, all tolerances will be set to ``citol * 10``. With
    !! ``reduce_tol = 2``, the tolerance is set to ``citol * 10`` if ``type_of_dyn = 1``
    !! (*i.e.* the state of interest is not "too far" from the surrounding
    !! states) or if a coupling between this state and the state of interest is
    !! to be computed. In all other cases, the tolerance is set to ``citol * 1000``.
    !!
    !! Usage:
    !! ```fortran
    !! character(len=:, allocatable) :: redtol
    !! redtol = col_wrt_redtol(citol, type_of_dyn, nstat, nstatdyn, reduce_tol)
    !! ```
    !!
    !! Example:
    !!
    !! ```fortran
    !! character(len=:, allocatable) :: redtol1, redtol2, redtol3, redtol4, redtol5
    !! redtol1 = col_wrt_redtol(1E-4, 1, 3, 2, 1)
    !! redtol2 = col_wrt_redtol(1E-4, 1, 3, 2, 0)
    !! redtol3 = col_wrt_redtol(1E-4, 1, 3, 3, 1)
    !! redtol4 = col_wrt_redtol(1E-4, 2, 4, 2, 2) ! (assuming kross = 0)
    !! redtol5 = col_wrt_redtol(1E-4, 2, 4, 2, 2) ! (assuming kross = 1)
    !!
    !! print *, 'REDTOL1: ', redtol1
    !! print *, 'REDTOL2: ', redtol2
    !! print *, 'REDTOL3: ', redtol3
    !! print *, 'REDTOL4: ', redtol4
    !! print *, 'REDTOL5: ', redtol5
    !! ```
    !!
    !! The results will be:
    !!
    !!     REDTOL1: 1E-3, 1E-4, 1E-3
    !!     REDTOL2: 1E-4, 1E-4, 1E-4
    !!     REDTOL3: 1E-3, 1E-3, 1E-4
    !!     REDTOL4: 1E-3, 1E-4, 1E-3, 1E-1
    !!     REDTOL5: 1E-3, 1E-4, 1E-3, 1E-3
    !!
    real(dp), intent(in) :: citol
    !! CI tolerance (``col_citol``).
    integer, intent(in) :: type_of_dyn
    !! Current type of dynamics, as given by ``type_of_dynamics`` routine from ``mod_md_utils``.
    integer, intent(in) :: nstat
    !! Number of states.
    integer, intent(in) :: nstatdyn
    !! Current dynamic state.
    integer, intent(in) :: reduce_tol
    !! How to reduce the tolerance (``col_redtol``).

    character(len=:), allocatable :: res
    character(len=MAX_STR_SIZE) :: tmp

    integer :: istring, ele
    real(dp) :: tol0
    integer, allocatable :: pairs(:)

    tmp = ''
    do istring=1, nstat
       if (istring == nstatdyn) then
          tol0 = citol
       else
          if (reduce_tol == 0) then
             tol0 = citol
          else if (reduce_tol == 1) then
             tol0 = citol * 10_dp
          else if (reduce_tol == 2) then

             if (type_of_dyn == 1) then
                tol0 = citol * 10_dp
             else
                tol0 = citol * 1000_dp
             end if

             call col_read_transmomin('transmomin', 'CI', pairs)
             do ele=1, size(pairs)
                if (istring == pairs(ele)) then
                   tol0 = citol * 10_dp
                end if
             end do
          end if
       end if

       write(tmp, '(A,E7.1,A)') trim(tmp)//' ', tol0, ','
    end do

    res = trim(tmp)
  end function col_wrt_redtol


  subroutine col_read_transmomin(filename, level, pairs)
    !! Find the couplings to be computed from a ``transmomin`` file.
    !!
    !! The routine parses ``transmomin`` and returns an array ``pairs`` with elements
    !! grouped by 2, giving the couplings to be computed.
    character(len=*), intent(in) :: filename
    !! File to parse.
    character(len=*), intent(in) :: level
    !! Level at which the gradients are computed.
    integer, intent(inout) :: pairs(:)
    !! Resulting pairs array.

    integer :: u, ierr, id
    integer :: d1, d2, st1, st2
    character(len=MAX_STR_SIZE) :: buf
    logical :: start_read

    start_read = .false.
    pairs(:) = -1
    id = 1
    open(newunit=u, file=filename, action='read')
    READ_TRANSMOMIN: do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit READ_TRANSMOMIN

       if ( index(level, buf) /= 0 ) start_read = .true.

       if (start_read) then
          do
             read(u, '(a)', iostat=ierr) buf
             if (ierr /= 0) exit READ_TRANSMOMIN

             read(buf, *) d1, st1, d2, st2
             if (d1 /= 1) exit READ_TRANSMOMIN

             pairs(id) = st1
             pairs(id+1) = st2

             id = id+2
          end do
       end if
    end do READ_TRANSMOMIN
    close(u)
  end subroutine col_read_transmomin


  function col_ci_type(infile) result(res)
    character(len=*), intent(in) :: infile

    integer :: res

    character(len=MAX_STR_SIZE), allocatable :: loaded(:)
    integer :: i
    logical :: nadcoupl, mcscf, pciudgav, ciudg

    nadcoupl = .false.
    mcscf = .false.
    pciudgav = .false.
    ciudg = .false.

    call col_load_file(infile, loaded)
    do i=1, size(loaded)
       if (loaded(i) == 'pciudg') then
          pciudgav = .true.
       end if

       if (loaded(i) == 'ciudg' .or. loaded(i) == 'cigrad') then
          ciudg = .true.
       end if

       if (loaded(i) == 'nadcoupl') then
          nadcoupl = .true.
       end if

       if (loaded(i) == 'mcscf') then
          mcscf = .true.
       end if
    end do

    if (pciudgav) then
       res = 2
       return
    else if (ciudg) then
       res = 1
       return
    else if (nadcoupl .and. .not. mcscf) then
       res = 1
       return
    else
       res = 0
       return
    end if
  end function col_ci_type


  function col_get_mode(infile) result(res)
    character(len=*), intent(in) :: infile

    character(len=:), allocatable :: res

    integer :: u, ierr
    character(len=MAX_STR_SIZE) :: buf

    res = "mc"
    open(newunit=u, file=infile, action='read')
    LOOP: do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'ciudgav') /= 0) then
          res = 'ms'
          exit LOOP
       else if (index(buf, 'ciudg') /= 0) then
          res = 'ss'
          exit LOOP
       else if (index(buf, 'mcscfgrad') /= 0) then
          res = 'mcs'
          exit LOOP
       end if
    end do LOOP
    close(u)

  end function col_get_mode


  function col_get_maximum_excitation_level(infile) result(res)
    character(len=*), intent(in) :: infile

    integer :: res

    integer :: u, ierr
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: group(:)

    res = -1
    open(newunit=u, file=infile, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'maximum excitation level') /= 0) then
          group = split_pattern(buf, pattern='/')
          read(group(1), *) res
       end if
    end do
    close(u)
  end function col_get_maximum_excitation_level



  subroutine col_init_double_molecule()
    integer :: ierr
    logical :: ext

    inquire(file='overlap.old/', exist=ext)
    if (ext) then
       print *, 'FOUND OVERLAP.OLD'
       ierr = rm(['overlap.old'])
       call check_error(ierr, COL_ERR_MISC, 'COL: Cannot delete overlap.old')
    end if

    inquire(file='overlap/', exist=ext)
    if (ext) then
       ierr = rename('overlap/', 'overlap.old/')
       call check_error(ierr, COL_ERR_MISC, 'COL: Cannot rename overlap')
    else
       ierr = mkdir('overlap.old')
       call check_error(ierr, COL_ERR_MISC, 'COL: Cannot create overlap.old')
    end if

    inquire(file='WORK/daltaoin', exist=ext)
    if (ext) then
       ierr = copy('WORK/daltaoin', 'overlap.old/daltaoin_single.old')
       call check_error(ierr, COL_ERR_MISC, 'COL: Cannot copy WORK/daltaoin as overlap.old/daltaoin_single.old')
    end if
  end subroutine col_init_double_molecule


  subroutine col_prepare_double_molecule()
    logical :: ext
    integer :: u, v, w, ierr, i
    character(len=MAX_STR_SIZE) :: buf, dum
    character(len=1) :: contraction
    integer :: nat, maxpri

    character(len=MAX_STR_SIZE), parameter :: &
         & origin = 'WORK/daltaoin', &
         & oldinp = 'overlap.old/daltaoin_single.old', &
         & newinp = 'overlap/daltaoin',&
         & daltcom_single = 'WORK/daltcomm', &
         & daltcom_new = 'overlap/daltcomm'

    inquire(file=origin, exist=ext)
    if (.not. ext) then
       call nx_log%log(LOG_ERROR, &
            & 'COLUMBUS: Cannot find '//trim(origin)//', required for double molecule !')
       error stop
    end if

    open(newunit=u, file=origin, action='read')
    open(newunit=v, file=newinp, action='write')
    ! Copy first three lines without modifications
    do i=1, 3
       read(u, '(a)') buf
       write(v, '(a)') trim(buf)
    end do

    ! For the fourth line, we need to update the second number
    read(u, '(a)') buf
    contraction = buf(1:1)
    read(buf(2:5), *) nat
    read(buf(6:), '(A)') dum
    write(v, '(a,I4,a)') contraction, 2*nat, trim(dum)

    open(newunit=w, file=oldinp, action='read')
    do i=1, 4
       read(w, '(a)') buf
    end do
    do
       read(w, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
    end do

    ! We already read the first 4 lines of this file before
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
    end do

    close(u)
    close(w)
    close(v)

    maxpri = -100
    open(newunit=u, file=daltcom_single, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, '.MAXPRI') /= 0) then
          read(u, *) maxpri
          exit
       end if
    end do
    close(u)

    open(newunit=u, file=daltcom_new, action='write')
    write(u, '(a)') "**DALTONINPUT"
    write(u, '(a)') ".INTEGRALS"
    write(u, '(a)') ".PRINT"
    write(u, '(a)') "   2"
    write(u, '(a)') "**INTEGRALS"
    write(u, '(a)') ".PRINT"
    write(u, '(a)') "   2"
    write(u, '(a)') ".NOSUP"
    write(u, '(a)') ".NOTWO"
    write(u, '(a)') "*READIN"
    if (maxpri /= -100) then
       write(u, '(a)') ".MAXPRI"
       write(u, '(I4)') maxpri
    end if
    write(u, '(a)') "**END OF INPUTS"
    close(u)

    ierr = copy('WORK/daltaoin', 'overlap/daltaoin_single.old')
    call check_error(ierr, COL_ERR_MISC, 'COL: Cannot copy WORK/daltaoin')
  end subroutine col_prepare_double_molecule


  subroutine col_extract_overlap(ovlfile, dim_ovl, ciopath, ovl)
    !! Extract the AO overlap from a Columbus computation.
    !!
    !! The routine works by extracting the AO overlap out of the ``aoints`` file produced
    !! by Dalton.  This file, in SIFS format, can be parsed by the ``readsifs`` program
    !! from the ``cioverlap`` distribution with the following command:
    !!
    !!     $CIOVERLAP/readsifs -1 aoints
    !!
    !! The resulting file is then called ``aoints1S``.
    !!
    !! The ``ovlfile`` parameter should thus always end with ``aoints1S`` (*e.g.*
    !! ``overlap/aoints1S``).
    !!
    !! Once the file has been generated, the routine parses it by reading ``dim_ovl``
    !! records from it and populates the ``ovl`` array with the successive records.  The
    !! obtained overlap matrix (symmetric) is obtained condensed in vector form.
    character(len=*), intent(in) :: ovlfile
    !! File to parse (or generate).
    integer, intent(in) :: dim_ovl
    !! Dimension of the AO overlap matrix.
    character(len=*), intent(in) :: ciopath
    !! Path to the cioverlap programs (``$CIOVERLAP``).
    real(dp), dimension(:), intent(inout) :: ovl
    !! Array containing the overlap in AO basis.

    integer :: u, i, irec
    integer :: status, ierr
    character(len=MAX_CMD_SIZE) :: cmd
    character(len=MAX_STR_SIZE) :: mydir

    logical :: ext

    ! First we have to generate the proper file
    inquire(file=ovlfile, exist=ext)
    if (.not. ext) then
       cmd = trim(ciopath)//'/readsifs -1 aoints'
       status = getcwd(mydir)
       status = chdir('./overlap/')
       call call_external(cmd, ierr, outfile='readsifsls')
       status = chdir(mydir)
    end if

    irec = 0
    open(newunit=u, file=ovlfile, form='unformatted', access='direct', recl=8)
    do i=1, dim_ovl
       read(u, rec=i) ovl(i)
       irec = irec + 1
       ! write(v, '(a,i5,a,F25.15)') 'irec = ', irec, ': ', ovl(i)
    end do
    close(u)
  end subroutine col_extract_overlap


  subroutine col_get_lcao(mosfile, nao, lcao)
    !! Extract the LCAO matrix.
    !!
    !! The routine parses ``mosfile`` and extracts the AO to MO transformation matrix, in
    !! AO to MO order.  In Columbus, the matrix is written in the ``mocoef`` file in the
    !! MO to AO order, so we need to transpose it at the end of the routine.
    character(len=*), intent(in) :: mosfile
    !! File containing the LCAO matrix.
    integer, intent(in) :: nao
    !! Number of atomic orbitals.
    real(dp), dimension(:, :), intent(inout) :: lcao
    !! LCAO matrix.

    integer :: u, i, j, k, l
    integer :: ntitle
    integer :: nele, remainder
    character(len=MAX_STR_SIZE) :: buf

    ! nele = aint(float(nao/3)) ! Number of elements in full lines
    nele = int(nao/3)
    remainder = nao - 3*nele  ! Number of elements in partial lines

    ! write(*, *) 'nele = ', nele, '; remainder = ', remainder

    open(newunit=u, file=mosfile, action='read')
    ! Header lines are discarded
    do i=1, 2
       read(u, *)
    end do

    ! Number of title lines
    read(u, *) ntitle
    do i=1, ntitle+5
       read(u, *)
    end do

    ! Now read the coefficient
    do i=1, nao
       k = 1
       do j=1, nele
          read(u, '(a)') buf
          read(buf, *) (lcao(i, l), l=k, k+2)
          k = k+3
       end do
       if (remainder > 0) then
          k = 3*nele
          read(u, *) (lcao(i, l), l=k+1, k+remainder)
       end if
    end do
    close(u)

    ! Now we finished reading, we have to transpose the result
    lcao = transpose(lcao)
  end subroutine col_get_lcao


  subroutine col_prepare_cio(ciopath, ci_type, ci_consolidate, nelec)
    !! Prepare the ``cioverlap`` computation.
    !!
    !! The routine performs several tasks in the ``WORK`` directory to prepare the
    !! execution of the ``cioverlap`` program later.  It starts by creating a list of
    !! Slater determinants as ``eivectors`` with, in the MRCI case, the ``mycipc.x``
    !! executable if it exists (or ``cipc.x`` if not), or in the MCSCF case with the
    !! ``mcpc.x`` program.  The file ``eivectorshead`` is then concatenated with
    !! ``eivectors`` to obtain ``eivectors2``, which will serve as the vector for time \(t\)
    !! in the ``cioverlap`` execution (ket state).
    !!
    !! If required (``ci_consolidate = 1``), the created wavefunction ``eivectors2`` is
    !! then consolidated.
    !!
    !! Finally, the required files (``eivectors2``, ``consolidatefile`` and
    !! ``slaterfile``) are copied to the ``cioverlap`` folder.
    character(len=*), intent(in) :: ciopath
    !! Path to ``cioverlap`` programs (``$CIOVERLAP``).
    integer, intent(in) :: ci_type
    !! Type of CI computation.
    integer, intent(in) :: ci_consolidate
    !! Consolidate the CI vector ?
    integer, intent(in) :: nelec
    !! Number of electrons

    logical :: ext
    integer :: u, status, ierr
    character(len=MAX_CMD_SIZE) :: colenv
    character(len=MAX_CMD_SIZE) :: cmd
    character(len=MAX_CMD_SIZE) :: mydir

    ! First, go into the WORK directory. All files are there, so it is more
    ! simple than copying everything.
    status = getcwd(mydir)
    status = chdir('WORK')

    ierr = 0
    ! Prepare list of slater determinants with cipc
    call get_environment_variable('COLUMBUS', colenv)
    ! write(msg, '(a30, a5)') 'Preparing list of Slater det', ' ... '
    ! res = message(msg, advance='no')
    call nx_log%log(LOG_DEBUG, 'Preparing list of Slater determinants')
    if (ci_type >= 1) then
       inquire(file=colenv//'/mycipc.x', exist=ext)
       if (ext) then
          cmd = 'mycipc.x < cipcin > mycipcls'
       else
          ! open(newunit=u, file='cipcin1', action='write')
          ! write(u, '(a)') "   3"
          ! write(u, '(a)') ''
          cmd = 'echo 3 | cipc.x > mycipcls'
       end if
       call execute_command_line(trim(cmd), exitstat=ierr)
       call check_error(ierr, CIO_COL_CIPC)

       inquire(file='eivectors', exist=ext)
       if (.not. ext) then
          call nx_log%log(LOG_ERROR, "Eivectors does not exist !! Check cipc program.")
          ERROR STOP
       end if

    else
       open(newunit=u, file='mcpcin', action='write')
       write(u, '(a)') "1"
       write(u, '(a)') "1"
       write(u, '(a)') "9"
       write(u, '(a)') ''
       cmd = 'mcpc.x < mcpcin > mcpcls.eivec'
       call execute_command_line(trim(cmd), exitstat=ierr)
       call check_error(ierr, CIO_COL_MCPC)
    end if
    call nx_log%log(LOG_DEBUG, 'Done preparing Slater determinants')

    call execute_command_line(&
         & 'mv eivectors eivectors.columbus', &
         & exitstat=ierr)
    call check_error(ierr, CIO_COL_MISC, &
         & msg="Couldn't rename 'eivectors' as 'eivectors.columbus'")
    call execute_command_line(&
         & 'cat eivectorshead eivectors.columbus > eivectors2', &
         & exitstat=ierr)
    call check_error(ierr, CIO_COL_MISC, &
         & msg="Couldn't create 'eivectors2'")

    ! Next consolidate the file if required
    if (ci_consolidate == 1) then
       call nx_log%log(LOG_DEBUG, 'Consolidating CI wavefunction')
       ! inquire(file='civecconsolidate.in', exist=ext)
       ! if (.not. ext) then
       !    open(newunit=u, file='civecconsolidate.in', action='write')
       !    write(u, '(I0, A)') nelec, ' 0'
       !    close(u)
       ! end if

       ! inquire(file='../cioverlap.old/consolidatefile', exist=ext)
       ! if (ext) then
       !    call execute_command_line(&
       !         & 'cp -f ../cioverlap.old/consolidatefile .', exitstat=ierr)
       !    call check_error(ierr, CIO_COL_MISC, &
       !         & msg="Couldn't copy 'cioverlap.old/consolidatefile'.")
       ! end if

       call execute_command_line('mv eivectors2 eivectors2.org', exitstat=ierr)
       call execute_command_line('mv slaterfile slaterfile.org', exitstat=ierr)

       write(cmd, '(A, I0, A)') 'echo ', nelec, ' 0'
       cmd = trim(cmd)//' | '//trim(ciopath)
       cmd = trim(cmd)//'civecconsolidate eivectors2.org slaterfile.org'
       cmd = trim(cmd)//' eivectors2 slaterfile consolidatefile'
       cmd = trim(cmd)//' > civecconsolidate.out'

       call execute_command_line(cmd, exitstat=ierr)
       call check_error(ierr, CIO_COL_CONSOLIDATE)
       if (ierr == 0) then
          call nx_log%log(LOG_DEBUG, 'Done with consolidating')
       else
          call nx_log%log(LOG_ERROR, 'COLUMBUS: Problem in consolidating wavefunction !')
          error stop
       end if

    end if
    status = chdir(mydir)

    ! In the first step cioverlap/ contains nothing (and does not
    ! exist yet), so let's test !
    inquire(file='./cioverlap/eivectors2', exist=ext)
    if (.not. ext) then
       call execute_command_line('cp WORK/eivectors2 cioverlap/', exitstat=ierr)
       call execute_command_line('cp WORK/slaterfile cioverlap/', exitstat&
            &=ierr)
       if (ci_consolidate == 1) then
          call execute_command_line('cp WORK/consolidatefile cioverlap/',&
               & exitstat=ierr)
       end if
    end if

  end subroutine col_prepare_cio
end module mod_columbus
