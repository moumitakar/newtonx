! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_integrators
  ! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  ! date: 2021-06-11

  !! # Integrators implementation for Newton-X
  !!
  !! The general problem handled here is the resolution of equations of the form
  !!
  !! \[ \boldsymbol{\dot{A}} = f (t, \boldsymbol{A}), \]
  !!
  !! where \(\boldsymbol{A}\) is a real or complex vector of
  !! arbitrary size.
  !!
  !! ## Usage
  !!
  !!      ! Definition of the quantity to integrate (e.g. electronic
  !!      ! coefficients)
  !!      type, public, extends(fgeneric_c) :: my_type
  !!       contains
  !!          procedure, pass :: init
  !!      end type my_type
  !!
  !!      ! To obtain t+dt (position 1), we need t (position 2) and
  !!      ! t-dt (position 3), so we define a 3-dimensional array.
  !!      ! This, of course, depends on the chosen method !
  !!      type(my_type) :: array_of_data(3)
  !!      type(arr_fgeneric_c) :: pointer_to_array(3)
  !!
  !!      ! Now we point to the array generated above.
  !!      do i=1, 3
  !!         array_of_data(i)%init
  !!         pointer_to_array(i) => array_of_data(i)
  !!      end do
  !!
  !!      ! And finally we can propagate
  !!      call finite_element(pointer_to_array, ...)
  !!
  !! ## Currently implemented
  !!
  !! - Second-order approximation ;
  !! - Finite element method ([Ferretti et al.](https://doi.org/10.1063/1.471791));
  !! - Second order Butcher integration (Butcher, *J. Ass. Comp. Mach.*, **12** (1965));
  !!
  !! The methods are available for real and complex functions, and
  !! are provided in two forms:
  !!
  !! - the actual routines for performing one integration step ;
  !! - an "integration scheme" version, where the step in the
  !! trajectory is taken as argument, and the method is chosen
  !! according to this type. For instance, in finite element method
  !! the data at \(t-dt\) and \(t\) are needed, which means it won't
  !! be possible in the first integration step. In this case, the
  !! routine will automatically select a second order approximation
  !! in the first step.
  !!
  !! ## Implementation details
  !!
  !! The functions to be integrated will be represented by the
  !! abstract types ``fgeneric_c`` and ``fgeneric_r`` for complex and
  !! real functions respectively.  To remain as general as possible,
  !! these abstract types will only contain the values and the
  !! corresponding derivatives (respectively ``acoef`` and ``adot``
  !! members) at a given time.  We also only require a procedure to
  !! compute the derivative, and a procedure to interpolate the
  !! parameters between two time-steps, that will be used in methods
  !! where we need to know the derivatives in-between time-steps (the
  !! method of Butcher for instance).
  !!
  !! As solving the problem usually involves knowledge of the first
  !! derivative of \(\boldsymbol{A}\) at different times, it is
  !! convenient to operate on arrays of vectors that contain the
  !! coefficients and the derivatives at different time steps.  All
  !! functions in this module will thus be designed to operate on a
  !! single array, with elements corresponding to the different times.
  !! The size of the array will of course depend on the chosen method.
  !!
  !! However it is not possible to declare arrays of abstract type in
  !! Fortran (2021).  Our solution is thus to operate on arrays of
  !! pointers to abstract types.  In practice, you can declare an
  !! array containing your derived types, and then declare an array
  !! of pointers to the elements of this array (see example above).
  !!
  use mod_kinds, only: dp
  implicit none

  private

  ! Function types
  public :: fgeneric_c, fgeneric_r
  public :: arr_fgeneric_c, arr_fgeneric_r

  ! Methods
  public :: second_order, finite_element, butcher, upropagator

  ! Integration schemes
  public :: finite_element_int, butcher_int, upropagator_int

  ! Fgeneric_c: complex function with complex derivative
  type, abstract :: fgeneric_c
     !! Function with complex values.
     !!
     complex(dp), allocatable :: acoef(:)
     !! Function coefficients.
     complex(dp), allocatable :: adot(:)
     !! Derivatives.
   contains
     procedure(derivative_c), deferred, pass :: get_deriv
     procedure(rebuild_c), deferred, pass :: rebuild
     procedure(print_c), deferred, pass :: print
  end type fgeneric_c

  abstract interface
     function derivative_c(func, x) result(dd)
       !! Compute the derivatives of the given function ``func``.
       !!
       !! Optionnaly, it is possible to explicitly set the point
       !! ``x`` at which the derivative is computed.  The function
       !! returns an arrays of size ``size(func%adot)``.
       use mod_kinds, only: dp
       import :: fgeneric_c
       class(fgeneric_c), intent(in) :: func
       real(dp), intent(in), optional :: x
       complex(dp), dimension(size(func%adot)) :: dd
     end function derivative_c
  end interface

  abstract interface
     subroutine rebuild_c(func, prev, step)
       !! Derive the parameters of ``func`` by linear interpolation.
       !!
       !! It is supposed that ``func`` contains the parameters at the
       !! step directly after ``prev``. The goal is to consctruct the
       !! parameters in-between these two, by splitting the original
       !! step in ``step`` steps.
       import :: fgeneric_c
       class(fgeneric_c), intent(inout) :: func
       class(fgeneric_c), intent(in) :: prev
       integer, intent(in) :: step
     end subroutine rebuild_c
  end interface

  abstract interface
     subroutine print_c(func)
       !! Print the function ``func``.
       import :: fgeneric_c
       class(fgeneric_c), intent(in) :: func
     end subroutine print_c
  end interface

  type, public :: arr_fgeneric_c
     !! Pointer to ``fgeneric_c``.
     class(fgeneric_c), pointer :: f
  end type arr_fgeneric_c


  ! Fgeneric_r: real function
  type, abstract :: fgeneric_r
     !! Function with real values.
     !!
     real(dp), allocatable :: acoef(:)
     !! Function coefficients.
     real(dp), allocatable :: adot(:)
     !! Derivatives.
   contains
     procedure(derivative_r), deferred, pass :: get_deriv
     procedure(rebuild_r), deferred, pass :: rebuild
     procedure(print_r), deferred, pass :: print
  end type fgeneric_r

  abstract interface
     function derivative_r(func, x) result(dd)
       !! Compute the derivatives of the given function ``func``.
       !!
       !! Optionnaly, it is possible to explicitly set the point
       !! ``x`` at which the derivative is computed.  The function
       !! returns an arrays of size ``size(func%adot)``.
       use mod_kinds, only: dp
       import :: fgeneric_r
       class(fgeneric_r), intent(in) :: func
       real(dp), intent(in), optional :: x
       real(dp), dimension(size(func%adot)) :: dd
     end function derivative_r
  end interface

  abstract interface
     subroutine rebuild_r(func, prev, step)
       !! Derive the parameters of ``func`` by linear interpolation.
       !!
       !! It is supposed that ``func`` contains the parameters at the
       !! step directly after ``prev``. The goal is to consctruct the
       !! parameters in-between these two, by splitting the original
       !! step in ``step`` steps.
       import :: fgeneric_r
       class(fgeneric_r), intent(inout) :: func
       class(fgeneric_r), intent(in) :: prev
       integer, intent(in) :: step
     end subroutine rebuild_r
  end interface

  abstract interface
     subroutine print_r(func)
       !! Print the function ``func``.
       import :: fgeneric_r
       class(fgeneric_r), intent(in) :: func
     end subroutine print_r
  end interface

  type, public :: arr_fgeneric_r
     !! Pointer to ``fgeneric_r``.
     class(fgeneric_r), pointer :: f
  end type arr_fgeneric_r

  ! Interfaces to algorithms
  interface second_order
     !! Perform a second order approximation.
     !!
     !! The function will compute \(\boldsymbol{A}(t+dt)\) and
     !! store it as ``funcs(1)``, with elements
     !!
     !! \[A_i(t+dt) = A_i(t) + \frac{dt}{2} \left( \dot{A}_i(t) + \dot{A}_i(t+dt) \right).\]
     !!
     !! If ``approx`` is ``.true.``, the derivatives at \(t + dt \) will be
     !! estimated by Euler method.  Else they will be taken as ``funcs%f(1)%adot``.
     module procedure second_order_c
     module procedure second_order_r
  end interface second_order

  interface finite_element
     !! Finite-element type approximation.
     !!
     !! The algorithm is inspired from Ferretti et al. (JCP, 1996),
     !! It will compute \(\boldsymbol{A}(t+dt)\) and store it as
     !! ``funcs(1)``, with elements
     !!
     !! \[ A_i(t+dt) = A_i(t) + dt* \left [ -\frac{\dot{A_i}(t-dt)}{2}
     !! + \frac{2\dot{A_i}(t)}{3} + \frac{5\dot{A_i}(t+dt)}{12} \right
     !! ]. \]
     !!
     !! If ``approx`` is ``.true.``, the derivatives at \(t+dt\) will
     !! be estimated as:
     !!
     !! \[ \dot{A_i}(t+dt) \approx A_i(t) + dt \left[ \frac{3 \dot{A_i}(t)}{2} -
     !! \frac{\dot{A_i}(t-dt)}{2} \right ]. \]
     module procedure finite_element_c
     module procedure finite_element_r
  end interface finite_element

  interface butcher
     !! Butcher-type integration.
     !!
     !! This integration takes part in three steps, and does not
     !! require knowledge of \(\dot{A}(t+dt)\). In the first step an
     !! estimate of the function at \(t + \frac{dt}{2}\) is carried
     !! out as:
     !!
     !! \[ \hat{A_i}\left(t + \frac{dt}{2}\right) = A_i(t-dt) + \frac{dt}{8} \left [ 9
     !! \dot{A_i}(t) + 3 \dot{A_i}(t-dt) \right ]. \]
     !!
     !! The derivative of this estimate is then used to compute \(\hat{A_i}(t) \) as:
     !!
     !! \[ \hat{A_i}(t+dt) = \frac{1}{5} \left [ 28 A_i(t) - 23
     !! A_i(t-dt) \right ] + \frac{dt}{15} \left [ 32
     !! \frac{d\hat{A_i}}{dt} - 60 \dot{A_i}(t) - 26
     !! \dot{A_i}(t-dt)\right ]. \]
     !!
     !! Finally, we use the derivative of this quantity to compute
     !! the value we want:
     !!
     !! \[ A_i(t+dt) = \frac{1}{31} \left [ 32 A_i(t) - A_i(t-dt)
     !! \right ] + \frac{dt}{32} \left [ 64 \frac{d\hat{A_i}}{dt} +
     !! 15 \frac{d\hat{A_i}}{dt} + 12 \dot{A_i}(t) - \dot{A_i}(t-dt)
     !! \right ].\]
     !!
     !! As the derivative of the function at a half time-step has to
     !! be computed, it is required here that a routine ``rebuild``
     !! is provided to estimate the parameters of the function at
     !! this time. This is usually done by interpolating the data
     !! between \(t\) and \(t+dtd\).
     module procedure butcher_c
     module procedure butcher_r
  end interface butcher

  interface upropagator
     !! EXPERIMENTAL: Unitary propagator
     !!
     module procedure upropagator_c
  end interface upropagator

  interface butcher_int
     !! Butcher integration scheme.
     !!
     !! If ``step`` is equal to ``1``, the routine will perform a
     !! second-order integration. Else, Butcher's scheme will be used.
     module procedure butcher_int_c
     module procedure butcher_int_r
  end interface butcher_int

  interface finite_element_int
     !! Finite element integration scheme.
     !!
     !! If ``step`` is equal to ``1``, the routine will perform a
     !! second-order integration. Else, finite element method will be
     !! used.
     module procedure finite_element_int_c
     module procedure finite_element_int_r
  end interface finite_element_int

  interface upropagator_int
     module procedure upropagator_int_c
  end interface upropagator_int



contains

  subroutine second_order_c(funcs, dt, x, approx)
    class(arr_fgeneric_c), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_c``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.
    logical, intent(in), optional :: approx
    !! If ``.true.``, approximate the derivative at ``t+dt``.
    !! (default: ``.false.``)

    class(fgeneric_c), allocatable :: temp

    complex(dp), allocatable :: deriv(:)
    logical :: do_approx

    allocate(deriv(size(funcs(1)%f%adot)))
    do_approx = .false.
    if (present(approx)) then
       if (approx) do_approx = .true.
    end if

    if (do_approx) then
       ! Obtain a first approximation to A(t+dt), and compute A'(t+dt)
       allocate(temp, source=funcs(1)%f)
       temp%acoef(:) = funcs(2)%f%acoef(:) + dt*funcs(2)%f%adot(:)
       deriv(:) = temp%get_deriv(x)
    else
       deriv(:) = funcs(1)%f%adot(:)
    end if

    ! Now use this approximation in the computation
    funcs(1)%f%acoef(:) = funcs(2)%f%acoef(:) + dt/2 * (funcs(2)%f%adot(:) + deriv(:))
  end subroutine second_order_c


  subroutine second_order_r(funcs, dt, x, approx) ! result (res)
    class(arr_fgeneric_r), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_r``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.
    logical, intent(in), optional :: approx
    !! If ``.true.``, approximate the derivative at ``t+dt``.
    !! (default: ``.false.``)

    class(fgeneric_r), allocatable :: temp

    real(dp), allocatable :: deriv(:)

    logical :: do_approx

    allocate(deriv(size(funcs(1)%f%adot)))
    do_approx = .false.
    if (present(approx)) then
       if (approx) do_approx = .true.
    end if

    if (do_approx) then
       ! Obtain a first approximation to A(t+dt), and compute A'(t+dt)
       allocate(temp, source=funcs(1)%f)
       temp%acoef(:) = funcs(2)%f%acoef(:) + dt*funcs(2)%f%adot(:)
       deriv(:) = temp%get_deriv(x)
    else
       deriv(:) = funcs(1)%f%adot(:)
    end if

    ! Now use this approximation in the computation
    funcs(1)%f%acoef(:) = funcs(2)%f%acoef(:) + dt/2 * (funcs(2)%f%adot(:) + deriv(:))

  end subroutine second_order_r


  subroutine finite_element_c(funcs, dt, x, approx) ! result (res)
    class(arr_fgeneric_c), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_c``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.
    logical, intent(in), optional :: approx
    !! If ``.true.``, approximate the derivative at ``t+dt``.
    !! (default: ``.false.``)

    class(fgeneric_c), allocatable :: temp
    real(dp) :: t1, t2, t3

    complex(dp), allocatable :: deriv(:)

    logical :: do_approx

    allocate(deriv(size(funcs(1)%f%adot)))
    do_approx = .false.
    if (present(approx)) then
       if (approx) do_approx = .true.
    end if

    if (do_approx) then
       ! Obtain a first approximation to A(t+dt), and compute A'(t+dt)
       allocate(temp, source=funcs(1)%f)
       t1 = dt*0.5_dp
       t2 = dt*0.5_dp*3.0_dp
       temp%acoef(:) = funcs(2)%f%acoef(:) + t2 * funcs(2)%f%adot(:) - t1 * funcs(3)%f%adot(:)
       deriv(:) = temp%get_deriv(x)
    else
       deriv(:) = funcs(1)%f%adot
    end if

    ! Now perform the finite element integration
    t1 = 2.0_dp / 3.0_dp
    t2 = 5.0_dp / 12.0_dp
    t3 = 1.0_dp / 12.0_dp
    funcs(1)%f%acoef(:) = funcs(2)%f%acoef(:) + dt * (&
         & t1 * funcs(2)%f%adot(:)&
         & + t2 * deriv(:) &
         & - t3 * funcs(3)%f%adot(:)&
         & )
  end subroutine finite_element_c


  subroutine finite_element_r(funcs, dt, x, approx) ! result (res)
    class(arr_fgeneric_r), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_r``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.
    logical, intent(in), optional :: approx
    !! If ``.true.``, approximate the derivative at ``t+dt``.
    !! (default: ``.false.``)

    class(fgeneric_r), allocatable :: temp
    real(dp) :: t1, t2, t3

    real(dp), allocatable :: deriv(:)

    logical :: do_approx

    allocate(deriv(size(funcs(1)%f%adot)))
    do_approx = .false.
    if (present(approx)) then
       if (approx) do_approx = .true.
    end if

    if (do_approx) then
       ! Obtain a first approximation to A(t+dt), and compute A'(t+dt)
       allocate(temp, source=funcs(1)%f)
       t1 = dt*0.5_dp
       t2 = dt*0.5_dp*3.0_dp
       temp%acoef(:) = funcs(2)%f%acoef(:) + t2 * funcs(2)%f%adot(:) - t1 * funcs(3)%f%adot(:)
       deriv(:) = temp%get_deriv(x)
    else
       deriv(:) = funcs(1)%f%adot
    end if

    ! Now perform the finite element integration
    t1 = 2.0_dp / 3.0_dp
    t2 = 5.0_dp / 12.0_dp
    t3 = 1.0_dp / 12.0_dp
    funcs(1)%f%acoef(:) = funcs(2)%f%acoef(:) + dt * (&
         & t1 * funcs(2)%f%adot(:)&
         & + t2 * deriv(:) &
         & - t3 * funcs(3)%f%adot(:)&
         & )
  end subroutine finite_element_r


  subroutine butcher_c(funcs, dt, x) ! result (res)
    class(arr_fgeneric_c), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_c``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.

    class(fgeneric_c), allocatable :: temp1
    class(fgeneric_c), allocatable :: temp2

    real(dp) :: t1, t2 ! Prefactors

    allocate(temp1, source=funcs(1)%f)
    allocate(temp2, source=funcs(1)%f)

    ! Build \hat{y_n-1/2} and its derivative in temp1
    temp1%acoef = funcs(3)%f%acoef + dt/8 * (9*funcs(2)%f%adot + 3*funcs(3)%f&
         &%adot)
    call temp1%rebuild(funcs(2)%f, 2)
    temp1%adot = temp1%get_deriv(x)

    ! Build \hat{y_n} and its derivative in temp2
    t1 = 1.0_dp/5
    t2 = 1.0_dp/15
    temp2%acoef = t1*( 28*funcs(2)%f%acoef - 23*funcs(3)%f%acoef ) &
         & + dt*t2*( 32*temp1%adot - 60*funcs(2)%f%adot - 26*funcs(3)%f%adot)
    temp2%adot = temp2%get_deriv(x)

    ! Finally build \tilde{y_n} in res
    t1 = 1.0_dp/31
    t2 = 1.0_dp/93
    funcs(1)%f%acoef = &
         & t1*( 32*funcs(2)%f%acoef - funcs(3)%f%acoef )&
         & + dt*t2*( 64*temp1%adot + 15*temp2%adot + 12*funcs(2)%f%adot - funcs(3)%f%adot)
  end subroutine butcher_c


  subroutine butcher_r(funcs, dt, x) ! result (res)
    class(arr_fgeneric_r), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_r``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.

    class(fgeneric_r), allocatable :: temp1
    class(fgeneric_r), allocatable :: temp2

    real(dp) :: t1, t2 ! Prefactors

    allocate(temp1, source=funcs(1)%f)
    allocate(temp2, source=funcs(1)%f)

    ! Build \hat{y_n-1/2} and its derivative in temp1
    temp1%acoef = funcs(3)%f%acoef + dt/8 * (9*funcs(2)%f%adot + 3*funcs(3)%f&
         &%adot)
    call temp1%rebuild(funcs(2)%f, 2)
    temp1%adot = temp1%get_deriv(x)

    ! Build \hat{y_n} and its derivative in temp2
    t1 = 1.0_dp/5
    t2 = 1.0_dp/15
    temp2%acoef = t1*( 28*funcs(2)%f%acoef - 23*funcs(3)%f%acoef ) &
         & + dt*t2*( 32*temp1%adot - 60*funcs(2)%f%adot - 26*funcs(3)%f%adot)
    temp2%adot = temp2%get_deriv(x)

    ! Finally build \tilde{y_n} in res
    t1 = 1.0_dp/31
    t2 = 1.0_dp/93
    funcs(1)%f%acoef = &
         & t1*( 32*funcs(2)%f%acoef - funcs(3)%f%acoef )&
         & + dt*t2*( 64*temp1%adot + 15*temp2%adot + 12*funcs(2)%f%adot - funcs(3)%f%adot)
  end subroutine butcher_r


  subroutine upropagator_c(funcs, hamiltonian, dt, x)
    class(arr_fgeneric_c), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_r``.
    complex(dp), intent(in) :: hamiltonian(:, :)
    !! Hamiltonian to propagate.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.

    complex(dp) :: fac
    integer :: nsurf, info, lw
    complex(dp), allocatable :: u(:, :), w(:), ww(:, :), ut(:, :)&
         &, bplex(:, :)
    real(dp), allocatable :: e(:), rw(:)

    print *, 'CALLING STD'
    nsurf = size(hamiltonian, 1)
    lw = nsurf**2
    fac = cmplx(0, 1.0_dp, kind=dp) * dt / 2

    allocate(u(nsurf, nsurf))
    allocate(ut(nsurf, nsurf))
    allocate(w(nsurf))
    allocate(ww(nsurf, nsurf))
    allocate(bplex(nsurf, nsurf))
    allocate(e(nsurf), rw(nsurf))

    u(:, :) = cmplx(0, 1.0_dp, kind=dp) * hamiltonian(:, :)
    call zheev('V','L',nsurf,u,nsurf,e,w,lw,rw,info)
    ! ut = transpose(conjg(u))
    ! w(:) = -e(:) * fac
    !
    ! do i=1, nsurf
    !    do j=1, nsurf
    !       ww(i, j) = u(i, j) * exp(w(j))
    !    end do
    ! end do
    !
    ! bplex = matmul(ww, ut)
    !
    ! funcs(1)%f%acoef = matmul(funcs(2)%f%acoef, bplex)
    ! deallocate(u, ut, w, ww, bplex, e, rw)
  end subroutine upropagator_c


  subroutine upropagator_2d_c(funcs, hamiltonian, dt, x)
    class(arr_fgeneric_c), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_r``.
    complex(dp), intent(in) :: hamiltonian(:, :)
    !! Hamiltonian to propagate.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.

    complex(dp) :: b21r, b21, b21x, b21y
    complex(dp), allocatable :: b(:, :)

    allocate(b(2, 2))

    b21r = abs(hamiltonian(2, 1))
    b21 = dt*hamiltonian(2, 1) / 2

    if (b21r == cmplx(0.0, 0.0, kind=dp)) then
       b21x = cmplx(0.0, 0.0, kind=dp)
       b21y = cmplx(0.0, 0.0, kind=dp)
    else
       b21x = b21 / b21r
       b21y = conjg(b21) / b21r
    end if

    b(1,1)=cos(b21r)
    b(2,2)=b(1,1)
    b(1,2)=-b21y*sin(b21r)
    b(2,1)=b21x*sin(b21r)

    funcs(1)%f%acoef = matmul(funcs(2)%f%acoef, b)
  end subroutine upropagator_2d_c



  subroutine butcher_int_c(funcs, dt, step, x)
    class(arr_fgeneric_c), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_c``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    integer, intent(in) :: step
    !! Current integration step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.

    if (step < 2) then
       call second_order_c(funcs, dt, x, approx=.true.)
    else
       call butcher_c(funcs, dt, x)
    end if
  end subroutine butcher_int_c


  subroutine butcher_int_r(funcs, dt, step, x)
    class(arr_fgeneric_r), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_c``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    integer, intent(in) :: step
    !! Current integration step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.

    if (step < 2) then
       call second_order_r(funcs, dt, x, approx=.true.)
    else
       call butcher_r(funcs, dt, x)
    end if
  end subroutine butcher_int_r


  subroutine finite_element_int_c(funcs, dt, step, x, approx)
    class(arr_fgeneric_c), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_c``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    integer, intent(in) :: step
    !! Current integration step.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.
    logical, intent(in), optional :: approx
    !! If ``.true.``, approximate the derivative at ``t+dt``.
    !! (default: ``.false.``)

    logical :: do_approx

    do_approx = .false.
    if (present(approx)) then
       do_approx = approx
    end if

    if (step < 2) then
       call second_order_c(funcs, dt, x, approx=approx)
    else
       call finite_element_c(funcs, dt, x, approx=approx)
    end if
  end subroutine finite_element_int_c

  subroutine finite_element_int_r(funcs, dt, step, x, approx)
    class(arr_fgeneric_r), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_c``.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    integer, intent(in) :: step
    !! Current time at which the integration is performed.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.
    logical, intent(in), optional :: approx
    !! If ``.true.``, approximate the derivative at ``t+dt``.
    !! (default: ``.false.``)

    logical :: do_approx

    do_approx = .false.
    if (present(approx)) then
       do_approx = approx
    end if

    if (step < 2) then
       call second_order_r(funcs, dt, x, approx=approx)
    else
       call finite_element_r(funcs, dt, x, approx=approx)
    end if
  end subroutine finite_element_int_r

  subroutine upropagator_int_c(funcs, hamiltonian, dt, step, x)
    class(arr_fgeneric_c), intent(inout) :: funcs(:)
    !! Array of pointers to ``fgeneric_c``.
    complex(dp), intent(in) :: hamiltonian(:, :)
    !! Hamiltonian.
    real(dp), intent(in) :: dt
    !! Integration time-step.
    integer, intent(in) :: step
    !! Current time at which the integration is performed.
    real(dp), intent(in), optional :: x
    !! Current time at which the integration is performed.

    if (step < 2) then
       call second_order_c(funcs, dt, x, approx=.true.)
    else
       if (size(hamiltonian, 1) == 2) then
          call upropagator_2d_c(funcs, hamiltonian, dt, x)
       else
          call upropagator_c(funcs, hamiltonian, dt, x)
       end if
    end if

  end subroutine upropagator_int_c




end module mod_integrators
