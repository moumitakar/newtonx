! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_external
  !! authors: Max Pinheiro Jr <max.pinheiro-jr@univ-amu.fr>
  !!          Eduarda Sangiogo <dudasangiogogil@hotmail.com>
  !! date: 2021-12-13
  !!
  !! This module contains routines used to communicate Newton-x with
  !! an external script. The goal is to give more flexibility to perform
  !! dynamics simulations with any QM program or a Machine Learning model.
  !! To this end, the path to an external script should be given in the
  !! configuration.inp file, and this script must provide the information
  !! required by Newton-X (energies, gradients, and non-adiabatic couplings)
  !! at every time step to run the dynamics.
  !!
  !! The script has to work with the following call:
  !!
  !!     $ script_name nat nstat nstatdyn
  !!
  !! and should write the files ``epot``, ``grad`` and ``nad_vectors``.
  !!
  !! These files should be organized, respectively, as:
  !!
  !! - ``epot``:
  !!
  !!         epot( state 1 )
  !!         epot( state 2 )
  !!         ...
  !!
  !! - ``grad``:
  !!
  !!         grad_x( atom 1 )   grad_y( atom 1 )  grad_z( atom 1 )
  !!         grad_x( atom 2 )   grad_y( atom 2 )  grad_z( atom 2 )
  !!         ...
  !!
  !! - ``nad_vectors``:
  !!
  !!         nad_x^{2 / 1} (atom 1)  nad_z^{2 / 1} (atom 1)  nad_z^{2 / 1} (atom 1)
  !!         nad_x^{2 / 1} (atom 2)  nad_z^{2 / 1} (atom 2)  nad_z^{2 / 1} (atom 2)
  !!         ...
  !!         nad_x^{3 / 2} (atom 2)  nad_z^{3 / 2} (atom 2)  nad_z^{3 / 2} (atom 2)
  !!         ...
  !!         nad_x^{3 / 1} (atom 2)  nad_z^{3 / 1} (atom 2)  nad_z^{3 / 1} (atom 2)
  !!         ...
  !!
  !!
  use mod_kinds, only: dp
  use mod_constants, only: MAX_CMD_SIZE, MAX_STR_SIZE
  use mod_logger, only: &
       & call_external, check_error
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_trajectory, only: nx_traj_t
  use mod_configuration, only: nx_config_t
  use mod_tools, only: to_str
  use mod_interface, only: &
       & get_list_of_files, copy
  use mod_input_parser, only: &
       & parser_t, set => set_config
  implicit none

  private

  public :: external_setup
  public :: external_run
  public :: external_read_outputs

  integer, parameter :: EXT_ERR_MAIN = 313
  integer, parameter :: EXT_ERR_PERL = 323

contains

  subroutine external_setup(nx_qm, parser)
    type(nx_qm_t), intent(inout) :: nx_qm
    type(parser_t), intent(in) :: parser

    call set(parser, 'external', nx_qm%ext_path, 'script_name')
  end subroutine external_setup

  subroutine external_run(nx_qm, conf, traj)
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    type(nx_config_t) :: conf
    !! General configuration.
    type(nx_traj_t) :: traj

    character(len=MAX_CMD_SIZE) :: cmd
    ! character(len=MAX_CMD_SIZE) :: env
    ! character(len=MAX_CMD_SIZE) :: namei
    ! character(len=MAX_CMD_SIZE) :: namej
    ! character(len=MAX_CMD_SIZE) :: namek
    character(len=256), allocatable :: filelist(:)

    integer :: ierr


    if (traj%step .eq. 0) then
       ierr = 0
       ! call get_environment_variable(name='NXHOME', value=env)
       ! cmd = trim(env)//'/utils/external.pl'
       ! call call_external(cmd, ierr)
       call get_list_of_files(nx_qm%job_folder, filelist, ierr, full_path=.true.)
       call check_error(ierr, EXT_ERR_PERL, 'EXT: Cannot read directory '//trim(nx_qm%job_folder))
       ierr = copy(filelist, './')
       call check_error(ierr, EXT_ERR_PERL, 'EXT: Cannot copy from directory '//trim(nx_qm%job_folder))
    end if

    ! write(namei, "(I10)") conf%nat
    ! write(namej, "(I10)") conf%nstat
    ! write(namek, "(I10)") traj%nstatdyn

    !cmd = trim(adjustl(nx_qm%script_name))//" "//trim(adjustl(namei))&
    !    &//" "//trim(adjustl(namej))//" "//trim(adjustl(namek))
    cmd = trim(nx_qm%ext_path)//' '//to_str(conf%nat)//' '&
         & //to_str(conf%nstat)//' '//to_str(traj%nstatdyn)

    ierr = 0

    call call_external(cmd, ierr)
    call check_error(ierr, EXT_ERR_MAIN)

  end subroutine external_run

  subroutine external_read_outputs(nx_qm, traj, conf, qminfo)
    type(nx_qm_t), intent(inout) :: nx_qm
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf
    type(nx_qminfo_t), intent(inout) :: qminfo
    integer :: ncoup
    integer :: u, i, j

    real(dp) :: gx,gy,gz

    open(newunit=u, file='epot', action='read', status='old')
       do i = 1, conf%nstat
          read(u,*) qminfo%repot(i)
       end do
    close(u)

    open(newunit=u, file='grad', action='read', status='old')
       do i = 1, conf%nat
          read(u,*) gx,gy,gz
          qminfo%rgrad(traj%nstatdyn,1,i) = gx
          qminfo%rgrad(traj%nstatdyn,2,i) = gy
          qminfo%rgrad(traj%nstatdyn,3,i) = gz
       end do
    close(u)

    ! NAD
    if (nx_qm%read_nac == 1) then
        ncoup = conf%nstat * (conf%nstat - 1)/2
        open(newunit=u, file='nad_vectors', action='read', status='old')
        do i = 1, ncoup
            do j = 1, conf%nat
                read(u,*) gx,gy,gz
                qminfo%rnad(i,1,j) = gx
                qminfo%rnad(i,2,j) = gy
                qminfo%rnad(i,3,j) = gz
            end do
        end do
        close(u)
    end if

  end subroutine external_read_outputs

end module mod_external
