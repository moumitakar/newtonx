module mod_recoherence_model
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Model for studying recoherence
  !!
  !!  Model 1: Flat Adiabats with one avoided crossing
  !!  Model 2: Flat Adiabats with two avoided crossings
  !!
  !! Reference: Subotnik et al. (J. Phys. Chem. A, 123, 5428 (2019).
  use mod_constants, only: &
       & pi
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  type, public :: nx_recohmodel_t
     integer :: mnum = 1
     real(dp) :: a = 0.01_dp
     real(dp) :: b = 3.0_dp
     real(dp) :: c = 3.0_dp
   contains
     procedure :: setup => recohmodel_setup
     procedure :: print => recohmodel_print
     procedure :: run => recohmodel_run
     procedure, private :: get_theta => recohmodel_theta
     procedure, private :: get_results => recohmodel_quantities
  end type nx_recohmodel_t

contains

  subroutine recohmodel_setup(self, parser)
    class(nx_recohmodel_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser

    call set(parser, 'recohmodel', self%mnum, 'modelnum')
    call set(parser, 'recohmodel', self%a, 'A')
    call set(parser, 'recohmodel', self%b, 'B')
    call set(parser, 'recohmodel', self%c, 'C')
  end subroutine recohmodel_setup


  subroutine recohmodel_print(self, out)
    class(nx_recohmodel_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out

    write(output, *) 'Recoherence Model: Subotnik et al. (J. Phys. Chem. A, 123, 5428 (2019).'
    call print_conf_ele(self%mnum, 'model no. = ', unit=output)
    if (self%mnum == 1) then
       write(output, *) '   Two flat adiabats with one avoided crossing.'
    else if (self%mnum == 2) then
       write(output, *) '   Two flat adiabats with two  avoided crossing.'
    end if
    call print_conf_ele(self%a, 'A', unit=output)
    call print_conf_ele(self%b, 'B', unit=output)
    call print_conf_ele(self%c, 'C', unit=output)
  end subroutine recohmodel_print


  subroutine recohmodel_run(self, r, epot, grad, nad)
    !! Run the recohrence model.
    !!
    !! - compute "theta" and derivative of theta "dtheta"
    !!   according to either the first or second model.
    !! - calculate the energies, gradients and nonadiabatic coupling
    class(nx_recohmodel_t), intent(in) :: self
    real(dp), intent(in) :: r
    real(dp), intent(out) :: epot(:)
    real(dp), intent(out) :: grad(:)
    real(dp), intent(out) :: nad

    real(dp) :: theta, dtheta

    theta = 0.0_dp
    dtheta = 0.0_dp

    call self%get_theta(r, theta, dtheta)
    call self%get_results(theta, dtheta, epot, grad, nad)
  end subroutine recohmodel_run


  subroutine recohmodel_theta(self, r, theta, dtheta)
    class(nx_recohmodel_t), intent(in) :: self
    real(dp), intent(in) :: r
    real(dp), intent(out) :: theta
    real(dp), intent(out) :: dtheta

    real(dp) :: erfunc, erfunc1, erfunc2

    select case(self%mnum)
    case(1)
       erfunc = erf(self%b * r)
       theta  = (pi/2.0_dp) * (erfunc + 1.0_dp)
       dtheta = self%b * sqrt(pi) &
            &        * exp(-(self%b * r)**2)
    case(2)
       erfunc1 = erf(self%b * (r - self%c))
       erfunc2 = erf(self%b * (r + self%c))
       theta = (pi/2.0_dp) * (erfunc1 - erfunc2)
       dtheta = self%b * sqrt(pi) &
            & * (   exp(-(self%b*(r - self%c))**2) &
            &     - exp(-(self%b*(r + self%c))**2) )
    end select
  end subroutine recohmodel_theta


  subroutine recohmodel_quantities(self, theta, dtheta, epot, grad, nad)
    class(nx_recohmodel_t), intent(in) :: self
    real(dp), intent(in) :: theta
    real(dp), intent(in) :: dtheta
    real(dp), intent(out) :: epot(:)
    real(dp), intent(out) :: grad(:)
    real(dp), intent(out) :: nad

    real(dp) :: tmp, tmp1,tmp2,tmp3,tmp4, tm1,tm2,tm3
    real(dp), dimension(2,2) :: H, dH

    !! Diabatic Hamiltonian
    H(1,1) =-self%a * cos(theta)
    H(2,2) = self%a * cos(theta)
    H(1,2) = self%a * sin(theta)
    H(2,1) = H(1,2)

    !! Derivative of the diabatic Hamiltonian
    dH(1,1) = self%a * sin(theta) * dtheta
    dH(2,2) =-self%a * sin(theta) * dtheta
    dH(1,2) = self%a * cos(theta) * dtheta
    dH(2,1) = dH(1,2)

    !! Adiabatic energies
    tmp = (H(2,2)-H(1,1))**2 + 4.0_dp*H(1,2)*H(1,2)
    epot(1) = ( (H(1,1)+H(2,2)) - sqrt(tmp) ) * 0.5_dp
    epot(2) = ( (H(1,1)+H(2,2)) + sqrt(tmp) ) * 0.5_dp

    !! Gradient
    tmp1 = (dH(1,1) + dH(2,2)) * 0.5_dp
    tmp2 = 0.5_dp * (H(2,2)-H(1,1)) * (dH(2,2)-dH(1,1))
    tmp3 = 2.0_dp * H(1,2) * dH(1,2)
    tmp4 = 0.25_dp * (H(2,2)-H(1,1))**2 + H(1,2)*H(1,2)
    grad(1) = tmp1 - (tmp2 + tmp3) * (1.0_dp/sqrt(tmp4))
    grad(2) = tmp1 + (tmp2 + tmp3) * (1.0_dp/sqrt(tmp4))

    !! Alternatively, we could also set the adiabatic energies and gradients
    !! according to the model construction:
    ! nx_qm%recoh_en(1) = -nx_qm%recoh_a
    ! nx_qm%recoh_en(2) =  nx_qm%recoh_a
    ! nx_qm%recoh_grad(1) = 0.0_dp
    ! nx_qm%recoh_grad(2) = 0.0_dp

    !! Nonadiabatic coupling (F21 is needed, hence the minus sign)
    tm1 = 1.0_dp + (2.0_dp * H(1,2) / (H(2,2)-H(1,1)) )**2
    tm2 = 1.0D0/(H(2,2)-H(1,1)) * dH(1,2)
    tm3 = H(1,2)/(H(2,2)-H(1,1))**2 * (dH(2,2)-dH(1,1))
    nad = -1.0_dp/tm1 * (tm2 - tm3)

  end subroutine recohmodel_quantities
  

end module mod_recoherence_model
