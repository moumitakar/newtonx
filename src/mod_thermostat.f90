! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_thermostat
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 07/02/2020
  !!
  !! Module description
  use mod_kinds, only: dp
  use mod_tools, only: gauss_rand
  use mod_constants, only: BK
  use mod_trajectory, only: nx_traj_t
  use mod_input_parser, only: &
       & parser_t, set => set_config
  implicit none

  private

  public :: nx_thermo_t
  public :: th_run

  type nx_thermo_t
     !! Object holding information about thermostat.

     integer :: ktherm = 0
     !! Thermostat type:
     !!
     !! - 1: Andersen
     !! - 2: Andersen-Lowe
     integer :: kts = 1
     !! Step at which the thermostat is switched on.
     integer :: lts = -1
     !! Step at which the thermostat is switched off.
     integer :: nstherm = 1
     !! 1: Apply thermostat in the excited state.
     real(dp) :: temp = 300.0_dp
     !! Temperature.
     real(dp) :: gamma = 0.2_dp
     !! Collision frequency
     integer :: seed = 1
     !! Random seed.
     integer :: lvp = 1
     !! Print level

   contains
     procedure :: construct
     procedure :: init
     procedure :: print
  end type nx_thermo_t
  

contains

  subroutine construct(this)
    class(nx_thermo_t), intent(inout) :: this

    this%ktherm = 0
    this%kts = 0
    this%lts = 0
    this%nstherm = 0
    this%temp = 0.0_dp
    this%gamma = 0.0_dp
    this%seed = 0
    this%lvp = 0
  end subroutine construct


  subroutine init(this, parser)
    class(nx_thermo_t), intent(inout) :: this
    type(parser_t), intent(in) :: parser

    ! integer :: ktherm
    ! integer :: kts
    ! integer :: lts
    ! integer :: nstherm
    ! real(dp) :: temp
    ! real(dp) :: gamma
    ! integer :: seed
    ! integer :: lvp
    ! 
    ! namelist /thermostat/ ktherm, kts, lts, nstherm, temp, gamma,&
    !      & seed, lvp
    ! 
    ! integer :: u
    ! 
    ! open(newunit=u, file=filename, action='read')
    ! read(u, nml=thermostat)
    ! close(u)
    ! 
    ! call this%construct()
    ! 
    call set(parser, 'thermostat', this%ktherm, 'ktherm')
    call set(parser, 'thermostat', this%kts, 'kts')
    call set(parser, 'thermostat', this%lts, 'lts')
    call set(parser, 'thermostat', this%nstherm, 'nstherm')
    call set(parser, 'thermostat', this%temp, 'temp')
    call set(parser, 'thermostat', this%gamma, 'gamma')
    call set(parser, 'thermostat', this%seed, 'seed')
    call set(parser, 'thermostat', this%lvp, 'lvp')
  end subroutine init

  
  subroutine print(this)
    class(nx_thermo_t), intent(in) :: this

    write(*, *) ' '
    write(*, '(A80)') repeat('*', 80)
    write(*, *) 'Thermostat configuration:'
    write(*, *) ''
    write(*, '(A20, I0)') 'ktherm = ', this%ktherm
    write(*, '(A20, I0)') 'kts = ', this%kts
    write(*, '(A20, I0)') 'lts = ', this%lts
    write(*, '(A20, I0)') 'nstherm = ', this%nstherm
    write(*, '(A20, F6.2)') 'temp = ', this%temp
    write(*, '(A20, F6.2)') 'gamma = ', this%gamma
    write(*, '(A20, I0)') 'seed = ', this%seed
    write(*, '(A20, I0)') 'lvp = ', this%lvp

    write(*, '(A80)') repeat('*', 80)
    write(*, *) ' '
  end subroutine print


  subroutine th_run(thermo, traj, freeze_me)
    ! Andersen thermostat
    type(nx_thermo_t), intent(in) :: thermo
    type(nx_traj_t), intent(inout) :: traj
    logical, dimension(:) :: freeze_me

    select case(thermo%ktherm)
    case(1)
       write(*, *) '  Running Andersen thermostat'
       call andersen(thermo, traj, freeze_me)
       write(*, *) '  Andersen thermostat done.'
    end select
    
  end subroutine th_run
  


  subroutine andersen(thermo, traj, freeze_me)
    ! Andersen thermostat
    type(nx_thermo_t), intent(in) :: thermo
    type(nx_traj_t), intent(inout) :: traj

    logical, dimension(:) :: freeze_me
    integer :: i, n

    real(dp) :: p, r, v_modulus, rg
    
    ! Probability P
    p = thermo%gamma * traj%dt

    do n = 1, size(traj%veloc, 2)
       if(freeze_me(n))then     !
          r = 1   ! Keep Newtonian velocity for this atom in each case!
          if (thermo%lvp >= 3) write(*, *) "  Atom ",n," is frozen!"
       else
          call random_number(r)  ! Random number uniform-distributed between 0 and 1
       end if
       !
       if     (p <  r) then
          if (thermo%lvp >= 3) write(*, *) "  Keep Newtonian velocity for atom:  ",n
       elseif (p >= r) then
          if (thermo%lvp >= 3) write(*, *) "  Random velocity generated for atom:",n
          v_modulus = dsqrt(BK * thermo%temp / traj%masses(n))
          do i = 1,3
             call gauss_rand(rg)
             traj%veloc(i, n) = v_modulus*rg
          enddo
       endif

    enddo

  end subroutine andersen
  
  
end module mod_thermostat
