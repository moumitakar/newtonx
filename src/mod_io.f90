! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_io
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2021-04-26
  !!
  !! This module defines how Newton-X deals with H5MD file format, by
  !! specifying how data should be accessed and written.
#ifdef USE_HDF5
  use hdf5, only: &
       & hid_t, hsize_t, &
       & H5T_NATIVE_DOUBLE, H5T_NATIVE_INTEGER, &
       & h5gcreate_f, h5lexists_f, h5eset_auto_f, &
       & h5gopen_f, h5gclose_f
  use mod_h5md, only: &
       & nx_h5md_ele_t, nx_h5md_t, &
       & h5md_add_fixed, h5md_read_fixed, h5md_create_box
#endif
  use mod_kinds, only: dp
  use mod_constants, only: &
       & au2ang, proton, ATOM_NAMES
  use mod_configuration, only: nx_config_t
  use mod_trajectory, only: nx_traj_t
  use mod_tools, only: norm, str_ends_with, str_starts_with, to_str
  use mod_print_utils, only: to_upper
  use mod_input_parser, only: &
       & parser_t, set => set_config, set_realloc => set_config_realloc
  use mod_version, only: &
       & nx_version
  implicit none

  private

  type :: nx_txt_out
     !! Object holding information about txt outputs,
     !! such as file names for the different files produced.
     character(len=:), allocatable :: geomf
     !! Geometries (XYZ format).
     character(len=:), allocatable :: velof
     !! Velocities.
     character(len=:), allocatable :: nadvf
     !! Non-adiabatic vectors.
     character(len=:), allocatable :: wavef
     !! Wavefunction.
     character(len=:), allocatable :: enerf
     !! Energies (total, kinetic, potential) and current state.
     character(len=:), allocatable :: popuf
     !! Electronic populations.
     character(len=:), allocatable :: seedf
     !! Random seed.
     character(len=:), allocatable :: shprf
     !! Hopping probabilities (optionnally, random number generated).
     character(len=:), allocatable :: mhopf
     !! Velocities before and after hopping (interpolated).
     character(len=:), allocatable :: hoppf
     !! Velocities before and after hopping (in regular time-step).
     character(len=:), allocatable :: gradf
     !! Gradients.
     character(len=:), allocatable :: oscsf
     !! Oscillator strengths.
     character(len=:), allocatable :: lmomf
     !! Linear momentum.
     character(len=:), allocatable :: amomf
     !! Angular momentum
     character(len=:), allocatable :: statusf
     !! Angular momentum
     character(len=:), allocatable :: diahamf
     !! Excitonic (diabatic) hamiltonian
     character(len=:), allocatable :: diapopf
     !! Diabatic populations and energies
     character(len=:), allocatable :: mopdyn
     character(len=:), allocatable :: gammaf
     !! CS-FSSH: Resonance energies.
     character(len=:), allocatable :: imnadf
     !! CS-FSSH: Imaginary part of NAD.
   contains
     procedure :: destroy => txtout_destroy
  end type nx_txt_out
  interface nx_txt_out
     module procedure construct_txtout
  end interface nx_txt_out

#ifdef USE_HDF5
  type nx_h5_out
     ! Particles type elements
     integer(hid_t) :: part_id
     type(nx_h5md_ele_t) :: geom
     type(nx_h5md_ele_t) :: veloc
     type(nx_h5md_ele_t) :: grad
     type(nx_h5md_ele_t) :: nad
     type(nx_h5md_ele_t) :: inad

     ! Observables type elements
     integer(hid_t) :: obs_id
     type(nx_h5md_ele_t) :: epot
     type(nx_h5md_ele_t) :: nstatdyn
     type(nx_h5md_ele_t) :: etot
     type(nx_h5md_ele_t) :: ekin
     type(nx_h5md_ele_t) :: pop
     type(nx_h5md_ele_t) :: wf
     type(nx_h5md_ele_t) :: osc
     type(nx_h5md_ele_t) :: shprob
     type(nx_h5md_ele_t) :: shrx
     type(nx_h5md_ele_t) :: seed
     type(nx_h5md_ele_t) :: angmom
     type(nx_h5md_ele_t) :: linmom
     type(nx_h5md_ele_t) :: diaham
     type(nx_h5md_ele_t) :: diaen
     type(nx_h5md_ele_t) :: diapop
     type(nx_h5md_ele_t) :: gamma
  end type nx_h5_out
#endif

  type, public :: nx_output_t
     character(len=:), allocatable :: base_path
     type(nx_txt_out) :: txtout
     logical :: use_txt_outputs = .true.
     logical :: use_h5md_outputs = .false.
     logical :: with_exciton = .false.
     logical :: print_diabpop = .false.
     logical :: run_complex = .false.
     logical :: with_imag_nad = .false.
     character(len=:), allocatable :: creator
     character(len=:), allocatable :: creator_version
     integer :: lvprt = 3
     integer :: kt = 1

     ! Switches indicating which data to print

     ! 1. ALWAYS PRINT THESE DATA
     logical :: prt_geom = .true.
     logical :: prt_veloc = .true.
     logical :: prt_grad = .true.
     logical :: prt_epot = .true.
     logical :: prt_ekin = .true.
     logical :: prt_etot = .true.

     ! 2. Data when ADIABATIC AND NSTAT > 1
     logical :: prt_osc = .false.

     ! 3. Data with NON-ADIABATIC
     logical :: prt_nstatdyn = .false.
     logical :: prt_nad = .false.
     logical :: prt_wf = .false.
     logical :: prt_shprob = .false.
     logical :: prt_shrx = .false.
     logical :: prt_shseed = .false.

     ! 4. CS-FSSH
     logical :: prt_inad = .false.
     logical :: prt_gamma = .false.

     ! 5. Method-specific
     logical :: prt_diab_ham = .false.
     logical :: prt_diab_en = .false.
     logical :: prt_diab_pop = .false.
     logical :: prt_mopdyn = .false.

     ! 6. Other
     logical :: prt_angmom = .false.
     logical :: prt_linmom = .false.

#ifdef USE_HDF5
     character(len=:), allocatable :: user_name
     character(len=:), allocatable :: user_email
     character(len=:), allocatable :: h5file
     type(nx_h5md_t) :: h5
     type(nx_h5_out) :: h5dat
#endif
   contains
     procedure :: destroy => output_destroy
     procedure :: reset_path => output_reset_path
     procedure :: set_txtout_path => output_set_path_txtfiles
     procedure :: init_txt => output_init_txt_files
     procedure :: write_txt => output_write_txt_files
     procedure :: write_hop_veloc => output_write_hopping_veloc
     procedure :: write_restart => output_write_restart_files
     procedure :: set_options => output_set_options

#ifdef USE_HDF5
     procedure :: init_h5 => output_h5_init_output
     procedure :: open_h5 => output_h5_open_output
     procedure :: close_h5 => output_h5_close_output
     procedure :: create_particles => output_h5_create_particles
     procedure :: create_obs => output_h5_create_obs
     procedure :: write_h5 => output_h5_write_traj
     procedure :: write_h5_restart => output_h5_write_restart
#endif
  end type nx_output_t
  interface nx_output_t
     module procedure construct_nx_output
  end interface nx_output_t

contains

  function construct_nx_output(config) result(res)
    type(nx_config_t), intent(in) :: config

    type(nx_output_t) :: res

    if (.not. str_ends_with(config%output_path, '/')) then
       allocate( character(len=len_trim(config%output_path)+1) :: res%base_path)
       res%base_path = trim(config%output_path)//'/'
    else
       res%base_path = trim(config%output_path)
    end if

    res%creator = 'NEWTONX'
    res%creator_version = nx_version()

    res%lvprt = config%lvprt
    res%kt = config%kt

    res%with_exciton = (&
         & (config%progname == 'exc_mopac' .or. config%progname == 'exc_gaussian')&
         & )
    res%print_diabpop = (config%compute_diabpop)
    
#ifdef USE_HDF5
    res%use_h5md_outputs = .true.
    res%use_txt_outputs = config%use_txt_outputs

    res%user_name = ''
    res%user_email = ''
    res%h5file = 'dyn.h5'
#else
    res%use_h5md_outputs = .false.
    res%use_txt_outputs = .true.
#endif

    res%txtout = construct_txtout(res%base_path)

    ! Set the defaults flags for output production.
    ! The routine `output_set_options` should be called after, to set
    ! user-specific options.
    if (config%nstat > 1) then
       res%prt_osc = .true.

       if (config%thres > 0) then
          res%prt_nstatdyn = .true.
          res%prt_nad = .true.
          res%prt_wf = .true.
          res%prt_shprob = .true.
          res%prt_shrx = .true.
          res%prt_shseed = .true.
       end if
    end if

    if (config%run_complex) then
       res%prt_gamma = .true.
       if (res%prt_nad .and. config%gamma_model == 0) res%prt_inad = .true.
    end if

    if (config%progname == 'exc_mopac' .or. config%progname == 'exc_gaussian') then
       res%prt_diab_en = .true.
       res%prt_diab_ham = .true.
       res%prt_diab_pop = .true.
       res%prt_mopdyn = .true.
    end if

    if (config%compute_diabpop) res%prt_diab_pop = .true.

    !! CS-FSSH
    res%run_complex = config%run_complex
    res%with_imag_nad = (config%run_complex .and. config%gamma_model == 0)
    call res%set_txtout_path()
  end function construct_nx_output
  

  function construct_txtout(base_path) result(res)
    character(len=*), intent(in) :: base_path

    type(nx_txt_out) :: res

    res%geomf =   trim(base_path)//'/geometries.xyz'
    res%velof =   trim(base_path)//'/velocities.dat'
    res%nadvf =   trim(base_path)//'/nad.dat'
    res%wavef =   trim(base_path)//'/wf.dat'
    res%enerf =   trim(base_path)//'/energies.dat'
    res%popuf =   trim(base_path)//'/populations.dat'
    res%seedf =   trim(base_path)//'/seed.dat'
    res%shprf =   trim(base_path)//'/sh_prob.dat'
    res%mhopf =   trim(base_path)//'/hopping_veloc_micro.dat'
    res%hoppf =   trim(base_path)//'/hopping_veloc.dat'
    res%gradf =   trim(base_path)//'/gradients.dat'
    res%oscsf =   trim(base_path)//'/osc_str.dat'
    res%lmomf =   trim(base_path)//'/linmom.dat'
    res%amomf =   trim(base_path)//'/angmom.dat'
    res%statusf = trim(base_path)//'/cur_step_and_time'
    res%diahamf = trim(base_path)//'/diaham.dat'
    res%diapopf = trim(base_path)//'/diapop.dat'
    res%mopdyn =  trim(base_path)//'/exc_mop.dyn'
    res%gammaf =  trim(base_path)//'/gamma.dat'
    res%imnadf =  trim(base_path)//'/nad_imag.dat'
  end function construct_txtout


  subroutine txtout_destroy(self)
    class(nx_txt_out), intent(inout) :: self
    deallocate(self%geomf  )
    deallocate(self%velof  )
    deallocate(self%nadvf  )
    deallocate(self%wavef  )
    deallocate(self%enerf  )
    deallocate(self%popuf  )
    deallocate(self%seedf  )
    deallocate(self%shprf  )
    deallocate(self%mhopf  )
    deallocate(self%hoppf  )
    deallocate(self%gradf  )
    deallocate(self%oscsf  )
    deallocate(self%lmomf  )
    deallocate(self%amomf  )
    deallocate(self%statusf)
    deallocate(self%diahamf)
    deallocate(self%diapopf)
    deallocate(self%mopdyn)
    deallocate(self%imnadf)
    deallocate(self%gammaf)
  end subroutine txtout_destroy


  subroutine output_set_path_txtfiles(self)
    class(nx_output_t), intent(inout) :: self

    self%txtout%geomf =   self%base_path//'geometries.xyz'
    self%txtout%velof =   self%base_path//'velocities.dat'
    self%txtout%nadvf =   self%base_path//'nad.dat'
    self%txtout%wavef =   self%base_path//'wf.dat'
    self%txtout%enerf =   self%base_path//'energies.dat'
    self%txtout%popuf =   self%base_path//'populations.dat'
    self%txtout%seedf =   self%base_path//'seed.dat'
    self%txtout%shprf =   self%base_path//'sh_prob.dat'
    self%txtout%mhopf =   self%base_path//'hopping_veloc_micro.dat'
    self%txtout%hoppf =   self%base_path//'hopping_veloc.dat'
    self%txtout%gradf =   self%base_path//'gradients.dat'
    self%txtout%oscsf =   self%base_path//'osc_str.dat'
    self%txtout%lmomf =   self%base_path//'linmom.dat'
    self%txtout%amomf =   self%base_path//'angmom.dat'
    self%txtout%statusf = self%base_path//'cur_step_and_time'
    self%txtout%diahamf = self%base_path//'diaham.dat'
    self%txtout%diapopf = self%base_path//'diapop.dat'
    self%txtout%mopdyn  = self%base_path//'exc_mop.dyn'
    self%txtout%gammaf  = self%base_path//'gamma.dat'
    self%txtout%imnadf  = self%base_path//'nad_imag.dat'
  end subroutine output_set_path_txtfiles


  subroutine output_set_options(self, parser)
    class(nx_output_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser

    call set(parser, 'nx_output', self%prt_geom, 'prt_geom')
    call set(parser, 'nx_output', self%prt_veloc, 'prt_veloc')
    call set(parser, 'nx_output', self%prt_grad, 'prt_grad')
    call set(parser, 'nx_output', self%prt_epot, 'prt_epot')
    call set(parser, 'nx_output', self%prt_ekin, 'prt_ekin')
    call set(parser, 'nx_output', self%prt_osc, 'prt_osc')
    call set(parser, 'nx_output', self%prt_etot, 'prt_etot')
    call set(parser, 'nx_output', self%prt_nstatdyn, 'prt_nstatdyn')
    call set(parser, 'nx_output', self%prt_nad, 'prt_nad')
    call set(parser, 'nx_output', self%prt_wf, 'prt_wf')
    call set(parser, 'nx_output', self%prt_shprob, 'prt_shprob')
    call set(parser, 'nx_output', self%prt_shrx, 'prt_shrx')
    call set(parser, 'nx_output', self%prt_shseed, 'prt_shseed')
    call set(parser, 'nx_output', self%prt_inad, 'prt_inad')
    call set(parser, 'nx_output', self%prt_gamma, 'prt_gamma')
    call set(parser, 'nx_output', self%prt_diab_ham, 'prt_diab_ham')
    call set(parser, 'nx_output', self%prt_diab_en, 'prt_diab_en')
    call set(parser, 'nx_output', self%prt_diab_pop, 'prt_diab_pop')
    call set(parser, 'nx_output', self%prt_angmom, 'prt_angmom')
    call set(parser, 'nx_output', self%prt_linmom, 'prt_linmom')

#ifdef USE_HDF5
    call set_realloc(parser, 'nx_output', self%user_name, 'user_name')
    call set_realloc(parser, 'nx_output', self%user_email, 'user_email')
    call set_realloc(parser, 'nx_output', self%h5file, 'h5file')
#endif
  end subroutine output_set_options


  subroutine output_init_txt_files(self)
    class(nx_output_t), intent(inout) :: self

    integer :: u
    
    call self%set_txtout_path()

    if (self%use_txt_outputs) then

       if (self%prt_geom) then
          open(newunit=u, file=self%txtout%geomf, action='write', status='new')
          close(u)
       end if
       
       if (self%prt_veloc) then
          open(newunit=u, file=self%txtout%velof, action='write', status='new')
          close(u)
       end if

       if (self%prt_grad) then
          open(newunit=u, file=self%txtout%gradf, action='write', status='new')
          close(u)
       end if

       if (self%prt_nad) then
          open(newunit=u, file=self%txtout%nadvf, action='write', status='new')
          close(u)
       end if
       
       if (self%prt_inad) then
          open(newunit=u, file=self%txtout%imnadf, action='write', status='new')
          close(u)
       end if

       if (self%prt_gamma) then
          open(newunit=u, file=self%txtout%gammaf, action='write', status='new')
          write(u, '(A)') '# Created with Newton-X version '//self%creator_version
          write(u, '(A,A9,A20,A20,A20,A20)') &
               & '#', 'Time', 'Step', 'State 1', 'State 2', '...'
          close(u)
       end if

       if (self%prt_wf) then
          open(newunit=u, file=self%txtout%wavef, action='write', status='new')
          write(u, '(A)') '# Created with Newton-X version '//self%creator_version
          write(u, '(A,A9,A20,A20,A20,A20,A20,A20)') &
               & '#', 'Time', 'Step', 'Real(State 1)', 'Im(State 1)', &
               & 'Real(State 2)', 'Im(State 2)', '...'
          close(u)

          open(newunit=u, file=self%txtout%popuf, action='write', status='new')
          write(u, '(A)') '# Created with Newton-X version '//nx_version()
          write(u, '(A,A9, A20, A20, A20, A20)') &
               & '#', 'Time', 'Step', 'State 1', 'State 2', '...'
          close(u)
       end if

       if (self%prt_ekin .or. self%prt_etot .or. self%prt_epot &
            & .or. self%prt_nstatdyn) then
          open(newunit=u, file=self%txtout%enerf, action='write', status='new')
          write(u, '(A)') '# Created with Newton-X version '//self%creator_version
          write(u, '(A,A9, A20, A10, A20, A20, A20, A20, A20)') &
               & '#', 'Time', 'Step', 'State', 'Etot', 'Ekin', 'Epot1', 'Epot2', '...'
          close(u)
       end if

       if (self%prt_shseed) then
          open(newunit=u, file=self%txtout%seedf, action='write', status='new')
          close(u)
       end if

       if (self%prt_shprob .or. self%prt_shrx) then
          open(newunit=u, file=self%txtout%shprf, action='write', status='new')
          write(u, '(A)') '# Created with Newton-X version '//nx_version()
          write(u, '(A,A9, A10, A20, A20, A20, A20)')&
               & '#', 'Time', 'Step', 'RNG', 'Prob state 1', 'Prob state 2', '...'
          close(u)
       end if

       open(newunit=u, file=self%txtout%statusf, action='write', status='new')
       write(u, '(I0)') 0
       write(u, '(F10.3)') 0.0
       close(u)

       if (self%prt_diab_ham) then
          open(newunit=u, file=self%txtout%diahamf, action='write', status='new')
          close(u)
       end if

       if (self%prt_mopdyn) then
          open(newunit=u, file=self%txtout%mopdyn, status='new', &
               & form='unformatted', action='write')
          write(u) 1, -1, 0
          write(u) 0, 0, 0, 1, 1
          close(u)
       end if

       if (self%prt_diab_pop) then
          open(newunit=u, file=self%txtout%diapopf, action='write', status='new')
          write(u, '(A10, A20, A20, A20, A20, A20, A20)') &
               & 'Time', 'Kind1 State 1', 'Kind1 State 2', &
               & 'Kind2 State 1', 'Kind2 State 2', 'Kind3 State 1', 'Kind3 State 2'
          close(u)
       end if

       if (self%prt_osc) then
          open(newunit=u, file=self%txtout%oscsf, action='write', status='new')
          write(u, '(A)') '# Created with Newton-X version '//self%creator_version
          write(u, '(A,A9, A20, A20, A20, A20)') &
               & '#', 'Time', 'Step', 'f(1->2)', 'f(1->3)', '...'
          close(u)
       end if

       if (self%prt_linmom) then
          open(newunit=u, file=self%txtout%lmomf, action='write', status='new')
          write(u, '(A)') '# Created with Newton-X version '//self%creator_version
          write(u, '(A,A9, A20, A20, A20, A20)') &
               & '#', 'Time', 'Total x', 'Total y', 'Total z', 'Norm'
          close(u)
       end if
       
       if (self%prt_angmom) then
          open(newunit=u, file=self%txtout%amomf, action='write', status='new')
          write(u, '(A)') '# Created with Newton-X version '//self%creator_version
          write(u, '(A,A9, A20, A20, A20, A20)') &
               & '#', 'Time', 'Total x', 'Total y', 'Total z', 'Norm'
          close(u)
       end if
    end if

    open(newunit=u, file=self%txtout%mhopf, action='write', status='new')
    close(u)

    open(newunit=u, file=self%txtout%hoppf, action='write', status='new')
    close(u)
  end subroutine output_init_txt_files


  subroutine output_write_txt_files(self, traj)
    class(nx_output_t), intent(inout) :: self
    type(nx_traj_t), intent(in) :: traj

    real(dp) :: t
    integer :: u, i, j, k
    integer :: nstatdyn, s, nstat, ms
    character(len=3) :: at
    character(len=64) :: eprtfmt ! Format for printing energies
    character(len=64) :: pprtfmt ! Format for printing populations
    character(len=64) :: wprtfmt ! Format for printing wavefunction
    character(len=64) :: prtfmt
    character(len=64) :: prtfmt1

    t = traj%t
    s = traj%step
    nstatdyn = traj%nstatdyn
    nstat = size(traj%epot)

    call self%set_txtout_path()

    write(eprtfmt, '(A,I0,A)') &
         & '(F10.3,I20,I10,2F20.12,', nstat, 'F20.12)'
    write(pprtfmt, '(A,I0,A)') &
         & '(F10.3,I20,', nstat, 'F20.12)'
    write(wprtfmt, '(A,I0,A)') &
         & '(F10.3,I20,', 2*size(traj%wf), 'F20.12)'

    ! Write current status
    open(newunit=u, file=self%txtout%statusf, action='write')
    write(u, *) s
    write(u, *) t
    close(u)

    ! Write geometry
    if (self%prt_geom) then
       open(newunit=u, file=self%txtout%geomf, action='write', position='append')
       write(u, '(I0)') size(traj%geom, 2)
       write(u, '(F10.3,I20)') t, s
       do i=1, size(traj%geom, 2)
          at = to_upper(traj%atoms(i))
          write(u, '(A3, 3F20.12)') &
               & adjustr(at), (traj%geom(j, i)*au2ang, j=1, 3)
       end do
       close(u)
    end if

    ! Write velocity
    if (self%prt_veloc) then
       open(newunit=u, file=self%txtout%velof, action='write', position='append')
       write(u, '(I0)') size(traj%veloc, 2)
       write(u, '(F10.3,I20)') t, s
       do i=1, size(traj%veloc, 2)
          write(u, '(3F20.12)') &
               & (traj%veloc(j, i), j=1, 3)
       end do
       close(u)
    end if

    ! Write gradients
    if (self%prt_grad) then
       open(newunit=u, file=self%txtout%gradf, action='write', position='append')
       write(u, '(F10.3,I20)') t, s
       write(u, '(I5, I5)') size(traj%grad, 1), size(traj%grad, 3)
       do i=1, size(traj%grad, 1)
          do j=1, size(traj%grad, 3)
             write(u, '(3F20.12)') &
                  & (traj%grad(i, k, j), k=1, 3)
          end do
       end do
       close(u)
    end if

    ! Write NAD
    if (self%prt_nad) then
       open(newunit=u, file=self%txtout%nadvf, action='write', position='append')
       write(u, '(F10.3,I20)') t, s
       write(u, '(I5, I5)') size(traj%nad, 1), size(traj%nad, 3)
       do i=1, size(traj%nad, 1)
          do j=1, size(traj%nad, 3)
             write(u, '(3F20.12)') &
                  & (traj%nad(i, k, j), k=1, 3)
          end do
       end do
       close(u)
    end if
    
    ! Write wavefunction
    if (self%prt_wf) then
       open(newunit=u, file=self%txtout%wavef, action='write', position='append')
       write(u, wprtfmt) &
            & t, s, (real(traj%wf(i)), aimag(traj%wf(i)), i=1, size(traj%wf))
       close(u)

       ! Write populations
       open(newunit=u, file=self%txtout%popuf, action='write', position='append')
       write(u, pprtfmt) &
            & t, s, (traj%population(i), i=1, size(traj%population))
       close(u)
    end if

    ! Write energies
    if (self%prt_ekin .or. self%prt_etot .or. self%prt_epot&
         & .or. self%prt_nstatdyn) then
       open(newunit=u, file=self%txtout%enerf, action='write', position='append')
       write(u, fmt=eprtfmt) &
            & t, s, nstatdyn, traj%etot, traj%ekin, (traj%epot(i), i=1, nstat)
       close(u)
    end if

    if (self%prt_shrx .or. self%prt_shprob) then
       ms = size(traj%rx)
       write(prtfmt1, '(A,I0,A)') '(F10.3,I10,', nstat + 1, 'F20.12)'
       write(prtfmt, '(A,I0,A)') '(I20,', nstat + 1, 'F20.12)'

       open(newunit=u, file=self%txtout%shprf, action='write', position='append')
       write(u, prtfmt1) &
            & t, 1, traj%rx(1), (traj%shprob(1, j), j=1, nstat)
       do i=2, ms
          write(u, prtfmt) &
               & i, traj%rx(i), (traj%shprob(i, j), j=1, nstat)
       end do
       close(u)
    end if

    ! Write seed
    if (self%prt_shseed) then
       block
         integer :: nseed
         integer, allocatable, dimension(:) :: seed

         call random_seed(size=nseed)
         allocate(seed(nseed))
         call random_seed(get=seed)

         open(newunit=u, file=self%txtout%seedf, action='write', position='append')
         write(u, *) traj%step, seed
         close(u)
         deallocate(seed)
       end block
    end if

    if (self%prt_gamma) then
       write(eprtfmt, '(A,I0,A)') '(F10.3,I20,', nstat, 'F20.12)'
       open(newunit=u, file=self%txtout%gammaf, action='write', position='append')
       write(u, fmt=eprtfmt) t, s, (traj%gamma(i), i=1, size(traj%gamma))
       close(u)
    end if

    if (self%prt_inad) then
       open(newunit=u, file=self%txtout%imnadf, action='write', position='append')
       write(u, '(F10.3,I20)') t, s
       write(u, '(I5, I5)') size(traj%nad_i, 1), size(traj%nad_i, 3)
       do i=1, size(traj%nad_i, 1)
          do j=1, size(traj%nad_i, 3)
             write(u, '(3F20.12)') &
                  & (traj%nad_i(i, k, j), k=1, 3)
          end do
       end do
       close(u)
    end if

    ! Write gradients and oscillator strength

    if (self%prt_osc) then
       write(eprtfmt, '(A,I0,A)') '(F10.3,I20,', size(traj%osc), 'F20.12)'

       open(newunit=u, file=self%txtout%oscsf, action='write', position='append')
       write(u, eprtfmt) &
            & traj%t, traj%step, (traj%osc(i), i=1, size(traj%osc))
       close(u)
    end if

    ! Write Linear Momentum
    if (self%prt_linmom) then
       open(newunit=u, file=self%txtout%lmomf, action='write', position='append')
       write(u, '(F10.3, 999F20.12)') &
            &    t, (traj%slinmom(i), i=1, 3), norm(traj%slinmom)
       close(u)
    end if

    ! Write Angular Momentum
    if (self%prt_angmom) then
       open(newunit=u, file=self%txtout%amomf, action='write', position='append')
       write(u, '(F10.3, 999F20.12)') &
            &    t, (traj%sangmom(i), i=1, 3), norm(traj%sangmom)
       close(u)
    end if

    ! Write additional exciton informations
    if (self%prt_diab_ham) then
       open(newunit=u, file=self%txtout%diahamf, action='write', position='append')
       write(u, '(F10.3,I20)') t, s
       write(u, '(I5, I5)') nstat, nstat
       do i = 1, nstat
          write(u, '(1x, *(1x,f20.6))') (traj%diaham(i,j), j=1, nstat)
       end do
       close(u)
    end if
    
    if (self%prt_diab_en) then
       open(newunit=u, file=self%txtout%diapopf, action='write', position='append')
       write(u, '(F10.3,1x,I10,1x,I5)') t, s, nstat
       write(u, *) "Current adiabatic state:", nstatdyn
       write(u,*) "Diab. state            Diab. population                Diab. Energy"
       do i = 1, nstat
          write(u, '(2x,i5,20x,f10.8, 13x, f18.8)') i, traj%diapop(i), traj%diaen(i)
       end do
       write(u,*)
       close(u)
    end if

    if (self%prt_diab_pop) then
       open(newunit=u, file=self%txtout%diapopf, action='write', position='append')
       write(u, '(F10.3, 999F20.12)') &
            & traj%t, (traj%diabatic_pop(1, i), i=1, size(traj%diabatic_pop, 2)), &
            &         (traj%diabatic_pop(2, i), i=1, size(traj%diabatic_pop, 2)), &
            &         (traj%diabatic_pop(3, i), i=1, size(traj%diabatic_pop, 2))
       close(u)
    end if
  end subroutine output_write_txt_files


  subroutine output_write_hopping_veloc(self, vel_before, vel_after, time, step, is_sh, microstep)
    !! Write the velocities in case of surface hopping.
    !!
    class(nx_output_t), intent(in) :: self
    real(dp), dimension(:, :), intent(in) :: vel_before
    real(dp), dimension(:, :), intent(in) :: vel_after
    real(dp), intent(in) :: time
    integer, intent(in) :: step
    integer, intent(in) :: is_sh
    !! If ``is_sh == 1``, then the interpolated velocities are printed
    !! in the corresponding file. Else, we write the main velocities.
    integer, intent(in), optional :: microstep
    !! In case we want to print interpolated velocities, then we can
    !! also give the ``ms`` step.

    integer :: u, i, j
    character(len=1024) :: filename

    if (is_sh == 1) then
       filename = self%txtout%mhopf
    else
       filename = self%txtout%hoppf
    end if

    open(newunit=u, file=filename, action='write', position='append')
    if (is_sh == 1) then
       write(u, '(A,F10.3,A,I0,A,I0,A)')&
            & 'SURFACE HOPPING AT t=', time, ', STEP ', step, '(', microstep,')'
    else
       write(u, '(A,F10.3,A,I0)') 'SURFACE HOPPING AT t=', time,' STEP ', step
    end if

    write(u, *) '  Velocity before hopping'
    do i=1, size(vel_before, 2)
       write(u, '(3F20.12)') (vel_before(j, i), j=1, 3)
    end do
    write(u, *) ''
    write(u, *) '  Velocity after hopping'
    do i=1, size(vel_after, 2)
       write(u, '(3F20.12)') (vel_after(j, i), j=1, 3)
    end do
    write(u, *) repeat('=', 80)
    write(u, *) ''
    close(u)
  end subroutine output_write_hopping_veloc
  

  subroutine output_reset_path(self, new_path)
    class(nx_output_t), intent(inout) :: self
    character(len=*), intent(in) :: new_path

    if (.not. str_ends_with(new_path, '/')) then
       deallocate(self%base_path)
       allocate( character(len=len_trim(new_path) + 1) :: self%base_path)
       self%base_path = trim(new_path)//'/'
    else
       self%base_path = new_path       
    end if
  end subroutine output_reset_path


  subroutine output_destroy(self)
    class(nx_output_t), intent(inout) :: self

    deallocate(self%base_path)
    deallocate(self%creator)
    deallocate(self%creator_version)
    call self%txtout%destroy()
  end subroutine output_destroy


  subroutine output_write_restart_files(self, step, nstat, target_dir, ierr)
    class(nx_output_t), intent(in) :: self
    integer, intent(in) :: step
    integer, intent(in) :: nstat
    character(len=*), intent(in) :: target_dir
    integer, intent(out) :: ierr

    call io_txt_extract_geometry(self, target_dir, step, ierr)
    if (ierr /= 0) then
       print *, 'Error: step '//to_str(step)//' not found in '//self%txtout%geomf
       return
    end if
    
    call io_txt_extract_velocity(self, target_dir, step, ierr)
    if (ierr /= 0) then
       print *, 'Error: step '//to_str(step)//' not found in '//self%txtout%velof
       return
    end if

    call io_txt_extract_wf(self, target_dir, step, nstat, ierr)
    if (ierr /= 0) then
       print *, 'Error: step '//to_str(step)//' not found in '//self%txtout%wavef
       return
    end if

    call io_txt_extract_nstatdyn(self, target_dir, step, nstat, ierr)
    if (ierr /= 0) then
       print *, 'Error: step '//to_str(step)//' not found in '//self%txtout%enerf
       return
    end if

    call io_txt_extract_seed(self, target_dir, step, nstat, ierr)
    if (ierr /= 0) then
       print *, 'Error: step '//to_str(step)//' not found in '//self%txtout%seedf
       return
    end if
  end subroutine output_write_restart_files


  subroutine io_txt_extract_geometry(nx_out, target_dir, step, ierr)
    !! Extract the geometry from a specific step.
    !!
    !! The routine parses ``geometries.xyz`` to find the geometry that corresponds to the
    !! given ``step``. It also parses ``geom.orig`` for atomic numbers, masses and atom
    !! names. Once done, it writes the resulting geometry in NX format in the file
    !! ``new_geom`` in the current directory.
    !!
    !! If the selected ``step`` is not found in ``geometries.xyz``, the function
    !! terminates with an error (code 127).
    type(nx_output_t), intent(in) :: nx_out
    character(len=*), intent(in) :: target_dir
    integer, intent(in) :: step
    !! Step from which to extract the geometry.
    integer, intent(out) :: ierr
    !! Error handling.

    character(len=1024) :: infile
    character(len=1024) :: buf
    real(dp), allocatable :: geom(:, :), masses(:), Z(:)
    character(len=2), allocatable :: atoms(:)

    integer :: u, nat, i, j
    real(dp) :: t
    integer :: s, found

    ! Extract geometry
    infile = trim(nx_out%base_path)//'/geometries.xyz'
    found = -1
    open(newunit=u, file=infile, action='read')
    read(u, *) nat
    allocate(geom(3, nat))
    allocate(atoms(nat))
    allocate(Z(nat))
    allocate(masses(nat))
    FIND_GEOM: do
       read(u, *, iostat=ierr) t, s
       if (ierr /= 0) exit
       if (s == step) then
          do i=1, nat
             read(u, *) atoms(i), (geom(j, i), j=1, 3)
          end do
          found = 1
          exit FIND_GEOM
       else
          do i=1, nat
             read(u, '(A)') buf
          end do

          read(u, '(A)', iostat=ierr) buf
          if (ierr /= 0) exit
       end if
    end do FIND_GEOM
    close(u)

    ierr = 0
    if (found == -1) then
       ierr = -1
       return
    end if
    geom(:, :) = geom(:, :)/au2ang

    open(newunit=u, file='geom.orig', action='read')
    do i=1, nat
       read(u, *) buf, Z(i), buf, buf, buf, masses(i)
    end do
    close(u)

    open(newunit=u, file=trim(target_dir)//'/geom.orig', action='write')
    do i=1, nat
       write(u, '(A2,F6.2,3F24.12,F20.12)')&
            & atoms(i), Z(i), (geom(j, i), j=1, 3), masses(i)
    end do
    close(u)
  end subroutine io_txt_extract_geometry


  subroutine io_txt_extract_velocity(nx_out, target_dir, step, ierr)
    !! Extract the velocity from a specific step.
    !!
    !! The routine parses ``velocities.dat`` to find the velocity that corresponds to the
    !! given ``step``. Once done, it writes the resulting velocity in NX format in the file
    !! ``new_veloc`` in the current directory.
    !!
    !! If the selected ``step`` is not found in ``velocities.dat``, the function
    !! terminates with an error (code 127).
    type(nx_output_t), intent(in) :: nx_out
    character(len=*), intent(in) :: target_dir
    integer, intent(in) :: step
    !! Step from which to extract the velocity.
    integer, intent(out) :: ierr
    !! Error handling.

    character(len=1024) :: infile
    character(len=1024) :: buf
    real(dp), allocatable :: veloc(:, :)

    integer :: u, nat, i, j
    real(dp) :: t
    integer :: s, found

    ! Extract velocity
    infile = trim(nx_out%base_path)//'/velocities.dat'
    found = -1
    open(newunit=u, file=infile, action='read')
    read(u, *) nat
    allocate(veloc(3, nat))
    FIND_VELOC: do
       read(u, *, iostat=ierr) t, s
       if (ierr /= 0) exit
       if (s == step) then
          do i=1, nat
             read(u, *) (veloc(j, i), j=1, 3)
          end do
          found = 1
          exit FIND_VELOC
       else
          do i=1, nat
             read(u, '(A)') buf
          end do
          read(u, '(A)', iostat=ierr) buf
          if (ierr /= 0) exit
       end if
    end do FIND_VELOC
    close(u)

    ierr = 0
    if (found == -1) then
       ierr = -1
       return
    end if

    open(newunit=u, file=target_dir//'/veloc.orig', action='write')
    do i=1, nat
       write(u, '(3F24.12)')&
            & (veloc(j, i), j=1, 3)
    end do
    close(u)
  end subroutine io_txt_extract_velocity


  subroutine io_txt_extract_wf(nx_out, target_dir, step, nstat, ierr)
    !! Extract the wavefunction from a specific step.
    !!
    !! The routine parses ``wf.dat`` to find the wavefunction that corresponds to the
    !! given ``step``. Once done, it writes the resulting wavefunction in NX format in the file
    !! ``new_wf`` in the current directory.
    !!
    !! If the selected ``step`` is not found in ``wf.dat``, the function terminates with
    !! an error (code 127).
    type(nx_output_t), intent(in) :: nx_out
    character(len=*), intent(in) :: target_dir
    integer, intent(in) :: step
    !! Step from which to extract the wavefunction.
    integer, intent(in) :: nstat
    !! Number of states in the computation.
    integer, intent(out) :: ierr
    !! Error handling.

    character(len=1024) :: infile
    character(len=1024) :: buf
    character(len=64) :: wfprtfmt
    real(dp), allocatable :: wf(:, :)
    integer :: found

    integer :: u, i, j
    real(dp) :: t
    integer :: s

    allocate(wf(2, nstat))
    write(wfprtfmt, '(A,I0,A)') &
         & '(F10.3,I20,', 2*nstat, 'F20.12)'

    found = -1
    infile = trim(nx_out%base_path)//'/wf.dat'
    open(newunit=u, file=infile, action='read')
    ! read(u, '(A)') buf ! Title line
    FIND_WF: do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (str_starts_with(buf, '#')) cycle

       read(buf, fmt=wfprtfmt, iostat=ierr) &
            & t, s, ((wf(i, j), i=1, 2), j=1, nstat)

       if (s == step) then
          found = 1
          exit FIND_WF
       end if
    end do FIND_WF
    close(u)

    ierr = 0
    if (found == -1) then
       ierr = -1
       return
    end if

    open(newunit=u, file=target_dir//'/wf.inp', action='write')
    do i=1, nstat
       write(u, '(2F24.12)')&
            & (wf(j, i), j=1, 2)
    end do
    close(u)
  end subroutine io_txt_extract_wf


  subroutine io_txt_extract_seed(nx_out, target_dir, step, seed_size, ierr)
    !! Extract the seed from a specific step.
    !!
    !! The routine parses ``seed.dat`` to find the seed that corresponds to the
    !! given ``step``. Once done, it writes the resulting seed in
    !! ``new_seed`` in the current directory.
    !!
    !! If the selected ``step`` is not found in ``seed.dat``, the function
    !! terminates with an error (code 127).
    type(nx_output_t), intent(in) :: nx_out
    character(len=*), intent(in) :: target_dir
    integer, intent(in) :: step
    !! Step from which to extract the seed.
    integer, intent(in) :: seed_size
    !! Size of the seed.
    integer, intent(out) :: ierr
    !! Error handling.

    character(len=1024) :: infile
    integer, allocatable :: seed(:)
    integer :: found

    integer :: u, i
    integer :: s

    allocate(seed(seed_size))

    found = -1

    infile = trim(nx_out%base_path)//'/seed.dat'
    open(newunit=u, file=infile, action='read')
    FIND_SEED: do
       read(u, *, iostat=ierr) &
            & s, (seed(i), i=1, seed_size)
       if (ierr /= 0) exit
       if (s == step) then
          found = 1
          exit FIND_SEED
       end if
    end do FIND_SEED
    close(u)

    ierr = 0
    if (found == -1) then
       ierr = -1
       return
    end if

    open(newunit=u, file=target_dir//'/rndseed', action='write')
    write(u, *)&
         & (seed(i), i=1, seed_size)
    close(u)
  end subroutine io_txt_extract_seed


  subroutine io_txt_extract_nstatdyn(nx_out, target_dir, step, nstat, ierr)
    !! Extract the ``nstatdyn`` from a specific step.
    !!
    !! The routine parses ``energies.dat`` to find the ``nstatdyn`` that corresponds to the
    !! given ``step``. Once done, it writes the resulting seed in
    !! ``new_nstatdyn`` in the current directory.
    !!
    !! If the selected ``step`` is not found in ``energies.dat``, the function
    !! terminates with an error (code 127).
    type(nx_output_t), intent(in) :: nx_out
    character(len=*), intent(in) :: target_dir
    integer, intent(in) :: step
    !! Step from which to extract ``nstatdyn``.
    integer, intent(in) :: nstat
    !! Number of states in the computation.
    integer, intent(out) :: ierr
    !! Error handling.

    character(len=1024) :: infile
    character(len=1024) :: buf
    character(len=64) :: eprtfmt
    real(dp), allocatable :: energies(:)
    integer :: nstatdyn

    integer :: u, i
    real(dp) :: t
    integer :: s

    allocate(energies(nstat + 2))

    write(eprtfmt, '(A,I0,A)') &
         & '(F10.3,I20,I10,2F20.12,', nstat, 'F20.12)'

    nstatdyn = -1

    infile = trim(nx_out%base_path)//'/energies.dat'
    open(newunit=u, file=infile, action='read')
    FIND_NSTATDYN: do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (str_starts_with(buf, '#')) cycle

       read(buf, fmt=eprtfmt, iostat=ierr) &
            & t, s, nstatdyn, (energies(i), i=1, nstat+2)

       if (s == step) then
          exit FIND_NSTATDYN
       end if
    end do FIND_NSTATDYN
    close(u)

    ierr = 0
    if (nstatdyn == -1) then
       ierr = -1
       return
    end if

    open(newunit=u, file=target_dir//'/nstatdyn', action='write')
    write(u, *)&
         & nstatdyn
    close(u)
  end subroutine io_txt_extract_nstatdyn
  
  
#ifdef USE_HDF5
  subroutine output_h5_init_output(self)
    class(nx_output_t), intent(inout) :: self

    integer :: hdferr
    integer(hid_t) :: grp_id
    character(len=:), allocatable :: filename

    filename = self%base_path//self%h5file

    call self%h5%init(&
         & filename, &
         & self%user_name, &
         & self%creator, &
         & self%creator_version, &
         & self%user_email&
         &)

    ! Create vmd_parameters
    call h5gcreate_f(self%h5%param_id, 'vmd_structure', grp_id, hdferr)
    call h5gclose_f(grp_id, hdferr)
  end subroutine output_h5_init_output


  subroutine output_h5_open_output(self)
    class(nx_output_t), intent(inout) :: self

    integer :: hdferr
    character(len=:), allocatable :: filename

    filename = self%base_path//self%h5file

    call self%h5%open(filename)

    ! Open particles groups
    call h5eset_auto_f(0, hdferr) ! Turn off error printing for check
    if (self%prt_geom) &
         call self%h5dat%geom%open('all/position', self%h5%part_id, hdferr)
    if (self%prt_veloc) &
         call self%h5dat%veloc%open('all/velocities', self%h5%part_id, hdferr)
    if (self%prt_grad) &
         call self%h5dat%grad%open('all/gradients', self%h5%part_id, hdferr)
    if (self%prt_nad) &
         call self%h5dat%nad%open('all/nad', self%h5%part_id, hdferr)

    ! At this point, if links exist in the file, all datasets
    ! "linked" attribute should be .true. meaning that step and time
    ! won't be incremented. Thus, we arbitrarily say that ``geom`` is
    ! not linked. We will do the same for the observable later
    if (self%h5dat%geom%linked) then
       self%h5dat%geom%linked = .false.
    end if

    ! Open observables groups
    if (self%prt_etot) &
         call self%h5dat%etot%open('total_energy', self%h5%obs_id, hdferr)
    if (self%prt_epot) &
         call self%h5dat%epot%open('potential_energy', self%h5%obs_id, hdferr)
    if (self%prt_nstatdyn) then
       call self%h5dat%nstatdyn%open('nstatdyn', self%h5%obs_id, hdferr)
    end if
    
    if (self%prt_ekin) &
         call self%h5dat%ekin%open('kinetic_energy', self%h5%obs_id, hdferr)
    if (self%prt_wf) then
       call self%h5dat%pop%open('populations', self%h5%obs_id, hdferr)
       call self%h5dat%wf%open('wavefunctions', self%h5%obs_id, hdferr)
    end if
    if (self%prt_osc) &
         call self%h5dat%osc%open('oscillator_strengths', self%h5%obs_id, hdferr)
    if (self%prt_linmom) &
         call self%h5dat%linmom%open('linear_momentum', self%h5%obs_id, hdferr)
    if (self%prt_angmom) &
         call self%h5dat%angmom%open('angular_momentum', self%h5%obs_id, hdferr)
    if (self%prt_shprob) &
         call self%h5dat%shprob%open('sh_probabilities', self%h5%obs_id, hdferr)
    if (self%prt_shrx) &
         call self%h5dat%shrx%open('generated_random_number', self%h5%obs_id, hdferr)
    if (self%prt_shseed) &
         call self%h5dat%seed%open('random_seed', self%h5%obs_id, hdferr)

    if (self%h5dat%etot%linked) then
       self%h5dat%etot%linked = .false.
    end if

    call h5eset_auto_f(1, hdferr) ! Turn off error printing for check
  end subroutine output_h5_open_output


  subroutine output_h5_close_output(self)
    class(nx_output_t), intent(inout) :: self

    integer :: hdferr

    call h5eset_auto_f(0, hdferr) ! Turn off error printing for check
    if (self%prt_geom) &
         call self%h5dat%geom%close()
    if (self%prt_veloc) &
         call self%h5dat%veloc%close()
    if (self%prt_grad) &
         call self%h5dat%grad%close()
    if (self%prt_nad) &
         call self%h5dat%nad%close()

    if (self%prt_etot) &
         call self%h5dat%etot%close()
    if (self%prt_epot) &
         call self%h5dat%epot%close()
    if (self%prt_nstatdyn) &
         call self%h5dat%nstatdyn%close()
    if (self%prt_ekin) &
         call self%h5dat%ekin%close()
    if (self%prt_wf) &
         call self%h5dat%pop%close()
    if (self%prt_wf) &
         call self%h5dat%wf%close()
    if (self%prt_osc) &
         call self%h5dat%osc%close()
    if (self%prt_linmom) &
         call self%h5dat%linmom%close()
    if (self%prt_angmom) &
         call self%h5dat%angmom%close()
    if (self%prt_shprob) &
         call self%h5dat%shprob%close()
    if (self%prt_shrx) &
         call self%h5dat%shrx%close()
    if (self%prt_shseed) &
         call self%h5dat%seed%close()
    
    call h5eset_auto_f(1, hdferr) ! Turn off error printing for check

    call self%h5%close()
  end subroutine output_h5_close_output


  subroutine output_h5_create_particles(self, name, box, traj)
    !! Create a ``particles`` type object.
    !!
    !! The routine creates a group with name ``name`` in the main ``h5`` object, with box
    !! ``box``. The object is initialized with respect to ``traj``, and the resulting
    !! ``h5_part`` object is created for convenience and later manipulation.
    !!
    !! The routine also populated the ``vmd_param`` group with information about the
    !! current system.
    class(nx_output_t), intent(inout) :: self
    character(len=*), intent(in) :: name
    character(len=*), dimension(:), intent(in) :: box
    type(nx_traj_t), intent(in) :: traj

    integer :: hdferr
    integer, dimension(:), allocatable :: present_species,&
         & h5_species
    integer :: i, j, old_species, spec_in_traj
    logical :: present
    integer(hid_t) :: vmd_param, part_id
    character(len=2), dimension(:), allocatable :: atoms
    integer, dimension(:), allocatable :: resid
    character(len=8), dimension(:), allocatable :: segid

    integer :: dim, nat, ncoupl, nstat
    integer(hsize_t), dimension(2) :: d2
    integer(hsize_t), dimension(3) :: d3

    dim = size(traj%geom, 1)
    nat = size(traj%geom, 2)
    nstat = size(traj%grad, 1)
    ncoupl = size(traj%nad, 1)

    call h5gcreate_f(self%h5%part_id, name, part_id, hdferr)
    call h5md_create_box(part_id, size(box), box)

    ! Now create the trajectory groups
    ! Time-dependent quantities: geometry, velocity, gradients and NAD
    if (self%prt_geom) then
       call self%h5dat%geom%init( &
            & part_id, H5T_NATIVE_DOUBLE, 'position', [dim, nat] &
            & )
    end if

    if (self%prt_veloc) then
       call self%h5dat%veloc%init( &
            & part_id, H5T_NATIVE_DOUBLE, 'velocities', [dim, nat], &
            & self%h5dat%geom%id & ! Linked to geom group
            & )
    end if
    
    if (self%prt_grad) then
       call self%h5dat%grad%init( &
            & part_id, H5T_NATIVE_DOUBLE, 'gradients', [nstat, dim, nat], &
            & self%h5dat%geom%id &
            & )
    end if

    if (self%prt_nad) then
       call self%h5dat%nad%init( &
            & part_id, H5T_NATIVE_DOUBLE, 'nad', [ncoupl, dim, nat], &
            & self%h5dat%geom%id &
            & )
    end if

    ! Time-independent : species, masses
    call h5md_add_fixed(part_id, 'species', traj%Z)
    call h5md_add_fixed(part_id, 'names', traj%atoms)
    call h5md_add_fixed(part_id, 'mass', traj%masses / proton)

    ! Determine the different species present in trajectory
    allocate(present_species(size(traj%Z)))
    present_species(:) = 0
    spec_in_traj = 0
    do i=1, size(traj%Z)
       present = .false.
       do j=1, size(present_species)
          ! write(*, *) 'atoms(i): ', traj%atoms(i)
          ! write(*, *) 'present_spec(j): ', present_species(j)
          if (traj%Z(i) == present_species(j)) then
             ! In this case the current atom is already in
             ! present_species
             present = .true.
          end if
       end do
       if (present .eqv. .false.) then
          ! If no match was found, this is a new atom and we add it
          ! to the list
          present_species(spec_in_traj + 1) = int(traj%Z(i))
          spec_in_traj = spec_in_traj + 1
       end if
    end do

    ! Now copy the already existing list if necessary
    if (self%h5%species(1) == 0) then
       deallocate(self%h5%species)
       allocate(self%h5%species(spec_in_traj))
       self%h5%species(:) = present_species(1:spec_in_traj)
    else
       old_species = size(self%h5%species)
       allocate(h5_species(old_species))
       deallocate(self%h5%species)
       allocate(self%h5%species(old_species + spec_in_traj))
       self%h5%species(1:old_species) = h5_species
       self%h5%species(old_species+1:old_species + spec_in_traj) = present_species(1:spec_in_traj)
    end if

    allocate(atoms(size(self%h5%species)))
    do i=1, size(atoms)
       atoms(i) = ATOM_NAMES(self%h5%species(i))
    end do

    allocate(resid(size(traj%atoms)))
    resid(:) = 0

    allocate(segid(size(traj%atoms)))
    segid(:) = "segA"

    ! And set this value in the right place in vmd_parameters
    call h5gopen_f(self%h5%param_id, 'vmd_structure', vmd_param, hdferr)
    call h5md_add_fixed(vmd_param, 'indexOfSpecies', self%h5%species)
    call h5md_add_fixed(vmd_param, 'atomicnumber', self%h5%species)
    call h5md_add_fixed(vmd_param, 'name', atoms)
    call h5md_add_fixed(vmd_param, 'type', atoms)
    call h5md_add_fixed(vmd_param, 'resid', resid)
    call h5md_add_fixed(vmd_param, 'segid', segid)
    call h5md_add_fixed(vmd_param, 'resname', ["mol"])
    call h5md_add_fixed(vmd_param, 'chain', ["AA"])
    call h5md_add_fixed(vmd_param, 'segname', segid)
    call h5gclose_f(vmd_param, hdferr)
  end subroutine output_h5_create_particles
  

  subroutine output_h5_create_obs(self, conf, ms)
    !! Create an ``observables`` type object.
    !!
    !! The routine creates a group with name ``name`` in the main ``h5`` object, with box
    !! ``box``. The object is initialized with respect to ``traj``, and the resulting
    !! ``h5_part`` object is created for convenience and later manipulation.
    class(nx_output_t), intent(inout) :: self
    type(nx_config_t), intent(in) :: conf
    integer :: ms

    integer(hid_t) :: obs_id

    ! Dimension for ekin, etot, nstatdyn
    if (self%prt_etot) then
       call self%h5dat%etot%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'total_energy', [0] &
            & )
    end if

    if (self%prt_ekin) then
       call self%h5dat%ekin%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'kinetic_energy', [0], &
            & self%h5dat%etot%id&
            & )
    end if

    if (self%prt_nstatdyn) then
       call self%h5dat%nstatdyn%init( &
            & self%h5%obs_id, H5T_NATIVE_INTEGER, 'nstatdyn', [0], &
            & self%h5dat%etot%id &
            & )
    end if

    ! Dimensions for epot, populations
    ! d = [nstates]
    if (self%prt_epot) then
       call self%h5dat%epot%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'potential_energy', [conf%nstat], &
            & self%h5dat%etot%id &
            & )
    end if

    if (self%prt_wf) then
       call self%h5dat%pop%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'populations', [conf%nstat], &
            & self%h5dat%etot%id &
            & )

       ! Dimensions for wavefunction
       ! d2 = [2, nstates]
       call self%h5dat%wf%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'wavefunctions', [2, conf%nstat], &
            & self%h5dat%etot%id &
            & )
    end if
    
    ! Oscillator strengths
    if (self%prt_osc) then
       call self%h5dat%osc%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'oscillator_strengths', [conf%nstat - 1], &
            & self%h5dat%etot%id &
            & )
    end if

    ! Surface hopping
    if (self%prt_shprob ) then
       call self%h5dat%shprob%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'sh_probabilities', [ms, conf%nstat], &
            & self%h5dat%etot%id &
            & )
    end if
    
    if (self%prt_shrx) then
       call self%h5dat%shrx%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'generated_random_number', [ms], &
            & self%h5dat%etot%id &
            & )
    end if

    if (self%prt_shseed) then
       block
         integer :: nseed
         integer, allocatable, dimension(:) :: seed
         call random_seed(size=nseed)
         allocate(seed(nseed))
         call random_seed(get=seed)
       
         call self%h5dat%seed%init( &
              & self%h5%obs_id, H5T_NATIVE_INTEGER, 'random_seed', [nseed], &
              & self%h5dat%etot%id &
              & )
         call self%h5dat%seed%append( seed, conf%init_step, conf%init_time)
       end block
    end if
    
    ! Momenta
    if (self%prt_linmom) then
       call self%h5dat%linmom%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'linear_momentum', [4], &
            & self%h5dat%etot%id &
            & )
    end if

    if (self%prt_angmom) then
       call self%h5dat%angmom%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE,  'angular_momentum', [4], &
            & self%h5dat%etot%id &
            &)
    end if
    
    ! Exciton dynamics informations
    if (self%prt_diab_ham) then
       call self%h5dat%diaham%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'diaham', [conf%nstat, conf%nstat], &
            & self%h5dat%etot%id &
            & )
    end if

    if (self%prt_diab_en) then
       call self%h5dat%diaen%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'diaen', [conf%nstat], &
            & self%h5dat%etot%id &
            & )
    end if

    if (self%prt_diab_pop) then
       call self%h5dat%diapop%init( &
            & self%h5%obs_id, H5T_NATIVE_DOUBLE, 'diapop', [conf%nstat], &
            & self%h5dat%etot%id &
            & )
    end if
  end subroutine output_h5_create_obs


  subroutine output_h5_write_traj(self, traj)
    class(nx_output_t), intent(inout) :: self
    type(nx_traj_t), intent(in) :: traj

    real(dp), dimension(:, :), allocatable :: wf
    real(dp), dimension(4) :: mom
    integer :: nseed
    integer, allocatable :: seed(:)

    if (self%prt_geom) &
         call self%h5dat%geom%append(au2ang*traj%geom, traj%step, traj%t)
    if (self%prt_veloc) &
         call self%h5dat%veloc%append(traj%veloc, traj%step, traj%t)

    if (self%prt_nad) &
         call self%h5dat%nad%append(traj%nad, traj%step, traj%t)

    if (self%prt_grad) &
         call self%h5dat%grad%append(traj%grad, traj%step, traj%t)

    if (self%prt_etot) &
         call self%h5dat%etot%append(traj%etot, traj%step, traj%t)
    if (self%prt_epot) &
         call self%h5dat%epot%append(traj%epot, traj%step, traj%t)
    if (self%prt_nstatdyn) &
         call self%h5dat%nstatdyn%append(traj%nstatdyn, traj%step, traj%t)
    if (self%prt_ekin) &
         call self%h5dat%ekin%append(traj%ekin, traj%step, traj%t)
    if (self%prt_wf) then
       call self%h5dat%pop%append(traj%population, traj%step, traj%t)

       ! Convert wavefunction from complex to real
       allocate(wf(2, size(traj%wf)))
       wf(1, :) = real(traj%wf(:))
       wf(2, :) = aimag(traj%wf(:))
       call self%h5dat%wf%append(wf, traj%step, traj%t)
    end if
    
    if (self%prt_diab_ham) &
         call self%h5dat%diaham%append(traj%diaham, traj%step, traj%t)
    if (self%prt_diab_en) &
         call self%h5dat%diaen%append(traj%diaen, traj%step, traj%t)
    if (self%prt_diab_pop) &
         call self%h5dat%diapop%append(traj%diapop, traj%step, traj%t)
    
    if (self%prt_shprob) &
       call self%h5dat%shprob%append( traj%shprob, traj%step, traj%t)
    if (self%prt_shrx) &
         call self%h5dat%shrx%append( traj%rx, traj%step, traj%t )

    if (self%prt_shseed) then
       call random_seed(size=nseed)
       allocate(seed(nseed))
       call random_seed(get=seed)
    
       call self%h5dat%seed%append( seed, traj%step, traj%t)
    end if
    
    if (self%prt_osc) &
         call self%h5dat%osc%append( traj%osc, traj%step, traj%t )
    
    if (self%prt_angmom) then
       mom(1:3) = traj%sangmom(:)
       mom(4) = norm(traj%sangmom)
       call self%h5dat%angmom%append(mom, traj%step, traj%t)
    end if

    if (self%prt_linmom) then
       mom(1:3) = traj%slinmom(:)
       mom(4) = norm(traj%slinmom)
       call self%h5dat%linmom%append(mom, traj%step, traj%t)
    end if
  end subroutine output_h5_write_traj


  subroutine output_h5_write_restart(self, step, target_dir, ierr)
    class(nx_output_t), intent(inout) :: self
    integer, intent(in) :: step
    character(len=*), intent(in) :: target_dir
    integer, intent(out) :: ierr

    integer :: hdferr
    integer, allocatable :: all_steps(:)
    integer :: selection

    real(dp), allocatable :: geom(:, :), veloc(:, :), wf(:, :)
    real(dp), allocatable :: masses(:), z(:)
    character(len=2), allocatable :: names(:)
    integer, allocatable :: rndseed(:)
    integer :: nstatdyn
    character(len=64) :: wfprtfmt
    logical :: seed_exists

    integer :: i, j, u

    ! Step selection
    call self%h5dat%geom%read_steps( all_steps )
    selection = -1
    do i=1, size(all_steps)
       if (step == all_steps(i)) then
          selection = i
       end if
    end do

    ierr = 0
    if (selection == -1) then
       ! The selected step does not exist in the output
       ierr = -1
       return
    end if

    ! Velocity
    call self%h5dat%veloc%read_single( veloc, selection )
    open(newunit=u, file=trim(target_dir)//'/veloc.orig', action='write')
    do i=1, size(veloc, 2)
       write(u, '(3F24.12)')&
            & (veloc(j, i), j=1, 3)
    end do
    close(u)

    ! Geometry
    call self%h5dat%geom%read_single( geom, selection )
    call h5md_read_fixed(self%h5%part_id, 'all/names', names)
    call h5md_read_fixed(self%h5%part_id, 'all/mass', masses)
    call h5md_read_fixed(self%h5%part_id, 'all/species', Z)
    open(newunit=u, file=trim(target_dir)//'/geom.orig', action='write')
    do i=1, size(geom, 2)
       write(u, '(A2,F6.2,3F24.12,F20.12)')&
            & names(i), Z(i), (geom(j, i)/au2ang, j=1, 3), masses(i)
    end do
    close(u)

    ! Nstatdyn
    if (self%prt_nstatdyn) then
       call self%h5dat%nstatdyn%read_single( nstatdyn, selection )
       open(newunit=u, file=trim(target_dir)//'/nstatdyn', action='write')
       write(u, *) nstatdyn
       close(u)
    end if

    ! WF
    if (self%prt_wf) then
       call self%h5dat%wf%read_single( wf, selection )
       write(wfprtfmt, '(A,I0,A)') &
            & '(F10.3,I20,', 2*size(wf, 1), 'F20.12)'
       open(newunit=u, file=trim(target_dir)//'/wf.inp', action='write')
       do i=1, size(wf, 2)
          write(u, '(2F24.12)')&
               & (wf(j, i), j=1, 2)
       end do
       close(u)
    end if

    ! Random seed
    if (self%prt_shseed) then
       call h5lexists_f(self%h5%obs_id, 'random_seed', seed_exists, hdferr)
       if (seed_exists) then
          call self%h5dat%seed%read_single( rndseed, selection )
          open(newunit=u, file=trim(target_dir)//'/rndseed', action='write')
          write(u, *)&
               & (rndseed(i), i=1, size(rndseed))
          close(u)
       end if
    end if
  end subroutine output_h5_write_restart
#endif
end module mod_io
