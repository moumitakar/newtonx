! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_timers
  use mod_kinds, only: dp
  use mod_constants, only: MAX_STR_SIZE
  use iso_fortran_env, only: stdout=>output_unit
  
  ! Timers
  public :: time_r
  type nx_timer_t
     !! Object to keep track of time in the dynamics.
     !!
     !! This object can be used to time individual steps, and can also be
     !! used to report on the total time spent in each part.
     !! The ``time``, ``count`` and ``max`` members correspond to the
     !! arguments of the Fortran routine ``system_clock`` (see [GCC
     !! documentation] (https://gcc.gnu.org/onlinedocs/gfortran/TIME.html)).
     !!
     !! Usage:
     !!
     !! ```fortran
     !! type(nx_timer_t) :: timer
     !! call timer%init()
     !! call timer%start()
     !! call do_something(parameters)
     !! call timer%update()
     !! write(*, *) 'Time spent doing something: ', timer%time
     !!
     !! call timer%start()
     !! call do_something_else(parameters)
     !! call timer%update()
     !! write(*, *) 'Time spent doing something else: ', timer%time
     !! write(*, *) 'Total time spent: ', timer%total
     !! ```
     !!
     !! In the dynamics, we typically set up different timers for the QM
     !! computation, the TDSE integration, and so on. Each call can then be
     !! monitored independently, and the total time is reported at the end.
     integer :: time
     !! Count of the processor clock since unspecified time in the past
     !! (modulo ``max``) just after calling ``start``, and count of the
     !! processor clock since calling ``start`` just after calling ``update``.
     integer :: count
     !! Number of clock ticks per second (system-dependent).
     integer :: max
     !! See ``time`` documentation.
     integer :: total
     !! Total time recorded by that timer
   contains
     procedure :: init => timer_init
     procedure :: start => timer_start
     procedure :: update => timer_update
     procedure :: print => timer_print
     procedure :: print_summary => timer_print_summary_s
     procedure :: to_seconds => timer_convert_to_seconds
  end type nx_timer_t

  interface nx_timer_t
     module procedure timer_create
  end interface nx_timer_t

  type, public :: nx_timers_t
     !! Collection of timers for running a dynamics.
     !!
     type(nx_timer_t) :: t_main
     !! Main timer.
     type(nx_timer_t) :: t_qm_exe
     !! Electronic structure execution time.
     type(nx_timer_t) :: t_qm_read
     !! Electronic structure reading time.
     type(nx_timer_t) :: t_sh
     !! Surface hopping time.
     type(nx_timer_t) :: t_cio
     !! Cioverlap time.
     type(nx_timer_t) :: t_auxnac
     !! Auxiliary non-adiabatic terms time.
     type(nx_timer_t) :: t_io_txt
     !! I/O time (for text outputs).
     type(nx_timer_t) :: t_io_h5
     !! I/O time (for HDF5 output).
   contains
     procedure :: init => timers_initialize
     procedure :: summary => timers_print_summary
     procedure :: print => timers_print_timer
     procedure :: start => timers_start_timer
     procedure :: update => timers_update_timer
     procedure :: sum => timers_sum_timer
  end type nx_timers_t
  
  character(len=24), public, parameter :: T_QM_EXE = 't_qm_exe'
  character(len=24), public, parameter :: T_QM_READ = 't_qm_read'

contains

  ! ===========================================
  ! NX_TIMERS_T (COLLECTION OF TIMERS) ROUTINES
  ! ===========================================
  subroutine timers_initialize(self, &
       & with_cio, with_sh, with_auxnac, with_io_txt, &
       & with_io_h5)
    class(nx_timers_t), intent(inout) :: self
    logical, intent(in), optional :: with_cio
    logical, intent(in), optional :: with_sh
    logical, intent(in), optional :: with_auxnac
    logical, intent(in), optional :: with_io_txt
    logical, intent(in), optional :: with_io_h5

    logical :: cio, sh, auxnac, io_txt, io_h5
    cio = .true.
    sh = .true.
    auxnac = .true.
    io_txt = .true.
    io_h5 = .true.
    if (present(with_cio)) cio = with_cio
    if (present(with_sh)) sh = with_sh
    if (present(with_auxnac)) auxnac = with_auxnac
    if (present(with_io_txt)) io_txt = with_io_txt
    if (present(with_io_h5)) io_h5 = with_io_h5

    ! First, create all timers
    self%t_main = nx_timer_t()
    self%t_qm_exe = nx_timer_t()
    self%t_qm_read = nx_timer_t()
    self%t_sh = nx_timer_t()
    self%t_cio = nx_timer_t()
    self%t_auxnac = nx_timer_t()
    self%t_io_txt = nx_timer_t()
    self%t_io_h5 = nx_timer_t()

    call self%t_main%init()
    call self%t_qm_exe%init()
    call self%t_qm_read%init()

    if (sh) call self%t_sh%init()
    if (cio) call self%t_cio%init()
    if (auxnac) call self%t_auxnac%init()
    if (io_txt) call self%t_io_txt%init()
    if (io_h5) call self%t_io_h5%init()
  end subroutine timers_initialize


  subroutine timers_update_timer(self, name)
    class(nx_timers_t), intent(inout) :: self
    character(len=*), intent(in) :: name

    select case(name)
    case('main')
       call self%t_main%update()
    case('qm_exe')
       call self%t_qm_exe%update()
    case('qm_read')
       call self%t_qm_read%update()
    case('sh')
       call self%t_sh%update()
    case('cio')
       call self%t_cio%update()
    case('io_txt')
       call self%t_io_txt%update()
    case('auxnac')
       call self%t_auxnac%update()
    case('io_h5')
       call self%t_io_h5%update()
    end select
  end subroutine timers_update_timer


  subroutine timers_start_timer(self, name)
    class(nx_timers_t), intent(inout) :: self
    character(len=*), intent(in) :: name

    select case(name)
    case('main')
       call self%t_main%start()
    case('qm_exe')
       call self%t_qm_exe%start()
    case('qm_read')
       call self%t_qm_read%start()
    case('sh')
       call self%t_sh%start()
    case('cio')
       call self%t_cio%start()
    case('io_txt')
       call self%t_io_txt%start()
    case('auxnac')
       call self%t_auxnac%start()
    case('io_h5')
       call self%t_io_h5%start()
    end select
  end subroutine timers_start_timer


  function timers_print_timer(self, name) result(tt)
    class(nx_timers_t), intent(inout) :: self
    character(len=*), intent(in) :: name

    character(len=:), allocatable :: tt
    select case(name)
    case('main')
       tt = self%t_main%print()
    case('qm_exe')
       tt = self%t_qm_exe%print()
    case('qm_read')
       tt = self%t_qm_read%print()
    case('sh')
       tt = self%t_sh%print()
    case('cio')
       tt = self%t_cio%print()
    case('io_txt')
       tt = self%t_io_txt%print()
    case('auxnac')
       tt = self%t_auxnac%print()
    case('io_h5')
       tt = self%t_io_h5%print()
    end select
  end function timers_print_timer


  subroutine timers_print_summary(self, unit)
    class(nx_timers_t), intent(in) :: self
    integer, intent(in), optional :: unit

    integer :: u

    u = stdout
    if (present(unit)) u = unit

    write(u, *) ''
    write(u, '(a)') repeat('=', 80)
    write(u, '(a)') 'TIMINGS'
    write(u, *) ''

    call self%t_main%print_summary('Total wall time (s.)')
    write(u, *) ''

    if (self%t_io_h5%total > 0) &
         & call self%t_io_h5%print_summary('IO (HDF5)')

    if (self%t_io_txt%total > 0) &
         & call self%t_io_txt%print_summary('IO (TXT)')

    call self%t_qm_exe%print_summary('QM (execution)')
    call self%t_qm_read%print_summary('QM (processing)')

    if (self%t_sh%total > 0) &
         & call self%t_sh%print_summary('TDSE integration')

    if (self%t_cio%total > 0) &
         & call self%t_cio%print_summary('Time-derivatives')

    if (self%t_auxnac%total > 0) &
         & call self%t_auxnac%print_summary('Auxiliary NAD')

    write(u, *) ''
    write(u, '(a)') repeat('=', 80)
  end subroutine timers_print_summary


  function timers_sum_timer(self, names) result(res)
    class(nx_timers_t) :: self
    character(len=*) :: names(:)

    real(dp) :: res

    res = 0.0_dp

    if ( any( 'main' == names) ) res = res + self%t_main%time
    if ( any( 'qm_exe' == names) ) res = res + self%t_qm_exe%time
    if ( any( 'qm_read' == names) ) res = res + self%t_qm_read%time
    if ( any( 'sh' == names) ) res = res + self%t_sh%time
    if ( any( 'cio' == names) ) res = res + self%t_cio%time
    if ( any( 'auxnac' == names) ) res = res + self%t_auxnac%time
    if ( any( 'io_txt' == names) ) res = res + self%t_io_txt%time
    if ( any( 'io_h5' == names) ) res = res + self%t_io_h5%time
  end function timers_sum_timer


  ! ===========================================
  ! NX_TIMER_T ROUTINES
  ! ===========================================
  type(nx_timer_t) function timer_create()
    !! Create a new timer.
    !!
    !! All elements are set to -1.
    timer_create%count = -1
    timer_create%max = -1
    timer_create%time = -1
    timer_create%total = -1
  end function timer_create


  subroutine timer_init(timer)
    !! Initialize a timer type.
    !!
    !! The ``total`` member is set to 0, and the ``count`` and ``max`` member
    !! are set by a call to the ``system_clock`` subroutine.
    class(nx_timer_t), intent(inout) :: timer
    !! Timer object.

    call system_clock(timer%time, timer%count, timer%max)
    timer%total = 0
  end subroutine timer_init


  subroutine timer_start(timer)
    !! Start the timer.
    !!
    !! The ``time`` member is set by:
    !!
    !! ``call system_clock(timer%time)``
    !!
    class(nx_timer_t), intent(inout) :: timer
    !! Timer object.

    call system_clock(timer%time)
  end subroutine timer_start


  subroutine timer_update(timer)
    !! Update the timer.
    !!
    !! The current clock time is obtained, and the ``time`` member is updated
    !! accordingly: ``timer%time = clock - timer%time``.  The ``total``
    !! element is then updated by adding the current ``time`` value to it.
    class(nx_timer_t), intent(inout) :: timer
    !! Timer object.

    integer :: end
    call system_clock(end)
    timer%time = end - timer%time

    timer%total = timer%total + timer%time
  end subroutine timer_update


  function timer_convert_to_seconds(self) result(tt)
    class(nx_timer_t) :: self
    real(dp) :: tt

    tt = 1.0_dp*self%total / self%count
  end function timer_convert_to_seconds


  function timer_print(timer, fmt) result (tt)
    class(nx_timer_t) :: timer
    character(len=*), optional :: fmt
    character(len=:), allocatable :: tt

    character(len=MAX_STR_SIZE) :: prtfmt, tmp
    if (present(fmt)) then
       prtfmt = fmt
    else
       prtfmt = '(F6.3)'
    end if

    write(tmp, fmt=prtfmt) 1.0*timer%total / timer%count
    tt = trim(tmp)
  end function timer_print


  subroutine timer_print_summary_s(timer, label)
    !! Print the total time in seconds.
    !!
    class(nx_timer_t), intent(in) :: timer
    !! Timer object.
    character(len=*), intent(in) :: label
    !! Label of the given timer.

    character(len=20) :: t_char

    write(t_char, '(f12.3)') 1.0*timer%total / timer%count
    write(*, '(a30, a, a)') &
         & trim(label), ': ', adjustl(t_char)
  end subroutine timer_print_summary_s


  function time_r()
    !! Get the current wall time (in s.).
    !!
    !! This can be seen as a very basic equivalent to the C ``time()``
    !! function.
    real(dp) :: time_r

    integer :: t, count, max

    call system_clock(t, count, max)

    time_r = 1.0_dp * t / count
  end function time_r

end module mod_timers
