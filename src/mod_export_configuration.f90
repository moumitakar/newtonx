module mod_export_configuration
  use mod_input_parser, only: parser_t
  use mod_configuration, only: nx_config_t
  use mod_sh_t, only: nx_sh_t
  use mod_nad_setup, only: nx_nad_t
  use mod_cioverlap, only: nx_cioverlap_t
  use mod_aux_nad, only: nx_auxnac_t
  use mod_adaptive_step, only: nx_adaptive_ts_t
  use mod_thermostat, only: nx_thermo_t
  use mod_zpecorrect, only: nx_zpe_t
  use mod_qm_t, only: nx_qm_t
  use mod_constants, only: &
       & MAX_STR_SIZE, au2ev, timeunit, cm2au
  use mod_tools, only: to_str
  use mod_io, only: nx_output_t
  implicit none

  private

  public :: nx_export_config
contains

  subroutine nx_export_config(&
       & nxconfig, sh, nad, cio, auxnac, adt, thermo, &
       & zpe, qm, nx_out&
       & )
    type(nx_config_t), intent(inout) :: nxconfig
    type(nx_sh_t), intent(inout) :: sh
    type(nx_nad_t), intent(inout) :: nad
    type(nx_cioverlap_t), intent(inout) :: cio
    type(nx_auxnac_t), intent(inout) :: auxnac
    type(nx_adaptive_ts_t), intent(inout) :: adt
    type(nx_thermo_t), intent(inout) :: thermo
    type(nx_zpe_t), intent(inout) :: zpe
    type(nx_qm_t), intent(inout) :: qm
    type(nx_output_t), intent(inout) :: nx_out

    type(parser_t) :: exported

    character(len=256), parameter :: filename = 'nx_exported_config.nml'
    integer :: u

    call export_nxconfig(exported, nxconfig)
    call export_qm(exported, qm)
    
    if (nxconfig%thres > 0) call export_sh(exported, sh)
    if (nxconfig%thres > 0) call export_nad(exported, nad)

    if (nxconfig%dc_method == 2) call export_cio(exported, cio)
    if (nxconfig%dc_method == 3) call export_auxnad(exported, auxnac)
    if (nxconfig%with_adt) call export_adt(exported, adt)
    if (nxconfig%with_thermo) call export_thermo(exported, thermo)
    if (nxconfig%with_zpe) call export_zpe(exported, zpe)

    call export_nxout(exported, nx_out)
    open(newunit=u, file=filename, action='write')
    call exported%print(u)
    close(u)

    call exported%clean()
    ! call exported%print()
  end subroutine nx_export_config


  subroutine export_nxconfig(exported, nxconfig)
    type(parser_t), intent(inout) :: exported
    type(nx_config_t), intent(in) :: nxconfig

    ! Mandatory parameters
    call exported%set('nxconfig', 'nat', to_str(nxconfig%nat), 'Number of atoms')
    call exported%set('nxconfig', 'progname', nxconfig%progname)
    call exported%set('nxconfig', 'methodname', nxconfig%methodname)
    call exported%set('nxconfig', 'nstat', to_str(nxconfig%nstat))
    call exported%set('nxconfig', 'nstatdyn', to_str(nxconfig%nstatdyn))
    call exported%set('nxconfig', 'tmax', to_str(nxconfig%tmax))
    call exported%set('nxconfig', 'dc_method', to_str(nxconfig%dc_method))

    ! Log control
    call exported%set('nxconfig', 'lvprt', to_str(nxconfig%lvprt))
    call exported%set('nxconfig', 'kt', to_str(nxconfig%kt))
    call exported%set('nxconfig', 'save_cwd', to_str(nxconfig%save_cwd))

    ! Other parameters
    call exported%set('nxconfig', 'dt', to_str(nxconfig%dt * timeunit))
    call exported%set('nxconfig', 'thres', to_str(nxconfig%thres * au2ev))
    call exported%set('nxconfig', 'killstat', to_str(nxconfig%killstat))
    call exported%set('nxconfig', 'timekill', to_str(nxconfig%timekill))
    call exported%set('nxconfig', 'etot_jump', to_str(nxconfig%etot_jump * au2ev))
    call exported%set('nxconfig', 'etot_drift', to_str(nxconfig%etot_drift * au2ev))
    call exported%set('nxconfig', 'epot_diff', to_str(nxconfig%epot_diff * au2ev))
    call exported%set('nxconfig', 'thrs_cos', to_str(nxconfig%thrs_cos))
    call exported%set('nxconfig', 'thrs_norm', to_str(nxconfig%thrs_norm))

    ! Restart control
    call exported%set('nxconfig', 'init_time', to_str(nxconfig%init_time))
    call exported%set('nxconfig', 'init_step', to_str(nxconfig%init_step))
    call exported%set('nxconfig', 'nxrestart', to_str(nxconfig%nxrestart))

    ! Switches for different parts of the computations or options
    call exported%set('nxconfig', 'use_txt_outputs', to_str(nxconfig%use_txt_outputs))
    call exported%set('nxconfig', 'with_adt', to_str(nxconfig%with_adt))
    call exported%set('nxconfig', 'with_zpe', to_str(nxconfig%with_zpe))
    call exported%set('nxconfig', 'with_thermo', to_str(nxconfig%with_thermo))
    call exported%set('nxconfig', 'use_locdiab', to_str(nxconfig%use_locdiab))
    call exported%set('nxconfig', 'check_mo_ovl', to_str(nxconfig%check_mo_ovl))

    call exported%set('nxconfig', 'init_input', nxconfig%init_input)
    call exported%set('nxconfig', 'debug_path', nxconfig%debug_path)
    call exported%set('nxconfig', 'output_path', nxconfig%output_path)

    ! CS-FSSH
    call exported%set('nxconfig', 'run_complex', to_str(nxconfig%run_complex))
    call exported%set('nxconfig', 'gamma_model', to_str(nxconfig%gamma_model))
    call exported%set('nxconfig', 'path_gamma_model', nxconfig%path_gamma_model)
    call exported%set('nxconfig', 'same_mo', to_str(nxconfig%same_mo))
    call exported%set('nxconfig', 'ress_shift', to_str(nxconfig%ress_shift))
  end subroutine export_nxconfig


  subroutine export_sh(exported, sh)
    type(parser_t), intent(inout) :: exported
    type(nx_sh_t), intent(in) :: sh

    call exported%set('sh', 'integrator', to_str(sh%integrator))
    call exported%set('sh', 'ms', to_str(sh%ms))
    call exported%set('sh', 'seed', to_str(sh%seed))
    call exported%set('sh', 'phase', to_str(sh%phase))
    call exported%set('sh', 'nohop', to_str(sh%nohop))
    call exported%set('sh', 'forcesurf', to_str(sh%forcesurf))
    call exported%set('sh', 'getphase', to_str(sh%getphase))
    call exported%set('sh', 'nrelax', to_str(sh%nrelax))
    call exported%set('sh', 'tully', to_str(sh%tully))
    call exported%set('sh', 'decay', to_str(sh%decay))
    call exported%set('sh', 'probmin', to_str(sh%probmin))
    call exported%set('sh', 'mom', to_str(sh%mom))
    call exported%set('sh', 'adjmom', to_str(sh%adjmom))
    call exported%set('sh', 'adjtheta', to_str(sh%adjtheta))
    call exported%set('sh', 'popdev', to_str(sh%popdev))
    call exported%set('sh', 'decohmod', to_str(sh%decohmod))
    call exported%set('sh', 'decovlp', to_str(sh%decovlp))
    call exported%set('sh', 'thrwp', to_str(sh%thrwp))
    call exported%set('sh', 'iatau', to_str(sh%iatau))
    call exported%set('sh', 'nohop', to_str(sh%nohop))
  end subroutine export_sh


  subroutine export_nad(exported, nad)
    type(parser_t), intent(inout) :: exported
    type(nx_nad_t), intent(in) :: nad

    call exported%set('nad_setup', 'kross', to_str(nad%kross))
    call exported%set('nad_setup', 'cascade', to_str(nad%cascade))
    call exported%set('nad_setup', 'current', to_str(nad%current))
    call exported%set('nad_setup', 'never_state', to_str(nad%never_state))
    call exported%set('nad_setup', 'include_pair', to_str(nad%include_pair))
  end subroutine export_nad


  subroutine export_cio(exported, cio)
    type(parser_t), intent(inout) :: exported
    type(nx_cioverlap_t), intent(in) :: cio

    call exported%set('cioverlap', 'ci_cons', to_str(cio%ci_cons))
    call exported%set('cioverlap', 'ncore', to_str(cio%ncore))
    call exported%set('cioverlap', 'ndisc', to_str(cio%ndisc))
    call exported%set('cioverlap', 'ovl_prog', to_str(cio%ovl_prog))
    call exported%set('cioverlap', 'coptda', to_str(cio%coptda))
    call exported%set('cioverlap', 'blasthread', to_str(cio%blasthread))
    call exported%set('cioverlap', 'read_ovl_matrix', cio%read_ovl_matrix)
    call exported%set('cioverlap', 'generate_wf', to_str(cio%generate_wf))
    call exported%set('cioverlap', 'cisc_options', cio%cisc_options)
    call exported%set('cioverlap', 'cio_options', cio%cio_options)
  end subroutine export_cio


  subroutine export_auxnad(exported, auxnad)
    type(parser_t), intent(inout) :: exported
    type(nx_auxnac_t), intent(in) :: auxnad

    call exported%set('auxnac', 'model', to_str(auxnad%model))
    call exported%set('auxnac', 'ba_smooth', to_str(auxnad%ba_smooth))
    call exported%set('auxnac', 'ba_dh', to_str(auxnad%ba_dh))
    call exported%set('auxnac', 'ba_de', to_str(auxnad%ba_de * au2ev))
    call exported%set('auxnac', 'ba_dv', to_str(auxnad%ba_dv))
    call exported%set('auxnac', 'lvprt', to_str(auxnad%lvprt))
  end subroutine export_auxnad


  subroutine export_adt(exported, adt)
    type(parser_t), intent(inout) :: exported
    type(nx_adaptive_ts_t), intent(in) :: adt

    call exported%set('adapt_dt', 'model', to_str(adt%model))
    call exported%set('adapt_dt', 'ratio', to_str(adt%ratio))
    call exported%set('adapt_dt', 'max_subtraj', to_str(adt%max_subtraj))
  end subroutine export_adt


  subroutine export_thermo(exported, thermo)
    type(parser_t), intent(inout) :: exported
    type(nx_thermo_t), intent(in) :: thermo

    call exported%set('thermostat', 'ktherm', to_str(thermo%ktherm))
    call exported%set('thermostat', 'kts', to_str(thermo%kts))
    call exported%set('thermostat', 'lts', to_str(thermo%lts))
    call exported%set('thermostat', 'nstherm', to_str(thermo%nstherm))
    call exported%set('thermostat', 'temp', to_str(thermo%temp))
    call exported%set('thermostat', 'gamma', to_str(thermo%gamma))
    call exported%set('thermostat', 'seed', to_str(thermo%seed))
    call exported%set('thermostat', 'lvp', to_str(thermo%lvp))
  end subroutine export_thermo


  subroutine export_zpe(exported, zpe)
    type(parser_t), intent(inout) :: exported
    type(nx_zpe_t), intent(in) :: zpe

    call exported%set('zpe_correction', 'kmodel', to_str(zpe%kmodel ))
    call exported%set('zpe_correction', ' kcheck', to_str(zpe%kcheck ))
    call exported%set('zpe_correction', 'tcheck', to_str(zpe%tcheck ))
    call exported%set('zpe_correction', 'tcycle', to_str(zpe%tcycle ))
    call exported%set('zpe_correction', 'zthresh', to_str(zpe%zthresh))
    call exported%set('zpe_correction', 'biascorrect', to_str(zpe%biascorrect))
  end subroutine export_zpe


  subroutine export_qm(exported, qm)
    type(parser_t), intent(inout) :: exported
    type(nx_qm_t), intent(in) :: qm

    if (qm%qmcode == 'columbus') then
       call exported%set('columbus', 'mem', to_str(qm%col_mem))
       call exported%set('columbus', 'cirestart', to_str(qm%col_cirestart))
       call exported%set('columbus', 'reduce_tol', to_str(qm%col_reduce_tol))
       call exported%set('columbus', 'citol', to_str(qm%col_citol))
       call exported%set('columbus', 'quad_conv', to_str(qm%col_quad_conv))
       call exported%set('columbus', 'mc_conv', to_str(qm%col_mc_conv))
       call exported%set('columbus', 'ci_conv', to_str(qm%col_ci_conv))
       call exported%set('columbus', 'ivmode', to_str(qm%col_ivmode))
       call exported%set('columbus', 'mocoef', to_str(qm%col_mocoef))
       call exported%set('columbus', 'ci_iter', to_str(qm%col_ci_iter))
       call exported%set('columbus', 'all_grads', to_str(qm%col_all_grads))
       call exported%set('columbus', 'prt_mo', to_str(qm%col_prt_mo))
       call exported%set('columbus', 'grad_lvl', to_str(qm%col_grad_lvl))

    else if (qm%qmcode == 'gaussian') then
       call exported%set('gaussian', 'mocoef', to_str(qm%gau_mocoef))
       call exported%set('gaussian', 'prt_mo', to_str(qm%gau_prt_mo))
       call exported%set('gaussian', 'td_st', to_str(qm%gau_td_st))
       call exported%set('gaussian', 'kind_g09', to_str(qm%gau_type))
       call exported%set('gaussian', 'ld_thr', to_str(qm%gau_ld_thr))

    else if (qm%qmcode == 'orca') then
       call exported%set('orca', 'is_tda', to_str(qm%orca_is_tda))
       call exported%set('orca', 'mocoef', to_str(qm%orca_mocoef))
       call exported%set('orca', 'prt_mo', to_str(qm%orca_prt_mo))

    else if (qm%qmcode == 'turbomole') then
       call exported%set('turbomole', 'nnodes', to_str(qm%tm_nnodes))
       call exported%set('turbomole', 'multiplicity', to_str(qm%tm_mult))
       call exported%set('turbomole', 'npre', to_str(qm%tm_npre))
       call exported%set('turbomole', 'nstart', to_str(qm%tm_nstart))

    else if (qm%qmmethod == 'sbh') then
          call exported%set('sbh', 'e0', to_str(qm%sbh%e0 / cm2au))
          call exported%set('sbh', 'v0', to_str(qm%sbh%v0 / cm2au))
          call exported%set('sbh', 'jw', qm%sbh%jw)
          call exported%set('sbh', 'wc', to_str(qm%sbh%wc / cm2au))
          call exported%set('sbh', 'wmax', to_str(qm%sbh%wmax / cm2au))
          call exported%set('sbh', 'xi', to_str(qm%sbh%xi))
          call exported%set('sbh', 'er', to_str(qm%sbh%er / cm2au))

       else if (qm%qmmethod == 'onedim_model') then
          call exported%set('onedim_model', 'onedim_mod', to_str(qm%onedim%onedim_mod))

       else if (qm%qmmethod == 'recohmodel') then
          call exported%set('recohmodel', 'modelnum', to_str(qm%recohmod%mnum))
          call exported%set('recohmodel', 'A', to_str(qm%recohmod%a))
          call exported%set('recohmodel', 'B', to_str(qm%recohmod%b))
          call exported%set('recohmodel', 'C', to_str(qm%recohmod%c))

    else if (qm%qmcode == 'exc_gaussian') then
       call exported%set('exc_gaussian', 'nchrom', to_str(qm%nchromexc))
       call exported%set('exc_gaussian', 'nproc', to_str(qm%nprocexc))
       call exported%set('exc_gaussian', 'nstattd', to_str(qm%nstattdexc))
       call exported%set('exc_gaussian', 'nproc_gau', to_str(qm%nproc_gauexc))
       call exported%set('exc_gaussian', 'functional', qm%functionalexc)
       call exported%set('exc_gaussian', 'basis', qm%basisexc)
       call exported%set('exc_gaussian', 'force_field', qm%force_fieldexc)
       call exported%set('exc_gaussian', 'verbose', to_str(qm%verboseexc))

       call exported%set('exc_inp', 'tresp ', to_str(qm%trespexc))
       call exported%set('exc_inp', 'gs', to_str(qm%gsexc))
       call exported%set('exc_inp', 'dip', to_str(qm%dipexc))
       call exported%set('exc_inp', 'nat_array', to_str(qm%nat_arrayexc))
       call exported%set('exc_inp', 'nstat', to_str(qm%nstatexc))
       call exported%set('exc_inp', 'coup_file', qm%coup_fileexc)

    else if (qm%qmcode == 'exc_mopac') then
       call exported%set('exc_mopac', 'nchrom', to_str(qm%nchromexc))
       call exported%set('exc_mopac', 'nproc', to_str(qm%nprocexc))
       call exported%set('exc_mopac', 'gen_file', to_str(qm%gen_fileexc))
       call exported%set('exc_mopac', 'verbose', to_str(qm%verboseexc))

       call exported%set('exc_inp', 'tresp ', to_str(qm%trespexc))
       call exported%set('exc_inp', 'gs', to_str(qm%gsexc))
       call exported%set('exc_inp', 'dip', to_str(qm%dipexc))
       call exported%set('exc_inp', 'nat_array', to_str(qm%nat_arrayexc))
       call exported%set('exc_inp', 'nstat', to_str(qm%nstatexc))
       call exported%set('exc_inp', 'coup_file', qm%coup_fileexc)

    else if (qm%qmcode == 'mopac') then
       call exported%set('mopac', 'verbose', to_str(qm%verboseexc))

    else if (qm%qmcode == 'tinker_mndo') then
       call exported%set('tinker_mndo', 'interface_path', qm%tmndo_interface)
       call exported%set('tinker_mndo', 'nac_mm', to_str(qm%compute_nac_mm ))

    else if (qm%qmcode == 'tinker_g16mmp') then
       call exported%set('tinker_g16mmp', 'interface_path', qm%tmndo_interface)
       call exported%set('tinker_g16mmp', 'nac_mm', to_str(qm%compute_nac_mm ))

    else if (qm%qmcode == 'mlatom') then
       call exported%set('mlatom', 'use_nx_interface', to_str(qm%mlat_use_nx_interface))
       call exported%set('mlatom', 'mlat_exe', trim(qm%mlat_exe))
    end if

  end subroutine export_qm


  subroutine export_nxout(exported, nx_out)
    type(parser_t), intent(inout) :: exported
    type(nx_output_t), intent(inout) :: nx_out

    call exported%set('nx_output', 'prt_geom', to_str(nx_out%prt_geom))
    call exported%set('nx_output', 'prt_veloc', to_str(nx_out%prt_veloc))
    call exported%set('nx_output', 'prt_grad', to_str(nx_out%prt_grad))
    call exported%set('nx_output', 'prt_epot', to_str(nx_out%prt_epot))
    call exported%set('nx_output', 'prt_ekin', to_str(nx_out%prt_ekin))
    call exported%set('nx_output', 'prt_osc', to_str(nx_out%prt_osc))
    call exported%set('nx_output', 'prt_etot', to_str(nx_out%prt_etot))
    call exported%set('nx_output', 'prt_nstatdyn', to_str(nx_out%prt_nstatdyn))
    call exported%set('nx_output', 'prt_nad', to_str(nx_out%prt_nad))
    call exported%set('nx_output', 'prt_wf', to_str(nx_out%prt_wf))
    call exported%set('nx_output', 'prt_shprob', to_str(nx_out%prt_shprob))
    call exported%set('nx_output', 'prt_shrx', to_str(nx_out%prt_shrx))
    call exported%set('nx_output', 'prt_shseed', to_str(nx_out%prt_shseed))
    call exported%set('nx_output', 'prt_inad', to_str(nx_out%prt_inad))
    call exported%set('nx_output', 'prt_gamma', to_str(nx_out%prt_gamma))
    call exported%set('nx_output', 'prt_diab_ham', to_str(nx_out%prt_diab_ham))
    call exported%set('nx_output', 'prt_diab_en', to_str(nx_out%prt_diab_en))
    call exported%set('nx_output', 'prt_diab_pop', to_str(nx_out%prt_diab_pop))
    call exported%set('nx_output', 'prt_angmom', to_str(nx_out%prt_angmom))
    call exported%set('nx_output', 'prt_linmom', to_str(nx_out%prt_linmom))
  end subroutine export_nxout
  
end module mod_export_configuration
