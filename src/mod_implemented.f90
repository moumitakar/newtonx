! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_implemented
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Module defining the available interfaces in Newton-X
  !!
  !! ## Usage
  !!
  !! ```fortran
  !! type(nx_interfaces_t) :: int
  !! integer :: ierr
  !!
  !! int = nx_interfaces_t()
  !!
  !! ! Returns -1 if `mrci` is not defined with 'columbus'
  !! ierr = int%has_method( 'columbus', 'mrci' ) 
  !! if (ierr < 0) then
  !!     do stuff ...
  !! fi
  !! ```
  !!
  !! ## Description
  !!
  !! The aim of this module is to make clear for the rest of the program the different
  !! combinations of ``progname`` and ``methodname`` that are available.  It is used, for
  !! instance, for validating a configuration file (see ``[[mod_configuration]]``).
  !!
  !! To include a new interface, one has to :
  !!
  !! 1. increment the ``NPROGS`` parameter in this file ;
  !! 2. add a line in the function ``[[interface_create]]`` with the content:
  !!
  !!        res%progs(new_nprogs) = nx_prog_t( 'progname', ['method1', 'method2'])
  !!
  !! ## Already implemented interfaces / methods:
  !!
  !! - ``turbomole``: ``tddft, adc2, ricc2``
  !! - ``columbus``: ``mcscf, mrci``
  !! - ``gaussian``: ``tddft``
  !! - ``analytical models``: ``sbh, recohmodel, onedim_model, con_int, cs_fssh``
  !! - ``tinker_g16mmp``: ``g16mmpol``
  !! - ``tinker_mndo``: ``mndo``
  !! - ``mopac``: ``fomo-ci``
  !! - ``exc_mopac``: ``mopac-fomo-ci``
  !! - ``exc_gaussian``: ``gaussian-tddft``
  !! - ``external``: ``user_defined``
  !! - ``mlatom``
  !! - ``orca``: ``tddft``
  implicit none

  integer, parameter :: NPROGS = 12

  type :: nx_prog_t
     !! Define a program, and all its implemented methods.
     !!
     character(len=256) :: name
     !! Corresponds to ``progname``
     character(len=256), allocatable :: methods(:)
     !! Array containing the ``methodname`` available for the program.
  end type nx_prog_t
  interface nx_prog_t
     module procedure prog_create
  end interface nx_prog_t
  
  type, public :: nx_interfaces_t
     !! Collection of ``nx_prog_t``.
     !!
     type(nx_prog_t) :: progs(NPROGS)
   contains
     procedure :: has_prog => interface_has_program
     procedure :: has_method => interface_has_method
     procedure :: print_progs => interface_print_programs
  end type nx_interfaces_t
  interface nx_interfaces_t
     module procedure interface_create
  end interface nx_interfaces_t

contains

  function interface_create() result(res)
    type(nx_interfaces_t) :: res

    res%progs(1)  = nx_prog_t('columbus',        ['mcscf', 'mrci '])
    res%progs(2)  = nx_prog_t('turbomole',       ['tddft', 'adc2 ', 'ricc2'])
    res%progs(3)  = nx_prog_t('gaussian',        ['tddft'])
    res%progs(4)  = nx_prog_t('analytical',&
         &       ['sbh         ', 'recohmodel  ', 'onedim_model', 'con_int     ', &
         &        'cs_fssh     '])
    res%progs(5)  = nx_prog_t('tinker_g16mmp',   ['g16mmp'])
    res%progs(6)  = nx_prog_t('tinker_mndo',     ['mndo'])
    res%progs(7)  = nx_prog_t('mopac',           ['fomo-ci'])
    res%progs(8)  = nx_prog_t('exc_mopac',       ['mopac-fomo-ci'])
    res%progs(9)  = nx_prog_t('exc_gaussian',    ['gaussian-tddft'])
    res%progs(10) = nx_prog_t('external',        [''])
    res%progs(11) = nx_prog_t('mlatom',          [''])
    res%progs(12) = nx_prog_t('orca',            ['tddft'])
  end function interface_create


  function interface_has_program(self, name) result(res)
    class(nx_interfaces_t), intent(in) :: self
    character(len=*), intent(in) :: name

    integer :: res

    integer :: i

    do i=1, NPROGS
       if (self%progs(i)%name == name) then
          res = i
          return
       end if
    end do
    res = -1
  end function interface_has_program


  function interface_has_method(self, prog, method) result(res)
    class(nx_interfaces_t), intent(in) :: self
    character(len=*), intent(in) :: prog
    character(len=*), intent(in) :: method

    integer :: res

    integer :: i, prog_index

    prog_index = self%has_prog(prog)
    if (prog_index < 0) then
       res = -1
       return
    end if

    if (prog == 'external' .or. prog == 'mlatom') then
       res = 1
       return
    end if

    do i=1, size( self%progs(prog_index)%methods )
       if (self%progs(prog_index)%methods(i) == method) then
          res = 1
          return
       end if
    end do
    res = -1
  end function interface_has_method


  function interface_print_programs(self) result(res)
    class(nx_interfaces_t), intent(in) :: self
    ! integer, intent(in) :: out

    character(len=:), allocatable :: res

    character(len=4096) :: tmp, tmp2
    character(len=*), parameter :: nl = NEW_LINE('c')
    integer :: i, j

    tmp = nl//repeat('-', 20)//'+'//repeat('-', 59)//nl
    write(tmp, '(A,A20,A,A59,A)') trim(tmp), adjustl('Available "progname"'), '|', 'Available "methodname"'//nl
    write(tmp, '(A,A20,A,A59,A)') trim(tmp), repeat('-', 20), '+', repeat('-', 59), nl
    do i=1, size( self%progs )
    
       tmp2 = self%progs(i)%methods(1)
       do j=2, size( self%progs(i)%methods )
          tmp2 = trim(tmp2)//', '//trim(self%progs(i)%methods(j))
       end do
       
       write(tmp, '(A,2X,A18,A,A59,A)') trim(tmp), trim(self%progs(i)%name), '|', trim(tmp2), nl
    end do
    
    write(tmp, '(A,A20,A,A59)') trim(tmp), repeat('-', 20), '-', repeat('-', 59)
    res = trim(tmp)
  end function interface_print_programs
  

  function prog_create(name, methods) result(res)
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: methods(:)

    type(nx_prog_t) :: res

    integer :: i

    res%name = name
    allocate(res%methods(size(methods)))
    do i=1, size(methods)
       res%methods(i) = methods(i)
    end do
  end function prog_create


  subroutine prog_destroy(prog)
    type(nx_prog_t), intent(inout) :: prog

    if (allocated(prog%methods)) deallocate(prog%methods)
  end subroutine prog_destroy

end module mod_implemented
