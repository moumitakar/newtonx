program nx_run_tests
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Testsuite handler for Newton-X.
  !!
  !! This program is used to run tests for the Newton-X programm, by running the example
  !! trajectories defined in the ``examples`` folder.  It is possible to run only a
  !! subset of the tests defined, with command line arguments (see ``[[usage]]``).
  !!
  !! The main functionalities are all defined and documented in the ``[[mod_nx_tests]]``
  !! module.
  use mod_kinds, only: dp
  use mod_constants, only: MAX_STR_SIZE
  use mod_nx_tests, only: main
  use mod_tools, only: &
       & title_message, to_str
  use mod_timers, only: time_r
  use iso_fortran_env, only: stdout => output_unit

  implicit none

  character(len=*), parameter :: THIS_NAME = 'nx_test'

  integer :: nargs, iarg, ierr
  character(len=MAX_STR_SIZE) :: arg, env
  character(len=:), allocatable :: progs, tests, mdexe, dir
  integer :: make_env
  logical :: skip_next
  real(dp) :: time

  time = time_r()

  call get_environment_variable('NXHOME', value=env)
  mdexe = trim(env)//'/bin/nx_moldyn'
  tests = ''
  progs = ''
  dir = './tests'
  make_env = 0

  nargs = command_argument_count()

  skip_next = .false.
  do iarg = 1, nargs

     call get_command_argument(iarg, arg)
     if (len_trim(arg) == 0) exit

     if (skip_next) then
        skip_next = .false.
        cycle
        
     
     else if ((arg == '--help') .or. (arg == '-h')) then
        call usage()
        stop
     
     else if ((arg == '--prog') .or. (arg == '-p')) then
        call get_command_argument(iarg + 1, arg)
        progs = trim(arg)
        skip_next = .true.
     
     else if ((arg == '--tests') .or. (arg == '-t')) then
        call get_command_argument(iarg + 1, arg)
        tests = trim(arg)
        skip_next = .true.
     
     else if ((arg == '--mdexe') .or. (arg == '-m')) then
        call get_command_argument(iarg + 1, arg)
        mdexe = trim(arg)
        skip_next = .true.
     
     else if ((arg == '--dir') .or. (arg == '-d')) then
        call get_command_argument(iarg + 1, arg)
        dir = trim(arg)
        skip_next = .true.
     
     else if (arg == '--make_env') then
        make_env = 1
     
     else
        write(*, '(A)') 'Unrecognized option: '//trim(arg)
        write(*, '(A)') ''
        call usage()
        stop
     end if
  end do

  call title_message(stdout)
  write(stdout, '(A)') '** RUNNING NEWTONX TESTSUITE **'
  write(stdout, '(A)') ''

  if (tests /= '' .and. progs == '') then
     write(*, '(A)') 'ERROR: Cannot specify tests to run without specifying a program !'
     error stop
  end if

  ierr = main(progs, tests, mdexe, dir, make_env)

  print *, ''
  print *, 'Time elapsed: '//to_str(time_r() - time, fmt='(F10.3)')//' s.'
  print *, ''
  if (ierr /= 0 .and. ierr /= 77) then
     print *, 'ABNORMAL TERMINATION OF TESTSUITE'
     stop 1
  else if (ierr == 77) then
     print *, 'Test SKIPPED'
     stop 77
  else
     print *, 'Normal termination of testsuite'
  end if
  
contains

  subroutine usage(output)
    integer, intent(in), optional :: output

    integer :: out

    character(len=*), parameter :: nl = NEW_LINE('c')

    out = stdout
    if (present(output)) out = output

    write(out, '(A)') 'Usage:'
    write(out, '(A)') '  '//THIS_NAME//' [-p prog_list [-t test_list]]'//&
                      '[-m path_to_executable] [-d dir] [--make_env]'
    write(out, '(A)') ''
    write(out, '(A)') 'Options:'
    write(out, '(A)') '  -h, --help      Print this help.'
    write(out, '(A)') '  -p, --prog      Comma-separated list of interfaces for which'
    write(out, '(A)') '                   to run the tests.  If left empty, all interfaces'
    write(out, '(A)') '                   present in $NXHOME/examples/ will be tested.'
    write(out, '(A)') '  -t, --tests     List of tests to run for each interface. The'
    write(out, '(A)') '                   tests must be comma-separated, and the tests '
    write(out, '(A)') '                   for different programs are sperated by ''|''.'
    write(out, '(A)') '                   For instance, to run the test 1 for columbus'
    write(out, '(A)') '                   interface and 1 and 2 for turbomole interface,'
    write(out, '(A)') '                   the following line commands must be used: '
    write(out, '(A)') '                   -p turbomole,columbus -t "01|01,02".'
    write(out, '(A)') '                   This option can be used ONLY WHEN a list '
    write(out, '(A)') '                   of programs is specified.'
    write(out, '(A)') '  -m, --mdexe     Full path to the program used for running '
    write(out, '(A)') '                   trajectories.'
    write(out, '(A)') '                   Default: $NXHOME/bin/md'
    write(out, '(A)') '  -d, --dir       Directory where the tests will be run.'
    write(out, '(A)') '                   Default: "./tests/"'
    write(out, '(A)') '  --make_env      Indicate if the this program is run via '
    write(out, '(A)') '                   "make check".'
    write(out, '(A)') ''
    write(out, '(A)') '--------------- Examples ---------------'
    write(out, '(A)') ''
    write(out, '(A)') '  1) Run all tests:'
    write(out, '(A)') ''
    write(out, '(A)') '       '//THIS_NAME
    write(out, '(A)') ''
    write(out, '(A)') '  2) Run all tests for interface COLUMBUS only:'
    write(out, '(A)') ''
    write(out, '(A)') '       '//THIS_NAME//' -p columbus'
    write(out, '(A)') ''
    write(out, '(A)') '  3) Run tests 01, 02 and 03 for interface COLUMBUS:'
    write(out, '(A)') ''
    write(out, '(A)') '       '//THIS_NAME//' -p columbus -t "1,2,3"'
    write(out, '(A)') ''
    write(out, '(A)') '  4) Run tests 01, 02 and 03 for interface COLUMBUS, and'
    write(out, '(A)') '     01 and 04 for TURBOMOLE:'
    write(out, '(A)') ''
    write(out, '(A)') '       '//THIS_NAME//' -p columbus,turbomole -t "1,2,3|1,4"'
    write(out, '(A)') ''
  end subroutine usage

end program nx_run_tests
