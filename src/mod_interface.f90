! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_interface
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2022-06-23
  !!
  !! # Interfaces to C-function and coreutils programs
  !!
  !! This module offers interfaces (functions and subroutines) to several C-functions and
  !! coreutils programs for easier integration in Newton-X.
  !!
  !! ## C-functions
  !!
  !! Interfaces are provided with the following C-functions, as functions with the same name:
  !!
  !! - ``mkdir``
  !! - ``setenv``
  !! - ``opendir
  !! - ``readdir``
  !! - ``closedir``
  !!
  !! A subroutine ``get_list_of_files`` is also available, that wraps the ``opendir``,
  !! ``readdir`` and ``closedir`` functions, to obtain directly a list of files from a
  !! given directory (see [get_list_of_files]).  The conversion to C-string is
  !! automatically handled by the module.
  !!
  !! ## Coreutils programs
  !!
  !! These interfaces are obtained to build a command line from a list of arguments, and
  !! by executing this command with the ``execute_command_line`` routine.  The return
  !! value is then obtained from the ``exitstat`` flag.
  !!
  !! Most functions will take an array of strings as their first argument, and the
  !! command will be built by concatenating all the alements together.
  use iso_c_binding, only: c_int, c_int16_t, c_char, c_ptr, c_int64_t, c_int8_t,&
       & c_f_pointer, c_associated, c_null_char
  implicit none

  private

  public :: mkdir
  public :: setenv
  public :: opendir, readdir, closedir, get_list_of_files
  public :: getpid, fork, wait
  public :: to_c_str

  interface copy
     !! Interface to the ``cp`` program.
     !!
     !! Three modes are provided:
     !!
     !! - Copy one file (see (``copy_as``)[copy_as]);
     !! - Copy a list of files to a given directory (see (``copy_list``)[copy_list]);
     !! - Copy a list of files with names given in another list (see (``copy_multiple``)[copy_multiple]);
     module procedure copy_list
     module procedure copy_as
     module procedure copy_multiple
  end interface copy
  public :: copy

  interface rm
     !! Interface to the ``rm`` program.
     !!
     !! The command is constructed exactly as given by the first argument, with the
     !! ``-rf`` flag, and shell expansion will be taken care of by the shell.
     module procedure rm_files_and_dir
  end interface rm
  public :: rm

  interface gzip
     !! Interface to the ``gzip`` program.
     !!
     module procedure f_gzip
  end interface gzip
  public :: gzip

  interface nx_chmod
     module procedure chmod_list_of_files
     module procedure chmod_recursive
  end interface nx_chmod
  public :: nx_chmod


  type, bind(c), public :: c_dirent
     !! Binds to the C type ``dirent`` (see ``readdir(3)`` documentation).
     !!
     !! This type is used in the ``c_readdir`` interface function (see
     !! (``readdir``)[readdir] function), to convert the pointer returned from the C
     !! ``readdir`` function to a Fortran type.
     integer(c_int64_t) :: d_ino
     !! Inode number.
     integer(c_int64_t) :: c_stream
     !! Unclear, even from the documentation. Treated as the stream value.
     integer(c_int16_t) :: c_reclen
     !! Length of the record.
     integer(c_int8_t) :: c_type
     !! Type of file.
     character(c_char) :: c_name(256)
     !! Null-terminated filename.
  end type c_dirent

  integer, parameter :: MAX_FILES = 4000
  !! Maximum number of files read from a directory (used in ``get_list_of_files``
  !! routine).

contains

  function setenv(name, value)
    !! Set an environment variable.
    !!
    !! The environement variable ``name`` is set to ``value``.  The routine provides an
    !! interface to the C ``setenv`` function, and has the same return values.  If the
    !! environment value already exists, it is always overwritten by this function.
    use iso_c_binding, only: c_int
    character(len=*) :: name
    !! Environment to set.
    character(len=*) :: value
    !! New value of the environment variable.
    integer(c_int), parameter :: overwrite = 1

    integer :: setenv

    character(len=len(name)+1) :: tmp1
    character(len=len(value)+1) :: tmp2
    interface
       function c_setenv(c_name, c_value, c_overwrite) bind(C, name='setenv')
         use iso_c_binding, only: c_int, c_char
         implicit none
         integer(C_int) :: c_setenv
         character(kind=C_char), intent(in) :: c_name(*)
         character(kind=C_char), intent(in) :: c_value(*)
         integer(C_int), value :: c_overwrite
       end function c_setenv
    end interface

    tmp1 = to_c_str(name)
    tmp2 = to_c_str(value)

    setenv = int(c_setenv(tmp1, tmp2, overwrite))
  end function setenv


  function mkdir(path)
    !! Create the given directory.
    !!
    !! The directory is always created with permission ``700``.  The function provides an
    !! interface to the ``mkdir`` C function, and has the same return values.
    character(len=*) :: path
    !! Name of the directory to create.

    integer :: mkdir
    integer(c_int16_t), parameter :: mode = int(o'700', c_int16_t)

    character(len=len(path)+1) :: tmp
    interface
       function c_mkdir(c_path, c_mode) bind(C, name='mkdir')
         import c_int, c_int16_t, c_char
         implicit none
         integer(c_int) :: c_mkdir
         character(kind=c_char, len=1) :: c_path(*)
         integer(c_int16_t), value :: c_mode
       end function c_mkdir
    end interface

    ! tmp = to_c_str(path)

    ! mkdir = int(c_mkdir(tmp, mode))
    call execute_command_line("mkdir -p "//trim(path), exitstat=mkdir)
  end function mkdir


  ! Directories
  subroutine get_list_of_files(path, filelist, ierr, exclude, full_path)
    !! Get the list of files in a directory.
    !!
    !! This routine will read the content of the directory ``path``, and populate
    !! ``filelist`` with the name of the files and directories present inside.  The
    !! indicator for current directory (``.``) and parent directory (``..``) are excluded
    !! automatically.
    !!
    !! Optionally, it is possible to give as ``exclude`` argument an array of strings
    !! consisting of patterns that should not be saved in the final ``filelist`` array.
    !! The matching is done for each element of the ``path`` directory by the ``index``
    !! Fortran function, so all elements from ``path`` that contain any element of
    !! ``exclude`` will be excluded.
    !!
    !! It is also possible to require that ``filelist`` contains not only the names of
    !! the element, but their full path, /i.e/ the ``path`` argument is appended in front
    !! of every element of ``filelist``.
    character(len=*), intent(in) :: path
    !! Directory to read.
    character(len=256), allocatable, intent(inout) :: filelist(:)
    !! Array of files selected from the directory ``path``.
    integer, intent(inout) :: ierr
    !! Return value:
    !!
    !! - 0: correct execution ;
    !! - 1: error in ``opendir``;
    !! - 3: error in ``closedir``;
    character(len=*), optional, intent(in) :: exclude(:)
    !! Array of elements that shouldn't be included in ``filelist``.
    logical, intent(in), optional :: full_path
    !! Append the name ``path`` to the elements of ``filelist`` (default: ``.false.``).

    type(c_ptr) :: stream
    character(len=256), allocatable :: tmplist(:)
    character(len=256) :: buf
    integer :: nfiles, i
    logical :: excl, full
    character(len=:), allocatable :: path_to_open

    allocate( tmplist(MAX_FILES) )

    i = len_trim(path)
    if ( path(len_trim(path):len_trim(path)) /= '/') then
       allocate( character(len=i+1) :: path_to_open )
       path_to_open(1:i) = path(1:i)
       path_to_open(i+1:i+1) = '/'
    else
       path_to_open = trim(path)
    end if

    call opendir(path_to_open, stream, ierr)
    if (ierr /= 0) then
       ierr = 1
       return
    end if

    if (allocated(filelist)) deallocate(filelist)
    nfiles = 0
    full = .false.
    if (present(full_path)) full = .true.
    GET_LIST: do
       call readdir(stream, buf, ierr)
       if (ierr == 1) exit
       if ((buf == '.') .or. (buf == '..')) then
          continue
       else
          excl = .false.

          MATCH_LOOP: if (present(exclude)) then
             do i=1, size(exclude)
                if (index(buf, trim(exclude(i))) /= 0) then
                   excl = .true.
                   exit MATCH_LOOP
                end if
             end do
          end if MATCH_LOOP

          if (.not. excl) then
             nfiles = nfiles + 1
             if (full) then
                tmplist(nfiles) = trim(path_to_open)//trim(buf)
             else
                tmplist(nfiles) = buf
             end if
          end if
       end if
    end do GET_LIST

    if (nfiles > 0) then
       allocate(filelist(nfiles))
       do i=1, nfiles
          filelist(i) = tmplist(i)
       end do
    end if

    call closedir(stream, ierr)
    if (ierr /= 0) then
       ierr = 3
       return
    end if
  end subroutine get_list_of_files


  subroutine opendir(name, stream, ierr)
    !! Open the given directory.
    !!
    !! This function is an interface to the ``opendir`` C function, and has the same
    !! return values.
    character(len=*), intent(in) :: name
    !! Directory to open.
    type(c_ptr), intent(inout) :: stream
    !! C pointer to the opened directory.
    integer, intent(inout) :: ierr
    !! Return code.

    character(len=len(name)+1) :: tmp
    type(c_ptr) :: test

    interface
       function c_opendir(c_name) bind(C, name='opendir')
         import c_char, c_ptr
         implicit none
         type(c_ptr) :: c_opendir
         character(len=1, kind=c_char) :: c_name(*)
       end function c_opendir
    end interface

    tmp = to_c_str(name)
    test = c_opendir(tmp)

    if ( c_associated(test) ) then
       stream = test
       ierr = 0
    else
       ierr = ierrno()
    end if
  end subroutine opendir


  subroutine readdir(stream, filename, ierr)
    !! Read the given stream.
    !!
    !! This function is an interface to the ``readdir`` C function.
    !! It will attempt to read ``stream``, and return the name of the
    !! element read as ``filename``.  In case the stream is over (no more elements to
    !! read), ``ierr`` is set to 1.  In other cases, ``ierr`` will be set to 0.
    type(c_ptr), intent(inout) :: stream
    !! Stream to read from.
    character(len=*), intent(inout) :: filename
    !! Filename read from ``stream``.
    integer, intent(inout) :: ierr
    !! Return value.

    type(c_dirent), pointer :: tmp
    type(c_ptr) :: c_tmp
    integer :: i

    interface
       function c_readdir(c_stream) bind(c, name='readdir')
         import c_ptr
         implicit none
         type(c_ptr), value :: c_stream
         type(c_ptr) :: c_readdir
       end function c_readdir
    end interface

    filename = ''
    tmp => null()
    c_tmp = c_readdir(stream)

    if (c_associated(c_tmp)) then
       call c_f_pointer(c_tmp, tmp)

       i = 1
       do while (tmp%c_name(i) /= c_null_char .and. i <= len(filename))
          filename(i:i) = tmp%c_name(i)
          i = i+1
       end do
       ierr = 0
       tmp => null()
    else
       ierr = 1
    end if
  end subroutine readdir


  subroutine closedir(stream, ierr)
    !! Open the given directory.
    !!
    !! This function is an interface to the ``opendir`` C function, and has the same
    !! return values.
    type(c_ptr), intent(inout) :: stream
    !! Opened stream.
    integer, intent(inout) :: ierr
    !! Return value.

    interface
       function c_closedir(c_stream) bind(c, name='closedir')
         import c_ptr, c_int
         implicit none
         type(c_ptr), value :: c_stream
         integer(c_int) :: c_closedir
       end function c_closedir
    end interface

    ierr = c_closedir(stream)
  end subroutine closedir


  ! Copy interface
  function copy_list(origin, dest, cpflags) result(res)
    !! Copy a list of files or folders to destination.
    !!
    !! The function constructs a ``cp`` command based on the content of ``origin``,
    !! ``dest`` and ``cpflags`` as following:
    !!
    !!     cp -f [cpflags] origin(1) origin(2) ... origin(N) dest
    !!
    !! If any element of ``ends with  ``/``, the ``-r`` (recursive) is automatically
    !! added.
    !!
    !! If any element of ``origin`` does not exist, then it is not added to the command.
    character(len=*), intent(in) :: origin(:)
    !! Set of files to copy.
    character(len=*), intent(in) :: dest
    !! Destination.
    character(len=*), intent(in), optional :: cpflags
    !! Optional additional flags for ``cp``.
    integer :: res

    character(len=2048) :: cpcmd, file_list

    integer :: i
    logical :: exists
    logical :: is_recursive

    cpcmd = 'cp -f'

    if (present(cpflags)) cpcmd = trim(cpcmd)//' '//trim(cpflags)

    is_recursive = copy_check_if_recursive(cpcmd)

    file_list = ''
    do i=1, size(origin)
       inquire(file=trim(origin(i)), exist=exists)
       if (exists) then
          if (.not. is_recursive) then
             if ( origin(i)(len_trim(origin(i)):len_trim(origin(i))) == '/') &
                  & cpcmd = trim(cpcmd)//' -r '
          end if
          file_list = trim(file_list)//' '//trim(origin(i))
       end if
    end do
    cpcmd = trim(cpcmd)//' '//trim(file_list)//' '//trim(dest)

    res = 0
    call execute_command_line(trim(cpcmd), exitstat=res)
  end function copy_list

  function copy_as(origin, dest, cpflags) result(res)
    !! Copy the ``origin`` element to ``dest``.
    !!
    !! If ``origin`` is a directory, the ``-r`` (recursive) flag is automatically added
    !! to the ``cp`` command.
    !!
    !! The command executed is constructed as:
    !!
    !!     cp -f [cpflags] [-r] origin dest
    !!
    character(len=*), intent(in) :: origin
    !! Origin file or directory.
    character(len=*), intent(in) :: dest
    !! Destination
    character(len=*), intent(in), optional :: cpflags
    !! Optional additional flags for the ``cp`` command.

    integer :: res

    character(len=2048) :: cpcmd
    logical :: is_dir, is_recursive

    cpcmd = 'cp -f'
    if (present(cpflags)) cpcmd = trim(cpcmd)//' '//trim(cpflags)

    is_dir = .false.
    if (origin( len_trim(origin):len_trim(origin) ) == '/') is_dir = .true.

    is_recursive = copy_check_if_recursive(cpcmd)

    if ((is_dir) .and. .not. (is_recursive)) cpcmd = trim(cpcmd)//' -r'

    cpcmd = trim(cpcmd)//' '//trim(origin)//' '//trim(dest)
    ! print *, 'CPCMD = ', trim(cpcmd)

    res = 0
    call execute_command_line(trim(cpcmd), exitstat=res)
  end function copy_as

  function copy_multiple(origin, dest, cpflags) result(res)
    !! Copy a list of files as a list of destinations.
    !!
    !! This function is used to copy a set of files to a set of given destinations. The
    !! ``ith`` element of ``origin`` is copied to the destination ``dest(i)`` with the
    !! following ``cp`` command:
    !!
    !!     cp -f [cpflags] [-r] origin(i) dest(i)
    !!
    !! If ``origin(i)`` is a directory, the ``-r`` (recursive) flag is automatically added.
    character(len=*), intent(in) :: origin(:)
    !! List of files and / or directories to copy.
    character(len=*), intent(in) :: dest(:)
    !! List of destinations.
    character(len=*), intent(in), optional :: cpflags
    !! Optional additional flags for the ``cp`` command.
    integer :: res

    character(len=2048) :: cpcmd, cptmp
    logical :: is_dir, is_recursive
    integer :: i

    if (size(origin) /= size(dest)) then
       res = -1
       return
    end if

    cpcmd = 'cp -f'
    if (present(cpflags)) cpcmd = trim(cpcmd)//' '//trim(cpflags)

    res = 0
    do i=1, size(origin)
       cptmp = cpcmd
       is_dir = .false.
       if (origin(i)( len_trim(origin(i)):len_trim(origin(i)) ) == '/') is_dir = .true.

       is_recursive = copy_check_if_recursive(cptmp)

       if (is_dir .and. .not. is_recursive) cptmp = trim(cptmp)//' -r'

       cptmp = trim(cptmp)//' '//trim(origin(i))//' '//trim(dest(i))
       ! print *, 'CPCMD = ', trim(cptmp)

       call execute_command_line(trim(cptmp), exitstat=res)
       if (res /= 0) return
    end do
  end function copy_multiple

  pure function copy_check_if_recursive(cpcmd) result(res)
    character(len=*), intent(in) :: cpcmd
    logical :: res

    res = ( (index('-R', cpcmd) /= 0) &
         &           .or. (index('--recusrive', cpcmd) /= 0) &
         &           .or. (index('-r ', cpcmd) /= 0) &
         & )
  end function copy_check_if_recursive


  ! RM interface
  function rm_files_and_dir(files) result(res)
    !! Interface to the ``rm`` program.
    !!
    !! The command is constructed by appending all elements from the array ``files`` to
    !! ``rm -rf`` exactly as they are.
    !!
    !! *WARNING*: shell expansion will occur !!
    character(len=*), intent(in) :: files(:)
    !! Array of files to delete.
    integer :: res

    integer :: i
    character(len=2048) :: cmd

    cmd = 'rm -rf'
    do i=1, size(files)
       cmd = trim(cmd)//' '//trim(files(i))
    end do

    res = 0
    call execute_command_line(trim(cmd), exitstat=res)
  end function rm_files_and_dir


  ! GZIP interface
  function f_gzip(files) result(res)
    !! Interface to the ``gzip`` program.
    !!
    character(len=*), intent(in) :: files(:)
    !! Files to compress.
    integer :: res

    character(len=2048) :: cmd
    integer :: i

    cmd = 'gzip -f'
    do i=1, size(files)
       cmd = trim(cmd)//' '//trim(files(i))
    end do

    res = 0
    call execute_command_line(trim(cmd), exitstat=res)
  end function f_gzip


  ! CHMOD interface
  function chmod_list_of_files(files, code) result(res)
    !! Interface to the ``chmod`` program.
    !!
    character(len=*), intent(in) :: files(:)
    !! Files to compress.
    character(len=*), intent(in) :: code

    integer :: res

    character(len=2048) :: cmd
    integer :: i

    cmd = 'chmod '//trim(code)
    do i=1, size(files)
       cmd = trim(cmd)//' '//trim(files(i))
    end do

    res = 0
    call execute_command_line(trim(cmd), exitstat=res)
  end function chmod_list_of_files


  function chmod_recursive(directory, code) result(res)
    !! Interface to the ``chmod`` program.
    !!
    character(len=*), intent(in) :: directory
    !! Files to compress.
    character(len=*), intent(in) :: code

    integer :: res

    character(len=2048) :: cmd

    cmd = 'chmod '//trim(code)//' '//trim(directory)//'/*'

    res = 0
    call execute_command_line(trim(cmd), exitstat=res)
  end function chmod_recursive

  ! PID, fork and wait functions
  function getpid() result(res)
    integer :: res

    interface
       function c_getpid() bind(C, name='getpid')
         use iso_c_binding, only: c_int32_t
         integer(c_int32_t) :: c_getpid
       end function c_getpid
    end interface

    res = c_getpid()
  end function getpid


  function fork() result(res)
    integer :: res

    interface
       function c_fork() bind(C, name='fork')
         use iso_c_binding, only: c_int32_t
         integer(c_int32_t) :: c_fork
       end function c_fork
    end interface

    res = c_fork()
  end function fork


  function wait( stat ) result(res)
    integer, intent(out) :: stat

    integer :: res

    interface
       function c_wait( wstat ) bind(C, name='wait')
         use iso_c_binding, only: c_int, c_int32_t
         integer(c_int), intent(out) :: wstat
         integer(c_int32_t) :: c_wait
       end function c_wait
    end interface

    res = c_wait( stat )
  end function wait


  ! Private routines
  pure function to_c_str(str) result(res)
    !! Convert a Fortran string to a C string.
    !!
    !! This function takes a Fortran string, and return the same string, terminated with ``C_NULL_CHAR``.
    use iso_c_binding, only: C_NULL_CHAR, c_char
    character(len=*), intent(in) :: str
    !! String to convert.

    character(kind=c_char, len=:), allocatable :: res

    allocate( character(len(str) + 1) :: res)
    res = trim(str)//C_NULL_CHAR
  end function to_c_str
end module mod_interface
