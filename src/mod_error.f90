! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_error
  use mod_constants
  implicit none

  private

  type, public :: nx_error_t
     integer :: code
     character(len=MAX_STR_LEN) :: message
  end type nx_error_t

  interface operator (==)
     module procedure error_equal
  end interface operator (==)
  

contains

  function error_equal(err, code) result(is_equal)
    type(nx_error_t), intent(in) :: err
    integer, intent(in) :: code
    logical :: is_equal

    if (err%code == code) then
       is_equal = .true.
    else
       is_equal = .false.
    end if
  end function error_equal

  
  
end module mod_error
