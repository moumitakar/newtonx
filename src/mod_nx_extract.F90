! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_nx_extract
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! Input files extractor for Newton-X
  !!
  !! This module handles the creation of new input files (or restart files) from the
  !! results of a Newton-X trajectory.
  !!
  !! The new files are either placed in ``INFO_RESTART`` for a true restart of the
  !! trajectory (/i.e./ a continuation of the trajectory, with the same output files), or
  !! in ``inputs_from_step_NSTEP``, where ``NSTEP`` is the step number that ha been
  !! extracted.
  !!
  use mod_configuration, only: nx_config_t
  use mod_constants, only: &
       & MAX_STR_SIZE, timeunit
  use mod_input_parser, only: parser_t
  use mod_interface, only: &
       & get_list_of_files, mkdir, rm, copy
  use mod_io, only: nx_output_t
  use mod_kinds, only: dp
  use mod_tools, only: to_str

#ifdef USE_HDF5
  use hdf5, only: h5open_f, h5close_f
#endif

  use iso_fortran_env, only: stdout => output_unit
  implicit none

  public :: main

  private

contains

  function main(extract_from_step, extend_to, with_txt) result(res)
    !! Main extractor function.
    !!
    !! The function returns:
    !! 
    !! - 0 in case of succes ;
    !! - -1 in case something went wrong in the directory preparation ;
    !! - 1 if the step required cannot be found in the results files.
    integer, intent(in) :: extract_from_step
    !! Step from which to extract new inputs. If set to ``-1``, the last step availble is
    !! extracted.
    real(dp), intent(in) :: extend_to
    !! Extends the trajectory by the given value.
    logical, intent(in) :: with_txt
    !! Indicate if the program should use text files or not (only relevant when the
    !! program is compiled with HDF5 support).

    integer :: res

    type(parser_t) :: parser
    type(nx_config_t) :: config
    type(nx_output_t) :: nx_out
    character(len=:), allocatable :: target_dir
    integer :: u, ierr, step
    logical :: ext
    character(len=*), parameter :: conffile = 'nx_exported_config.nml'

    call parser%init(conffile)
    call parser%parse()
    if (parser%status /= 0) then
       write(*, '(A)') trim(parser%errmsg)
       res = -1
       return
    end if

    config = nx_config_t()
    call config%init( parser )
    nx_out = nx_output_t(config)
    call nx_out%set_options(parser)

    step = extract_from_step

    if (step == -1) then
       step = get_last_step(nx_out%base_path//'/cur_step_and_time')
       target_dir = 'INFO_RESTART'
       call parser%set('nxconfig', 'nxrestart', to_str(1))

       call prepare_restart_from_last(ierr)

       if (ierr /= 0) then
          res = -1
          print *, 'Error in file preparation from last step'
          return
       end if

    else
       target_dir = 'inputs_from_step_'//to_str(step)
    end if

    call set_new_time_and_step(parser, step, ierr)

    print *, 'Restart files will be put in '//target_dir
    print *, ''
    print '(A30,A)', adjustl('Restarting from step: '), parser%get('nxconfig', 'init_step')
    print '(A30,A,A)', adjustl('New initial time: '), &
         & parser%get('nxconfig', 'init_time'), ' fs.'

    inquire(file=target_dir, exist=ext)
    if (.not. ext) then
       res = mkdir(target_dir)
       if (res /= 0) then
          res = -1
          print *, 'Cannot create directory '//target_dir
          return
       end if
    end if

    res = copy(['JOB_AD/ ', 'JOB_NAD/'], target_dir)
    if (res /=0 ) then
       print *, 'Cannot copy input files folder'
       res = -1
       return
    end if

#ifdef USE_HDF5
    if (.not. with_txt) then
       call h5open_f(ierr)
       call nx_out%open_h5()
       call nx_out%write_h5_restart(step, target_dir, ierr)
       call nx_out%close_h5()
       call h5close_f(ierr)
    else
#endif
       call nx_out%write_restart(step, config%nstat, target_dir, ierr)
#ifdef USE_HDF5
    end if
#endif
    
    if (ierr < 0) then
       res = 1
       print *, 'ERROR: Selected step does not exist'
       return
    end if

    call set_nstatdyn(parser, target_dir)
    if (extend_to > 0) then
       call parser%set('nxconfig', 'tmax', to_str(extend_to))
       print *, 'Extending simulation time by '//to_str(extend_to)//' fs.'
    end if

    open(newunit=u, file=target_dir//'/nx-restart-config.nml', action='write')
    call parser%print(u)
    close(u)

    call parser%clean()

    res = 0
  end function main


  subroutine prepare_restart_from_last(ierr)
    !! Prepares the restart from the last step.
    !!
    !! The  routine will back up an existing ``INFO_RESTART`` directory
    !! into ``INFO_RESTART_N``, where ``N-1`` is the number of already exisiting
    !! ``INFO_RESTART_`` directories.
    !!
    !! ``target_dir`` is always set to ``INFO_RESTART``.
    integer, intent(out) :: ierr

    integer :: nrestarts, i
    character(len=256), allocatable :: filelist(:)
    logical :: ext

    nrestarts = 0

    call get_list_of_files('./', filelist, ierr)
    if (ierr /= 0) then
       print *, 'Cannot read list of files in current directory'
       return
    end if

    do i=1, size(filelist)
       if (index(filelist(i), 'INFO_RESTART_') /= 0) then
          nrestarts = nrestarts + 1
       end if
    end do

    if (nrestarts > 0) then
       print *, 'I found '//to_str(nrestarts)//' backed up restart directories'
    end if

    inquire(file='INFO_RESTART', exist=ext)
    if (ext) then
       print *, 'INFO_RESTART directory found: I''ll back it up as INFO_RESTART_'//to_str(nrestarts+1)
       ierr = rename('INFO_RESTART', 'INFO_RESTART_'//to_str(nrestarts))
       if (ierr /= 0) then
          print *, 'Cannot rename existing INFO_RESTART'
          return
       end if
    end if

    ierr = 0
  end subroutine prepare_restart_from_last


  subroutine set_new_time_and_step(parser, step, ierr)
    type(parser_t), intent(inout) :: parser
    integer, intent(in) :: step
    integer, intent(out) :: ierr

    real(dp) :: dt, new_init_time
    character(len=:), allocatable :: dt_char

    dt_char = parser%get('nxconfig', 'dt')
    read(dt_char, *, iostat=ierr) dt

    if (ierr /= 0) then
       ierr = -1
       print *, 'Cannot get "dt" from "nx_exported_config.nml'
       return
    end if

    new_init_time = dt * step
    call parser%set('nxconfig', 'init_time', to_str(new_init_time))
    call parser%set('nxconfig', 'init_step', to_str(step))

    ierr = 0
  end subroutine set_new_time_and_step
  

  function get_last_step(filename) result(res)
    character(len=*), intent(in) :: filename

    integer :: res, u

    open(newunit=u, file=filename, action='read')
    read(u, *) res
    close(u)
  end function get_last_step
  

  subroutine set_nstatdyn(parser, path)
    type(parser_t), intent(inout) :: parser
    character(len=*), intent(in) :: path

    integer :: u, nstatdyn
    logical :: ext

    inquire(file=path//'/nstatdyn', exist=ext)
    if (ext) then
       open(newunit=u, file=path//'/nstatdyn', action='read')
       read(u, *) nstatdyn
       close(u)
       call parser%set('nxconfig', 'nstatdyn', to_str(nstatdyn))
    end if
  end subroutine set_nstatdyn

end module mod_nx_extract
