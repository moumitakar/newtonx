! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_h5md
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-03-27
  !!
  !! This module implements the H5MD file format as used in Newton-X,
  !! following the specifications from
  !!
  !! It defines the ``nx_h5md_t`` type, for storing general
  !! information about the
  use iso_c_binding, only: dp => c_double
  use hdf5
  use h5lib
  implicit none

  private

  public :: nx_h5md_t, nx_h5md_ele_t
  public :: h5md_create_box
  public :: h5md_add_fixed
  public :: h5md_read_fixed

  ! Version of H5MD
  integer, dimension(2), parameter :: H5MD_VERSION = [1, 0]
  ! Useful parameters
  integer, parameter :: MAX_STR_LEN = 512
  integer, parameter :: MAX_NUMBER_SPECIES = 120

  ! Error codes
  integer, parameter :: CREATE_F_FAILURE = 200
  integer, parameter :: CREATE_G_FAILURE = 201
  integer, parameter :: OPEN_F_FAILURE = 202
  integer, parameter :: OPEN_G_FAILURE = 203

  type nx_h5md_t
     !! General object holding information about the H5MD file at
     !! hand, such as the ID of the file, or information about the
     !! simulation.
     integer(hid_t) :: id
     !! ID of the file created.
     character(len=MAX_STR_LEN) :: filename
     !! Name of the file created.
     integer(hid_t) :: part_id
     !! ID of the ``particles`` group.
     integer(hid_t) :: obs_id
     !! ID of the ``observables`` group.
     integer(hid_t) :: param_id
     !! ID of the ``parameters`` group.
     integer, dimension(:), allocatable :: species
     !! List of different species in the system.
   contains
     procedure :: init => h5md_f_init
     procedure :: close => h5md_f_close
     procedure :: open => h5md_f_open
     procedure :: finalize => h5md_f_finalize
  end type nx_h5md_t


  type nx_h5md_ele_t
     !! Object for handling time-dependent dataseries in H5MD.
     integer(hid_t) :: id
     !! ID of the group containing the object.
     integer(hid_t) :: type
     !! HDF5 datatype of the series.
     character(len=MAX_STR_LEN) :: name
     !! Name of the group.
     integer(hid_t) :: s_id
     !! ID of the ``step`` series.
     integer(hid_t) :: t_id
     !! ID of the ``time`` series.
     integer(hid_t) :: v_id
     !! ID of the ``value`` series.
     logical :: linked
     !! Indicate if the ``time`` and ``step`` series are linked from
     !! somewhere else.

   contains
     procedure :: init => h5md_ele_init
     procedure :: close => h5md_ele_close
     procedure :: open => h5md_ele_open
     generic, public :: append => &
          & h5md_element_append_d2, h5md_element_append_d1, &
          & h5md_element_append_d3, h5md_element_append_d0, &
          & h5md_element_append_i0, h5md_element_append_i1
     procedure, private :: h5md_element_append_d2
     procedure, private :: h5md_element_append_d1
     procedure, private :: h5md_element_append_d3
     procedure, private :: h5md_element_append_d0
     procedure, private :: h5md_element_append_i0
     procedure, private :: h5md_element_append_i1
     generic, public :: read => &
          & h5md_element_read_d1, h5md_element_read_d2, &
          & h5md_element_read_d3, &
          & h5md_element_read_i1
     procedure, private :: h5md_element_read_d1
     procedure, private :: h5md_element_read_d2
     procedure, private :: h5md_element_read_d3
     procedure, private :: h5md_element_read_i1
     procedure :: read_steps => h5md_element_read_steps
     generic, public :: read_single => &
          & h5md_element_read_single_d2, h5md_element_read_single_i1, &
          & h5md_element_read_single_i0
     procedure, private :: h5md_element_read_single_d2
     procedure, private :: h5md_element_read_single_i1
     procedure, private :: h5md_element_read_single_i0

  end type nx_h5md_ele_t

  interface h5md_add_fixed
     module procedure h5md_add_fixed_d1
     module procedure h5md_add_fixed_c1
     module procedure h5md_add_fixed_i1
  end interface h5md_add_fixed

  interface h5md_read_fixed
     module procedure h5md_read_fixed_c1
     module procedure h5md_read_fixed_d1
  end interface h5md_read_fixed


contains

  subroutine h5md_f_init(this, filename, author, creator, version, email)
    !! Initialize an H5MD file.
    !!
    !! The file is created with the attributes given in argument.
    !! Only the ``email`` parameter is optional.
    class(nx_h5md_t), intent(inout) :: this
    !! Object initialized.
    character(len=*), intent(in) :: filename
    !! Name of the H5MD file.
    character(len=*), target, intent(in) :: author
    !! Name of the person doing the simulation.
    character(len=*), target, intent(in) :: creator
    !! Software that creates the file.
    character(len=*), target, intent(in) :: version
    !! Software version that creates the file.
    character(len=*), target, optional, intent(in) :: email
    !! Email of the person doing the simulation.

    integer :: hdferr
    integer(hid_t) :: grp_id, auth_id, crea_id
    integer :: time

    character(len=:), allocatable :: fname

    fname = 'h5md_f_init '//trim(filename)

    ! Create file
    call h5fcreate_f(filename, H5F_ACC_TRUNC_F, this%id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create file.')
    this%filename = filename

    ! Create main h5md group
    call h5gcreate_f(this%id, 'h5md', grp_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create h5md group.')

    ! Create author and creator groups
    call h5gcreate_f(grp_id, 'author', auth_id, hdferr)
    call h5gcreate_f(grp_id, 'creator', crea_id, hdferr)

    ! Add version attribute
    call h5md_create_attribute_int(grp_id, 'version', 1, [int(2, hsize_t)], H5MD_VERSION)

    ! Add name attribute to author group
    if (author /= '') then
       call h5md_create_attribute_str(auth_id, 'name', 0, [int(1, hsize_t)], [author])
    end if

    ! Add email attribute to author group
    if (present(email)) then
       if (email /= '') then
          call h5md_create_attribute_str(auth_id, 'email', 0, [int(1, hsize_t)], [email])
       end if
    end if

    ! Add name attribute to creator group
    call h5md_create_attribute_str(crea_id, 'name', 0, [int(1, hsize_t)], [creator])

    ! Add version attribute to creator group
    call h5md_create_attribute_str(crea_id, 'version', 0, [int(1, hsize_t)], [version])

    ! Add creation_time attribute
    call h5md_time_since_epoch(time)
    call h5md_create_attribute_int(grp_id, 'creation_time', 0, [int(1&
         &, hsize_t)], [time])

    ! Atributes indicating the general status (running, terminated,
    ! ...) and the total number of hops.
    call h5md_create_attribute_str(&
         &grp_id, 'status', 0, [int(1,hsize_t)], ['RUNNING'], 24&
         &)
    call h5md_create_attribute_int(&
         & grp_id, 'nr_of_hops', 0, [int(1, hsize_t)], [0]&
         &)

    ! Create other groups
    call h5gcreate_f(this%id, 'particles', this%part_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create particles group.')
    call h5gcreate_f(this%id, 'observables', this%obs_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create observables group.')
    call h5gcreate_f(this%id, 'parameters', this%param_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create parameters group.')

    ! Close groups
    call h5gclose_f(grp_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close h5md group.')
    call h5gclose_f(crea_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close creator group.')
    call h5gclose_f(auth_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close author group.')

    ! Initialize species to dummy value
    allocate(this%species(1))
    this%species = 0
  end subroutine h5md_f_init


  subroutine h5md_f_finalize(this, status)
    use iso_c_binding, only: c_null_char, c_char
    class(nx_h5md_t), intent(inout) :: this
    character(len=*), intent(in), target :: status

    integer(hid_t) :: grp_id, attr_id, strtype
    integer :: hdferr
    integer(hsize_t) :: length
    type(C_PTR) :: buf
    character(len=1024, kind=c_char), dimension(1), target :: tmpbuf

    character(len=:), allocatable :: fname

    fname = 'h5md_f_finalize:'
    tmpbuf = trim(status)//c_null_char

    length = len(status)
    call h5gopen_f(this%id, 'h5md', grp_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open h5md group.')

    call h5aopen_f(grp_id, 'status', attr_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open h5md/status attribute.')

    call h5md_create_str_type(strtype, length)
    buf = C_LOC(tmpbuf)
    call h5awrite_f(attr_id, strtype, buf, hdferr)
    call h5md_check_error(hdferr, fname//' cannot write attribute.')

    call h5aclose_f(attr_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close attribute.')
    call h5gclose_f(grp_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close h5md group.')
  end subroutine h5md_f_finalize


  subroutine h5md_f_open(this, filename)
    !! Open an H5MD file for R/W access.
    !!
    class(nx_h5md_t), intent(inout) :: this
    !! New ``nx_h5md_t`` object.
    character(len=*), intent(in) :: filename
    !! Name of the file to open.

    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_f_open:'

    call h5fopen_f(filename, H5F_ACC_RDWR_F, this%id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open file '//trim(filename))
    call h5gopen_f(this%id, 'observables', this%obs_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open group observables.')
    call h5gopen_f(this%id, 'particles', this%part_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open group particles.')
    call h5gopen_f(this%id, 'parameters', this%param_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open group parameters.')

  end subroutine h5md_f_open


  subroutine h5md_f_close(this)
    !! Close the HDF5 file.
    !!
    !! This subroutine will close all main groups before closing the
    !! file.
    class(nx_h5md_t), intent(inout) :: this
    !! File object to close.

    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_f_close:'

    call h5gclose_f(this%obs_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close group observables.')
    call h5gclose_f(this%part_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close group particles.')
    call h5gclose_f(this%param_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close group parameters.')
    call h5fclose_f(this%id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close file.')

    if (allocated(this%species)) deallocate(this%species)
  end subroutine h5md_f_close


  subroutine h5md_time_since_epoch(time)
    !! Compute the time elapsed since Epoch in seconds.
    !!
    !! This subroutine returns the time in second that has passed
    !! since January 1st, 1970 (Epoch time).
    integer, intent(out) :: time
    !! Epoch time (in seconds).

    integer, dimension(8) :: val
    integer, dimension(12) :: months

    integer :: i

    time = 0

    months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    call date_and_time(values=val)

    ! Compute the seconds elapsed since 1970 for "regular" years
    time = sum(months) * (val(1) - 1970) * 24 * 3600

    ! Days in current year, past months
    if (val(2) > 1) then
       time = time + sum(months(1:val(2) - 1)) * 24 * 3600
    end if

    ! Days in this month
    time = time + val(3) * 24 * 3600

    ! Time of this day
    time = time + val(7) + val(6) * 60 + val(5) * 3600

    ! Now account for irregular years, where February has 29 days
    do i=1970, val(1)
       if ((mod(i, 4) == 0) .and. (mod(i, 100) /= 0)) then
          time = time + 24 * 3600
       else if (mod(i, 400) == 0) then
          time = time + 24 * 3600
       end if
    end do
  end subroutine h5md_time_since_epoch


  subroutine h5md_ele_init(this, loc_id, type, name, dims, origin)
    !! Initialize an H5MD element.
    !!
    !! The element is created as a group from ``loc_id``, with the
    !! name ``name`` and HDF5 datatype ``type``. The dimension of the
    !! dataset must also be specified.
    !!
    !! Optionally it is also possible to specify an ``origin``
    !! identifier, which must refer to the ID of another
    !! ``nx_h5md_ele_t`` object. If specified, the ``time`` and
    !! ``step`` of the created object will be hard-linked to those of
    !! ``origin``.
    class(nx_h5md_ele_t), intent(inout) :: this
    !! Object to initialized.
    integer(hid_t), intent(in) :: loc_id
    !! Parent of the new object.
    integer(hid_t), intent(in) :: type
    !! HDF5 datatype that the series will contain.
    character(len=*), intent(in) :: name
    !! Name of the data series.
    integer, dimension(:), intent(in) :: dims
    !! Dimension of the dataset.
    integer(hid_t), intent(in), optional :: origin
    !! (Optional) If specified, the ``time`` and ``step`` datasets
    !! will be hard-linked to those present in ``origin``.

    integer :: hdferr, i
    integer(hsize_t) :: dd(size(dims))
    character(len=:), allocatable :: fname

    fname = 'h5md_ele_init '//trim(name)

    do i=1, size(dd)
       dd(i) = int(dims(i), hsize_t)
    end do

    ! First create the corresponding group
    call h5gcreate_f(loc_id, name, this%id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create group ')

    this%name = name

    this% linked = .false.
    if (present(origin)) then
       ! Check if a hard link to another object has been requested
       this% linked = .true.
       call h5lcreate_hard_f(origin, 'time', this% id, 'time', hdferr)
       call h5md_check_error(hdferr, fname//' cannot link time')
       call h5lcreate_hard_f(origin, 'step', this% id, 'step', hdferr)
       call h5md_check_error(hdferr, fname//' cannot link step')
    else
       ! Create the time and step datasets
       call h5md_create_extensible_ds(this%id, 'time', [int(0, hsize_t)],&
            & H5T_NATIVE_DOUBLE, this%t_id)
       call h5md_check_error(hdferr, fname//' cannot create extensible "time" dataset')
       call h5md_create_extensible_ds(this%id, 'step', [int(0, hsize_t)],&
            & H5T_NATIVE_INTEGER, this%s_id)
       call h5md_check_error(hdferr, fname//' cannot create extensible "step" dataset')
    end if

    ! Create the value dataset
    call h5md_create_extensible_ds(this%id, 'value', dd, type, this%v_id)
    call h5md_check_error(hdferr, fname//' cannot create extensible "value" dataset')

  end subroutine h5md_ele_init


  subroutine h5md_ele_close(this)
    !! Close the H5MD element.
    class(nx_h5md_ele_t), intent(inout) :: this
    !! This object.

    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_ele_close '//trim(this%name)//':'

    call h5dclose_f(this%s_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close step dataset.')
    call h5dclose_f(this%t_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close time dataset.')
    call h5dclose_f(this%v_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close value dataset.')
    call h5gclose_f(this%id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close group.')
  end subroutine h5md_ele_close


  subroutine h5md_ele_open(this, name, loc_id, hdferr)
    !! Open the given ``name`` series at ``loc_id`` as H5MD element.
    !!
    class(nx_h5md_ele_t), intent(inout) :: this
    character(len=*), intent(in) :: name
    integer(hid_t), intent(in) :: loc_id
    integer, intent(out), optional :: hdferr

    logical :: link_exists

    character(len=:), allocatable :: fname

    fname = 'h5md_ele_open '//trim(this%name)//':'

    call h5gopen_f(loc_id, name, this%id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open group.')
    call h5dopen_f(this%id, 'time', this%t_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open time dataset.')
    call h5dopen_f(this%id, 'step', this%s_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open step dataset.')
    call h5dopen_f(this%id, 'value', this%v_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot open value dataset.')

    call h5lexists_f(this%id, 'step', link_exists, hdferr)
    call h5md_check_error(hdferr, fname//' cannot check if link exists.')
    if (link_exists) then
       this%linked = .true.
    else
       this%linked = .false.
    end if

  end subroutine h5md_ele_open



  subroutine h5md_create_str_type(strtype, length)
    !! Derive a string type for HDF5.
    !!
    !! This subroutine derives a HDF5 string type for the given string
    !! length.
    integer(hid_t), intent(inout) :: strtype
    !! Resulting string type.
    integer(hsize_t), intent(in) :: length
    !! Length of the target string.

    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_create_str_type:'

    ! Derive the string type for the boundary attribute
    call h5tcopy_f(H5T_NATIVE_CHARACTER, strtype, hdferr)
    call h5md_check_error(hdferr, fname//' cannot copy type.')
    call h5tset_size_f(strtype, length, hdferr)
    call h5md_check_error(hdferr, fname//' cannot set size.')
    call h5tset_strpad_f(strtype, H5T_STR_NULLTERM_F, hdferr)
    call h5md_check_error(hdferr, fname//' cannot pad.')
  end subroutine h5md_create_str_type


  subroutine h5md_create_attribute_str(loc_id, name, rank, dim, val, maxlength)
    !! Create a string-type attribute.
    !!
    !! The attribute is created at the specified location ``loc_id``,
    !! with the value ``val`` written to it. ``val`` must be given as
    !! an array for generality (it will be used for box creation,
    !! where more than one dimension is possible).
    use iso_c_binding, only: c_null_char, c_char
    integer(hid_t), intent(inout) :: loc_id
    !! Location where the attribute is written.
    character(len=*), intent(in) :: name
    !! Name of the attribute.
    integer, intent(in) :: rank
    !! Rank of the written attribute.
    integer(hsize_t), dimension(:), intent(in) :: dim
    !! Dimension of the attribute.
    character(len=*), dimension(:), target, intent(in) :: val
    !! Value to write as attribute (must have an array shape).
    integer, intent(in), optional :: maxlength
    !! Maximal lengths of the fields in ``val``.  If not provided, the
    !! max length is taken as the highest length of all values in the
    !! ``val`` array.

    integer(hid_t) :: strtype
    integer(hsize_t) :: length
    integer(hid_t) :: space_id, attr_id
    integer :: hdferr
    integer :: i

    type(C_PTR) :: buf
    character(len=1024, kind=c_char), dimension(1), target :: tmpbuf
    character(len=:), allocatable :: fname

    fname = 'h5md_create_attribute '//trim(name)//':'

    if (present(maxlength)) then
       length = int(maxlength, hsize_t)
    else
       length = len(val(1))
       do i=1, size(val)
          if (len(val(i)) > length) then
             length = len(val(i))
          end if
       end do
    end if

    tmpbuf = trim(val(1))//C_NULL_CHAR

    call h5md_create_str_type(strtype, length)
    call h5screate_simple_f(rank, dim, space_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')

    call h5acreate_f(loc_id, name, strtype, space_id, attr_id, hdferr)
    buf = C_LOC(tmpbuf)
    call h5md_check_error(hdferr, fname//' cannot create attribute.')
    call h5awrite_f(attr_id, strtype, buf, hdferr)
    call h5md_check_error(hdferr, fname//' cannot write attribute.')

    ! Close everything
    call h5aclose_f(attr_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close attribute.')
    call h5sclose_f(space_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close dataspace.')
  end subroutine h5md_create_attribute_str


  subroutine h5md_create_attribute_int(loc_id, name, rank, dim, val)
    !! Create an integer-type attribute.
    !!
    !! The attribute is created at the specified location ``loc_id``,
    !! with the value ``val`` written to it. ``val`` must be given as
    !! an array for generality (it will be used for box creation,
    !! where more than one dimension is possible).
    integer(hid_t), intent(inout) :: loc_id
    !! Location where the attribute is written.
    character(len=*), intent(in) :: name
    !! Name of the attribute.
    integer, intent(in) :: rank
    !! Rank of the written attribute.
    integer(hsize_t), dimension(:), intent(in) :: dim
    !! Dimension of the attribute.
    integer, dimension(:), target, intent(in) :: val
    !! Value to write as attribute (must have an array shape).

    integer(hid_t) :: space_id, attr_id
    integer :: hdferr

    type(C_PTR) :: buf

    character(len=:), allocatable :: fname

    fname = 'h5md_create_attribute '//trim(name)//':'

    call h5screate_simple_f(rank, dim, space_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')

    call h5acreate_f(loc_id, name, H5T_NATIVE_INTEGER, space_id, attr_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create attribute.')
    buf = C_LOC(val)
    call h5awrite_f(attr_id, H5T_NATIVE_INTEGER, buf, hdferr)
    call h5md_check_error(hdferr, fname//' cannot write attribute.')

    ! Close everything
    call h5aclose_f(attr_id, hdferr)
    call h5md_check_error(hdferr, fname//' problem in closing attribute.')
    call h5sclose_f(space_id, hdferr)
    call h5md_check_error(hdferr, fname//' problem in closing dataspace.')
  end subroutine h5md_create_attribute_int


  subroutine h5md_create_box(loc_id, dims, boundaries)
    !! Create a box object for the selected particles group.
    !!
    !! For now only non-periodic boxes are implemented.
    integer(hid_t), intent(inout) :: loc_id
    !! ID of the object to add the box to.
    integer, intent(in) :: dims
    !! Dimension of the molecular system (1, 2 or 3).
    character(len=*), dimension(:), intent(in) :: boundaries
    !! Array containing the boundary conditions.

    integer :: hdferr
    integer(hid_t) :: box_id

    character(len=:), allocatable :: fname

    fname = 'h5md_create_box:'

    call h5gcreate_f(loc_id, 'box', box_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create box group.')
    call h5md_create_attribute_int(box_id, 'dimension', 0, [int(1, hsize_t)], [dims])
    call h5md_check_error(hdferr, fname//' cannot create dimension attribute.')
    call h5md_create_attribute_str(box_id, 'boundary', 1, [int(dims, hsize_t)], boundaries)
    call h5md_check_error(hdferr, fname//' cannot create boundary attribute.')
    call h5gclose_f(box_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close box group.')
  end subroutine h5md_create_box


  subroutine h5md_create_extensible_ds(loc_id, name, dims, type,&
       & set_id)
    !! Create a new extensible dataset.
    !!
    !! This subroutine handles the creation of time-dependent
    !! datasets, given the wanted dimension. The time dimension will
    !! be inserted as the first dimension of the final dataset. The
    !! subroutine returns the ``set_id`` of the created dataset.
    integer(hid_t), intent(inout) :: loc_id
    !! Parent object of the new dataset.
    character(len=*), intent(in) :: name
    !! Name of the dataset.
    integer(hsize_t), dimension(:), intent(in) :: dims
    !! Dimensions of the dataset.
    integer(hid_t), intent(in) :: type
    !! HDF5 datatypes.
    integer(hid_t), intent(out) :: set_id
    !! ID of the created dataset.

    integer :: rank
    integer(hsize_t), dimension(:), allocatable :: new_dims
    integer(hsize_t), dimension(:), allocatable :: maxdims
    integer(hsize_t), dimension(:), allocatable :: chunk_dims
    integer(hid_t) :: space_id, prop_list
    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_create_extensible_ds '//trim(name)//':'

    ! Define the rank and dimension of the EXTENSIBLE dataset
    if ((size(dims) == 1) .and. (dims(1) == 0)) then
       rank = 1
    else
       rank = size(dims) + 1
    end if
    allocate(new_dims(rank))
    new_dims(rank) = 0
    if (rank > 1) then
       new_dims(1:rank-1) = dims
    end if

    ! Create an extensible dataspace based on parameter `dims`
    allocate(maxdims(rank))
    maxdims(rank) = H5S_UNLIMITED_F
    if (rank > 1) then
       maxdims(1:rank-1) = dims
    end if
    call h5screate_simple_f(rank, new_dims, space_id, hdferr, maxdims)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')
    deallocate(maxdims)

    ! Set chunking
    allocate(chunk_dims(rank))
    chunk_dims(rank) = 1 ! Access the chunks by time step index
    if (rank > 1) then
       chunk_dims(1:rank-1) = dims
    end if
    call h5pcreate_f(H5P_DATASET_CREATE_F, prop_list, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create property.')
    call h5pset_chunk_f(prop_list, rank, chunk_dims, hdferr)
    call h5md_check_error(hdferr, fname//' cannot set chunks.')
    deallocate(chunk_dims)

    ! Create the dataspace and return the value ``set_id``
    call h5dcreate_f(loc_id, name, type, space_id, set_id,&
         & hdferr, prop_list)
    call h5md_check_error(hdferr, fname//' cannot create dataset.')

    ! Close everything
    call h5pclose_f(prop_list, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close property.')
    call h5sclose_f(space_id, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close dataspace.')
  end subroutine h5md_create_extensible_ds


  ! subroutine h5md_create_fixed_ds(loc_id, name, dims, type, set_id)
  !   integer(hid_t), intent(inout) :: loc_id
  !   !! Parent object of the new dataset.
  !   character(len=*), intent(in) :: name
  !   !! Name of the dataset.
  !   integer(hsize_t), dimension(:), intent(in) :: dims
  !   !! Dimensions of the dataset.
  !   integer(hid_t), intent(in) :: type
  !   !! HDF5 datatypes.
  !   integer(hid_t), intent(out) :: set_id
  !   !! ID of the created dataset.
  !
  !   integer :: rank
  !   integer :: hdferr
  !   integer(hid_t) :: space_id
  !
  !   rank = size(dims)
  !   call h5screate_simple_f(rank, dims, space_id, hdferr)
  !   call h5dcreate_f(loc_id, name, type, space_id, set_id, hdferr)
  !   call h5sclose_f(space_id, hdferr)
  ! end subroutine h5md_create_fixed_ds


  subroutine h5md_extend_dimension(dset, rank, dims)
    !! Extend the dimension of the given dataset.
    !!
    !! The subroutine will extend the extensible dimension (the last
    !! dimension in this implementation)of the given dataset by one.
    !! It returns the rank (``rank``) and the dimensions (``dims``) of
    !! the extended dataset.
    integer(hid_t), intent(in) :: dset
    !! ID of the dataset to modify.
    integer, intent(out) :: rank
    !! Rank of the new dataset.
    integer(hsize_t), dimension(:), allocatable, intent(out) :: dims
    !! Dimensions of the new dataset.

    integer(hid_t) :: srcspace
    integer :: hdferr
    integer(hsize_t), dimension(:), allocatable :: maxdims

    character(len=:), allocatable :: fname

    fname = 'h5md_extend_dimension:'

    ! Start by deallocating dims
    if (allocated(dims)) deallocate(dims)

    ! Get current dimensions of the dataspace
    call h5dget_space_f(dset, srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get space.')
    call h5sget_simple_extent_ndims_f(srcspace, rank, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get simple extent ndims.')
    allocate(dims(rank))
    allocate(maxdims(rank))
    call h5sget_simple_extent_dims_f(srcspace, dims, maxdims, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get simple extent dims.')
    call h5sclose_f(srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot close dataspace.')

    ! Extend
    dims(rank) = dims(rank) + 1

    call h5dset_extent_f(dset, dims, hdferr)
    call h5md_check_error(hdferr, fname//' cannot extend dataset.')
  end subroutine h5md_extend_dimension


  subroutine h5md_element_append_s_t(this, step, time)
    class(nx_h5md_ele_t), intent(inout) :: this
    integer, intent(in) :: step
    real(dp), intent(in) :: time

    integer(hsize_t), dimension(1) :: offset, count
    integer(hsize_t), dimension(:), allocatable :: dims
    integer :: r
    integer(hid_t) :: memspace, srcspace
    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_element_append_s_t '//trim(this%name)//':'

    count(1) = 1
    call h5md_extend_dimension(this%t_id, r, dims)
    offset(1) = dims(r) - 1
    call h5screate_simple_f(0, [int(1, hsize_t)], memspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')

    call h5dget_space_f(this%t_id, srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get dataspace.')

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5md_check_error(hdferr, fname//' cannot select hyperslab.')
    call h5dwrite_f(this%t_id, H5T_NATIVE_DOUBLE, time, dims, hdferr,&
         & memspace, srcspace)
    call h5md_check_error(hdferr, fname//' cannot write dataset.')

    call h5md_extend_dimension(this%s_id, r, dims)
    offset = dims(r) - 1
    call h5screate_simple_f(0, [int(1, hsize_t)], memspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')

    call h5dget_space_f(this%s_id, srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get dataspace.')

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5md_check_error(hdferr, fname//' cannot select hyperslab.')

    call h5dwrite_f(this%s_id, H5T_NATIVE_INTEGER, step, dims, hdferr,&
         & memspace, srcspace)
    call h5md_check_error(hdferr, fname//' cannot write dataset.')
  end subroutine h5md_element_append_s_t


  subroutine h5md_element_append_d2(this, data, step, time)
    class(nx_h5md_ele_t), intent(inout) :: this
    real(dp), dimension(:, :), intent(in) :: data
    integer, intent(in) :: step
    real(dp), intent(in) :: time

    integer :: r, memrank
    integer(hid_t) :: srcspace, memspace
    integer(hsize_t), dimension(:), allocatable :: memdims, dims
    integer(hsize_t), dimension(:), allocatable :: offset, count
    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_element_append_d2 '//trim(this%name)//':'

    ! Extend the dimension for the values
    call h5md_extend_dimension(this%v_id, r, dims)

    ! Memory dataspace definition
    memrank = rank(data)
    allocate(memdims(memrank))
    memdims = shape(data)
    call h5screate_simple_f(memrank, memdims, memspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')

    call h5dget_space_f(this%v_id, srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get dataspace.')

    allocate(offset(r))
    allocate(count(r))
    offset = 0
    offset(r) = dims(r) - 1
    count(r) = 1
    if (memrank > 0) then
       count(1:r-1) = shape(data)
    end if

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5md_check_error(hdferr, fname//' cannot select hyperslab.')

    call h5dwrite_f(this%v_id, H5T_NATIVE_DOUBLE, data, dims, hdferr,&
         & memspace, srcspace)
    call h5md_check_error(hdferr, fname//' cannot write dataset.')

    deallocate(count, offset)

    ! Now set the time and step elements
    if (.not. this%linked) then
       call h5md_element_append_s_t(this, step, time)
    end if

  end subroutine h5md_element_append_d2


  subroutine h5md_element_append_d1(this, data, step, time)
    class(nx_h5md_ele_t), intent(inout) :: this
    real(dp), dimension(:), intent(in) :: data
    integer, intent(in) :: step
    real(dp), intent(in) :: time

    integer :: r, memrank
    integer(hid_t) :: srcspace, memspace
    integer(hsize_t), dimension(:), allocatable :: memdims, dims
    integer(hsize_t), dimension(:), allocatable :: offset, count
    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_element_append_d1 '//trim(this%name)//':'

    ! Extend the dimension for the values
    call h5md_extend_dimension(this%v_id, r, dims)

    ! Memory dataspace definition
    memrank = rank(data)
    allocate(memdims(memrank))
    memdims = shape(data)
    call h5screate_simple_f(memrank, memdims, memspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')

    call h5dget_space_f(this%v_id, srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get dataspace.')

    allocate(offset(r))
    allocate(count(r))
    offset = 0
    offset(r) = dims(r) - 1
    count(r) = 1
    if (memrank > 0) then
       count(1:r-1) = shape(data)
    end if

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5md_check_error(hdferr, fname//' cannot select hyperslab.')

    call h5dwrite_f(this%v_id, H5T_NATIVE_DOUBLE, data, dims, hdferr,&
         & memspace, srcspace)
    call h5md_check_error(hdferr, fname//' cannot write dataset.')

    deallocate(count, offset)

    ! Now set the time and step elements
    if (.not. this%linked) then
       call h5md_element_append_s_t(this, step, time)
    end if

  end subroutine h5md_element_append_d1


  subroutine h5md_element_append_d3(this, data, step, time)
    class(nx_h5md_ele_t), intent(inout) :: this
    real(dp), dimension(:, :, :), intent(in) :: data
    integer, intent(in) :: step
    real(dp), intent(in) :: time

    integer :: r, memrank
    integer(hid_t) :: srcspace, memspace
    integer(hsize_t), dimension(:), allocatable :: memdims, dims
    integer(hsize_t), dimension(:), allocatable :: offset, count
    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_element_append_d3 '//trim(this%name)//':'

    ! Extend the dimension for the values
    call h5md_extend_dimension(this%v_id, r, dims)

    ! Memory dataspace definition
    memrank = rank(data)
    allocate(memdims(memrank))
    memdims = shape(data)
    call h5screate_simple_f(memrank, memdims, memspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')

    call h5dget_space_f(this%v_id, srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get dataspace.')

    allocate(offset(r))
    allocate(count(r))
    offset = 0
    offset(r) = dims(r) - 1
    count(r) = 1
    if (memrank > 0) then
       count(1:r-1) = shape(data)
    end if

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5md_check_error(hdferr, fname//' cannot select hyperslab.')

    call h5dwrite_f(this%v_id, H5T_NATIVE_DOUBLE, data, dims, hdferr,&
         & memspace, srcspace)
    call h5md_check_error(hdferr, fname//' cannot write dataset.')

    deallocate(count, offset)

    ! Now set the time and step elements
    if (.not. this%linked) then
       call h5md_element_append_s_t(this, step, time)
    end if

  end subroutine h5md_element_append_d3


  subroutine h5md_element_append_d0(this, data, step, time)
    class(nx_h5md_ele_t), intent(inout) :: this
    real(dp), intent(in) :: data
    integer, intent(in) :: step
    real(dp), intent(in) :: time

    integer :: r, memrank
    integer(hid_t) :: srcspace, memspace
    integer(hsize_t), dimension(:), allocatable :: memdims, dims
    integer(hsize_t), dimension(:), allocatable :: offset, count
    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_element_append_d0 '//trim(this%name)//':'

    ! Extend the dimension for the values
    call h5md_extend_dimension(this%v_id, r, dims)

    ! Memory dataspace definition
    memrank = rank(data)
    allocate(memdims(memrank))
    memdims = shape(data)
    call h5screate_simple_f(memrank, memdims, memspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')
    
    call h5dget_space_f(this%v_id, srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get dataspace.')

    allocate(offset(r))
    allocate(count(r))
    offset = 0
    offset(r) = dims(r) - 1
    count(r) = 1
    if (memrank > 0) then
       count(1:r-1) = shape(data)
    end if

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5md_check_error(hdferr, fname//' cannot select hyperslab.')

    call h5dwrite_f(this%v_id, H5T_NATIVE_DOUBLE, data, dims, hdferr,&
         & memspace, srcspace)
    call h5md_check_error(hdferr, fname//' cannot write dataset.')

    deallocate(count, offset)

    ! Now set the time and step elements
    if (.not. this%linked) then
       call h5md_element_append_s_t(this, step, time)
    end if

  end subroutine h5md_element_append_d0


  subroutine h5md_element_append_i0(this, data, step, time)
    class(nx_h5md_ele_t), intent(inout) :: this
    integer, intent(in) :: data
    integer, intent(in) :: step
    real(dp), intent(in) :: time

    integer :: r, memrank
    integer(hid_t) :: srcspace, memspace
    integer(hsize_t), dimension(:), allocatable :: memdims, dims
    integer(hsize_t), dimension(:), allocatable :: offset, count
    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_element_append_i0 '//trim(this%name)//':'

    ! Extend the dimension for the values
    call h5md_extend_dimension(this%v_id, r, dims)

    ! Memory dataspace definition
    memrank = rank(data)
    allocate(memdims(memrank))
    memdims = shape(data)
    call h5screate_simple_f(memrank, memdims, memspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')
    
    call h5dget_space_f(this%v_id, srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get dataspace.')

    allocate(offset(r))
    allocate(count(r))
    offset = 0
    offset(r) = dims(r) - 1
    count(r) = 1
    if (memrank > 0) then
       count(1:r-1) = shape(data)
    end if

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5md_check_error(hdferr, fname//' cannot select hyperslab.')
    
    call h5dwrite_f(this%v_id, H5T_NATIVE_INTEGER, data, dims, hdferr,&
         & memspace, srcspace)
    call h5md_check_error(hdferr, fname//' cannot select hyperslab.')

    deallocate(count, offset)

    ! Now set the time and step elements
    if (.not. this%linked) then
       call h5md_element_append_s_t(this, step, time)
    end if

  end subroutine h5md_element_append_i0


  subroutine h5md_element_append_i1(this, data, step, time)
    class(nx_h5md_ele_t), intent(inout) :: this
    integer, dimension(:), intent(in) :: data
    integer, intent(in) :: step
    real(dp), intent(in) :: time

    integer :: r, memrank
    integer(hid_t) :: srcspace, memspace
    integer(hsize_t), dimension(:), allocatable :: memdims, dims
    integer(hsize_t), dimension(:), allocatable :: offset, count
    integer :: hdferr

    character(len=:), allocatable :: fname

    fname = 'h5md_element_append_i1 '//trim(this%name)//':'

    ! Extend the dimension for the values
    call h5md_extend_dimension(this%v_id, r, dims)

    ! Memory dataspace definition
    memrank = rank(data)
    allocate(memdims(memrank))
    memdims = shape(data)
    call h5screate_simple_f(memrank, memdims, memspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot create dataspace.')

    call h5dget_space_f(this%v_id, srcspace, hdferr)
    call h5md_check_error(hdferr, fname//' cannot get dataspace.')

    allocate(offset(r))
    allocate(count(r))
    offset = 0
    offset(r) = dims(r) - 1
    count(r) = 1
    if (memrank > 0) then
       count(1:r-1) = shape(data)
    end if

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5md_check_error(hdferr, fname//' cannot select hyperslab.')

    call h5dwrite_f(this%v_id, H5T_NATIVE_INTEGER, data, dims, hdferr,&
         & memspace, srcspace)
    call h5md_check_error(hdferr, fname//' cannot write dataset.')
    
    deallocate(count, offset)

    ! Now set the time and step elements
    if (.not. this%linked) then
       call h5md_element_append_s_t(this, step, time)
    end if

  end subroutine h5md_element_append_i1


  subroutine h5md_add_fixed_c1(loc_id, name, elements)
    integer(hid_t), intent(inout) :: loc_id
    character(len=*), intent(in) :: name
    character(len=*), dimension(:), intent(in) :: elements

    integer(hid_t) :: strtype, space_id, set_id
    integer :: r, hdferr
    integer(hsize_t), dimension(1) :: dims
    integer(hsize_t) :: length

    length = len(elements(1))
    r = rank(elements)
    dims = shape(elements)

    call h5md_create_str_type(strtype, length)
    call h5screate_simple_f(r, dims, space_id, hdferr)
    call h5dcreate_f(loc_id, name, strtype, space_id, set_id, hdferr)

    call h5dwrite_f(set_id, strtype, elements, dims, hdferr)

    call h5sclose_f(space_id, hdferr)
    call h5dclose_f(set_id, hdferr)
  end subroutine h5md_add_fixed_c1


  subroutine h5md_add_fixed_d1(loc_id, name, elements)
    integer(hid_t), intent(inout) :: loc_id
    character(len=*), intent(in) :: name
    real(dp), dimension(:), intent(in) :: elements

    integer(hid_t) :: space_id, set_id
    integer :: r, hdferr
    integer(hsize_t), dimension(1) :: dims

    r = rank(elements)
    dims = shape(elements)

    call h5screate_simple_f(r, dims, space_id, hdferr)
    call h5dcreate_f(loc_id, name, H5T_NATIVE_DOUBLE, space_id, set_id, hdferr)

    call h5dwrite_f(set_id, H5T_NATIVE_DOUBLE, elements, dims, hdferr)

    call h5sclose_f(space_id, hdferr)
    call h5dclose_f(set_id, hdferr)
  end subroutine h5md_add_fixed_d1


  subroutine h5md_add_fixed_i1(loc_id, name, elements)
    integer(hid_t), intent(inout) :: loc_id
    character(len=*), intent(in) :: name
    integer, dimension(:), intent(in) :: elements

    integer(hid_t) :: space_id, set_id
    integer :: r, hdferr
    integer(hsize_t), dimension(1) :: dims

    r = rank(elements)
    dims = shape(elements)

    call h5screate_simple_f(r, dims, space_id, hdferr)
    call h5dcreate_f(loc_id, name, H5T_NATIVE_INTEGER, space_id, set_id, hdferr)

    call h5dwrite_f(set_id, H5T_NATIVE_INTEGER, elements, dims, hdferr)

    call h5sclose_f(space_id, hdferr)
    call h5dclose_f(set_id, hdferr)
  end subroutine h5md_add_fixed_i1


  subroutine h5md_element_read_d1(this, data, step, time)
    !! Read the content of an H5MD element.
    !!
    class(nx_h5md_ele_t), intent(in) :: this
    real(dp), dimension(:), allocatable, intent(out) :: data
    integer, dimension(:), allocatable, intent(out), optional :: step
    real(dp), dimension(:), allocatable, intent(out), optional :: time

    integer :: hdferr, io
    integer(HSIZE_T) :: num_points
    integer(hid_t) :: s_id ! For space id

    ! Get Space ID
    call h5dget_space_f(this%v_id, s_id, hdferr)

    ! Find the number of elements to allocate arrays
    call h5sget_simple_extent_npoints_f(s_id, num_points, hdferr)
    allocate(data(num_points), stat=io)
    if (io /= 0) STOP "Error in allocation of data()"

    ! Read values
    call h5dread_f(this%v_id, H5T_NATIVE_DOUBLE, data, [num_points], hdferr)
    if (present(time)) then
       allocate(time(num_points), stat=io)
       if (io /= 0) STOP "Error in allocation of time()"
       call h5dread_f(this%t_id, H5T_NATIVE_DOUBLE, time, [num_points], hdferr)
    end if
    if (present(step)) then
       allocate(step(num_points), stat=io)
       if (io /= 0) STOP "Error in allocation of step()"
       call h5dread_f(this%s_id, H5T_NATIVE_INTEGER, step, [num_points], hdferr)
    end if


  end subroutine h5md_element_read_d1


  subroutine h5md_element_read_d2(this, data, step, time)
    !! Read the content of an H5MD element.
    !!
    class(nx_h5md_ele_t), intent(in) :: this
    real(dp), dimension(:, :), allocatable, intent(out) :: data
    integer, dimension(:), allocatable, intent(out), optional :: step
    real(dp), dimension(:), allocatable, intent(out), optional :: time

    integer :: hdferr
    integer :: rr
    integer(hsize_t), dimension(:), allocatable  :: dims
    integer(hsize_t), dimension(:), allocatable  :: maxdims
    integer(hid_t) :: s_id ! For space id

    ! Get Space ID
    call h5dget_space_f(this%v_id, s_id, hdferr)

    ! Find the number of elements to allocate arrays
    call h5sget_simple_extent_ndims_f(s_id, rr, hdferr)
    allocate(dims(rr))
    allocate(maxdims(rr))
    call h5sget_simple_extent_dims_f(s_id, dims, maxdims, hdferr)
    allocate(data(dims(1), dims(2)))

    ! Read values
    call h5dread_f(this%v_id, H5T_NATIVE_DOUBLE, data, dims, hdferr)
    if (present(step)) then
       allocate(step(dims(2)))
       call h5dread_f(this%s_id, H5T_NATIVE_INTEGER, step, [dims(2)], hdferr)
    end if
    if (present(time)) then
       allocate(time(dims(2)))
       call h5dread_f(this%t_id, H5T_NATIVE_DOUBLE, time, [dims(2)], hdferr)
    end if

  end subroutine h5md_element_read_d2


  subroutine h5md_element_read_d3(this, data, step, time)
    !! Read the content of an H5MD element.
    !!
    class(nx_h5md_ele_t), intent(in) :: this
    real(dp), dimension(:, :, :), allocatable, intent(out) :: data
    integer, dimension(:), allocatable, intent(out), optional :: step
    real(dp), dimension(:), allocatable, intent(out), optional :: time

    integer :: hdferr
    integer :: rr
    integer(hsize_t), dimension(:), allocatable  :: dims
    integer(hsize_t), dimension(:), allocatable  :: maxdims
    integer(hid_t) :: s_id ! For space id

    ! Get Space ID
    call h5dget_space_f(this%v_id, s_id, hdferr)

    ! Find the number of elements to allocate arrays
    call h5sget_simple_extent_ndims_f(s_id, rr, hdferr)
    allocate(dims(rr))
    allocate(maxdims(rr))
    call h5sget_simple_extent_dims_f(s_id, dims, maxdims, hdferr)
    allocate(data(dims(1), dims(2), dims(3)))

    ! Read values
    call h5dread_f(this%v_id, H5T_NATIVE_DOUBLE, data, dims, hdferr)
    if (present(step)) then
       allocate(step(dims(3)))
       call h5dread_f(this%s_id, H5T_NATIVE_INTEGER, step, [dims(3)], hdferr)
    end if
    if (present(time)) then
       allocate(time(dims(3)))
       call h5dread_f(this%t_id, H5T_NATIVE_DOUBLE, time, [dims(3)], hdferr)
    end if

  end subroutine h5md_element_read_d3


  subroutine h5md_element_read_i1(this, data, step, time)
    !! Read the content of an H5MD element.
    !!
    class(nx_h5md_ele_t), intent(in) :: this
    integer, dimension(:), allocatable, intent(out) :: data
    integer, dimension(:), allocatable, intent(out), optional :: step
    real(dp), dimension(:), allocatable, intent(out), optional :: time

    integer :: hdferr, io
    integer(HSIZE_T) :: num_points
    integer(hid_t) :: s_id ! For space id

    ! Get Space ID
    call h5dget_space_f(this%v_id, s_id, hdferr)

    ! Find the number of elements to allocate arrays
    call h5sget_simple_extent_npoints_f(s_id, num_points, hdferr)
    allocate(data(num_points), stat=io)
    if (io /= 0) STOP "Error in allocation of data()"

    ! Read values
    call h5dread_f(this%v_id, H5T_NATIVE_INTEGER, data, [num_points], hdferr)
    if (present(time)) then
       allocate(time(num_points), stat=io)
       if (io /= 0) STOP "Error in allocation of time()"
       call h5dread_f(this%t_id, H5T_NATIVE_DOUBLE, time, [num_points], hdferr)
    end if
    if (present(step)) then
       allocate(step(num_points), stat=io)
       if (io /= 0) STOP "Error in allocation of step()"
       call h5dread_f(this%s_id, H5T_NATIVE_INTEGER, step, [num_points], hdferr)
    end if
  end subroutine h5md_element_read_i1


  subroutine h5md_element_read_single_d2(this, data, step)
    !! Read the content of an H5MD element.
    !!
    class(nx_h5md_ele_t), intent(in) :: this
    real(dp), allocatable, dimension(:, :), intent(inout) :: data
    integer, intent(in) :: step

    integer, parameter :: memrank = 2 ! Rank of the dataset in memory
    integer, parameter :: filerank = 3 ! Rank of the dataset in file

    integer :: hdferr
    integer(hid_t) :: srcspace, memspace
    integer(hsize_t) :: dims(filerank), maxdims(filerank)
    integer(hsize_t) :: memdims(memrank)
    integer(hsize_t) :: count(filerank), offset(filerank)

    integer :: i

    ! Get Space ID
    call h5dget_space_f(this%v_id, srcspace, hdferr)
    call h5md_check_error(hdferr, 'GET_SPACE error')
    call h5sget_simple_extent_dims_f(srcspace, dims, maxdims, hdferr)
    call h5md_check_error(hdferr, 'GET_SIMPLE_EXTENT_DIMS error')
    do i=1, memrank
       memdims(i) = dims(i)
    end do
    allocate(data(dims(1), dims(2)))

    ! We select the slab corresponding to what we look for. The
    ! 'time' dimension is the last one, so we need to select 1 full
    ! matrix at position (x, y, step - 1) where (x, y) is the dimension of
    ! a single matrix. So the offset will be (0, 0, step - 1), and
    ! the number of elements to select will be (x, y, 1).
    call h5screate_simple_f(memrank, memdims, memspace, hdferr)

    offset(:) = 0
    offset(filerank) = step - 1
    count(filerank) = 1
    if (memrank > 0) then
       count(1:filerank-1) = shape(data)
    end if

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5sselect_hyperslab_f(memspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)

    ! Read values
    call h5dread_f(this%v_id, H5T_NATIVE_DOUBLE, data, dims,&
         & hdferr, memspace, srcspace)
  end subroutine h5md_element_read_single_d2


  subroutine h5md_element_read_single_i1(this, data, step)
    !! Read the content of an H5MD element.
    !!
    class(nx_h5md_ele_t), intent(in) :: this
    integer, allocatable, dimension(:), intent(inout) :: data
    integer, intent(in) :: step

    integer, parameter :: memrank = 1 ! Rank of the dataset in memory
    integer, parameter :: filerank = 2 ! Rank of the dataset in file

    integer :: hdferr
    integer(hid_t) :: srcspace, memspace
    integer(hsize_t) :: dims(filerank), maxdims(filerank)
    integer(hsize_t) :: memdims(memrank)
    integer(hsize_t) :: count(filerank), offset(filerank)

    integer :: i

    ! Get Space ID
    call h5dget_space_f(this%v_id, srcspace, hdferr)
    call h5sget_simple_extent_dims_f(srcspace, dims, maxdims, hdferr)
    do i=1, memrank
       memdims(i) = dims(i)
    end do
    allocate(data(dims(1)))

    ! We select the slab corresponding to what we look for. The
    ! 'time' dimension is the last one, so we need to select 1 full
    ! matrix at position (x, y, step - 1) where (x, y) is the dimension of
    ! a single matrix. So the offset will be (0, 0, step - 1), and
    ! the number of elements to select will be (x, y, 1).
    call h5screate_simple_f(memrank, memdims, memspace, hdferr)

    offset(:) = 0
    offset(filerank) = step - 1
    count(filerank) = 1
    if (memrank > 0) then
       count(1:filerank-1) = shape(data)
    end if

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)
    call h5sselect_hyperslab_f(memspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)

    ! Read values
    call h5dread_f(this%v_id, H5T_NATIVE_INTEGER, data, dims,&
         & hdferr, memspace, srcspace)
  end subroutine h5md_element_read_single_i1


  subroutine h5md_element_read_single_i0(this, data, step)
    !! Read the content of an H5MD element.
    !!
    class(nx_h5md_ele_t), intent(in) :: this
    integer, intent(inout) :: data
    integer, intent(in) :: step

    integer, parameter :: memrank = 0 ! Rank of the dataset in memory
    integer, parameter :: filerank = 1 ! Rank of the dataset in file

    integer :: hdferr
    integer(hid_t) :: srcspace, memspace
    integer(hsize_t) :: dims(filerank), maxdims(filerank)
    integer(hsize_t) :: memdims(memrank)
    integer(hsize_t) :: count(filerank), offset(filerank)

    ! integer :: i

    ! Get Space ID
    call h5dget_space_f(this%v_id, srcspace, hdferr)
    call h5sget_simple_extent_dims_f(srcspace, dims, maxdims, hdferr)
    ! memdims(1) = dims(1)
    ! do i=1, memrank
    !    memdims(i) = dims(i)
    ! end do

    ! We select the slab corresponding to what we look for. The
    ! 'time' dimension is the last one, so we need to select 1 full
    ! matrix at position (x, y, step - 1) where (x, y) is the dimension of
    ! a single matrix. So the offset will be (0, 0, step - 1), and
    ! the number of elements to select will be (x, y, 1).
    call h5screate_simple_f(memrank, memdims, memspace, hdferr)

    offset(:) = 0
    offset(filerank) = step - 1
    count(filerank) = 1
    if (memrank > 0) then
       count(1:filerank-1) = shape(data)
    end if

    call h5sselect_hyperslab_f(srcspace, H5S_SELECT_SET_F, offset,&
         & count, hdferr)

    ! Read values
    call h5dread_f(this%v_id, H5T_NATIVE_INTEGER, data, dims,&
         & hdferr, memspace, srcspace)
  end subroutine h5md_element_read_single_i0


  subroutine h5md_element_read_steps(this, steps)
    !! Read the content of an H5MD element.
    !!
    class(nx_h5md_ele_t), intent(in) :: this
    integer, dimension(:), allocatable, intent(out) :: steps

    integer :: hdferr, io
    integer(HSIZE_T) :: num_points
    integer(hid_t) :: s_id ! For space id

    num_points = 0

    ! Get Space ID
    call h5dget_space_f(this%v_id, s_id, hdferr)

    ! Find the number of elements to allocate arrays
    call h5sget_simple_extent_npoints_f(s_id, num_points, hdferr)

    allocate(steps(num_points), stat=io)
    if (io /= 0) STOP "Error in allocation of step()"
    steps(:) = -1

    call h5dread_f(this%s_id, H5T_NATIVE_INTEGER, steps, [num_points], hdferr)
  end subroutine h5md_element_read_steps


  subroutine h5md_read_fixed_c1(loc_id, name, elements)
    integer(hid_t), intent(inout) :: loc_id
    character(len=*), intent(in) :: name
    character(len=*), dimension(:), allocatable, intent(inout) ::&
         & elements

    integer(hid_t) :: filespace, strtype, id
    integer(hsize_t) :: num_points, length
    integer :: hdferr

    call h5dopen_f(loc_id, name, id, hdferr)

    call h5dget_space_f(id, filespace, hdferr)
    call h5sget_simple_extent_npoints_f(filespace, num_points, hdferr)
    allocate(elements(num_points))

    length = len(elements(1))

    call h5md_create_str_type(strtype, length)
    call h5dread_f(id, strtype, elements, [num_points], hdferr)

    call h5dclose_f(id, hdferr)
  end subroutine h5md_read_fixed_c1


  subroutine h5md_read_fixed_d1(loc_id, name, elements)
    integer(hid_t), intent(inout) :: loc_id
    character(len=*), intent(in) :: name
    real(dp), dimension(:), allocatable, intent(inout) ::&
         & elements

    integer(hid_t) :: filespace, id
    integer(hsize_t) :: num_points
    integer :: hdferr

    call h5dopen_f(loc_id, name, id, hdferr)

    call h5dget_space_f(id, filespace, hdferr)
    call h5sget_simple_extent_npoints_f(filespace, num_points, hdferr)
    allocate(elements(num_points))

    call h5dread_f(id, H5T_NATIVE_DOUBLE, elements, [num_points], hdferr)

    call h5dclose_f(id, hdferr)
  end subroutine h5md_read_fixed_d1


  subroutine h5md_check_error(hdferr, msg)
    integer, intent(in) :: hdferr
    character(len=*), intent(in) :: msg

    if (hdferr < 0) then
       write(*, '(A)') 'H5MD ERROR: '//trim(msg)
       error stop
    end if
  end subroutine h5md_check_error


end module mod_h5md
