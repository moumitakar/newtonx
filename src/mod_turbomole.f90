! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_turbomole
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-08-27
  !!
  !! This modules contains all routines and functions related to
  !! running electronic structure computations with TURBOMOLE.
  !! It uses the ``nx_qm_t`` type described in ``mod_qm_t``.
  !! The module exports function necessary to setup, print, update
  !! the input for, run and read information from, the TURBOMOLE
  !! job.  All the exported functions are meant to
  !! be used by the general handler ``mod_qm_general``.
  !!
  !! All functions and routines defined in this module should be
  !! prefixed with ``tm_``.
  !!
  !! LIMITATIONS:
  !!
  !! - CC2 / ADC(2): the specification of frozen core orbitals can only be done with the
  !! syntax:
  !!
  !!     $freeze
  !!       implicit core= ... virt= ...
  !!
  !! meaning that freezing per irreducible representation is not supported.
  use mod_kinds, only: dp
  use mod_interface, only: setenv
  use mod_constants, only: &
       & MAX_CMD_SIZE, MAX_STR_SIZE
  use mod_logger, only: &
       & nx_log, &
       & LOG_WARN, &
       & call_external, check_error, print_conf_ele
  use mod_tools, only: &
       & split_blanks, to_str, split_pattern
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_orbspace, only: nx_orbspace_t
  use mod_trajectory, only: nx_traj_t
  use mod_configuration, only: nx_config_t
  use mod_interface, only: &
       & rm, copy, mkdir, nx_chmod, get_list_of_files
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: tm_setup, tm_print
  public :: tm_update_input
  public :: tm_run
  public :: tm_read
  public :: tm_backup

  public :: tm_get_mos_energies
  public :: tm_get_singles_tddft
  public :: tm_get_singles_cc2
  public :: tm_extract_overlap
  public :: tm_get_lcao
  public :: tm_ricc2_bin2matrix
  public :: tm_init_double_molecule
  public :: tm_prepare_double_molecule
  public :: tm_get_orbspace

  integer, parameter :: MAX_CONTRIB = 6
  !! Maximum number of dominant contributions printed (DFT)

  ! Termination codes
  integer, parameter :: TM_ERR_UPDATE = 201
  integer, parameter :: TM_ERR_MAIN = 202
  integer, parameter :: CIO_TM_BIN2MAT = 203
  integer, parameter :: TM_OVL_PREPARE = 204

  integer  :: TM_VERSION(2) = [0, 0]


contains

  subroutine tm_setup(nx_qm, parser, conf, control)
    !! Setup TURBOMOLE.
    !!
    !! The setup is made from the ``turbomole`` namelist in
    !! ``configfile`` (the main NX configuration file) for general
    !! parameters and from information from the ``control`` file (for
    !! populating the orbital space). This routine also sets the
    !! parallel environment ``OMP_NUM_THREADS`` if required.
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm_t`` object to setup.
    type(parser_t) :: parser
    type(nx_config_t) :: conf
    character(len=*) :: control
    !! TURBOMOLE ``control`` file.

    integer :: ierr

    nx_qm%tm_npre = conf%nstat - 1
    nx_qm%tm_nstart = nx_qm%tm_npre
    call set(parser, 'turbomole', nx_qm%tm_nnodes, 'nnodes')
    call set(parser, 'turbomole', nx_qm%tm_mult, 'multiplicity')
    call set(parser, 'turbomole', nx_qm%tm_npre, 'npre')
    call set(parser, 'turbomole', nx_qm%tm_nstart, 'nstart')

    ! Set the parallel environment if required
    if (nx_qm%tm_nnodes > 1) then
       ierr = setenv( 'OMP_NUM_THREADS', to_str(nx_qm%tm_nnodes) )
    end if

    ! Set the orbital parameters
    call tm_set_orbspace(nx_qm%orb, control)

    ! Overlap parameters
    nx_qm%ovl_cmd = 'dscf'
    nx_qm%ovl_out = 'dscf.out'

    ! ! Set the permissions on JOB_AD folder
    ! ierr = nx_chmod(nx_qm%job_folder, '744')
    ! call check_error(ierr, TM_ERR_MAIN, &
    !      & 'Cannot change permissions on '//nx_qm%job_folder, system=.true.)

    ! Try do decide which version we are using.  This is done by opening the folder
    ! ``TURBODIR``, and look for the file ``TURBOMOLE_$version_Linux``.
    block
      character(len=256), allocatable :: filelist(:)
      character(len=256), allocatable :: group(:)
      character(len=MAX_STR_SIZE) :: env
      integer :: i, j

      call get_environment_variable("TURBODIR", env)

      call get_list_of_files(env, filelist, ierr)
      do i=1, size(filelist)
         if (index(filelist(i), 'TURBOMOLE_') /= 0) then
            if (index(filelist(i), 'Linux') /=0) then
               group = split_pattern(filelist(i), pattern='_')
               print *, (trim(group(j))//' ', j=1, size(group))
               read(group(2)(1:1), *) nx_qm%tm_vers(1)
               read(group(2)(2:len(group(2))), *) nx_qm%tm_vers(2)
            end if
         end if
      end do

      TM_VERSION = [ nx_qm%tm_vers(1), nx_qm%tm_vers(2) ]
    end block

  end subroutine tm_setup


  subroutine tm_print(nx_qm, out)
    !! Print information related to TURBOMOLE execution.
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM object to print.
    integer, intent(in), optional :: out

    integer :: output
    character(len=MAX_STR_SIZE) :: env

    output = stdout
    if (present(out)) output = out

    call get_environment_variable("TURBODIR", env)
    write(output, '(A30, A3, I0, A, I0)') &
         & 'TURBOMOLE Version', ' = ', nx_qm%tm_vers(1), '.', nx_qm%tm_vers(2)
    call print_conf_ele(trim(env), 'TURBODIR path', unit=output)
    call print_conf_ele(nx_qm%tm_nnodes, 'nnodes', unit=output)
    call print_conf_ele(nx_qm%tm_mult, 'mult', unit=output)


    if (nx_qm%tm_npre /= -1) then
       call print_conf_ele(nx_qm%tm_npre, 'npre', unit=output)
    end if
    if (nx_qm%tm_nstart /= -1) then
       call print_conf_ele(nx_qm%tm_nstart, 'nstart', unit=output)
    end if
    call nx_qm%orb%print()
  end subroutine tm_print


  subroutine tm_backup(conf, traj, dbg_dir)
    !! Backup files from the current Turbomole run.
    !!
    !! This should always be called AFTER the cioverlap computation.  By default, the
    !! routine copies the ``scf.out`` and ``grad.out`` files.  If they exist, the output
    !! from ``cioverlap`` (``cis_casida.out`` and ``cioverlap.out`` files) are also
    !! copied.
    !!
    !! If ``conf%lvprt > 4`` (highest feedback level), the whole content of the folder is
    !! saved instead.
    type(nx_config_t), intent(in) :: conf
    !! Main NX configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Trajectory object.
    character(len=*), intent(in) :: dbg_dir
    !! Name of the directory where the information is saved.

    logical :: bck, bck_all, ext
    integer :: ierr
    character(len=256) :: bckfile(11)
    character(len=256) :: rmfile(14)

    bckfile = [ character(len=256) :: &
         & 'scf.out', 'grad.out', 'control.bck', 'control', 'coord', &
         & 'dipl_a', 'sing_a', 'basis', 'mos', 'mos.old', &
         & 'mos.bck' &
         & ]

    rmfile = [ character(len=256) :: &
         & 'scf.out', 'grad.out', 'control.bck', 'control', 'coord', &
         & 'dipl_a', 'sing_a', 'basis', 'dipl_a', 'energy', &
         & 'exspectrum', 'gradient', 'moments', 'statistics' &
         & ]

    bck = (mod(traj%step, conf%save_cwd) == 0)
    bck_all = (conf%lvprt > 4)
    if (bck) then
       ierr = mkdir(dbg_dir)
       call check_error(ierr, TM_ERR_MAIN, &
            & 'Cannot create '//dbg_dir, system=.true.)

       ! ierr = copy(['scf.out ', 'grad.out'], dbg_dir)
       ierr = copy(bckfile, dbg_dir)
       call check_error(ierr, TM_ERR_MAIN, &
            & 'Cannot copy files to '//dbg_dir, system=.true.)

       inquire(file='cioverlap/', exist=ext)
       if (ext) then
          ierr = copy(['cioverlap/cioverlap.out   ', &
                     & 'cioverlap/cis_casida.out  ', &
                     & 'cioverlap/cis_casida.input', &
                     & 'cioverlap/cioverlap.input '],&
                     & dbg_dir)
          call check_error(ierr, TM_ERR_MAIN, &
               & 'Cannot copy cio files to '//dbg_dir, system=.true.)
       end if
    else if (bck_all) then
       ierr = mkdir(dbg_dir)
       call check_error(ierr, TM_ERR_MAIN, &
            & 'Cannot create '//dbg_dir, system=.true.)
       call execute_command_line('cp -rf * '//dbg_dir, exitstat=ierr)
       call check_error(ierr, TM_ERR_MAIN, &
            & 'Cannot copy files to '//dbg_dir, system=.true.)
    end if

    ! ierr = rm( rmfile )
  end subroutine tm_backup


  subroutine tm_set_orbspace(orbspace, control)
    !! Set the orbital space.
    !!
    !! This routine extracts the relevant information from the input
    !! ``control`` file.
    type(nx_orbspace_t), intent(inout) :: orbspace
    !! Orbital space to populate.
    character(len=*), intent(in) :: control
    !! Name of the ``control`` file.

    integer :: u, ierr, id, id2, length
    character(len=512) :: buf1
    character(len=256), allocatable :: split(:)

    ! The number of frozen orbitals is only relevant for CC type
    ! computations. For TD-DFT this won't get executed and the number
    ! of frozen orbitals will be set to 0.
    ! This number can be found in line of the form:
    !
    ! $freeze
    !  implicit core=    nfrozen virt=    nvirt
    !
    ! We want the nfrozen quantity !
    if (orbspace%nfrozen .eq. -1) then
       length = len('implicit core')
       open(newunit=u, file=control, action='read', status='old')
       do
          read(u, '(a)', iostat=ierr) buf1
          if (ierr /= 0) exit
          if (index(buf1, '$freeze') /= 0) then
             read(u, '(A)') buf1
             call split_blanks(buf1, split)
             read(split(3), *) orbspace%nfrozen
             read(split(5), *) orbspace%ndisc
          end if
       end do
       close(u)
    end if

    ! If no frozen orbital has been found, set to 0
    if (orbspace%nfrozen .eq. -1) then
       orbspace%nfrozen = 0
    end if

    if (orbspace%nbas .eq. -1) then
       open(newunit=u, file=control, action='read', status='old')

       do
          read(u, '(a)', iostat=ierr) buf1
          if (ierr /= 0) exit

          ! First the number of occupied
          id = index(buf1, 'closed shells')
          if (id /= 0) then
             read(u, '(a)') buf1
             id = index(buf1, '-')
             id2 = index(buf1, '(')
             read(buf1(id+1:id2-1), *) orbspace%nocc
          end if

          ! Then the number of AOs
          length = len('nbf(AO)=')
          id = index(buf1, 'nbf(AO)=')
          if (id /= 0) then
             read(buf1(id+length:len(buf1)), *) orbspace%nbas
          end if

       end do
       close(u)

       orbspace%nvirt = orbspace%nbas - orbspace%nocc
    end if

    orbspace%nelec = 2*orbspace%nocc - 2*orbspace%nfrozen + 2*orbspace%nfrozen

  end subroutine tm_set_orbspace


  subroutine tm_update_input(nx_qm, traj, conf)
    !! Update the TURBOMOLE input.
    !!
    !! Nx_Qm subroutine cleans the working directory from leftover of
    !! previous QM computations, defines (or updates) ``tm_typedft``
    !! and updates the information in ``control``. Cleaning,
    !! preparation and modification of ``control`` are handled by the
    !! Perl script ``tm_utils.pl``.
    !!
    !! - ``tm_utils.pl clean``: clean the working directory and back
    !! up ``mos`` file.
    !! - ``tm_utils.pl copy``: copy the original input files from
    !! their location.
    !! - ``tm_utils.pl change``: modifies ``control``.
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    type(nx_traj_t), intent(in):: traj
    type(nx_config_t), intent(in) :: conf

    integer :: nstatdyn
    integer :: nstat
    integer :: step, init_step
    integer :: lvprt
    integer :: ierr

    nstatdyn = traj%nstatdyn
    nstat = conf%nstat
    step = traj%step
    init_step = conf%init_step
    lvprt = conf%lvprt

    ! Clean up if it is the second run
    if (step > init_step) then
       call tm_clean_directory(ierr)
       call check_error(ierr, TM_ERR_UPDATE, 'TURBOMOLE: Cannot clean working directory !')
    end if

    ! Copy the original files
    call tm_copy_input_files(nx_qm%job_folder, ierr)
    call check_error(ierr, TM_ERR_UPDATE, 'TURBOMOLE: Cannot copy input files !')

    ! Update control file
    if (nx_qm%qmmethod == 'tddft') then
       call tm_update_input_dft(nx_qm, nstatdyn, nstat)
    else if (nx_qm%qmmethod == 'ricc2') then
       call tm_update_input_cc2(nx_qm, nstatdyn, nstat)
    else if (nx_qm%qmmethod == 'adc2') then
       call tm_update_input_adc2(nx_qm, nstatdyn, nstat)
    end if

    ! Set the different programs to be used.
    if (nx_qm%qmmethod == 'tddft') then
       call tm_set_programs_dft(nx_qm)
    else if (nx_qm%qmmethod == 'ricc2') then
       call tm_set_programs_cc2(nx_qm)
    else if (nx_qm%qmmethod == 'adc2') then
       call tm_set_programs_cc2(nx_qm)
    end if
  end subroutine tm_update_input


  subroutine tm_update_input_dft(nx_qm, nstatdyn, nstat)
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    integer, intent(in) :: nstatdyn
    !! Current state.
    integer, intent(in) :: nstat
    !! Total number of states.

    character(len=MAX_STR_SIZE), allocatable :: control(:)
    character(len=MAX_STR_SIZE), allocatable :: keys(:), values(:)

    ! Update control file
    if (nstatdyn > 1) then
       nx_qm%tm_typedft = 1
    end if

    if ((nstat > 1) .and. (nstatdyn .eq. 1)) then
       nx_qm%tm_typedft = 2
    end if

    if ((nstat .eq. 1) .and. (nstatdyn .eq. 1)) then
       nx_qm%tm_typedft = 3
    end if

    control = tm_read_control('control')

    if (nstatdyn > 1) then
       allocate( keys(2), values(2) )
       keys(1) = 'exopt'
       values(1) = to_str(nstatdyn - 1)

       keys(2) = 'soes'
       valueS(2) = 'all '//to_str(nstat - 1)

       call tm_change_value(control, keys, values)
    else if ((nstatdyn == 1) .and. (nstat > 1)) then
       call tm_change_value(control, &
            & ['soes'], ['all '//to_str(nstat - 1)])
    end if

    call tm_print_control(control, 'control')
  end subroutine tm_update_input_dft


  subroutine tm_update_input_cc2(nx_qm, nstatdyn, nstat)
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    integer, intent(in) :: nstatdyn
    !! Current state.
    integer, intent(in) :: nstat
    !! Total number of states.

    character(len=4) :: ia
    character(len=MAX_STR_SIZE) :: state_info, myfmt
    character(len=MAX_STR_SIZE), allocatable :: controlarray(:)

    if (nx_qm%tm_mult == 1) then
       ia = 'a'
    else if (nx_qm%tm_mult == 3) then
       ia = 'a{3}'
    end if

    if (nstatdyn > 1) then
       myfmt = '(&
            & A,"irrep=a multiplicity=", I0, " nexc=", I0, " npre=", I0, &
            & " nstart=", I0, A, " exgrad states=(", A, " ", I0, ")", &
            & A, " spectrum states=all operators=diplen" &
            &)'
       write(state_info, fmt=myfmt) &
            & NEW_LINE('c'), nx_qm%tm_mult, nstat-1, nx_qm%tm_npre, nx_qm%tm_nstart,&
            & NEW_LINE('c'), ia, nstatdyn-1, NEW_LINE('c')


    else if (nstatdyn == 1) then
       myfmt = '(&
            & A,"irrep=a multiplicity=", I0, " nexc=", I0, " npre=", I0, &
            & " nstart=", I0, A, " spectrum states=all operators=diplen"&
            &)'
       write(state_info, fmt=myfmt) &
            & NEW_LINE('c'), nx_qm%tm_mult, nstat-1, nx_qm%tm_npre, nx_qm%tm_nstart,&
            & NEW_LINE('c')
    end if

    controlarray = tm_read_control('control')
    call tm_print_control(controlarray, 'control.tmp1')
    call tm_change_value(controlarray, ['excitations'], [state_info])
    call tm_print_control(controlarray, 'control.tmp')

    myfmt = tm_get_value(controlarray, 'ricc2')

    if ( index(myfmt, 'geoopt') == 0 ) then
       call tm_change_value(controlarray, ['ricc2'], &
            & [NEW_LINE('c')//'cc2'//NEW_LINE('c')//'maxiter=300'//NEW_LINE('c')//'geoopt model=cc2']&
            &)
    end if

    call tm_print_control(controlarray, 'control')
  end subroutine tm_update_input_cc2


  subroutine tm_update_input_adc2(nx_qm, nstatdyn, nstat)
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    integer, intent(in) :: nstatdyn
    !! Current state.
    integer, intent(in) :: nstat
    !! Total number of states.

    character(len=4) :: ia
    character(len=MAX_STR_SIZE) :: state_info, ricc2_info, myfmt
    character(len=MAX_STR_SIZE), allocatable :: controlarray(:), controlarray2(:)
    character(len=1) :: nl

    nl = NEW_LINE('c')
    if (nx_qm%tm_mult == 1) then
       ia = 'a'
    else if (nx_qm%tm_mult == 3) then
       ia = 'a{3}'
    end if

    if (nstatdyn > 1) then
       controlarray = tm_read_control('control')
       myfmt = '(&
            & A,"irrep=a multiplicity=", I0, " nexc=", I0, " npre=", I0, &
            & " nstart=", I0, A, " exgrad states=(", A, " ", I0, ")", &
            & A, " spectrum states=all operators=diplen" &
            &)'
       write(state_info, fmt=myfmt) &
            & NEW_LINE('c'), nx_qm%tm_mult, nstat-1, nx_qm%tm_npre, nx_qm%tm_nstart,&
            & NEW_LINE('c'), ia, nstatdyn-1, NEW_LINE('c')

       call tm_change_value(controlarray, ['excitations'], [state_info])
       ricc2_info = tm_get_value(controlarray, 'ricc2')

       if ( index(ricc2_info, 'geoopt') == 0 ) then
          print *, 'CHANGING RICC2'
          ricc2_info = nl//'adc2'//nl//'maxiter=300'//nl//'geoopt model=adc(2)'
          call tm_change_value(controlarray, ['ricc2'], [ricc2_info])
       end if
       call tm_print_control(controlarray, 'control')

    else if (nstatdyn == 1) then
       controlarray = tm_read_control('control')
       allocate( controlarray2( size(controlarray) ) )
       controlarray2(:) = controlarray(:)

       call tm_delete_keyword(controlarray2, ['excitations'])

       ricc2_info = nl//'mp2'//nl//'maxiter=300'//nl//'geoopt model=mp2'
       call tm_change_value(controlarray2, ['ricc2'], [ricc2_info])
       call tm_print_control(controlarray2, 'control-mp2')

       myfmt = '(&
            & A,"irrep=a multiplicity=", I0, " nexc=", I0, " npre=", I0, &
            & " nstart=", I0, A, " spectrum states=all operators=diplen"&
            &)'
       write(state_info, fmt=myfmt) &
            & NEW_LINE('c'), nx_qm%tm_mult, nstat-1, nx_qm%tm_npre, nx_qm%tm_nstart,&
            & NEW_LINE('c')
       call tm_change_value(controlarray, ['excitations'], [state_info])

       ricc2_info = nl//'adc(2)'//nl//'maxiter=300'
       call tm_change_value(controlarray, ['ricc2'], [ricc2_info])
       call tm_print_control(controlarray, 'control-adc2')
    end if
  end subroutine tm_update_input_adc2


  subroutine tm_set_programs_dft(nx_qm)
    type(nx_qm_t) :: nx_qm

    logical :: exists

    inquire(file='auxbasis', exist=exists)
    if (exists) then
       nx_qm%tm_scf_prog = 'ridft'
       nx_qm%tm_grad_prog = 'rdgrad'
    else
       if (nx_qm%tm_nnodes > 1) then
          nx_qm%tm_scf_prog = 'dscf_smp'
          nx_qm%tm_grad_prog = 'grad_smp'
       else
          nx_qm%tm_scf_prog = 'dscf'
          nx_qm%tm_grad_prog = 'grad'
       end if
    end if

    if (nx_qm%tm_typedft .eq. 1) then
       nx_qm%tm_grad_prog = 'egrad'
    else if (nx_qm%tm_typedft .eq. 2) then
       nx_qm%tm_en_prog = 'escf'
       nx_qm%tm_grad_prog = 'grad'
    end if
  end subroutine tm_set_programs_dft


  subroutine tm_set_programs_cc2(nx_qm)
    !! Set the TURBOMOLE programs for RICC2 and ADC(2) dynamics.
    type(nx_qm_t) :: nx_qm

    nx_qm%tm_scf_prog = 'dscf'
    nx_qm%tm_grad_prog = 'ricc2'
    if (nx_qm%tm_nnodes > 1) then
       nx_qm%tm_scf_prog = 'dscf_smp'
       nx_qm%tm_grad_prog = 'ricc2_smp'
    end if
  end subroutine tm_set_programs_cc2


  subroutine tm_run(nx_qm)
    !! Execute TURBOMOLE.
    !!
    !! The SCF output will be in ``scf.out`` and the gradient outputs in
    !! ``grad.out`` (for ES gradient) or ``grad_s0.out`` (for GS
    !! gradient).
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.

    character(len=MAX_CMD_SIZE) :: command
    integer :: ierr

    ! Back up control file
    ierr = copy( 'control', 'control.bck' )
    ierr = copy( 'mos', 'mos.bck' )

    ! SCF step
    write(command, '(A)') trim(nx_qm%tm_scf_prog)
    call call_external(command, ierr, outfile='scf.out')
    call check_error(ierr, TM_ERR_MAIN)

    if (nx_qm%qmmethod == 'tddft') then
       call tm_run_dft(nx_qm)
    else if (nx_qm%qmmethod == 'ricc2') then
       call tm_run_cc2(nx_qm)
    else if (nx_qm%qmmethod == 'adc2') then
       call tm_run_adc2(nx_qm)
    end if
  end subroutine tm_run


  subroutine tm_run_dft(nx_qm)
    type(nx_qm_t) :: nx_qm

    character(len=MAX_CMD_SIZE) :: command
    integer :: ierr
    ! Gradients and ES energies
    if (nx_qm%tm_typedft .eq. 1) then
       write(command, '(A, A3, A)') trim(nx_qm%tm_grad_prog)
       call call_external(command, ierr, outfile='grad.out')
       call check_error(ierr, TM_ERR_MAIN)
    else if (nx_qm%tm_typedft .eq. 2) then
       write(command, '(A, A3, A)') trim(nx_qm%tm_grad_prog)
       call call_external(command, ierr, outfile='grad_s0.out')
       call check_error(ierr, TM_ERR_MAIN)

       write(command, '(A, A3, A)') trim(nx_qm%tm_en_prog)
       call call_external(command, ierr, outfile='grad.out')
       call check_error(ierr, TM_ERR_MAIN)
    else if (nx_qm%tm_typedft .eq. 3) then
       write(command, '(A, A3, A)') trim(nx_qm%tm_grad_prog)
       call call_external(command, ierr, outfile='grad.out')
       call check_error(ierr, TM_ERR_MAIN)
    end if
  end subroutine tm_run_dft


  subroutine tm_run_cc2(nx_qm)
    type(nx_qm_t) :: nx_qm

    character(len=MAX_CMD_SIZE) :: command
    integer :: ierr

    write(command, '(A, A3, A)') trim(nx_qm%tm_grad_prog)
    call call_external(command, ierr, outfile='grad.out')
    call check_error(ierr, TM_ERR_MAIN)
  end subroutine tm_run_cc2


  subroutine tm_run_adc2(nx_qm)
    type(nx_qm_t) :: nx_qm

    character(len=MAX_CMD_SIZE) :: command
    logical :: ext
    integer :: ierr

    ! This is for GS only dynamics (nstatdyn = 1)
    inquire(file='control-mp2', exist=ext)
    if (ext) then
       call execute_command_line("cp -f control-mp2 control")
    end if

    write(command, '(A, A3, A)') trim(nx_qm%tm_grad_prog)
    call call_external(command, ierr, outfile='grad.out')
    call check_error(ierr, TM_ERR_MAIN)

    inquire(file='control-adc2', exist=ext)
    if (ext) then
       call execute_command_line("cp -f control-adc2 control")
       call call_external(command, ierr, outfile='grad.out')
       call check_error(ierr, TM_ERR_MAIN)
    end if

  end subroutine tm_run_adc2


    subroutine tm_read(nx_qm, nat, nstatdyn, qminfo,  stat)
    !! Read TURBBOMOLE DFT outputs and report.
    !!
    !! ``gradfile`` output should come from a gradient computation. The
    !! current potential energies, acceleration, and gradient from the
    !! trajectory are backed up in ``traj%old_*``. The new values are
    !! then read in the output and stored in the ``traj`` object. The
    !! oscillator strength are also reported.
    !! TODO: Implement the reporting of ES .
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    integer, intent(in) :: nat
    !! Number of atoms (required for reading the gradient file).
    integer, intent(in) :: nstatdyn
    !! Current state (required for filling the ``rgrad`` array).
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Extracted information from QM job.
    integer, intent(out) :: stat
    !! Status.

    select case(nx_qm%qmmethod)
    case ("tddft")
       call tm_read_dft(nx_qm, nat, nstatdyn, qminfo, stat)
    case ("ricc2")
       call tm_read_cc2(nx_qm, nat, nstatdyn, qminfo, stat)
    case ("adc2")
       call tm_read_cc2(nx_qm, nat, nstatdyn, qminfo, stat)
    end select

  end subroutine tm_read


  subroutine tm_read_dft(nx_qm, nat, nstatdyn, qminfo, ierr)
    !! Read TURBBOMOLE DFT outputs and report.
    !!
    !! ``gradfile`` output should come from a gradient computation. The
    !! current potential energies, acceleration, and gradient from the
    !! trajectory are backed up in ``traj%old_*``. The new values are
    !! then read in the output and stored in the ``traj`` object. The
    !! oscillator strength are also reported.
    !! TODO: Implement the reporting of ES .
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    integer, intent(in) :: nat
    !! Number of atoms (required for reading the gradient file).
    integer, intent(in) :: nstatdyn
    !! Current state (required for filling the ``rgrad`` array).
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Extracted information from QM job.
    integer, intent(inout) :: ierr
    !! Status.

    character(len=MAX_STR_SIZE) :: line
    integer :: i, u, j

    integer :: id

    character(len=MAX_STR_SIZE), dimension(:, :), allocatable :: contrib

    character(len=1024) :: msg
    character(len=:), allocatable :: scffile

    allocate(contrib(size(qminfo%repot)-1, MAX_CONTRIB))
    contrib(:, :) = 'NULL'

    scffile = 'grad.out'
    if (nx_qm%tm_typedft == 3) scffile = 'scf.out'

    ! Find energies, oscillator strengths, and contributions to
    ! excited states (if relevant)
    call tm_parse_output(scffile, qminfo%repot, nx_qm%osc_str, contrib, nx_qm%tm_typedft)

    ! if (nx_qm%tm_typedft == 3) then
    !    call tm_parse_output('scf.out', qminfo%repot, nx_qm%osc_str, contrib, &
    !         & nx_qm%tm_typedft)
    ! else
    !    call tm_parse_output('grad.out', qminfo%repot, nx_qm%osc_str, contrib, &
    !         & nx_qm%tm_typedft)
    ! end if


    ! Now report what we found
    if (nx_qm%tm_typedft /= 3) then
       call qminfo%append_data('Dominant contributions and oscillator strengths')

       do i=1, size(qminfo%repot) - 1
          write(msg, '(a,i0,a,F14.10)') '  Excitation ', i, ': f = ', nx_qm%osc_str(i)
          call qminfo%append_data(msg)
          do j=1, size(contrib, 2)
             id = index(contrib(i, j), 'NULL')
             if ((id == 0) .and. (contrib(i,j) /= '')) then
                write(msg, '(A,A)') '      ', trim(contrib(i, j))
                call qminfo%append_data(msg)
             end if
          end do
          call qminfo%append_data(msg)
       end do
    end if

    open(newunit=u, file='gradient', status='old', action='read')
    ! We are not interested in the first Nat + 2 lines
    do i=1, nat + 2
       read(u, '(A)') line
    end do
    i = 1
    do
       read(u, '(A)', iostat=ierr) line
       if (ierr /= 0) exit
       if (line .eq. '$end') exit
       read(line, *) (qminfo%rgrad(nstatdyn, j, i), j=1, 3)
       i = i+1
    end do
    close(u)

  end subroutine tm_read_dft


  subroutine tm_parse_output(filename, epot, osc, contrib, typedft)
    !! We assume that osc and epot are already initialized to their
    !! right dimension. The dimensions given are general
    character(len=*), intent(in) :: filename
    real(dp), dimension(:), intent(inout) :: epot
    real(dp), dimension(:), intent(inout) :: osc
    character(len=*), dimension(:, :), intent(inout) :: contrib
    integer, intent(in) :: typedft

    integer :: u, id, ierr
    integer :: iepot, icontrib1, icontrib2, iosc
    character(len=MAX_STR_SIZE) :: line
    character(len=MAX_STR_SIZE), dimension(:), allocatable :: split

    iepot = 1
    iosc = 1
    icontrib1 = 1 ! Indexes the state
    icontrib2 = 1 ! Indexes the number of contributions

    open(newunit=u, file=filename, status='old', action='read')
    do
       read(u, '(a)', iostat=ierr) line
       if (ierr /= 0) exit

       ! Line contains the total energy
       if (typedft == 3) then
          id = index(line, '|  total energy')
          if (id /= 0) then
             call split_blanks(line, split)
             read(split(5), *) epot(iepot)
          end if
       else
          id = index(line, 'Total energy')
          if (id /= 0) then
             call split_blanks(line, split)
             read(split(3), *) epot(iepot)
             iepot = iepot + 1
          end if
       end if

       ! Line is an oscillator strength
       id = index(line, 'Oscillator strength:')
       if (id /= 0) then
          FIND_OSC: do

             read(u, '(a)') line
             id = index(line, 'length representation:')
             if (id /= 0) then
                id = index(line, '.')
                if (id /= 0) then
                   read(line(id-3:len(line)), '(E24.16)') osc(iosc)
                   iosc = iosc + 1
                end if
             end if

             id = index(line, 'Rotatory strength:')
             if (id /= 0) exit FIND_OSC
          end do FIND_OSC
       end if

       ! Dominant contributions
       id = index(line, 'Dominant contributions:')
       if (id /= 0) then
          read(u, '(a)') line ! This one is empty
          read(u, '(a)') line ! This one contains headers
          contrib(icontrib1, icontrib2) = trim(adjustl(line))
          icontrib2 = icontrib2 + 1
          FIND_CONTRIB: do
             read(u, '(a)') line
             id = index(line, 'Change of')
             if (id /= 0) exit FIND_CONTRIB

             contrib(icontrib1, icontrib2) = trim(adjustl(line))
             icontrib2 = icontrib2 + 1
             if (icontrib2 > MAX_CONTRIB) exit FIND_CONTRIB

          end do FIND_CONTRIB
          icontrib1 = icontrib1 + 1
          icontrib2 = 1
       end if

    end do

    close(u)

  end subroutine tm_parse_output


  subroutine tm_read_cc2(nx_qm, nat, nstatdyn, qminfo, ierr)
    !! Read TURBBOMOLE CC2 outputs and report.
    !!
    !! ``gradfile`` output should come from a gradient computation. The
    !! current potential energies, acceleration, and gradient from the
    !! trajectory are backed up in ``traj%old_*``. The new values are
    !! then read in the output and stored in the ``traj`` object. The
    !! oscillator strength are also reported.
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    integer, intent(in) :: nat
    !! Number of atoms (required for reading the gradient file).
    integer, intent(in) :: nstatdyn
    !! Current state (required for filling the ``rgrad`` array).
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Extracted information from QM job.
    integer, intent(out) :: ierr
    !! Status.

    character(len=MAX_STR_SIZE) :: line
    integer :: u, id, ien, i, j, nstat, iosc
    character(len=MAX_STR_SIZE), allocatable :: splitted(:)
    character(len=MAX_STR_SIZE), allocatable :: contrib1(:, :)
    ! For excited states orbital contributions
    character(len=MAX_STR_SIZE), allocatable :: contrib2(:)
    ! Contribution of each excited state
    real(dp), allocatable :: norms(:)
    real(dp) :: entmp, d1mp2, d1cc2

    character(len=1024) :: msg

    ien = 0 ! Loop over excited states
    d1mp2 = -1.0_dp
    d1cc2 = -1.0_dp
    iosc = 1 ! Loop over oscillator strengths

    ! Some allocations
    nstat = size(qminfo%repot)
    allocate( contrib1(nstat-1, 4+MAX_CONTRIB+2) )
    allocate( contrib2(5+nstat+1) )
    allocate( norms(nstat-1) )

    contrib1(:, :) = 'NULL'
    contrib2(:) = 'NULL'
    norms(:) = -1.0_dp
    nx_qm%osc_str(:) = -1.0_dp

    open(newunit=u, file='grad.out', action='read')
    do
       read(u, '(A)', iostat=ierr) line
       if (ierr /= 0) exit

       ! Find Ground state energy
       id = index(line, 'Final CC2 energy')
       if (id /= 0) then

          if (nx_qm%qmmethod == 'ricc2') then
             call split_blanks(line, splitted)
             read(splitted(6), *) qminfo%repot(1)
          end if

          read(u, '(a)') line
          read(u, '(a)') line
          call split_blanks(line, splitted)
          read(splitted(5), *) d1cc2
       end if

       id = index(line, 'Final MP2 energy')
       if (id /= 0) then
          if (nx_qm%qmmethod == 'adc2') then
             ! Output file sample:
             !**********************************************************************
             !*                                                                    *
             !*   RHF  energy                             :   -328.8936700776      *
             !*   MP2 correlation energy                  :     -0.2124570545      *
             !*                                                                    *
             !*   Final MP2 energy                        :   -329.1061271321      *
             !*                                                                    *
             !*   Norm of MP1 T2 amplitudes               :      0.1842261354      *
             !*                                                                    *
             !*   D1 diagnostic                           :      0.0302            *
             !*                                                                    *
             !**********************************************************************
             call split_blanks(line, splitted)
             read(splitted(6), *) qminfo%repot(1)
             do i=1, 4
                read(u, '(a)') line
             end do

             call split_blanks(line, splitted)
             read(splitted(5), *) d1mp2
          else
             read(u, '(a)') line
             call split_blanks(line, splitted)
             read(splitted(5), *) d1mp2
          end if
       end if

       ! Excited states energies
       id = index(line, 'Energy:')
       if (id /= 0) then
          ! HERE WE UPDATE THE ENERGY INDEX ien
          ! This influences the next parts: find 'type RE0' and find 'norm of
          ! printed elements'
          ien = ien + 1
          call split_blanks(line, splitted)
          read(splitted(2), *) entmp
          qminfo%repot(ien+1) = qminfo%repot(1) + entmp
       end if

       ! Excited state informations
       id = index(line, ' type: RE0')
       if (id /= 0) then
          read(line, '(a)') contrib1(ien, 1)
          do i=1, 3
             read(u, '(a)') line
             read(line, '(a)') contrib1(ien, i + 1)
          end do

          CONTRIBUTIONS1: do i=1, MAX_CONTRIB
             read(u, '(a)') line
             id = index(line, '=====')
             if (id == 0) then
                read(line, '(a)') contrib1(ien, i + 4)
             else
                exit CONTRIBUTIONS1
             end if
          end do CONTRIBUTIONS1
       end if

       id = index(line, 'norm of printed elements')
       if (id /= 0) then
          call split_blanks(line, splitted)
          read(splitted(5), *) norms(ien)
       end if

       id = index(line, 'contributions of excitation levels to')
       if (id /= 0) then
          read(line, '(a)') contrib2(1)
          do i=1, 4
             read(u, '(a)') line
             read(line, '(a)') contrib2(i + 1)
          end do

          CONTRIBUTIONS2: do i=1, nstat
             read(u, '(a)') line
             read(line, '(a)') contrib2(i + 5)
             ! if ((line == ' ') .or. (line == '')) then
             !    exit CONTRIBUTIONS2
             ! end if
             ! write(*, *) trim(line)
          end do CONTRIBUTIONS2
          read(u, '(a)') line
          read(line, '(a)') contrib2(size(contrib2))
          ! write(*, *) ''
       end if

       ! Oscillator strengths
       id = index(line, 'oscillator strength (length gauge)')
       if (id /= 0) then
          call split_blanks(line, splitted)
          read(splitted(6), *) nx_qm%osc_str(iosc)
          iosc = iosc + 1
       end if
    end do
    close(u)

    ! res = message('')
    if (d1mp2 > 0) then
       write(msg, *) 'D1 diagnostic for MP2: ', d1mp2
       call nx_log%log(LOG_WARN, msg)
       ! res = message(msg)
    end if

    if (d1cc2 > 0) then
       write(msg, *) 'D1 diagnostic for CC2: ', d1cc2
       call nx_log%log(LOG_WARN, msg)
       ! res = message(msg)
    end if

    if ((d1mp2 > 0.04) .or. (d1mp2 > 0.05)) then
       msg = "   D1 diagnostic indicates that MP2"//NEW_LINE('a')
       msg = trim(msg)//"   and CC2 may be inadequate to describe the"//NEW_LINE('a')
       msg = trim(msg)//"   ground state for this geometry.          "//NEW_LINE('a')
       call nx_log%log(LOG_WARN, msg)
    end if

    if (size(qminfo%repot) > 1) then
       call qminfo%append_data('Information on the excited states:')

       do i=1, size(contrib1, 1)
          do j=1, size(contrib1, 2)
             if (contrib1(i, j) /= 'NULL') then
                write(msg, *) trim(contrib1(i, j))
                call qminfo%append_data(msg)
             end if
          end do
          write(msg, '(A, F8.6)')&
               & '      norm of printed elements: ', norms(i)
          msg = trim(msg)//NEW_LINE('a')
          call qminfo%append_data(msg)
       end do
       call qminfo%append_data('')

       do i=1, size(contrib2)
          write(msg, *) trim(contrib2(i))
          call qminfo%append_data(msg)
       end do

       do i=1, size(nx_qm%osc_str)
          write(msg, '(a,i0,a,i0,a,F10.6)') &
               & 'Oscillator strength (', 1, ',', i+1, ') = ', nx_qm%osc_str(i)
          call qminfo%append_data(msg)
       end do
    end if

    open(newunit=u, file='gradient', status='old', action='read')
    ! We are not interested in the first Nat + 2 lines
    do i=1, nat + 2
       read(u, '(A)') line
    end do
    i = 1
    do
       read(u, '(A)', iostat=ierr) line
       if (ierr /= 0) exit
       if (line .eq. '$end') exit
       read(line, *) (qminfo%rgrad(nstatdyn, j, i), j=1, 3)
       i = i+1
    end do
    close(u)

    deallocate(contrib1)
    deallocate(contrib2)

  end subroutine tm_read_cc2


  subroutine tm_get_mos_energies(mos_file, mos)
    !! Read the MO energies from ``mos_file``.
    !!
    !! Exit the program with an error if the number of
    !! energies found is lower than the expected number of orbitals.
    character(len=*), intent(in) :: mos_file
    !! File to read.
    real(dp), dimension(:), intent(out) :: mos
    !! Allocated vector containing MO energies

    integer :: u, id, ao, ierr
    character(len=512) :: buf
    ao = 1
    open(newunit=u, file=mos_file, status='old', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) then
          if (ao < size(mos)) then
             write(*, *) 'ERROR: The number of eigenvalues in mos is lower'
             write(*, *) '       than the number of Atomic Orbitals !'
             STOP
          else
             exit
          end if
       end if
       id = index(buf, 'eigenvalue')
       if (id /= 0) then
          read(buf(id+len('eigenvalue')+1:id+len('eigenvalue')+21),&
               & '(d20.14)') mos(ao)
          ao = ao + 1
       end if
    end do
    close(u)
  end subroutine tm_get_mos_energies


  subroutine tm_get_singles_tddft(orb, singfile, tia)
    !! Read the singles amplitudes contained in ``singfile``.
    !!
    !! These amplitudes are written in ``singfile`` in
    !! matrix form, in blocks of 4 columns. We have to read a number
    !! of elements corresponding to the total dimension of the CIS
    !! problem (NOCC x NVIRT). For simplicity, these elements are read
    !! in a vector. Once enough element have been read, the vector is
    !! reshaped in matrix form and the matrix is then stored at the
    !! correct position in the ``tia`` array.
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space.
    character(len=*), intent(in) :: singfile
    !! File to read.
    real(dp), dimension(:, :, :), intent(out) :: tia
    !! Matrix containing the single amplitudes for each excitation.
    !! The last dimension should correspond to the total number of excitations.

    integer :: u, id, ierr, j, i
    integer :: state
    integer :: full_dim
    integer :: nele
    integer :: nbLines, remainder
    character(len=512) :: buf
    character(len=9) :: fmt_spec
    real(dp), dimension(:), allocatable :: tia_vec
    integer, dimension(2) :: tia_shape

    ! tia_vec is a vector containing all singles amplitudes in line
    ! it is aimed at being reshaped for storage in tia

    ! full_dim = (orb%nocc - orb%nfrozen + 1) * orb%nvirt
    full_dim = size(tia, 1) * size(tia, 2)
    allocate( tia_vec(full_dim) )

    ! For reshaping the vector at the end
    ! tia_shape = (/ orb%nvirt, (orb%nocc - orb%nfrozen + 1) /)
    tia_shape = shape( tia(:, :, 1) )

    ! The nocc * nvirt amplitudes are stored in 4 columns
    ! This amounts to nbLines full lines and rest remaining lines, with
    ! nbLines = int( nocc*nvirt / 4 )
    ! rest = mod( nocc*nvirt / 4)
    ! The remaining lines in sing_a should correspond to deexcitation
    ! amplitudes ???
    nbLines = int( full_dim / 4 )
    remainder = modulo( full_dim, 4 )

    ! Format specifier for reading the remaining lines
    write(fmt_spec, '(a1, i1, a7)') '(', remainder, 'D20.14)'

    state = 0
    nele = 0

    open(newunit=u, file=singfile, status='old', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       ! File has ended
       if (ierr /= 0) then
          exit
       end if

       ! Found '$end'
       if (buf(1:4) .eq. '$end') then
          exit
       end if

       ! New state
       id = index(buf, 'eigenvalue')
       if (id /=0 ) then
          state = state+1
          nele = 0

          do i=1, nbLines
             read(u, '(4D20.14)') (tia_vec(nele + j), j=1, 4)
             nele = nele + 4
          end do

          if (remainder /= 0) then
             read(u, fmt=fmt_spec) (tia_vec(nele+j), j=1, remainder)
          end if

          tia(:, :, state) = reshape(tia_vec, tia_shape)
       end if
    end do
    close(u)

    do state=1, size(tia, 3)
       do i=1, size(tia, 2)
          do j=1, size(tia, 1)
             if (abs(tia(j, i, state)) > 0.5) then
                print '(A,I0,A,I0,A,I0,A,F12.8,A,F12.8,A)', &
                     & 'STATE ', state, ': excitation from ', i, ' to ', j, &
                     & ' (coeff ', tia(j, i, state), &
                     & ', coeff**2 ', tia(j, i, state)**2, ')'
             end if
          end do
       end do
    end do
    
    deallocate(tia_vec)

  end subroutine tm_get_singles_tddft


  subroutine tm_get_singles_cc2(orb, singfile, tia)
    type(nx_orbspace_t) :: orb
    character(len=*) :: singfile
    real(dp), dimension(:, :, :) :: tia

    integer :: u, i
    integer :: istat
    character(len=MAX_STR_SIZE) :: buf

    open(newunit=u, file=singfile, action='read')
    do istat=1, size(tia, 3)
       ! Discard the first line
       read(u, *) buf
       do i=1, orb%nocc - orb%nfrozen
          read(u, *) tia(:, i, istat)
       end do
    end do
    close(u)
  end subroutine tm_get_singles_cc2



  subroutine tm_extract_overlap(source, dim, ovl)
    !! Extract overlap matrix from a TURBOMOLE computation.
    !!
    !! The symmetric matrix is stored in vector form in ``ovl``.
    character(len=*), intent(in) :: source
    !! TURBOMOLE output to read.
    integer, intent(in) :: dim
    !! Dimension of the overlap matrix
    real(dp), dimension(:), intent(out) :: ovl
    !! Resulting symmetric matrix in vector form.

    integer :: u, ierr
    integer :: id
    character(len=512) :: line
    character(len=13) :: fmt_spec
    integer :: nele
    integer :: remainder
    integer :: j

    ! For TM 7.5 and higher
    integer :: nlines, n, i, nl_in_matrix

    nele = 0                    ! Number of elements already read
    remainder = mod(dim, 3)
    print *, 'DIM = ', dim

    write(fmt_spec, '(A4, I0, A7)') '(', remainder, 'E24.14)'
    open(newunit=u, file=source, action='read', status='old')
    do
       read(u, '(a)', iostat=ierr) line
       if (ierr /= 0) exit
       id = index(line, 'OVERLAP')
       if (id /= 0) then
          read(u, '(a)', iostat=ierr) line ! Read '-----' line

          if (TM_VERSION(2) < 5) then
             do
                read(u, '(a)', iostat=ierr) line
                if (nele < dim - remainder) then
                   read(line, '(3E24.14)') (ovl(nele+j), j=1, 3)
                   nele = nele + 3
                else if ((nele >= dim-remainder) .and. (nele <&
                     & dim)) then
                   read(line, fmt=fmt_spec) (ovl(nele+j), j=1, remainder)
                   nele = nele + remainder
                else
                   exit
                end if
             end do
          else
             nl_in_matrix = (-1 + sqrt(1.0+8*dim)) / 2
             ! print *, 'NLINES = ', nl_in_matrix
             do n=1, int(nl_in_matrix)
                nlines = n / 3
                ! print *, 'nlines = ', nlines
                remainder = mod(n, 3)
                ! print *, 'remainder = ', remainder
                do i=1, nlines
                   read(u, *) (ovl(nele+j), j=1,3)
                   nele = nele + 3
                end do
                if (remainder > 0) then
                   read(u, *) (ovl(nele+j), j=1, remainder)
                   nele = nele + remainder
                end if
             end do
          end if
       end if

    end do
    close(u)

  end subroutine tm_extract_overlap


  subroutine tm_get_lcao(mos_file, nao, lcao)
    !! Extract AO to MO conversion matrix from ``mos_file``.
    !!
    !! LCAO has the ordering (AO, MO).
    character(len=*), intent(in) :: mos_file
    !! File to read.
    integer, intent(in) :: nao
    !! Number of atomic orbitals.
    real(dp), dimension(:, :), intent(out) :: lcao
    !! Resulting AO to MO conversion matrix.

    integer :: u, ierr
    integer :: id
    character(len=512) :: buf
    character(len=16) :: fmt_spec

    integer :: nele, remainder, nbLines
    integer :: ll ! For counting the complete lines already read
    integer :: i
    integer :: mo

    ! The mos file is organised in 4 columns, and we have to read this%nao
    ! elements. The number of complete lines is stored in nbLines
    remainder = mod(nao, 4)
    nbLines = int(nao / 4)

    ! Format specifier for incomplete lines
    write(fmt_spec, '(A1, I0, A7)') '(', remainder, 'D20.14)'

    mo = 0
    nele = 0
    open(newunit=u, file=mos_file, status='old', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       ! End of file
       if (ierr /= 0) exit

       ! New MO
       id = index(buf, 'eigenvalue')
       if (id /= 0) then
          mo = mo + 1
          nele = 0
       end if

       ! Coefficients
       if ((buf(1:1) .eq. '-') .or. (buf(1:1) .eq. '0')) then
          ll = int(nele / 4)
          if (ll < nbLines) then
             ! If we still have elements in complete lines to read
             read(buf, '(4D20.14)') ( lcao(mo, nele+i), i=1, 4 )
             nele = nele + 4
          else if ((ll == nbLines) .and. (remainder /= 0)) then
             ! Else if incomplete lines exist, read it too
             read(buf, fmt=fmt_spec) ( lcao(mo, nele+i), i=1, remainder )
             nele = nele + remainder
          end if
       end if
    end do
    close(u)

    ! Write lcao
    lcao = transpose(lcao)
  end subroutine tm_get_lcao


  subroutine tm_ricc2_bin2matrix(nstat, mult, ciopath, nocc, nfrozen, nvirt)
    integer :: nstat
    integer :: mult
    character(len=*) :: ciopath
    integer :: nocc
    integer :: nfrozen
    integer :: nvirt

    integer :: offset
    character(len=MAX_STR_SIZE) :: filename
    character(len=MAX_CMD_SIZE) :: command
    integer :: i, ierr

    ! This value is for Turbomole version > 7.6
    offset = 48

    ! For version 7.3 we need to adapt
    if (TM_VERSION(2) == 3) offset = 56
    do i=1, nstat-1
       write(filename, '(a,i0,a,i0)') 'CCRE0-1--', mult, '---', i
       write(command, *) &
            & trim(ciopath), '/bin2matrix -o ', offset, ' -d ', nocc - nfrozen, nvirt,&
            & trim(filename), ' >> bin2matrix.out'
       ierr = 0
       call execute_command_line(command, exitstat=ierr)
       call check_error(ierr, CIO_TM_BIN2MAT)
    end do

  end subroutine tm_ricc2_bin2matrix


  function tm_read_control(controlfile) result(res)
    character(len=*), intent(in) :: controlfile

    character(len=MAX_STR_SIZE), allocatable :: res(:)

    integer :: u, ierr, i, id
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: temp(:)

    allocate(temp(1024)) ! We assume the control file to have at most 1024 lines

    id = 0
    open(newunit=u, file=controlfile, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, '$') /= 0) then
          id = id + 1
          temp(id) = trim(buf)
       else
          temp(id) = trim(temp(id))//NEW_LINE('a')//trim(buf)
       end if
    end do
    close(u)

    allocate(res(id))
    do i=1, id
       res(i) = trim(temp(i))
    end do
  end function tm_read_control


  subroutine tm_change_value(controlarray, keys, values)
    character(len=*), allocatable, intent(inout) :: controlarray(:)
    character(len=*), intent(in) :: keys(:)
    character(len=*), intent(in) :: values(:)

    integer :: i, j, notfound
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    integer, allocatable :: changed(:)

    allocate( changed(size(keys)) )
    changed(:) = 0
    do i=1, size(controlarray)
       call split_blanks(controlarray(i), split)

       do j=1, size(keys)
          if ( index(split(1), '$'//trim(keys(j))) /= 0 ) then
             ! We found one of the key we want to change
             changed(j) = 1
             controlarray(i) = '$'//trim(keys(j))//' '//trim(values(j))
          end if
       end do
    end do

    notfound = count( changed == 0 )
    if (notfound /= 0) then
       ! Here one of the keys couldn't be found, so we have to extend our initial control
       ! array by the number of keys that we not found.
       block
         character(len=MAX_STR_SIZE) :: tmp( size(controlarray) - 1 + notfound )
         character(len=MAX_STR_SIZE) :: last
         integer :: ssize

         ssize = size(controlarray)

         last = controlarray(ssize)
         do i=1, ssize - 1
            tmp(i) = controlarray(i)
         end do

         do i=1, size(changed)
            if (changed(i) == 0) then
               tmp(ssize) = '$'//trim(keys(i))//' '//trim(values(i))
               ssize = ssize + 1
            end if
         end do

         deallocate( controlarray )
         allocate( controlarray(ssize) )

         do i=1, ssize - 1
            controlarray(i) = tmp(i)
         end do

         controlarray(ssize) = last
       end block
    end if
  end subroutine tm_change_value


  subroutine tm_delete_keyword(controlarray, keys)
    character(len=*), allocatable, intent(inout) :: controlarray(:)
    character(len=*), intent(in) :: keys(:)

    integer :: i, j, ind
    logical :: found

    character(len=MAX_STR_SIZE), allocatable :: temp(:), split(:)

    allocate( temp(size(controlarray) - size(keys) + 1) )

    ind = 1
    do i=1, size(controlarray)
       call split_blanks(controlarray(i), split)

       found = .false.
       do j=1, size(keys)
          if (index(split(1), keys(j)) /= 0) then
             found = .true.
             exit
          end if
       end do

       if (.not. found) then
          temp(ind) = controlarray(i)
          ind = ind + 1
       end if
    end do

    deallocate(controlarray)
    allocate(controlarray(size(temp)))

    do i=1, size(temp)
       controlarray(i) = temp(i)
    end do
  end subroutine tm_delete_keyword


  subroutine tm_print_control(controlarray, filename)
    character(len=*), intent(in) :: controlarray(:)
    character(len=*), intent(in) :: filename

    integer :: u, i

    open(newunit=u, file=filename, action='write')
    do i=1, size(controlarray)
       write(u, '(a)') trim(controlarray(i))
    end do
    close(u)
  end subroutine tm_print_control


  function tm_get_value(controlarray, key) result(res)
    character(len=*) :: controlarray(:)
    character(len=*) :: key

    character(len=:), allocatable :: res

    integer :: i, j
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    character(len=MAX_STR_SIZE) :: tmp

    tmp = ''
    do i=1, size(controlarray)
       call split_blanks(controlarray(i), split)
       if ( index(split(1), key) /= 0 ) then
          do j=2, size(split)
             tmp = trim(tmp)//' '//trim(split(j))
          end do
       end if
    end do

    res = tmp
  end function tm_get_value


  subroutine tm_make_double_control(original, newfile, nat)
    character(len=*), intent(in) :: original
    character(len=*), intent(in) :: newfile
    integer, intent(in) :: nat

    logical :: ext, ri
    integer :: u, ierr, i, at1, at2
    character(len=MAX_STR_SIZE) :: buf, temp, basis
    character(len=MAX_STR_SIZE) :: atoms, rundim, cshell
    character(len=MAX_STR_SIZE), allocatable :: split(:), group(:), gp(:), control(:)
    character(len=1) :: nl

    ! RI test
    ri = .false.
    inquire(file='auxbasis', exist=ext)
    if (ext) ri = .true.

    nl = NEW_LINE('c')
    basis = ''
    temp = ''
    atoms = nl

    open(newunit=u, file=original, action='read')
    READ_FILE: do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, '$atoms') /= 0) then
          ! $atoms section is found: parse the lines for the atom number
          atoms = nl
          READ_ATOMS: do
             read(u, '(a)', iostat=ierr) buf
             if (ierr /= 0) then
                atoms = trim(atoms)//'  '//trim(temp)//' \'//nl//trim(basis)
                exit READ_ATOMS ! Exit at end of file
             end if

             if (index(buf, '$') /= 0) then
                atoms = trim(atoms)//'  '//trim(temp)//' \'//nl//trim(basis)
                exit READ_ATOMS ! Exit at new section

             else if (index(buf, 'basis') /= 0) then
                basis = trim(basis)//trim(buf)//nl

             else if (index(buf, 'cbas') /= 0) then
                basis = trim(basis)//trim(buf)//nl

             else
                ! This line corresponds to an atom description line, with format:
                ! ``atom_name  at1-at2,at3-at4         \``
                ! We want to obtain the corresponding:
                ! ``atom_name  at1+nat-at2+nat,at3+nat-at4+nat         \``

                ! Flush the current values of basis and temp
                if (temp /= '') then
                   atoms = trim(atoms)//'  '//trim(temp)//' \'//nl//trim(basis)
                end if

                basis = ''
                temp = ''
                call split_blanks(buf, split)

                ! First, save the atom name
                atoms = trim(atoms)//trim(split(1))

                ! Then we build the atom numbers
                if (index(split(2), ',') /= 0) then
                   ! Here we have several groups of atoms, so we split the different
                   ! groups
                   group = split_pattern(split(2), pattern=',')

                   temp = trim(split(2))//','
                   do i=1, size(group)
                      ! Let's see if we have several atoms in each group
                      if (index(group(i), '-') /= 0) then
                         gp = split_pattern(group(i), pattern='-')
                         read(gp(1), *) at1
                         read(gp(2), *) at2
                         at1 = at1 + nat
                         at2 = at2 + nat
                         temp= trim(temp)//to_str(at1)//'-'//to_str(at2)//','
                      else
                         read(group(i), *) at1
                         at1 = at1 + nat
                         temp= trim(temp)//to_str(at1)//','
                      end if
                   end do
                else
                   ! Only one atom of this type, this is the simple case
                   temp = trim(split(2))//','
                   if (index(split(2), '-') /= 0) then
                      gp = split_pattern(split(2), pattern='-')
                      read(gp(1), *) at1
                      read(gp(2), *) at2
                      at1 = at1 + nat
                      at2 = at2 + nat
                      temp= trim(temp)//to_str(at1)//'-'//to_str(at2)//','
                   else
                      read(split(2), *) at1
                      at1 = at1 + nat
                      temp= trim(temp)//to_str(at1)//','
                   end if
                end if

                if (temp(len_trim(temp):len_trim(temp)) == ',') then
                   temp(len_trim(temp):len_trim(temp)) = ''
                end if
             end if
          end do READ_ATOMS

       else if (index(buf, '$rundimensions') /= 0) then
          rundim = nl
          READ_DIM: do
             read(u, '(a)', iostat=ierr) buf
             if (ierr /= 0) exit READ_DIM ! Exit at end of file

             if (index(buf, '$') /= 0) exit READ_DIM ! Exit at new section

             group = split_pattern(buf, pattern='=')
             do i=1, size(group)
                print *, trim(group(i))
             end do

             read(group(2), *) at1

             if (index(buf, 'fock,dens') /= 0) then
                at1 = at1 * 4
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             else if (index(buf, 'natoms') /= 0) then
                at1 = at1 * 2
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             else if (index(buf, 'nshell') /= 0) then
                at1 = at1 * 2
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             else if (index(buf, 'nbf') /= 0) then
                at1 = at1 * 2
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             else if (index(buf, 'trafo') /= 0) then
                at1 = at1 * 2
                rundim = trim(rundim)//trim(group(1))//'='//to_str(at1)//nl
             end if
          end do READ_DIM

       else if (index(buf, '$closed shells') /= 0) then
          cshell = nl
          READ_SHELL: do
             read(u, '(a)', iostat=ierr) buf
             if (ierr /= 0) exit READ_SHELL ! Exit at end of file

             if (index(buf, '$') /= 0) exit READ_SHELL ! Exit at new section

             block
               character(len=len(cshell)) :: tmp
               group = split_pattern(buf)
               cshell = trim(cshell)//trim(group(1))

               tmp = ''
               do i=3, size(group)
                  tmp = trim(tmp)//trim(group(i))
               end do

               group = split_pattern(group(2), '-')
               read(group(2), *) at1
               at1 = at1*2
               cshell = trim(cshell)//'  '//trim(group(1))//'-'//to_str(at1)//'  '&
                    &//trim(tmp)
             end block

          end do READ_SHELL
       end if
    end do READ_FILE
    close(u)

    print *, 'ATOMS'
    print *, trim(atoms)

    print *, 'RUNDIMENSIONS'
    print *, trim(rundim)

    print *, 'CSHELL'
    print *, trim(cshell)

    control = tm_read_control(original)
    block
      character(len=MAX_STR_SIZE) :: keys(5), values(5)

      keys(1) = 'atoms'
      values(1) = trim(atoms)
      keys(2) = 'rundimensions'
      values(2) = trim(rundim)
      keys(3) = 'closed shells'
      values(3) = trim(cshell)
      keys(4) = 'intsdebug'
      values(4) = '  sao'
      keys(5) = 'scfiterlimit'
      values(5) = '  0'

      call tm_change_value(control, keys, values)
    end block

    call tm_print_control(control, newfile)
  end subroutine tm_make_double_control


  subroutine tm_make_double_mos(nbas, nocc, newfile)
    integer, intent(in) :: nbas
    integer, intent(in) :: nocc
    character(len=*), intent(in) :: newfile

    real(dp), parameter :: coeff_false = 0.1_dp
    real(dp), parameter :: e_false = 0.0_dp

    integer :: nbas2, nocc2
    integer :: u, i, j, jaux
    character(len=MAX_STR_SIZE) :: buf

    nbas2 = nbas*2
    nocc2 = nocc*2

    open(newunit=u, file=newfile, action='write')
    write(u, '(a)') '$scfmo   expanded   format(4d20.14)'
    do i=1, nocc2
       write(u, '(I6,A,F16.14,A,I0)') &
            & i, '  a      eigenvalue=', e_false, 'D-00   nsaos=', nbas2

       j = 1
       jaux = 1

       buf = ''
       do while (j <= nbas2)

          write(buf, '(A,D20.14)' ) trim(buf), coeff_false
          if ((jaux == 4) .and. (j /= nbas2)) then
             write(u, '(a)') trim(buf)
             jaux = 0
             buf = ''
          end if
          if (j == nbas2) then
             write(u, '(a)') trim(buf)
             buf = ''
          end if

          j = j+1
          jaux = jaux+1
       end do
    end do
    write(u, '(a)') '$end'
    close(u)
  end subroutine tm_make_double_mos


  subroutine tm_init_double_molecule()

    integer :: ierr
    logical :: ext
    integer :: nat, nocc, nbas

    ierr = mkdir('double_molecule_input')
    call check_error(ierr, TM_OVL_PREPARE, &
         & 'OVL: Cannot create double_molecule_input folder')

    call tm_get_orbspace('control', nat, nocc, nbas)
    call tm_make_double_control('control', &
         & 'double_molecule_input/control', nat)
    call tm_make_double_mos(nbas, nocc, &
         & 'double_molecule_input/mos')

    ierr = copy('basis', 'double_molecule_input/basis')
    call check_error(ierr, TM_OVL_PREPARE, &
         & 'OVL: Cannot copy basis in double_molecule_input folder')
    inquire(file='auxbasis', exist=ext)
    if (ext) then
       ierr = copy('auxbasis', 'double_molecule_input/auxbasis')
       call check_error(ierr, TM_OVL_PREPARE, &
            & 'OVL: Cannot copy auxbasis in double_molecule_input folder')
    end if
  end subroutine tm_init_double_molecule


  subroutine tm_prepare_double_molecule()
    integer :: ierr
    logical :: ext
    ierr = copy('merged_coord', 'overlap/coord')
    ierr = copy('double_molecule_input/control', 'overlap/control')
    ierr = copy('double_molecule_input/basis', 'overlap/basis')
    ierr = copy('double_molecule_input/mos', 'overlap/mos')

    inquire(file='double_molecule_input/auxbasis', exist=ext)
    if (ext) then
       ierr = copy('double_molecule_input/mos', 'overlap/auxbasis')
    end if

    ierr = rm(['merged_coord'])
  end subroutine tm_prepare_double_molecule



  subroutine tm_get_orbspace(control, nat, nocc, nbas)
    character(len=*), intent(in) :: control
    integer, intent(out) :: nat
    integer, intent(out) :: nocc
    integer, intent(out) :: nbas

    integer :: u, ierr, id
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    open(newunit=u, file=control, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'natoms') /= 0) then
          id = index(buf, '=')
          read(buf(id+1:), *) nat
       else if (index(buf, 'nbf(AO)') /= 0) then
          id = index(buf, '=')
          read(buf(id+1:), *) nbas
       else if (index(buf, 'closed shells') /= 0) then
          read(u, '(a)') buf
          call split_blanks(buf, split)
          id = index(split(2), '-')
          read(split(2)(id+1:), *) nocc
       end if
    end do
    close(u)
  end subroutine tm_get_orbspace


  subroutine tm_clean_directory(ierr)
    integer, intent(out) :: ierr
    logical :: ext

    character(len=MAX_STR_SIZE), parameter :: files_to_clean(3) = [&
         & 'scf.out ', 'grad.out', 'gradient' &
         & ]

    ierr = rm(files_to_clean)
    ! call check_error(ierr, TM_ERR_UPDATE, 'TURBOMOLE: Cannot clean directory')

    ierr = copy('mos', 'mos.old')
    ! call check_error(ierr, TM_ERR_PERL, 'TURBOMOLE: Cannot copy mos as mos.old')

    call execute_command_line('rm -rf CC* adc*cao*', exitstat=ierr)
    ! call check_error(ierr, TM_ERR_PERL, 'TURBOMOLE: Problem in deleting CC2 files')

    inquire(file='bin2matrix.out', exist=ext)
    if (ext) then
       ierr = rm(['bin2matrix.out'])
       ! call check_error(ierr, TM_ERR_PERL, 'TURBOMOLE: Problem in deleting bin2matrix.out')
    end if
  end subroutine tm_clean_directory


  subroutine tm_copy_input_files(input_dir, ierr)
    character(len=*), intent(in) :: input_dir
    integer, intent(out) :: ierr

    character(len=MAX_STR_SIZE) :: files_to_copy(4)

    files_to_copy(1) = trim(input_dir)//'/control'
    files_to_copy(2) = trim(input_dir)//'/basis'
    files_to_copy(3) = trim(input_dir)//'/mos'
    files_to_copy(4) = trim(input_dir)//'/auxbasis'
    ierr = copy(files_to_copy, './')
  end subroutine tm_copy_input_files
end module mod_turbomole
