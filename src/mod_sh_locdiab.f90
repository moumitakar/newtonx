! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_sh_locdiab
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date:
  !!
  !! Perform local diabatization according to
  !! G. Granucci, M. Persico, A. Toniolo, J. Chem. Phys. 114 (2001),
  !! 10608-10615
  !!
  !! Adaptation for Newton-X:
  !! Felix Plasser (felix.plasser@univie.ac.at), 2010-Mar-15
  use mod_kinds, only: dp
  use mod_logger, only: &
       & nx_logger_t, nx_log, &
       & LOG_DEBUG, LOG_WARN, LOG_INFO
  use mod_constants, only: MAX_CMD_SIZE
  implicit none

  private

  ! public :: wrtmat
  public :: get_tmat
  public :: propag_ld
  public :: trans_ld

contains

 subroutine get_tmat(smat,nstati,logger)
  ! Compute the transformation matrix by Lowdin
  ! orthogonalization of the overlap matrix.
  !
   !.. Implicit Declarations ..
   ! 2022-01-04 BD: Remove nlist parameter, replaced with ``logger``
  implicit none
  !
  !.. Formal Arguments ..
  integer, intent(in) :: nstati
  type(nx_logger_t), intent(inout) :: logger
  real(dp), dimension(nstati,nstati), intent(inout) :: smat
  ! smat comes in as the S-matrix and is returned as the T-matrix
  !
  !.. Local Scalars ..
  real(dp), dimension(nstati)  :: ove
  real(dp), dimension(nstati,nstati) :: ovs
  ! ovs is the overlap between the columns of the S-matrix entering the Lowdin orthogonalization
  logical :: zflag
  integer :: i,j,k,l
  real(dp) :: sum

  character(len=1024) :: msg
  ! 2020-07-10 BD:
  ! Removed variables (not used anymore) :
  ! integer :: ij, kj
  ! 2022-01-04 BD: Adapt to new logger system

  !
  !  Mau ago 2002
  !  Intruder state check
  !
  do k = 1,nstati
    sum = 0.d0
    do l = 1,nstati
      sum = sum + smat(k,l)**2
      if (l /= k) then
        sum = sum + smat(l,k)**2
      end if
    end do
    if (sum < 1.d-3) then
      ! write (nlist,"(/a)") " Overlap matrix"
       ! call wrtmat(smat,nstati,nstati,nlist)
       call logger%log(LOG_WARN, smat, title='Overlap matrix')
      do l = 1,nstati
        smat(k,l) = 0.d0
        smat(l,k) = 0.d0
      end do
      smat(k,k) = 1.d0
      write(msg, '(A,I0,A)') &
           & 'Intruder state problem with state ', k, &
           & ' ... value taken as 1.'
      call logger%log(LOG_WARN, msg)
      ! write (nlist,*) " *************************************"
      ! write (nlist,*) "  WARNING: intruder state problem with"
      ! write (nlist,*) "  state number ", k
      ! write (nlist,*) "  ... value taken as 1."
      ! write (nlist,*) " *************************************"
    end if
  end do
  !  Mau - end
  ! --- ortonormalizzazione di smat = <I(0)|J(dt)>
  !ij = 0
  do i = 1,nstati
    do j = 1,i
      !ij = ij + 1
      sum = 0.d0
      do k = 1,nstati
        sum = sum + smat(k,i)*smat(k,j)
      end do
      ovs(i,j) = sum
      ovs(j,i) = sum
    end do
  end do
  !write(nlist,*) 'ovs before and after lowdin'
  !call wrtmat(ovs,nstati,nstati,nlist)
  call logger%log(LOG_DEBUG, 'Lowdin orthogonalization')
  call lowdin(nstati,ovs,ove,zflag)
  if (zflag) then
     call logger%log(LOG_WARN, &
          & "REORDERING OF LOWDIN ORTHOGONALIZED VECTORS NOT COMPLETE")
  end if

  !call wrtmat(ovs,nstati,nstati,nlist)
  do i = 1,nstati
    !kj = 0
    do j = 1,nstati
      sum = 0.d0
      do k = 1,nstati
       ! kj = kj + 1
        sum = sum + smat(i,k)*ovs(k,j)
      end do
      ove(j) = sum
    end do
    do j = 1,nstati
      smat(i,j) = ove(j)
    end do
 end do
 call logger%log(LOG_DEBUG, smat, title='T Matrix')
end subroutine get_tmat
!***********************************************************************
! subroutine wrtmat(mat,dim1,dim2,nlist)
!   implicit none
!   integer, intent(in) :: dim1,dim2,nlist
!   real(dp), dimension(dim1,dim2), intent(in) :: mat
!
!   character(len=1024) :: msg
!   integer :: res
!
!    integer :: i,j
!    do i = 1,dim1
!       write(msg,'(10ES25.16)') (mat(i,j),j=1,dim2)
!       res = message(msg, file=nlist)
!     end do
! end subroutine
!***********************************************************************
! subroutine wrtlpt(mat,dim,nlist)
! ! write a lower packed triangular matrix
!   implicit none
!   integer, intent(in) :: dim,nlist
!   real(dp), dimension(dim*(dim+1)/2), intent(in) :: mat
!
!   integer :: i,j,st
!   integer :: res
!   character(len=1024) :: msg
!
!    st = 1
!    do i = 1,dim
!        write(msg,*) (mat(j),j=st,st+i-1)
!        st = st+i
!        res = message(msg, file=nlist)
!     end do
! end subroutine
!***********************************************************************
subroutine lowdin(n,s,sd,zflag)
  !
  !     LOWDIN'S ORTHOGONALIZATION
  !     N = DIMENSION OF THE VECTOR SPACE
  !     S = OVERLAP MATRIX
  !     SD = INVERSE SQUARE ROOT EIGENVALUES
  !     W,WW = SCRATCH ARRAYS
  !     AFTER PROCESSING, THE COLUMNS OF S ARE A BASIS OF ORTHO-
  !     NORMALIZED VECTORS IN THE GIVEN METRIC, REORDERED ACCORDING
  !     TO THEIR OVERLAP WITH THE ORIGINAL BASIS
  !
  !
  !.. Implicit Declarations ..
  implicit none
  !
  !.. Formal Arguments ..
  logical, intent(out) :: zflag
  integer, intent(in) :: n
  real(dp), dimension(n), intent(inout) :: sd
  real(dp), dimension(n,n), intent(inout) :: s
  ! integer, intent(in) :: nlist
  !
  !.. Local ..
  real(dp), dimension(n,n)                :: ev
  real(dp), allocatable, dimension(:)                :: work
  integer :: i,ip,iter,j,k,niter,nm,info,lwork
  real(dp) :: a,b,sij,ski
  character(len=MAX_CMD_SIZE) :: msg
  !
  !
  !.. Intrinsic Functions ..
  intrinsic Sqrt
  !
  ! ... Executable Statements ...
  !

  !write(*,*) 'Before dsyev'
  ev = s
  !call wrtmat(ev,n,n,6)
  niter = 1000
  lwork = max(5,n**2)
  allocate(work(lwork))
  call dsyev('V','L',n, ev, n, sd, work, lwork, info)
  deallocate(work)
  !write(*,*) 'After dsyev',info
  !call wrtmat(ev,n,n,6)
  !write(*,*) 'EW:',(sd(i),i=1,n)
  ! upper or lower?
  !call real_symmetric_diagonalizer(s,'P',n,1,n,sd,ww,info)
  do k = 1,n
     sd(k) = 1.d0 / Sqrt(sd(k))
  end do
  do i = 1,n
     do j = 1,i
        sij = 0.d0
        do k = 1,n
           sij = sij + ev(i,k)*sd(k)*ev(j,k)
        end do
        s(i,j) = sij
        s(j,i) = sij
     end do
  end do
  iter = 0
  nm = n - 1
  do
     iter = iter + 1
     do i = 1,nm
        ip = i + 1
        do j = ip,n
           a = s(i,i)*s(i,i) + s(j,j)*s(j,j)
           b = s(i,j)*s(i,j) + s(j,i)*s(j,i)
           if (a < b) goto 1000
        end do
     end do
     exit
1000 do k = 1,n
        ski = s(k,i)
        s(k,i) = s(k,j)
        s(k,j) = ski
     end do
     if (iter >= niter) then
        write (msg,10000)
        ! res = message(msg, file=nlist)
        call nx_log%log(LOG_WARN, msg)
        zflag = .true.
        return
     end if
  end do
  zflag = .false.
  return
  !
  ! ... Format Declarations ...
  !
10000 format ( &
       //5x,"REORDERING OF LOWDIN ORTHOGONALIZED VECTORS ","NOT COMPLETE"//)
end subroutine lowdin
!***********************************************************************
subroutine propag_ld(eci,eciold,ezero,time,tmat,rotr,roti,acoer0,acoei0,acoer1,acoei1,nstati,logger)
  ! Propagate the diabatic coefficients and transform to the adiabatic basis.
  !
  !.. Implicit Declarations ..
  implicit none
  !
  !.. Formal Arguments ..
  integer, intent(in) :: nstati
  real(dp), intent(in) :: time,ezero
  real(dp), dimension(nstati), intent(in) :: eci,eciold
  real(dp), dimension(nstati,nstati), intent(in) :: tmat
  real(dp), dimension(nstati,nstati), intent(inout) :: rotr,roti
  real(dp), dimension(nstati) :: acoer0,acoei0,acoer1,acoei1
  type(nx_logger_t), intent(inout) :: logger
  !
  !.. Local ..
  integer :: i,j,k,NSTEP,istep,info,lwork
  real(dp) :: dum, difc, timetau, fac, ck, sk
  character(len=1024) :: msg
  !
  !.. Local Arrays ..
  real(dp), dimension(nstati,nstati) :: hdia, hdx
  real(dp), dimension(nstati) :: ex
  real(dp), dimension(nstati,nstati) :: tw,ty,tz,rotsi,rotsr
  real(dp), allocatable, dimension(:)                :: work

  ! Useful for printing information
  complex(dp) :: coeff0(nstati), coeff1(nstati)
  !
  !
  !.. Intrinsic Functions ..
  intrinsic Sum, Matmul, Sin, Cos, Abs

  ! 2020-07-10 BD:
  ! Removed variables (not used anymore) :
  ! integer :: ii, ij
  !
  ! ... Executable Statements ...
  !
  !
  !     Integration of the time dependent Schrodinger equation
  !
  ! --- diabatic hamiltonian at time 0+dt
  ! --- Hdia(dt)
  !ij = 0
  ! res = message('Starting propag_ld ...', file=nlist)
  call logger%log(LOG_INFO, 'Starting propag_ld')
  do i = 1,nstati
     do j = 1,i
        !ij = ij + 1
        hdia(i,j) = Sum(tmat(i,:)*(eci(:)-ezero)*tmat(j,:))
        hdia(j,i) = hdia(i,j)
     end do
  end do
  call logger%log(LOG_DEBUG, hdia, title='Diabatic hamiltonian')

  ! --- evaluation of NSTEP
  difc=0.d0
  do i=2,nstati
     do j=1,i-1
        !ij = (i*i-i)/2 + j
        dum = Abs(hdia(i,j)*(eci(i)-eci(j)))
        if (dum > difc) then
           difc = dum
        endif
     end do
  end do
  difc = Abs(Sin(difc*time**2/3.d0))
  if (difc < 0.01) then
     NSTEP = 1
  elseif (difc < 0.5) then
     NSTEP = 5
  else
     NSTEP = 20
  endif
  write(msg, *) "**** TDSE integration:  NSTEP=", NSTEP, difc
  call logger%log(LOG_DEBUG, msg)
  write(msg,*) " hdia,de",hdia(2,1)/time,eci(2)-eci(1)
  call logger%log(LOG_DEBUG, msg)

  lwork = max(5,nstati**2)
  allocate(work(lwork))
  !
  !  The full propagator is split in NSTEP propagators
  !
  timetau = time/NSTEP
  do istep = 1,NSTEP
     ! --- evaluation of hdx = [Hdia(k*tau) + Hdia((k-1)*tau)]/2
     ! --- Hdia(k*tau) = E(0) + 2k-1/(2NSTEP) *[Hdia(dt)-E(0)]
     ! --- with k=istep, tau=dt/NSTEP   dt=time
     fac = (2.d0*istep-1.d0)/(2.d0*NSTEP)
     hdx = fac*hdia
     do i=1,nstati
        !ii = (i*i+i) / 2
        hdx(i,i) = hdx(i,i) + (1.d0 - fac)*(eciold(i) - ezero)
     end do
     hdx = hdx*timetau
     ! --- spectral resolution of exp(-i HDX tau)
     !tx=0.d0
     call dsyev('V','L',nstati, hdx, nstati, ex, work, lwork, info)
  ! upper or lower?

     !call real_symmetric_diagonalizer(hdx,'P',nstati,1,nstati,ex,tx,info)
     rotsr=0.d0
     rotsi=0.d0
     do k = 1,nstati
        sk = Sin(ex(k))
        ck = Cos(ex(k))
        do i = 1,nstati
           do j = 1,nstati
              rotsr(i,j) = rotsr(i,j) + hdx(i,k)*hdx(j,k)*ck
              rotsi(i,j) = rotsi(i,j) - hdx(i,k)*hdx(j,k)*sk
           end do
        end do
     end do
     ! --- the full transformation is: R(dt) = R(NSTEP) R(NSTEP-1) ... R(1)
     if (istep == 1) then
        rotr = rotsr
        roti = rotsi
     else
        tw = Matmul(rotsr,rotr)
        hdx = Matmul(rotsi,roti)
        ty = Matmul(rotsr,roti)
        tz = Matmul(rotsi,rotr)
        rotr = tw - hdx
        roti = ty + tz
     endif
  end do
  deallocate(work)
  !

  call logger%log(LOG_DEBUG, rotr, &
       & title="Real part of the rotation matrix (rotr)")
  call logger%log(LOG_DEBUG, roti, &
       & title="Imaginary part of the rotation matrix (roti)")

  !      The coefficients in the adiabatic representation
  !      are updated
  !
  !fp_todo: read acoer
  do i = 1,nstati
    acoer1(i) = 0.d0
    acoei1(i) = 0.d0
    do j = 1,nstati
      do k = 1,nstati
        acoer1(i) = &
          acoer1(i) + tmat(j,i)*(acoer0(k)*rotr(j,k)-acoei0(k)*roti(j,k))
        acoei1(i) = &
          acoei1(i) + tmat(j,i)*(acoei0(k)*rotr(j,k)+acoer0(k)*roti(j,k))
      end do
    end do
 end do

 do i=1, nstati
    coeff0(i) = complex(acoer0(i), acoei0(i))
    coeff1(i) = complex(acoer1(i), acoei1(i))
 end do

 call logger%log(LOG_DEBUG, coeff0, &
      & 'Electronic coefficients before propagation')
 call logger%log(LOG_DEBUG, coeff1, &
      & 'Electronic coefficients after propagation')
end subroutine propag_ld
!***********************************************************************
subroutine trans_ld(tmat,rotr,roti,acoer0,acoei0,acoer1,acoei1,trans,istati,irk,nstati,logger)
! compute the transition probabilites, eq. (19)
  !.. Implicit Declarations ..
  implicit none
  !
  !.. Formal Arguments ..
  integer, intent(in) :: nstati
  integer, intent(inout) :: istati
  integer, intent(inout) :: irk
  real(dp), dimension(nstati,nstati), intent(in) :: tmat
  real(dp), dimension(nstati,nstati), intent(in) :: rotr,roti
  real(dp), dimension(nstati), intent(in) :: acoer0,acoei0,acoer1,acoei1
  real(dp), dimension(nstati), intent(inout) :: trans
  type(nx_logger_t), intent(inout) :: logger
  !
  !.. Parameters ..
  real(dp), parameter :: eps = 1.d-25
  real(dp), parameter :: eps_switch = 1.d-9
  !
  !.. Local Scalars ..
  integer :: j,l,istati_old
  integer, dimension (1) :: lx
  real(dp) :: aci0,aci1,acr0,acr1,popk0,popk1,rden,rekk,rekl, &
                      & vkki,vkkr,vkli,vklr,tswitch, &
                      & trans_tot,rnum
  character(len=1024) :: msg
  ! 2020-07-10: BD
  ! Variables removed (not used anymore) :
  ! integer :: i, k,
  ! real(dp) :: popl0, popl1
  ! --- transition probability
  vkkr = 0.d0
  vkki = 0.d0
  do j = 1,nstati
    vkkr = vkkr + tmat(j,istati)*rotr(j,istati)
    vkki = vkki + tmat(j,istati)*roti(j,istati)
  end do
  ! write(nlist,*) ' controllo: vkkr, vkki',vkkr,vkki
  !
  tswitch = Abs(tmat(istati,istati))
  istati_old=istati
  if (tswitch < eps_switch) then      ! state switch
     trans(1:nstati) = 0.d0
     lx = Maxloc(Abs(tmat(istati,:)))
     !trans(lx(1)) = 1.d0
     write(msg,"(/a,i3,a,i3)")"  ****  STATE SWITCH   ",istati," --> ",lx(1)
     call logger%log(LOG_WARN, msg)
     ! res = message(msg, file=nlist)
     irk = 2
     istati=lx(1)
  endif
  acr0 = acoer0(istati_old)
  aci0 = acoei0(istati_old)
  acr1 = acoer1(istati)
  aci1 = acoei1(istati)
  popk0 = acr0*acr0 + aci0*aci0
  popk1 = acr1*acr1 + aci1*aci1
  rekk = vkkr*(acr0*acr1+aci0*aci1) + vkki*(acr0*aci1-aci0*acr1)
 !write(nlist,*)' rekk:',rekk
  rnum = (popk1-popk0) / popk0
  rden = popk1 - rekk

  write(msg,*) ' controllo: rnum,rden',rnum,rden
  call logger%log(LOG_DEBUG, msg)

  if ((popk1-popk0) >= 0) then        ! pop(istati) increases: no hop
     trans(1:nstati) = 0.d0
  else                                ! normal evaluation
     do l = 1,nstati
        if (l == istati) then
           trans(l) = 0.d0
        else
           vklr = 0.d0
           vkli = 0.d0
           do j = 1,nstati
              vklr = vklr + tmat(j,istati)*rotr(j,l)
              vkli = vkli + tmat(j,istati)*roti(j,l)
           end do
          !write(nlist,*)' controllo: l, vklr,vkli',l,vklr,vkli
           rekl = vklr*acoer0(l)*acr1 + vklr*acoei0(l)*aci1 + &
                & vkli*acoer0(l)*aci1 - vkli*acoei0(l)*acr1

           write(msg,*)' rekl,rden,rekl-rden',rekl,rden,rekl-rden
           call logger%log(LOG_DEBUG, msg)
           write(msg,*)' rnum:',rnum
           call logger%log(LOG_DEBUG, msg)

          if ((rekl*rnum == 0.0_dp) .and. (rden == 0.0_dp)) then
             trans(l) = 0.0_dp
          else
             trans(l) = -rekl*rnum/rden
          end if

           !
!fp: this check was removed
          !if (Abs(rekl-rden) < eps) then
          !   popl1 = acoer1(l)**2 + acoei1(l)**2
          !   popl0 = acoer0(l)**2 + acoei0(l)**2
          !   write(nlist,*) ' *** transl changed from',trans(l)
          !! wrong:                trans(l) = -rnum*(popl1-popl0)
          ! this would be correct: trans(l) = -rnum
          !   write(nlist,*) ' to',trans(l)
          ! end if
           if (trans(l) < 0.d0) then
              trans(l) = 0.d0
           end if
        end if
     end do
  endif
  ! --- renormalize trans if necessary
  trans_tot = Sum(trans(1:nstati))
  if (trans_tot > 1.d0) then
     trans(1:nstati) = trans(1:nstati)/trans_tot
     call logger%log(LOG_WARN, &
          & "*** Warning: Sum of transition probabilities larger than 1")

     write(msg,'(a,/100d20.8)') ' TRANS=',(trans(l),l=1,nstati)
     call logger%log(LOG_WARN, msg)
     call logger%log(LOG_WARN, "Probabilities renormalized")
  endif
  !

  call logger%log(LOG_DEBUG, trans, title='TRANS=')
end subroutine trans_ld
!***********************************************************************
end module mod_sh_locdiab
