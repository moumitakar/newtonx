module mod_exash
  !! author: Eduarda Sangiogo Gil
  !! date: 2022-02-15
  !!
  !! EXASH (EXiton Analysis for Surface Hopping dynamics)
  !! This module calculates:        
  !! The Excition Energies in the Excition Model;
  !! The Gradients for the Exciton Model;
  !! The wave function overlaps between two consecutive time steps.
  !!                                                
  !! The excitonic coupling between two states on different chromophores  
  !! are performed using the transition monopole aproximation (TMA) 
  !! which is based on the transition charges (TrESP).     
  !!                                                      
  !! This program can be interfaced with MOPAC + TINKER using FOMO-CI and 
  !! with Gaussian (ONIOM) with TD-DFT
  use mod_kinds, only: dp
  use mod_constants, only: &
       & au2ev, au2ang, au2debye, au2kcalm
  use mod_tools, only: &
       & split_blanks
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_trajectory, only: nx_traj_t
  use mod_configuration, only: nx_config_t
  use mod_input_parser, only: &
       & parser_t, set => set_config, set_alloc => set_config_with_alloc
  
  implicit none

  private

  public :: start_prog
  public :: exc_read_input
  public :: dyn_option
  public :: calc_en_grad
  public :: calc_ovl
  public :: eigfunc
  public :: rgrad
  public :: off_dig_grad
  public :: wout
  public :: search
  public :: search2
  public :: winf
  public :: wdyn

  contains

  subroutine start_prog(nx_qm, traj, conf, qminfo, &
                        & osc_str, diaham, diapop)
     type(nx_qm_t), intent(inout) :: nx_qm
     !! Nx_Qm object.
     type(nx_traj_t), intent(inout) :: traj
     !! Classical trajectory.
     type(nx_config_t), intent(inout) :: conf
     !! General configuration.
     type(nx_qminfo_t), intent(inout) :: qminfo

     integer :: i

     integer :: nchrom
     !! Number of chromophores

     integer :: nstat_tot
     !! Total number of excited states

     integer :: nat_tot
     !! Number of all the QM atoms (monomers included in the exciton model)

     ! integer :: numb_atom
     !! Total numbel of atoms

     ! integer :: tresp
     !! Option for TMA approximation

     ! integer :: gs
     !! Only calculate ground state properties if gs=1

     ! integer :: dip
     !! Calculate the trans. dip moments of dip=1

     ! integer :: nc
     !! Current step (equivalent to traj%step)

     ! integer :: nsurf 
     !! Total number of states (equivalent to conf%nstat)

     ! integer, allocatable, dimension(:) :: nstat
     !! Array with the number of states of each chromophore (exciton)

     ! integer, allocatable, dimension(:) :: nat_array
     !! Array with the number of atoms of each chromophore (exciton)
     
     character(len=120) :: name_file
     !! Name file of the QM calculations
     character(len=120) :: out_name
     !! Name file of EXASH output
     character(len=120) :: coup_file
     !! Name file for the excitonic couplings (not important)
     character(len=120) :: name_ind
     character(len=120) :: file_esp
     character(len=120) :: nameprog

     character(len=120), allocatable, dimension(:) :: namefiles_qmmm
     !! Array with name of the QM files for each chromophores

     real(dp), dimension(:), intent(inout) :: osc_str
     !! Oscillator strenght
     real(dp), dimension(:), intent(inout) :: diapop
     !! Additional exciton informations - diabatic populations
     real(dp), dimension(:,:), intent(inout) :: diaham
     !! Additional exciton informations - diabatic Hamiltonian
     !*******************************************************************  

     nchrom = nx_qm%nchromexc
     !! Number of chromophores
    
     nameprog = nx_qm%qmcode
     !! Program name 
     
     ! "name_file" and "out_name" are standart in this merged version
     select case(nameprog)
     case("exc_mopac")
        name_file = "exc_mop"
        out_name = "exc_mop"
     case("exc_gaussian")
        name_file = "exc_gau"
        out_name = "exc_gau"
     end select

     ! allocate ( nat_array(nchrom) )
     ! nat_array(:) = 0
     ! allocate( nstat(nchrom) )
     ! nstat(:) = 0
     allocate( namefiles_qmmm(nchrom) )
     namefiles_qmmm(:) = ""
    
     ! ! Default input informations:
     ! tresp = 1    ! Off dig terms of the H will be performed using TMA aproximation.
     ! gs = 0       ! Compute only the ground state inf.
     ! dip = 0      ! Compute the tra. dip. moments. if .ne. 1
     ! coup_file = ""
     ! ! configfile = 'configuration.inp'
    
     ! call read_input(nx_qm, parser, nstat, nat_array)

     nstat_tot = sum(nx_qm%nstatexc)
     nat_tot = sum(nx_qm%nat_arrayexc)

     if (nx_qm%gsexc .gt. 0) then
        nstat_tot = 0
     end if

     ! Array which contains the name of the qmmm calculations for all the 
     ! chromophores
     do i = 1, nchrom
        write (name_ind, "(I10)") i
        write (file_esp, "(A)") name_file
        namefiles_qmmm(i) =trim(adjustl(file_esp))//trim(adjustl(name_ind))
     end do

     ! numb_atom = conf%nat
     ! nc = traj%step
     ! dt = conf%dt
     ! nsurf = conf%nstat
     ! ti = dt * nc

     call dyn_option(nx_qm, conf, traj, namefiles_qmmm, out_name, coup_file, & 
                        & nstat_tot, nat_tot, qminfo%repot, qminfo%rgrad, osc_str, diaham, &
                        & diapop)


  end subroutine start_prog

  subroutine exc_read_input(nx_qm, parser)
      !! Read the EXASH input from configfile
      type(nx_qm_t), intent(inout) :: nx_qm
      !! Nx_Qm object.
      type(parser_t), intent(in) :: parser

      ! integer :: tresp
      ! integer :: gs
      ! integer :: dip
      ! integer :: u   


      ! character(len=120) :: coup_file

     !  namelist /exc_inp/ tresp, gs, dip, nat_array, &
     !                    & nstat, coup_file
     !  
     ! open(newunit=u, file=configfile, action='read', status='old')
     ! read(u, nml=exc_inp)
     ! close(u)
     call set(parser, 'exc_inp', nx_qm%trespexc, 'tresp ')
     call set(parser, 'exc_inp', nx_qm%gsexc, 'gs')
     call set(parser, 'exc_inp', nx_qm%dipexc, 'dip')
     call set_alloc(parser, 'exc_inp', nx_qm%nat_arrayexc, 'nat_array')
     call set_alloc(parser, 'exc_inp', nx_qm%nstatexc, 'nstat')
     call set(parser, 'exc_inp', nx_qm%coup_fileexc, 'coup_file')
  end subroutine exc_read_input

  subroutine dyn_option(nx_qm, conf, traj, namefiles_qmmm, out_name, coup_file, & 
                        & nstat_tot, nat_tot, repot, rgrad, osc_str, diaham, &
                        & diapop)
  !! Calculate energies, gradients and wevefunction overlap matrix for the 
  !! exciton model
     
     type(nx_qm_t), intent(inout) :: nx_qm
     !! Nx_Qm object.
     type(nx_config_t) :: conf
     !! General configuration.
     type(nx_traj_t) :: traj
     !! Classical trajectory.

     integer :: i, j 
     integer :: u
     integer :: nstat_tot
     integer :: nat_tot
   
     real(dp), dimension(:), allocatable :: vec_en
     !! Exciton energies
     real(dp), dimension(:), allocatable :: osc_st
     !! Oscilattor strenght
     real(dp), dimension(:), allocatable :: mat_ene
     !! Exciton energies without the ground state
     real(dp), dimension(:), allocatable :: pop_dia
     !! Diabatic populations
     real(dp), dimension(:), allocatable :: atomic_number
     !! Diabatic populations
     real(dp), dimension(:), allocatable :: mass

     real(dp), dimension(:,:), allocatable :: ham_tot
     !! Exciton hamiltonian
     real(dp), dimension(:,:), allocatable :: eigenvectors
     !! Eigenvectors of the exciton hamiltonian
     real(dp), dimension(:,:), allocatable :: grad_total
     !! Gradients of the current state
     real(dp), dimension(:,:), allocatable :: trdip_debye
     !! Transition dipole moments
     real(dp), dimension(:,:), allocatable :: coord
     !! Geometries
                                              
     !! characters
     character(len=120) :: out_name
     character(len=120) :: coup_file
     character(len=120) :: io_path

     character(len=120), dimension(:), intent(inout) :: namefiles_qmmm

     character(len=120), allocatable, dimension(:) :: labels

     !************************************************************************
     real(dp), dimension(:), intent(inout) :: repot
     !! New potential energies read from output.
     real(dp), dimension(:, :, :), intent(inout) :: rgrad
     !! New gradients read from output.
     real(dp), dimension(:), intent(inout) :: osc_str
     !! Oscillator strenght
     real(dp), dimension(:), intent(inout) :: diapop
     !! Additional exciton informations - diabatic populations
     real(dp), dimension(:,:), intent(inout) :: diaham
     !! Additional exciton informations - diabatic Hamiltonian
     !************************************************************************

     allocate( vec_en(nstat_tot+1) )
     vec_en(:) = 0._dp
     
     allocate( osc_st(nstat_tot) ) 
     osc_st(:) = 0._dp
     
     allocate( mat_ene(nstat_tot) )
     mat_ene(:) = 0._dp
     
     allocate( ham_tot(nstat_tot+1, nstat_tot+1) )
     ham_tot(:,:) = 0._dp
     
     allocate( eigenvectors(nstat_tot+1, nstat_tot+1) ) 
     eigenvectors(:,:) = 0._dp
     
     allocate( grad_total(conf%nat,3) )
     grad_total(:,:) = 0._dp
     
     allocate( trdip_debye(nstat_tot, 3) ) 
     trdip_debye(:,:) = 0._dp
     
     !! Check the number of states!!! The number of states must be consistent...
     if (traj%step .eq. 0 .and. conf%nstat .ne. (nstat_tot+1) ) then
        write(*,*)
        write(*,*) "The number of states in Newton-X is: ", conf%nstat
        write(*,*) "The number of states in Exciton program is: ", (nstat_tot+1)
        write(*,*) "The number of states in Newton-X and in exciton &
                    & program must be the same..."
        write(*,*) "Aborting calculation..."
        call exit
        write(*,*)
     end if

     call calc_en_grad(nx_qm, conf, traj, namefiles_qmmm, nstat_tot, coup_file, ham_tot, &
                       & vec_en, eigenvectors, grad_total, trdip_debye, osc_st, & 
                       & nat_tot, mat_ene)

     nx_qm%eigenvectorsexc = eigenvectors

     if (nx_qm%qmcode .eq. "exc_mopac") then
        call calc_ovl(nx_qm, traj)
     end if

     !! Computes the diabatic populations
     allocate( pop_dia(nstat_tot+1) )
     pop_dia(:) = 0._dp
     do i = 1, nstat_tot + 1
        pop_dia(i) = ( eigenvectors(i, traj%nstatdyn) )**2
     end do

     nx_qm%trdipexc = trdip_debye

     !! In case of "verbose > 0", generate a formatted file 
     !! ( It is good to check the exciton results )
     if (nx_qm%verboseexc .eq. 1 .or. nx_qm%verboseexc .eq. 3) then
        io_path = '../'//trim(conf%output_path)//'/'//trim(out_name)
        call wout(nx_qm, conf, traj, io_path, nstat_tot, ham_tot, mat_ene, &
                  & eigenvectors, trdip_debye, osc_st)
     end if
      
     !! Replace the eigenvectors of the previous step into eigenvectors of 
     !! the current step
     open(newunit=u, file="old_eigenvectors.txt", status='replace', form='formatted')
     rewind u
     do i = 1, nstat_tot+1
        write(u,*) (eigenvectors(i,j), j = 1,nstat_tot+1)
     end do
     close(u)

     !! Final dynamics informations
     repot = vec_en
     open(newunit=u, file='epot', form='formatted', status='unknown')
     rewind u
     do i = 1, nstat_tot+1
        write(u,*) repot(i)
     end do
     close(u)
     
     do i = 1, conf%nat
        do j = 1, 3
           rgrad(traj%nstatdyn,j,i) = grad_total(i,j) 
        end do
     end do
 
     open(newunit=u, file='grad_check', form='formatted', status='unknown')
     rewind u
     do i = 1, conf%nat
        write(u,'(1x,*(1x,f18.10))') (grad_total(i,j), j = 1, 3)
     end do
     close(u)
    
     osc_str = osc_st
   
     diaham = ham_tot
  
     diapop = pop_dia
 
     !! Copy the current geometry... It will be used in the computations of the overlap matrix
     allocate( labels(conf%nat) )
     labels(:) = ""
     allocate( atomic_number(conf%nat) )
     atomic_number(:) = 0._dp
     allocate( coord(conf%nat, 3) )
     coord(:,:) = 0._dp
     allocate( mass(conf%nat) )
     mass(:) = 0._dp

     open(newunit=u, file='geom', status='unknown', form='formatted')
     rewind u
     do i = 1, conf%nat
        read(u,*) labels(i), atomic_number(i), (coord(i,j), j = 1, 3), mass(i)
     end do
     close(u)
     
     open(newunit=u, file='geom_old', status='unknown', form='formatted')
     rewind u
     do i = 1, conf%nat
        write(u,100) labels(i), atomic_number(i), (coord(i,j), j = 1, 3), mass(i)
     end do
     close(u)

     deallocate( vec_en )
     deallocate( osc_st ) 
     deallocate( mat_ene )
     deallocate( ham_tot )
     deallocate( eigenvectors ) 
     deallocate( grad_total )
     deallocate( trdip_debye ) 
     deallocate( labels )
     deallocate( atomic_number )
     deallocate( coord )
     deallocate( mass )
     
     100 format( a6, 2x, f4.1, *(1x,f15.8) ) 

  end subroutine dyn_option

  subroutine calc_en_grad(nx_qm, conf, traj, namefiles_qmmm, nstat_tot, coup_file, ham_tot, &
                          & vec_en, eigenvectors, grad_total, trdip_debye, osc_st, & 
                          & nat_tot, mat_ene)
  !! Calculate the energies and gradients
     type(nx_qm_t), intent(inout) :: nx_qm
     !! Nx_Qm object.
     type(nx_config_t) :: conf
     !! General configuration.
     type(nx_traj_t) :: traj
     !! Classical trajectory.

     integer :: g, h, i, j, k, l, m, n, p, q, w, z 
     integer :: u
     integer :: nchrom
     integer :: nstat_tot
     integer :: nat_tot
     integer :: numb_atom
     integer :: unz
     integer :: ngrad
     integer :: ii, jj, yy, zz
     integer :: iii, jjj
     integer :: init1, init2
     integer :: num_lab1, num_lab2
     integer :: ind_coup
     integer :: num_grad
     integer :: ind_test
     integer :: atom_dif
     integer :: id
     integer :: natig
     integer :: ierr

     integer, dimension(:), allocatable :: nstat
     integer, dimension(:), allocatable :: nat_array

     real(dp) :: enMM
     real(dp) :: e_gs
     real(dp) :: e_ref
     real(dp) :: eint
     real(dp) :: r
     real(dp) :: num_x, num_y, num_z
     real(dp) :: full_grd_x, full_grd_y, full_grd_z
     real(dp) :: osc
     real(dp) :: mat_en
     real(dp) :: mod_trd
     real(dp) :: a
     real(dp) :: en_au
     real(dp) :: grad_gs_x, grad_gs_y, grad_gs_z

     real(dp), dimension(:), intent(inout) :: vec_en
     real(dp), dimension(:), intent(inout) :: osc_st
     real(dp), dimension(:), intent(inout) :: mat_ene

     real(dp), dimension(:,:), intent(inout) :: ham_tot
     real(dp), dimension(:,:), intent(inout) :: eigenvectors
     real(dp), dimension(:,:), intent(inout) :: grad_total
     real(dp), dimension(:,:), intent(inout) :: trdip_debye

     real(dp), allocatable, dimension(:) :: omega
     real(dp), allocatable, dimension(:) :: array_gs
     real(dp), allocatable, dimension(:) :: grad_off_dig_x, grad_off_dig_y, &
                                                & grad_off_dig_z
     real(dp), allocatable, dimension(:) :: elec_coup
     real(dp), allocatable, dimension(:) :: eig_vec
     real(dp), allocatable, dimension(:) :: full_grd_x_prima, full_grd_y_prima, &
                                                & full_grd_z_prima 
     real(dp), allocatable, dimension(:) :: mod_trdip

     real(dp), allocatable, dimension(:,:) :: mat_e_ref
     real(dp), allocatable, dimension(:,:) :: coord1, coord2
     real(dp), allocatable, dimension(:,:) :: qtot_1, qtot_2
     real(dp), allocatable, dimension(:,:) :: ham_exp
     real(dp), allocatable, dimension(:,:) :: grad_GS
     real(dp), allocatable, dimension(:,:) :: grad_dig
     real(dp), allocatable, dimension(:,:) :: grad_dig_tot
     real(dp), allocatable, dimension(:,:) :: grad_off
     real(dp), allocatable, dimension(:,:) :: full_grad_mat
     real(dp), allocatable, dimension(:,:) :: grad1, grad2, grad3
     real(dp), allocatable, dimension(:,:) :: trdip
     real(dp), allocatable, dimension(:,:) :: grad_x, grad_y, grad_z
     real(dp), allocatable, dimension(:,:) :: mmgrd
     real(dp), allocatable, dimension(:,:) :: chrom_en
     real(dp), allocatable, dimension(:,:) :: ham

     character(len=120) :: coup_file
     character(len=120) :: lab1, lab2
     character(len=120) :: buff
     character(len=120) :: nameprog

     character(len=120), dimension(:), intent(inout) :: namefiles_qmmm

     character(len=1200), allocatable :: split(:)
  
     logical :: conferma
     !**************************************************************************************

     nchrom = nx_qm%nchromexc
     numb_atom = conf%nat
     nameprog = nx_qm%qmcode

     allocate(nstat(nchrom))
     nstat = 0
     nstat = nx_qm%nstatexc
     
     allocate(nat_array(nchrom))
     nat_array = 0
     nat_array = nx_qm%nat_arrayexc
     
     allocate(ham(nstat_tot, nstat_tot))
     ham(:,:) = 0._dp

     !! Read the MM energies
     select case(nameprog)
     case("exc_mopac")

        ! Extract the MM energy from Tinker output
        open(newunit=u, file="fullMM_tnk.out", status='old', form='formatted')
        do
           read(u, '(A)', iostat=ierr) buff
           if (ierr /= 0) exit

           id = index(buff, 'Total Potential Energy :')
           if (id /= 0) then
              call split_blanks(buff, split)
              read(split(5), *) enMM
              exit
           end if
        end do
        close(u)
        enMM = enMM / au2kcalm 

     case("exc_gaussian")
     
        open(newunit=u, file="exc_gau_MM.log", status="old", form="formatted")
        ! rewind u
        ! call search("Energy=", card, u, conferma)
        ! backspace u
        do
           read(u, '(A)', iostat=ierr) buff
           if (ierr /= 0) exit
           id = index(buff, 'Energy=')
           if (id /= 0) then
              read(buff,*) buff, enMM
           end if
        end do
        close(u)
          
        ! print *, 'ENERGY MM CHECK:'
        ! print *, enMM
     end select

     !! Extract the site energies
     select case(nameprog)
     case("exc_mopac")

        ! Extract the energies from Mopac output
        allocate( omega(nstat_tot) ) 
        omega(:) = 0._dp

        allocate( array_gs(nchrom) )
        array_gs = 0._dp

        j = 0
        k = 0
        do i = 1, nchrom
           j = j + 1
           ierr = getcwd(buff)
           inquire(file=trim(buff)//'/'//trim(namefiles_qmmm(i))//"_nx.epot", exist=conferma)
           open(newunit=u, file=trim(namefiles_qmmm(i))//"_nx.epot", &
                & status='old', form='formatted')
           rewind u
           read(u,*) array_gs(j)

           do l = 1, nstat(i)
              k = k + 1
              read(u,*) en_au
              omega(k) = en_au - array_gs(j)
           end do
           close(u)
        end do

     case("exc_gaussian")

        ! Extract the energies from Gaussian output
        allocate( omega(nstat_tot) ) 
        omega(:) = 0._dp
     
        allocate( array_gs(nchrom) )
        array_gs = 0._dp

        j = 0
        l = 0
        do i = 1, nchrom
           j = j + 1
           open(newunit=u, file=trim(namefiles_qmmm(i))//".log", &
                & status='old', form='formatted')
           ! rewind u
           ! call search("ONIOM: extrapolated energy =", card, u, conferma)
           ! backspace u
           do
              read(u, '(A)', iostat=ierr) buff
              if (ierr /= 0) exit
              id = index(buff, 'ONIOM: extrapolated energy =')
              if (id /= 0) then
                 call split_blanks(buff, split)
                 read(split(5), *) array_gs(j)
                 ! read(buff,*) buff, buff, buff, buff, array_gs(j)
              end if
           end do

           rewind u
           k = 0
           FIND_ES: do
              read(u, '(A)', iostat=ierr) buff
              if (ierr /= 0) exit
              id = index(buff, 'Excited State ')
              if (id /= 0) then
                 k = k+1
                 if (k > nstat(i)) then
                    exit FIND_ES
                 else
                    l = l+1
                    call split_blanks(buff, split)
                    read(split(5), *) en_au
                    omega(l) = en_au / au2ev
                 end if
              end if 
           end do FIND_ES
           close(u)
        end do

     end select
    
     !! Computes the reference energy
     e_gs = 0._dp
     do i = 1, nchrom
        e_gs = e_gs + ( array_gs(i) - enMM )
     end do
     
     e_ref = enMM + e_gs 

     allocate( chrom_en(nchrom, (nstat_tot+1)) )
     chrom_en = 0._dp
     k = 0
     do i = 1, nchrom
        chrom_en(i,1) = array_gs(i) 
        do j = 2, nstat(i)+1
           k = k + 1
           chrom_en(i,j) = omega(k) + array_gs(i)
        end do
     end do
     nx_qm%chrom_enexc = chrom_en
     deallocate( chrom_en )
    
     allocate( mat_e_ref((nstat_tot+1), (nstat_tot+1)) )
     mat_e_ref(:,:) = 0._dp
     do i = 1, nstat_tot + 1
        mat_e_ref(i,i) = e_ref
     end do

     !! Computes the diagonal terms of the Exciton Hamiltonian
     do i = 1, nstat_tot
        ham (i,i) = omega(i)
     end do

     if (nx_qm%gsexc .gt. 0) then
        vec_en(1) = e_ref
     else
        if (nx_qm%trespexc .eq. 1) then
           !! Computes the off-diagonal terms of the Exciton Hamiltonian (electronic couplings) 
           !! and the off-diagonal dH/dR derivatives for the case of TrESP aproximation...                                              
           ! Array's dimension:
           ngrad = 0 
           do i = 1, nchrom
              q = nat_array(i)*nstat(i)
              do j = 1, nchrom
                 if (j .ne. i) then
                    ngrad = ngrad + q*nstat(j)
                 end if
              end do
           end do
   
           allocate( grad_off_dig_x(ngrad), grad_off_dig_y(ngrad), grad_off_dig_z(ngrad) )
           grad_off_dig_x(:) = 0._dp
           grad_off_dig_y(:) = 0._dp
           grad_off_dig_z(:) = 0._dp

           w = 1 
           ! Loop over the chromophores
           do m = 1, nchrom  
              init1 = 0 
              do iii = 1, m-1 
                 init1 = init1 + nat_array(iii)
              end do
    
              do n = 1, nchrom
                 init2 = 0 
                 do jjj = 1, n-1 
                    init2 = init2 + nat_array(jjj)
                 end do
   
                 if (m .ne. n) then
    
                    allocate( coord1(nat_array(m),3), coord2(nat_array(n),3) ) 
                    coord1(:,:) = 0._dp
                    coord2(:,:) = 0._dp

                    allocate( qtot_1(nat_array(m), nstat(m)), qtot_2(nat_array(n), nstat(n)) )
                    qtot_1(:,:) = 0._dp
                    qtot_2(:,:) = 0._dp

                    call rxyz(nat_array(m), coord1, init1)
                    call rxyz(nat_array(n), coord2, init2)
  
                    ! Read the transition charges (TrESP)
                    select case(nameprog)
                    
                    case("exc_mopac")

                       open(newunit=u, file=trim(namefiles_qmmm(m))//'.out', status='old', &
                            & form='formatted')
                       ! rewind u
                       ! call search("MATRIX WITH THE ELECTROSTATIC CHARGES OF ALL THE STATES", &
                       !             & card, u, conferma)
                       ! do i = 1, nat_array(m)
                       !    read(u,*) num_lab1, lab1, ( qtot_1(i,j), j=1,nstat(m) )
                       ! end do
                       do
                          read(u, '(a)', iostat=ierr) buff
                          if (ierr /= 0) exit
                          id = index(buff, 'MATRIX WITH THE ELECTROSTATIC CHARGES OF ALL THE STATES')
                          if (id /= 0) then
                             do i=1, nat_array(m)
                                read(u,*) num_lab1, lab1, ( qtot_1(i,j), j=1,nstat(m) )
                             end do
                          end if
                       end do
                       close(u)
   
                       open(newunit=u, file=trim(namefiles_qmmm(n))//'.out', status='old', &
                            & form='formatted')
                       do
                          read(u, '(a)', iostat=ierr) buff
                          if (ierr /= 0) exit
                          id = index(buff, 'MATRIX WITH THE ELECTROSTATIC CHARGES OF ALL THE STATES')
                          if (id /= 0) then
                             do i=1, nat_array(n)
                                read(u,*) num_lab2, lab2, ( qtot_2(i,j), j=1,nstat(n) )
                             end do
                          end if
                       end do
                       ! rewind u
                       ! call search("MATRIX WITH THE ELECTROSTATIC CHARGES OF ALL THE STATES", &
                       !         card, u, conferma)
                       ! do i = 1, nat_array(n)
                       !    read(u,*) num_lab2, lab2, ( qtot_2(i,j), j=1,nstat(n) )
                       ! end do
                       close(u)
                       
                    case("exc_gaussian")
                    
                       open(newunit=u, file=trim(namefiles_qmmm(m))//'.log', status='old', &
                            & form='formatted')
                       ! rewind u
                       do i = 1, nstat(m)

                          ! call search("ESP charges:", card, u,
                          ! conferma)
                          do
                             read(u, '(A)', iostat=ierr) buff
                             if (ierr /= 0) exit
                             id = index(buff, 'ESP charges:')
                             if (id /= 0) then
                                read(u,*)
                          
                                if (m .eq. 1) then

                                   do j = 1, nat_array(m)
                                      read(u,*) num_lab1, lab1, qtot_1(j,i)
                                   end do

                                else
                     
                                   natig = 0
                                   do j = 1, (m-1)
                                      natig = natig + nat_array(j)
                                   end do
                         
                                   do j = 1, natig
                                      read(u,*) 
                                   end do
                                   do j = 1, nat_array(m) 
                                      read(u,*) num_lab1, lab1, qtot_1(j,i)
                                   end do
                           
                                end if
                             end if
                          end do
                       end do
                       close(u)
                         
                       open(newunit=u, file=trim(namefiles_qmmm(n))//'.log', status='old', &
                            & form='formatted')
                       ! rewind u
                       do i = 1, nstat(n)

                          ! call search("ESP charges:", card, u,
                          ! conferma)
                          do
                             read(u, '(A)', iostat=ierr) buff
                             if (ierr /= 0) exit
                             id = index(buff, 'ESP charges:')
                             if (id /= 0) then
                                read(u,*)

                                if (n .eq. 1) then

                                   do j = 1, nat_array(n)
                                      read(u,*) num_lab2, lab2, qtot_2(j,i)
                                   end do

                                else

                                   natig = 0
                                   do j = 1, (n-1)
                                      natig = natig + nat_array(j)
                                   end do

                                   do j = 1, natig
                                      read(u,*) 
                                   end do
                                   do j = 1, nat_array(n) 
                                      read(u,*) num_lab2, lab2, qtot_2(j,i)
                                   end do

                                end if
                             end if
                          end do
                       end do
                      close(u)

                    end select
                          
                    ! Loop over the states
                    ii = 0
                    do p = 1, m - 1
                       ii = ii + nstat(p)
                    end do

                    do i = 1, nstat(m)

                       ii = ii + 1

                       jj = 0
                       do q = 1, n - 1
                          jj = jj + nstat(q)
                       end do

                       do j = 1, nstat(n)
                          jj = jj + 1
                          eint = 0._dp
                          ! Loop over the atoms
                          do k = 1, nat_array(m)
                             num_x = 0._dp
                             num_y = 0._dp
                             num_z = 0._dp

                             do l = 1, nat_array(n)

                                r = sum((coord1(k,1:3) - coord2(l,1:3))**2)

                                ! Calculate the transition monopole aproximation (TMA) using
                                ! the transition charges (TrESP).       
                                eint = eint + ( qtot_1(k,i) * qtot_2(l,j) / sqrt(r) )

                                ! Calculate the dH/dR derivatives       
                                num_x = num_x - (((qtot_1(k,i)*qtot_2(l,j)) * &
                                        & (coord1(k,1)-coord2(l,1)))/r**(1.5))
                                num_y = num_y - (((qtot_1(k,i)*qtot_2(l,j)) * &
                                        & (coord1(k,2)-coord2(l,2)))/r**(1.5))
                                num_z = num_z - (((qtot_1(k,i)*qtot_2(l,j)) * & 
                                        & (coord1(k,3)-coord2(l,3)))/r**(1.5))
                             end do

                             grad_off_dig_x(w) = num_x
                             grad_off_dig_y(w) = num_y
                             grad_off_dig_z(w) = num_z

                             w = w + 1
                          end do
                          ham(ii,jj) = eint
                       end do
                    end do
                    deallocate (coord1, coord2, qtot_1, qtot_2)
                 end if
              end do
           end do
        else
           ! Compute the off-diagonal terms using mopac -> ab initio way to calculate...
           !  * For now, this approach works only in the case of two chromophores!
           allocate( elec_coup(nstat(1)*nstat(2)) )
           open(newunit=u, file=coup_file, status='old', form='formatted')
           ! rewind u
           ! call search("State_Chrom_1  State_Chrom_2     Exciton coupling (au)", &
           !             & card, u, conferma)
           do
              read(u, '(A)', iostat=ierr) buff
              if (ierr /= 0) exit
              id = index(buff, 'State_Chrom_1  State_Chrom_2     Exciton coupling (au)')
              if (id /= 0) then                                     
                 do m = 1, nstat(1)*nstat(2)
                    read(u,*) a, a, elec_coup(m)
                 end do
              end if
           end do
           close(u)

           ind_coup = 0

           do m = 1, nchrom-1
              ii = 0
              do p = 1, m - 1
                 ii = ii + nstat(p)
              end do
              do n = m+1, nchrom
                 ii = 0
                 do i = 1, nstat(m)
                    ii = ii + 1
                    jj = 0
                    do q = 1, n - 1
                       jj = jj + nstat(q)
                    end do
                    do j = 1, nstat(n)
                       jj = jj + 1
                       ind_coup = ind_coup + 1
                       ham(ii,jj) = elec_coup(ind_coup)
                       ham(jj,ii) = ham(ii,jj)
                    end do
                 end do
              end do
           end do

        end if

        ! Add the GS energy (reference) in the Exciton Hamiltonian in 
        ! order to obtain the full Hamiltonian

        allocate( ham_exp(nstat_tot+1, nstat_tot+1) )
        ham_exp(:,:) = 0._dp

        do i = 2, nstat_tot + 1
           do j = 2, nstat_tot + 1
              ham_exp(i,j) = ham(i-1, j-1)
           end do
        end do

        do i = 1, nstat_tot + 1
           do j = 1, nstat_tot + 1
              ham_tot(i,j) = ham_exp(i,j) + mat_e_ref(i,j)
           end do
        end do
  
        deallocate( ham_exp )
        deallocate( mat_e_ref )

        ! Diagonalize the Hamiltonian in order to obtain the eigenvalues (energies)  
        ! and eigenvectors (unitary matrix).                                            
        call eigfunc(ham, nstat_tot, mat_ene)

        vec_en(1) = e_ref
        do i = 2, nstat_tot+1
           vec_en(i) = mat_ene(i-1) + e_ref
        end do

        ! Construct the unitary matrix including the ground state
        eigenvectors(1,1) = 1._dp
        do i = 2, nstat_tot + 1
           do j = 2, nstat_tot + 1
              eigenvectors(i,j) = ham(i-1, j-1)
           end do
        end do

     end if

     !! Gradients section starts here!!! 
     allocate( grad_GS(numb_atom,3) )
     grad_GS(:,:) = 0._dp

     ! Computes the ground state (GS) gradients 

     select case(nameprog)
     
     case("exc_mopac")
        ! *Reads the QM/MM gradients for the GS
        allocate( grad_x(numb_atom,nchrom), grad_y(numb_atom,nchrom), &
                  & grad_z(numb_atom,nchrom) )
        grad_x(:,:) = 0._dp
        grad_y(:,:) = 0._dp
        grad_z(:,:) = 0._dp

        ! print *, 'NUMB ATOM = ', numb_atom, ' ; NCHROM = ', nchrom
        ! call system('sleep 1')
        do i = 1, nchrom
           open(newunit=u, file=trim(namefiles_qmmm(i))//"_nx.grad.all", &
                & status='old', form='formatted')
           ! print *, 'CHROM ', i
           rewind u
           do j = 1, numb_atom
              ! print *, 'j = ', j
              read(u, '(a)') buff
              ! print *, trim(buff)
              read(buff,*) grad_x(j,i), grad_y(j,i), grad_z(j,i)
           end do
           close(u)
        end do
    
        ! *Reads the MM gradients
        allocate( mmgrd(numb_atom,3) )
        mmgrd(:,:) = 0._dp

        open(newunit=u, file="fullMM_tnk.out", status='old', form='formatted')
        ! rewind u
        ! call search("Cartesian Gradient Breakdown over Individual Atoms :", &
        !              card, u, conferma)
        do
           read(u, '(A)', iostat=ierr) buff
           if (ierr /= 0) exit
           
           id = index(buff, 'Cartesian Gradient Breakdown over Individual Atoms :')
           if (id /= 0) then
              read(u,*)
              read(u,*)
              read(u,*)
              do i = 1, numb_atom
                 read(u,*) buff, j, (mmgrd(i,k), k = 1, 3)
              end do
              exit
           end if
        end do
        close(u)
        mmgrd = mmgrd / au2kcalm * au2ang

        ! *Finally, compute the GS gradients
        do i = 1, numb_atom
           grad_gs_x = 0._dp
           grad_gs_y = 0._dp
           grad_gs_z = 0._dp

           do j = 1, nchrom
              grad_gs_x = grad_gs_x + ( grad_x(i,j) - mmgrd(i,1) )
              grad_gs_y = grad_gs_y + ( grad_y(i,j) - mmgrd(i,2) )
              grad_gs_z = grad_gs_z + ( grad_z(i,j) - mmgrd(i,3) )
           end do

           grad_GS(i,1) = mmgrd(i,1) + grad_gs_x
           grad_GS(i,2) = mmgrd(i,2) + grad_gs_y
           grad_GS(i,3) = mmgrd(i,3) + grad_gs_z
        end do

        deallocate( grad_x, grad_y, grad_z )
        deallocate( mmgrd )

     case("exc_gaussian")

        ! *Reads the QM/MM gradients for the GS
        allocate( grad_x(numb_atom,nchrom), grad_y(numb_atom,nchrom), &
                  & grad_z(numb_atom,nchrom) )
        grad_x(:,:) = 0._dp
        grad_y(:,:) = 0._dp
        grad_z(:,:) = 0._dp

        do i = 1, nchrom
           open(newunit=u, file=trim(namefiles_qmmm(i))//".log", &
                & status='old', form='formatted')
           ! rewind u
           ! call search("Integrated Forces", card, u, conferma)
           do
              read(u, '(A)', iostat=ierr) buff
              if (ierr /= 0) exit
              id = index(buff, 'Integrated Forces')
              if (id /= 0) then
                 read(u,*)
                 read(u,*)
                 do j = 1, numb_atom
                    read(u,*) z, z, grad_x(j,i), grad_y(j,i), grad_z(j,i)
                 end do
              end if
           end do
           close(u)
        end do
    
        ! *Reads the MM gradients
        allocate( mmgrd(numb_atom,3) )
        mmgrd(:,:) = 0._dp

        open(newunit=u, file="exc_gau_MM.log", status='old', form='formatted')
        ! rewind u
        ! call search("Forces (Hartrees/Bohr)", card, u, conferma)
        do
           read(u, '(A)', iostat=ierr) buff
           if (ierr /= 0) exit
           id = index(buff, 'Forces (Hartrees/Bohr)')
           if (id /= 0) then
              read(u,*)
              read(u,*)
              do i = 1, numb_atom
                 read(u,*) z, z, (mmgrd(i,k), k = 1, 3)
              end do
           end if
        end do
        close(u)

        ! *Finally, compute the GS gradients
        do i = 1, numb_atom
           grad_gs_x = 0._dp
           grad_gs_y = 0._dp
           grad_gs_z = 0._dp

           do j = 1, nchrom
              grad_gs_x = grad_gs_x + ( grad_x(i,j) - mmgrd(i,1) )
              grad_gs_y = grad_gs_y + ( grad_y(i,j) - mmgrd(i,2) )
              grad_gs_z = grad_gs_z + ( grad_z(i,j) - mmgrd(i,3) )
           end do

           grad_GS(i,1) = mmgrd(i,1) + grad_gs_x
           grad_GS(i,2) = mmgrd(i,2) + grad_gs_y
           grad_GS(i,3) = mmgrd(i,3) + grad_gs_z
        end do
        grad_GS = grad_GS * (-1._dp)

        deallocate( grad_x, grad_y, grad_z )
        deallocate( mmgrd )
        
     end select

     if (traj%nstatdyn .ne. 1) then
        !! Compute the gradients of the site energies
        
        ! Array's size with the dig gad
        num_grad = 0
        do i = 1, nchrom
           num_grad = num_grad + numb_atom*nstat(i)
        end do
       
        allocate ( grad_dig(num_grad, 3) )
        grad_dig(:,:) = 0._dp
        
        select case(nameprog)

        case("exc_mopac")

           w = 1 
           do i = 1, nchrom
              do k = 1, nstat(i)
                 ii = k*numb_atom
                 allocate ( grad_dig_tot(numb_atom*(nstat(i)+1),3) )
                 grad_dig_tot(:,:) = 0._dp
                 call rgrad(numb_atom, nstat(i), namefiles_qmmm(i), grad_dig_tot)
                 do j = 1, numb_atom
                    ii = ii + 1
                    grad_dig(w,1) = grad_dig_tot(ii,1) - grad_dig_tot(j,1)  
                    grad_dig(w,2) = grad_dig_tot(ii,2) - grad_dig_tot(j,2)
                    grad_dig(w,3) = grad_dig_tot(ii,3) - grad_dig_tot(j,3)
                    w = w + 1
                 end do
                 deallocate (grad_dig_tot)
              end do
           end do

        case("exc_gaussian")
           
           p = 1
           do i = 1, nchrom

              open(newunit=u, file=trim(namefiles_qmmm(i))//'.log', status='old', &
                   & form='formatted') 
              ! rewind u
              w = 0
              allocate( grad_dig_tot(numb_atom*(nstat(i)+1),3) )
              grad_dig_tot(:,:) = 0._dp
              do k = 1, (nstat(i)+1)           
                 ! call search("Integrated Forces", card, u, conferma)
                 do
                    read(u, '(A)', iostat=ierr) buff
                    if (ierr /= 0) exit
                    id = index(buff, 'Integrated Forces')
                    if (id /= 0) then
                       read(u,*)
                       read(u,*)
                       do j = 1, numb_atom
                          w = w + 1
                          read(u,*) z, z, (grad_dig_tot(w,l), l = 1, 3)
                       end do
                    end if
                 end do
              end do

              do k = 1, nstat(i)
                 ii = k*numb_atom
                 do j = 1, numb_atom
                    ii = ii + 1
                    grad_dig(p,1) = grad_dig_tot(ii,1) - grad_dig_tot(j,1)
                    grad_dig(p,2) = grad_dig_tot(ii,2) - grad_dig_tot(j,2)
                    grad_dig(p,3) = grad_dig_tot(ii,3) - grad_dig_tot(j,3)
                    p = p + 1
                 end do
              end do
              close(u)
              deallocate( grad_dig_tot )

           end do

           grad_dig = grad_dig * (-1._dp)
        end select
              
        if (nx_qm%trespexc .ne. 1) then
           ! Read the off-dig gradients for the case in which the TrESP 
           ! aproximation is NOT take into account!
           ! * Array's dimension:
           ngrad = 0
           do i = 1, nchrom
              q = nat_array(i)*nstat(i)
              do j = 1, nchrom
                 if (j .ne. i) then
                    ngrad = ngrad + q*nstat(j)
                 end if
              end do
           end do
           allocate( grad_off(ngrad,3) )
           grad_off(:,:) = 0._dp
           call off_dig_grad(nchrom, nat_array, nstat, grad_off, coup_file)
        end if
 
        !! Final gradients section: 
        allocate( grad1(nstat_tot, nstat_tot), grad2(nstat_tot, nstat_tot), &
                  grad3(nstat_tot, nstat_tot) )
        grad1(:,:) = 0._dp
        grad2(:,:) = 0._dp
        grad3(:,:) = 0._dp
         
        allocate( eig_vec(nstat_tot) )
        eig_vec(:) = 0._dp

        ! Eigenvector of the current state
        do i = 1, nstat_tot
           eig_vec(i) = ham(i,(traj%nstatdyn-1))
        end do
   
        allocate ( full_grd_x_prima(nstat_tot), full_grd_y_prima(nstat_tot), &
                  full_grd_z_prima(nstat_tot) ) 
        full_grd_x_prima(:) = 0._dp
        full_grd_y_prima(:) = 0._dp
        full_grd_z_prima(:) = 0._dp

        allocate( full_grad_mat(numb_atom,3) )
        full_grad_mat(:,:) = 0._dp

        w = 0
        z = 0
        unz = 0
        g = 0
        ind_test = 0
        do i = 1, nchrom
           !if (nstat_tot .eq. 2) then
           if (nstat_tot .eq. nchrom) then
              if (i .gt. 1) then
                 w = w + nat_array(i-1)*nstat(i-1)
              else
                 w = w
              end if
           else
              if (i .gt. 1) then
                 w = w + nat_array(i-1)*nstat(i-1)
              else
                 w = w
              end if
           end if
           do j = 1, nat_array(i)
              unz = unz + 1
              grad1 = 0._dp
              grad2 = 0._dp
              grad3 = 0._dp
              z = z + 1
              do k = 1, nchrom-1
        
                 do l = k+1, nchrom
                    ii = 0
                    do p = 1, k-1
                       ii = ii + nstat(p)
                    end do
          
                    do m = 1, nstat(k)
                       ii = ii + 1
                       jj = 0
                       do q = 1, l-1
                          jj = jj + nstat(q)
                       end do
                       h = nat_array(i)*nstat(k)*(m-1)
             
                       do n = 1, nstat(l)
                          jj = jj + 1
                          g = h + w + nat_array(i)*(n-1) + j 
                          ind_test =  ind_test + 1
                          if (nx_qm%trespexc .eq. 1) then
                             grad1(ii,jj) = grad_off_dig_x(g)
                             grad1(jj,ii) = grad1(ii,jj)
                             grad2(ii,jj) = grad_off_dig_y(g)
                             grad2(jj,ii) = grad2(ii,jj)
                             grad3(ii,jj) = grad_off_dig_z(g)
                             grad3(jj,ii) = grad3(ii,jj)
                          else
                             grad1(ii,jj) = grad_off(ind_test,1)
                             grad1(jj,ii) = grad1(ii,jj)
                             grad2(ii,jj) = grad_off(ind_test,2)
                             grad2(jj,ii) = grad2(ii,jj)
                             grad3(ii,jj) = grad_off(ind_test,3)
                             grad3(jj,ii) = grad3(ii,jj)
                          end if
                       end do
                    end do
                  end do
               end do
               do yy = 1, nstat_tot
                  zz = (yy-1)*numb_atom + unz
                  grad1(yy,yy) = grad_dig(zz,1)
                  grad2(yy,yy) = grad_dig(zz,2)
                  grad3(yy,yy) = grad_dig(zz,3)
               end do
               full_grd_x_prima = matmul(grad1, eig_vec)
               full_grd_x = dot_product(eig_vec, full_grd_x_prima)
               full_grd_y_prima = matmul(grad2, eig_vec)
               full_grd_y = dot_product(eig_vec, full_grd_y_prima)
               full_grd_z_prima = matmul(grad3, eig_vec)
               full_grd_z = dot_product(eig_vec, full_grd_z_prima)
 
               full_grad_mat(z,1) = full_grd_x
               full_grad_mat(z,2) = full_grd_y
               full_grad_mat(z,3) = full_grd_z
             end do
          end do

          atom_dif = numb_atom - nat_tot
   
          if (nat_tot < numb_atom) then
             do j = 1, atom_dif
                z = z + 1
                unz = unz + 1
                grad1 = 0._dp
                grad2 = 0._dp
                grad3 = 0._dp
                do k = 1, nstat_tot
                   zz = (k-1)*numb_atom + unz
                   grad1(k,k) = grad_dig(zz,1)
                   grad2(k,k) = grad_dig(zz,2)
                   grad3(k,k) = grad_dig(zz,3)
                end do
             full_grd_x_prima = matmul(grad1, eig_vec)
             full_grd_x = dot_product(eig_vec, full_grd_x_prima)
             full_grd_y_prima = matmul(grad2, eig_vec)
             full_grd_y = dot_product(eig_vec, full_grd_y_prima)
             full_grd_z_prima = matmul(grad3, eig_vec)
             full_grd_z = dot_product(eig_vec, full_grd_z_prima)

             full_grad_mat(z,1) = full_grd_x
             full_grad_mat(z,2) = full_grd_y
             full_grad_mat(z,3) = full_grd_z
          end do
       end if
      
       ! Add to the exciton gradients the GS gradients in order to 
       ! take the total gradients
       do i = 1, numb_atom
          full_grad_mat(i,1) = full_grad_mat(i,1) + grad_GS(i,1)
          full_grad_mat(i,2) = full_grad_mat(i,2) + grad_GS(i,2)
          full_grad_mat(i,3) = full_grad_mat(i,3) + grad_GS(i,3)
       end do
 
       grad_total = full_grad_mat

    else 

       grad_total = grad_GS

    end if 
    !! Gradient section finishes here!!!

    deallocate( grad_GS )
    if (traj%nstatdyn .ne. 1) then
       deallocate( full_grad_mat )
       deallocate( eig_vec )
    end if
    
     !!Calculate the transition dipole moment
     if (nx_qm%gsexc .gt. 0) then

        continue

     else

        if (nx_qm%dipexc .gt. 0) then

           allocate( trdip(nstat_tot, 3) )
           call trandip(nchrom, nstat, ham, namefiles_qmmm, trdip, nameprog)
           trdip_debye = trdip * au2debye

           ! Calculate the oscillator strenght
           allocate ( mod_trdip(nstat_tot) )
           mod_trdip(:) = 0._dp

           ! Calculate the module of the transitions dipole moments
           do i = 1, nstat_tot
              mod_trdip(i) = sqrt( trdip(i,1)**2 + trdip(i,2)**2 + trdip(i,3)**2 )
           end do

           osc_st = 0._dp
           do i = 1, nstat_tot
              mat_en = mat_ene(i)
              mod_trd = mod_trdip(i)
              osc = mat_en*(mod_trd**2)*2/3
              osc_st(i) = osc
           end do
         
         deallocate( trdip, mod_trdip )

         end if

      end if
     
      !! Deallocate some arrays and matrices
      deallocate( omega )
      deallocate( array_GS )
      deallocate( grad_off_dig_x, grad_off_dig_y, grad_off_dig_z )
      if (nx_qm%trespexc .ne. 1) then
         deallocate( grad_off)
      end if

  end subroutine calc_en_grad

  subroutine rxyz (natn, coordn, init)
  !! Read the geometry in coulomb format
     integer :: i, j
     integer :: u
     integer :: natn
     integer :: init

     real(dp) :: rbuff

     real(dp), dimension(:,:), intent(inout) :: coordn

     character(len=10) :: cbuff

     open(newunit=u, file="geom", status='old', form='formatted')
     rewind u
     do i = 1, init
        read(u,*)
     end do
     do i = 1, natn
        read(u,*) cbuff, rbuff, (coordn(i,j), j = 1, 3), rbuff
     end do
     close(u)
  end subroutine rxyz

  subroutine eigfunc(ham, nstat_tot, mat_ene)
  !! This subroutine is used in order to diagonalize matrices
     integer :: nstat_tot
     integer :: Lwmax
     integer :: Lwork
     integer :: Iwork(5000)
     integer :: Liwork
     integer :: info

     real(dp), dimension(nstat_tot, nstat_tot) :: ham

     real(dp), dimension(nstat_tot) :: mat_ene

     real(dp) :: work(5000)

     Lwmax = 5000
     Lwork = -1
     iwork = -1

     call DSYEVD('Vectors','Upper',nstat_tot,ham,nstat_tot,mat_ene,work, &
                 & Lwork,Iwork,Liwork,info)
     LWORK = min( LWMAX, int( WORK( 1 ) ) )
     LIWORK = min( LWMAX, IWORK(1) )

     ! Solve eigenproblem
     call DSYEVD('Vectors','Upper',nstat_tot,ham,nstat_tot,mat_ene,work, &
                 & Lwork,Iwork,Liwork,info)

     ! Checking for problems in the diagonalization...
     if (info > 0) then
         write(*,*)'The algorithm failed to compute eigenvalues.'
         write(*,*)
     end if
   end subroutine eigfunc

   subroutine rgrad(nat, nstat, name_grad, grad_dig_tot)
   !! Read the MOPAC's gradients
     integer :: i, j
     integer :: u
     integer :: nstat
     integer :: nat

     character(len=120) :: name_grad

     real(dp), dimension(:,:), intent(inout) :: grad_dig_tot

     open(newunit=u, file=trim(name_grad)//"_nx.grad.all", status='old', &
          & form='formatted')
     rewind u
     do i = 1, (nstat+1)*nat
        read(u,*) (grad_dig_tot(i,j), j = 1, 3)
     end do
     close(u)
   end subroutine rgrad

   subroutine off_dig_grad(nchrom, nat_array, nstat, grad_off, nfile)
     integer :: i, j, k, l, m, n
     integer :: u
     integer :: nchrom
     integer :: total
     integer :: stat1, stat2

     integer, dimension(:), intent(inout) :: nat_array
     integer, dimension(:), intent(inout) :: nstat

     real(dp) :: grad

     real(dp), dimension(:,:), intent(inout) :: grad_off

     character(len=120) ::  nfile
     integer :: ierr, id
     character(len=1024) :: buff

     total = nstat(1)*nstat(2)

     open(newunit=u, file=nfile, status='old', form='formatted')
     ! rewind u
     ! call search("State_Chrom_1  State_Chrom_2     Non-relaxed derivative of V_ec (au) wrt coord.     1", &
     !      card, u, conferma)
     m = 0
     n = 0
     do
        read(u, '(A)', iostat=ierr) buff
        if (ierr /= 0) exit
        id = index(buff, 'State_Chrom_1  State_Chrom_2     Non-relaxed derivative of V_ec (au) wrt coord.     1')
        if (id /= 0) then
           do i = 1, nchrom
              do j = 1, nat_array(i)
                 do l = 1, 3
                    m = total*(j-1) + n
                    do k = 1, total
                       m = m + 1
                       read(u,*) stat1, stat2, grad
                       grad_off(m,l) = grad
                    end do
                    read(u,*)
                 end do
              end do
              n = m
           end do
        end if
     end do
     close(u)
   end subroutine off_dig_grad

   subroutine calc_ovl(nx_qm, nx_traj)
   !! Calculates the wavefunction overlap matrix for the exciton model
     type(nx_qm_t), intent(inout) :: nx_qm
     !! QM object.
     type(nx_traj_t) :: nx_traj
     !! Main classical trajectory

     integer :: u
     integer :: h, i, j, k ,l, m, n, p, q, w, y, z
     integer :: ii, jj
     integer :: nchrom
     integer :: nstat_tot
     integer :: nstat_gs

     integer, allocatable, dimension(:) :: nstat
  
     character(len=120) :: filename
     character(len=120) :: nameprog
     character(len=120) :: name_ind
     character(len=120) :: file_ovl
     character(len=120) :: file_qm
     character(len=120) :: nameprog_inp
     
     character(len=120), allocatable, dimension(:) :: namefiles_ovl
     character(len=120), allocatable, dimension(:) :: namefiles_qmmm
  
     real(dp) ::  ovl_gs
     real(dp) ::  res
     real(dp) ::  rand

     real(dp), allocatable, dimension(:) :: ovlgs
     real(dp), allocatable, dimension(:) :: firstline
     real(dp), allocatable, dimension(:) :: prod
     real(dp), allocatable, dimension(:) :: firstarray

     real(dp), allocatable, dimension(:,:) :: temp_over
     real(dp), allocatable, dimension(:,:) :: temp_over1
     real(dp), allocatable, dimension(:,:) :: temp_over2
     real(dp), allocatable, dimension(:,:) :: prime
     real(dp), allocatable, dimension(:,:) :: old_eigenvectors
     real(dp), allocatable, dimension(:,:) :: mat_overlap
     real(dp), allocatable, dimension(:,:) :: adoverlap
     real(dp), allocatable, dimension(:,:) :: eigenvectors
     
     integer :: ierr, id
     character(len=1024) :: buff

     nchrom = nx_qm%nchromexc
     
     allocate( nstat(nchrom) )
     nstat = nx_qm%nstatexc

     nstat_tot = sum(nstat) 
     
     allocate( eigenvectors((nstat_tot+1), (nstat_tot+1)) )
     eigenvectors = nx_qm%eigenvectorsexc
 
     nameprog = nx_qm%qmcode

     allocate( mat_overlap((nstat_tot+1), (nstat_tot+1)) )
     mat_overlap(:,:) = 0._dp
     allocate( adoverlap((nstat_tot+1), (nstat_tot+1)) )
     adoverlap(:,:) = 0._dp

     allocate( namefiles_qmmm(nchrom) )
     namefiles_qmmm(:) = ""

     select case(nameprog)
     case('exc_mopac')
        nameprog_inp = 'exc_mop'
     case('exc_gaussian')
        nameprog_inp = 'exc_gau'
     end select

     ! Array which contains the name of the qmmm calculations for all the
     ! chromophores
     do i = 1, nchrom
        write (name_ind, "(I10)") i
        write (file_qm, "(A)") nameprog_inp
        namefiles_qmmm(i) =trim(adjustl(file_qm))//trim(adjustl(name_ind))   
     end do     

     !! 
     allocate(ovlgs(nchrom))
     ovlgs = 0._dp

     if (nx_qm%gsexc .gt. 0) then

        adoverlap(1,1) = 1._dp
        mat_overlap(1,1) = 1._dp

     else

        if (nx_traj%step .eq. 0) then
        
           do i = 1, nstat_tot+1
              adoverlap(i,i) = 1._dp
              mat_overlap(i,i) = 1._dp
           end do

        else

           !! First element of the overlap matrix <S0|S0>
           select case(nameprog)
      
           case("exc_mopac")
           
              do i = 1, nchrom
                 filename = namefiles_qmmm(i)
                 open(newunit=u, file=trim(filename)//'_nx.run_cioverlap.log', &
                      & status='old', form='formatted')
                 rewind u
                 read(u,*)
                 read(u,*) ovl_gs
                 close(u)
                 ovlgs(i) = ovl_gs
              end do
           
           case("exc_gaussian")
           
              ! Array which contains the name of the overlap matrix files for all the
              ! chromophores
              allocate( namefiles_ovl(nchrom) )
              namefiles_ovl(:) = ""
              do i = 1, nchrom
                 write (name_ind, "(I10)") i
                 write (file_ovl, "(A)") "chrom_"
                 namefiles_ovl(i) = trim(adjustl(file_ovl))//trim(adjustl(name_ind))//'/cioverlap/cioverlap.out'
              end do
      
              do i = 1, nchrom
                 filename = namefiles_ovl(i)
                 open(newunit=u, file=filename, status='old', &
                      & form='formatted')
                 ! rewind u
                 ! call search("CI overlap matrix", card, u,
                 ! conferma)
                 do
                    read(u, '(A)', iostat=ierr) buff
                    if (ierr /= 0) exit
                    id = index(buff, 'CI overlap matrix')
                    if (id /= 0) then
                       read(u,*)
                       read(u,*) ovl_gs
                       close(u)
                       ovlgs(i) = ovl_gs
                    end if
                 end do
              end do
      
           end select
              
           mat_overlap(1,1) = product(ovlgs)
        
           !! Build the first line of the overlap matrix
           select case(nameprog)
      
           case("exc_mopac")
           
              w = 1
              do i = 1, nchrom
        
                 filename = namefiles_qmmm(i)
                 open(newunit=u, file=trim(filename)//'_nx.run_cioverlap.log', &
                      & status='old', form='formatted')
              
                 allocate( firstline(nstat(i)+1) )
                 firstline(:) = 0._dp
        
                 rewind u
                 read(u,*)
                 read(u,*) ( firstline(j), j = 1, (nstat(i)+1) )
                 close(u)
        
                 allocate ( prod(nchrom) )
                 prod(:) = 0._dp
                 h = 0
                 do l = 1, nchrom
                    if (l .ne. i ) then
                       prod(l) = ovlgs(l)
                    else
                       prod(l) = 1._dp
                    end if
                 end do
                 res = product(prod)
        
                 do k = 2, nstat(i)+1
                    w = w + 1
                    mat_overlap(1,w) = firstline(k)*res
                 end do
        
                 deallocate ( prod, firstline )
        
              end do
      
           case("exc_gaussian")
      
              w = 1
              do i = 1, nchrom
        
                 filename = namefiles_ovl(i)
                 open(newunit=u, file=filename, status='old', &
                      & form='formatted')
                 ! rewind u
      
                 ! call search("CI overlap matrix", card, u,
                 ! conferma)
                 do
                    read(u, '(A)', iostat=ierr) buff
                    if (ierr /= 0) exit
                    id = index(buff, 'CI overlap matrix')
                    if (id /= 0) then
              
                       allocate( firstline(nstat(i)+1) )
                       firstline(:) = 0._dp
        
                       read(u,*)
                       read(u,*) ( firstline(j), j = 1, (nstat(i)+1) )
                    end if
                 end do
                 close(u)
        
                 allocate ( prod(nchrom) )
                 prod(:) = 0._dp
                 h = 0
                 do l = 1, nchrom
                    if (l .ne. i ) then
                       prod(l) = ovlgs(l)
                    else
                       prod(l) = 1._dp
                    end if
                 end do
                 res = product(prod)
        
                 do k = 2, nstat(i)+1
                    w = w + 1
                    mat_overlap(1,w) = firstline(k)*res
                 end do
        
                 deallocate ( prod, firstline )
        
              end do
      
           end select
      
           !! Build the first array of the overlap matrix
           select case(nameprog)
      
           case("exc_mopac")
        
              w = 1
              do i = 1, nchrom
        
                 filename = namefiles_qmmm(i)
                 open(newunit=u, file=trim(filename)//'_nx.run_cioverlap.log', &
                      & status='old', form='formatted')
        
                 allocate( firstarray(nstat(i)+1) )
                 firstarray(:) = 0._dp
        
                 rewind u
                 read(u,*)
                 do j = 1, nstat(i)+1
                    read(u,*) rand
                    firstarray(j) = rand
                 end do
                 close(u)
        
                 allocate( prod(nchrom) )
                 do k = 1, nstat(i)
                    w = w + 1
                    prod = 0._dp
                    res = 0._dp
                    h = 0
                    do l = 1, nchrom
                       if (l .ne. i ) then
                          prod(l) = ovlgs(l)
                       else
                          prod(l) = 1._dp
                       end if
                    end do
                    res = product(prod)
                    mat_overlap(w,1) = firstarray(k+1)*res
                 end do
        
                 deallocate ( prod, firstarray )
        
              end do
              
           case("exc_gaussian")
      
              w = 1
              do i = 1, nchrom
        
                 filename = namefiles_ovl(i)
                 open(newunit=u, file=filename, status='old', &
                      & form='formatted')
                 ! rewind u
        
                 ! call search("CI overlap matrix", card, u,
                 ! conferma)
                 do
                    read(u, '(A)', iostat=ierr) buff
                    if (ierr /= 0) exit
                    id = index(buff, 'CI overlap matrix')
                    if (id /= 0) then
      
                       allocate( firstarray(nstat(i)+1) )
                       firstarray(:) = 0._dp
        
                       read(u,*)
                       do j = 1, nstat(i)+1
                          read(u,*) rand
                          firstarray(j) = rand
                       end do
                    end if
                 end do
                 close(u)
        
                 allocate( prod(nchrom) )
                 do k = 1, nstat(i)
                    w = w + 1
                    prod = 0._dp
                    res = 0._dp
                    h = 0
                    do l = 1, nchrom
                       if (l .ne. i ) then
                          prod(l) = ovlgs(l)
                       else
                          prod(l) = 1._dp
                       end if
                    end do
                    res = product(prod)
                    mat_overlap(w,1) = firstarray(k+1)*res
                 end do
                 deallocate ( prod, firstarray )
        
              end do
              
           end select
      
           !! Build the diagonal blocks
           y = 1
           do i = 1, nchrom
        
              if (i .gt. 1) then
                 y = y + nstat(i-1)
              else
                 y = y
              end if
        
              ! Take the overlap matrix for chromophore "i"
              allocate ( temp_over(nstat(i)+1, nstat(i)+1) )
              temp_over(:,:) = 0._dp
       
              select case(nameprog)
      
              case("exc_mopac")
      
                 open(newunit=u, file=trim(namefiles_qmmm(i))//"_nx.run_cioverlap.log", &
                      & status='old', form='formatted')
                 rewind u
                 read(u,*)
                 do j = 1, nstat(i)+1
                    read(u,*) (temp_over(j,k), k = 1, nstat(i)+1)
                 end do
                 close(u)
           
              case("exc_gaussian") 
      
                 open(newunit=u, file=namefiles_ovl(i), status='old', &
                      & form='formatted')
                 ! rewind u
                 ! call search("CI overlap matrix", card, u,
                 ! conferma)
                 do
                    read(u, '(A)', iostat=ierr) buff
                    if (ierr /= 0) exit
                    id = index(buff, 'CI overlap matrix')
                    if (id /= 0) then
                       read(u,*)
                       do j = 1, nstat(i)+1
                          read(u,*) (temp_over(j,k), k = 1, nstat(i)+1)
                       end do
                    end if
                 end do
                 close(u)
      
              end select
        
              ! Take the product overlap of the other chromophores (for this case is only between GS state)
              allocate( prod (nchrom-1) )
              prod(:) = 0._dp
              h = 0
              q = 0
              do l = 1, nchrom
                 if (l .ne. i ) then
                    q = q + 1
                    prod(q) = ovlgs(l)
                 end if
              end do
              res = product(prod)
              deallocate ( prod )
        
              do j = 1, nstat(i)
                 z = y + j
        
                 do k = 1, nstat(i)
                    w = y + k
                    mat_overlap(z,w) = temp_over(j+1,k+1)*res
                 end do
              end do
              deallocate( temp_over )
           end do
        
           !! Build the off-diagonal blocks
           
           ! Loop over the chromophores:
           do m = 1, nchrom
              do n = 1, nchrom
                if (m .ne. n) then
                   allocate ( prod (nchrom-2) )
                    prod(:) = 0._dp
                    h = 0
                    q = 0
                    do l = 1, nchrom
                       if (l .ne. m .and. l .ne. n ) then
                          q = q + 1
                          prod(q) = ovlgs(l)
                       end if
                    end do
                    res = product(prod)
                    deallocate ( prod )
        
                    if (nchrom .le. 2) then
                       res = 1._dp
                    end if
        
                    allocate( temp_over1(nstat(m)+1,nstat(m)+1) ) 
                    allocate( temp_over2(nstat(n)+1,nstat(n)+1) )
                    temp_over1(:,:) = 0._dp
                    temp_over2(:,:) = 0._dp
                    
                    select case(nameprog)
                 
                    case("exc_mopac")
        
                       open(newunit=u, file=trim(namefiles_qmmm(m))//"_nx.run_cioverlap.log", &
                            & status='old', form='formatted')
                       rewind u
                       read(u,*)
                       do j = 1, nstat(m)+1
                          read(u,*) (temp_over1(j,k), k = 1, nstat(m)+1)
                       end do
                       close(u)
                    
                       open(newunit=u, file=trim(namefiles_qmmm(n))//"_nx.run_cioverlap.log", &
                            & status='old', form='formatted')
                       rewind u
                       read(u,*)
                       do j = 1, nstat(n)+1
                          read(u,*) (temp_over2(j,k), k = 1, nstat(n)+1)
                       end do
                       close(u)
                       
                    case("exc_gaussian")
        
                       open(newunit=u, file=namefiles_ovl(m), status='old', &
                            & form='formatted')
                       ! rewind u
                       ! call search("CI overlap matrix", card, u,
                       ! conferma)
                       do
                          read(u, '(A)', iostat=ierr) buff
                          if (ierr /= 0) exit
                          id = index(buff, 'CI overlap matrix')
                          if (id /= 0) then
                             read(u,*)
                             do j = 1, nstat(m)+1
                                read(u,*) (temp_over1(j,k), k = 1, nstat(m)+1)
                             end do
                          end if
                       end do
                       close(u)
                    
                       open(newunit=u, file=namefiles_ovl(n), status='old', &
                            & form='formatted')
                       ! rewind u
                       ! call search("CI overlap matrix", card, u,
                       ! conferma)
                       do
                          read(u, '(A)', iostat=ierr) buff
                          if (ierr /= 0) exit
                          id = index(buff, 'CI overlap matrix')
                          if (id /= 0) then
                             read(u,*)
                             do j = 1, nstat(n)+1
                                read(u,*) (temp_over2(j,k), k = 1, nstat(n)+1)
                             end do
                          end if
                       end do
                       close(u)
                    
                    end select
        
                    ! Loop over the states
                    ii = 1
                    do p = 1, m-1
                       ii = ii + nstat(p)
                    end do
        
                    do i = 2, nstat(m)+1
                       ii = ii + 1
        
                       jj = 1
                       do q = 1, n-1
                          jj = jj + nstat(q)
                       end do
        
                       do j = 2, nstat(n)+1
                          jj = jj + 1
                          mat_overlap(ii,jj) = temp_over1(i,1)*temp_over2(1,j)*res
                       end do
                    end do
                    deallocate( temp_over1, temp_over2 )
                 end if
              end do
           end do
          
           !! Now, we have to obtain the overlap matrix in adiabatic basis...
           nstat_gs = nstat_tot + 1
           allocate( old_eigenvectors(nstat_gs, nstat_gs) )
           allocate( prime(nstat_gs, nstat_gs) )
           old_eigenvectors(:,:) = 0._dp
           prime(:,:) = 0._dp
      
           open(newunit=u, file="old_eigenvectors.txt", status='old', &
                & action='read', form='formatted')
           rewind u
           do i = 1, nstat_tot+1
              read(u,*) (old_eigenvectors(i,j), j = 1, nstat_tot+1)
           end do
           close(u)
            
           prime = matmul(transpose(old_eigenvectors), mat_overlap)
           adoverlap = matmul(prime, eigenvectors)
      
           deallocate( old_eigenvectors )
           deallocate( prime )
        
        end if

     end if
    
     !! Writes the overlap matrix in adiabatic basis
     nstat_gs = nstat_tot + 1
     select case(nx_qm%qmcode)
     case("exc_mopac")
        open(newunit=u, file='exc_overlap.out', status='unknown', form='formatted')
     case("exc_gaussian")
        open(newunit=u, file='./cioverlap/cioverlap.out', status='unknown', &
             & form='formatted')
     end select
     rewind u
     write(u,*) 'CI overlap matrix'
     write(u,*) nstat_gs, nstat_gs
     do i = 1, nstat_gs
        write(u,*) (adoverlap(i,j), j = 1, nstat_gs)
     end do
     close(u)
     
     deallocate( ovlgs )
     deallocate( mat_overlap )
     deallocate( adoverlap )
     deallocate( namefiles_qmmm )
     deallocate( eigenvectors )
     deallocate( nstat )
   end subroutine calc_ovl

   subroutine wout(nx_qm, conf, traj, out_name, nstat_tot, ham_tot, mat_ene, &
                  & eigenvectors, trdip_debye, osc_st)
   !! Write a formatted input file for the exciton model. I like to use it just to 
   !! check the results...
     type(nx_qm_t) :: nx_qm
     !! Nx_Qm object.
     type(nx_config_t) :: conf
     !! General configuration.
     type(nx_traj_t) :: traj
     !! Classical trajectory.
     
     integer :: i, j
     integer :: u
     integer :: nstat_tot

     integer, allocatable, dimension(:) :: indice1, indice2
   
     real(dp) :: ti

     real(dp), dimension(:), intent(inout) :: mat_ene
     real(dp), dimension(:), intent(inout) :: osc_st

     real(dp), dimension(:,:), intent(inout) :: ham_tot
     real(dp), dimension(:,:), intent(inout) :: eigenvectors
     real(dp), dimension(:,:), intent(inout) :: trdip_debye

     character(len=120) :: out_name

     ti = conf%dt * traj%step

     open(newunit=u, file=trim(out_name)//'.out', status='unknown', position='append', &
              action='write')
     !
     !
     !
     if (traj%step == 0) then
        write(u,*)
        write(u,*) "***************************************************************************"
        write(u,*) "*                       _______  __ ___   _____ __  __                    *"
        write(u,*) "*                      / ____/ |/ //   | / ___// / / /                    *"
        write(u,*) "*                     / __/  |   // /_|| \__ \/ /_/ /                     *"
        write(u,*) "*                    / /___ /   |/ ___ |___/ / __  /                      *"
        write(u,*) "*                   /_____//_/|_/_/  |_/____/_/ /_/                       *"
        write(u,*) "*                                                                         *"
        write(u,*) "*           EXcitonic Analysis for Surface Hopping dynamics               *"
        write(u,*) "*                                                                         *"
        write(u,*) "*                             Eduarda S. Gil                              *"
        write(u,*) "*                             Novermber 2021                              *"
        write(u,*) "*                                                                         *"
        write(u,*) "*                                                                         *"
        write(u,*) "* => This program calculates:                                             *"
        write(u,*) "*    -> The excitation energies within the Frankel exciton model;         *"
        write(u,*) "*    -> Approximated electronic couplings;                                *"
        write(u,*) "*    -> The gradients for the exciton model;                              *"
        write(u,*) "*    -> The wave function overlaps between two consecutive time steps     *"
        write(u,*) "*       (it's needed in the local diabatization algorithm);               *"
        write(u,*) "*    -> Diabat populations (exciton populations);                         *"
        write(u,*) "*    -> The transition dipole moments for the Exciton Model.              *"
        write(u,*) "*    -> The oscillator strenghts for the exciton model.                   *"
        write(u,*) "*                                                                         *"
        write(u,*) "***************************************************************************"
        if (nx_qm%trespexc .eq. 1) then
           write(u,*)
           write(u,*) "The excitonic coupling between two states on different chromophores will be"
           write(u,*) "evaluated  using the  transition monopole aproximation (TMA) which is based"
           write(u,*) "on the transition charges (TrESP)."
        else
           write(u,*)
           write(u,*) "The excitonic coupling between two states on different chromophores will be"
           write(u,*) "evaluated exactly."
        end if
        write(u,*)
     end if
     write(u,*)
     write(u,*)"--------------------------------------------------------------------"
     write(u,*)
     write(u,*)" STEP:",traj%step
     write(u,*)" TIME:",ti
     write(u,*)
     write(u,*)"--------------------------------------------------------------------"
     write(u,*)
     write(u,*)"*********** => Informations read from file control.d <= ************"
     write(u,*)
     write(u,'(a,i8)') " natom =",conf%nat
     write(u,'(a,i8)') " nc    =",traj%step
     write(u,'(a,i8)') " nsurf =",conf%nstat
     write(u,'(a,i8)') " isurf =",traj%nstatdyn
     write(u,'(a,F20.9)') " timestep   =",conf%dt
     write(u,*)
     write(u,*)"********************************************************************"
     if (traj%step .eq. 0 .and. conf%nstat .ne. (nstat_tot+1) ) then
        write(u,*)
        write(u,*) "The number of states in NX is: ", conf%nstat
        write(u,*) "The number of states in exciton program is: ", (nstat_tot+1)
        write(u,*) "The number of states in NX and in exciton &
                    & program must be the same..."
        write(u,*) "Aborting calculation..."
        call exit
        write(u,*)
     end if
  
     !! Print the full Hamiltonian
     write(u,*)
     write(u,*)
     write(u,*) "=> EXCITONIC HAMILTONIAN (a.u) <="
     write(u,*)
     do i = 1, nstat_tot + 1
        write(u,'(*(f15.8))') (ham_tot(i,j), j = 1, nstat_tot + 1)
     end do

     !! Write the Excitonic Energies 
     write(u,*)
     write(u,*)
     write(u,*) "=> EXCITONIC ENERGIES (a.u.) <="
     write(u,*)
     do i = 1, nstat_tot
        write(u,'(2x,f20.9)') mat_ene(i)
     end do
     write(u,*)

     !!Print the eigenvectors
     write(u,*)
     write(u,*) " => EIGENVECTORS OF THE EXCITONIC HAMILTONIAN (a.u.) <= "
     do i = 1, nstat_tot+1
        write(u,'(*(f15.8))') (eigenvectors(i,j), j = 1,nstat_tot+1)
     end do
     write(u,*)

     if (nx_qm%dipexc .gt. 0) then
      
        !! Print the transition dipoles
        allocate( indice1(nstat_tot), indice2(nstat_tot) )
        indice1(:) = 0
        indice2(:) = 0
        do i = 1, nstat_tot
           indice1(i) = i + 1
        end do
        do i = 1, nstat_tot
           indice2(i) = 1
        end do

        write(u,*)
        write(u,*) "                            => Transition dipoles in Debye <="
        write(u,*) "   I  J                                <I| mu |J>"
        do i = 1, nstat_tot
           write(u,'(2x,i3,i3,5x,*(f20.15))') indice2(i), indice1(i), (trdip_debye(i,j), j=1,3)
        end do
        write(u,*)
        write(u,*)

        !! Write the oscillator strenght
        mat_ene = mat_ene * au2ev
        write(u,*) "  States          Excitation Energy(eV)          Oscillator strength"
        write(u,*)
        do i = 1, nstat_tot
           write(u,'(2x,i3,i3,9x,f20.15,11x,f20.15)') indice2(i), indice1(i), &
                 & mat_ene(i), osc_st(i)
        end do
        write(u,*)
  
        deallocate( indice1, indice2 )

     end if
     close(u)
  end subroutine wout

  subroutine search(targt, stringa, nfiles, conferma)
  !! Search a string "target"
     character (len=*), intent (in)  :: targt
     character (len=*), intent (out) :: stringa
     integer, intent(in)  :: nfiles
     logical, intent(out) :: conferma
 
     integer :: icod
 
     intrinsic Index
 
     conferma=.false.
     do
        if (conferma) exit
        read(nfiles,'(A)',iostat=icod)stringa
        if (icod < 0) exit
        conferma=(Index(stringa,targt) /= 0)
     end do
 
     return
  end subroutine search

  subroutine search2(targt, stringa, nfiles, conferma, nl)
  !! Search a string "target"
     character (len=*), intent (in)  :: targt
     character (len=*), intent (out) :: stringa
     integer, intent(in)  :: nfiles
    logical, intent(out) :: conferma

    integer :: icod, nl, i

    intrinsic Index

    conferma=.false.     
    do i = 1, nl
       if (conferma) exit
       read(nfiles,'(A)',iostat=icod)stringa
       if (icod < 0) exit
       conferma=(Index(stringa,targt) /= 0)
    end do

    return
end subroutine search2

subroutine trandip (nchrom, nstat, ham, namefiles_qmmm, trdip, nameprog)
!! Calculate the transition dipole moments of the exciton model
   integer :: i, j, k
   integer :: ii, jj
   integer :: u
   integer :: nchrom
   integer :: stati
   integer :: statj
   integer :: nstat_tot

   integer, dimension(:), intent(inout) :: nstat

   real(dp) :: compx, compy, compz
   real(dp) :: dipx, dipy, dipz

   real(dp), allocatable,  dimension(:,:) :: mu

   real(dp), allocatable,  dimension(:) :: eigvec

   real(dp), dimension(:,:), intent(inout) :: ham
   real(dp), dimension(:,:), intent(inout) :: trdip

   character(len=120) :: filename
   character(len=120) :: nameprog

   character(len=120), dimension(:), intent(inout) :: namefiles_qmmm

   integer :: ierr, id
   character(len=1024) :: buff
   real(dp) :: dumr
   
   nstat_tot = sum(nstat)
   allocate( mu(nstat_tot,3) )
   mu(:,:) = 0._dp

   select case(nameprog)
  
   case("exc_mopac") 
  
      k = 0
      do i = 1, nchrom
         ! Calculate the dimension of the next loop
         jj = 0
         do ii = 1, (nstat(i) + 1)
            jj = jj + ii
         end do

         filename = trim(namefiles_qmmm(i))//".out"
         open(newunit=u, file=filename, status='old', action='read', &
              & form='formatted')

         ! rewind u
         ! call search("Dipoles (au)", card, u, conferma)
         do
            read(u, '(A)', iostat=ierr) buff
            if (ierr /= 0) exit
            id = index(buff, 'Dipoles (au)')
            if (id /= 0) then
               read(u,*)
               do j = 1, jj
                  read(u,*) stati, statj, compx, compy, compz
                  if (stati .ne. 1 .and. statj .eq. 1) then
                     k = k + 1
                     mu(k,1) = compx
                     mu(k,2) = compy
                     mu(k,3) = compz
                  end if
               end do
            end if
         end do
         close(u)
      end do

   case("exc_gaussian")

      k = 0
      do i = 1, nchrom

         filename = trim(namefiles_qmmm(i))//".log"
         open(newunit=u, file=filename, status='old', action='read', &
              & form='formatted')

         ! rewind u
         ! call search("Ground to excited state transition electric dipole moments (Au)", &
         !             & card, u, conferma)
         do
            read(u, '(A)', iostat=ierr) buff
            if (ierr /= 0) exit
            id = index(buff, 'Ground to excited state transition electric dipole moments (Au)')
            if (id /= 0) then
               read(u,*)
               do j = 1, nstat(i)
                  k = k + 1
                  ! read(u,*) stati, compx, compy, compz, dumr, dumr
                  read(u,*) stati, mu(k,1), mu(k,2), mu(k,3), dumr, dumr
                  ! mu(k,1) = compx
                  ! mu(k,2) = compy
                  ! mu(k,3) = compz
               end do
               exit ! Do this only once, as there are several jobs in
                    ! the Gaussian input !
            end if
         end do
         close(u)
      
      end do

   end select

   do ii = 1, nstat_tot
      allocate( eigvec(nstat_tot) )
      eigvec(:) = 0._dp
      ! Eigenvector of the current state
      eigvec = 0._dp
      do i = 1, nstat_tot
         eigvec(i) = ham(i,ii)
      end do
      dipx = 0._dp
      dipy = 0._dp
      dipz = 0._dp
      do k = 1, nstat_tot
         dipx = dipx + eigvec(k)*mu(k,1)
         dipy = dipy + eigvec(k)*mu(k,2)
         dipz = dipz + eigvec(k)*mu(k,3)
      end do
      trdip(ii,1) = dipx
      trdip(ii,2) = dipy
      trdip(ii,3) = dipz
      deallocate( eigvec )
   end do

   deallocate( mu ) 
end subroutine trandip

subroutine winf(nx_qm, conf)
!! It writes a file with a list which contains the locations
!! of the informations presented in the unformatted file which
!! is generated by the subroutine wdyn. This is one of the output
!! files that is used by "statdyn" program.
!! The program "statdyn" is used in Pisa for the dynamics analysis. 
   type(nx_qm_t) :: nx_qm
   !! Nx_Qm object.
   type(nx_config_t) :: conf
   !! General configuration.

   integer, parameter :: ngwmax=999999

   integer :: u
   integer :: i
   integer :: nintx
   integer :: n1
   integer :: nflo
   integer :: versinf
   integer :: norbs
   integer :: nmos
   integer :: ncf
   integer :: nstat_t
   integer :: inco
   integer :: iwrt
   integer :: atn
   integer :: isolv
   integer :: numatmm_act
   integer :: inicoord
   integer :: numfrz
   integer :: nlines
   integer :: io

   integer, allocatable, dimension(:) :: nat_p

   character(len=120) :: out_name

   character(len=200) :: string
   character(len=200) :: namefile
   character(len=2) :: dash

   character(len=10), allocatable, dimension(:) :: elemnt
   character(len=200), allocatable, dimension(:) :: mop_inp

   real(dp), parameter :: con_mass=1822.888515

   real(dp) :: atnumb
   real(dp) :: rbuff
   real(dp) :: mass

   real(dp), allocatable, dimension(:) :: atmau

   !Intrinsic Functions .. 
   intrinsic Index, Trim

   select case(nx_qm%qmcode)
   case("exc_mopac")
      out_name = "exc_mop"
   case("exc_gaussian")
      out_name = "exc_gau"
   case("mopac") 
      out_name = "mopac"
   end select

   allocate( elemnt(conf%nat) )
   elemnt(:) =  ""
   allocate( nat_p(conf%nat) )
   nat_p(:) = 0
   allocate( atmau(conf%nat) )
   atmau(:) = 0._dp

   if (nx_qm%qmcode .eq. "mopac") then
      ! --Checks the number of lines mopac input
      nlines = 0
      open(newunit=u, file='mopac.dat')
      do
         read(u,*,iostat=io)
         if (io /= 0) exit
         nlines = nlines + 1
      end do
      close(u)
      allocate( mop_inp(nlines) )
      mop_inp(:) = ""
      open(newunit=u, file='mopac.dat', status='old')
      rewind u
      do i = 1, nlines 
         read(u,"(a)") mop_inp(i)
      end do
      close(u)
   end if

   open(newunit=u, file='geom', status='old', form='formatted')
   rewind u
   do i = 1, conf%nat
      read(u,*) elemnt(i), atnumb, rbuff, rbuff, rbuff, mass
      atn = INT(atnumb)
      nat_p(i) = atn
      atmau(i) = mass*con_mass
   end do
   close(u)

   namefile = '../'//trim(conf%output_path)//'/'//trim(out_name)
   open(newunit=u, file=Trim(namefile)//'.inf', form='formatted', &
        & status='unknown')
   rewind u
   versinf = 160
   write(u,"(a,i10)") " Versione =",versinf
   !
   norbs = 1
   nmos = 1
   ncf = 1
   nstat_t = conf%nstat
   inco = 0
   iwrt = 0
   write (u,"(a)") "# numat, norbs, nmos, ncf, nstat_t, nstati, inco, dynwrt"
   write (u,"(8i9)") conf%nat, norbs, nmos, ncf, nstat_t, conf%nstat, inco, iwrt

   write (u,"(a)") "# atom n.  element    Z    atomic mass (a.u)"
   do i = 1, conf%nat
      write (u,10) i, elemnt(i), nat_p(i), atmau(i)
   end do
   !  numatmm_act = total number of MM atoms - frozen MM atoms
   !  only p and q's of numatm_act MM atoms are written to file .dyn
   numatmm_act = 0
   isolv = 0
   inicoord = 0
   numfrz = 0
   write (u,"(a)") "# isolv   numatmm_act   num_mol_solv     inicoord    numfrz"
   write (u,"(5i9)") isolv, numatmm_act, 1, inicoord, numfrz

   !inquire (u,NAME = fdyn)
   dash=" -"
   write (u,"(//80a)")("-",i=1,80)
   !        & "Information recorded in file ", fdyn(1:Index(fdyn," ")), &
   select case (nx_qm%qmcode)
   case ("exc_mopac")
      write (u,"(5x,a,/5x,a/)") &
           & "Information recorded in file exc_mopac.dyn", &
           & "(all data in a.u., unless otherwise specified)"
   case ("exc_gaussian")
      write (u,"(5x,a,/5x,a/)") &
           & "Information recorded in file exc_gaussian.dyn", &
           & "(all data in a.u., unless otherwise specified)"
   case ("mopac")
      write (u,"(5x,a,/5x,a/)") &
           & "Information recorded in file mopac.dyn", &
           & "(all data in a.u., unless otherwise specified)"
   end select
   write (u,"(a,I8,a/a/)") &
        & " Structure of the records:   (records with ngw within 1 and", &
        & ngwmax-1, ")", " ngw,nintx,nflo,(ibuf(i),i=1,nintx),(fbuf(i),i=1,nflo)"

   !! Integer variables (ibuf)
   write (u,"(a)") " ibuf contains:"
   nintx = 1
   write (u,10000) nintx, "nc = number of time steps"
   nintx = nintx + 1
   write (u,"(i9,2x,a/11x,a/11x,a/11x,a)") &
        & nintx, "ntest = 0 (no reaction)", &
        & "        1 (the internal coord. has reached the threshold W) ***disable***", &
        & "        2 (the internal coord. has crossed the threshold S)", &
        & "        3 (no reaction, total number of steps reached)"
   nintx = nintx + 1
   write (u,10000) nintx, "npath (not used)"
   nintx = nintx + 1
   write (u,10000) nintx, "istat = index of the adiabatic surface"
   nintx = nintx + 1
   write (u,"(i9,2x,a/11x,a/11x,a,/11x,a,/11x,a,/11x,a,/11x,a,/11x,a,/11x,a)") &
        & nintx, "irk   = 0 (no hopping)", "      = 1 (surface hopping)", &
        & "      = 2 (state switch)", &
        & "      = 3 (surface hopping following a state switch) ***disabled***", &
        & "      = 4 (insufficient Ecin for surface hopping)", &
        & "      = 5 (classical Franck-Condon excitation at step KVERT) ***disabled***", &
        & "      = 6 (failure in energy conservation - reset Ekin) ***disabled***", &
        & "      = -1 (radiative surface hopping -- no energy conservation)",&
        & "      = -2 (field decay surface hopping -- no energy conservation)"
   nintx = nintx + 1
   write (u,10000) nintx, "ntz = trajectory number"
   nintx = nintx + 1
   write (u,10000) nintx, "istat_old = istat at the previous timestep"
   nintx = nintx + 1
   write (u,10000) nintx, "trdip_from_state = state from which trans. dip. are evaluated"

   !! Real variables (fbuf)
   write (u,"(/a)") " fbuf contains:"
   nflo = 1
   write (u,10001) nflo, dash, nflo, "_TIME_     = time (in fsec)"
   nflo = nflo + 1
   write (u,10001) nflo, dash, nflo, "_EKIN_     = kinetic energy"
   nflo = nflo + 1
   write (u,10001) nflo, dash, nflo, "_EPOT_     = potential energy"
   nflo = nflo + 1
   write (u,10001) nflo, dash, nflo, "_ETOT_     = total energy"
   n1 = nflo + 1
   nflo = nflo + (conf%nat)*6
   write (u,10001) &
   & n1, dash, nflo, "_QP_       q(i),p(i),i=1,3n    = cartesian coords. and momenta"
   n1 = nflo + 1
   nflo = nflo + conf%nstat
   write (u,10001) n1, dash, nflo, "_ECI_      eci(i),i=1,nstat_t   = CI energies"
   if (nx_qm%qmcode .eq. 'mopac') then
      continue
   else
      n1 = nflo + 1
      nflo = nflo + ( (conf%nstat)*(conf%nstat) )
      write (u,10001) &
      & n1, dash, nflo, "_EXVEC_    tvec(i,j),i,j=1,nstat_t  = eigenvetors of the exciton Hamiltonian"
   end if
   n1 = nflo + 1
   nflo = nflo + conf%nstat
   write (u,10001) n1, dash, nflo, &
         & "_POP_      pop(i),i=1,nstat_t   = populations of the adiabatic states"
   n1 = nflo + 1
   nflo = nflo + 3*(conf%nstat)
   string = "_AMUT_     amut(i,j),j=1,3,i=1,nstat_t = transition dipole moments (DEBYE)"
   write (u,10001)  n1, dash, nflo,Trim(string)
   n1 = nflo + 1
   nflo = nflo + ( (conf%nstat)*(conf%nstat) )
   write (u,10001) &
   & n1, dash, nflo, "_TPAR_     ciovl(i,j),i,j=1,nstat_t  = diabatic transf. (this step)"
   if (nx_qm%qmcode .eq. 'mopac') then
      continue
   else
      n1 = nflo + 1
      nflo = nflo + (((conf%nstat*conf%nstat) - conf%nstat)/2 + conf%nstat)
      write (u,10001) &
      & n1, dash, nflo, "_EXHAM_    ham_tot(j,i),i=1,nstat_t; j=i,nstat_t = lower triangular matrix of the exciton Ham."
      n1 = nflo + 1
      nflo = nflo + conf%nstat
      write (u,10001) &
      & n1, dash, nflo, "_DIPOP_    diapop(i),i=1,nstat_t = populations of the diabatic states"
   end if
   n1 = nflo + 1
   nflo = nflo + conf%nstat
   write (u,10001) &
      & n1, dash, nflo, "_TRANS_    shprob(i),i=1,nstat_t = FS trans. rate (tprob/dt, a.u.), referred to the previous timestep"


   !! trj reattiva
   write (u,"(//a,I8,a)") &
        & " For reactive trjs. only (records with ngw=", ngwmax, ")"
   write (u,"(a)") " ibuf contains:"
   write (u,10000) 1, "npath = reactive channel"
   write (u,10000) 2, "nfrag = number of fragments (products)"
   write (u,"(a)") " fbuf contains:"
   write (u,"(a)") " for each fragment, Etrasl, <Erot>, <Evib>"
   write (u,"(80a)")("-", i = 1,80)
   if (nx_qm%qmcode .eq. "mopac") then
      write (u,*)
      write (u,"(/a)")"------- Mopac input -------"
      do i = 1, nlines
         write(u,"(a)") mop_inp(i)
      end do
      deallocate( mop_inp ) 
   end if

   close(u)

   deallocate( elemnt )
   deallocate( nat_p )
   deallocate( atmau )

   10  format(i6,6x,a2,2x,i6,2x,f20.10)
   10001 format (i9,a,i9,2x,a)
   10000 format (i9,2x,a)
   
end subroutine winf

subroutine wdyn(nx_qm, conf, traj, check_en_cons)
!! It writes all the important informations of the dynamics in a unformatted file.
!! This is the output format that is recognized by the statdyn program.
!! Statdyn is a program used in Pisa to perform SH dynamics analysis.
   type(nx_qm_t) :: nx_qm
   !! Nx_Qm object.
   type(nx_config_t) :: conf
   !! General configuration.
   type(nx_traj_t) :: traj
   !! Classical trajectory.

   integer :: i, j, k, m, l
   integer :: u
   integer :: nintx
   integer :: nflo
   integer :: maxstep
   integer :: check_en_cons
   integer :: ngw
   integer :: iseed
   integer :: idx
   integer :: idr
   integer :: ntest

   integer, allocatable, dimension(:) :: ibuf
  
   real(dp), parameter :: con_mass=1822.888515

   real(dp) :: ams
   real(dp) :: time_on_traj

   real(dp), allocatable, dimension(:) :: q
   real(dp), allocatable, dimension(:) :: v
   real(dp), allocatable, dimension(:) :: fbuf

   character(len=120) :: out_name
   character(len=200) :: namefile

   logical :: last_step

   nintx = 0
   nflo = 0

   ! Create the size of the ibuf and fbuf arrays
   idx = 8

   if (nx_qm%qmcode .eq. 'mopac') then
      idr = 4 + (conf%nat*3*2) + conf%nstat + (conf%nstat*conf%nstat) + &
            & conf%nstat + (conf%nstat*3) + conf%nstat 
   else
      idr = 4 + (conf%nat*3*2) + conf%nstat + 2*(conf%nstat*conf%nstat) + &
            & conf%nstat + (conf%nstat*3) + (((conf%nstat*conf%nstat) - &
            & conf%nstat)/2 + conf%nstat) + conf%nstat + conf%nstat
   end if

   allocate( ibuf(idx) )
   ibuf(:) = 0
   allocate( fbuf(idr) )
   fbuf(:) = 0._dp

   ! 1 - INT. Record the current step
   nintx = nintx + 1
   ibuf(nintx) = traj%step

   maxstep = conf%init_step + int(conf%tmax / conf%dt)

   ! 2 - INT. Record ntest = 0 (no reaction);
   !                       = 2 (satisfies the stop cond);
   !                       = 3 (total number of steps reached).
   nintx = nintx + 1

   if (traj%old_nstatdyn .ne. traj%nstatdyn) then
      time_on_traj = 0.0_dp
   else
      time_on_traj = traj%time_on_traj + traj%dt
   end if
   
   last_step = .false.
   if ((traj%nstatdyn .eq. conf%killstat) &
        & .and. (conf%timekill .ne. 0)) then
      if (time_on_traj .ge. conf%timekill) then
         last_step = .true.
      end if
   end if

   if (traj%step .eq. maxstep) then
      ibuf(nintx) = 3
      ntest = 3
   else if (last_step .eqv. .true.) then
      ibuf(nintx) = 2
      ntest = 2
   else
      ibuf(nintx) = 0
      ntest = 0
   end if

   ! 3 - INT. Record npath
   nintx = nintx + 1
   ibuf(nintx) = 0

   ! 4 - INT. Record current state (index of the adiabatic surface)
   nintx = nintx + 1
   ibuf(nintx) = traj%nstatdyn
   
   ! 5 - INT. Record irk = 0 (no hopping)
   !                      = 1 (surface hopping)
   !                      = -1 ( no energy conservation)
   nintx = nintx + 1
   if (traj%nstatdyn .ne. traj%old_nstatdyn) then
      ibuf(nintx) = 1
   else if (check_en_cons .eq. 2) then
      ibuf(nintx) = -1
   else 
      ibuf(nintx) = 0
   end if
   
   ! 6 - INT. Record the traj number. However, this approach has only one trajectory 
   ! per input, so the number of the trajectory is always 1.
   nintx = nintx + 1
   ibuf(nintx) = 1

   ! 7 - INT. Record istat at the previous timestep.
   nintx = nintx + 1
   ibuf(nintx) = traj%old_nstatdyn

   ! 8 - INT. Record trdip_from_state. I set = 1 because the program
   ! always calculate the trdip for all the states.
   nintx = nintx + 1
   ibuf(nintx) = 1

   ! 1 - RE. Record the time
   nflo = nflo + 1
   fbuf(nflo) = traj%t

   ! 2 - RE. Kinetic energy
   nflo = nflo + 1
   fbuf(nflo) = traj%ekin

   ! 3 - RE. Potential energy of the active state
   nflo = nflo + 1
   fbuf(nflo) = traj%epot(traj%nstatdyn)

   ! 4 - RE. Total energy
   nflo = nflo + 1
   fbuf(nflo) = traj%etot

   ! 5 - RE. Record the cartesian coords. and momenta
   allocate( q(conf%nat*3) )
   q(:) = 0._dp
   k = 0
   do i = 1, conf%nat
      do j = 1, 3
         k = k + 1
         q(k) = traj%geom(j,i) 
      end do
   end do

   allocate( v(conf%nat*3) )
   v(:) = 0._dp
   k = 0
   do i = 1, conf%nat
      do j = 1, 3
         k = k + 1
         v(k) = traj%veloc(j,i) 
      end do
   end do

   j = 1
   do l = 1, conf%nat
      m = j + 2
      ams = traj%masses(l)*con_mass
      do i = j, m
         nflo = nflo + 1
         fbuf(nflo) = q(i)
         nflo = nflo + 1
         fbuf(nflo) = v(i) * ams
      end do
      j = j + 3
   end do

   deallocate( q )
   deallocate( v )

   ! 6 - RE. Record the CI energies
   do i = 1, conf%nstat
      nflo = nflo + 1
      fbuf(nflo) = traj%epot(i)
   end do

   if (nx_qm%qmcode .eq. 'mopac') then
      continue
   else
      ! 7 - RE. Record the diabatic2adiabatic transf. matrix
      do i = 1, conf%nstat
         do j = 1, conf%nstat
            nflo = nflo + 1
            fbuf(nflo) = nx_qm%eigenvectorsexc(j,i)
         end do
      end do
   end if

   ! 7 - RE. Record the population of the adiabatic states
   do i = 1, conf%nstat
      nflo = nflo + 1
      fbuf(nflo) = traj%population(i)
   end do

   ! 8 - RE. Record the transition dipole moments (Debye)
   ! --record the ground state
   do i = 1, 3
      nflo = nflo + 1
      fbuf(nflo) = 0._dp
   end do
   ! --record the trans. to excited states : <S0|mu|Si>
   do i = 1, (conf%nstat-1)
      do j = 1, 3
         nflo = nflo + 1
         fbuf(nflo) = nx_qm%trdipexc(i,j)
      end do
   end do

   ! 9 - RE. Record overlap matrix in adiabatic basis
   do i = 1, conf%nstat
      do j = 1, conf%nstat
         nflo = nflo + 1
         fbuf(nflo) = traj%cio(j,i)
      end do
   end do

   if (nx_qm%qmcode .eq. 'mopac') then
      continue
   else
      ! 10 - RE. Record a triangular matrix of the exciton 
      ! hamiltonian
      do i = 1, conf%nstat
         do j = 1, i
            nflo = nflo + 1
            fbuf(nflo) = nx_qm%diaham(i,j)
         end do
      end do
   
      ! 11 - RE. Record the populations of the diabatic states
      do i = 1, conf%nstat
         nflo = nflo + 1
         fbuf(nflo) = nx_qm%diapop(i)
      end do
   end if

   ! 12 - RE. FS trans. rate (tprob/dt, a.u.)
   do i = 1, conf%nstat
      nflo = nflo + 1
      fbuf(nflo) = traj%shprob(1, i)
   end do

   iseed = 1

   select case (nx_qm%qmcode)
   case("exc_mopac")
      out_name = 'exc_mop'
   case("exc_gaussian")
      out_name = 'exc_gau'
   case("mopac")
      out_name = 'mopac'
   end select

   ngw = traj%step + 1 !CHECK

   namefile = '../'//trim(conf%output_path)//'/'//trim(out_name)//'.dyn'
   open(newunit=u, file=namefile, status='unknown', form='unformatted', &
        & position='append')
   write(u) ngw, nintx, nflo, (ibuf(i), i = 1,nintx), (fbuf(i), i = 1,nflo)
   if (ntest .ne. 0) then
      write(u) iseed, -2, 0
   end if
   close(u)

   deallocate(ibuf)
   deallocate(fbuf)

end subroutine wdyn

end module mod_exash

