module mod_tinker_mndo
  !! author: Bondanza Mattia <mattia.bondanza@phd.unipi.it>
  !! date: February 2021
  !!
  !! This modules contains all routines and functions related to
  !! running electronic structure computations with MNDO
  !! performed with Tinker/MNDO interface.
  !! It uses the ``nx_qm_t`` type described in ``mod_qm_t``.
  !! The module exports function necessary to setup, print, update
  !! the input for, run and read information from, the Tinker/MNDO
  !! job.  All the exported functions are meant to
  !! be used by the general handler ``mod_qm_general``.
  !!
  !! All functions and routines defined in this module should be
  !! prefixed with ``tmndo_``.
  !!
  !! LIMITATIONS:
  !! - It is completely experimental!
  !!
  use mod_kinds, only: dp
  use mod_constants, only: &
       & MAX_STR_SIZE, MAX_CMD_SIZE, au2ang, au2kcalm
  use mod_logger, only: &
       & nx_log, &
       & LOG_WARN, LOG_ERROR, &
       & call_external, print_conf_ele, check_error
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_configuration, only: nx_config_t
  use mod_trajectory, only: nx_traj_t
  use mod_tools, only: to_str, split_pattern
  use mod_interface, only: &
       & copy, rm
  use mod_input_parser, only: &
       & parser_t, set => set_config, set_config_realloc
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: tmndo_setup, tmndo_print
  public :: tmndo_update_input
  public :: tmndo_run
  public :: tmndo_read
  public :: tmndo_set_qmmm_atoms

  ! Interface for setting environment variables
  interface
     subroutine setenv(name, value, overwrite) bind(C, name='setenv')
       use iso_c_binding
       implicit none
       character(kind=C_char), intent(in) :: name(*)
       integer(C_int), intent(in) :: value
       integer(C_int), value :: overwrite
     end subroutine  setenv
  end interface

  integer, parameter :: MAX_CONTRIB = 6
  !! Maximum number of dominant contributions printed (DFT)

  ! Termination codes
  integer, parameter :: TMNDO_ERR_PREP = 201
  integer, parameter :: TMNDO_ERR_MAIN = 202

contains

  subroutine tmndo_setup(nx_qm, parser)
    !! Setup Tinker-MNDO.
    !!
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm_t`` object to setup.
    type(parser_t), intent(in) :: parser

    call set_config_realloc(parser, 'tinker_mndo', nx_qm%tmndo_interface, 'interface_path')
    call set(parser, 'tinker_mndo', nx_qm%compute_nac_mm , 'nac_mm')
  end subroutine tmndo_setup


  subroutine tmndo_print(nx_qm, out)
    !! Print information related to Tinker-g16-mmp execution.
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM object to print.
    integer, intent(in), optional :: out

    integer :: output
    character(len=MAX_STR_SIZE) :: env

    output = stdout
    if (present(out)) output = out

    call get_environment_variable("TINKER_MNDO", env)
    call print_conf_ele(trim(env), 'TINKER_MNDO path', unit=output)
    call get_environment_variable("MNDO", env)
    call print_conf_ele(trim(env), 'MNDO path', unit=output)
    call print_conf_ele(nx_qm%tmndo_interface, 'MNDO interface', unit=output)
  end subroutine tmndo_print


  subroutine tmndo_update_input(nx_qm, traj, conf)
    !! Update the tinker-mndo input.
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf

    integer :: nstatdyn
    integer :: nstat
    integer :: step, init_step
    integer :: lvprt

    character(len=MAX_STR_SIZE) :: env

    character(len=256), parameter :: delete_list(7) = [&
         & 'tinker_mndo.xyz  ', 'tmp_mndo.inp     ', &
         & 'tmp_mndo.out     ', 'interface.dat    ', &
         & 'fort.15          ', 'tinker.out       ', &
         & 'interface_nac.dat'&
         & ]

    logical :: ext
    character(len=MAX_STR_SIZE), allocatable :: loaded(:)
    integer :: ierr

    nstatdyn = traj%nstatdyn
    nstat = conf%nstat
    step = traj%step
    init_step = conf%init_step
    lvprt = conf%lvprt

    call get_environment_variable("NXHOME", env)
    env = trim(env)//'/utils/'

    if(nx_qm%compute_nac /= 1 .and. nstat > 1) then
      write(*, *) "Tinker-MNDO only supports QM handled NAC"
      STOP
    end if

    ! Clean up if it is the second run
    if (step > init_step) then
       ierr = rm(delete_list)
       call check_error(ierr, TMNDO_ERR_PREP, 'T/MNDO: Cannot clean work dir')
       inquire(file='fort.11', exist=ext)
       if (ext) then
          ierr = copy('fort.11', 'old_fort.11')
          call check_error(ierr, TMNDO_ERR_PREP, &
               & 'T/MNDO: Cannot backup fort.11')
       end if
    else
       block
         character(len=256) :: copy_list(2)
         character(len=256), parameter :: copy_as(2) = [&
              & 'tinker_mndo.key',&
              & 'template.inp   '&
              & ]

         copy_list= [&
              & trim(nx_qm%job_folder)//'/frame00000.key',&
              & trim(nx_qm%job_folder)//'/template.inp  '&
              & ]

         ierr = copy(copy_list, copy_as)
         call check_error(ierr, TMNDO_ERR_PREP, &
              & 'T/MNDO: Cannot copy input files')
       end block
    end if

    ! Update files (?)
    loaded = tmndo_load_key_file('tinker_mndo.key')
    loaded = tmndo_delete_keys(loaded, ['mndocurrentstate', 'mndostates      '])
    block
      character(len=256), allocatable :: keys(:)
      character(len=256), allocatable :: values(:)

      if (nstat > 1) then
         allocate(keys(2))
         allocate(values(2))

         keys(1) = 'mndocurrentstate'
         values(1) = to_str(traj%nstatdyn)
         keys(2) = 'mndostates'
         values(2) = to_str(conf%nstat)
      else
         allocate(keys(1))
         allocate(values(1))
         keys(1) = 'mndocurrentstate'
         values(1) = to_str(traj%nstatdyn)
      end if

      loaded = tmndo_add_key(loaded, keys, values)
    end block
    call tmndo_print_key_file(loaded, 'tinker_mndo.key')

    call tmndo_write_coords(&
         & trim(nx_qm%job_folder)//'/frame00000.xyz', 'tinker_mndo.xyz', &
         & au2ang * traj%geom)
  end subroutine tmndo_update_input


  subroutine tmndo_run(nx_qm)
    !! Execute Tinker-MNDO.
    !!
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.

    character(len=MAX_CMD_SIZE) :: command, err_str
    integer :: ierr, imaperr
    logical :: int_ok

    write(command, '(A)') trim(nx_qm%tmndo_interface)//&
    &' '//'tinker_mndo.xyz'
    call call_external(command, ierr, outfile='tinker.out', errfile='stdout')
    ! Check the termination status of Tinker/MNDO
    !! call check_error(ierr, 1, "Tinker/MNDO exits with errors!")
    ! Check if interface.dat exists
    inquire(file='interface.dat', exist=int_ok)
    if(.not. int_ok) then
      err_str = "ERROR: ACTIVE ORBITAL MAPPING FAILED"
      write(command, *) "grep '", trim(err_str), "' tmp_mndo.out > /dev/null 2>&1"
      call execute_command_line(trim(command), exitstat=imaperr)

      if(imaperr == 0) then
         call nx_log%log(LOG_ERROR, "Error in mapping orbitals!")
      else
        call nx_log%log(LOG_ERROR, "Unknown exception during Tinker/MNDO execution!")
        ERROR STOP
      end if

    end if
  end subroutine tmndo_run


  subroutine tmndo_read(nx_qm, conf, traj, qminfo, ierr)
    !! Read tinker-MNDO outputs and report.
    !!
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    type(nx_config_t) :: conf
    !! Main NX configuration.
    type(nx_traj_t) :: traj
    !! Main NX trajectory.
    type(nx_qminfo_t), intent(inout) :: qminfo
    integer, intent(inout) :: ierr
    !! Status.

    real(dp), dimension(:, :), allocatable :: st_components
    real(dp), dimension(:, :), allocatable :: components
    real(dp), dimension(:), allocatable :: osc_str
    ! Temp. array for reading transition properties

    character(len=MAX_STR_SIZE) :: line, err_str, command, msg
    integer :: imaperr, scferr
    integer :: i, u, j, nat
    logical :: int_ok
    logical :: print_osc

    integer :: sec_id, tmndo_natom_grd, tmndo_nstates, tmndo_istate, &
               tmndo_st_a, tmndo_st_b, nc, &
               nstat, ncoupl, iat


    character(len=MAX_STR_SIZE), parameter :: &
    & NAD_H = 'CARTESIAN INTERSTATE COUPLING GRADIENT FOR STATES'

    nat = conf%nat
    nstat = size(qminfo%repot)
    ncoupl = nstat*(nstat - 1) / 2

    ! First, read energies and gradient from the tinker interface file
    ! rgrad(:, :, :) = 0.0_dp
    ! repot(:) = 0.0_dp
    ! if(nx_qm%compute_nac == 1) rnad(:, :, :) = 0.0_dp

    inquire(file='interface.dat', exist=int_ok)

    if(.not. int_ok) then
      err_str = "ERROR: ACTIVE ORBITAL MAPPING FAILED"
      write(command, *) "grep '", trim(err_str), "' tmp_mndo.out > /dev/null 2>&1"
      call execute_command_line(trim(command), exitstat=imaperr)

      err_str = "SCF CONVERGENCE FAILURE"
      write(command, *) "grep '", trim(err_str), "' fort.15 > /dev/null 2>&1"
      call execute_command_line(trim(command), exitstat=scferr)

      if(imaperr == 0 .or. scferr == 0) then
        call nx_log%log(LOG_WARN, "Trying an adaptive step procedure!")
        ! revert the orbital guess contained in fort.11
        write(command, *) "mv old_fort.11 fort.11"
        call execute_command_line(trim(command), exitstat=ierr)

        nx_qm%request_adapt_dt = .true.
        return
      else
        call nx_log%log(LOG_ERROR, "Interface file (interface.dat) not found!")
        ERROR STOP
      end if

    end if

    open(newunit=u, file='interface.dat', status='old', action='read')

    sec_id = 0
    do
      read(u, '(a)', iostat=ierr) line
      if (ierr /= 0) exit
      ! POTENTIAL section
      if( sec_id == 0 .and. line(1:10) == 'POTENTIAL ') then
        read(line(10:), '(i10)') tmndo_nstates
        sec_id = 1
        i = 1
      else if( sec_id == 0 .and. line(1:9) == 'GRADIENT ') then
        read(line(9:), "(2i10)") tmndo_istate, tmndo_natom_grd
        sec_id = 2
        i = 1
      else if(sec_id == 1) then ! POTENTIAL
        read(line, '(f20.8)') qminfo%repot(i)
        if( i .eq. tmndo_nstates) then
          sec_id = 0
        else
          i = i + 1
        end if
      else if(sec_id == 2) then ! GRADIENT
        read(line, '(3f20.8)') qminfo%rgrad(tmndo_istate,:,i)
        if(i == tmndo_natom_grd) then
          sec_id = 0
        else
          i = i + 1
        end if
      else
        write(*, *) 'The following line of the interface file cannot be', &
                   &' understood.'
        write(*, *) trim(line)
        STOP
      end if
    end do
    close(u)

    ! Convert potential in atomic units
    qminfo%repot = qminfo%repot / au2kcalm
    ! Convert gradients in atomic units
    qminfo%rgrad = qminfo%rgrad * au2ang / au2kcalm

    !Then read NAC from fort.15 interface file
    if(nx_qm%compute_nac == 1) then
      open(newunit=u, file='interface_nac.dat', status='old', action='read')

      sec_id = 0
      do
        read(u, '(a)', iostat=ierr) line
        if (ierr /= 0) exit

        if( sec_id == 0 .and. &
     &      line(:len(trim(NAD_H))) == NAD_H) then
          read(line(len(trim(NAD_H))+1:), *) tmndo_st_a, tmndo_st_b
          sec_id = 1
          i = 1
          nc = (tmndo_st_a - 2)*(tmndo_st_a - 1) / 2 + tmndo_st_b
          !write(*, '(a,3i3)') "Reading interstate coupling ", tmndo_st_a, &
          !& tmndo_st_b, nc
        else if( sec_id == 1 ) then
          if(len(trim(line)) == 0) then
            sec_id = 0
          else
            if( i > nat ) then
              call nx_log%log(LOG_ERROR, &
                   & "Error while reading NAC: there are too many atoms!")
              error stop
            end if

            read(line, "(I5, 3F20.10)") iat, qminfo%rnad(nc, :, i)

            if(iat /= i) then
              call nx_log%log(LOG_ERROR, "Problems while reading NAC: index mismatch!")
              error stop
            end if

            i = i + 1
          end if
        end if
      end do
      close(u)

      qminfo%rnad = qminfo%rnad * au2ang / au2kcalm
      ! NX wants non-adiabatic couplings (d_{ij}), but MNDO computes
      ! interstate coupling vectors (h_{ij}): according to eq. 30
      ! of 10.1016/j.chemphys.2008.01.044 d_{ij} is computed dividing
      ! h_{ij} by \DeltaE_{ij}.

      do tmndo_st_a=2, tmndo_nstates
        do tmndo_st_b=1, tmndo_st_a-1
          nc = (tmndo_st_a - 2)*(tmndo_st_a - 1) / 2 + tmndo_st_b
          ! write(*, *) "CORRECTING", tmndo_st_a, tmndo_st_b, nc
          qminfo%rnad(nc,:,:) = qminfo%rnad(nc,:,:) / (qminfo%repot(tmndo_st_a) - qminfo%repot(tmndo_st_b))
        end do
      end do

      if(nx_qm%compute_nac_mm == 0) then
        do i=1, nat
          if( .not. traj%is_qm_atom(i) ) then
            qminfo%rnad(:,:,i) = 0.0_dp
          end if
        end do
      end if
    end if

    allocate(st_components(3, nstat))
    st_components(:, :) = 0.0_dp
    allocate(components(3, nstat-1))
    components(:, :) = 0.0_dp
    ! Osc str
    allocate(osc_str(nstat-1))
    osc_str(:) = 0.0_dp
    print_osc = .false.

    if (tmndo_check_trdip()) then
      print_osc = .true.
      call tmndo_read_osc_str(st_components, components, osc_str, nstat)
    end if

    if (print_osc) then
       ! Transition properties from GS
       call qminfo%append_data('Oscillator strengths and transition moments (A.U.)')

       write(msg, '(5a15)') 'Excitation', 'Osc. str.', 'dx', 'dy', 'dz'
       call qminfo%append_data(msg)

       write(msg, '(a)') repeat(' --------------', 5)
       call qminfo%append_data(msg)

       do i=1, nstat-1
          write(msg, '("      1 ->", i3, 4F15.6)') &
               & i+1, osc_str(i), (components(j, i), j=1, 3)
          call qminfo%append_data(msg)
       end do
       ! State Dipoles
       msg = ''//NEW_LINE('a')
       msg = trim(msg)//'State Dipoles (A.U.)'
       msg = trim(msg)//NEW_LINE('a')
       call qminfo%append_data(msg)

       write(msg, '(4a15)') 'State', 'dx', 'dy', 'dz'
       call qminfo%append_data(msg)

       write(msg, '(a)') repeat(' --------------', 4)
       call qminfo%append_data(msg)

       do i=1, nstat
          write(msg, '(i15, 3F15.6)') &
               & i, (st_components(j, i), j=1, 3)
          call qminfo%append_data(msg)
       end do
    end if

    ! Now we transfer the oscillator strengths to nx_qm object
    nx_qm%osc_str(:) = osc_str(:)

    deallocate(components)
    deallocate(osc_str)

  end subroutine tmndo_read

  function tmndo_check_trdip()
    logical :: tmndo_check_trdip
    !! Return value
    character(len=MAX_STR_SIZE), parameter :: &
      & mndooutfile = "tmp_mndo.out", &
      & dipole_str = "Properties of transitions   1 -> #:"
    ! character(len=MAX_CMD_SIZE) :: command
    character(len=MAX_CMD_SIZE) :: buf

    logical :: exi
    integer :: ierr, id, u

    tmndo_check_trdip = .false.
    inquire(file=mndooutfile, exist=exi)
    if(exi) then
      !! This is not elegant, should probably be improved
      ! write(command, *) "grep '", trim(dipole_str), "' ", &
      !   & trim(mndooutfile)," > /dev/null 2>&1"
      ! call execute_command_line(trim(command), exitstat=ierr)
      ! if(ierr == 0) then
      !   tmndo_check_trdip = .true.
       ! end if

       open(newunit=u, file=mndooutfile, action='read')
       do
          read(u, '(A)', iostat=ierr) buf
          if (ierr /= 0) exit
          id = index(buf, dipole_str)
          if (id /= 0) tmndo_check_trdip = .true.
       end do
       close(u)
    end if
  end function

  subroutine tmndo_read_osc_str(st_components, components, osc_str, nstat)
    character(len=MAX_STR_SIZE), parameter :: &
      & mndooutfile = "tmp_mndo.out", &
      & HEADER_OSCSTR = " Properties of transitions   1 -> #:", &
      & HEADER_TRNDIP = " Dipole-length electric dipole transition moments   1 -> #:", &
      & HEADER_STDIP = " State dipole moments:"

    integer, intent(in) :: nstat
    real(dp), dimension(3,nstat), intent(out) :: st_components
    real(dp), dimension(3,nstat-1), intent(out) :: components
    real(dp), dimension(:), intent(out) :: osc_str

    character(len=MAX_STR_SIZE) :: line
    integer :: u, sec_id, ierr, i


    open(newunit=u, file=mndooutfile, status='old', action='read')

    i = 0
    sec_id = 0
    do
      read(u, '(a)', iostat=ierr) line
      if (ierr /= 0) exit
      if( sec_id == 0 .and. line(1:36) == HEADER_OSCSTR ) then
        sec_id = 1

        !! Skip two lines
        do i = 1, 2
          read(u, '(a)', iostat=ierr) line
          if(ierr /= 0) then
             call nx_log%log(LOG_ERROR, &
                  & "Unexpected error while reading oscillator strenghts.")

             error STOP
          end if
        end do

        i = 1
      else if( sec_id == 0 .and. line(1:59) == HEADER_TRNDIP ) then
        sec_id = 2

        !! Skip two lines
        do i = 1, 2
          read(u, '(a)', iostat=ierr) line
          if(ierr /= 0) then
            call nx_log%log(LOG_ERROR, &
                 & "Unexpected error while reading transition dipoles.")
            error stop
          end if
        end do

        i = 1
      else if( sec_id == 0 .and. line(1:22) == HEADER_STDIP ) then
        sec_id = 3

        !! Skip two lines
        do i = 1, 2
          read(u, '(a)', iostat=ierr) line
          if(ierr /= 0) then
             call nx_log%log(LOG_ERROR, "Unexpected error while reading state dipoles.")
             error stop
          end if
        end do

        i = 1
      else if(sec_id == 1) then ! Oscillator strength
        if( trim(line) == "" ) then
          sec_id = 0
        else
          if(i >= nstat) then
            call nx_log%log(LOG_ERROR, &
                 & "Unexpected error while reading oscillator strenghts.")
            error stop
          end if
          read(line(52:62), '(f10.6)') osc_str(i)
          i = i + 1
        end if
      else if(sec_id == 2) then ! Transition dipoles
        if( trim(line) == "" ) then
          sec_id = 0
        else
          if(i >= nstat) then
            call nx_log%log(LOG_ERROR, &
                 & "Unexpected error while reading transition dipoles.")
            error stop
          end if
          read(line(41:76), '(3f12.6)') components(1,i), &
            & components(2,i), components(3, i)
          i = i + 1
        end if
      else if(sec_id == 3) then ! State dipoles
        if( trim(line) == "" ) then
          sec_id = 0
        else
          if(i > nstat) then
            call nx_log%log(LOG_ERROR, &
                 &  "Unexpected error while reading state dipoles.")
            error stop
          end if
          read(line(41:76), '(3f12.6)') st_components(1,i), &
            & st_components(2,i), st_components(3, i)
          i = i + 1
        end if
      end if
    end do
    close(u)

    ! Convert dipoles in D to A.U.
    components(:,:) = components(:,:) * 0.3935
    st_components(:,:) = st_components(:,:) * 0.3935

  end subroutine

  subroutine tmndo_set_qmmm_atoms(nx_qm, is_qm_atom, nat)
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    integer, intent(in) :: nat
    logical, intent(out) :: is_qm_atom(nat)
    integer, allocatable :: qmat(:)
    integer :: i, j
    integer :: u, ierr
    character(len=MAX_STR_SIZE) :: line, filename

    allocate( qmat(nat) )
    qmat = 0
    i = 0

    filename = trim(nx_qm%job_folder)//'/frame00000.key'
    open(newunit=u, file=filename, &
       & status='old', action='read')

    do
      read(u, '(a)', iostat=ierr) line
      if (ierr /= 0) exit
      line = adjustl(line)
      if(line(1:8) == 'qmatoms ') then
        read(line(8:), *, iostat=ierr) (qmat(j), j=i+1,nat)

        do while( qmat(i+1) > 0 )
          i = i + 1
          if( i == nat ) exit
        end do
      end if
    end do


    is_qm_atom = .false.
    do j=1, i
      is_qm_atom(qmat(j)) = .true.
    end do

  end subroutine tmndo_set_qmmm_atoms


  function tmndo_load_key_file(keyfile) result(res)
    character(len=*) :: keyfile

    character(len=MAX_STR_SIZE), allocatable :: res(:)

    integer :: u, ierr
    integer :: nlines
    character(len=MAX_STR_SIZE) :: buf

    nlines = 0
    open(newunit=u, file=keyfile, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       nlines = nlines + 1
    end do

    allocate(res(nlines))
    res(:) = ''
    rewind(u)

    nlines = 0
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       nlines = nlines + 1
       res(nlines) = trim(buf)

       ! print *, 'BUF ', nlines, ': ', trim(res(nlines))
    end do
    close(u)

    ! do nlines=1, size(res)
    !    print *, 'I ', nlines, ': ', trim(res(nlines))
    ! end do
    ! error stop
  end function tmndo_load_key_file


  function tmndo_delete_keys(loaded, keys) result(res)
    character(len=*) :: loaded(:)
    character(len=*) :: keys(:)

    character(len=MAX_STR_SIZE), allocatable :: res(:)

    character(len=MAX_STR_SIZE) :: tmp(size(loaded))
    integer :: i, j, deleted
    integer :: delete_this(size(keys))

    delete_this(:) = 0

    do i=1, size(loaded)
       tmp(i) = trim(loaded(i))
       do j=1, size(keys)
          if (index(loaded(i), trim(keys(j))) /= 0) then
             tmp(i) = '$$$DELETED$$$'
             delete_this(j) = 1
          end if
       end do
    end do

    deleted = sum(delete_this)
    print *, delete_this
    print *, 'DELETED =  ', deleted

    allocate(res(size(loaded) - deleted))
    res(:) = ''
    j = 1
    do i=1, size(tmp)
       if (tmp(i) == '$$$DELETED$$$') then
          continue
       else
          res(j) = trim(tmp(i))
          j = j + 1
       end if
    end do
  end function tmndo_delete_keys


  function tmndo_add_key(loaded, keys, values) result(res)
    character(len=*) :: loaded(:)
    character(len=*) :: keys(:)
    character(len=*) :: values(:)

    character(len=MAX_STR_SIZE), allocatable :: res(:)

    integer :: add_this(size(keys))
    integer :: i, j, added

    add_this(:) = 0

    do i=1, size(loaded)
       do j=1, size(keys)
          if (index(loaded(i), trim(keys(j))) == 0) then
             add_this(j) = 1
          end if
       end do
    end do

    added = sum(add_this)

    allocate(res(size(loaded) + added))
    res(:) = ''
    do i=1, size(loaded)
       res(i) = trim(loaded(i))
    end do

    if (added /= 0) then
       j = 1
       do i=1, size(keys)
          if (add_this(i) > 0) then
             res(size(loaded)+j) = trim(keys(i))//' '//trim(values(i))
             j = j + 1
          end if
       end do
    end if
  end function tmndo_add_key


  subroutine tmndo_print_key_file(loaded, filename)
    character(len=*), intent(in) :: loaded(:)
    character(len=*), intent(in) :: filename

    integer :: u, i

    open(newunit=u, file=filename, action='write')
    do i=1, size(loaded)
       write(u, '(a)') trim(loaded(i))
    end do
    close(u)
  end subroutine tmndo_print_key_file


  subroutine tmndo_write_coords(origin, newfile, geometry)
    character(len=*), intent(in) :: origin
    !! Original coordinate file.
    character(len=*), intent(in) :: newfile
    !! New coordinate file.
    real(dp), intent(in) :: geometry(:, :)
    !! Geometry in Angstrom

    integer :: u, v, ierr, at, i
    character(len=MAX_STR_SIZE) :: buf, tmp
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    integer :: grpsize

    open(newunit=u, file=origin, action='read')
    open(newunit=v, file=newfile, action='write')

    read(u, '(a)') buf
    write(v, '(a)') trim(buf)

    at = 1
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       group = split_pattern(buf)
       grpsize = size(group)
       tmp = ''
       if (grpsize >= 6) then
          do i=6, grpsize
             tmp = trim(tmp)//' '//trim(group(i))
          end do
       end if

       write(v, '(A6,A,A4,A,3F13.6,A)') &
            & trim(group(1)), ' ', adjustl(group(2)), ' ', &
            & geometry(1, at), geometry(2, at), geometry(3, at), &
            & ' '//trim(tmp)
       at = at + 1
    end do

    close(v)
    close(u)
  end subroutine tmndo_write_coords

end module mod_tinker_mndo
