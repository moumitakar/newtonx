module mod_exc_gaussian
  use mod_async, only: run_async
  use mod_kinds, only: dp
    use mod_constants, only: &
         & MAX_STR_SIZE, MAX_CMD_SIZE, au2ang
    use mod_logger, only: &
         & call_external, check_error
    use mod_qm_t, only: nx_qm_t
    use mod_qminfo, only: nx_qminfo_t
    use mod_exash, only: &
         & search2, start_prog, exc_read_input
    use mod_trajectory, only: nx_traj_t
    use mod_configuration, only: nx_config_t
    use mod_gaussian, only: gau_read_orb
    use mod_tools, only: to_str
    use mod_interface, only: &
         & get_list_of_files, copy
    use mod_input_parser, only: &
         & parser_t, set => set_config
    implicit none

    private

    public :: excgau_setup
    public :: excgau_update_input
    public :: excgau_run
    public :: excgau_read_outputs
    public :: inp_exc_gau

    ! Termination codes
    integer, parameter :: EXCGAU_ERR_PERL = 452
    integer, parameter :: EXCGAU_ERR_PERL2 = 453

  contains

  subroutine excgau_setup(nx_qm, conf, parser)
    type(nx_qm_t), intent(inout) :: nx_qm
    !! QM object.
    type(nx_config_t), intent(inout) :: conf
    !! General configuration.
    type(parser_t), intent(in) :: parser

    integer :: nchrom
    !! Number of chromophores
    integer :: nproc
    !! Number of cpu to split the QM/MM calculations
    integer :: nstattd
    ! Number of excited states in TDDFT calculations
    integer :: nproc_gau
    ! Number of cpu for gaussian calculations (double paralalization)
    integer :: verbose
    !! Genereate additional inputs EXASH program
    character(len=200) :: functional
    ! The functional, for example, B3LYP...
    character(len=200) :: basis
    ! Basis set
    character(len=200) :: force_field
    ! Force field (UFF or Amber or Dreiding)

    integer :: iseed, u

    character(len=200) :: namefile
    character(len=MAX_CMD_SIZE) :: env

    namelist /exc_gaussian/ nchrom, nproc, nstattd, nproc_gau, functional, &
         & basis, force_field, verbose

    call set(parser, 'exc_gaussian', nx_qm%nchromexc, 'nchrom')
    call set(parser, 'exc_gaussian', nx_qm%nprocexc, 'nproc')
    call set(parser, 'exc_gaussian', nx_qm%nstattdexc, 'nstattd')
    call set(parser, 'exc_gaussian', nx_qm%nproc_gauexc, 'nproc_gau')
    call set(parser, 'exc_gaussian', nx_qm%functionalexc, 'functional')
    call set(parser, 'exc_gaussian', nx_qm%basisexc, 'basis')
    call set(parser, 'exc_gaussian', nx_qm%force_fieldexc, 'force_field')
    call set(parser, 'exc_gaussian', nx_qm%verboseexc, 'verbose')

    if (nx_qm%verboseexc == 2 .or. nx_qm%verboseexc == 3) then
       ! BD 2022-09-20: Transfered to mod_io (io_txt_init_files)
       
       ! iseed = 1 !CHECK
       ! namefile = '../'//trim(conf%output_path)//'/exc_gau.dyn'
       ! open(newunit=u, file=namefile, status='new', &
       !      & form='unformatted', action='write')
       ! write(u) iseed, -1, 0
       ! write(u) 0, 0, 0, 1, 1
       ! close(u)
    end if

    deallocate(nx_qm%diaham, nx_qm%diapop, nx_qm%diaen, &
         & nx_qm%nstatexc, nx_qm%nat_arrayexc,&
         & nx_qm%eigenvectorsexc, nx_qm%chrom_enexc, &
         & nx_qm%trdipexc)

    allocate(nx_qm% diaham(conf%nstat, conf%nstat))
    nx_qm%diaham(:,:) = 0.0_dp

    allocate(nx_qm% diapop(conf%nstat))
    nx_qm%diapop(:) = 0.0_dp

    allocate(nx_qm% diaen(conf%nstat))
    nx_qm%diaen(:) = 0.0_dp

    allocate(nx_qm% nstatexc(nx_qm%nchromexc))
    nx_qm%nstatexc(:) = 0

    allocate(nx_qm% nat_arrayexc(nx_qm%nchromexc))
    nx_qm%nat_arrayexc(:) = 0

    allocate(nx_qm% eigenvectorsexc(conf%nstat, conf%nstat))
    nx_qm%eigenvectorsexc(:,:) = 0.0_dp

    allocate(nx_qm% chrom_enexc(nx_qm%nchromexc, conf%nstat))
    nx_qm%chrom_enexc(:,:) = 0.0_dp

    allocate(nx_qm% trdipexc((conf%nstat-1), 3))
    nx_qm%trdipexc(:,:) = 0.0_dp

    ! Overlap parameters
    call get_environment_variable("g16root", env)
    ! nx_qm%ovl_cmd = trim(env)//'/utils/run_parallel.pl exc_gaussian_ovl '&
    !      & //to_str(nx_qm%nchromexc)//' '//to_str(nx_qm%nprocexc)
    block
      integer :: i
      if (allocated(nx_qm%ovl_async)) deallocate(nx_qm%ovl_async)
      allocate(nx_qm%ovl_async(nx_qm%nchromexc))

      do i=1, nx_qm%nchromexc
         nx_qm%ovl_async(i) = trim(env)//'/g16/g16 exc_gau'//to_str(i)//'_ovl.com'
      end do
    end block
    nx_qm%qmproc = nx_qm%nprocexc
    
    nx_qm%ovl_out = ''

    call get_environment_variable("g16root", env)
    nx_qm%ovl_post = trim(env)//'/g16/rwfdump'

    call exc_read_input(nx_qm, parser)
  end subroutine excgau_setup


  subroutine excgau_update_input(nx_qm, nat, step)
    type(nx_qm_t), intent(inout) :: nx_qm
    integer, intent(in) :: nat
    !! Total number of atoms
    integer, intent(in) :: step
    !! Current step

    integer :: nchrom
    !! Number of chromophores
    integer :: ierr
    character(len=256), allocatable :: filelist(:)
    
    nchrom = nx_qm%nchromexc

    if (step .eq. 0) then
       ! ! Copy files from JOB_AD
       ! call get_environment_variable(name='NXHOME', value=env)
       ! env = trim(env)//'/utils/excgau_copy.pl'
       ! 
       ! call call_external(env, ierr)
       ! call check_error(ierr, EXCGAU_ERR_PERL2)
       call get_list_of_files(nx_qm%job_folder, filelist, ierr, full_path=.true.)
       call check_error(ierr, EXCGAU_ERR_PERL2, 'EXC GAU: Cannot obtain list of files')
       ierr = copy(filelist, './')
       call check_error(ierr, EXCGAU_ERR_PERL2, 'EXC GAU: Cannot copy input files')
    end if

    ! Update gaussian input files
    call inp_exc_gau(nx_qm, nat, step)
  end subroutine excgau_update_input

  subroutine excgau_run(nx_qm)
    type(nx_qm_t), intent(inout) :: nx_qm

    integer :: ierr

    character(len=MAX_CMD_SIZE) :: env
    character(len=MAX_CMD_SIZE) :: job_list(nx_qm%nchromexc)
    integer :: i

    ierr = 0

    call execute_command_line("rm -f *.log *.chk *.rwf", exitstat=ierr)

    ! call get_environment_variable(name='NXHOME', value=env)
    ! env = trim(env)//'/utils/run_parallel.pl exc_gaussian '//to_str(nx_qm%nchromexc)//&
    !      &  ' '//to_str(nx_qm%nprocexc)

    call get_environment_variable(name='g16root', value=env)
    do i=1, size(job_list)
       job_list(i) = trim(env)//'/g16/g16 exc_gau'//to_str(i)
       ! print *, 'Appending command: '//trim(job_list(i))
    end do
    call run_async(job_list, nx_qm%nprocexc, ierr)

    ! call call_external(env, ierr)
    call check_error(ierr, EXCGAU_ERR_PERL)


    env = trim(env)//'/g16/g16 exc_gau_MM.com'
    call call_external(env, ierr)
    call check_error(ierr, EXCGAU_ERR_PERL)
 end subroutine excgau_run


 subroutine excgau_read_outputs(nx_qm, traj, conf, nstat, qminfo)
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    type(nx_traj_t) :: traj
    !! Classical trajectory.
    type(nx_config_t) :: conf
    !! General configuration.
    integer, intent(in) :: nstat
    !! Total number of states.
    type(nx_qminfo_t), intent(inout) :: qminfo

    real(dp), allocatable, dimension(:) :: osc_str
    !! Oscillator strenght
    real(dp), allocatable, dimension(:) :: diapop
    !! Additional exciton informations - diabatic populations
    real(dp), allocatable, dimension(:,:) :: diaham
    !! Additional exciton informations - diabatic Hamiltonian


    integer :: i, step
    character(len=MAX_STR_SIZE) :: name_ind, name_qm, namefile

    allocate( osc_str(nstat-1) )
    osc_str(:) = 0._dp
    allocate( diapop(nstat) )
    diapop(:) = 0._dp
    allocate( diaham(nstat,nstat) )
    diaham(:,:) = 0._dp

    ! Run exciton program
    call start_prog(nx_qm, traj, conf, qminfo, osc_str, diaham, diapop)

    !! Oscillator strenght
    nx_qm%osc_str = osc_str
    nx_qm%diaham = diaham
    nx_qm%diapop = diapop

    do i = 1, nstat
       nx_qm%diaen(i) = diaham(i,i)
    end do

    deallocate( osc_str )
    deallocate( diapop )
    deallocate( diaham )

    ! First step: create the file with the orbital informations
    step = traj%step
    if (step == conf%init_step) then
       do i = 1, nx_qm%nchromexc
          write (name_ind, "(I10)") i
          write (name_qm, "(A)") "exc_gau"
          namefile = trim(adjustl(name_qm))//trim(adjustl(name_ind))
          call gau_read_orb(nx_qm, namefile)
       end do
    end if

 end subroutine excgau_read_outputs

 subroutine inp_exc_gau(nx_qm, nat, step)
 !! It generates the Gaussian inputs
   type(nx_qm_t), intent(inout) :: nx_qm
    !
    integer :: nchrom
    ! Numb of chromophores
    integer :: nat
    ! Total number of atoms
    integer :: step
    ! Current step
    integer :: nstattd
    ! Number of excited states in TDDFT calculations
    integer :: nproc_gau
    ! Number of cpu for gaussian calculations (double paralalization)
    character(len=200) :: functional
    ! The functional, for example, B3LYP...
    character(len=200) :: basis
    ! Basis set
    character(len=200) :: force_field
    ! Force field (UFF or Amber or Dreiding)

    !! Variables from "exc_inp" namelist (see more details in the subroutine
    !  "dyn_option"
    integer :: tresp
    integer :: verbose
    integer :: dip
    integer :: gs


    !! Other variables
    integer :: i, j, k, l
    integer :: u
    integer :: ibuf
    integer :: nlink
    integer :: npar
    integer :: nl
    integer :: io
    integer :: link_atoms
    integer :: par

    integer, allocatable, dimension(:,:) :: atom_group
    integer, allocatable, dimension(:,:) :: at_position

    character(len=150) :: name_ind
    character(len=150) :: first_part
    character(len=150) :: firstline
    character(len=150) :: cbuff
    character(len=150) :: charge_mult
    character(len=150) :: name_ind2
    character(len=150) :: name_ind3
    character(len=150) :: coup_file
    character(len=150) :: secondline
    character(len=150) :: line3
    character(len=150) :: file_chk
    character(len=150) :: file_esp
    character(len=150) :: file_nproc
    character(len=150) :: nfile
    character(len=150) :: card
    character(len=150) :: file_rwf

    character(len=120), allocatable, dimension(:) :: namefiles_qmmm
    character(len=120), allocatable, dimension(:) :: label
    character(len=120), allocatable, dimension(:) :: string_par
    character(len=120), allocatable, dimension(:) :: atom_label

    real(dp) :: rbuff

    real(dp), allocatable, dimension(:,:) :: coord
    real(dp), allocatable, dimension(:,:) :: coord_old

    logical :: print1, print2, print3
    logical :: conferma
    !!
    !
    nchrom = nx_qm%nchromexc
    nstattd = nx_qm%nstattdexc
    nproc_gau = nx_qm%nproc_gauexc
    functional = nx_qm%functionalexc
    basis = nx_qm%basisexc
    force_field = nx_qm%force_fieldexc
    !

    ! Default values /exc_inp/:
    tresp = 1    ! Off dig terms of the H will be performed using TMA aproximation.
    gs = 0       ! Compute only the ground state inf.
    dip = 0      ! Compute the tra. dip. moments. if .ne. 1
    verbose = 0  ! verbose = 0 meas that the exash outputs will not be generated
    coup_file = ""

    ! allocate ( nat_array(nchrom), nstat(nchrom) )
    ! nat_array(:) = 0
    ! nstat(:) = 0
    ! call exc_read_input(nx_qm, parser)
    

    tresp = nx_qm%trespexc
    gs = nx_qm%gsexc
    dip = nx_qm%dipexc
    verbose = nx_qm%verboseexc
    coup_file = nx_qm%coup_fileexc

   !! Array which contains the name of the qmmm calculations for all the
   ! chromophores
   allocate( namefiles_qmmm(nchrom) )
   namefiles_qmmm(:) = ""
   do i = 1, nchrom
      write (name_ind, "(I10)") i
      write (file_esp, "(A)") 'exc_gau'
      namefiles_qmmm(i) =trim(adjustl(file_esp))//trim(adjustl(name_ind))
   end do

   !! Read the current geometry
   allocate( coord(nat,3) )
   coord(:,:) = 0._dp
   open(newunit=u, file='geom', status='old', form='formatted')
   rewind u
   do i = 1, nat
      read(u,*) cbuff, rbuff, (coord(i,j), j = 1,3), rbuff
   end do
   close(u)
   coord = coord * au2ang

   !! Read the model for gaussian coordinates
   allocate( label(nat) )
   label(:) = ""
   open(newunit=u, file='geom_model', status='old', form='formatted')
   rewind u
   read(u,'(a)') charge_mult
   do i = 1, nat
      read(u,*) label(i)
   end do
   close(u)

    !! Checks the number of lines in the "list_exc" file
    nl = 0
    open(newunit=u, file='list_exc')
    do
       read(u,*,iostat=io)
       if (io /= 0) exit
       nl = nl + 1
    end do
    close(u)

    write (name_ind, "(I10)") nstattd

    !! Read the informations from "list_exc"
    open(newunit=u, file='list_exc', status='old', form='formatted')
    rewind u
    read(u,*) nchrom
    allocate( atom_group(nchrom,2) )
    do i = 1, nchrom
       read(u,*) (atom_group(i,j), j = 1, 2)
    end do
    ! Parameters
    npar = 0
    rewind u
    call search2("PARAMETERS", card, u, conferma, nl)
    par = 0
    if (conferma) then
       par = 1
       read(u,*) npar
       allocate( string_par(npar) )
       do i = 1, npar
          read(u,'(a)') string_par(i)
       end do
    end if
    close(u)

    !! Generate the inputs
    do j = 1, nchrom
       open(newunit=u, file='list_exc', status='old', form='formatted')
       rewind u
       link_atoms = 0
       nlink = 0

       if (allocated(at_position)) deallocate(at_position)
       ! Link atoms
       call search2("LINK ATOMS", card, u, conferma, nl)
       if (conferma) then
          if (j .eq. 1) then
             read(u,*) ibuf, nlink
             if (nlink .gt. 0) then
                link_atoms = 1
                allocate(at_position(nlink,2))
                at_position(:,:) = 0
                allocate(atom_label(nlink))
                atom_label(:) = ""
                do i = 1, nlink
                   read(u,*) (at_position(i,k), k = 1, 2), atom_label(i)
                end do
             end if
          else
             do i = 1, j-1
                read(u,*) ibuf, nlink
                if (nlink .ne. 0) then
                   do k = 1, nlink
                      read(u,*)
                   end do
                end if
             end do
             read(u,*) ibuf, nlink
             allocate( at_position(nlink,2), atom_label(nlink) )
             if (nlink .gt. 0) then
                link_atoms = 1
                do k = 1, nlink
                   read(u,*) (at_position(k,l), l = 1, 2), atom_label(k)
                end do
             end if
          end if
       else
          allocate(at_position(1, 1))
          at_position(:, :) = 0
       endif
       close(u)

       !! Ground state
       open(newunit=u, file=trim(namefiles_qmmm(j))//".com", status='unknown', &
            & form='formatted')
       rewind u

       first_part = '# ONIOM('
       firstline = trim(adjustl(first_part))//trim(adjustl(functional))//"/"&
                   &//trim(adjustl(basis))//" :"//trim(adjustl(force_field))//")=EmbedCharge force"

       nfile = namefiles_qmmm(j)
       file_chk = '%chk='//trim(adjustl(nfile))//'.chk'
       file_rwf = '%rwf='//trim(adjustl(nfile))//'.rwf'

       write (name_ind2, "(I10)") nproc_gau

       file_nproc = '%nproc='//trim(adjustl(name_ind2))
       file_nproc = adjustl(file_nproc)

       write(u,*) file_nproc
       write(u,*) file_chk
       write(u,*) file_rwf
       write(u,*) firstline
       write(u,*)
       write(u,*)"Ground state exc_gaussian"
       write(u,*)
       write(u,*) charge_mult
       do i = 1, nat
          print2 = .false.
          print3 = .false.
          do k = atom_group(j,1), atom_group(j,2)
             if (i .eq. k) then
                print1 = .false.
                print2 = .true.
                if (nlink .ne. 0) then
                   do l = 1, nlink
                      if ( i .eq. at_position(l,1) ) then
                         write(u,100) label(i), coord(i,1), coord(i,2), coord(i,3), "H", &
                                      atom_label(l), at_position(l,2)
                         print1 = .true.
                      end if
                   end do
                end if
                if ( print1 ) then
                   continue
                else
                   write(u,101) label(i), coord(i,1), coord(i,2), coord(i,3), "H"
                end if
             end if
          end do
          if ( print2 ) then
             continue
          else
             if (nlink .ne. 0) then
                do l = 1, nlink
                   if ( i .eq. at_position(l,1) ) then
                      write(u,100) label(i), coord(i,1), coord(i,2), coord(i,3), "L", &
                                   atom_label(l), at_position(l,2)
                      print3= .true.
                   end if
                end do
             end if
             if ( print3 ) then
                continue
             else
                write(u,101) label(i), coord(i,1), coord(i,2), coord(i,3), "L"
             end if
          end if
       end do

       if ( par .eq. 1 ) then
          write(u,*)
          do i = 1, npar
             write(u,*) string_par(i)
          end do
       end if

       write(u,*)

       ! Excited states for the individual chromophores
       do i = 1, nx_qm%nstatexc(j)
          write(u,'(a9)') "--Link1--"
          write(u,*) file_nproc
          write(u,*) file_chk
          write(u,*) file_rwf
          write (name_ind3, "(I10)") i

          secondline = trim(adjustl(first_part))//trim(adjustl(functional))//"/"&
                       &//trim(adjustl(basis))//" TD=(NStates="//trim(adjustl(name_ind))//&
                       &",root="//trim(adjustl(name_ind3))//") :"//trim(adjustl(force_field))&
                       &//")=EmbedCharge force guess=read geom=check"

          write(u,*) secondline
          line3 = "IOp(6/22=-4) IOp(6/29="//trim(adjustl(name_ind3))//") pop=mk IOp(6/17=2)"
          write(u,*) line3

          write(u,*)
          write(u,*)"Excited state exc_gaussian"
          write(u,*)
          write(u,*) charge_mult
          write(u,*)
          write(u,*)
       end do

       !! Write the input just of the QM part in order to exctract the
       ! wave function informations
       firstline = "#p "//trim(adjustl(functional))//"/"&
                   &//trim(adjustl(basis))//" TD=(NStates="//trim(adjustl(name_ind))//&
                   &",root=0) nosymm IOP(3/59=14) "
       write(u,'(a9)') "--Link1--"
       write(u,*) file_nproc
       write(u,*) file_chk
       write(u,*) file_rwf
       write(u,*) firstline
       write(u,*)
       write(u,*) "exctract the wavefunction iformations"
       write(u,*)
       write(u,*) charge_mult
       do i = atom_group(j,1), atom_group(j,2)
          write(u,102) label(i), (coord(i,k), k = 1, 3)
       end do
       write(u,*)

       close(u)

       if (link_atoms .eq. 1) then
          deallocate(at_position, atom_label)
       end if

    end do

    !! Generate the MM input
    secondline = "#"//trim(adjustl(force_field))//" force"
    open(newunit=u, file="exc_gau_MM.com", status='unknown', form='formatted')
    rewind u
    write(u,*) secondline
    write(u,*)
    write(u,*) "MM energy and gradients exc_gaussian"
    write(u,*)
    write(u,*) charge_mult
    do i = 1, nat
       write(u,102) label(i), (coord(i,j), j = 1, 3)
    end do
    write(u,*)
    if ( par .eq. 1 ) then
       write(u,*)
       do i = 1, npar
          write(u,*) string_par(i)
       end do
    end if
    write(u,*)
    write(u,*)
    close(u)

   !! Generate the overlap inputs
   ! Read the coordinates of the previous time step
   allocate(coord_old(nat,3))
   coord_old(:,:) = 0._dp
   if (step .eq. 0) then
      coord_old = coord
   else
      open(newunit=u, file='geom_old', status='old', form='formatted')
      rewind u
      do i = 1, nat
         read(u,*) cbuff, rbuff, (coord_old(i,j), j = 1,3), rbuff
      end do
      close(u)
      coord_old = coord_old * au2ang
   end if

   secondline = "#p nosymm Geom=NoTest IOp(3/33=1) "//trim(adjustl(basis))
   do i = 1, nchrom
      nfile = namefiles_qmmm(i)
      file_chk = '%chk='//trim(adjustl(nfile))//'_ovl.chk'
      file_rwf = '%rwf='//trim(adjustl(nfile))//'_ovl.rwf'
      open(newunit=u, file=trim(namefiles_qmmm(i))//"_ovl.com", &
           & status='unknown', form='formatted')
      write(u,*) '%kjob l302'
      write(u,*) file_rwf
      write(u,*) file_chk
      write(u,*) secondline
      write(u,*)
      write(u,*) "Overlap run, chromophore:", i
      write(u,*)
      write(u,*) charge_mult
      do j = atom_group(i,1), atom_group(i,2)
         write(u,102) label(j), (coord_old(j,k), k = 1, 3)
      end do
      do j = atom_group(i,1), atom_group(i,2)
         write(u,102) label(j), (coord(j,k), k = 1, 3)
      end do
      write(u,*)
      write(u,*)
      close(u)
   end do

    ! deallocate( nat_array, nstat )
    deallocate( namefiles_qmmm )
    deallocate( coord )
    deallocate( coord_old )
    deallocate( label )
    deallocate( atom_group )
    if (par .eq. 1) then
       deallocate( string_par )
    end if

    100 format (a8, 2x, f12.6, 2x, f12.6, 2x, f12.6, 2x, a8, 2x, a6, 1x, i6)
    101 format (a8, 2x, f12.6, 2x, f12.6, 2x, f12.6, 2x, a8)
    102 format (a8, 2x, f12.6, 2x, f12.6, 2x, f12.6)

 end subroutine inp_exc_gau

end module mod_exc_gaussian
