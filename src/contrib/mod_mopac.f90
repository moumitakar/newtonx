module mod_mopac
  use mod_kinds, only: dp
    use mod_constants, only: &
         & MAX_CMD_SIZE, au2debye, timeunit
    use mod_logger, only: &
         & call_external, check_error
    use mod_qm_t, only: nx_qm_t
    use mod_qminfo, only: nx_qminfo_t
    use mod_trajectory, only: nx_traj_t
    use mod_configuration, only: nx_config_t
    use mod_exash, only : search
    use mod_interface, only: &
         & get_list_of_files, copy, mkdir
    use mod_input_parser, only: &
         & parser_t, set => set_config
    implicit none

    private

    public :: mop_setup
    public :: mop_update_input
    public :: mop_run
    public :: mop_read_outputs

    public :: mop_backup

    ! Termination codes
    integer, parameter :: MOP_ERR_PERL = 455
    integer, parameter :: MOP_ERR_MAIN = 456

  contains

  subroutine mop_setup(nx_qm, conf, parser)
    type(nx_qm_t), intent(inout) :: nx_qm
    type(nx_config_t), intent(inout) :: conf
    type(parser_t), intent(in) :: parser

    integer :: u
    integer :: iseed

    character(len=200) :: namefile

    call set(parser, 'mopac', nx_qm%verboseexc, 'verbose')

    if (nx_qm%verboseexc == 2) then
       iseed = 1 !CHECK
       namefile = '../'//trim(conf%output_path)//'/mopac.dyn'
       open(newunit=u, file=namefile, status='new', &
            & form='unformatted', action='write')
       write(u) iseed, -1, 0
       write(u) 0, 0, 0, 1, 1
       close(u)
    end if
  end subroutine mop_setup

  subroutine mop_update_input(qm, traj, conf)
    type(nx_qm_t), intent(in) :: qm
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf

    real(dp) :: ti
    !! Current time.

    integer :: u, ierr
    character(len=256), allocatable :: filelist(:)

    ti = traj%dt * timeunit * traj%step

    open(newunit=u, file='nx2exc.inf', form='formatted', &
         status='unknown')
    rewind u
    write(u,*) conf%nat
    write(u,*) traj%step
    write(u,*) ti
    write(u,*) traj%dt * timeunit
    write(u,*) traj%nstatdyn
    write(u,*) conf%nstat
    close(u)

    if (traj%step == conf%init_step) then
       call get_list_of_files(qm%job_folder, filelist, ierr, full_path=.true.)
       call check_error(ierr, MOP_ERR_MAIN, 'MOPAC: Cannot obtain files from input folder')
       ierr = copy(filelist, './')
       call check_error(ierr, MOP_ERR_MAIN, 'MOPAC: Cannot copy input files')
    end if

  end subroutine mop_update_input


  subroutine mop_backup(conf, traj, dbg_dir)
    !! Backup information about previous MOPAC run.
    !!
    type(nx_config_t), intent(in) :: conf
    !! Main NX configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Trajectory object.
    character(len=*), intent(in) :: dbg_dir
    !! Name of the directory where the information is saved.

    integer :: ierr

    if (mod(traj%step, conf%save_cwd) == 0) then
       ierr = mkdir(dbg_dir)
       call check_error(ierr, MOP_ERR_MAIN, &
            & 'Cannot create '//dbg_dir, system=.true.)

       ierr = copy([&
            & 'mopac.out                 ', &
            & 'mopac_nx.mopac_oldvecs    ', &
            & 'mopac_nx.run_cioverlap.log'&
            & ], &
            & dbg_dir)
       call check_error(ierr, MOP_ERR_MAIN, &
            & 'Cannot copy files to '//dbg_dir, system=.true.)
    end if

  end subroutine mop_backup


  subroutine mop_run()
    integer ierr
    character(len=MAX_CMD_SIZE) :: env

    ierr = 0

    call get_environment_variable(name='MOPAC', value=env)
    env = trim(env)//'/mopac2002.x mopac '
    call call_external(env, ierr)
    call check_error(ierr, MOP_ERR_MAIN, 'MOPAC: Problem in program execution')
  end subroutine mop_run

  subroutine mop_read_outputs(nx_qm, traj, nstat, nat, qminfo)
    type(nx_qm_t) :: nx_qm
    type(nx_traj_t) :: traj
    integer, intent(in) :: nstat
    !! Total number of states.
    integer, intent(in) :: nat
    !! Total number of atoms.
    type(nx_qminfo_t), intent(inout) :: qminfo

    integer :: i, j, k
    integer :: u
    integer :: stati
    integer :: statj
    integer :: nstatdyn

    real(dp) :: cx, cy, cz
    real(dp) :: delta_en

    real(dp), allocatable, dimension(:) :: mod_mu
    real(dp), allocatable, dimension(:) :: osc_st

    real(dp), allocatable, dimension(:,:) :: mu
    real(dp), allocatable, dimension(:,:) :: mu_debye
    real(dp), allocatable, dimension(:,:) :: ovl

    character(len=MAX_CMD_SIZE) :: str

    logical :: check

    nstatdyn = traj%nstatdyn

    ! Take the energies
    open(newunit=u, file='mopac_nx.epot', status='old', &
         & form='formatted', action='read')
    rewind u
    do i = 1, nstat
        read(u,*) qminfo%repot(i)
    end do
    close(u)

    ! Take the gradients
    open(newunit=u, file='mopac_nx.grad', status='old', &
         & form='formatted')
    rewind u
    do i = 1, nat
        read(u,*) cx, cy, cz
        qminfo%rgrad(nstatdyn, 1, i) = cx
        qminfo%rgrad(nstatdyn, 2, i) = cy
        qminfo%rgrad(nstatdyn, 3, i) = cz
    end do
    close(u)

    ! Extract the transition dipoles and calculates the oscillator strenght
    j = 0
    do i = 1, nstat
       j = j + i
    end do
    allocate(mu((nstat-1), 3))
    mu(:,:) = 0._dp
    open(newunit=u, file='mopac.out', status='old', &
         & form='formatted')
    call search("Dipoles (au)", str, u, check)
    read(u,*)
    k = 0
    do i = 1, j
       read(u,*) stati, statj, cx, cy, cz
       if (stati .ne. 1 .and. statj .eq. 1) then
          k = k + 1
          mu(k,1) = cx
          mu(k,2) = cy
          mu(k,3) = cz
       end if
    end do
    close(u)

    allocate( mu_debye(nstat-1,3) )
    mu_debye(:,:) = 0._dp
    mu_debye = mu * au2debye

    nx_qm%trdipexc = mu_debye

    allocate ( mod_mu(nstat-1) )
    mod_mu(:) = 0._dp

    ! Calculate the module of the transitions dipole moments
    do i = 1, (nstat-1)
       mod_mu(i) = sqrt( mu(i,1)**2 + mu(i,2)**2 + mu(i,3)**2 )
    end do

    allocate(osc_st(nstat-1))
    osc_st(:) = 0._dp

    do i = 1, (nstat-1)
       delta_en = qminfo%repot(i+1) - qminfo%repot(1)
       osc_st(i) = 2._dp / 3._dp * delta_en * (mod_mu(i)**2)
    end do

    nx_qm%osc_str = osc_st

    ! Take the overlap matrix (local diabatization scheme)
    allocate(ovl(nstat,nstat))
    ovl(:,:) = 0._dp
    open(newunit=u, file='mopac_nx.run_cioverlap.log', status='old', &
         & form='formatted')
    rewind u
    read(u,*)
    do i = 1, nstat
       read(u,*) (ovl(i,j), j = 1, nstat)
    end do
    close(u)

    open(newunit=u, file='mopac_nx.run_cioverlap.log', status='old', &
         & form='formatted')
    rewind u
    read(u,*)
    do i = 1, nstat
       read(u,*) (ovl(i,j), j = 1, nstat)
    end do
    close(u)

    open(newunit=u, file='mop_overlap.out', status='unknown', form='formatted')
    rewind u
    write(u,*) 'CI overlap matrix'
    write(u,*) nstat, nstat
    do i = 1, nstat
       write(u,*) (ovl(i,j), j = 1, nstat)
    end do
    close(u)

    deallocate(mu)
    deallocate(mod_mu)
    deallocate(osc_st)
    deallocate(ovl)
  end subroutine mop_read_outputs

end module mod_mopac
