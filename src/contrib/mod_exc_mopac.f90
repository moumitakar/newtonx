module mod_exc_mopac
  use mod_async, only: run_async
  use mod_kinds, only: dp
    use mod_constants, only: &
         & MAX_CMD_SIZE, au2ang
    use mod_logger, only: &
         & check_error, call_external, nx_log, LOG_ERROR
    use mod_qm_t, only: nx_qm_t
    use mod_qminfo, only: nx_qminfo_t
    use mod_exash, only: &
         & search, start_prog, exc_read_input
    use mod_trajectory, only: nx_traj_t
    use mod_configuration, only: nx_config_t
    use mod_interface, only: &
         & get_list_of_files, copy, rm, mkdir
    use mod_tools, only: &
         & to_str
    use mod_input_parser, only: &
         & parser_t, set => set_config
    implicit none

    private

    public :: excmop_setup
    public :: excmop_update_input
    public :: excmop_run
    public :: excmop_read_outputs
    public :: inp_exc
    public :: inp_tnk
    public :: excmop_backup

    !public :: excmop_backup

    ! Termination codes
    integer, parameter :: EXCMOP_ERR_MAIN = 451

  contains

  subroutine excmop_setup(nx_qm, parser, conf)
    type(nx_qm_t), intent(inout) :: nx_qm
    !! QM object.
    type(parser_t), intent(in) :: parser
    type(nx_config_t), intent(inout) :: conf
    !! General configuration.

    ! integer :: nchrom
    ! !! Number of chromophores
    ! integer :: nproc
    ! !! Number of cpu to split the QM/MM calculations
    ! integer :: gen_file
    ! !! If gen_file == 1, the mopac inputs will be
    ! !! generated automatically
    ! integer :: verbose
    ! !! Genereate additional inputs EXASH program


    integer :: iseed, u

    character(len=200) :: namefile

    call set(parser, 'exc_mopac', nx_qm%nchromexc, 'nchrom')
    call set(parser, 'exc_mopac', nx_qm%nprocexc, 'nproc')
    call set(parser, 'exc_mopac', nx_qm%gen_fileexc, 'gen_file')
    call set(parser, 'exc_mopac', nx_qm%verboseexc, 'verbose')

    ! if (nx_qm%gen_fileexc == 1) then
    !    call inp_exc(nx_qm)
    !    call inp_tnk(nx_qm, conf%nat)
    ! else if (nx_qm%gen_fileexc == 2) then
    !    call inp_exc(nx_qm)
    ! else if (nx_qm%gen_fileexc > 2) then
    !    call inp_tnk(nx_qm, conf%nat)
    ! end if

    call exc_read_input(nx_qm, parser)

    if (nx_qm%verboseexc == 2 &
         & .or. nx_qm%verboseexc == 3) then
       ! BD 2022-09-20: Transfered to mod_io (io_txt_init_files)
       !
       ! iseed = 1 !CHECK
       ! namefile = trim(conf%output_path)//'/exc_mop.dyn'
       ! open(newunit=u, file=namefile, status='new', &
       !      & form='unformatted', action='write')
       ! write(u) iseed, -1, 0
       ! write(u) 0, 0, 0, 1, 1
       ! close(u)
    end if

    deallocate(nx_qm%diaham, nx_qm%diapop, nx_qm%diaen, &
         & nx_qm%eigenvectorsexc, nx_qm%chrom_enexc, &
         & nx_qm%trdipexc)

    allocate(nx_qm% diaham(conf%nstat, conf%nstat))
    nx_qm%diaham(:,:) = 0.0_dp

    allocate(nx_qm% diapop(conf%nstat))
    nx_qm%diapop(:) = 0.0_dp

    allocate(nx_qm% diaen(conf%nstat))
    nx_qm%diaen(:) = 0.0_dp

    ! allocate(nx_qm% nstatexc(nx_qm%nchromexc))
    ! nx_qm%nstatexc(:) = 0

    ! allocate(nx_qm% nat_arrayexc(nx_qm%nchromexc))
    ! nx_qm%nat_arrayexc(:) = 0

    allocate(nx_qm% eigenvectorsexc(conf%nstat, conf%nstat))
    nx_qm%eigenvectorsexc(:,:) = 0.0_dp

    allocate(nx_qm% chrom_enexc(nx_qm%nchromexc, conf%nstat))
    nx_qm%chrom_enexc(:,:) = 0.0_dp

    allocate(nx_qm% trdipexc((conf%nstat-1), 3))
    nx_qm%trdipexc(:,:) = 0.0_dp

    
  end subroutine excmop_setup

  subroutine excmop_update_input(qm, traj, conf)
    type(nx_qm_t), intent(in) :: qm
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf

    real(dp) :: ti

    integer :: u, ierr
    character(len=256), allocatable :: filelist(:)

    ti = traj%dt * traj%step

    open(newunit=u, file='nx2exc.inf', form='formatted', &
         status='unknown')
    rewind u
    write(u,*) conf%nat
    write(u,*) traj%step
    write(u,*) ti
    write(u,*) traj%dt
    write(u,*) traj%nstatdyn
    write(u,*) conf%nstat
    close(u)

    if (traj%step == 0) then
       call get_list_of_files(qm%job_folder, filelist, ierr, full_path=.true.)
       call check_error(ierr, EXCMOP_ERR_MAIN, 'EXC MOP: Cannot obtain list of files')
       ierr = copy(filelist, './')
       call check_error(ierr, EXCMOP_ERR_MAIN, 'EXC MOP: Cannot copy input files')

       if (qm%gen_fileexc == 1) then
          call inp_exc(qm)
          call inp_tnk(qm, conf%nat)
       else if (qm%gen_fileexc == 2) then
          call inp_exc(qm)
       else if (qm%gen_fileexc > 2) then
          call inp_tnk(qm, conf%nat)
       end if
    end if


  end subroutine excmop_update_input


  subroutine excmop_backup(nx_qm, conf, traj, dbg_dir)
    !! Backup information about previous MOPAC run.
    !!
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM configuration.
    type(nx_config_t), intent(in) :: conf
    !! Main NX configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Trajectory object.
    character(len=*), intent(in) :: dbg_dir
    !! Name of the directory where the information is saved.

    integer :: ierr, i, j, ii
    character(len=MAX_CMD_SIZE), allocatable :: filelist(:)

    if (mod(traj%step, conf%save_cwd) == 0) then
       allocate(filelist( 3*nx_qm%nchromexc + 1))
       j = 0
       do i=1, nx_qm%nchromexc
          ii = j*i + 1
          filelist(ii) = 'exc_mop'//to_str(i)//'.out'
          filelist(ii+1) = 'exc_mop'//to_str(i)//'_nx.mopac_oldvecs'
          filelist(ii+2) = 'exc_mop'//to_str(i)//'_nx.run_cioverlap.out'
          j = j+1
       end do

       filelist(3*nx_qm%nchromexc + 1) = 'exc_overlap.out'

       ierr = mkdir(dbg_dir)
       call check_error(ierr, EXCMOP_ERR_MAIN, &
            & 'Cannot create '//dbg_dir, system=.true.)

       ierr = copy(filelist, dbg_dir)
       call check_error(ierr, EXCMOP_ERR_MAIN, &
            & 'Cannot copy files to '//dbg_dir, system=.true.)
    end if

  end subroutine excmop_backup


  subroutine excmop_run(nx_qm)
    type(nx_qm_t), intent(inout) :: nx_qm

    integer ierr

    character(len=MAX_CMD_SIZE) :: env
    character(len=MAX_CMD_SIZE) :: job_list(nx_qm%nchromexc)
    integer :: i

    ierr = 0

    call nx2tnk(trim(nx_qm%job_folder)//'/fullMM_tnk.xyz', 'fullMM_tnk.xyz_new')
    ierr = rename("fullMM_tnk.xyz_new","fullMM_tnk.xyz")

    call get_environment_variable(name='TINKER', value=env)
    env = trim(env)//'/testgrad.x fullMM_tnk.xyz -k fullMM_tnk.key Y N N > fullMM_tnk.out &
         &2>&1'
    call execute_command_line(env, exitstat=ierr)

    call execute_command_line(&
         & "rm -rf *_nx.epot *_nx.grad *_nx.grad *_nx.grad.all *_nx.grad.run_cioverlap.log", &
         & exitstat=ierr)

    call get_environment_variable(name='MOPAC', value=env)
    do i=1, size(job_list)
       job_list(i) = trim(env)//'/mopac2002.x exc_mop'//to_str(i)//' > mopac_tmp.out'
       ! print *, 'Appending command: '//trim(job_list(i))
    end do
    call run_async(job_list, nx_qm%nprocexc, ierr)
    
    ! call get_environment_variable(name='NXHOME', value=env)
    ! env = trim(env)//'/utils/run_parallel.pl exc_mopac '//to_str(nx_qm%nchromexc)//&
    !      & ' '//to_str(nx_qm%nprocexc)
    ! 
    ! call call_external(env, ierr)
    call check_error(ierr, EXCMOP_ERR_MAIN)
 end subroutine excmop_run

 subroutine excmop_read_outputs(nx_qm, traj, conf, nstat, qminfo)
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    type(nx_traj_t) :: traj
    !! Classical trajectory.
    type(nx_config_t) :: conf
    !! General configuration.
    integer, intent(in) :: nstat
    !! Total number of states.
    type(nx_qminfo_t), intent(inout) :: qminfo

    real(dp), allocatable, dimension(:) :: osc_str
    !! Oscillator strenght
    real(dp), allocatable, dimension(:) :: diapop
    !! Additional exciton informations - diabatic populations
    real(dp), allocatable, dimension(:,:) :: diaham
    !! Additional exciton informations - diabatic Hamiltonian


    integer :: i

    allocate( osc_str(nstat-1) )
    osc_str(:) = 0._dp
    allocate( diapop(nstat) )
    diapop(:) = 0._dp
    allocate( diaham(nstat,nstat) )
    diaham(:,:) = 0._dp

    call start_prog(nx_qm, traj, conf, qminfo, osc_str, diaham, diapop)

    !! Oscillator strenght
    nx_qm%osc_str = osc_str
    nx_qm%diaham = diaham
    nx_qm%diapop = diapop

    do i = 1, nstat
       nx_qm%diaen(i) = diaham(i,i)
    end do

    deallocate( osc_str )
    deallocate( diapop )
    deallocate( diaham )

 end subroutine excmop_read_outputs

 subroutine inp_exc(nx_qm)
 !! It generates the mopac input files in the first time step the dyncamis
    type(nx_qm_t) :: nx_qm

    integer :: i, j, k
    integer :: u
    integer :: nlines
    integer :: io
    integer :: nat
    integer :: nchrom
    integer :: natf
    integer :: nati
    integer :: nlines_ok
    integer :: nlines2
    integer :: ibuf
    integer :: nlink
    integer :: nl

    integer, allocatable, dimension(:) :: ind
    integer, allocatable, dimension(:) :: atom_lnk_pos

    integer, allocatable, dimension(:,:) :: at_pos

    character(len=200) :: name_ind
    character(len=200) :: file
    character(len=200) :: nameqm
    character(len=200) :: namemm
    character(len=200) :: string
    character(len=200) :: name_tot
    character(len=200) :: card

    character(len=200), allocatable, dimension(:) :: tot_txt
    character(len=200), allocatable, dimension(:) :: lab
    character(len=200), allocatable, dimension(:) :: add_info
    character(len=200), allocatable, dimension(:) :: name_mopac
    character(len=200), allocatable, dimension(:) :: tot_txt2
    character(len=200), allocatable, dimension(:) :: tot_txt3
    character(len=200), allocatable, dimension(:) :: atom_lnk_label

    real(dp), allocatable, dimension(:,:) :: geom

    logical :: file_exists
    logical :: conferma

    nlines = 0

    ! -- Checks the number of lines
    open(newunit=u, file=trim(nx_qm%job_folder)//'/layout_exc')
    do
       read(u,*,iostat=io)
       if (io /= 0) exit
       nlines = nlines + 1
    end do
    close (u)

    ! --Reads the tinker file in order to take the geometry:
    open(newunit=u, file=trim(trim(nx_qm%job_folder))//'/fullMM_tnk.xyz', &
         & status='old', form='formatted')

    rewind u
    read(u,*) nat
    allocate( ind(nat), lab(nat), geom(nat,3) )
    ind(:) = 0
    lab(:) = ""
    geom(:,:) = 0._dp
    do i = 1, nat
       read(u,*) ind(i), lab(i), (geom(i,j), j = 1, 3)
    end do
    close(u)

    ! --Reads the list
    open(newunit=u, file=trim(nx_qm%job_folder)//'/list_exc', &
         & status='old', form='formatted')
    rewind u
    read(u,*) nchrom
    allocate( at_pos(nchrom,2) )
    at_pos(:,:) = 0
    do i = 1, nchrom
       read(u,*) (at_pos(i,j), j = 1, 2)
    end do
    ! --Read link atoms (if it is available)
    rewind u
    call search("LINK ATOMS", card, u, conferma)
    if ( conferma ) then
       do i = 1 , nchrom
          read(u,*) ibuf, nlink
          if (nlink .gt. 0) then
             allocate(atom_lnk_pos(nlink))
             atom_lnk_pos(:) = 0
             allocate(atom_lnk_label(nlink))
             atom_lnk_label(:) = ""
             do j = 1, nlink
                read(u,*) atom_lnk_pos(j), atom_lnk_label(j)
             end do
              do j = 1, nlink
                lab(atom_lnk_pos(j)) = atom_lnk_label(j)
             end do
              deallocate(atom_lnk_pos)
             deallocate(atom_lnk_label)
          end if
       end do
    end if
    close(u)

    ! -- Array which contains the name of the Mopac's files
    allocate( name_mopac(nchrom) )
    name_mopac(:) = ""
    do i = 1, nchrom
       write (name_ind, "(I10)") i
       write (file, "(A)") 'exc_mop'
       name_mopac(i) =trim(adjustl(file))//trim(adjustl(name_ind))//'.dat'
    end do

    ! --Ignores the blak lines
    allocate( tot_txt(nlines) )
    tot_txt(:) = ""
    nlines_ok = 0
    open(newunit=u, file=trim(nx_qm%job_folder)//'/layout_exc', &
         & status='old', form='formatted')
    rewind u
    do i = 1, nlines
       read(u,'(a)') string
       if (string .eq. ' ') then
          continue
       else
          nlines_ok = nlines_ok + 1
          tot_txt(nlines_ok) = string
       end if
    end do
    close(u)

    allocate( tot_txt2(nlines_ok) )
    tot_txt2(:) = ""
    do i = 1, nlines_ok
       tot_txt2(i) = tot_txt(i)
    end do

    ! --Checks if the file "layout_exc2" exists
    inquire(file=trim(nx_qm%job_folder)//"/layout_exc2", exist=file_exists)
    if (file_exists .eqv. .true.) then

       nlines2 = 0
       open(newunit=u, file=trim(nx_qm%job_folder)//'/layout_exc2')
       do
          read(u,*,iostat=io)
          if (io /= 0) exit
          nlines2 = nlines2 + 1
       end do
       close (u)

       allocate( tot_txt3(nlines2) )
       tot_txt3(:) = ""
       open(newunit=u, file=trim(nx_qm%job_folder)//'/layout_exc2', &
            & status='old', form='formatted')
       rewind u
       do i = 1, nlines2
          read(u,'(a)') string
          tot_txt3(i) = string
       end do
       close(u)

    end if

    ! --Generates the input files
    do i = 1, nchrom

       open(newunit=u, file=trim(nx_qm%job_folder)//'/list_exc', &
            & status='unknown', form='formatted')
       rewind u
       nl = 0
       call search("ADD INF", card, u, conferma)
       if ( conferma ) then
          if ( i .gt. 1 ) then
             do j = 1, i-1
                read(u,*) ibuf, nl
                if (nl .eq. 0) then
                   continue
                else
                   do k = 1, nl
                      read(u,*)
                   end do
                end if
             end do
          end if
          read(u,*) ibuf, nl
          if (nl .eq. 0) then
             continue
          else
             allocate ( add_info(nl) )
             add_info(:) = ""
             do j = 1, nl
                read(u,'(a)') add_info(j)
             end do
          end if
       end if
       close(u)

       open(newunit=u, file=name_mopac(i), status='unknown', form='formatted')
          rewind u

          if (at_pos(i,1) .eq. 1) then

             natf = nat - at_pos(i,2)

             write (name_ind, "(I10)") natf
             write (file, "(A)") 'QMMM='
             nameqm = trim(adjustl(file))//trim(adjustl(name_ind))//' +'
             write(u,*) nameqm

             do j = 1, nlines_ok
                write(u,*) tot_txt2(j)
             end do

             write(u,*)
             do j = 1, at_pos(i,2)
                write(u,100) lab(j), geom(j,1), 1,  geom(j,2), 1,  geom(j,3), 1
             end do

             if (file_exists .eqv. .true.) then
                write(u,*)
                do j = 1, nlines2
                   write(u,*) tot_txt3(j)
                end do
             end if

             if (nl .ne. 0) then
                write(u,*) ""
                do j = 1, nl
                   write(u,*) add_info(j)
                end do
                write(u,*) ""
                deallocate( add_info )
             end if

          else

             nati = at_pos(i,1) - 1
             natf = nat - at_pos(i,2)

             write (name_ind, "(I10)") natf
             write (file, "(A)") 'QMMM='
             nameqm = trim(adjustl(file))//trim(adjustl(name_ind))

             write (name_ind, "(I10)") nati
             write (file, "(A)") 'MMQM='
             namemm = trim(adjustl(file))//trim(adjustl(name_ind))//' +'

             name_tot = trim(adjustl(nameqm))//' '//trim(adjustl(namemm))

             write(u,*) name_tot

             do j = 1, nlines_ok
                write(u,*) tot_txt2(j)
             end do
             write(u,*)

             do j = at_pos(i,1), at_pos(i,2)
                write(u,100) lab(j), geom(j,1), 1,  geom(j,2), 1,  geom(j,3), 1
             end do

             if (file_exists .eqv. .true.) then
                write(u,*)
                do j = 1, nlines2
                   write(u,*) tot_txt3(j)
                end do
             end if

             if (nl .ne. 0) then
                write(u,*) ""
                do j = 1, nl
                   write(u,*) add_info(j)
                end do
                write(u,*) ""
                deallocate( add_info )
             end if

          end if

       close(u)

    end do

    100 format (a5, 1x, *(f12.6, i2))

 end subroutine inp_exc

 subroutine inp_tnk(nx_qm, nat_tot)
 !! It generates the tinker input files in the first time step the dyncamis
    type(nx_qm_t) :: nx_qm

    integer :: i, j, k, l
    integer :: u
    integer :: nchrom
    integer :: nel
    integer :: nat
    integer :: nlines
    integer :: io
    integer :: nat_tot
    integer :: nat_last
    integer :: gen_file
    integer :: ibuf
    integer :: nl
    integer :: numb

    integer, allocatable, dimension(:) :: vecnumb
    integer, allocatable, dimension(:) :: atype
    integer, allocatable, dimension(:) :: atype_new
    integer, allocatable, dimension(:,:) :: atom_group
    integer, allocatable, dimension(:,:) :: lnk
    integer, allocatable, dimension(:,:) :: at_ty

    character(len=200) :: card
    character(len=200) :: name1
    character(len=200) :: name2
    character(len=200) :: line

    character(len=200), allocatable, dimension(:) :: name_mopac
    character(len=200), allocatable, dimension(:) :: coord_one
    character(len=200), allocatable, dimension(:) :: coord_two
    character(len=200), allocatable, dimension(:) :: veclab
    character(len=200), allocatable, dimension(:) :: save_key
    character(len=200), allocatable, dimension(:) :: groups

    real(dp), allocatable, dimension(:,:) :: geom

    logical :: conferma

    gen_file = nx_qm%gen_fileexc

    ! --Reads the "list_exc" file
    open(newunit=u, file=trim(nx_qm%job_folder)//'/list_exc', &
         & form='formatted', status='old')
    rewind u
    read(u,*) nchrom
    allocate( atom_group(nchrom,2) )
    atom_group(:,:) = 0
    do i = 1, nchrom
       read(u,*) (atom_group(i,j), j = 1, 2)
    end do
    nat_last = atom_group(nchrom, 2)
    if (gen_file .ne. 5) then
       call search("ATOM TYPE", card, u, conferma)
       read(u,*) nel
       allocate( at_ty(nel,2) )
       at_ty(:,:) = 0
       do i = 1, nel
          read(u,*) (at_ty(i,j), j = 1, 2)
       end do
    else
       allocate(at_ty(1,1))
       at_ty(:, :) = 0
    end if
    close(u)

    ! --Array with the name of the Mopac's files
    allocate( name_mopac(nchrom) )
    name_mopac(:) = ""
    do i = 1, nchrom
       write (name1, "(I10)") i
       write (name2, "(A)") 'exc_mop'
       name_mopac(i) =trim(adjustl(name2))//trim(adjustl(name1))
    end do

    if (gen_file .ne. 5) then
       ! -- 1°: Reads all the rows of the "file_tnk.xyz" file as string
       !        and add "zeros" in the end of each line;
       !
       ! -- 2°: Reads all the coluns of the strings.
       open (newunit=u, file=trim(nx_qm%job_folder)//'/fullMM_tnk.xyz', &
             & status="old", form="formatted")
       rewind u
       read(u,*) nat
       allocate( coord_one(nat), coord_two(nat) )
       coord_one(:) = ""
       coord_two(:) = ""
       do i = 1, nat
          read (u, '(a)') line
          coord_one(i) = line
          coord_two(i) = trim(line)//"     "//"0     0     0	0	0	0	0"
       end do
       close(u)

       allocate( vecnumb(nat), veclab(nat), geom(nat,3), atype(nat), &
                 & lnk(nat,8) )
       vecnumb(:) = 0
       veclab(:) = ""
       geom(:,:) = 0._dp
       atype(:) = 0
       lnk(:,:) = 0

       do i = 1, nat
          read(coord_two(i),*) vecnumb(i), veclab(i), (geom(i,j), j = 1, 3), &
                               & atype(i), (lnk(i,j), j = 1, 8)
       end do

       ! --Loop over the chromophores
       !  - Switch the atoms type
       do i = 1, nchrom

          allocate( atype_new(nat) )
          atype_new(:) = 0
          atype_new = atype

          do j = atom_group(i,1), atom_group(i,2)
             do k = 1, nel
                if (atype(j) .eq. at_ty(k,2)) then
                   atype_new(j) = at_ty(k,1)
                end if
             end do
          end do

          open(newunit=u, file=trim(name_mopac(i))//'_tnk.xyz', status='unknown', &
               form='formatted')
          rewind u
          write(u,*) nat
          do j = 1, nat
             write(u,10) vecnumb(j), veclab(j), (geom(j,k), k = 1,3), &
                          & atype_new(j), (lnk(j,l), l = 1, 8)
          end do
          close(u)

          deallocate( atype_new )

       end do

    end if

    10 format (4x,i2,2x,a2,2x,f12.5,1x,f12.5,1x,f12.5,1x,i6,*(3x,i3))

    if (gen_file .eq. 3 .or. gen_file .eq. 5 .or. gen_file .eq. 1) then
       !
       ! => Generate the tinker.key files
       !

       ! --Checks the number of lines
       nlines = 0
       open(newunit=u, file=trim(nx_qm%job_folder)//'/fullMM_tnk.key')
       do
          read(u,*,iostat=io)
          if (io /= 0) exit
          nlines = nlines + 1
       end do
       close (u)


       ! --Read all the lines layout lines
       allocate( save_key(nlines) )
       save_key(:) = ""
       open(newunit=u, file=trim(nx_qm%job_folder)//'/fullMM_tnk.key', &
            & status='old', form='formatted')
       rewind u
       do i = 1, nlines
          read(u,'(a)') line
          save_key(i) = line
       end do
       close(u)

       ! --Loop over the chromophores
       do i = 1, nchrom
          conferma = .false.
          open(newunit=u, file=trim(nx_qm%job_folder)//'/fullMM_tnk.key', &
               & form='formatted', status='old')
          rewind u
          call search("GROUPS", card, u, conferma)
          if (conferma) then
             if (i .eq. 1) then
                read(u,*) ibuf, nl
                allocate( groups(nl) )
                do j = 1, nl
                   read(u,'(a)') groups(j)
                end do
             else
                do j = 1, i-1
                   read(u,*) ibuf, nl
                   do k = 1, nl
                      read(u,*)
                   end do
                end do
                read(u,*) ibuf, nl
                allocate( groups(nl) )
                do j = 1, nl
                   read(u,'(a)') groups(j)
                end do
             end if
          end if
          close(u)

          open(newunit=u, file=trim(name_mopac(i))//'_tnk.key', status='unknown', &
               form='formatted')
          rewind u

          do j = 1, nlines
             write(u,'(a)') save_key(j)
          end do
          write(u,*)
          write(u,'(a19)') '# Partial Structure'

          if (conferma) then
             do j = 1, nl
                write(u,'(a)') groups(j)
             end do
             deallocate( groups )
          else
             if (i .eq. 1) then
                numb = atom_group(i,1)*(-1)
                write(u,12) 'GROUP', 1, numb, atom_group(i,2)
                numb = (atom_group(i,2) + 1)*(-1)
                write(u,12) 'GROUP', 2, numb, nat
                write(u,11) 'GROUP-SELECT', 1, 1, 0.0
                write(u,11) 'GROUP-SELECT', 1, 2, 1.0
                write(u,11) 'GROUP-SELECT', 2, 2, 1.0
             else
                if ( i .eq. nchrom) then
                   if (nat_last .ne. nat_tot) then
                      numb = atom_group(i,1) - 1
                      write(u,12) 'GROUP', 1, -1, numb
                      numb = atom_group(i,1)*(-1)
                      write(u,12) 'GROUP', 2, numb, atom_group(i,2)
                      numb = (atom_group(i,2) + 1)*(-1)
                      write(u,12) 'GROUP', 3, numb, nat
                      write(u,11) 'GROUP-SELECT', 1, 1, 1.0
                      write(u,11) 'GROUP-SELECT', 1, 2, 1.0
                      write(u,11) 'GROUP-SELECT', 2, 2, 0.0
                      write(u,11) 'GROUP-SELECT', 1, 3, 1.0
                      write(u,11) 'GROUP-SELECT', 2, 3, 1.0
                      write(u,11) 'GROUP-SELECT', 3, 3, 1.0
                   else
                      numb = atom_group(1,1)*(-1)
                      write(u,12) 'GROUP', 1, numb, (atom_group(i,1)-1)
                      numb = (atom_group(i,1))*(-1)
                      write(u,12) 'GROUP', 2, numb, nat
                      write(u,11) 'GROUP-SELECT', 1, 1, 1.0
                      write(u,11) 'GROUP-SELECT', 1, 2, 1.0
                      write(u,11) 'GROUP-SELECT', 2, 2, 0.0
                   end if
                else
                   numb = atom_group(i,1) - 1
                   write(u,12) 'GROUP', 1, -1, numb
                   numb = atom_group(i,1)*(-1)
                   write(u,12) 'GROUP', 2, numb, atom_group(i,2)
                   numb = (atom_group(i,2) + 1)*(-1)
                   write(u,12) 'GROUP', 3, numb, nat
                   write(u,11) 'GROUP-SELECT', 1, 1, 1.0
                   write(u,11) 'GROUP-SELECT', 1, 2, 1.0
                   write(u,11) 'GROUP-SELECT', 2, 2, 0.0
                   write(u,11) 'GROUP-SELECT', 1, 3, 1.0
                   write(u,11) 'GROUP-SELECT', 2, 3, 1.0
                   write(u,11) 'GROUP-SELECT', 3, 3, 1.0
                end if
             end if
          end if
          close(u)
       end do
    end if

    12 format (a5, 8x, i2, 1x, i10, 1x, i10)
    11 format (a12, 1x, i2, 1x, i2, 2x, f3.1)

 end subroutine inp_tnk

 subroutine nx2tnk(origin, newfile)
   !! It updates the tinker coordinates for the full-MM calculation
   character(len=*), intent(in) :: origin
   character(len=*), intent(in) :: newfile

    integer :: i, j, k
    integer :: u
    integer :: nat

    integer, allocatable, dimension(:) :: vecnumb
    integer, allocatable, dimension(:) :: atype

    integer, allocatable, dimension(:,:) :: lnk

    real(dp) :: rbuff

    real(dp), allocatable, dimension(:,:) :: coord

    character(len=200) :: line
    character(len=200) :: cbuff

    character(len=200), allocatable, dimension(:) :: coord_one
    character(len=200), allocatable, dimension(:) :: coord_two
    character(len=200), allocatable, dimension(:) :: veclab

    !
    ! => Reads the old tinker file in order to extract the numb of atoms,
    ! atom types and link atoms
    !

    ! -- 1°: Reads all the rows of the "file_tnk.xyz" file as string
    !        and add "zeros" in the end of each line;
    !
    ! -- 2°: Reads all the coluns of the strings.
    !open (newunit=u, file=trim(trim(nx_qm%job_folder))//'/fullMM_tnk.xyz', &
    open (newunit=u, file=origin, &
          & status="old", form="formatted")
    rewind u
    read(u,*) nat
    allocate( coord_one(nat), coord_two(nat) )
    coord_one(:) = ""
    coord_two(:) = ""
    do i = 1, nat
       read (u, '(a)') line
       coord_one(i) = line
       coord_two(i) = trim(line)//"     "//"0   0   0   0   0   0	  0"
    end do
    close(u)

    allocate( vecnumb(nat), veclab(nat), atype(nat), lnk(nat,8) )
    vecnumb(:) = 0
    veclab(:) = ""
    atype(:) = 0
    lnk(:,:) = 0
    do i = 1, nat
       read(coord_two(i),*) vecnumb(i), veclab(i), rbuff, rbuff, rbuff, &
            & atype(i), (lnk(i,j), j = 1, 8)
    end do

    !
    ! => Reads the current geometry from Newton-X
    !
    allocate( coord(nat,3) )
    coord(:,:) = 0._dp

    open(newunit=u, file='geom', status='old', form='formatted')
    rewind u
    do i = 1, nat
       read(u,*) cbuff, rbuff, (coord(i,j), j = 1, 3), rbuff
    end do
    close(u)

    coord = coord * au2ang

    !
    ! => Writes the updated geometry tinker's file
    !
    open(newunit=u, file=newfile, status='unknown', form='formatted')
    rewind u
    write(u, '(3x, i6)') nat
    do i = 1, nat
       write(u,100) vecnumb(i), veclab(i), (coord(i,j), j=1,3), atype(i), &
       (lnk(i,k), k = 1,8)
    end do
    close(u)

    100 format (4x,i2,2x,a2,2x,f12.5,1x,f12.5,1x,f12.5,1x,i6,*(3x,i3))

 end subroutine nx2tnk

end module mod_exc_mopac
