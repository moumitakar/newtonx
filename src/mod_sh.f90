! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_sh
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-03-27
  !!
  !! This module handles the resolution of the time-dependent
  !! Schrödinger equation (TDSE).  The implementation is
  !! object-oriented, with the ``nx_sh_solver_t`` object being used
  !! to handle the integration.
  !!
  !! The details can be found in ``mod_sh_traj_t.f08``, that defines
  !! specific forms of the electronic population and probabilities,
  !! and ``mod_decoherence.f08``, that defines the routines for
  !! handling decoherence corrections.
  use mod_kinds, only: dp
  use mod_constants, only: deg2rad, timeunit, au2ev, MAX_STR_SIZE
  use mod_cs_fssh, only: nx_csfssh_params_t
  use mod_logger, only: &
       & nx_logger_t, nx_log, &
       & LOG_DEBUG, LOG_TRIVIA, LOG_INFO, LOG_WARN, LOG_ERROR, &
       & check_error
  use mod_tools, only: &
       & find_index, compute_ekin, compute_ekin_sub, assert_equal, &
       & norm, reshape_epot, nx_random_number, scalar_product, norm, &
       & to_str
  use mod_configuration, only: nx_config_t
  use mod_trajectory, only: nx_traj_t
  use mod_sh_t, only: nx_sh_t
  use mod_sh_hop_point_t, only: nx_hop_point_t
  use mod_sh_traj_t, only: &
       & nx_sh_traj_t, nx_coeff_t, nx_phase_t, nx_shprob_t
  use mod_io, only: nx_output_t
  use mod_integrators, only: &
       & arr_fgeneric_c, arr_fgeneric_r, &
       & finite_element_int, butcher_int, upropagator_int
  use mod_decoherence, only: &
       & set_wp, evolrwp, add_prob_wp, &
       & decoherence
  use mod_sh_locdiab, only: &
       & get_tmat, propag_ld, trans_ld
  use mod_numerics, only: polint
  implicit none

  private

  type, public :: nx_sh_solver_t
     !! Surface hopping solver.
     !!
     !!
     type(nx_sh_traj_t), allocatable :: shtraj(:)
     !! Array of SH trajectories. The length of this array depends on
     !! the integration method chosen, and the size will be allocated
     !! accordingly (for instance, choosing Butcher's integration
     !! algorithm requires 3 members in the history, corresponding to
     !! \(t-dt\), \(t\) and \(t+dt\)).
     type(nx_sh_t) :: params
     !! Set of SH parameters, extracted from a preexisting
     !! ``nx_sh_t`` object.
     integer :: step
     !! Current integration step.
     integer :: lvprt
     !! Debugging produced by the SH routines:
     !!
     !! - 1 (default): no specific debug file is provided
     !! - 2: basic debug info
     real(dp), dimension(:, :), allocatable :: incv
     !! Increment of velocity for the linear interpolation between times ``t``
     !! and ``t+dt``.  It is typically equal to ``traj%veloc - traj%old_veloc``.
     real(dp), dimension(:, :, :), allocatable :: incnad
     !! Increment of NAD for the linear interpolation between times ``t``
     !! and ``t+dt``.  It is typically equal to ``traj%nad - traj%old_nad``.
     real(dp), dimension(:, :), allocatable :: epot_all
     !! Array of potential energies, with first dimension corresponding to the
     !! number of time steps taken into account (typically 4 for ``t+dt``,
     !! ``t``, ``t-dt`` and ``t-2dt`` corresponding to the values in
     !! ``traj%epot`` and ``traj%old_epot), and second dimension corresponding
     !! to the number of states.  This array is created with the routine ``reshape_epot``.
     real(dp), dimension(:), allocatable :: en_times
     !! Array corresponding to the times at which we have the potential
     !! energies computed, typically ``0, dt, 2dt, 3dt``, with ``0`` being set
     !! for time ``t-2dt``.  This array is also created by the ``reshape_epot``
     !! routine.
     real(dp), dimension(:, :), allocatable :: shprob
     !! SH probabilities computed along the SH trajectory (only for
     !! the current macro-iteration step).
     real(dp), dimension(:), allocatable :: rx
     !! Random number generated along the SH trajectory (same as
     !! for ``shprob``).
     real(dp), dimension(:, :), allocatable :: cio
     !! State overlap matrix, obtained from cioverlap typically (this
     !! is only needed with local diabatization).
     integer :: last_hop
     !! Last hopping step.

     ! CS-FSSH
     type(nx_csfssh_params_t) :: csfssh
     logical :: run_complex
     real(dp), dimension(:, :, :), allocatable :: incnad_i
     !! Increment of NAD for the linear interpolation between times ``t``
     !! and ``t+dt``.  It is typically equal to ``traj%nad_i - traj%old_nad_i``.
     real(dp), dimension(:, :), allocatable :: epot_i_all
     !! Array of imaginary part of potential energies, with first dimension corresponding
     !! to the number of time steps taken into account (typically 4 for ``t+dt``, ``t``,
     !! ``t-dt`` and ``t-2dt`` corresponding to the values in ``traj%epot_i`` and
     !! ``traj%old_epot_i), and second dimension corresponding to the number of states.
     !! This array is created with the routine ``reshape_epot``.
   contains
     procedure :: init => sh_solver_init
     procedure :: run => sh_solver_run
     procedure :: destroy => sh_solver_destroy
     procedure :: set => sh_solver_set_data
     procedure :: compute_gamma => sh_compute_gamma
  end type nx_sh_solver_t

  type(nx_logger_t) :: sh_log


contains

  subroutine sh_solver_init(this, traj, sh, conf)
    !! Initialize  the solver.
    !!
    class(nx_sh_solver_t), intent(inout) :: this
    !! Solver object.
    type(nx_traj_t), intent(inout) :: traj
    !! Main trajectory object.
    type(nx_sh_t), intent(inout) :: sh
    !! SH configuration object.
    type(nx_config_t), intent(in) :: conf
    !! Main NX configuration object.

    integer :: i

    ! Set all parameters
    this%params = sh
    this%params%dts = conf%dt / sh%ms
    this%last_hop = 0

    ! Initialize the size of the shtraj array, according to integrator type
    ! selected.
    select case (sh%integrator)
    case(0)
       allocate(this%shtraj(3))
    case(1)
       allocate(this%shtraj(3))
    case(2)
       allocate(this%shtraj(2))
    case(3)
       allocate(this%shtraj(3))
    end select

    allocate(this%incv, mold=traj%veloc)
    allocate(this%incnad, mold=traj%nad)
    this%incv(:, :) = 0.0_dp
    this%incnad(:, :, :) = 0.0_dp

    allocate(this%epot_all(size(traj%old_epot + 1), conf%nstat))
    this%epot_all(:, :) = 0.0_dp

    ! CS-FSSH
    this%run_complex = traj%run_complex
    if (this%run_complex) then
       allocate(this%incnad_i, mold=traj%nad)
       allocate(this%epot_i_all(size(traj%old_gamma + 1), conf%nstat))
    else
       allocate(this%incnad_i(1, 1, 1))
       allocate(this%epot_i_all(1, 1))
    end if
    this%epot_i_all(:, :) = 0.0_dp
    this%incnad_i = 0.0_dp

    do i=1, size(this%shtraj)
       call this%shtraj(i)% init(conf%nat, conf%nstat, traj%run_complex)
       if(traj%is_qmmm) then
         call this%shtraj(i)% init_qmmm(traj%is_qm_atom)
       end if
    end do

    ! Set the first element of the array
    call this%shtraj(1)% set_data(traj%wf, traj%nstatdyn, traj%veloc)
    this%shtraj(1)%epot(:) = traj%epot(:)
    if (this%run_complex) then
       this%shtraj(1)%epot_i(:) = traj%gamma(:)
    end if
    
    call this%shtraj(1)% build_sigma(traj%nad, traj%nad_i, traj%veloc, this%params%vdoth)
    call this%shtraj(1)% compute_adot()

    allocate(this%shprob( sh%ms, conf%nstat ))
    allocate(this%rx( sh%ms ))
    this%shprob(:, :) = 0.0_dp
    this%rx = 0.0_dp

    if (this%params%integrator == 2) then
       allocate(this%cio( conf%nstat, conf%nstat ))
       this%cio(:, :) = 0.0_dp
    end if

    this%lvprt = conf%lvprt

    this%step = 0

    ! Initialize logger
    call sh_log%init( conf%kt, conf%init_step, conf%lvprt, &
         & logfile='sh.out', errfile='sh.out')
  end subroutine sh_solver_init


  subroutine sh_solver_destroy(this)
    !! Destructor for the solver object.
    !!
    class(nx_sh_solver_t), intent(inout) :: this
    !! Current solver.

    integer :: i

    do i=1, size(this%shtraj)
       call this%shtraj(i)% destroy()
    end do

    deallocate(this%shtraj)
    deallocate(this%shprob)
    deallocate(this%rx)
    deallocate(this%incnad, this%incv, this%incnad_i, this%epot_i_all)

    if (this%params%integrator == 2) then
       deallocate(this%cio)
    end if

    deallocate(this%epot_all, this%en_times)
  end subroutine sh_solver_destroy


  subroutine sh_solver_set_data(this, traj)
    !! Set the elements of the solver for the integration.
    !!
    !! The routine will extract information for populating the
    !! ``incv``, ``incnad``, ``cio``,  ``epot_all`` and ``en_times``
    !! members.
    class(nx_sh_solver_t), intent(inout) :: this
    !! Solver object.
    type(nx_traj_t), intent(in) :: traj
    !! Main trajectory object.

    integer :: nstat

    nstat = size(this%epot_all, 2)

    this%incv(:, :) = traj%veloc(:, :) - traj%old_veloc(:, :)
    this%incnad(:, :, :) = traj%nad(:, :, :) - traj%old_nad(:, :, :)
    
    if (this%params%integrator == 2) then
       this%cio = traj%cio(:, :)
    end if

    call reshape_epot(&
         & traj%step, traj%dt, traj%epot, traj%old_epot, nstat, &
         & this%epot_all, this%en_times&
         &)

    if (traj%run_complex) then
       this%incnad_i = traj%nad_i - traj%old_nad_i
       call reshape_epot(&
            & traj%step, traj%dt, traj%gamma, traj%old_gamma, nstat, &
            & this%epot_i_all, this%en_times&
            &)
    end if
  end subroutine sh_solver_set_data


  subroutine sh_solver_run(this, traj, txtout, csfssh)
    !! Main routine for running the integration.
    !!
    !! The routine is actually a loop over ``ms``, where the
    !! following steps take place:
    !!
    !! 0. Preparation of the new integration (reallocation of data,
    !!    reset, ...) ;
    !! 1. Construction of the parameters for the new step ;
    !! 2. Integration and computation of the hopping probabilities ;
    !! 3. Surface hopping algorithm (does it hop or not ?)
    !! 4. Decoherence correction ;
    !!
    !! After all steps have been completed, we rescale the main
    !! velocity if required by surface hopping, and we transfer the
    !! new parameters (new surface, coefficients, ...) to the main
    !! trajectory.
    class(nx_sh_solver_t), target, intent(inout) :: this
    !! Solver object.
    type(nx_traj_t), intent(inout) :: traj
    !! Main trajectory object.
    type(nx_output_t), intent(inout) :: txtout
    !! Logging object.
    type(nx_csfssh_params_t), intent(in), optional :: csfssh
    !! CS-FSSH configuration (if required)

    ! Interpolated quantities
    real(dp), allocatable :: nad_itp(:, :, :), nad_i_itp(:, :, :)
    real(dp), allocatable :: veloc_itp(:, :)

    ! Pointer to the next SH step
    type(nx_sh_traj_t), pointer :: next

    ! Misc. variables
    integer :: i, step
    real(dp) :: relt
    integer :: switch_to
    integer :: nstat, nat
    character(len=128) :: str
    real(dp) :: decotime
    logical :: zdecovlp
    real(dp) :: totpop
    integer :: hop_counter(3)
    ! This one counts the number of hopping, with the same convention
    ! as for traj%nrofhops.

    if (this%run_complex) then
       if (.not. present(csfssh)) then
          call check_error(-1, 101, &
               & 'SH: run_complex requested but `csfssh` object not found !'&
               & )
       end if
    end if
       
    nstat = size(this%shtraj(1)%acoef)
    nat = size(this%shtraj(1)%veloc, 2)

    this%rx(:) = 0.0_dp
    this%shprob(:, :) = 0.0_dp

    ! Size of the history
    allocate(nad_itp, mold=traj%nad)

    if (this%run_complex) then
       allocate(nad_i_itp, mold=traj%nad)
    else
       allocate(nad_i_itp(1, 1, 1))
    end if
    
    allocate(veloc_itp, mold=traj%veloc)
    nad_itp(:, :, :) = traj%old_nad(:, :, :)
    veloc_itp(:, :) = traj%old_veloc(:, :)

    if (this%run_complex) then
       nad_i_itp(:, :, :) = traj%old_nad_i(:, :, :)
    else
       nad_i_itp(:, :, :) = 0.0_dp
    end if
    

    relt = 1.0_dp / this%params%ms

    write(str, '(A, I20)') 'MACRO STEP', traj%step
    call sh_log%log(LOG_INFO, str)

    if (this%step == 0) then
       if (this%params%decohmod == 2) then
          call set_wp(this%shtraj(1), this%params, traj%masses, this&
               &%lvprt, traj%geom)
       end if
    end if

    if ((this%step == 0) .and. (this%params%decohmod == 2)) then
       call evolrwp(this%shtraj(1), this%params, decotime, .true., nx_log&
            &, traj%masses, traj%geom)
    end if

    hop_counter(:) = 0

    SH_LOOP: do step=1, this%params%ms

       this%step = this%step + 1

       zdecovlp = .false.
       if ((this%params%decohmod == 2) .and. &
            & (mod(this%step, this%params%ms * this%params%iatau) == 0)) then
          decotime = this%params%dts * this%params%ms * this%params%iatau
          zdecovlp = .true.
       end if

       write(str, '(A, I20)') 'Micro step', this%step
       call sh_log%log(LOG_INFO, str)


       ! 0. Copy information t-ndt -> t-(n-1)dt, ..., t-2dt -> t-dt
       !    Then, initialize a new object for current step
       ! call prepare_sh_loop(this%shtraj, conf)
       call prepare_sh_loop(this%shtraj, nstat, nat)
       next => this%shtraj(1)

       ! 1. Construct ``next`` with interpolated elements.
       nad_itp(:, :, :) = nad_itp(:, :, :) + relt * this%incnad(:, :, :)

       if (this%run_complex) then
          nad_i_itp(:, :, :) = nad_i_itp(:, :, :) + relt * this%incnad_i(:, :, :)
       end if
       
       veloc_itp(:, :) = veloc_itp(:, :) + relt * this%incv(:, :)
       next%epot(:) = interpolate_epot(this%epot_all, this%en_times, this%params%ms, step)
       call next%build_sigma(nad_itp, nad_i_itp, veloc_itp, this%params%vdoth)
       ! next%acoef(:) = this%shtraj(2)%acoef(:)
       next%veloc(:, :) = veloc_itp(:, :)

       call sh_log%log(LOG_TRIVIA, next%sigma, title='|v.h|')
       if (this%run_complex) then
          call sh_log%log(LOG_TRIVIA, next%sigma_i, title='|v.h| imag')
       end if
       call sh_log%log(LOG_DEBUG, next%epot, title='Interpolated energies')
       if (this%run_complex) then
          next%epot_i = this%compute_gamma(csfssh, traj, next%epot, step)
          call sh_log%log(LOG_DEBUG, next%epot_i, title='Interpolated resonance energies')
       end if

       totpop = sum(abs(next%acoef(:))**2)

       write(str, '(A,F20.12)') &
            & 'Total population (before integration): ', totpop
       call sh_log%log(LOG_DEBUG, str)
       call sh_log%log(LOG_DEBUG, abs(next%acoef)**2, &
            & title='Populations (before integration)')

       ! 2. Integrate the TDSE and get the hopping probabilities
       if (this%params%integrator == 2) then
          call local_diabatization(this%shtraj(1), this%shtraj(2),&
               & this%params%dts , this%cio,&
               & this%shprob(1, :))
       else
          call sh_evolve_and_get_prob(&
               & this%params, this%shtraj, this%shprob(step, :)&
          & )
       end if

       totpop = sum(abs(next%acoef(:))**2)

       write(str, '(A,F20.12)') &
            & 'Total population (after integration): ', totpop
       call sh_log%log(LOG_DEBUG, str)
       call sh_log%log(LOG_DEBUG, abs(next%acoef)**2, &
            & title='Populations (after integration)')

       ! 3. Decide on wether to hop or not
       if (this%params%nohop == 0) then
          ! Hopping allowed, transition are computed

          if ((this%step - this%last_hop) <= this%params%nrelax) then
             this%rx(step) = 1.0_dp
             switch_to = 0
          else
             call will_i_hop(this%shprob(step, :), this%rx(step),&
                  & switch_to)
          end if

          if (this%rx(step) <= this%params%probmin) then
             switch_to = 0
          end if

       else if (this%params%nohop > 0) then
          ! Hopping is forced at a certain time step
          if (this%step == this%params%nohop) then
             this%rx(step) = 0.0_dp
             switch_to = this%params%forcesurf
             print *, 'FORCING HOP'
          else
             this%rx(step) = 1.0_dp
             switch_to = 0
          end if

       else if (this%params%nohop < 0) then
          ! Hopping is never allowed
          this%rx(step) = 1.0_dp
          switch_to = 0
       end if

       if (this%step == this%params%nohop) then
          print *, 'SWITCH_TO = ', switch_to
          print *, 'RX = ', this%rx(step)
       end if

       if (switch_to > 0) then
          write(str, '(A,F10.3,A,I0,A)') &
               & 'Hopping event !! (t = ', traj%t, ' ; microstep = ', step, ')'
          call nx_log%log(LOG_WARN, str)
          write(str, '(A,I0,A,I0)') &
               & 'Attempt to switch from ', next%isurf, ' to ', switch_to

          ! If a switch occurs switch_to is strictly positive
          ! Here we make the final decision on wether to hop or not,
          ! based on kinetic energy
          call sh_hop_and_rescale(&
               & next, this%params, switch_to, traj%masses, traj%old_grad, nad_itp&
               & )

          block
            type(nx_hop_point_t) :: hop
            logical :: frustrated

            frustrated = .false.
            if (next%irk /= 1) frustrated = .true.
            
            hop = nx_hop_point_t(nstat, nat)
            call hop%create(&
                 & step=traj%step, &
                 & ms_step=step, &
                 & time=traj%t, &
                 & old=traj%nstatdyn, &
                 & new=switch_to, &
                 & old_veloc=veloc_itp, &
                 & new_veloc=next%veloc, &
                 & epot=next%epot, &
                 & sh_prob=this%shprob(step, switch_to), &
                 & gen_rx=this%rx(step),&
                 & rejected=frustrated, &
                 & masses=traj%masses&
                 & )
            call txtout%set_txtout_path()
            call hop%write(txtout%txtout%mhopf)
          end block

          ! call sh_write_hopping_info(&
          !      & this, txtout, next%epot(:), &
          !      & veloc_itp, next%veloc, traj%nstatdyn, switch_to, traj%masses&
          !      & )
          veloc_itp(:, :) = next%veloc(:, :)
          
          ! In case of surface hopping we need to rescale the velocities at
          ! times t and t+dt to continue the interpolation. To this end we set
          ! up a "virtual" interpolation (what would it look like if it was
          ! carried out on the new surface). At step i this corresponds to:
          !
          !  oldv = oldv - acc(newsurf) * i * dt / ms
          !  newv = oldv + acc(newsurf) * (ms-i) * dt / ms
          !
          ! where dt is the macroscopic time step. We just need the difference
          ! between these two for the next interpolation, which amounts to:
          !
          !  incv = dt * acc(newsurf)
          !
          ! If the hopping is frustrated, then we need to scale the velocities
          ! by the ``sh%mom`` factor.
          call adjust_vel_increment(&
               & this%incv, next%irk, traj%grad, traj%masses, &
               & switch_to, this%params%mom, traj%dt &
               & )

          ! Report the outcome
          select case(next%irk)
          case(1)
             next%isurf = switch_to
             call nx_log%log(LOG_WARN, '! SURFACE HOPPING OCCURS !')
             this%last_hop = this%step
          case(2)
             call nx_log%log(LOG_WARN, &
                  & '   FRUSTRATED HOPPING (not enough kinetic energy)')
          case(3)
             call nx_log%log(LOG_WARN, &
                  & '   FRUSTRATED HOPPING (velocity condition)')
          end select

          if (next%irk /= 0) then
             hop_counter(next%irk) = hop_counter(next%irk) + 1
          end if
       end if

       ! 4. Decoherence corrections
       if (this%params%decohmod == 1) then
          call decoherence(next, this%params, traj%masses)
       else if (this%params%decohmod == 2) then
          if (zdecovlp) then
             write(str, '(a,i0)') 'Calling evolwrp at step = ', this%step
             call sh_log%log(LOG_DEBUG, str)
             write(str, '(a,i0)') 'decotime = ', decotime
             call sh_log%log(LOG_DEBUG, str)
             call evolrwp(this%shtraj(1), this%params, decotime, .false., sh_log,&
                  & traj%masses, traj%geom)
             call add_prob_wp(this%shtraj(1), sh_log)

             ! Renormalization
             totpop = sum(abs(next%acoef(:))**2)
             next%acoef(:) = next%acoef(:) / sqrt(totpop)
          end if
       end if

       write(str, '(A,F20.12)') &
            & 'Total population (after decoherence): ', totpop
       call sh_log%log(LOG_INFO, str)
       call sh_log%log(LOG_INFO, abs(next%acoef)**2, &
            & title='Populations (after decoherence)')

       if (.not. this%run_complex) then
          if ((totpop < 1.0_dp - this%params%popdev) &
               & .or. (totpop > 1.0_dp + this%params%popdev)) then
             write(str, '(A,F8.6,A,F8.6,A)') &
                  & 'Total population (', totpop, &
                  & ') deviated by more than popdev (popdev = ', &
                  & this%params%popdev, ')'
             call nx_log%log(LOG_ERROR, str)
             ERROR STOP
          end if
       end if

       ! Account for the change of speed and recompute both ``sigma`` if
       ! required and the derivatives
       if (next%irk == 1) then
          call next%build_sigma(&
               & nad_itp, nad_i_itp, next%veloc, this%params%vdoth&
               & )
       end if

       ! 6. Re-compute the derivative including decoherence
       ! corrections, when we actually need them
       if (this%params%integrator /= 2) then
          call next%compute_adot()
       end if

       call sh_log%log_blank(LOG_INFO, n=1)
    end do SH_LOOP

    ! Update the hopping counter
    do i=1, 3
       traj%nrofhops(i) = traj%nrofhops(i) + hop_counter(i)
    end do

    ! End of the trajectory: reset the old velocity, adjust the new
    ! one in case of surface hopping, if it is possible
    ! VEL_RESCALE: if (sh_traj(1)%irk .eq. 1) then
    VEL_RESCALE: if (traj%nstatdyn /= this%shtraj(1)%isurf) then
       write(*, *) 'SURFACE HOPPING OCCURED !'
       write(*, '(A22,I0,A4,I0)') &
            & '   HOPPING FROM STATE ', traj%nstatdyn, &
            & ' TO ', this%shtraj(1)%isurf

       veloc_itp(:, :) = traj%veloc(:, :)
       call rescale_main_velocity(traj, this%params, this%shtraj(1)%isurf)
       ! call io_txt_write_vel_hop(txtout, veloc_itp, traj%veloc, traj%t, traj%step, 0)
       call txtout%write_hop_veloc(veloc_itp, traj%veloc, traj%t, traj%step, 0)
    end if VEL_RESCALE

    ! Now copy information into the main trajectory
    traj%old_nstatdyn = traj%nstatdyn
    traj%nstatdyn = this%shtraj(1)%isurf
    do i=1, size(traj%population)
       traj%population(i) = abs(this%shtraj(1)%acoef(i))**2
       traj%wf(i) = this%shtraj(1)%acoef(i)
    end do

    traj%shprob(:, :) = this%shprob(:, :)
    traj%rx(:) = this%rx(:)

    call sh_log%log_blank(LOG_INFO, n=3)
  end subroutine sh_solver_run

  ! =============================
  ! NON-MEMBER INTERNAL FUNCTIONS
  ! =============================

  function interpolate_epot(epot, times, ms, step) result(res)
    !! Interpolate the potentiale energies.
    !!
    !! The energy is interpolated with a polynomial method, with a
    !! degree depending on the number of energies already available
    !! (we use polynomials with degrees up to 4).
    !!
    !! The ``epot`` and ``times`` inputs should be the one created by
    !! the ``reshape_epot`` routine.
    real(dp), intent(in) :: epot(:, :)
    !! Array containing the potential energies.
    real(dp), intent(in) :: times(:)
    !! Array containing the times corresponding to the potential energies.
    integer, intent(in) :: ms
    !! Total numbe of steps in the SH integration routine.
    integer, intent(in) :: step
    !! Current integration step.

    real(dp), dimension(size(epot, 2)) :: res

    real(dp) :: t, dt, de
    integer :: i, deg

    deg = size(times)
    dt = times(2) - times(1)
    t = times(deg-1) + step*dt / ms
    do i=1, size(epot, 2)
       call polint(times, epot(:, i), deg, t, res(i), de)
    end do
  end function interpolate_epot


  function csfssh_interpolate_gamma(ref_resonance, epot, epot_ref, ress_shift) result(res)
    use mod_numerics, only: uvip3p
    real(dp), intent(in) :: ref_resonance(:, :, :)
    real(dp), intent(in) :: epot(:)
    real(dp), intent(in) :: epot_ref
    real(dp), intent(in) :: ress_shift

    real(dp) :: res( size(epot) )

    real(dp) :: xa( size(ref_resonance, 3) ), ya( size(ref_resonance, 3) )
    real(dp) :: x( size(epot) )
    integer :: n, i
    character(len=MAX_STR_SIZE) :: msg


    x(:) = epot(:) - epot_ref - ress_shift

    n = size(ref_resonance, 3)

    do i=1, size(epot)
       write(msg, '(A,I0,A,F16.10,A,F16.10,A,F16.10)') &
            & 'i = ', i, ' e = ', epot(i), ' e_ref = ', epot_ref, ' ress_e = ', x(i)
       call sh_log%log(LOG_DEBUG, msg)
       ya(:) = ref_resonance(i, 2, :)
       xa(:) = ref_resonance(i, 1, :)
       call uvip3p(3, n, xa, ya, 1, x(i), res(i))
    end do
  end function csfssh_interpolate_gamma


  subroutine sh_evolve_and_get_prob(sh, shtraj, shprob)
    !! Carry out the integrations.
    !!
    !! This routine computes the next sets of phases and
    !! coefficients, and updates the coefficient derivatives. It also
    !! computes the surface hopping probabilities.
    type(nx_sh_t), intent(inout) :: sh
    !! SH configuration object.
    type(nx_sh_traj_t), target, intent(inout) :: shtraj(:)
    !! Array of SH trajectories.
    real(dp), intent(inout) :: shprob(:)
    !! Hopping probabilities.

    type(nx_coeff_t), allocatable, target :: coeff(:)
    type(nx_phase_t), allocatable, target :: phase(:)
    type(nx_shprob_t), allocatable, target :: prob(:)
    type(arr_fgeneric_c), allocatable :: coeffs(:)
    type(arr_fgeneric_r), allocatable :: phases(:), probs(:)

    integer :: ntraj, step, isurf, nstat
    integer :: i, k, l

    character(len=1024) :: rr, im
    complex(dp), allocatable :: hamiltonian(:, :)

    ntraj = size(shtraj)
    step = shtraj(1)%step
    isurf = shtraj(1)%isurf
    nstat = size(shtraj(1)%acoef)
    allocate( coeff(ntraj), coeffs(ntraj) )
    allocate( phase(ntraj), phases(ntraj) )
    allocate( prob(ntraj), probs(ntraj) )

    ! In the first step, don't try to everything, as it won't make sense !
    ! if (shtraj(1)%step == 1) ntraj = 2
    do i=1, ntraj
       call shtraj(i)%split( coeff(i), phase(i), prob(i) )
       phases(i)%f => phase(i)
       coeffs(i)%f => coeff(i)
       probs(i)%f => prob(i)
    end do

    ! 1. Phase evolution
    call finite_element_int(phases, sh%dts, step, approx=.false.)
    shtraj(1)%gamma(:) = phases(1)%f%acoef(:)
    call sh_log%log(LOG_DEBUG, shtraj(1)%gamma, title='New phases')

    ! 2. Coefficient evolution
    coeff(1)%gamma = shtraj(1)%gamma(:)
    select case(sh%integrator)
    case(0)
       call finite_element_int(coeffs, sh%dts, step, approx=.true.)
    case(1)
       call butcher_int(coeffs, sh%dts, step)
    case(3)
       allocate(hamiltonian(nstat, nstat))
       hamiltonian(:, :) = 0.0_dp
       call build_hamiltonian(hamiltonian, coeff(1), coeff(2))
       write(*, *) 'HAMILTONIAN:'
       do k=1, nstat
          do l=1, nstat
             write(rr, '(F20.12)') real(hamiltonian(k, l))
             write(im, '(F20.12)') aimag(hamiltonian(k, l))
             write(*, *) 'real: ', trim(rr)
             write(*, *) 'im:', trim(im)
          end do
       end do
       call upropagator_int(coeffs, hamiltonian, sh%dts, step)
       deallocate(hamiltonian)
    end select

    shtraj(1)%acoef(:) = coeff(1)%acoef(:)
    shtraj(1)%adot(:) = coeff(1)%get_deriv()
    call sh_log%log(LOG_DEBUG, shtraj(1)%acoef, title='New coefficients')
    call sh_log%log(LOG_DEBUG, shtraj(1)%adot, title='New coefficient derivatives')

    ! 3. Get hopping probabilities
    prob(1)%gamma = shtraj(1)%gamma(:)
    prob(1)%atraj = shtraj(1)%acoef(:)
    select case(sh%tully)
    case(0)
       shprob(:) = sh%dts * prob(1)%get_deriv()
    case(1)
       prob(1)%adot(:) = prob(1)%get_deriv()
       call finite_element_int(probs, sh%dts, step, approx=.false.)
       shprob(:) = probs(1)%f%acoef(:)
    end select

    shprob(:) = shprob(:) / abs(shtraj(1)%acoef(isurf))**2
    call sh_log%log(LOG_DEBUG, shprob, title='New probabilities')
    do i=1, nstat
       if (shprob(i) < 0.0_dp) shprob(i) = 0.0_dp
    end do

    do i=1, ntraj
       call coeff(i)%destroy()
       call phase(i)%destroy()
       call prob(i)%destroy()
    end do
  end subroutine sh_evolve_and_get_prob


  subroutine will_i_hop(shprob, rx, switch_to)
    !! Fewest-switch algorithm.
    !!
    !! The routine returns the generated random number.
    !! ``switch_to`` is set to -1 if no hopping occur, and the
    !! corresponding state number if hopping is possible.
    real(dp), intent(in) :: shprob(:)
    !! SH probabilities.
    real(dp), intent(inout) :: rx
    !! Generated random number.
    integer, intent(out) :: switch_to
    !! State to which we can hop (-1 if no hop is possible).

    real(dp) :: bt
    integer :: k

    call nx_random_number(rx)

    bt = 0.0_dp
    switch_to = -1
    do k=1, size(shprob)
       bt = bt + shprob(k)
       if (rx <= bt) then
          switch_to = k
          exit
       end if
    end do
  end subroutine will_i_hop


  subroutine sh_hop_and_rescale(shtraj, sh, switch_to, masses, grad,&
       & nad)
    !! Perform the surface hopping.
    !!
    !! In case a surface hopping has been found possible, this
    !! routine tries to actually hop.
    !!
    !! 1. First, it checks if the system has enough kinetic energy to
    !!    hop.  The kinetic energy has different expression, according to the value of
    !!    ``sh%adjmom``: standard kinetic energy (1), kinetic energy scaled by the number
    !!    of degrees of freedoms (0), or kinetic energy in the adjustment direction (2).
    !!    The adjustment direction itself depends on ``sh%adjmom``: in the momentum
    !!    direction (0 or 1), or somewhere in the \( (g, h) \) plane (\(g\) is the
    !!    gradient difference, and \(h\) is the non adiabatic vector ``nad`` between the
    !!    current and the new state) with an angle given
    !!    by ``sh%adjtheta``.
    !! 2. If the system has enough kinetic energy, the system can hop, and we then
    !!    attempt to conserve the total energy by rescaling the velocity in the direction
    !!    given by ``sh%adjmom`` (see above).  Rescaling in the momentum direction is
    !!    always possible.  However, if we rescale in the \((g, h)\) plane, this can be
    !!    impossible (see the manual for details).
    !!
    !! In any case, the result of this routine is contained in the ``shtraj%irk``
    !! parameter:
    !!
    !! - ``shtraj%irk = 1``: Surface hopping has occured ;
    !! - ``shtraj%irk = 2``: Frustrated hopping (not enough kinetic energy to hop) ;
    !! - ``shtraj%irk = 3``: Frustrated hopping (impossible to rescale the velocity in
    !!   the chosen direction); 
    type(nx_sh_traj_t), intent(inout) :: shtraj
    !! Current step in SH integration.
    type(nx_sh_t), intent(in) :: sh
    !! SH paramters.
    integer, intent(in) :: switch_to
    !! State to which the system attempts to hop.
    real(dp), intent(in) :: masses(:)
    !! Atomic masses.
    real(dp), intent(in) :: grad(:, :, :)
    !! Gradients.
    real(dp), intent(in) :: nad(:, :, :)
    !! Non-adiabatic couplings.

    real(dp) :: ekin, de, alpha, num, den
    real(dp), allocatable :: d(:, :)
    integer :: isurf, i

    isurf = shtraj%isurf
    de = shtraj%epot(isurf) - shtraj%epot(switch_to)

    ! Compute the direction in which the hop will occur
    allocate( d(3, size(masses)) )
    select case(sh%adjmom)
    case(0:1)
       do i=1, size(masses)
          d(:, i) = shtraj%veloc(:, i) * masses(i)
       end do
    case(2)
       d = direction_of_rescale(grad, nad, sh%adjtheta, switch_to, isurf)
    end select

    ! Here we test if a surface hopping is possible, by comparing the kinetic energy of
    ! the system (rescaled or not, combined with the direction of hopping or not) to the
    ! energy difference between the two states.  For simplicity of expression, we will
    ! consider the sign of ``de - ekin``, with ``ekin`` having different definitions
    ! depending of ``adjmom`` (see manual, or ``default.yml``).
    if (sh%adjmom == 2) then
       ! In this case we will need the direction of hopping, as it will be required to
       ! "scale" the kinetic energy.
       num = 0.0_dp
       num = scalar_product(shtraj%veloc, d) ** 2

       den = 0.0_dp
       do i=1, size(masses)
          den = den + norm( d(:, i) )**2 / masses(i)
       end do
       den = 2 * den

       ekin = num / den
    else
       if(shtraj%is_qmmm) then
          !! If it is a QM/MM trajectory and I'm adjusting along the
          !! momentum direction, only QM atoms should be considered!
          ekin = compute_ekin_sub(shtraj%veloc, masses, shtraj%is_qm_atom)
       else
          !! If it is not a QMMM system or if it's a QMMM system but
          !! I'm rescaling on the nonadiabatic coupling vector,
          !! since it is defined on all atoms, I can compute kinetic
          !! energy on the whole system and then let the rescale
          !! routine check if the energy is in the right modes.
          ekin = compute_ekin(shtraj%veloc, masses)
       endif

       ! If required, rescale ekin with the number of degrees of freedoms.
       if (sh%adjmom == 0) then
          if (size(masses) > 2) then
             ekin = ekin / (3 * size(masses) - 6)
          else
             ekin = ekin / (3 * size(masses) - 5)
          end if
       end if
    end if

    if (de + ekin < 0) then
       ! In this case we don't have enough kinetic energy on the
       ! current surface: FRUSTRATED HOPPING
       if( .not. shtraj%is_qmmm ) then
          shtraj%veloc(:, :) = sh%mom * shtraj%veloc(:, :)
       else
          ! In case of QM/MM only QM atoms' velocities are rescaled
          do i=1, size(shtraj%is_qm_atom)
             if(shtraj%is_qm_atom(i)) then
               shtraj%veloc(:,i) = sh%mom * shtraj%veloc(:,i)
             end if
          end do
       end if

       shtraj%irk = 2
    else
       ! Hopping can be allowed in this case.

       if (sh%adjmom == 0 .or. sh%adjmom == 1) then
          ! Rescale the velocities in the momentum direction. This is
          ! always possible.
          alpha = (de / ekin) + 1
          alpha = sh%mom * sqrt(alpha)

          if( .not. shtraj%is_qmmm ) then
             shtraj%veloc(:, :) = alpha * shtraj%veloc(:, :)
          else
           ! In case of QM/MM only QM atoms velocities are rescaled
           do i=1, size(shtraj%is_qm_atom)
              if(shtraj%is_qm_atom(i)) then
                shtraj%veloc(:, i) = alpha * shtraj%veloc(:, i)
              end if
           end do
          end if

          shtraj%irk = 1
       else
          ! Rescale the velocity around a direction in the plane (g,
          ! h). This can be impossible depending on the relative
          ! orientation of the velocity and the vector.
          ! To find the rescaling factor we have to solve the second
          ! order equation: A*alpha**2 - B*alpha - de = 0 with
          !   A = 0.5*sum(masses * |d|)
          !   B = sum(masses * v . d)
          ! where d is the direction vector (usually unitary), given
          ! by
          !   d = sin(adjmom) g / ||g|| + cos(adjmom) * h / ||h||
          ! with g the difference gradient vector and h the NAC vector

          ! In case of QM/MM with explicit coupling we could in
          ! in principle have the nonadiabatic coupling defined on all
          ! atoms; otherwise h(MM) are forced to zero, so it is equivalent
          ! to reduce the system to the only QM atoms. g instead should
          ! be anyway defined on all atoms. We expect that in all cases
          ! both g and h are small on every MM atom.

          call vel_rescale_in_direction(shtraj%veloc, d, sh%mom, masses, de, shtraj%irk)
       end if
    end if
    deallocate(d)

  end subroutine sh_hop_and_rescale


  subroutine rescale_main_velocity(traj, sh, switch_to)
    !! Rescale the main trajectory velocity.
    !!
    !! If a surface hopping has occured, we need to rescale the main
    !! velocity to ensure energy conservation.
    type(nx_traj_t), intent(inout) :: traj
    !! Main trajectory object.
    type(nx_sh_t), intent(in) :: sh
    !! SH configuration.
    integer, intent(in) :: switch_to
    !! State to which the system has hopped.

    real(dp) :: de, ekin, discri
    real(dp), allocatable :: rescale_direction(:, :)
    integer :: i

    allocate(rescale_direction, mold=traj%veloc)

    de = traj%epot(switch_to) - traj%epot(traj%nstatdyn)

    select case (sh%adjmom)
    case(0:1)
       do i=1, 3
          rescale_direction(i, :) = traj%veloc(i, :) * traj%masses(:)
       end do
    case(2)
       rescale_direction = direction_of_rescale(&
            & traj%grad, traj%nad, sh%adjtheta, switch_to, traj%nstatdyn &
            )
    end select

    ekin = compute_ekin(traj%veloc, traj%masses)

    discri = 1.0_dp - de / ekin

    if (discri >= 0) then
       call vel_rescale_in_direction(traj%veloc, rescale_direction, sh%mom,&
            & traj%masses, -de)
    else
       block
         character(len=MAX_STR_SIZE) :: str
         write(str, '(A,F15.9,A,F15.9,A,A,F15.9,A)') &
              & 'SH: de (', de, ') > ekin (', ekin, ') -> rescaling velocity by factor ',&
              & ' (1 - de/ekin)^(1/2) (', discri, ')'
         call nx_log%log(LOG_WARN, str)
       end block
       traj%veloc(:, :) = - traj%veloc(:, :) * sqrt(discri)
    end if
  end subroutine rescale_main_velocity


  subroutine prepare_sh_loop(shtraj, nstat, nat)
    !! Prepare SH loop.
    !!
    !! The routine "propagates" the existing elements of the ``shtraj`` array:
    !!
    !!    shtraj(t) -> shtraj(t-dt)
    !!    shtraj(t-dt) -> shtraj(t-2dt)
    !!    shtraj(t-2dt) -> shtraj(t-3dt)
    !!    ...
    !!
    !! We then reinitialize the first element of the array, and set its
    !! ``isurf`` and ``step`` parameters.
    type(nx_sh_traj_t), intent(inout) :: shtraj(:)
    integer :: nstat, nat

    integer :: ntraj, i

    ntraj = size(shtraj)

    do i=1, ntraj - 1
       shtraj(ntraj - i + 1) = shtraj(ntraj - i)
    end do
    call shtraj(1)%init(nat, nstat, shtraj(2)%run_complex)
    shtraj(1)%step = shtraj(2)%step + 1
    shtraj(1)%isurf = shtraj(2)%isurf
    shtraj(1)%acoef = shtraj(2)%acoef
    shtraj(1)%adot = shtraj(2)%adot
  end subroutine prepare_sh_loop


  subroutine vel_rescale_in_direction(veloc, d, mom, masses,&
       & de, irk)
    !! Compute and adjustment to velocity in the given direction, if
    !! possible.
    !!
    !! The adjusted velocity is returned in ``new_veloc``. The optional
    !! ``irk`` value is either 1 (the rescaling is possible) or 2
    !! (impossible), and is used only during surface hopping (it is
    !! useless in adjusting the classical velocity).
    !! If the rescaling cannot be done (energy conservation cannot be
    !! ensured), then it is simply multiplied by ``mom``, i.e. it is
    !! either kept or reversed.
    real(dp), dimension(:, :), intent(inout) :: veloc
    !! Current nuclear velocity.
    real(dp), dimension(:, :), intent(in) :: d
    !! Direction in which the adjustment is to be carried out.
    integer, intent(in) :: mom
    !! If the rescaling is impossible, indicate if the velocities
    !! will be reversed (-1) or kept (1).
    real(dp), dimension(:), intent(in) :: masses
    !! Nuclear masses.
    real(dp), intent(in) :: de
    !! Potential energy difference between the old and the new surface.
    integer, optional, intent(out) :: irk
    !! Indicate if the hop is allowed (1) or frustrated (3).

    real(dp) :: A, B, delta, alpha, sigm1, sigm2, eps
    integer :: k, i

    A = 0.0_dp
    B = 0.0_dp
    eps = 0.000000001_dp

    do k=1, size(masses)
       do i=1, 3
          A = A + d(i, k)**2 / masses(k)
          B = B + veloc(i, k) * d(i, k)
       end do
    end do

    delta = B**2 + 2.0_dp*A*de

    print *, 'A = ', A
    print *, 'B = ', B
    print *, 'D = ', delta

    if (delta < 0) then
       ! In that case there is no solution, so no hopping can
       ! occur and we repeat the same procedure as for
       ! frustrated hoppings
       veloc = mom * veloc
       if (present(irk)) irk = 3
    else
       ! Here the hopping is allowed and we can rescale the
       ! velocity along the chosen vector.
       sigm1 = (-B + sqrt(delta)) / (A)
       sigm2 = (-B - sqrt(delta)) / (A)
       alpha = min(dabs(sigm1), dabs(sigm2))

       if (abs(alpha - abs(sigm1)) < eps) alpha = sigm1
       if (abs(alpha - abs(sigm2)) < eps) alpha = sigm2
       print *, 'SGM1 = ', sigm1
       print *, 'SGM1 = ', sigm2
       print *, 'ALPH = ', alpha
       ! alpha = sigm1
       ! if ( abs(sigm2) <= abs(sigm1) ) then
       !    alpha = sigm2
       ! end if

       do i=1, 3
          veloc(i, :) = veloc(i, :) + alpha * d(i, :) / masses(:)
       end do

       if (present(irk)) irk = 1
    end if
  end subroutine vel_rescale_in_direction


  function direction_of_rescale(grad, nad, angle, surf1, surf2) result(d)
    !! Construct the vector defining the direction in which to adjust
    !! the momentum.
    !!
    !! Return a unit vector.
    real(dp), dimension(:, :, :) :: grad
    !! Gradient array.
    real(dp), dimension(:, :, :) :: nad
    !! Non-adiabatic couplings array.
    real(dp) :: angle
    !! Angle between ``g`` and ``h``.
    integer :: surf1
    !! State the switch has been predicted to.
    integer :: surf2
    !! old state.
    
    real(dp), dimension(3, size(grad, 3)) :: d
    !! Resulting vector.

    real(dp), dimension(:, :), allocatable :: g, h
    real(dp) :: mu
    integer :: kl

    allocate( g(3, size(grad, 3) ))
    allocate( h(3, size(nad, 3) ))

    mu = angle * deg2rad
    g = (grad(surf1, :, :) - grad(surf2, :, :)) / 2.0_dp
    kl = find_index(surf1, surf2)
    h = nad(kl, :, :)

    if (assert_equal( norm(g), 0.0_dp )) then
       g(:, :) = 0.0_dp
    else
       g = g / norm(g)
    end if
    
    if (assert_equal( norm(h), 0.0_dp )) then
       h(:, :) = 0.0_dp
    else
       h = h / norm(h)
    end if
       
    d(:, :) = sin(mu) * g(:, :) + cos(mu) * h(:, :)
  end function direction_of_rescale


  subroutine adjust_vel_increment(incv, irk, grad, masses, new_surf, mom, dt)
    real(dp), intent(inout) :: incv(:, :)
    integer, intent(in) :: irk
    real(dp), intent(in) :: grad(:, :, :)
    real(dp), intent(in) :: masses(:)
    integer, intent(in) :: new_surf
    integer, intent(in) :: mom
    real(dp), intent(in) :: dt

    real(dp), allocatable :: acc_after_hop(:, :)
    integer :: i

    if (irk == 1) then
       allocate( acc_after_hop, mold=incv )

       do i=1, size(acc_after_hop, 2)
          acc_after_hop(:, i) = -grad(new_surf, :, i) / masses(i)
       end do

       incv(:, :) = dt  * acc_after_hop(:, :)

    else if ((irk == 2) .or. (irk == 3)) then
       ! In this case we have a frustrated hopping, and
       ! possibly we have reverted the velocities.
       incv = mom * incv
    end if
  end subroutine adjust_vel_increment


  subroutine local_diabatization(traj, traj_prev, dt, stmat, b)
    !! Perform a local diabatization to compute the electronic
    !! coefficients.
    !!
    !! This routine is a wrapper around the functions found in
    !! ``mod_sh_locdiab.f08``, and implements the method described in
    !! [Granucci et al., JCP 114 (2001)](https://doi.org/10.1063/1.1376633)
    type(nx_sh_traj_t), intent(inout)  :: traj
    !! SH trajectory at the current integration step.
    type(nx_sh_traj_t), intent(in)  :: traj_prev
    !! Sh trajectory at the step before.
    real(dp), intent(in) :: dt
    !! Integration time step.
    real(dp), dimension(:, :), intent(inout) :: stmat
    !! State overlap matrix.
    real(dp), dimension(:), intent(inout) :: b
    !! Computed transition probabilities.

    real(dp), dimension(:, :), allocatable :: rotr, roti
    real(dp), dimension(:), allocatable :: acoer0,acoei0,acoer1,acoei1
    real(dp), dimension (:), allocatable  :: ecitmp,eciotmp
    real(dp) :: ezero

    integer           :: i,dim

    ! B.D: Allocate memory (same dimensions as ``b``)
    dim = size(b)
    allocate(rotr(dim, dim), roti(dim, dim))
    allocate(acoer0(dim), acoei0(dim), acoer1(dim), acoei1(dim))
    allocate(eciotmp(dim), ecitmp(dim))

    ! write(*,*) 'Starting local diabatization routine ...'
    ezero = 0.0_dp ! maybe a different reference energy should be chosen

    call sh_log%log(LOG_INFO, stmat, &
         & title='S-matrix, read in from run_cioverlap.log')
    ! eq. (15)
    call get_tmat(stmat,dim,sh_log)
    do i = 1, dim
       acoer0(i) = real(traj_prev%acoef(i))
       acoei0(i) = aimag(traj_prev%acoef(i))
    end do
    ! eq. (16)
    call propag_ld(traj%epot, traj_prev%epot, 0.d0, dt, stmat&
         &, rotr, roti, acoer0, acoei0, acoer1, acoei1, dim, sh_log)
    !
    do i = 1, dim
       traj%acoef(i)=cmplx(acoer1(i),acoei1(i),kind=dp)
    end do
    !
    ! eq. (19), the transition probabilitites are computed here while all the variables are still available
    call trans_ld(stmat, rotr, roti, acoer0, acoei0, acoer1, acoei1,&
         & b, traj%isurf, traj%irk, dim, sh_log)
  end subroutine local_diabatization


  subroutine build_hamiltonian(hamiltonian, coeff1, coeff2)
    complex(dp), intent(inout) :: hamiltonian(:, :)
    type(nx_coeff_t), intent(in) :: coeff1
    type(nx_coeff_t), intent(in) :: coeff2

    integer :: k, l, kl, nsurf
    complex(dp) :: sx, sx1

    nsurf = size(hamiltonian, 1)
    do k=1, nsurf
       do l=1, k-1
          kl = find_index(k, l)
          sx = coeff1%sigma(kl) * exp(cmplx(0, -coeff1%gamma(kl), kind=dp))
          sx1 = coeff2%sigma(kl) * exp(cmplx(0, -coeff2%gamma(kl), kind=dp))
          hamiltonian(k, l) = (sx + sx1)
          hamiltonian(l, k) = -conjg(hamiltonian(k, l))
       end do
    end do

  end subroutine build_hamiltonian


  subroutine sh_write_hopping_info(solver, txtout, epot, old_veloc, new_veloc, old_state,&
       & new_state, masses)
    type(nx_sh_solver_t), intent(in) :: solver
    type(nx_output_t), intent(inout) :: txtout
    real(dp), intent(in) :: epot(:)
    real(dp), intent(in) :: old_veloc(:, :)
    real(dp), intent(in) :: new_veloc(:, :)
    integer, intent(in) :: old_state
    integer, intent(in) :: new_state
    real(dp), intent(in) :: masses(:)

    character(len=:), allocatable :: filename
    integer :: u, step, i, j
    real(dp) :: t, de, ekin_before, ekin_after

    call txtout%set_txtout_path()
    filename = txtout%txtout%mhopf
    step = mod(solver%step, solver%params%ms) + 1
    t = solver%step * solver%params%dts * timeunit

    ekin_before = compute_ekin(old_veloc, masses)
    ekin_after = compute_ekin(new_veloc, masses)

    de = epot(new_state) - epot(old_state)

    open(newunit=u, file=filename, action='write', position='append')
    write(u, '(A)') repeat('*', 80)
    write(u, '(A, F20.12, A, A, I0, A)') 'SURFACE HOPPING at t = ', t, ' fs. ', &
         & '(MICRO STEP ', step, ')'
    write(u, '(A,I0)') 'From state: ', old_state
    write(u, '(A,I0)') '  To state: ', new_state
    write(u, '(A)') ''
    write(u, '(A)') 'Velocity before hopping (a.u.):'
    do i=1, size(old_veloc, 2)
       write(u, '(3F20.12)') (old_veloc(j, i), j=1, 3)
    end do
    write(u, '(A)') 'Velocity after hopping (a.u.):'
    do i=1, size(new_veloc, 2)
       write(u, '(3F20.12)') (new_veloc(j, i), j=1, 3)
    end do
    write(u, '(A)') ''
    write(u, '(A)') 'Kinetic energies (a.u.):'
    write(u, '(A,F20.12)') 'ekin(before) = ', ekin_before
    write(u, '(A,F20.12)') ' ekin(after) = ', ekin_after
    write(u, '(A)') ''
    write(u, '(A)') 'Potential energies (a.u.):'
    write(u, '(A,F20.12)') '       epot(before) = ', epot(old_state)
    write(u, '(A,F20.12)') '        epot(after) = ', epot(new_state)
    write(u, '(A,F20.12)') ' de(before - after) = ', de
    write(u, '(A,F20.12,A)') ' de(before - after) = ', de * au2ev, ' eV'
    write(u, '(A)') ''
    write(u, '(A)') 'Total energies (a.u.):'
    write(u, '(A,F20.12)') &
         & '       ETOT(before) = ', epot(old_state) + ekin_before
    write(u, '(A,F20.12)') &
         & '        ETOT(after) = ', epot(new_state) + ekin_after
    write(u, '(A)') ''
    write(u, '(A)') 'Surface hopping parameters:'
    write(u, '(A,F20.12)') 'Computed probability: ', solver%shprob(step, new_state)
    write(u, '(A,F20.12)') '       Random number: ', solver%rx(step)
    write(u, '(A)') repeat('*', 80)
    write(u, '(A)') ''
    close(u)
  end subroutine sh_write_hopping_info


  function sh_compute_gamma(self, csfssh, traj, epot, step) result(res)
    class(nx_sh_solver_t), intent(inout) :: self
    type(nx_csfssh_params_t), intent(in) :: csfssh
    type(nx_traj_t), intent(in) :: traj
    real(dp), intent(in) :: epot(:)
    integer, intent(in) :: step

    real(dp) :: res( size(traj%epot) )

    if (self%run_complex) then
       if (csfssh%gamma_model /= 2) then
          res = interpolate_epot(self%epot_i_all, self%en_times, self%params%ms, step)
       else
          res = csfssh_get_gamma_model_2(self, traj, epot, csfssh%ref_resonance, csfssh%ress_shift, step)
       end if
    end if
  end function sh_compute_gamma
  


  function csfssh_get_gamma_model_2(self, traj, epot, ref_resonance, ress_shift, step) result(res)
    class(nx_sh_solver_t), intent(inout) :: self
    type(nx_traj_t), intent(in) :: traj
    real(dp), intent(in) :: epot(:)
    real(dp), intent(in) :: ref_resonance(:, :, :)
    real(dp), intent(in) :: ress_shift
    integer, intent(in) :: step

    real(dp) :: res( size(traj%epot) )

    real(dp), allocatable :: epot_ref(:, :), epot_ref_for_step(:)
    call reshape_epot(traj%step, traj%dt, [traj%epot_ref], traj%epot_ref_old, 1, epot_ref, self%en_times)
    epot_ref_for_step = interpolate_epot(epot_ref, self%en_times, self%params%ms, step)
    res = csfssh_interpolate_gamma(ref_resonance, epot, epot_ref_for_step(1), ress_shift)
  end function csfssh_get_gamma_model_2
  


end module mod_sh
