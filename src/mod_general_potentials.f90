module mod_general_potentials
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Definition of generic potentials
  !!
  use mod_kinds, only: dp
  implicit none

  private


  type, public, abstract :: nx_potential_t
     real(dp) :: v = 0.0_dp
     real(dp) :: dv = 0.0_dp
   contains
     procedure(compute_potential), deferred, pass :: compute
  end type nx_potential_t
  abstract interface
     subroutine compute_potential(self, r)
       use mod_kinds, only: dp
       import :: nx_potential_t
       class(nx_potential_t), intent(inout) :: self
       real(dp), intent(in) :: r
     end subroutine compute_potential
  end interface


  type, public, extends(nx_potential_t) :: nx_harmonic_pot_t
     real(dp) :: k = 0.0_dp
     real(dp) :: req = 0.0_dp
     real(dp) :: vmin = 0.0_dp
   contains
     procedure :: compute => harmonic_pot_compute
  end type nx_harmonic_pot_t
  interface nx_harmonic_pot_t
     module procedure harmonic_pot_init
  end interface nx_harmonic_pot_t


  type, public, extends(nx_potential_t) :: nx_exponential_pot_t
     real(dp) :: alpha = 0.0_dp
     real(dp) :: d = 0.0_dp
     real(dp) :: v0 = 0.0_dp
   contains
     procedure :: compute => exponential_pot_compute
  end type nx_exponential_pot_t
  interface nx_exponential_pot_t
     module procedure exponential_pot_init
  end interface nx_exponential_pot_t


  type, public, extends(nx_potential_t) :: nx_imag_pot_t
     real(dp) :: gr = 0.0_dp
     real(dp) :: vr = 0.0_dp
     real(dp) :: v0 = 0.0_dp
     real(dp) :: grad_vr = 0.0_dp
     real(dp) :: grad_v0 = 0.0_dp
   contains
     procedure :: compute => imag_pot_compute
  end type nx_imag_pot_t
  interface nx_imag_pot_t
     module procedure imag_pot_init
  end interface nx_imag_pot_t


  type, public, extends(nx_potential_t) :: nx_gaussian_pot_t
     real(dp) :: rc = 0.0_dp
     real(dp) :: vmax = 0.0_dp
     real(dp) :: beta = 0.0_dp
   contains
     procedure :: compute => gaussian_pot_compute
  end type nx_gaussian_pot_t
  interface nx_gaussian_pot_t
     module procedure gaussian_pot_init
  end interface nx_gaussian_pot_t

contains

  function harmonic_pot_init(k, req, vmin) result(res)
    real(dp), intent(in) :: k
    real(dp), intent(in) :: req
    real(dp), intent(in) :: vmin

    type(nx_harmonic_pot_t) :: res

    res%k = k
    res%req = req
    res%vmin = vmin
  end function harmonic_pot_init


  subroutine harmonic_pot_compute(self, r) 
    class(nx_harmonic_pot_t), intent(inout) :: self
    real(dp), intent(in) :: r

    self%v = pot_harmonic(self%k, self%req, self%vmin, r)
    self%dv = pot_harmonic_grad(self%k, self%req, r)
  end subroutine harmonic_pot_compute


  function exponential_pot_init(alpha, d, v0) result(res)
    real(dp), intent(in) :: alpha
    real(dp), intent(in) :: d
    real(dp), intent(in) :: v0

    type(nx_exponential_pot_t) :: res

    res%alpha = alpha
    res%d = d
    res%v0 = v0
  end function exponential_pot_init


  subroutine exponential_pot_compute(self, r) 
    class(nx_exponential_pot_t), intent(inout) :: self
    real(dp), intent(in) :: r

    self%v = pot_exponential(self%alpha, self%v0, self%d, r)
    self%dv = pot_exponential_grad(self%alpha, self%v0, self%d, r)
  end subroutine exponential_pot_compute


  function imag_pot_init(gr) result(res)
    real(dp), intent(in) :: gr

    type(nx_imag_pot_t) :: res

    res%gr = gr
  end function imag_pot_init


  subroutine imag_pot_compute(self, r)
    class(nx_imag_pot_t), intent(inout) :: self
    real(dp), intent(in) :: r

    self%v = pot_model_vi(self%gr, self%vr, self%v0)
    self%dv = pot_model_vi_grad(self%gr, self%vr, self%v0, self%grad_vr, self%grad_v0)
  end subroutine imag_pot_compute


  function gaussian_pot_init(rc, vmax, beta) result(res)
    real(dp), intent(in) :: rc
    real(dp), intent(in) :: vmax
    real(dp), intent(in) :: beta

    type(nx_gaussian_pot_t) :: res

    res%rc = rc
    res%vmax = vmax
    res%beta = beta
  end function gaussian_pot_init


  subroutine gaussian_pot_compute(self, r) 
    class(nx_gaussian_pot_t), intent(inout) :: self
    real(dp), intent(in) :: r

    self%v = pot_gaussian(self%beta, self%rc, self%vmax, r)
    self%dv = pot_gaussian_grad(self%beta, self%rc, self%vmax, r)
  end subroutine gaussian_pot_compute

  ! ========================================================
  ! PRIVATE ROUTINES: EXPRESSION OF THE DIFFERENT POTENTIALS
  ! ========================================================
  function pot_harmonic(k, r1, vr1, r) result(v)
    !! Compute a harmonic potential.
    !!
    !! The potential is computed with the following expression:
    !!
    !! \[ V(r) = k (r - r_1)^2 + V(r_1). \]
    !!
    real(dp) :: k
    !! Harmonic constant.
    real(dp) :: r1
    !! Coordinate for the minimum potential.
    real(dp) :: vr1
    !! Value of the potential at the minimum.
    real(dp) :: r
    !! Position at which the potential will be computed.

    real(dp) :: v

    v = k * (r-r1)**2 + vr1
  end function pot_harmonic

  function pot_harmonic_grad(k, r1, r) result(v)
    !! Compute the derivative of the harmonic potential.
    !!
    !! /[ dV(r) = 2 k (r - r_1). \]
    !!
    real(dp) :: k
    !! Harmonic constant.
    real(dp) :: r1
    !! Coordinate for the minimum potential.
    real(dp) :: r
    !! Position at which the derivative will be computed.

    real(dp) :: v

    v = 2.0_dp * k * (r-r1)
  end function pot_harmonic_grad


  function pot_exponential(alpha, vr0, d, r) result(v)
    real(dp) :: alpha
    real(dp) :: vr0
    real(dp) :: d
    real(dp) :: r

    real(dp) :: v

    v = (vr0 - d) * exp( - alpha*r ) + d
  end function pot_exponential


  function pot_exponential_grad(alpha, vr0, d, r) result(v)
    real(dp) :: alpha
    real(dp) :: vr0
    real(dp) :: d
    real(dp) :: r

    real(dp) :: v

    v = - alpha * (vr0 - d) * exp( - alpha*r )
  end function pot_exponential_grad


  function pot_gaussian(beta, rc, vrc, r) result(v)
    real(dp) :: beta
    real(dp) :: rc
    real(dp) :: vrc
    real(dp) :: r

    real(dp) :: v

    v = vrc * exp( - beta*(r-rc)**2 )
  end function pot_gaussian


  function pot_gaussian_grad(beta, rc, vrc, r) result(v)
    real(dp) :: beta
    real(dp) :: rc
    real(dp) :: vrc
    real(dp) :: r

    real(dp) :: v

    v = - 2.0_dp * beta * (r-rc) * vrc * exp( -beta*(r-rc)**2 )
  end function pot_gaussian_grad


  function pot_model_vi(gr,vr,v0) result(v)
    real(dp) :: gr
    real(dp) :: vr
    real(dp) :: v0

    real(dp) :: v

    v =  max( gr * (vr - v0), 0.0_dp)
  end function pot_model_vi


  function pot_model_vi_grad(gr,vr,v0,grad_vr,grad_v0) result(v)
    real(dp) :: gr
    real(dp) :: vr
    real(dp) :: v0
    real(dp) :: grad_vr
    real(dp) :: grad_v0

    real(dp) :: v

    v = gr * ( grad_vr - grad_v0 )
    if( (vr-v0) < 0.0_dp ) v = 0.0_dp
  end function pot_model_vi_grad


end module mod_general_potentials
