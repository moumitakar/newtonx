! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_decoherence
  !! # Decoherence corrections implementation.
  !!
  !! This module implements several decoherence correction schemes,
  !! namely:
  !!
  !! - Energy-based decoherence from [Granucci and
  !! Persico](https://doi.org/10.1063/1.2715585), *JCP*, **126**
  !! (2007).
  !! - Overlap-based decoherence from [Granucci, Persico and
  !! Zoccante](https://doi.org/10.1063/1.3489004), *JCP*, **133**
  !! (2010).
  !!
  !! ## Energy-based decoherence
  !!
  !! This one is the most simple to use, and only requires calling the
  !! routine ``decoherence`` after the integration of the surface
  !! hopping step.
  !!
  !! ## Orbital-based decoherence
  !!
  !! This method exports 3 routine: ``set_wp``, ``evolrwp`` and
  !! ``add_prob_wp``.
  !!
  !! - ``set_wp`` should be called at the very first SH step, to
  !! initialize some parameters and the arrays used in the subsequent
  !! steps.
  !! - ``evolrwp`` is used to propagate the decoherence scheme.
  !! - ``add_prob_wp`` is used to add the wavepacket.
  use mod_kinds, only: dp
  use mod_sh_t, only: nx_sh_t
  use mod_sh_traj_t, only: nx_sh_traj_t
  use mod_configuration, only: nx_config_t
  use mod_tools, only: &
       & compute_ekin, compute_ekin_sub
  use mod_logger, only: &
       & nx_logger_t, &
       & LOG_INFO, LOG_ERROR
  implicit none
  real(dp), parameter :: soglia_prob = 1.d-9 ,small_pop = 1.d-15
  integer, parameter        :: maxwp = 1000
  real(dp) :: soglia_ov
  real(dp) :: soglia_wp
  real(dp), allocatable, dimension (:)     :: vscal,v2,pop0
  real(dp), allocatable, dimension (:,:,:) :: rwp
  real(dp), allocatable, dimension (:,:)   :: vwp
  real(dp), allocatable, dimension (:,:)   :: awp,prob_wp,ovl_wp
  real(dp), allocatable, dimension (:)     :: sxwp,svwp
  real(dp), allocatable, dimension (:)     :: q,v
  real(dp), allocatable, dimension (:)     :: acoer1,acoei1
  integer, allocatable, dimension (:)            :: numwp
  logical :: debug, debug2
  integer :: n3, nstat_t, istati, numat, iw, iatau
  integer :: decovlwp
  !double precision, dimension (numat*3,nstati,maxwp), save   :: rwp
  !double precision, dimension (numat*3,nstati), save         :: vwp
  !double precision, dimension (maxwp,nstati), save           :: awp
  !double precision, dimension (maxwp,nstati), save           :: prob_wp
  !double precision, dimension (nstati,nstati), save          :: ovl_wp
  !double precision, dimension (numat*3), save                :: sxwp
  !double precision, dimension (numat*3), save                :: svwp
  !integer, dimension (nstati), save                          :: numwp
  !
  public :: decoherence
  public :: set_wp, evolrwp, add_prob_wp
  private
  save
  !
contains

  subroutine decoherence(shtraj, sh, masses)
    !! Energy-based decoherence.
    !!
    !! Method from [Granucci and Persico](https://doi.org/10.1063/1.2715585), *JCP*, **126** (2007).
    !!
    type(nx_sh_traj_t), intent(inout) :: shtraj
    !! Current SH trajectory.
    type(nx_sh_t), intent(in) :: sh
    !! SH parameters.
    real(dp), intent(in) :: masses(:)
    !! Atomic masses.

    real(dp) :: decay_kin, decay_pot, ekin, norm, tau, dfac, norm1
    integer :: i, isurf

    isurf = shtraj%isurf

    ! CS-FSSH: Scaling factor
    norm1 = sum(abs(shtraj%acoef(:))**2)

    if(.not. shtraj%is_qmmm) then
      ekin = compute_ekin(shtraj%veloc, masses)
    else
      ekin = compute_ekin_sub(shtraj%veloc, masses, shtraj%is_qm_atom)
    end if
    decay_kin = sh%decay / ekin
    norm = 0.0_dp

    ! Population decays for non active states
    do i=1, size(shtraj%epot)
       if (i == isurf) cycle ! This case is handled later
       decay_pot = 1.0_dp / abs( shtraj%epot(i)-shtraj%epot(isurf) )
       tau = decay_pot * (1.0_dp + decay_kin)
       dfac = sqrt(exp(-sh%dts / tau))
       shtraj%acoef(i) = shtraj%acoef(i) * dfac
       norm = norm + abs(shtraj%acoef(i))**2
    end do

    ! Population decay for the current active state
    if (shtraj%run_complex) then
       norm = sqrt((norm1 - norm) / abs(shtraj%acoef(isurf))**2)
    else
       norm = sqrt((1.0_dp - norm) / abs(shtraj%acoef(isurf))**2)
    end if
    shtraj%acoef(isurf) = shtraj%acoef(isurf) * norm
  end subroutine decoherence

  !****************************************************************************
  !SET_WP: settaggi iniziali
  !****************************************************************************
  subroutine set_wp(shtraj, sh, masses, lvprt, geom)
    type(nx_sh_traj_t), intent (in)    :: shtraj
    type(nx_sh_t), intent (in) :: sh
    real(dp), intent(in) :: masses(:)
    integer, intent(in) :: lvprt
    real(dp), intent(in) :: geom(:, :)
    real(dp) :: sx,sx2 !,delx ,delp
    integer :: i,j,ii, u, w
    intrinsic  Exp, Sqrt

!   sxwp(ii) = Sqrt(tc%amass(i))/(sx^2*8.0_dp)
!   funzioni gaussiane del tipo:
!   a(stato,pacchetto)*exp^-[(x-Q(t))^2/(4*sx^2/Sqrt(m(i))) -i*P(t)*x]

    sx = sh%decovlp
    iatau = sh%iatau
    numat = size(shtraj%veloc, 2)
    nstat_t = size(shtraj%epot)
    istati  = shtraj%isurf

    open(newunit=u, file='shdecoher', position='append')
    iw = u

    debug = lvprt > 1
    debug2 = lvprt > 2

    n3=numat*3
    allocate(rwp(n3,nstat_t,maxwp),vwp(n3,nstat_t))
    allocate(awp(maxwp,nstat_t),prob_wp(maxwp,nstat_t))
    allocate(ovl_wp(nstat_t,nstat_t))
    allocate(sxwp(n3),svwp(n3),numwp(nstat_t))
    allocate(q(n3),v(n3),acoer1(nstat_t),acoei1(nstat_t))

    ii = 1
    do i=1, numat
       do j=1, 3
          q(ii) = geom(j, i)
          v(ii) = shtraj%veloc(j, i)
          ii = ii + 1
       end do
    end do

    soglia_wp = -Log(Abs(sh%thrwp))
    soglia_ov = Abs(sh%thrwp)

    ii=0
    do i=1,numat
       do j=1,3
          ii = ii+1
          sxwp(ii)=Sqrt(masses(i))
          svwp(ii)=masses(i)**2/sxwp(ii)
       end do
    end do
    sx2=sx**2
    sxwp=sxwp/(sx2*8.0_dp)
    svwp=svwp*sx2/2.0_dp

    allocate(vscal(nstat_t),v2(n3),pop0(nstat_t))

    open(newunit=w,file='decovlwp',form='unformatted',status='unknown')
    decovlwp = w

  end subroutine set_wp
  !****************************************************************************
  !EVOLRWP: sposta i centri dei pacchetti, aggiunge l'ultimo pacchetto
  !****************************************************************************
  subroutine evolrwp(shtraj,sh,time,ini, logger, masses, geom)
    type (nx_sh_traj_t), intent (in)             :: shtraj
    type (nx_sh_t), intent (in)          :: sh
    real(dp), intent (in)                :: time
    logical, intent (in)                       :: ini
    ! integer, intent(in) :: shout
    type(nx_logger_t), intent(inout) :: logger
    real(dp), intent(in) :: masses(:)
    real(dp), intent(in) :: geom(:, :)
    !
    real(dp), dimension (numat*3) :: vv
    real(dp) :: ek,vfac,dum,anorm,delx,delp
    integer :: i,j,ii
    real(dp), allocatable :: v_orig(:), q_orig(:)

    character(len=1024) :: msg, msga(numat)

    acoer1 = real(shtraj%acoef)
    acoei1 = aimag(shtraj%acoef)

    allocate(v_orig(n3), q_orig(n3))
    ii = 1
    do i=1, numat
       do j=1, 3
          q_orig(ii) = geom(j, i)
          v_orig(ii) = shtraj%veloc(j, i)
          ii = ii + 1
       end do
    end do

    if (ini) then
       numwp=0
       rwp=0.0_dp
       prob_wp=0.0_dp
       awp=0.0_dp
       awp(1,1:nstat_t) = 1.0_dp
       call norma(acoer1, acoei1, pop0, anorm, nstat_t)
       call logger%log(LOG_INFO, "--- Overlap driven decoherence ---")

       ! write (shout,"(/a)")"--- Overlap driven decoherence ---"
       ! write (shout,"(a)")"  Delta(X)         Delta(P)     (a.u.)"
       do i=1,numat
          delx = sh%decovlp/Sqrt(Sqrt(masses(i)))
          delp = 0.5_dp/delx
          write (msga(i),"(2D20.10)") delx, delp
       end do
       call logger%log(LOG_INFO, msga, nlines=numat, &
            & title='Delta(X)         Delta(P)     (a.u.)')
       ! write (shout,"(a,d15.6)")"Threshold on wp overlap:",soglia_ov
       write (msg,"(a,d15.6)")"Threshold on wp overlap:",soglia_ov
       call logger%log(LOG_INFO, msg)
       ! write (shout,"(a,i8,a/)")"Ancillary wp created each ",iatau," timesteps"
       write (msg,"(a,i8,a/)")"Ancillary wp created each ",iatau," timesteps"
       call logger%log(LOG_INFO, msg)
       call wrt_wp("w",shtraj, logger)
       return
    else
       call wrt_wp("r",shtraj, logger)
    endif

    ! ii=0
    ! ek=0.0_dp
    ! do i = 1,numat
    !   do j=1,3
    !     ii = ii + 1
    !     v2(ii) = t%v(ii)**2
    !     ek = ek + v2(ii)*tc%amass(i)
    !   enddo
    ! enddo
    ! ek=ek*0.5_dp
    if( .not. shtraj%is_qmmm ) then
      ek = compute_ekin(shtraj%veloc, masses)
    else
      ek = compute_ekin_sub(shtraj%veloc, masses, shtraj%is_qm_atom)
    end if

    do i=1,nstat_t
       if (i == istati) then
          numwp(i) = 1
          prob_wp(1,istati) = acoei1(istati)**2 + acoer1(istati)**2
          awp(1,istati)=1.0_dp
          rwp(1:n3,i,1) = q_orig(1:n3)
          vwp(1:n3,i)   = v_orig(1:n3)
          cycle
       endif
       vfac=(ek+shtraj%epot(istati)-shtraj%epot(i))
       if (vfac > 0.0_dp) then
          if (ek <= 0.0_dp) then
             vfac = 1.0_dp
          else
             vfac = Sqrt(vfac/ek)
          endif
       else
          vfac = 0.0_dp
       endif
       vscal(i)=vfac
       dum=time*vfac
       vv(1:n3)=v_orig(1:n3)*dum
       do j=1,numwp(i)
          rwp(1:n3,i,j)= rwp(1:n3,i,j) + vv(1:n3)
       end do
       if (numwp(i) == maxwp) then
          write(iw,"(//a)")" **** Too many ancillary wavepackets ****"
          write(iw,"(a,i8)")" state =",i
          write(iw,"(a,i8)")" # wp  =",numwp(i)
          write(iw,"(a)")" Increase iatau"
          call logger%log(LOG_ERROR, &
               & 'EVOLRWP: Too many ancillary wavepackets, increase iatau')
          ! call dec_err("EVOLRWP","Too many ancillary wavepackets, increase iatau&
          !      &", shout)
       endif
       numwp(i) = numwp(i) + 1
       rwp(1:n3,i,numwp(i))= q_orig(1:n3)
       vwp(1:n3,i)=v_orig(1:n3)*vfac
       if (debug) then
          write(iw,"(a,i5,a,i5)")' In evolrwp: stato ',i, &
               & ' num. wp:',numwp(i)
          write(iw,"(50d15.6)")rwp(1:n3,i,numwp(i))
       endif
    end do
    q = q_orig
    v = v_orig
  end subroutine evolrwp
  !****************************************************************************
  !WPOV: calcola la matrice degli overlap tra tutti i pacchetti
  !****************************************************************************
  ! subroutine wpov
  !   implicit none
  !   integer :: i,j,nwp,nwpj,k,l
  !   real(dp) :: ovl
  !
  !   ! --- normalizzazione dei pacchetti
  !   do i=1,nstat_t
  !      nwp=numwp(i)-1
  !      if (nwp <= 1) then
  !         cycle
  !      endif
  !      call norm_wp(i,nwp)
  !   end do
  !
  !   ! --- overlaps fuori diagonale dei pacchetti
  !   do i=1,nstat_t
  !      nwp=numwp(i)-1
  !      if (nwp <= 0) then
  !         nwp = 1
  !      endif
  !      do j=1,i-1
  !         nwpj=numwp(i)-1
  !         if (nwpj <= 0) then
  !            nwpj = 1
  !         endif
  !         ovl = 0.0_dp
  !         do k=1,nwp
  !            do l=1,nwpj
  !               ovl = ovl + awp(k,i)*awp(l,j)*gauss_ovl(k,l,i,j)
  !            end do
  !         end do
  !         ovl_wp(i,j) = ovl
  !         ovl_wp(j,i) = ovl
  !      end do
  !      ovl_wp(i,i) = 1.0_dp
  !   end do
  ! end subroutine wpov
  !****************************************************************************
  !NORM_WP: normalizza i pacchetti
  !****************************************************************************
  subroutine norm_wp(ist,nwp)
    implicit none
    integer, intent(in) :: ist,nwp
    integer :: i,j
    real(dp) :: ov, ov_off, ov_dia
    if (nwp == 0) return
    ov=0.0_dp
    do i=1,nwp
       ov_off=0.0_dp
       do j=1,i-1
          ov_off=ov_off+awp(i,ist)*awp(j,ist)*gauss_ovl(i,j,ist,ist)
       end do
       ov_dia=awp(i,ist)*awp(i,ist)
       ov = ov + ov_dia + ov_off*2.0_dp
    end do
    awp(1:nwp,ist)=awp(1:nwp,ist)/Sqrt(ov)

  end subroutine norm_wp
  !****************************************************************************
  !ADD_PROB_WP: agisce sulle probabilita' associate alle singole gaussianoidi,
  !aggiungendo e eliminando pacchetti quando necessario
  !****************************************************************************
  subroutine add_prob_wp(t, logger)
    implicit none
    type (nx_sh_traj_t), intent (inout)   :: t
    type(nx_logger_t), intent(inout) :: logger
    real(dp), dimension(nstat_t)  :: pop1
    real(dp)                      :: anorm, diff, vs, vsi, popfac
    ! real(dp)                      :: ovj, pop_agg, popfaci UNUSED
    real(dp)                      :: pop_agg, popfaci
    ! real(dp)                      :: xprob_wp, probtot, adiff, pop_rem UNUSED
    real(dp)                      :: xprob_wp, adiff, pop_rem
    integer                             :: i,j,jj,kwp,k,kk
    intrinsic  Sqrt, Sum, Abs

    call norma(acoer1, acoei1, pop1, anorm, nstat_t)
    vs = Sum(svwp(1:n3)*v2(1:n3))
    if (debug) then
       write(iw,"(a)")" *********** debug in add_prob_wp *********** "
       write(iw,*)"  starting pop0 =", pop0(1:nstat_t)
       write(iw,*)"  starting pop1 =", pop1(1:nstat_t)
    endif

    do i=1,nstat_t
       ! if (debug) then
       !    write(iw,*) "  ----- stato = ",i
       !    do j=1,numwp(i)
       !    write(iw,"(a)")"i, j, prob_wp(j,i)"
       !    write(iw,"(2i5,d15.6)")i, j, prob_wp(j,i)
       !    write(iw,"(a)")"i, j, awp(j,i)"
       !    write(iw,"(2i5,d15.6)")i, j, awp(j,i)
       !    write(iw,"(a,i5,a,i5)")"rwp of packet ",j," on surface",i
       !    write(iw,"(150d15.6)")rwp(1:n3,i,j)
       !    write(iw,"(a,i5,a,i5)")"rwp-q of packet ",j," on surface",i
       !    write(iw,"(150d15.6)")rwp(1:n3,i,j)-q(1:n3)
       !    end do
       ! endif
       if (i == istati) then
          cycle
       endif
       !-----controllo pop
       if (pop1(i) > soglia_prob) then
          diff = pop1(i) - pop0(i)
          if (debug) then
             write(iw,"(a,I3,d20.10)")" population increment on ", i, diff
          endif
          !-----aggiunta pop nell'ultimo pacchetto
          if (diff > 0.0_dp) then
             if (numwp(i) == 1) then
                diff = pop1(i)
             endif
             prob_wp(numwp(i),i)=diff
             if (debug) then
                write(iw,"(a,i3,d15.8)") "probtot on ",i,Sum(prob_wp(1:numwp(i),i))
             endif
             awp(1:numwp(i),i)= Sqrt(prob_wp(1:numwp(i),i)/pop1(i))
          else
             !-----in caso di diminuzione della popolazione:
             call cut_wp(i,numwp(i))
             if (numwp(i) <= 0) cycle
             adiff = Abs(diff)
             jj = numwp(i)
             do j = jj, 1, -1
                xprob_wp= prob_wp(j,i) - adiff
                if (xprob_wp <= 0.0_dp) then
                   adiff = adiff - prob_wp(j,i)
                   call cut_wp(i,j)
                else
                   prob_wp(j,i) = xprob_wp
                   exit
                endif
             enddo
             !-----controlli vari
             if (debug) then
                write(iw,"(a,i3,d15.8)") "probtot on ",i,Sum(prob_wp(1:numwp(i),i))
             endif
          endif
       else
          if (debug) then
             write (iw,"(a)")"popolazione non sufficente: eliminazione pacchetti"
          endif
          kk = numwp(i)
          do k = 1,kk
             call cut_wp(i,k)
          enddo
          pop1(istati) = pop1(istati)+pop1(i)
          pop1(i) = 0.0_dp
       endif
       !-----check sull'overlap: velocita'
       if (numwp(i) == 0) cycle
       vsi = vs*(1-vscal(i))**2
       if (vsi > soglia_wp ) then
          rwp(1:n3,i,1) = rwp(1:n3,i,numwp(i))
          prob_wp(1:numwp(i),i) = 0.0_dp
          awp(1:numwp(i),i)=0.0_dp
          acoer1(i)=0.0_dp
          acoei1(i)=0.0_dp
          popfac=Sqrt((pop1(istati)+pop1(i))/pop1(istati))
          acoer1(istati)=acoer1(istati)*popfac
          acoei1(istati)=acoei1(istati)*popfac
          pop1(istati) = pop1(istati)+pop1(i)
          pop1(i) = 0.0_dp
          numwp(i) = 0
          if (debug) then
             write(iw,"(a,I3)") " ---- deleting all wavepackets on ",i
          endif
       else
          !-----check sull'overlap: pacchetto per pacchetto
          call elim_wp(i,pop_agg,kwp)
          if (kwp > 0) then
             pop_rem = pop1(i)-pop_agg
             if (debug) then
                write(iw,"(I4,a,I3)")kwp," wavepacket(s) erased on state ",i
                write(iw,"(a,I3,a,d15.8)") "population erased on ",i," ", pop_agg
                write(iw,"(a,I3,a,d15.8)") "population remained on ",i," ",pop_rem
             endif
             if (pop_rem < 0.0_dp) then
                if (Abs(pop_rem) > small_pop) then
                   ! call dec_err("DECOVLP","pop_rem > small_pop", shout)
                   call logger%log(LOG_ERROR, &
                        & 'DECOVLP: pop_rem > small_pop')
                else
                   !-----riscalamento popolazioni e coefficenti
                   popfaci=0.0_dp
                endif
             else
                popfaci=Sqrt((pop_rem)/pop1(i))
             endif
             popfac=Sqrt((pop1(istati)+pop_agg)/pop1(istati))
             if (debug) then
                write(iw,"(a,d20.10)") "popfaci ", popfaci
                write(iw,"(a,d20.10)") "popfac ", popfac
             endif
             acoer1(i)=acoer1(i)*popfaci
             acoei1(i)=acoei1(i)*popfaci
             acoer1(istati)=acoer1(istati)*popfac
             acoei1(istati)=acoei1(istati)*popfac
             pop1(istati) = pop1(istati)+pop_agg
             pop1(i) = pop_rem
          endif
       endif
       !-----normalizzazione pacchetti
       call norm_wp(i, numwp(i))
       if (debug) then
          write(iw,"(a)")"normalizzazione dei pacchetti"
          write(iw,"(a,i3,a,f15.10)")"acoer ",i," ", acoer1(i)
          write(iw,"(a,i3,a,f15.10)")"acoei ",i," ", acoei1(i)
       endif
    end do

    !-----controlli finali vari
    if (debug) then
       do i=1,nstat_t
          write(iw,"(a,I3,a,i5)")"numwp(",i,")", numwp(i)
          ! write(iw,"(a,I3,a,200d15.8)")"prob_wp(",i,")", prob_wp(1:numwp(i),i)
          write(iw,"(a,i3,d15.8)") "probtot finale su ",i,Sum(prob_wp(1:numwp(i),i))
          write(iw,"(a,d15.8)")"epsilon ", pop1(i) - Sum(prob_wp(1:numwp(i),i))
       enddo
    endif

    !-----salvataggio pop1 per il timestep successivo
    pop0(1:nstat_t) = pop1(1:nstat_t)
    if (debug) then
       write (iw,*)'pop1 ', pop1(1:nstat_t)
       write (iw,*)'pop0 ', pop0(1:nstat_t)
    endif
    prob_wp(1,istati)=pop1(istati)
    if (debug) then
       do i=1,nstat_t
          if (i == istati) cycle
          write(iw,"(a,I3)")"awp dei pacchetti per lo stato", i
          write(iw,*) awp(1:numwp(i),i)
          do j=1,numwp(i)
             write(iw,*)"rwp del pacchetto",j," per lo stato",i
             write(iw,'(3f12.6)') rwp(1:n3,i,j)
          end do
       enddo
    endif

    !-----reset t%acoef and final writings
    t%acoef = cmplx(acoer1,acoei1,kind=dp)
    call wrt_wp("w",t, logger)

  end subroutine add_prob_wp

  !****************************************************************************
  !GAUSS_OVL: calcola l'overlap tra due gaussianoidi
  !****************************************************************************
  function gauss_ovl(iwp,jwp,ist1,ist2)
    implicit none
    integer, intent (in)             :: iwp,jwp,ist1,ist2
    real(dp)     :: gauss_ovl
    real(dp), dimension (n3) :: dpos,dvel
    ! integer :: i
    intrinsic Exp, Sum

    if (ist1 == istati) then
       dpos(1:n3) = q(1:n3) - rwp(1:n3,ist2,jwp)
    elseif (ist2 == istati) then
       dpos(1:n3) = q(1:n3) - rwp(1:n3,ist1,iwp)
    else
       dpos(1:n3) = rwp(1:n3,ist1,iwp) - rwp(1:n3,ist2,jwp)
    endif

    dpos = dpos**2
    gauss_ovl = Sum(sxwp(1:n3)*dpos(1:n3))
    if (debug2) then
      write(iw,"(a)")"debug in gauss_ovl"
      write(iw,"(a,40d15.8)")"gauss_ovl1 ",gauss_ovl,dpos
    endif

    dvel = 0.0_dp
    if (ist2 /= ist2) then
       if (ist1 == istati) then
          dvel(1:n3) = v(1:n3) - vwp(1:n3,ist2)
       elseif (ist2 == istati) then
          dvel(1:n3) = v(1:n3) - vwp(1:n3,ist1)
       else
          dvel(1:n3) = vwp(1:n3,ist1) - vwp(1:n3,ist2)
       endif
       dvel = dvel**2
       gauss_ovl = gauss_ovl+Sum(svwp(1:n3)*dvel(1:n3))
    endif
    if (debug2) then
      write(iw,"(a,40d15.8)")"gauss_ovl2 ",gauss_ovl,dvel
    endif

    gauss_ovl = Exp(-gauss_ovl)
    if (debug2) then
      write(iw,"(a,d15.8)")"gauss_ovl3 ",gauss_ovl
    endif
  end function gauss_ovl

  !****************************************************************************
  !ELIM_WP: eliminazione di pacchetti in base all'overlap
  !****************************************************************************
  subroutine elim_wp(ist,pop_wp,kwp)
    implicit none
    integer, intent(in) :: ist
    integer, intent(out) :: kwp
    integer             :: j,jj,k,iist,kk,kj,ki
    real(dp), intent(out) :: pop_wp
    real(dp)    :: ovj
    iist=ist
    jj=0
    pop_wp = 0.0_dp
    kwp = 0
    kk = numwp(ist)
    do j=1,kk
       jj=jj+1
       ovj= gauss_ovl(1,jj,istati,ist)*awp(jj,ist)
       if (debug) then
          write(iw,"(a)")"debug in elim_wp"
          write(iw,"(a)")" stato, wp numbers, overlap, awp(jj,ist)"
          write(iw,"(3i3,2d15.8)")ist, j, jj, ovj, awp(jj,ist)
       endif
       if (ovj < soglia_ov) then
          kwp = kwp + 1
          pop_wp = pop_wp + prob_wp(jj,ist)
          if (debug) then
            write(iw,"(a,i3,d15.8)")"pop erased",jj, pop_wp
          endif
          do k=jj+1,numwp(ist)
             rwp(1:n3,iist,k-1) =  rwp(1:n3,iist,k)
             awp(k-1,ist) = awp(k,ist)
             prob_wp(k-1,ist) = prob_wp(k,ist)
          end do
          numwp(ist) = numwp(ist) - 1
          jj=jj-1
       endif
    end do
    if (kwp > 0) then
      kj=numwp(ist)+1
      ki=numwp(ist)+kwp
      rwp(1:n3,iist,kj:ki) = 0.0_dp
      awp(kj:ki,ist) = 0.0_dp
      prob_wp(kj:ki,ist) = 0.0_dp
    endif

  end subroutine elim_wp
!************************************************************************
!CUT_WP: eliminazione ultimo pacchetto aggiunto
!************************************************************************
  subroutine cut_wp(ist,iwp)
    implicit none
    integer, intent(in) :: ist,iwp
    integer             :: iist
    iist = ist
    rwp(1:n3,iist,iwp) =  0.0_dp
    awp(iwp,ist) = 0.0_dp
    prob_wp(iwp,ist) = 0.0_dp
    numwp(ist) = numwp(ist) - 1
  end subroutine cut_wp

!************************************************************************
!WRT_WP: scrittura informazioni e salvataggio arrays
!************************************************************************
  subroutine wrt_wp(iop,t, logger)
    implicit none
    character(len=1), intent (in)   :: iop
    type (nx_sh_traj_t), intent (in)  :: t
    type(nx_logger_t), intent(inout) :: logger

    integer :: i

    rewind decovlwp
    if (iop == "w") then
       write(iw,"(/a,i8)") " Overlap driven decoherence  NC =",t%step
       write(iw,"(a)")  " State   #of ancillary wp        prob"
       if (t%step == 0) then
          write(iw,"(i4,i10,11x,f15.9)")(i,numwp(i),pop0(i),i=1,nstat_t)
       else
          write(iw,"(i4,i10,11x,f15.9)")(i,numwp(i),Sum(prob_wp(1:numwp(i),i)),i=1,nstat_t)
       endif
       write(decovlwp) rwp,vwp,awp,prob_wp,numwp,pop0
    elseif (iop == "r") then
       read(decovlwp) rwp,vwp,awp,prob_wp,numwp,pop0
    else
       call logger%log(LOG_ERROR, &
            & 'DECOVLP - WRT_WP: iop should be either w or r')
       ! call dec_err("DECOVLP - WRT_WP","iop should be either w or r", shout)
    endif

  end subroutine wrt_wp
!***********************************************************************
!NORMA: valuta la popolazione degli stati dati i coefficienti
!***********************************************************************
  subroutine norma(a,b,p,anorm,nd)
    implicit none
    real(dp), dimension(*), intent(in) :: a,b
    real(dp), dimension(*), intent(inout) :: p
    real(dp), intent(out) :: anorm
    integer, intent(in) :: nd
    integer :: i
    !
    anorm = 0.0_dp
    do i = 1,nd
       p(i) = a(i)**2 + b(i)**2
       anorm = anorm + p(i)
    end do
  end subroutine norma
!***********************************************************************
!DEC_ERR: error termination
!***********************************************************************
  ! subroutine dec_err(string1,string2, shout)
  !   implicit none
  !   character(len=*), intent (in) :: string1
  !   character(len=*), intent (in) :: string2
  !   integer :: shout
  !   integer :: icode
  !   !
  !   icode = 668
  !   write(shout,113) string1,string2
  !   open(unit=24,file="inderr",status="unknown")
  !   write(24,*) icode
  !   STOP  668
  ! 113 format(/,&
  !            "************************************************************",&
  !          /,"ERROR TERMINATION OF ",A,/, A,/,&
  !            "************************************************************",&
  !          /)
  ! end subroutine dec_err
end module mod_decoherence
