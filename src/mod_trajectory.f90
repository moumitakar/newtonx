! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_trajectory
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2021-03-24
  !!
  !! This module handles the classical trajectory in Newton-X, by
  !! defining the ``nx_traj_t`` type used to store and monitor the
  !! state of the system.
  use mod_kinds, only: dp
  use mod_constants, only: &
       & au2ang, proton, timeunit, au2ev, &
       & TRAJ_KILLED_EDRIFT, TRAJ_KILLED_ETOT0, TRAJ_KILLED_EDIFF, &
       & MAX_STR_SIZE
  use mod_logger, only: &
       & nx_log, &
       & LOG_INFO
  use mod_print_utils, only: to_upper
  use mod_tools, only: &
       & norm, compute_ekin, compute_ekin_sub, clear, to_str
  use mod_configuration, only: nx_config_t
  implicit none

  private

  public :: traj_init_txt_files
  public :: traj_check_consistency

  interface traj_t
     !! Constructor for ``nx_traj_t`` objects.
     !!
     !! Usage:
     !!
     !!    type(nx_config_t) :: conf
     !!    type(nx_traj_t) :: traj
     !!    conf = nx_config_t('configuration.inp')
     !!    traj = nx_traj_t(conf)
     !!
     module procedure construct
  end interface traj_t
  public :: traj_t

  type, public :: nx_traj_t
     !! Current state of the trajectory.
     !!
     !! This is the main result object from the dynamics, and contains
     !! everything that will be updated during the computation: geometries,
     !! velocities, electronic populations, etc.

     ! Dynamics features
     integer :: step
     !! Current simulation step.
     real(dp) :: t
     !! Current simulation time (in fs.).
     real(dp) :: dt
     !! Time step (in fs).
     real(dp) :: time_on_traj
     !! Time spent in the same state (in fs.)

     ! Energies
     real(dp) :: ekin
     !! Kinetic energy (in a.u.).
     real(dp) :: etot
     !! Total energy (in a.u.).
     real(dp) :: old_etot
     !! Previous total energy (in a.u.)
     real(dp), allocatable :: epot(:)
     !! Potential energies of all the states.
     real(dp) :: etot0
     !! For consistency checking, total energy at step 0.

     ! We also keep the last 3 epot, which will be useful in interpolation
     ! procedures (see mod_sh)
     real(dp), allocatable :: old_epot(:, :)
     !! Potential energies of the last 3 steps, stored as:
     !!
     !! - ``old_epot(1, :)`` -> t-dt
     !! - ``old_epot(2, :)`` -> t-2*dt
     !! - ``old_epot(3, :)`` -> t-3*dt.
     ! real(dp), allocatable, target :: epot_all(:)
     !! Ensemble of potential energies, with first line corresponding to ``epot``, and
     !! remaining four lines to ``old_epot``.

     ! Wavefunction / population
     real(dp), allocatable :: population(:)
     !! Adiabatic electronic population of each state.
     complex(dp), allocatable :: wf(:)
     !! Wavefunction of each state.
     real(dp), allocatable :: diabatic_pop(:, :)
     !! Diabatic populations.

     ! Trajectory parameters
     character(len=2), allocatable, dimension(:) :: atoms
     !! Name of the atoms in the system.
     real(dp), allocatable, dimension(:) :: masses
     !! Masses of each particle.
     real(dp), allocatable, dimension(:) :: Z
     !! Atomic numbers.
     real(dp), allocatable, dimension(:, :) :: geom
     !! Current geometry (atomic positions).
     real(dp), allocatable, dimension(:, :) :: veloc
     !! Current velocity.
     real(dp), allocatable, dimension(:, :) :: acc
     !! Current acceleration.
     real(dp), allocatable, dimension(:, :) :: old_geom
     !! Geometry at t-dt (atomic position).
     real(dp), allocatable, dimension(:, :) :: old_veloc
     !! Velocity at t-dt.
     real(dp), allocatable, dimension(:, :) :: old_acc
     !! Acceleration at t-dt.
     real(dp), allocatable, dimension(:, :, :) :: grad
     !! Gradients on each electronic state.
     real(dp), allocatable, dimension(:, :, :) :: nad
     !! Non-adiabatic coupling vectors for each couple of state,
     !! stored as:
     !!
     !! - ``nad(1, :, :)`` -> state 2 / state 1
     !! - ``nad(2, :, :)`` -> state 3 / state 1
     !! - ``nad(3, :, :)`` -> state 3 / state 2
     !! - ...
     real(dp), allocatable, dimension(:, :, :) :: old_grad
     !! Gradients at t-dt
     real(dp), allocatable, dimension(:, :, :) :: old_nad
     !! Non-adiabatic coupling vectors at t-dt.
     real(dp), allocatable, dimension(:) :: osc
     !! Atomic numbers.

     ! CS-FSSH parameters
     logical :: run_complex
     !! Indicate if the computation runs with complex surfaces or not.
     real(dp), allocatable, dimension(:, :, :) :: nad_i
     !! (CS-FSSH ONLY) Imaginary part of the 1D non-adiabatic couplings (see
     !! documentation for ``nad``).
     real(dp), allocatable, dimension(:, :, :) :: old_nad_i
     !! (CS-FSSH ONLY) Imaginary part of the 1D non-adiabatic couplings (see
     !! documentation for ``old_nad``).
     real(dp), allocatable, dimension(:) :: gamma
     !! (CS-FSSH ONLY) Imaginary part of potential energy (similar to ``epot``).
     real(dp), allocatable, dimension(:, :) :: old_gamma
     !! (CS-FSSH ONLY) Previous imaginary part of potential energy (similar to
     !! ``old_epot``).
     real(dp) :: epot_ref
     !! Reference potential energy (``gamma_model = 2``).
     real(dp), allocatable :: epot_ref_old(:, :)
     !! Reference potential energy (``gamma_model = 2``).

     logical, allocatable, dimension(:) :: is_qm_atom
     !! For each atom True = QM, False = MM
     logical :: is_qmmm
     !! If all atoms are QM = False, if there is an MM part = True
     integer :: nqm_atoms
     !! Number of QM atoms if the system is QMMM
     real(dp) :: ekin_qm
     !! Kinetic energy for the QM subsystem

     ! Excited state characteristics
     integer :: typeofdyn
     !! Type of dynamics (1, 2, 3 or 4, depending on the relative
     !! position of the current surface).
     integer :: nstatdyn
     !! Active state (i.e. the state the dynamics is currently
     !! taking place)
     integer :: old_nstatdyn
     !! Active state at t-dt.
     integer, allocatable, dimension(:, :) :: couplings
     !! Coupling matrix indicating which couplings are to be
     !! computed.

     ! Surface hopping
     integer, dimension(3) :: nrofhops
     !! Number of surface hoppings events. The array contains 3
     !! elements, with:
     !!
     !! - ``nrofhops(1)``: surface hopping
     !! - ``nrofhops(2)``: rejected hop (kinetic energy)
     !! - ``nrofhops(3)``: rejected hop (velocity condition)

     ! Momentum
     real(dp), dimension(:, :), allocatable :: linmom
     !! Linear Momentum
     real(dp), dimension(:, :), allocatable :: angmom
     !! Angular Momentum
     real(dp), dimension(3) :: slinmom, sangmom
     !! Total Linear and Angular Momentum vector

     ! Surface hopping elements
     logical :: has_sh
     !! Indicate if surface hopping elements are there (non-adiabatic dynamics) or not
     !! (adiabatic dynamics).
     real(dp), dimension(:, :), allocatable :: shprob
     !! SH probabilities computed along the SH trajectory (only for
     !! the current macro-iteration step).
     real(dp), dimension(:), allocatable :: rx
     !! Random number generated along the SH trajectory (same as
     !! for ``shprob``).
     real(dp), dimension(:, :), allocatable :: cio
     !! State overlap matrix, obtained from cioverlap typically (this
     !! is only needed with local diabatization).
     real(dp), dimension(:, :), allocatable :: diaham
     !! Excitonic hamiltonian -- only for exciton dynamics
     real(dp), allocatable, dimension(:) :: diapop
     !! Save the diabatic populations
     real(dp), allocatable, dimension(:) :: diaen
     !! Save the diabatic energies

     ! Adaptive time step
     logical :: is_virtual
     !! Indicates if the trajectory is currently virtual or not.

   contains
     procedure :: destroy
     procedure :: init => initialize
     procedure :: allocate => allocate_memory
     procedure :: print_geom
     procedure :: print_veloc
     procedure :: print_wf
     procedure :: print_epot_all
     procedure :: print_gamma_all
     procedure :: read_gradient
     procedure :: update_pos
     procedure :: update_veloc
     procedure :: compute_energies
     procedure :: compute_momentum
     procedure :: write_coordinates
     procedure :: write_merged_coordinates
     procedure :: update_s_and_t
     procedure :: copy => traj_copy
     procedure :: write_txt_files
     procedure :: check_de_sup_inf => traj_check_de_sup_inf
     procedure :: go_back => traj_go_back
  endtype nx_traj_t


contains

  subroutine traj_check_consistency(traj, edrift, ejump, ediff_gs_es, status, msg)
    type(nx_traj_t), intent(in) :: traj
    real(dp), intent(in) :: edrift
    real(dp), intent(in) :: ejump
    real(dp), intent(in) :: ediff_gs_es
    integer, intent(out) :: status
    character(len=*), intent(out) :: msg

    msg = ''
    if (abs(traj%old_etot - traj%etot) > ejump) then
       write(msg, '(A, F20.12, A)') &
            & 'Energy jump is ', abs(traj%old_etot - traj%etot)*au2ev, ' eV'
       ! call check_error(1, ERR_ETOT_JUMP)
       status = TRAJ_KILLED_ETOT0
    end if

    if (abs(traj%etot0 - traj%etot) > edrift) then
       write(msg, '(A, F20.12, A)') &
            & 'Energy drift is ', abs(traj%etot0 - traj%etot)*au2ev, ' eV'
       ! call check_error(1, ERR_ETOT_DRIFT)
       status = TRAJ_KILLED_EDRIFT
    end if

    if (ediff_gs_es > 0) then
       block
         integer :: i, nstat
         character(len=MAX_STR_SIZE) :: tmp
         nstat = size(traj%epot)
         do i=nstat, 2, -1
            if (abs(traj%epot(i) - traj%epot(1)) < ediff_gs_es) then
               write(tmp, '(A,I0,A,F20.12,A)') &
                   'Epot(', i, ') - Epot(1) = ', &
                   (traj%epot(i) - traj%epot(1))*au2ev, ' eV'
               msg = trim(msg)//NEW_LINE('c')//trim(tmp)
               status = TRAJ_KILLED_EDIFF
            end if
         end do
       end block
    end if
    
  end subroutine traj_check_consistency



  type(nx_traj_t) function construct(nat, nstat, ms, run_complex)
    !! Constructor of the trajectory object.
    !!
    !! The trajectory is initialized directly from the main configuration.
    !! The following parameters are needed:
    !!
    !! - ``nat`` (number of atoms)
    !! - ``init_geom`` (initial geometry file)
    !! - ``init_veloc`` (initial velocity file)
    !! - ``nstat_dyn_init`` (initial state of the dynamics)
    !!
    !! If the file ``wf.inp`` is found in the current directory, the initial
    !! wavefunction will be set accordingly to this file.
    integer :: nat
    integer :: nstat
    integer :: ms
    logical :: run_complex

    construct%t = 0.0_dp
    construct%step = 0.0_dp
    construct%dt = 0.0_dp

    construct%nrofhops(:) = 0

    construct%run_complex = run_complex

    ! Geometry and velocity
    call construct%allocate( nat, nstat, ms, run_complex )

    ! Initial acceleration and gradient set to 0
    construct%geom(:, :) = 0.0_dp
    construct%veloc(:, :) = 0.0_dp
    construct%acc(:, :) = 0.0_dp
    construct%grad(:, :, :) = 0.0_dp

    ! Initial wavefunction
    construct%wf(:) = complex(0.0_dp, 0.0_dp)

    ! Initial population
    construct%population(:) = 0.0_dp
    construct%diabatic_pop(:, :) = 0.0_dp

    ! Initial old_*
    construct%old_geom(:, :) = 0.0_dp
    construct%old_veloc(:, :) = 0.0_dp
    construct%old_acc(:, :) = 0.0_dp
    construct%old_grad(:, :, :) = 0.0_dp

    ! Energies
    construct%epot(:) = 0.0_dp
    construct%old_epot(:, :) = 0.0_dp
    construct%ekin = 0.0_dp
    construct%etot = 0.0_dp
    construct%old_etot = 0.0_dp
    construct%etot0 = 0.0_dp

    ! nstatdyn
    construct%nstatdyn = -1
    construct%old_nstatdyn = -1

    ! NAD
    construct%nad(:, :, :) = 0.0_dp
    construct%old_nad(:, :, :) = 0.0_dp
    construct%couplings(:, :) = -1

    ! CS-FSSH
    construct%nad_i(:, :, :) = 0.0_dp
    construct%gamma(:) = 0.0_dp
    construct%old_nad_i(:, :, :) = 0.0_dp
    construct%old_gamma(:, :) = 0.0_dp
    construct%epot_ref = 0.0_dp
    construct%epot_ref_old(:, :) = 0.0_dp

    ! Momentum
    construct%slinmom(:) = 0.0_dp
    construct%linmom(:, :) = 0.0_dp
    construct%angmom(:, :) = 0.0_dp

    ! QM/MM
    construct%is_qm_atom = .true.
    construct%is_qmmm = .false.

    construct%sangmom(:) = 0.0_dp

    ! SH
    construct%shprob(:, :) = 0.0_dp
    construct%rx(:) = 0.0_dp
    construct%cio(:, :) = 0.0_dp
    construct%diaham(:, :) = 0.0_dp
    construct%diapop(:) = 0.0_dp
    construct%diaen(:) = 0.0_dp

    ! ADT
    construct%is_virtual = .false.
  end function construct


  subroutine initialize(this, conf)
    class(nx_traj_t) :: this
    !! Trajectory object.
    type(nx_config_t) :: conf
    !! General configuration of Newton-X.

    logical :: exst

    ! Initial time and step
    this%t = conf%init_time
    this%step = conf%init_step
    this%dt = conf%dt

    this%has_sh = (conf%thres > 0)

    call read_geometry(this, conf, conf%init_geom)
    call read_velocity(this, conf, conf%init_veloc)

    inquire(file='wf.inp', exist=exst)
    if (exst) then
       call read_wf(this, conf, 'wf.inp')
    else
       this%wf(:) = complex(0.0_dp, 0.0_dp)
       this%wf(conf%nstatdyn) = complex(1.0_dp, 0.0_dp)
    end if

    this%population( conf%nstatdyn ) = 1.0_dp

    this%nstatdyn = conf%nstatdyn
    this%old_nstatdyn = conf%nstatdyn
  end subroutine initialize



  subroutine allocate_memory(this, nat, nstat, ms, run_complex)
    !! Allocate memory.
    !!
    !! The following arrays are allocated:
    !!
    !! - ``masses``
    !! - ``atoms``
    !! - ``Z``
    !! - ``geom`` and ``old_geom``
    !! - ``veloc`` and ``old_veloc``
    !! - ``acc`` and ``old_acc``
    !! - ``grad`` and ``old_grad``
    !! - ``nad`` and ``old_nad``
    !! - ``epot`` and ``old_epot``
    !! - ``wf``
    !! - ``population``
    class(nx_traj_t) :: this
    !! Trajectory object.
    integer :: nat
    integer :: nstat
    integer :: ms
    logical :: run_complex

    integer :: ncoup

    allocate( this%masses( nat ) )
    allocate( this%atoms( nat ) )
    allocate( this%Z( nat ) )

    allocate( this%geom(3, nat) )
    allocate( this%veloc(3, nat) )
    allocate( this%acc(3, nat) )
    allocate( this%old_geom(3, nat) )
    allocate( this%old_veloc(3, nat) )
    allocate( this%old_acc(3, nat) )

    allocate( this%wf(nstat) )

    allocate( this%epot(nstat) )
    allocate( this%old_epot(3, nstat) )
    ! allocate( this%epot_all(4, nstat))
    ! this%epot => this%epot_all(1, :)
    ! this%old_epot => this%epot_all(2:nstat, :)
    allocate( this%population(nstat) )
    allocate( this%diabatic_pop(3, nstat) )

    ncoup = nstat * (nstat - 1) / 2
    allocate( this%nad(ncoup, 3, nat) )
    allocate( this%old_nad(ncoup, 3, nat) )
    allocate( this%couplings(nstat, nstat) )

    if (run_complex) then
       allocate( this%nad_i(ncoup, 3, nat) )
       allocate( this%gamma(nstat) )
       allocate( this%old_nad_i(ncoup, 3, nat) )
       allocate( this%old_gamma(3, nstat) )
       allocate( this%epot_ref_old(3, 1))
    else
       allocate( this%nad_i(1, 1, 1) )
       allocate( this%gamma(1) )
       allocate( this%old_nad_i(1, 1, 1) )
       allocate( this%old_gamma(1, 1) )
       allocate( this%epot_ref_old(1, 1))
    end if

    allocate( this%grad(nstat, 3, nat) )
    allocate( this%old_grad(nstat, 3, nat) )

    allocate( this%osc(nstat - 1))

    allocate( this%linmom(3, nat) )
    allocate( this%angmom(3, nat) )

    allocate( this%is_qm_atom(nat) )

    allocate(this%shprob( ms, nstat ))
    allocate(this%rx( ms ))
    allocate(this%cio( nstat, nstat ))
    allocate(this%diaham( nstat, nstat))
    allocate(this%diapop( nstat ))
    allocate(this%diaen( nstat ))
  endsubroutine allocate_memory


  subroutine destroy(this)
    !! Destructor for the ``nx_traj_t`` type.
    class(nx_traj_t), intent(inout):: this
    !! Trajectory object.

    deallocate( this%masses )
    deallocate( this%atoms )
    deallocate( this%Z )

    deallocate( this%geom )
    deallocate( this%veloc )
    deallocate( this%acc )
    deallocate( this%old_geom )
    deallocate( this%old_veloc )
    deallocate( this%old_acc )

    deallocate( this%wf )

    deallocate( this%epot )
    deallocate( this%old_epot )
    deallocate( this%population )

    deallocate( this%nad )
    deallocate( this%old_nad )
    deallocate( this%couplings )

    deallocate( this%grad )
    deallocate( this%old_grad )

    deallocate( this%old_nad_i )
    deallocate( this%nad_i )
    deallocate( this%gamma )
    deallocate( this%old_gamma )

    deallocate( this%osc )

    deallocate( this%linmom )
    deallocate( this%angmom )

    deallocate( this%is_qm_atom )

    deallocate( this%rx )
    deallocate( this%shprob )
    deallocate( this%cio )
    deallocate( this%diaham )
    deallocate( this%diapop )
    deallocate( this%diaen )
  end subroutine destroy


  subroutine read_geometry(this, conf, geomfile)
    !! Read a geometry into ``nx_traj_t``.
    !!
    !! The subroutine parses ``geomfile`` and populates the
    !! ``atoms``, ``Z``, ``masses`` and ``geom`` members of the
    !! object. It also backs up the current geometry in ``old_geom``.
    class(nx_traj_t) :: this
    !! Trajectory object.
    type(nx_config_t) :: conf
    !! General configuration of Newton-X.
    character(len=*) :: geomfile
    !! Name of the ``geom`` file to read, in Newton-X format.

    integer :: u, i, j

    ! Back up
    this%old_geom = this%geom

    open(newunit=u, file=geomfile, status='old', action='read')
    do i=1, conf%nat
       read(u, *) this%atoms(i), this%Z(i), (this%geom(j, i), j=1, 3), this%masses(i)
       this%masses(i) = this%masses(i) * proton
    end do
    close(u)
  end subroutine read_geometry


  subroutine read_wf(this, conf, wffile)
    !! Read a wavefunction into ``nx_traj_t``.
    !!
    !! The subroutine parses file ``wffile`` and populates the
    !! ``wf`` member of the object.
    class(nx_traj_t) :: this
    !! Trajectory object.
    type(nx_config_t) :: conf
    !! General configuration of Newton-X.
    character(len=*) :: wffile
    !! Name of the ``wf`` file to read, in Newton-X format.

    integer :: u, i
    real(dp), dimension(2) :: rdat

    open(newunit=u, file=wffile, status='old', action='read')
    do i=1, conf%nstat
       read(u, *) rdat(:)
       this%wf(i) = complex(rdat(1), rdat(2))
    end do
    close(u)

  end subroutine read_wf


  subroutine read_velocity(this, conf, velocfile)
    !! Read a velocity into ``nx_traj_t``.
    !!
    !! The subroutine parses the file ``velocfile`` and populates the
    !! ``veloc`` member of the object.
    class(nx_traj_t) :: this
    !! Trajectory object.
    type(nx_config_t) :: conf
    !! General configuration of Newton-X.
    character(len=*) :: velocfile
    !! Name of the ``geom`` file to read, in Newton-X format.

    integer :: u, i, j

    ! Back up
    this%old_veloc = this%veloc

    open(newunit=u, file=velocfile, status='old', action='read')
    do i=1, conf%nat
       read(u, *) (this%veloc(j, i), j=1, 3)
    end do
    close(u)
  end subroutine read_velocity


  subroutine read_gradient(this, conf, gradfile)
    !! Read a gradient into ``nx_traj_t``.
    !!
    !! The subroutine parses the file ``gradfile`` and populates the ``acc``
    !! member of the object, backing up the current value in ``old_acc``.
    class(nx_traj_t) :: this
    !! Trajectory object.
    type(nx_config_t) :: conf
    !! General configuration of Newton-X.
    character(len=*) :: gradfile
    !! Name of the ``grad`` file to read, in Newton-X format.

    integer :: u, i, j

    ! Backup gradient
    this%old_acc = this%acc

    open(newunit=u, file=gradfile, status='old', action='read')
    do i=1, conf%nat
       read(u, *) (this%acc(j, i), j=1, 3)
       this%acc(:, i) = -(1.0_dp / this%masses(i)) * this%acc(:, i)
    end do
    close(u)
  end subroutine read_gradient


  subroutine update_pos(this)
    !! Update the geometry with the Verlet algorithm.
    !!
    !! This corresponds to the first 2 steps of the algorithm:
    !!
    !! - Back up the geometry and velocity to ``old_geom`` and
    !!   ``old_veloc`` respectively.
    !! - Update the position from R(t-dt) to R(t)
    !! - Update the velocities from V(t-dt) to V(t - 0.5*dt).
    class(nx_traj_t) :: this
      !! Trajectory object.

    ! real(dp) :: dt

    ! Backup of the current geometry and velocity
    this%old_geom = this%geom
    this%old_veloc = this%veloc

    ! v(t + 0.5 dt) = v(t) + 0.5 * a(t)
    ! r(t + dt) = r(t) + dt * v(t + 0.5 dt)

    ! dt = this%dt / timeunit
    ! Allows the update of dt by the adaptive time step

    this%veloc = this%veloc + 0.5_dp * this%acc * this%dt
    this%geom = this%geom + this%dt*this%veloc
  end subroutine update_pos


  subroutine update_veloc(this)
    !! Update the velocity with the Verlet algorithm.
    !!
    !! The velocities are updated from V(t+0.5*dt) to V(t + dt).
    class(nx_traj_t) :: this
    !! Trajectory object.

    ! real(dp) :: dt

    ! v(t + dt) = v(t + 0.5 dt) + 0.5 * a(t + dt) * dt

    ! dt = this%dt / timeunit
    ! Allows the update of dt by the adaptive time step

    this%veloc = this%veloc + 0.5_dp * this%acc * this%dt
  end subroutine update_veloc


  subroutine compute_energies(this)
    !! Compute the kinetic and the total energy of the system.
    class(nx_traj_t) :: this
    !! Trajectory object.

    ! Backup
    this%old_etot = this%etot

    ! Ekin = 0.5 * m * v**2
    this%ekin = compute_ekin(this%veloc, this%masses)

    ! In a QM/MM simulation we also want to compute QM kinetic energy
    if(this%is_qmmm) then
      ! Compute the energy for the QM subsystem
      this%ekin_qm = compute_ekin_sub(this%veloc, this%masses, this%is_qm_atom)
    else
      ! If every atom is QM, it is the same of ekin
      this%ekin_qm = this%ekin
    end if

    ! Etot = Ekin + Epot(nstatdyn)
    this%etot = this%ekin + this%epot( this%nstatdyn )
  end subroutine compute_energies


  subroutine compute_momentum(this)
    class(nx_traj_t) :: this

    integer :: i, j
    real(dp), dimension(3) :: vec1, vec2

    logical :: split_components

    do i = 1, size(this%masses)
       do j = 1, 3
          this%linmom(j, i) = this%masses(i) * this%veloc(j, i)
          vec1(j) = this%geom(j, i)
          vec2(j) = this%linmom(j, i)
       end do
       this%angmom(1, i) = vec1(2)*vec2(3) - vec1(3)*vec2(2)
       this%angmom(2, i) = vec1(3)*vec2(1) - vec1(1)*vec2(3)
       this%angmom(3, i) = vec1(1)*vec2(2) - vec1(2)*vec2(1)
    end do

    do j = 1, 3
       this%slinmom(j) = 0.0_dp
       this%sangmom(j) = 0.0_dp
       do i = 1, size(this%masses)
          this%slinmom(j) = this%slinmom(j) + this%linmom(j, i)
          this%sangmom(j) = this%sangmom(j) + this%angmom(j, i)
       end do
    end do

    split_components = .false.
    if (split_components) then
       block
         real(dp), dimension(3) :: P, PCM, Pc
         real(dp), dimension(3) :: AL, ALCM, ALC
         call traj_split_angmom_components(&
              & this%masses, this%t, this%geom, this%veloc, P, AL, PCM, ALCM, Pc, ALC&
              &)
       end block
    end if
  end subroutine compute_momentum


  subroutine traj_split_angmom_components(masses, t, geom, veloc, P, AL, PCM, ALCM, Pc, ALC)
    !! MARIO ROUTINE FOR SPLITTING Angular Momentum Component
    !!
    real(dp), intent(in) :: masses(:)
    real(dp), intent(in) :: geom(:, :)
    real(dp), intent(in) :: veloc(:, :)
    real(dp), intent(in) :: t
    real(dp), intent(inout) :: P(3)
    real(dp), intent(inout) :: AL(3)
    real(dp), intent(inout) :: PCM(3)
    real(dp), intent(inout) :: ALCM(3)
    real(dp), intent(inout) :: Pc(3)
    real(dp), intent(inout) :: ALC(3)

    real(dp), allocatable :: aM(:)
    real(dp), allocatable :: x(:, :), v(:, :)
    real(dp) :: xcm(3), vcm(3), xc(3)
    real(dp) :: aMT
    integer :: n
    
    allocate( aM(size(masses)) )
    allocate( x(size(masses), 3) )
    allocate( v(size(masses), 3) )

    do n = 1, size(masses)
       aM(n) = masses(n)
    end do

    x= RESHAPE(geom, (/size(masses), 3/), order=(/2,1/))
    v= RESHAPE(veloc, (/size(masses), 3/), order=(/2,1/))

    aMT = 0.0_dp
    P(:) = 0.0_dp
    AL(:) = 0.0_dp

    do n = 1, size(masses)
       ! Total Mass
       aMT = aMT + aM(n)

       ! Total Linear Momentum
       P(:) = P(:) + aM(n)*v(n, :)
       ! P(1) = P(1) + aM(n)*v(n,1)
       ! P(2) = P(2) + aM(n)*v(n,2)
       ! P(3) = P(3) + aM(n)*v(n,3)

       ! Total angular momentum
       AL(1)= AL(1) + x(n,2)*aM(n)*v(n,3)-x(n,3)*aM(n)*v(n,2)
       AL(2)= AL(2) + x(n,3)*aM(n)*v(n,1)-x(n,1)*aM(n)*v(n,3)
       AL(3)= AL(3) + x(n,1)*aM(n)*v(n,2)-x(n,2)*aM(n)*v(n,1)
    enddo

    write(81, '(F10.3, 999F20.12)') t, P(1), P(2), P(3)
    write(82, '(F10.3, 999F20.12)') t, AL(1), AL(2), AL(3)

    xcm(:) = 0.0_dp
    vcm(:) = 0.0_dp
    do n = 1, size(masses)
       ! Total position of COM
       xcm(:) = xcm(:) + aM(n)*x(n,:)
       ! xcm(1) = xcm(1) + aM(n)*x(n,1)
       ! xcm(2) = xcm(2) + aM(n)*x(n,2)
       ! xcm(3) = xcm(3) + aM(n)*x(n,3)

       ! Total velocity of COM
       vcm(:) = vcm(:) + aM(n)*v(n,:)
       ! vcm(1) = vcm(1) + aM(n)*v(n,1)
       ! vcm(2) = vcm(2) + aM(n)*v(n,2)
       ! vcm(3) = vcm(3) + aM(n)*v(n,3)
    enddo
    xcm(:)=xcm(:)/aMT
    vcm(:)=vcm(:)/aMT

    ! Total Linear Momentum (another way)
    PCM(:)= aMT*vcm(:)

    ! Angular momentum of the COM
    ALCM(1)= xcm(2)*aMT*vcm(3)-xcm(3)*aMT*vcm(2)
    ALCM(2)= xcm(3)*aMT*vcm(1)-xcm(1)*aMT*vcm(3)
    ALCM(3)= xcm(1)*aMT*vcm(2)-xcm(2)*aMT*vcm(1)

    write(83, '(F10.3, 999F20.12)') t, PCM(1), PCM(2), PCM(3)
    write(84, '(F10.3, 999F20.12)') t, ALCM(1), ALCM(2), ALCM(3)

    do n = 1, size(masses)
       xc(:) = x(n,:) - xcm(:)
       ! xc(1) = x(n,1) - xcm(1)
       ! xc(2) = x(n,2) - xcm(2)
       ! xc(3) = x(n,3) - xcm(3)

       Pc(1) = aM(n)*v(n,1) - aMT*vcm(1)
       Pc(2) = aM(n)*v(n,2) - aMT*vcm(2)
       Pc(3) = aM(n)*v(n,3) - aMT*vcm(3)

       ! Angular momentum about the COM
       ALC(1)= ALC(1) + xc(2)*Pc(3)-xc(3)*Pc(2)
       ALC(2)= ALC(2) + xc(3)*Pc(1)-xc(1)*Pc(3)
       ALC(3)= ALC(3) + xc(1)*Pc(2)-xc(2)*Pc(1)
    enddo

    write(85, '(F10.3, 999F20.12)') t, Pc(1), Pc(2), Pc(3)
    write(86, '(F10.3, 999F20.12)') t, ALC(1), ALC(2), ALC(3)

    deallocate( aM )
    deallocate( x )
    deallocate( v )
  end subroutine traj_split_angmom_components
  



  subroutine print_geom(this, outfile)
    !! Print the current geometry in Newton-X format.
    !!
    !! The geometry is printed in the file ``outfile``.
    class(nx_traj_t), intent(in) :: this
    !! Trajectory object.
    character(len=*), intent(in) :: outfile
    !! File where to print the geometry.

    integer :: i, j, u

    open(newunit=u, file=outfile)
    do i=1, size(this%geom, 2)
       write(u, '(1x,a2,2x,f5.1,3f25.14,f14.8)') this%atoms(i), this&
            &%Z(i), (this%geom(j,i),j=1,3), this%masses(i)/proton
    enddo
    close(u)
  endsubroutine print_geom


  subroutine print_veloc(this, outfile)
    !! Print the current velocity in Newton-X format.
    !!
    !! The velocity is printed in ``outfile`` if present, or else in STDOUT.
    class(nx_traj_t), intent(in) :: this
    !! Trajectory object.
    character(len=*), intent(in), optional :: outfile
    !! File where to print the geometry.

    integer :: i, j, u

    if (present(outfile)) then
       open(newunit=u, file=outfile)
       do i=1, size(this%veloc, 2)
          write(u, '(3f25.14)') (this%veloc(j,i),j=1,3)
       enddo
       close(u)
    else
       do i=1, size(this%veloc, 2)
          write(*, '(3f25.14)') (this%veloc(j,i),j=1,3)
       enddo
    end if
  endsubroutine print_veloc


  subroutine print_wf(this, outfile)
    !! Print the wavefunction in ``outfile``.
    class(nx_traj_t) :: this
    !! Trajectory object.
    character(len=*) :: outfile
    !! File where to print the geometry.

    integer :: i, u

    open(newunit=u, file=outfile)
    do i=1, size(this%wf)
       write(u, '(2f25.14)') real(this%wf(i)), aimag(this%wf(i))
    enddo
    close(u)
  endsubroutine print_wf


  subroutine print_epot_all(this)
    !! Print the potential energies.
    !!
    !! The subroutine prints the current potential energies (``epot``) and
    !! the energies from the last 3 steps (``epot_old``).
    class(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: i

    character(len=20), dimension(4) :: clabels
    real(dp) :: epot_all(size(this%epot), 4)

    epot_all(:, 1) = this%epot(:)
    do i=1, size(this%old_epot, 1)
       epot_all(:, i+1) = this%old_epot(i, :)
    end do

    clabels = [ character(len=20) :: &
         & 'Epot (t+dt)', 'Epot(t)', 'Epot(t-dt)', 'Epot(t-2dt)'&
         &]
    call nx_log%log(LOG_INFO, epot_all, &
         & clabels=clabels, title='Potential energies')
  end subroutine print_epot_all


  subroutine print_gamma_all(this)
    !! Print the potential energies.
    !!
    !! The subroutine prints the current potential energies (``epot``) and
    !! the energies from the last 3 steps (``epot_old``).
    class(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: i

    character(len=20), dimension(4) :: clabels
    real(dp) :: gamma_all(size(this%gamma), 4)

    gamma_all(:, 1) = this%gamma(:)
    do i=1, size(this%old_gamma, 1)
       gamma_all(:, i+1) = this%old_gamma(i, :)
    end do

    clabels = [ character(len=20) :: &
         & 'Gamma (t+dt)', 'Gamma(t)', 'Gamma(t-dt)', 'Gamma(t-2dt)'&
         &]
    call nx_log%log(LOG_INFO, gamma_all, &
         & clabels=clabels, title='Resonance energies')
  end subroutine print_gamma_all


  subroutine traj_check_de_sup_inf(this, thres, msga, status)
    !! Check the energy difference betweent current and adjacent states.
    !!
    !! The routine is used to compute the difference between the current dynamic state
    !! (``nstatdyn``) and the directly adjacent states.  If any of those differences is
    !! superior to the value ``thres``, ``status`` is set to -1.  Else, ``status`` is set
    !! to 0.
    !!
    !! If the array ``msga`` is provided, it will then be filled with messages containing
    !! feedback about what happened (intended to be used with ``nx_logger_t`` objects).
    !! The resulting array will contain 4 lines (last one is blank).
    class(nx_traj_t), intent(in) :: this
    !! Trajectory object.
    real(dp), intent(in) :: thres
    !! Threshold to check against.
    integer, intent(out) :: status
    !! Status code (0 if energy differences are lower than ``thres``, else -1).
    character(len=*), intent(inout), optional :: msga(:)
    !! Array containing information log.
    
    integer :: nstat, nstatdyn
    real(dp)de_sup, de_inf

    nstat = size(this%epot)
    nstatdyn = this%nstatdyn

    if (nstatdyn < nstat) then
       de_sup = abs(&
            & this%epot(nstatdyn) - this%epot(nstatdyn + 1)&
            & )
    else
       de_sup = 0.0_dp
    end if

    if (nstatdyn > 1) then
       de_inf = abs(&
            & this%epot(nstatdyn) - this%epot(nstatdyn - 1)&
            & )
    else
       de_inf = 0.0_dp
    end if

    status = 0
    if ((de_sup > thres) .or. (de_inf > thres)) then
       status = -1

       if (present(msga)) then
          call clear(msga)
          write(msga(1), '(A,F8.3,A)') &
               & ' Energy difference with upper state: ', de_sup*au2ev, ' eV'
          write(msga(2), '(A,F8.3,A)') &
               & ' Energy difference with lower state: ', de_inf*au2ev, ' eV'
          write(msga(3), '(A,F8.3,A)') &
               & '   One value is > to thres = ', thres*au2ev, ' eV'
          msga(4) = ''
       end if
    end if
    
  end subroutine traj_check_de_sup_inf



  subroutine write_coordinates(this, type)
    !! Write the current geometry for external program.
    !!
    !! Depending on the electronic structure program used, the routine will
    !! print a suitable coordinate file. The ``type`` parameter should
    !! correspond to the ``program`` from the main configuration ``nx_config_t``.
    !!
    !! The program supported are:
    !!
    !! - Turbomole
    !! - Columbus
    !! - Gaussian
    !! - Exciton model with mopac (FOMO-CI)
    !! - Exciton model with gaussian (TD-DFT)
    !!
    !! For analytical methods the routine returns without doing anything.
    class(nx_traj_t), intent(in) :: this
    !! Trajectory object.
    character(len=*), intent(in) :: type
    !! Type of file to write.

    select case (type)
    case ("turbomole")
       call tm_print_coord(this)
    case ("gaussian")
       call gau_print_coords(this)
    case ("sbh")
       return
    case ("recohmodel")
       return
    case ("onedim_model")
       return
    case ("columbus")
       call col_print_coords(this)
    case ("tinker_g16mmp")
       call tg16mmp_print_coords(this)
    case ("tinker_mndo")
       call tmndo_print_coords(this)
     case ("exc_mopac")
       call col_print_coords(this)
    case ("exc_gaussian")
       call col_print_coords(this)
     case ("mopac")
       call col_print_coords(this)
    case ("external")
       call external_print_coords(this)
    case("mlatom")
       call mlat_print_coords(this)
    case("orca")
       call orca_print_coords(this)
    end select
  end subroutine write_coordinates


  subroutine write_merged_coordinates(this, type)
    !! Write the coordinates of the overlapping system between \(t-dt\) and \(t\).
    !!
    !! Depending on the electronic structure program used, the routine will
    !! print a suitable coordinate file. The ``type`` parameter should
    !! correspond to the ``program`` from the main configuration ``nx_config_t``.
    !!
    !! The program supported are:
    !!
    !! - Turbomole
    !! - Gaussian
    !!
    !! For analytical methods the routine returns without doing anything.
    class(nx_traj_t), intent(in) :: this
    !! Trajectory object.
    character(len=*), intent(in) :: type
    !! Type of file to write.

    select case (type)
    case ("turbomole")
       call tm_print_merged_coord(this)
    case("gaussian")
       call gau_print_merged_coord(this)
    case("tinker_g16mmp")
       call tg16mmp_print_merged_coord(this)
    case("orca")
       call orca_print_merged_coords(this)
    end select
  end subroutine write_merged_coordinates


  subroutine update_s_and_t(this)
    !! Increment the time and step.
    class(nx_traj_t), intent(inout) :: this

    this%t = this%t + this% dt * timeunit
    this%step = this%step + 1
  end subroutine update_s_and_t


  subroutine traj_copy(this, that)
    !! Copy the current object into a new one.
    !!
    !! The new object will be initialized based on the current one,
    !! so it does not need to be initialized before hand.
    class(nx_traj_t), intent(out) :: this
    !! Original trajectory object.
    type(nx_traj_t), intent(in) :: that
    !! New object.

    integer :: nat, nstat, ms, ncoup

    nat = size(that%masses)
    nstat = size(that%epot)
    ncoup = size(that%nad, 1)
    ms = size(that%rx)

    call this%allocate( nat, nstat, ms, that%run_complex )
    ! this = traj_t(nat, nstat, ms)

    this%t = that%t 
    this%step = that%step 
    this%dt = that%dt
    this%masses = that%masses

    this%nrofhops(:) = that%nrofhops(:) 

    this%geom(:, :) = that%geom(:, :) 
    this%veloc(:, :) = that%veloc(:, :) 
    this%acc(:, :) = that%acc(:, :) 
    this%grad(:, :, :) = that%grad(:, :, :) 

    this%wf(:) = that%wf(:) 

    this%population(:) = that%population(:) 

    this%old_geom(:, :) = that%old_geom(:, :) 
    this%old_veloc(:, :) = that%old_veloc(:, :) 
    this%old_acc(:, :) = that%old_acc(:, :) 
    this%old_grad(:, :, :) = that%old_grad(:, :, :) 

    this%epot(:) = that%epot(:) 
    this%old_epot(:, :) = that%old_epot(:, :) 
    this%ekin = that%ekin 
    this%etot = that%etot 
    this%old_etot = that%old_etot 
    this%etot0 = that%etot0 

    this%nstatdyn = that%nstatdyn 
    this%old_nstatdyn = that%old_nstatdyn 

    this%nad(:, :, :) = that%nad(:, :, :) 
    this%old_nad(:, :, :) = that%old_nad(:, :, :) 
    this%couplings(:, :) = that%couplings(:, :) 

    this%slinmom(:) = that%slinmom(:) 
    this%linmom(:, :) = that%linmom(:, :) 
    this%angmom(:, :) = that%angmom(:, :) 

    this%is_qm_atom = that%is_qm_atom 
    this%is_qmmm = that%is_qmmm 

    this%sangmom(:) = that%sangmom(:) 

    this%shprob(:, :) = that%shprob(:, :) 
    this%rx(:) = that%rx(:) 
    this%cio(:, :) = that%cio(:, :) 
    this%diaham(:, :) = that%diaham(:, :) 
    this%diapop(:) = that%diapop(:) 
    this%diaen(:) = that%diaen(:) 

    this%is_virtual = that%is_virtual 
    ! 1. Allocate the memory for the new object
    ! allocate( that%masses( nat ) )
    ! allocate( that%atoms( nat ) )
    ! allocate( that%Z( nat ) )
    ! allocate( that%geom(3, nat) )
    ! allocate( that%veloc(3, nat) )
    ! allocate( that%acc(3, nat) )
    ! allocate( that%old_geom(3, nat) )
    ! allocate( that%old_veloc(3, nat) )
    ! allocate( that%old_acc(3, nat) )
    ! allocate( that%wf(nstat) )
    ! allocate( that%epot(nstat) )
    ! allocate( that%old_epot(3, nstat) )
    ! allocate( that%population(nstat) )
    ! allocate( that%nad(ncoup, 3, nat) )
    ! allocate( that%old_nad(ncoup, 3, nat) )
    ! allocate( that%grad(nstat, 3, nat) )
    ! allocate( that%old_grad(nstat, 3, nat) )
    ! allocate( that%linmom(3, nat) )
    ! allocate( that%angmom(3, nat) )
    ! allocate( that%is_qm_atom( nat ) )

    ! ! 2. Copy all elements
    ! that%masses(:) = this%masses(:)
    ! that%atoms(:) = this%atoms(:)
    ! that%Z(:) = this%Z(:)
    ! that%geom(:, :) = this%geom(:, :)
    ! that%veloc(:, :) = this%veloc(:, :)
    ! that%acc(:, :) = this%acc(:, :)
    ! that%old_geom(:, :) = this%old_geom(:, :)
    ! that%old_veloc(:, :) = this%old_veloc(:, :)
    ! that%old_acc(:, :) = this%old_acc(:, :)
    ! that%wf(:) = this%wf(:)
    ! that%epot(:) = this%epot(:)
    ! that%old_epot(:, :) = this%old_epot(:, :)
    ! that%population(:) = this%population(:)
    ! that%nad(:, :, :) = this%nad(:, :, :)
    ! that%old_nad(:, :, :) = this%old_nad(:, :, :)
    ! that%grad(:, :, :) = this%grad(:, :, :)
    ! that%old_grad(:, :, :) = this%old_grad(:, :, :)
    ! 
    ! that%linmom(:, :) = this%linmom(:, :)
    ! that%angmom(:, :) = this%angmom(:, :)
    ! that%slinmom(:) = this%slinmom(:)
    ! that%sangmom(:) = this%sangmom(:)
    ! 
    ! that%is_qm_atom(:) = this%is_qm_atom(:)
    ! 
    ! that%t = this%t
    ! that%step = this%step
    ! that%dt = this%dt
  end subroutine traj_copy


  subroutine traj_go_back(self)
    class(nx_traj_t), intent(inout) :: self

    self%geom = self%old_geom
    self%veloc = self%old_veloc
    self%grad = self%old_grad
    self%acc = self%old_acc
    self%nad = self%old_nad
    self%epot(:) = self%old_epot(1, :)
    self%t = self%t - self%dt * timeunit
  end subroutine traj_go_back
  


  ! ------------------------------
  ! Functions for non-HDF5 outputs
  ! ------------------------------
  subroutine traj_init_txt_files()
    !! Initialize the ``txt`` output files.
    !!
    !! The following files will be created and initialized:
    !!
    !! - ``epot.dat``: All potential energies
    !! - ``energies.dat``: Complete energy summary (etot, ekin, epot)
    !! - ``populations.dat``: Electronic population summary.
    !! - ``linmom.dat``: Linear momentum.
    !! - ``angmom.dat``: Angular momentum.
    character(len=12), parameter :: epotf = 'epot.dat'
    character(len=12), parameter :: enf = 'energies.dat'
    character(len=15), parameter :: popf = 'populations.dat'
    character(len=12), parameter :: lmomf = 'linmom.dat'
    character(len=12), parameter :: amomf = 'angmom.dat'

    integer :: u

    open(newunit=u, file=epotf, action='write', status='new')
    write(u, '(A10, A20, A20, A20)') &
         & 'Time', 'State 1', 'State 2', '...'
    close(u)

    open(newunit=u, file=enf, action='write', status='new')
    write(u, '(A10, A10, A20, A20, A20, A20, A20)') &
         & 'Time', 'State', 'Etot', 'Ekin', 'Epot1', 'Epot2', '...'
    close(u)

    open(newunit=u, file=popf, action='write', status='new')
    write(u, '(A10, A20, A20, A20)') &
         & 'Time', 'State 1', 'State 2', '...'
    close(u)

    open(newunit=u, file=lmomf, action='write', status='new')
    write(u, '(A10, A20, A20, A20, A20)') &
         & 'Time', 'Total x', 'Total y', 'Total z', 'Norm'
    close(u)

    open(newunit=u, file=amomf, action='write', status='new')
    write(u, '(A10, A20, A20, A20, A20)') &
         & 'Time', 'Total x', 'Total y', 'Total z', 'Norm'
    close(u)

!!   This write initializes the outfile of r
!!   MARIO ROUTINE FOR SPLITTING Angular Momentum Component
!    write(81, *)'# Total Linear Momentum'
!    write(82, *)'# Total Angular Momentum'
!    write(83, *)'# Total Linear Momentum (another way)'
!    write(84, *)'# Total Angular Momentum of COM'
!    write(85, *)'# Total Linear Momentum about COM'
!    write(86, *)'# Total Angular Momentum about COM'

  end subroutine traj_init_txt_files


  subroutine write_txt_files(traj)
    !! Write txt file output.
    !!
    !! Currently the following data are written to disk:
    !!
    !! - Geometries: ``geometries.xyz``
    !! - Velocities: ``velocities.xyz``
    !! - Potential energies: ``epot.dat``
    !! - Energies: ``energies.dat``
    !! - Populations: ``populations.dat``
    !! - Linear momentum: ``linmom.dat``
    !! - Angular momentum: ``angmom.dat``
    class(nx_traj_t), intent(in) :: traj

    character(len=14), parameter :: geomf = 'geometries.xyz'
    character(len=14), parameter :: velocf = 'velocities.xyz'
    character(len=12), parameter :: epotf = 'epot.dat'
    character(len=12), parameter :: enf = 'energies.dat'
    character(len=15), parameter :: popf = 'populations.dat'
    character(len=12), parameter :: lmomf = 'linmom.dat'
    character(len=12), parameter :: amomf = 'angmom.dat'

    real(dp) :: t
    integer :: u, i, j
    integer :: nstatdyn
    character(len=3) :: at
    character(len=30) :: prtfmt

    t = traj%t
    nstatdyn = traj%nstatdyn

    write(prtfmt, '(A,I0,A)') &
         & '(F10.3,I10,2F20.12,', size(traj%epot), 'F20.12)'

    ! Write geometry
    open(newunit=u, file=geomf, action='write', position='append')
    write(u, '(I0)') size(traj%geom, 2)
    write(u, '(F10.3)') t
    do i=1, size(traj%geom, 2)
       at = to_upper(traj%atoms(i))
       write(u, '(A3, 3F20.12)') &
            & adjustr(at), (traj%geom(j, i), j=1, 3)
    end do
    close(u)

    ! Write velocity
    open(newunit=u, file=velocf, action='write', position='append')
    write(u, '(I0)') size(traj%geom, 2)
    write(u, '(F10.3)') t
    do i=1, size(traj%veloc, 2)
       write(u, '(3F20.12)') &
            & (traj%veloc(j, i), j=1, 3)
    end do
    close(u)

    ! Write potential energies
    open(newunit=u, file=epotf, action='write', position='append')
    write(u, '(F10.3, 999F20.12)') &
         & t, (traj%epot(i), i=1, size(traj%epot))
    close(u)

    ! Write energies
    open(newunit=u, file=enf, action='write', position='append')
    write(u, fmt=prtfmt) &
         & t, traj%nstatdyn, traj%etot, traj%ekin, (traj%epot(i), i=1, size(traj%epot))
    close(u)

    ! Write populations
    open(newunit=u, file=popf, action='write', position='append')
    write(u, '(F10.3, 999F20.12)') &
         & t, (traj%population(i), i=1, size(traj%population))
    close(u)

    ! Write Linear Momentum
    open(newunit=u, file=lmomf, action='write', position='append')
    write(u, '(F10.3, 999F20.12)') &
     &    t, (traj%slinmom(i), i=1, 3), norm(traj%slinmom)
    close(u)

    ! Write Angular Momentum
    open(newunit=u, file=amomf, action='write', position='append')
    write(u, '(F10.3, 999F20.12)') &
     &    t, (traj%sangmom(i), i=1, 3), norm(traj%sangmom)
    close(u)
  end subroutine write_txt_files



  ! -------------------------
  ! SPECIFIC PRIVATE ROUTINES
  ! -------------------------
  subroutine tm_print_merged_coord(this)
    !! Write a TURBOMOLE geometry for the overlapping system.
    !!
    !! The geometry is printed in the file ``merged_coord``.
    type(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: u, i, j

    open(newunit=u, file='merged_coord', status='new', action='write')
    write(u, '(A6)') '$coord'
    do i=1, size(this%geom, 2)
       write(u, '(3f15.9, A4)') (this%geom(j, i), j=1, 3), this%atoms(i)
    end do
    do i=1, size(this%old_geom, 2)
       write(u, '(3f15.9, A4)') (this%old_geom(j, i), j=1, 3), this%atoms(i)
    end do
    write(u,'(A19)') '$user-defined bonds'
    write(u,'(A4)') '$end'
    close(u)
  end subroutine tm_print_merged_coord


  subroutine tm_print_coord(this)
    !! Write the current geometry for TURBOMOLE.
    !!
    !! The geometry file is named ``coord``.
    type(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: i, u, j

    open(newunit=u, file='coord', action='write', status='unknown')
    write(u, '(A6)') '$coord'
    do i=1, size(this%geom, 2)
       write(u, '(3F20.14, A4)') (this%geom(j, i), j=1, 3), this%atoms(i)
    end do
    write(u,'(A19)') '$user-defined bonds'
    write(u,'(A4)') '$end'
    close(u)
  end subroutine tm_print_coord


  subroutine col_print_coords(this)
    !! Write the current geometry for COLUMBUS.
    !!
    !! The geometry file is named ``geom``.
    type(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: i, u, j
    real(dp) :: mass

    ! Now write the updated 'geom' file
    open(newunit=u, file='geom', action='write')
    do i=1, size(this%geom, 2)

       ! Columbus limit in mass format
       mass = this%masses(i) / proton
       if (mass >= 100.0_dp) then
          mass = 99.9_dp
       end if

       write(u, '(1x,a2,2x,f5.1,4f14.8)') &
            & this%atoms(i), this%Z(i), (this%geom(j, i), j=1, 3), &
            & mass
    end do
    close(u)
  end subroutine col_print_coords


  subroutine tg16mmp_print_merged_coord(this)
    !! Write the current geometry in XYZ format for Gaussian.
    !! Also take care that only QM atoms have to be printed
    type(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: i, u, j

    open(newunit=u, file='geom_double', action='write', status='unknown')
    do i=1, size(this%geom, 2)
       if( this%is_qm_atom(i) ) then
          write(u, '(A4, 3F12.8)') this%atoms(i), (this%geom(j, i)*au2ang, j=1, 3)
       end if
    end do
    do i=1, size(this%geom, 2)
       if( this%is_qm_atom(i) ) then
          write(u, '(A4, 3F12.8)') this%atoms(i), (this%old_geom(j, i)*au2ang, j=1, 3)
       end if
    end do
    close(u)
  end subroutine tg16mmp_print_merged_coord


  subroutine gau_print_merged_coord(this)
    !! Write a GAUSSIAN (xyz) geometry for the overlapping system.
    !!
    !! The geometry is printed in the file ``geom_double``.
    type(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: i, u, j

    open(newunit=u, file='geom_double', action='write', status='unknown')
    do i=1, size(this%geom, 2)
       write(u, '(A4, 3F12.8)') this%atoms(i), (this%geom(j, i)*au2ang, j=1, 3)
    end do
    do i=1, size(this%geom, 2)
       write(u, '(A4, 3F12.8)') this%atoms(i), (this%old_geom(j, i)*au2ang, j=1, 3)
    end do
    close(u)
  end subroutine gau_print_merged_coord


  subroutine gau_print_coords(this)
    !! Write the current geometry for GAUSSIAN (xyz).
    !!
    !! The geometry file is named ``geom``.
    type(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: i, u, j

    open(newunit=u, file='geom', action='write', status='unknown')
    do i=1, size(this%geom, 2)
       write(u, '(A4, 3F12.8)') this%atoms(i), (this%geom(j, i)*au2ang, j=1, 3)
    end do
    close(u)
  end subroutine gau_print_coords


  subroutine external_print_coords(this)
    type(nx_traj_t), intent(in) :: this
     !! Trajectory object.

     integer :: i, u, j, n_atoms

     n_atoms = size(this%geom, 2)

     open(newunit=u, file='xyz.dat', action='write', status='unknown')
     rewind u
     write(u, *) n_atoms
     write(u, *)
     do i=1, n_atoms
       write(u, '(A4, 3F12.8)') this%atoms(i), (this%geom(j, i), j=1, 3)
     end do
    close(u)

  end subroutine external_print_coords 


  subroutine tmndo_print_coords(this)
    type(nx_traj_t), intent(in) :: this

    integer :: i, j, u

    open(newunit=u, file='geom', action='write')
    do i=1, size(this%geom, 2)
       write(u, '(3f13.6)') (this%geom(j, i), j=1,3)
    end do
    close(u)

  end subroutine tmndo_print_coords

  subroutine tg16mmp_print_coords(this)
    type(nx_traj_t), intent(in) :: this

    integer :: i, j, u

    open(newunit=u, file='geom', action='write')
    do i=1, size(this%geom, 2)
       write(u, '(3f13.6)') (this%geom(j, i), j=1,3)
    end do
    close(u)

  end subroutine tg16mmp_print_coords


  subroutine mlat_print_coords(this)
    type(nx_traj_t), intent(in) :: this

    integer :: u, n_atoms, i, j

    n_atoms = size(this%geom, 2)

    open(newunit=u, file='xyz.dat', action='write', status='unknown')
    write(u, *) n_atoms
    write(u, *)
    do i=1, n_atoms
       write(u, '(A4, 3F20.8)') this%atoms(i), (this%geom(j, i), j=1, 3)
    end do
    close(u)

    open(newunit=u, file='x.dat', action='write')
    do i=1, n_atoms
       write(u, '(A)',advance='no') ' '//to_str(this%geom(1, i), fmt='(F20.8)')
    end do
    write(u, '(A)') NEW_LINE('c')
    close(u)
  end subroutine mlat_print_coords


  subroutine orca_print_coords(this)
    !! Write the current geometry for GAUSSIAN (xyz).
    !!
    !! The geometry file is named ``geom``.
    type(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: i, u, j

    open(newunit=u, file='orca.xyz', action='write', status='unknown')
    write(u, '(I0)') size(this%geom, 2)
    write(u, '(A)') ''
    do i=1, size(this%geom, 2)
       write(u, '(A4, 3F12.8)') this%atoms(i), (this%geom(j, i)*au2ang, j=1, 3)
    end do
    close(u)
  end subroutine orca_print_coords

  
  subroutine orca_print_merged_coords(this)
    !! Write the current geometry for GAUSSIAN (xyz).
    !!
    !! The geometry file is named ``geom``.
    type(nx_traj_t), intent(in) :: this
    !! Trajectory object.

    integer :: i, u, j

    open(newunit=u, file='sorca.xyz', action='write', status='unknown')
    write(u, '(I0)') 2*size(this%geom, 2)
    write(u, '(A)') ''
    do i=1, size(this%geom, 2)
       write(u, '(A4, 3F12.8)') this%atoms(i), (this%geom(j, i)*au2ang, j=1, 3)
    end do
    do i=1, size(this%geom, 2)
       write(u, '(A4, 3F12.8)') this%atoms(i), (this%old_geom(j, i)*au2ang, j=1, 3)
    end do
    close(u)
  end subroutine orca_print_merged_coords

endmodule mod_trajectory
