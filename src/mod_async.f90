module mod_async
  !! From a list of jobs and a number of processors to use, fork and wait accordingly
  
  use mod_interface, only: &
       & getpid, fork, wait
  use mod_tools, only: to_str
  
  implicit none

  private

  public :: run_async

contains

  subroutine run_async(job_list, nprocs, ierr)
    character(len=*), intent(in) :: job_list(:)
    integer, intent(in) :: nprocs
    integer, intent(out) :: ierr

    integer :: njobs, nbatches, remainder
    integer :: n

    njobs = size(job_list)
    nbatches = int( njobs / nprocs )
    remainder = mod( njobs, nprocs )

    ! print *, 'NJOBS     = ', njobs
    ! print *, 'NPROCS    = ', nprocs
    ! print *, 'NBATCHES  = ', nbatches
    ! print *, 'REMAINDER = ', remainder

    do n=1, nbatches
       call async_divide(job_list, nprocs, n, ierr)
    end do

    if (remainder /= 0) then
       call async_divide(job_list, remainder, nbatches + 1, ierr)
    end if
  end subroutine run_async


  subroutine async_divide(job_list, njobs, ibatch, ierr)
    character(len=*), intent(in) :: job_list(:)
    integer, intent(in) :: njobs
    integer, intent(in) :: ibatch
    integer, intent(out) :: ierr

    integer :: parent_id, my_id, pid
    integer :: index, tmp, rv, n
    integer :: pid_list(njobs)
    logical :: ok
    character(len=:), allocatable :: cur_command

    parent_id = getpid()

    do n=1, njobs

       ! The loop should only be executed by the parent process, not by any of the
       ! children !
       my_id = getpid()
       if (my_id == parent_id) then

          index = n + ((ibatch - 1) * njobs)

          pid = fork()

          if (pid == 0) then
             ! print *, 'index = '//to_str(index)//': Executing command: '//trim(job_list(index))
             call execute_command_line(job_list(index))
             stop
             
          else if (pid < 0) then
             ierr = -1
             print *, 'Cannot run '//trim(job_list(index))//' with pid '//to_str(pid)
             call perror('fork: ')

          else
             pid_list(n) = pid
          end if
       end if
    end do

    do while (.true.)
       pid = wait(rv)

       if (pid == -1) exit

       ok = .false.
       do n=1, njobs
          if (pid == pid_list(n)) then
             ! ok = .true.
             index = n + ((ibatch - 1) * njobs)
             cur_command = trim(job_list(index))
          end if
       end do

       print '(I0,A,I0)', &
            & pid, '('//cur_command//') finished with code ', rv
    end do
  end subroutine async_divide
  

end module mod_async
