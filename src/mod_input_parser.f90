module mod_input_parser
  !! Newton-X input parser.
  !!
  !! The input for Newton-X is intended as a single file, containing an arbitrary number
  !! of namelists.  The file can thus be of the form:
  !!
  !!     &nxconfig
  !!       nat = 10 ! Number of atoms
  !!       progname = 'columbus' ! Name of the program
  !!       methodname = 'mcscf' ! Name of the method used
  !!       nstat = 2 ! Number of states included
  !!       nstatdyn = 2 ! Start on which to start the dynamics
  !!     /
  !!
  !!     &sh
  !!       seed = -1
  !!     /
  !!
  !! Only the ``nxconfig`` part is strictly required (all other parts can have relatively
  !! sane default values).
  !!
  !! We adopt the following convention:
  !!
  !! - Each line ``key = value ! Comment`` will be called a ``pair`` ;
  !! - Each part of the configuration file enclosed between a name starting with ``&``
  !!   and ending with ``/`` is called a ``section``.
  !!
  !! All lines not included in a section are considered as comments, and will not be
  !! processed by this parser.  Inside a section, lines that have a ``!`` as the first
  !! non blank character will also be considered as comments.
  !!
  !! The parsing of the input file is done by the ``parser_t`` object.  All sections are
  !! stored as a linked list of ``section_t`` type, where each element contains itself a
  !! linked-list of ``pair_t`` objects holding the various parameters.
  !!
  !! The values are always stored as characters.
  use mod_kinds, only: dp
  use mod_constants, only: MAX_STR_SIZE
  use mod_print_utils, only: to_lower
  use mod_tools, only: to_str
  use iso_fortran_env, only: stdout => output_unit

  implicit none

  private

  character(len=17), parameter :: UNDEF = 'UNDEFINED_ELEMENT'

  public :: set_config
  interface set_config
     module procedure set_config_bool
     module procedure set_config_int
     module procedure set_config_r64
     module procedure set_config_str
     module procedure set_config_complex
  end interface set_config
  public :: set_config_realloc

  public :: set_config_with_alloc
  interface set_config_with_alloc
     module procedure set_config_with_alloc_int_1
     module procedure set_config_with_alloc_real_1
  end interface set_config_with_alloc


  type pair_t
     !! Linked-list type holding Key / Value / Comment element.
     !!
     !!
     !! The typical use case for this type is to store namelists lines of the form
     !!
     !!     key = value ! Comment
     !!
     character(len=:), allocatable :: key
     character(len=:), allocatable :: val
     character(len=:), allocatable :: comm
     type(pair_t), pointer :: next => null()
   contains
     procedure :: put => pair_put
     procedure :: get => pair_get
     procedure :: print => pair_print
     procedure :: clean => pair_clean
     procedure :: count => pair_count
  end type pair_t

  type section_t
     !! Linked-list type holding sections of the namelist.
     !!
     character(len=:), allocatable :: name
     !! Name of the section (starts with ``&`` in the namelist).
     type(pair_t) :: keyval
     !! Linked-list of key / value pairs.
     type(section_t), pointer :: next => null()
   contains
     procedure :: put => section_put
     procedure :: get => section_get
     procedure :: print => section_print
     procedure :: clean => section_clean
     procedure :: count => section_count_sections
  end type section_t

  type, public :: parser_t
     !! Input parser.
     character(len=:), allocatable :: filename
     !! Name of the input file.
     type(section_t) :: sections
     !! Linked-list of namelist sections.
     integer :: fileu = 0
     !! File unit.
     integer :: status = 0
     character(len=2048) :: errmsg = ''
   contains
     procedure :: init => parser_initialize
     procedure :: parse => parser_parse_file
     procedure :: get_keys => parser_get_keys
     procedure :: extract => parser_extract_section
     procedure :: clean => parser_clean
     procedure :: get => parser_get
     procedure :: set => parser_set
     procedure :: has_section => parser_has_section
     procedure :: has_key => parser_has_key
     procedure :: print => parser_print
  end type parser_t


contains

  ! ===============
  ! PAIR_T ROUTINES
  ! ===============
  recursive subroutine pair_put(self, key, val, com)
    class(pair_t), intent(inout) :: self
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: val
    character(len=*), intent(in) :: com

    if (allocated(self%key)) then
       if (self%key /= key) then
          if (.not. associated(self%next)) allocate(self%next)
          call pair_put(self%next, key, val, com)
       else
          self%key = key
          self%val = val
          self%comm = com
       end if
    else
       self%key = key
       self%val = val
       self%comm = com
    end if
  end subroutine pair_put


  recursive function pair_get(self, key) result(res)
    class(pair_t), intent(in) :: self
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: res

    res = UNDEF

    if (allocated(self%key)) then
       if (self%key /= key) then
          if (associated(self%next)) then
             res = pair_get(self%next, key)
          end if
       else
          res = self%val
       end if
    end if
  end function pair_get


  recursive subroutine pair_print(self, out)
    class(pair_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out

    if (self%comm /= '') then
       write(output, '(A)') '  '//trim(self%key)//' = '//trim(self%val)//' ! '//trim(self%comm)
    else
       write(output, '(A)') '  '//trim(self%key)//' = '//trim(self%val)
    end if

    if (associated(self%next)) then
       call pair_print(self%next, out)
    end if
  end subroutine pair_print


  recursive function pair_count(self) result(res)
    class(pair_t), intent(in) :: self

    integer :: res

    res = 0
    if (allocated(self%key)) then
       res = res + 1
       if (associated(self%next)) then
          res = res + pair_count(self%next)
       end if
    else
       res = 0
    end if
  end function pair_count


  recursive subroutine pair_clean(self)
    class(pair_t), intent(inout) :: self

    if (associated(self%next)) then
       call pair_clean(self%next)
       deallocate(self%next)
    end if
    self%next => null()
    if (allocated(self%key)) deallocate(self%key)
    if (allocated(self%val)) deallocate(self%val)
    if (allocated(self%comm)) deallocate(self%comm)
  end subroutine pair_clean


  ! ==================
  ! SECTION_T ROUTINES
  ! ==================
  recursive subroutine section_put(self, name, key, val, com)
    class(section_t), intent(inout) :: self
    character(len=*), intent(in) :: name
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: val
    character(len=*), intent(in) :: com

    if (allocated(self%name)) then
       ! If this section already has a name, two possibilities:
       ! 1. Either this is the one we want to put the keyval in ...
       ! 2. Or not !
       if (self%name == name) then
          ! This is the right section
          call self%keyval%put(key, val, com)
       else
          ! Else, we have to look at another position
          if (.not. associated(self%next)) allocate(self%next)
          call section_put(self%next, name, key, val, com)
       end if
    else
       ! We have to create a new section
       self%name = name
       call self%keyval%put(key, val, com)
    end if
  end subroutine section_put


  recursive function section_get(self, section, key) result(val)
    class(section_t), target, intent(in) :: self
    character(len=*), intent(in) :: section
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: val


    val = UNDEF
    if (allocated(self%name)) then
       if (self%name /= section) then
          if (associated(self%next)) then
             val = section_get(self%next, section, key)
          end if
       else
          val = self%keyval%get(key)
       end if
    end if
  end function section_get


  recursive subroutine section_print(self, out)
    class(section_t), intent(in) :: self
    integer, optional, intent(in) :: out

    integer :: output

    output = stdout
    if (present(out)) output = out
    write(output, '(A)') '&'//self%name
    call self%keyval%print(out)
    write(output, '(A)') '/'
    write(output, '(A)') ''
    if (associated(self%next)) then
       call section_print(self%next, output)
    end if
  end subroutine section_print


  recursive subroutine section_clean(self)
    class(section_t), intent(inout) :: self

    if (associated(self%next)) then
       call section_clean(self%next)
       deallocate(self%next)
    end if
    self%next => null()
    if (allocated(self%name)) deallocate(self%name)
    call pair_clean(self%keyval)

  end subroutine section_clean


  recursive function section_count_sections(self) result(res)
    class(section_t), intent(in) :: self

    integer :: res

    res = 0
    if (allocated(self%name)) then
       res = res + 1
       if (associated(self%next)) then
          res = res + section_count_sections(self%next)
       end if
    else
       res = 0
    end if
  end function section_count_sections


  ! ==================
  ! PARSER_T FUNCTIONS
  ! ==================
  subroutine parser_initialize(self, filename)
    class(parser_t), intent(inout) :: self
    character(len=*), intent(in) :: filename

    logical :: ext

    inquire(file=filename, exist=ext)
    if (.not. ext) then
       self%status = -1
       self%errmsg = 'Cannot find input file '//trim(filename)//' in directory !'
       return
    end if

    self%filename = filename
  end subroutine parser_initialize


  subroutine parser_parse_file(self)
    class(parser_t), intent(inout) :: self

    integer :: io, ind, i, nline
    character(len=2048) :: buf
    character(len=:), allocatable :: key, val, com, name

    nline = 0
    open(newunit=self%fileu, file=self%filename, action='read')
    PARSE: do
       read(self%fileu, '(A)', iostat=io) buf
       if (io /= 0) exit

       nline = nline + 1

       ! Strip starting blank characters, if any
       ind = 1
       do i=1, len_trim(buf)
          if (buf(i:i) == ' ') then
             ind = ind + 1
          else
             exit
          end if
       end do
       buf(1:len(buf)) = buf(ind:)

       ! Section start
       if (index(buf, '&') == 1) then
          name = remove_blanks(buf(2:))

          do
             read(self%fileu, '(A)', iostat=io) buf
             if (io /= 0) exit

             nline = nline + 1

             if (remove_blanks(buf) == '/') exit

             call split_line(buf, key, val, com, self%status)
             if (self%status /= 0) then
                write(self%errmsg, '(A,I0,A, A)') &
                     & 'Error in parsing '//self%filename//':', nline, NEW_LINE('a'), &
                     & NEW_LINE('a')//trim(buf)//NEW_LINE('a')
                return
             end if

             call self%set(name, key, val, com)
          end do
       end if
    end do PARSE
    close(self%fileu)
  end subroutine parser_parse_file


  subroutine parser_clean(self)
    class(parser_t), intent(inout) :: self

    if (allocated(self%filename)) deallocate(self%filename)
    self%fileu = 0

    call self%sections%clean()
  end subroutine parser_clean


  subroutine parser_get_keys(self, section, keys, val)
    class(parser_t), target, intent(in) :: self
    character(len=*), intent(in) :: section
    character(len=*), allocatable, intent(out) :: keys(:)
    character(len=*), allocatable, intent(out), optional :: val(:)

    integer :: nele, iter
    type(section_t), pointer :: tmp
    type(pair_t), pointer :: keyval

    tmp => self%sections
    do while (associated(tmp))
       if (tmp%name /= section) then
          ! print *, associated(tmp%next)
          if (associated(tmp%next)) then
             tmp => tmp%next
          else
             nullify(tmp)
          end if
       else
          nele = tmp%keyval%count()
          allocate(keys(nele))
          if (present(val)) then
             allocate(val(nele))
          end if

          keyval => tmp%keyval
          iter = 1
          do while (associated(keyval))
             keys(iter) = keyval%key
             if (present(val)) then
                val(iter) = keyval%val
             end if

             keyval => keyval%next
             iter = iter + 1
          end do

          nullify(tmp)
       end if
    end do
  end subroutine parser_get_keys


  function parser_get(self, section, key) result(res)
    class(parser_t), target, intent(in) :: self
    character(len=*), intent(in) :: section
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: res

    type(section_t), pointer :: tmp

    tmp => self%sections
    res = UNDEF
    do while(associated(tmp))
       if (tmp%name == section) then
          res = tmp%get(section, key)
          nullify(tmp)
          return
       else
          if (associated(tmp%next)) then
             tmp => tmp%next
          else
             nullify(tmp)
          end if
       end if
    end do
  end function parser_get


  subroutine parser_set(self, section, key, val, com)
    class(parser_t), target, intent(inout) :: self
    character(len=*), intent(in) :: section
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: val
    character(len=*), optional, intent(in) :: com

    call self%sections%put(section, key, val, com)
  end subroutine parser_set


  subroutine parser_print(self, out)
    class(parser_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out
    call self%sections%print(output)
  end subroutine parser_print



  function parser_has_section(self, section) result(res)
    class(parser_t), target, intent(in) :: self
    character(len=*), intent(in) :: section

    logical :: res

    type(section_t), pointer :: tmp
    tmp => self%sections
    do while(associated(tmp))
       if (tmp%name == section) then
          res = .true.
          nullify(tmp)
          return
       else
          if (associated(tmp%next)) then
             tmp => tmp%next
          else
             nullify(tmp)
          end if
       end if
    end do

    res = .false.
  end function parser_has_section


  function parser_has_key(self, section, key) result(res)
    class(parser_t), target, intent(in) :: self
    character(len=*), intent(in) :: section
    character(len=*), intent(in) :: key

    logical :: res

    type(section_t), pointer :: tmp
    type(pair_t), pointer :: keyval
    tmp => self%sections

    do while(associated(tmp))
       if (tmp%name == section) then

          keyval => tmp%keyval
          do while(associated(keyval))

             if (keyval%key == key) then
                res = .true.
                nullify(keyval)
                nullify(tmp)
                return
             else
                if (associated(keyval%next)) then
                   keyval => keyval%next
                else
                   nullify(keyval)
                   nullify(tmp)
                end if
             end if
          end do

       else
          if (associated(tmp%next)) then
             tmp => tmp%next
          else
             nullify(tmp)
          end if
       end if
    end do

    res = .false.
  end function parser_has_key




  function parser_extract_section(self, name) result(res)
    class(parser_t), target, intent(in) :: self
    character(len=*), intent(in) :: name

    type(section_t) :: res

    type(section_t), pointer :: tmp

    tmp => self%sections
    do while(associated(tmp))
       if (tmp%name == name) then
          res%name = name
          res%keyval = tmp%keyval
          nullify(tmp)
       else
          if (associated(tmp%next)) then
             tmp => tmp%next
          else
             nullify(tmp)
          end if
       end if
    end do
  end function parser_extract_section


  subroutine split_line(line, key, val, com, ierr)
    character(len=*), intent(in) :: line
    character(len=:), allocatable, intent(out) :: key
    character(len=:), allocatable, intent(out) :: val
    character(len=:), allocatable, intent(out) :: com
    integer, intent(out) :: ierr

    integer :: ind1, ind2, i, start
    character(len=:), allocatable :: tmpkey, tmpval, tmpcom

    ierr = 0

    ind1 = index(line, '=')
    if (ind1 == 0) then
       ierr = -1
       return
    end if

    ind2 = index(line, '!')

    start = 1
    do i=1, len_trim(line)
       if (line(i:i) == ' ') then
          start = start + 1
       else
          exit
       end if
    end do

    tmpkey = line(start:ind1-1)

    if (ind2 == 0) then
       tmpval = line(ind1+1:len_trim(line))
       tmpcom = ''
    else
       tmpval = line(ind1+1:ind2-1)
       tmpcom = line(ind2+1:len_trim(line))
    end if

    tmpval = sanitize_character(tmpval)

    key = trim(tmpkey)
    val = trim(tmpval)
    com = trim(tmpcom)

  end subroutine split_line



  recursive function section_has_name(self, name) result(res)
    class(section_t), intent(in) :: self
    character(len=*), intent(in) :: name

    logical :: res

    if (self%name == name) then
       res = .true.
    else
       if (associated(self%next)) then
          res = section_has_name(self%next, name)
       else
          res = .false.
       end if
    end if
  end function section_has_name


  pure function remove_blanks(line) result(res)
    character(len=*), intent(in) :: line

    character(len=:), allocatable :: res

    integer :: i, start

    start = 1
    do i=1, len_trim(line)
       if (line(i:i) == ' ') then
          start = start + 1
       else
          exit
       end if
    end do

    res = line(start:len_trim(line))
  end function remove_blanks


  pure function sanitize_character(line) result(res)
    character(len=*), intent(in) :: line

    character(len=:), allocatable :: res

    res = remove_blanks(line)
    if (res(1:1) == '"' .or. res(1:1) == "'") then
       res = res(2:len_trim(res)-1)
    end if
  end function sanitize_character


  subroutine set_config_int(parser, section, ele, key)
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in) :: section
    integer, intent(inout) :: ele
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: val

    val = parser%get(section, key)

    ! print *, 'KEY: '//trim(key)//' ; VAL: '//val

    if (val /= 'UNDEFINED_ELEMENT') then
       read( val, *) ele
    end if
  end subroutine set_config_int


  subroutine set_config_r64(parser, section, ele, key)
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in) :: section
    real(dp), intent(inout) :: ele
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: val

    val = parser%get(section, key)

    ! print *, 'KEY: '//trim(key)//' ; VAL: '//val

    if (val /= 'UNDEFINED_ELEMENT') then
       read( val, *) ele
       ! print *, 'NEW ELEMENT: ', ele
    end if
    ! real(dp), intent(inout) :: ele
    ! character(len=*), intent(in) :: name
    ! character(len=*), intent(in) :: keys(:)
    ! character(len=*), intent(in) :: values(:)

    ! integer :: i, ind

    ! ind = -1
    ! do i=1, size(keys)
    !    if (name == trim(keys(i))) then
    !       ind = i
    !       exit
    !    end if
   ! end do

    ! if (ind > 0) read(values(ind), *) ele
  end subroutine set_config_r64


  subroutine set_config_str(parser, section, ele, key)
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in) :: section
    character(len=*), intent(inout) :: ele
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: val

    val = parser%get(section, key)

    ! print *, 'KEY: '//trim(key)//' ; VAL: '//val

    if (val /= 'UNDEFINED_ELEMENT') then
       read( val, '(A)') ele
    end if
    ! character(len=*), intent(inout) :: ele
    ! character(len=*), intent(in) :: name
    ! character(len=*), intent(in) :: keys(:)
    ! character(len=*), intent(in) :: values(:)

    ! integer :: i, ind

    ! ind = -1
    ! do i=1, size(keys)
    !    if (name == trim(keys(i))) then
    !       ind = i
    !       exit
    !    end if
    ! end do

    ! if (ind > 0) read(values(ind), *) ele
  end subroutine set_config_str


  subroutine set_config_realloc(parser, section, ele, key)
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in) :: section
    character(len=:), allocatable, intent(inout) :: ele
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: val

    val = parser%get(section, key)

    if (val /= 'UNDEFINED_ELEMENT') then
       if (allocated(ele)) deallocate(ele)
       allocate( character(len=len_trim(val)) :: ele )

       read( val, '(A)') ele
    end if
    ! character(len=*), intent(inout) :: ele
    ! character(len=*), intent(in) :: name
    ! character(len=*), intent(in) :: keys(:)
    ! character(len=*), intent(in) :: values(:)

    ! integer :: i, ind

    ! ind = -1
    ! do i=1, size(keys)
    !    if (name == trim(keys(i))) then
    !       ind = i
    !       exit
    !    end if
    ! end do

    ! if (ind > 0) read(values(ind), *) ele
  end subroutine set_config_realloc


  subroutine set_config_bool(parser, section, ele, key)
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in) :: section
    logical, intent(inout) :: ele
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: val
    integer :: tmp, ierr

    val = parser%get(section, key)

    if (val /= 'UNDEFINED_ELEMENT') then
       read( val, *, iostat=ierr ) tmp
       if (ierr == 0) then
          if (tmp <= 0) then
             ele = .false.
          else
             ele = .true.
          end if
       else
          read( val, *) ele
       end if
    end if
  end subroutine set_config_bool


  subroutine set_config_complex(parser, section, ele, key)
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in) :: section
    complex(dp), intent(inout) :: ele
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: val

    real(dp) :: rr, ii

    val = parser%get(section, key)

    if (val /= 'UNDEFINED_ELEMENT') then
       read( val, *) rr, ii
    end if

    ele = complex(rr, ii)
  end subroutine set_config_complex



  subroutine set_config_with_alloc_int_1(parser, section, ele, key)
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in) :: section
    integer, allocatable, intent(inout) :: ele(:)
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: val

    val = parser%get(section, key)

    if (val /= UNDEF) then

       ! print *, 'VAL IS '//val

       if (allocated(ele)) deallocate(ele)

       block
         integer :: i, nele

         nele = 1
         do i=1, len(val)
            if (val(i:i) == ',') then
               nele = nele + 1
            end if
         end do
         if (val(len(val):len(val)) == ',') nele = nele - 1

         ! print *, 'FOUND '//to_str(nele)//' ELEMENTS'

         allocate(ele(nele))
       end block

       read(val, *) ele
    end if

  end subroutine set_config_with_alloc_int_1

  subroutine set_config_with_alloc_real_1(parser, section, ele, key)
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in) :: section
    real(dp), allocatable, intent(inout) :: ele(:)
    character(len=*), intent(in) :: key

    character(len=:), allocatable :: val

    val = parser%get(section, key)

    if (val /= UNDEF) then

       ! print *, 'VAL IS '//val

       if (allocated(ele)) deallocate(ele)

       block
         integer :: i, nele

         nele = 1
         do i=1, len(val)
            if (val(i:i) == ',') then
               nele = nele + 1
            end if
         end do
         if (val(len(val):len(val)) == ',') nele = nele - 1

         ! print *, 'FOUND '//to_str(nele)//' ELEMENTS'

         allocate(ele(nele))
       end block

       read(val, *) ele
    end if

  end subroutine set_config_with_alloc_real_1

end module mod_input_parser
