! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_tools
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-07-10
  !!
  !! This module contains useful functions that are general enough to
  !! be used in many places inside Newton-X. This category includes
  !! several vector or matrix operation (scalar product, norm
  !! computation, ...).
  !!
  !! TODO: Unify the scalar_product and norm_ functions in an
  !! interface.
  use mod_kinds, only: dp
  use mod_constants, only: MAX_STR_SIZE
  use iso_fortran_env, only: &
       & compiler_options, compiler_version
  use mod_version, only: &
       & nx_version
  use mod_print_utils, only: pad_str
  use mod_interface, only: &
       & copy, rm, mkdir
  use mod_logger, only: check_error
  implicit none

  private

  public :: title_message
  public :: nx_step
  public :: nx_init_random
  public :: nx_random_number
  public :: compute_ekin, compute_ekin_sub
  public :: norm
  public :: scalar_product
  public :: find_index
  public :: gauss_rand
  public :: split_blanks
  public :: split_pattern
  public :: phase_adjust_h_vec
  public :: reshape_epot
  public :: clear
  public :: assert_equal
  public :: to_str
  public :: remove_blanks
  public :: str_ends_with
  public :: str_starts_with
  public :: file_exists

  integer, parameter :: DEFAULT_SEED = 4357
  logical, save :: is_test
  integer, save :: rnx

  interface to_str
     module procedure to_str_int
     module procedure to_str_int_d1
     module procedure to_str_r64
     module procedure to_str_bool
  end interface to_str

  interface scalar_product
     module procedure scalar_product_1d
     module procedure scalar_product_2d
  end interface scalar_product

  interface norm
     module procedure norm_1d
     module procedure norm_2d
  end interface norm

  interface clear
     module procedure clear_msg_array
  end interface clear

  interface assert_equal
     module procedure assert_equal_r64
  end interface assert_equal

contains

  function build_nx_version() result(res)
    character(len=:), allocatable :: res

    logical :: ext
    integer :: u, i, ll, id
    character(len=MAX_STR_SIZE) :: env, tmp, tmp2, tmp3

    call get_environment_variable('NXHOME', env)

    tmp = nx_version()

    inquire(file=trim(env)//'/.git/', exist=ext)
    if (ext) then

       ! We have a git repository, so let's use that
       call execute_command_line(&
            & 'git --git-dir='//trim(env)//'/.git describe --dirty --always --tags > version.txt'&
            &)
       open(newunit=u, file='version.txt')
       read(u, '(A)') tmp2
       close(u)
       id = index(tmp2, trim(tmp))
       if (id /= 0) then
          ll = len_trim(tmp) + 1
          if ( tmp2(ll:ll) == '-' ) then
             ll = ll+1
          end if

          tmp3 = ''
          do i=ll, len_trim(tmp2)
             tmp3(i-ll+1:i-ll+1) = tmp2(i:i)
          end do

          tmp2 = tmp3
       end if
       id = rm(['version.txt'])
    else
       tmp2 = 'release'
    end if

    tmp = trim(tmp)//' - '//trim(tmp2)

    res = trim(tmp)
  end function build_nx_version


  subroutine title_message(out)
    integer, intent(in) :: out

    integer :: i
    integer, parameter :: length = 80
    character(len=MAX_STR_SIZE) :: tmp
    character(len=:), allocatable :: group(:)
    character(len=:), allocatable :: version

    version = build_nx_version()

    write(out, '(A)') repeat('=', length)
    write(out, '(A)') pad_str("NEWTON-X New Series 3", length)
    write(out, '(A)') pad_str("Version "//version, length)
    write(out, '(A)') pad_str("Newtonian dynamics close to the crossing seam", length)
    write(out, '(A)') pad_str("www.newtonx.org", length)
    write(out, '(A)') repeat('=', length)
    write(out, '(A)') ''
    write(out, '(A)') 'Copyright (C) 2022  Light and Molecules Group'
    write(out, '(A)') ''
    write(out, '(A)') 'This program is free software: you can redistribute it and/or modify'
    write(out, '(A)') 'it under the terms of the GNU General Public License as published by'
    write(out, '(A)') 'the Free Software Foundation, either version 3 of the License, or'
    write(out, '(A)') '(at your option) any later version.'
    write(out, '(A)') ''
    write(out, '(A)') 'This program is distributed in the hope that it will be useful,'
    write(out, '(A)') 'but WITHOUT ANY WARRANTY; without even the implied warranty of'
    write(out, '(A)') 'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the'
    write(out, '(A)') 'GNU General Public License for more details.'
    write(out, '(A)') ''
    write(out, '(A)') 'You should have received a copy of the GNU General Public License'
    write(out, '(A)') 'along with this program.  If not, see <https://www.gnu.org/licenses/>.'

    write(out, '(A)') ''
    write(out, '(A)') ''
    write(out, '(A)') " Please report any bugs as issues in our Gitlab:";
    write(out, '(A)') "   https://gitlab.com/light-and-molecules/newtonx/";
    write(out, '(A)') ''
    write(out, '(A)') " Or to our mailling list:";
    write(out, '(A)') "   newtonx [at] freelists.org";
    write(out, '(A)') ''
    write(out, '(A)') ''
    write(out, '(A)') repeat('-', 80)
    write(out, '(A)') '| Compiler:     '//compiler_version()
    write(out, '(A)') '| With options: '

    tmp = compiler_options()
    group = split_pattern(tmp)
    tmp = ''
    do i=1, size(group)
       tmp = trim(tmp)//' '//trim(group(i))
       if (mod(i, 5) == 0) then
          write(out, '(A)') '|     '//trim(tmp)
          tmp = ''
       end if
    end do
    write(out, '(A)') '|     '//trim(tmp)
    write(out, '(A)') repeat('-', 80)
    write(out, '(A)') ''
    write(out, '(A)') repeat('=', length)
    write(out, '(A)') ''
  end subroutine title_message


  pure function nx_step(status, step, t, virtual) result(res)
    character(len=*), intent(in) :: status
    integer, intent(in) :: step
    real(dp), intent(in) :: t
    logical, intent(in), optional :: virtual

    character(len=:), allocatable :: res

    character(len=1024) :: tmp
    logical :: vv

    vv = .false.
    if (present(virtual)) vv = virtual

    if (vv) then
       write(tmp, '(A)') &
            & status//' virtual step '//to_str(step) &
            & //' (time t = '//to_str(t, fmt='(F10.3)') &
            & //' fs)'
    else
       write(tmp, '(A)') &
            & status//' step '//to_str(step) &
            & //' (time t = '//to_str(t, fmt='(F10.3)') &
            & //' fs)'
    end if
    res = trim(tmp)
  end function nx_step


  subroutine nx_init_random(seed)
    !! Initialize the random seed.
    !!
    !! The initialization method depends on the value of ``seed``:
    !!
    !! - ``seed = -1``: a randomized seed is used;
    !! - ``seed = 0``: use a default seed (4357);
    !! - ``seed = N``: use ``N`` as a seed.
    !!
    !! Optionnally, it is possible to provide a ``seedold`` array, in
    !! which case this array is set as the current seed.
    integer, intent(in) :: seed
    !! Decide on how to initialize the seed.

    logical :: ext
    integer :: u, i

    integer, allocatable :: seedold(:)
    character(len=2048) :: buf
    character(len=64), allocatable :: splitted(:)

    rnx = 0
    if (seed == -2) then
       is_test = .true.
       return
    else
       inquire(file='rndseed', exist=ext)
       if (ext) then
          open(newunit=u, file='rndseed', action='read')
          read(u, '(A)') buf
          call split_blanks(buf, splitted)
          allocate(seedold(size(splitted)))
          do i=1, size(splitted)
             read(splitted(i), *) seedold(i)
          end do
          call random_seed(put=seedold)
       else
          if (seed == 0) then
             ! Use the default seed defined above
             call init_seed_from_value(DEFAULT_SEED)
          else if (seed == -1) then
             ! Use a randomized seed
             call init_random_seed()
          else
             ! Use the seed provided
             call init_seed_from_value(seed)
          end if
       end if
    end if
  end subroutine nx_init_random


  subroutine init_random_seed()
    !! Initialize a random seed.
    !!
    !! This routine is taken from the [gfortran manual]
    !! (https://gcc.gnu.org/onlinedocs/gcc-4.6.1/gfortran/RANDOM_005fSEED.html).
    integer :: i, n, clock
    integer, dimension(:), allocatable :: seed

    call random_seed(size=n)
    allocate(seed(n))

    call system_clock(count=clock)

    seed = clock + 37 * (/ (i-1, i=1, n) /)
    call random_seed(put=seed)
    deallocate(seed)
  end subroutine init_random_seed


  subroutine init_seed_from_value(seed_value)
    !! Initialize the random seed with the given value.
    !!
    !! The seed is given by:
    !! ``seed(i) = value + 37 * (i-1)``.
    integer, intent(in) :: seed_value
    !! Seed value.

    integer, dimension(:), allocatable :: seed
    integer :: i, n

    call random_seed(size=n)
    allocate(seed(n))

    seed = seed_value + 37 * (/ (i-1, i=1, n) /)
    call random_seed(put=seed)

    deallocate(seed)
  end subroutine init_seed_from_value


  function nx_random(idum) result(res)
    !! ``ran`` generator from Numerical Recipes.
    !!
    !! WARNING: This function should not be used in production, and
    !! is only intended at providing a portable random number
    !! generator for testing purpose !!
    implicit none

    integer, intent(inout) :: idum

    real(dp) :: res

    integer, parameter :: IA=16807,IM=2147483647,IQ=127773,IR=2836
    real(dp), save :: am
    integer, save :: ix=-1,iy=-1,k

    if (idum <= 0 .or. iy < 0) then
       am=nearest(1.0,-1.0)/real(IM, dp)
       iy=ior(ieor(888889999,abs(idum)),1)
       ix=ieor(777755555,abs(idum))
       idum=abs(idum)+1
    end if

    ix=ieor(ix,ishft(ix,13))
    ix=ieor(ix,ishft(ix,-17))
    ix=ieor(ix,ishft(ix,5))
    k=iy/IQ
    iy=IA*(iy-k*IQ)-IR*k
    if (iy < 0) iy=iy+IM
    res=am*ior(iand(IM,ieor(ix,iy)),1)

  end function nx_random


  subroutine nx_random_number(x)
    real(dp), intent(out) :: x

    if (is_test) then
       x = nx_random(rnx)
    else
       call random_number(x)
    end if
  end subroutine nx_random_number


  pure function compute_ekin_sub(veloc, masses, use_atom) result(ekin)
    !! Compute the kinetic energy for a sybsystem
    !! This is used for example to compute the kinetic energy of the QM
    !! subsystem in a QM/MM simulation

    real(dp), dimension(:, :), intent(in) :: veloc
    !! Current velocity (in a.u.)
    real(dp), dimension(:), intent(in) :: masses
    !! Masses of each particle (in a.u.)
    logical, dimension(:), intent(in) :: use_atom
    !! Logical value for each atom True = included, False = Excluded

    real(dp) :: ekin
    !! Resulting kinetic energy
    integer :: nsub
    !! Number of atoms in the subsystem
    real(dp), allocatable :: sub_masses(:), sub_velocities(:,:)
    !! Masses and velocities for the QM subsystem
    integer :: i, j

    nsub = count(use_atom)

    allocate(sub_masses(nsub))
    allocate(sub_velocities(3,nsub))

    sub_masses = 0.0_dp
    sub_velocities = 0.0_dp

    j = 1
    do i=1, size(masses)
      if(use_atom(i)) then
        sub_velocities(:,j) = veloc(:,i)
        sub_masses(j) = masses(i)
        j = j + 1
      end if
    end do

    ekin = compute_ekin(sub_velocities, sub_masses)

    deallocate(sub_masses)
    deallocate(sub_velocities)

  end function compute_ekin_sub


  pure function compute_ekin(veloc, masses) result(ekin)
    !! Compute the kinetic energy.
    !!
    !! The energy will be computed in the unit system defined by the
    !! arguments.
    real(dp), dimension(:, :), intent(in) :: veloc
    !! Current velocity (in a.u.)
    real(dp), dimension(:), intent(in) :: masses
    !! Masses of each particle (in a.u.)
    real(dp) :: ekin
    !! Resulting kinetic energy

    integer :: i, j
    real(dp) :: v2

    ekin = 0.0_dp
    do i=1, size(masses)
       v2 = 0.0_dp
       do j=1, 3
          v2 = v2 + veloc(j, i)**2
       end do
       ekin = ekin + 0.5_dp * masses(i) * v2
    end do
  end function compute_ekin


  pure function norm_1d(vec) result(n)
    !! Compute the norm of the given vector.
    real(dp), dimension(:), intent(in) :: vec

    real(dp) :: n

    n = norm2(vec)
  end function norm_1d


  pure function norm_2d(mat) result(n)
    !! Compute the norm of the given matrix.
    !!
    !! This norm is defined as the norm of the vector constructed
    !! with all lines of the matrix put side by side.
    real(dp), dimension(:, :), intent(in) :: mat

    real(dp) :: n

    n = norm2(mat)
  end function norm_2d


  pure function scalar_product_1d(vec1, vec2) result(sc)
    !! Compute the scalar product of two vectors.
    real(dp), dimension(:), intent(in) :: vec1
    real(dp), dimension(:), intent(in) :: vec2

    real(dp) :: sc

    sc = dot_product(vec1, vec2)
  end function scalar_product_1d


  pure function scalar_product_2d(mat1, mat2) result(sc)
    !! Compute the scalar product of two matrices.
    !!
    !! This scalar product is defined as the sum of the scalar
    !! products of the lines of ``mat1`` with those of ``mat2``.
    real(dp), dimension(:, :), intent(in) :: mat1
    real(dp), dimension(:, :), intent(in) :: mat2

    real(dp) :: sc

    integer :: i

    sc = 0.0_dp
    do i=1, size(mat1, 2)
       sc = sc + dot_product(mat1(:, i), mat2(:, i))
    end do
  end function scalar_product_2d


  pure function find_index(k, l) result(kl)
    !! Find the index corresponding to the given couple.
    !!
    !! Assuming we have a vector storing the off-diagonal elements of
    !! a symmetric matrix, this function returns the index storing
    !! the element that corresponds to indices k and l of the
    !! corresponding matrix. If k and l are equal, the value -1 is
    !! returned instead.
    integer, intent(in) :: k, l
    integer :: kl

    if (k .gt. l) then
       kl = ( (k-2)*(k-1) / 2) + l
    else if (k .lt. l) then
       kl = ( (l-2)*(l-1) / 2) + k
    else
       kl = -1
    end if
  end function find_index


  subroutine gauss_rand(rg)
    !! Generate a random number from a Gaussian distribution.
    !!
    !! This is obtained via the Box-Muller transformation.
    real(dp), intent(inout) :: rg

    real(dp) :: w, r1, r2, x1, x2, y, rf

    w = 0.0_dp
    x1 = 0.0_dp
    x2 = 0.0_dp
    do while ((w >= 1.0_dp) .or. (w == 0.0_dp))
       call random_number(r1)   ! Random number uniform-distributed between 0 and 1
       call random_number(r2)   ! Random number uniform-distributed between 0 and 1
       x1 = 2.0_dp*r1-1.0_dp
       x2 = 2.0_dp*r2-1.0_dp
       w = x1**2 + x2**2
    enddo
    y = dsqrt(-2.0_dp * dlog(w) / w )
    rf = x1*y   ! this random number is not used
    rg = x2*y   ! only this omne returns

  end subroutine gauss_rand


  subroutine split_blanks(line, splitted)
    !! Split a line with respect to blanks.
    !!
    !! The subroutine returns an array containing the (non-blank) elements
    !! of ``line``. The ``splitted`` resulting array is automatically allocated
    !! by this routine.
    !!
    !! Example:
    !!     character(len=64), allocatable :: temp(:)
    !!     character(len=64) :: buf
    !!
    !!     buf = ' This line has blanks     '
    !!     call split_blanks(buf, temp)
    !!
    !! ``temp`` is equal to: [``'This'``, ``'line'``, ``'has'``, ``'blanks'``]
    !!
    character(len=*), intent(in) :: line
    character(len=*), allocatable, intent(out) :: splitted(:)

    integer :: nblanks, i

    if (allocated(splitted)) deallocate(splitted)

    nblanks = 0
    i = 1
    ! Find first non blank character
    do
       if (line(i:i) == ' ') then
          i = i + 1
       else
          exit
       end if
    end do

    do
       i = i+1
       if (i >= len_trim(line)) exit
       if ((line(i:i) == ' ') .and. (line(i-1:i-1) /= ' ')) then
          nblanks = nblanks + 1
       end if
    end do

    ! If the last character is a blank, we counted one too many blanks !
    ! if (line(len(line):len(line)) == ' ') nblanks = nblanks - 1

    allocate(splitted(nblanks + 1))
    splitted(:) = ''

    read(line, *) splitted(1:nblanks+1)
  end subroutine split_blanks


  function split_pattern(string, pattern, keep_last, keep_first) result(res)
    !! Split a line with respect to blanks.
    !!
    !! The subroutine returns an array containing the (non-blank) elements
    !! of ``line``. The ``splitted`` resulting array is automatically allocated
    !! by this routine.
    !!
    !! Example:
    !!     character(len=64), allocatable :: temp(:)
    !!     character(len=64) :: buf
    !!
    !!     buf = ' This line has blanks     '
    !!     call split_blanks(buf, temp)
    !!
    !! ``temp`` is equal to: [``'This'``, ``'line'``, ``'has'``, ``'blanks'``]
    !!
    character(len=*)  :: string
    character(len=*), optional :: pattern
    logical, optional :: keep_first
    logical, optional :: keep_last

    character(len=len_trim(string)), allocatable :: res(:)

    integer :: strlen, patternlen
    integer :: id, npattern, alloc_size, start_ind, ind
    integer :: str_first_id
    logical :: pattern_first, pattern_last, first, last
    character(len=:), allocatable :: fpattern, mstring

    fpattern = ' '
    if (present(pattern)) fpattern = pattern

    strlen = len_trim(string)
    if (fpattern == ' ') then
       patternlen = 1
    else
       patternlen = len_trim(fpattern)
    end if

    first = .false.
    last = .false.
    if (present(keep_first)) first = keep_first
    if (present(keep_last)) last = keep_last

    ! First non blank index in the line
    str_first_id = 1
    do id=1, strlen
       if (string(id:id) == ' ') then
          str_first_id = str_first_id + 1
       else
          exit
       end if
    end do

    ! Determine if the string starts or ends with pattern (makes sense only when the
    ! pattern is not ' ').
    pattern_first = .false.
    pattern_last = .false.
    if (fpattern /= ' ') then
       if (string(str_first_id:patternlen) == fpattern) then
          pattern_first = .true.
       end if

       if (string(strlen-patternlen+1:strlen) == fpattern) then
          pattern_last = .true.
       end if
    end if

    ! If we want to split according to spaces, then we'll modify the original string to
    ! collapse all consecutive spaces together
    if (fpattern == ' ') then
       block
         character(len=1) :: char_a(len(string))
         integer :: new_size

         new_size = 1
         char_a(:) = ''
         id = str_first_id
         do
            if (id > strlen) exit
            if ((string(id:id) == ' ') .and. (string(id+1:id+1) == ' ')) then
               id = id+1
            else
               ! print *, 'ELE = ', string(id:id)
               char_a(new_size) = string(id:id)
               id = id + 1
               new_size = new_size + 1
            end if
         end do

         allocate(character(len=new_size-1) :: mstring)
         do id=1, new_size-1
            mstring(id:id) = char_a(id)
         end do

         strlen = new_size-1
         str_first_id = 1
       end block
    else
       mstring = string
    end if

    ! Count the number of patterns in the mstring
    id = str_first_id
    npattern = 0
    COUNT_PAT: do
       if (id > strlen) then
          exit COUNT_PAT

       else if (mstring(id:id+patternlen-1) == fpattern) then
          npattern = npattern + 1
          id = id+patternlen
       else
          id = id + 1
       end if
    end do COUNT_PAT

    ! Determine the number of elements in the resulting array, depending on the options
    ! and on the presence of patterns as first and / or last elements in the mstring.
    ! We start from the easiest case, where the pattern appears neither as first not last
    ! element: then, the number of elements in the array is simply npattern + 1
    if (pattern_first) npattern = npattern - 1
    if (pattern_last) npattern = npattern - 1
    alloc_size = npattern + 1
    start_ind = 1

    ! If the pattern appears first, and we want to keep the first element as empty, then
    ! the total number of elements will be increased by one, and we want to start filling
    ! the array at index 2
    if (pattern_first) then
       if (first) then
          alloc_size = alloc_size + 1
          start_ind = 2
       end if
    end if

    ! If the pattern is last in the mstring, and we want to keep the last element, we
    ! increase the number of elements in the array by 1.
    if (pattern_last) then
       if (last) then
          alloc_size = alloc_size + 1
       end if
    end if

    allocate(res(alloc_size))
    res(:) = ''

    ind = start_ind
    id = str_first_id
    if (pattern_first) then
       id = id+patternlen
    end if

    do
       if (id > strlen) exit

       if (mstring(id:id+patternlen-1) == fpattern) then
          id = id+patternlen
          ind = ind+1
       else
          res(ind) = trim(res(ind))//mstring(id:id)
          id = id+1
       end if
    end do
  end function split_pattern


  subroutine phase_adjust_h_vec(oldh, newh, phase, cossine)
    !! Adjust the phase of the NAD vector.
    !!
    !! The routine returns the ``cossine`` array, updated ``newh`` array, and
    !! ``phase`` array.
    !! First the norm of the ``oldh`` and ``newh`` vectors are computed, as
    !! well as their scalar product. If the product of the norms is small,
    !! the ``phase`` of the selected coupling is set to 1, and ``cossine`` to
    !! 2. Else, we compute the ``cossine`` value as ``scalar / (norm_old *
    !! norm_new)`` and the phase for the coupling is set to the sign of the
    !! cossine. Finally ``newh`` is multiplied by ``phase``.
    !!
    !! All arrays should be allocated before calling the routine.
    real(dp), dimension(:, :, :), intent(in) :: oldh
    !! NAD vectors at previous time-step.
    real(dp), dimension(:, :, :), intent(inout) :: newh
    !! NAD vectors  at current time-step.
    integer, dimension(:), intent(out) :: phase
    !! Phase for each different couplings.
    real(dp), dimension(:), allocatable :: cossine
    !! Mostly for debugging purpose

    integer :: i
    real(dp) :: norm_oldh, norm_newh, scalar_old_new
    real(dp) :: eps

    eps = 1.0E-6_dp

    do i=1, size(phase)
       norm_oldh = norm(oldh(i, :, :))
       norm_newh = norm(newh(i, :, :))
       scalar_old_new = scalar_product(oldh(i, :, :), newh(i, :, :))

       if ( dabs(norm_oldh * norm_newh) <= eps ) then
          phase(i) = 1
          cossine(i) = 2.0_dp
       else
          cossine(i) = scalar_old_new / (norm_newh * norm_oldh)
          if (cossine(i) > 0.0_dp) then
             phase(i) = 1
          else
             phase(i) = -1
          end if
       end if

       newh(i, :, :) = newh(i, :, :) * phase(i)
    end do

    ! print *, 'PHASES: ', phase
  end subroutine phase_adjust_h_vec


  subroutine reshape_epot(step, dt, epot_t, epot_old, nstat, epot, times)
    !! Concatenate data from ``traj%epot`` and ``traj%old_epot``.
    !!
    !! The routine also prepare time array for energy interpolation. We will
    !! carry out:
    !!
    !! - linear interpolation if 2 sets of energies are available (step = 1)
    !! - quadratic interpolation if 3 sets are available (step = 2)
    !! - cubic integration afterward.
    !!
    !! The degree of the interpolation is thus given by: step + 1 for
    !! step = 1 and step = 2, and deg = 4 after.
    !!
    !! The polint subroutine will be used, and require a set of times
    !! where the energies are evaluated. We prepare such a set, as well
    !! as the corresponding energies, here:
    !!
    !!     times = [ 0, dt, 2dt, 3dt]
    !!     epot_all = [ [epot_1(0)  , epot_2(0)  , ... , epot_N(0)  ]
    !!                  [epot_1(dt) , epot_2(dt) , ... , epot_N(dt) ]
    !!                  [epot_1(2dt), epot_2(2dt), ... , epot_N(2dt)]
    !!                  [epot_1(3dt), epot_2(3dt), ... , epot_N(3dt)] ]
    !!
    !! for a N-state computation.
    integer, intent(in) :: step
    !! Current step.
    real(dp), intent(in) :: dt
    !! Time-step (in fs).
    real(dp), intent(in) :: epot_t(:)
    !! Current array of potential energies.
    real(dp), intent(in) :: epot_old(:, :)
    !! Array of previous potential energies.
    integer, intent(in) :: nstat
    !! Number of states included.
    real(dp), allocatable, intent(out) :: epot(:, :)
    !! Resulting rearranged potential energies.
    real(dp), allocatable, intent(out) :: times(:)
    !! Time array.

    integer :: deg, i

    if (step < 3) then
       deg = step + 1
    else
       deg = 4
    end if

    allocate( epot( deg, nstat ))
    allocate(times(deg))

    times = [ ( (i-1)*dt, i=1, deg ) ]
    do i=1, deg - 1
       epot(i, :) = epot_old(deg-i, :)
    end do
    epot(size(epot, 1), :) = epot_t(:)

  end subroutine reshape_epot


  subroutine clear_msg_array(msga)
    !! Clear the content of an array of strings.
    !!
    !! The routine replaces all elements of the array with empty strings.
    character(len=*), dimension(:) :: msga
    !! Character array to clear.

    integer :: i

    do i=1, size(msga)
       msga(i) = ''
    end do

  end subroutine clear_msg_array


  elemental function assert_equal_r64(a, b, epsilon) result(res)
    real(dp), intent(in) :: a
    real(dp), intent(in) :: b
    real(dp), intent(in), optional :: epsilon

    logical :: res
    real(dp) :: eps

    eps = 1E-8_dp
    if (present(epsilon)) eps = epsilon

    if (abs(a-b) < eps) then
       res = .true.
    else
       res = .false.
    end if

  end function assert_equal_r64


  pure function to_str_int(n, fmt) result(res)
    integer, intent(in) :: n
    character(len=*), intent(in), optional :: fmt

    character(len=:), allocatable :: res

    character(len=4096) :: tmp
    character(len=1024) :: ff

    ff = '(I0)'
    if (present(fmt)) ff = fmt

    write(tmp, fmt=ff) n
    res = trim(tmp)
  end function to_str_int


  pure function to_str_int_d1(n, fmt) result(res)
    integer, intent(in) :: n(:)
    character(len=*), intent(in), optional :: fmt

    character(len=:), allocatable :: res

    character(len=4096) :: tmp
    character(len=1024) :: ff
    integer :: length, i

    length = size(n)
    ff = '(I0)'
    if (present(fmt)) ff = fmt

    tmp = to_str_int(n(1), fmt=ff)
    do i=2, length
       tmp = trim(tmp)//','//to_str_int(n(i), fmt=ff)
    end do

    res = trim(tmp)
  end function to_str_int_d1


  pure function to_str_bool(n) result(res)
    logical, intent(in) :: n

    character(len=:), allocatable :: res

    character(len=4096) :: tmp

    write(tmp, *)  n
    res = trim(tmp)
  end function to_str_bool


  pure function to_str_r64(n, fmt) result(res)
    real(dp), intent(in) :: n
    character(len=*), intent(in), optional :: fmt

    character(len=:), allocatable :: res

    character(len=4096) :: tmp
    character(len=1024) :: ff

    ff = '(F20.12)'
    if (present(fmt)) ff = fmt

    write(tmp, fmt=ff) n
    res = trim(adjustl(tmp))
  end function to_str_r64


  function remove_blanks(str) result(res)
    character(len=*), intent(in) :: str
    character(len=:), allocatable :: res

    character(len=:), allocatable :: tmp
    integer :: i, len_str

    tmp = adjustl(trim(str))

    len_str = len_trim(tmp)

    allocate( character(len=len_str) :: res )
    do i=1, len_str
       res(i:i) = tmp(i:i)
    end do
    ! print *, 'RES = ', res
  end function remove_blanks


  function str_ends_with(str, pattern) result(res)
    character(len=*), intent(in) :: str
    character(len=*), intent(in) :: pattern
    logical :: res

    integer :: strlength, patlength

    strlength = len_trim(str)
    patlength = len_trim(pattern)

    if (str(strlength-patlength+1:strlength) == pattern) then
       res = .true.
    else
       res = .false.
    end if
  end function str_ends_with


  function str_starts_with(str, pattern) result(res)
    character(len=*), intent(in) :: str
    character(len=*), intent(in) :: pattern
    logical :: res

    integer :: patlength, firstnb
    integer :: i

    patlength = len_trim(pattern)

    res = .false.
    firstnb = 1
    do i=1, len(str)
       if (str(i:i) == ' ') then
          firstnb = firstnb + 1
       else
          exit
       end if
    end do

    if (str(firstnb:firstnb + patlength) == pattern) then
       res = .true.
    else
       res = .false.
    end if
  end function str_starts_with


  function file_exists(fname) result(res)
    character(len=*), intent(in) :: fname

    logical :: res

    inquire(file=fname, exist=res)
  end function file_exists
  
  
end module mod_tools
