! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_keyval_pairs
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2023-03-06
  !!
  !! # Simple Fortran implementation of key / value pairs
  !!
  !! ## Description
  !!
  !! This module implements simple key / value pairs in pure Fortran.  The ``pair_t``
  !! type is a linked list of pairs ``(key, val)``, where both ``key`` and ``val`` are of
  !! ``character(len=:), allocatable`` type.  This allows for simple a very simple
  !! implementation, as we don't have to mind about the type of the ``val`` member.
  !!
  !! When fetching for the value of a key in the list, if the key is not found, the value
  !! is set to ``UNDEFINED``.  This allows to make a difference for cases when a value is
  !! set to an empty string, and cases where the key has not been defined yet.
  !!
  !! ## Synopsis
  !!
  !!     type(pair_t) :: dict
  !!     logical :: has_key
  !!     character(len=:), allocatable :: val
  !!
  !!     ! Populate the linked list
  !!     call dict%set('key1', 'Val1')
  !!     call dict%set('key2', 'Val2')
  !!     call dict%set('key3', 'Val3')
  !!
  !!     ! Probe the list
  !!     has_key = dict%has_key('key1') ! -> .true.
  !!     has_key = dict%has_key('keyN') ! -> .false.
  !!
  !!     ! Get an element
  !!     val = dict%get('key1') ! -> val = 'Val1'
  !!
  !!     ! Clean the memory
  !!     call dict%clean()
  !!
  !!
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  type, public :: pair_t
     !! Pair definition.
     !!
     character(len=:), allocatable :: key
     character(len=:), allocatable :: val
     type(pair_t), pointer :: next => null()
   contains
     procedure, pass :: set => pair_set
     procedure, pass :: get => pair_get
     procedure, pass :: print => pair_print
     procedure, pass :: has => pair_has_key
     procedure, pass :: clean => pair_clean
  end type pair_t

  character(len=*), parameter :: UNDEF = 'UNDEFINED'

contains

  recursive function pair_get(self, key) result(res)
    !! Get the value corresponding to ``key``.
    !!
    class(pair_t), intent(in) :: self
    !! ``pair_t`` object.
    character(len=*), intent(in) :: key
    !! Key to fetch.

    character(len=:), allocatable :: res

    character(len=2048) :: tmp

    res = UNDEF
    if (allocated(self%key)) then
       if (self%key /= key) then
          if (associated(self%next)) then
             res = pair_get(self%next, key)
          end if
       else
          res = self%val
       end if
    end if
  end function pair_get


  recursive subroutine pair_set(self, key, val)
    !! Insert the pair ``(key, val)``  at the end of the list.
    !!
    !! If the pair already exists, its content is replaced.
    class(pair_t), intent(inout) :: self
    character(len=*), intent(in) :: key
    character(len=*), intent(in) :: val

    if (allocated(self%key)) then
       if (self%key /= key) then
          if (.not. associated(self%next)) allocate(self%next)
          call pair_set(self%next, key, val)
       else
          self%key = key
          self%val = val
       end if
    else
       self%key = key
       self%val = val
    end if
  end subroutine pair_set


  recursive subroutine pair_print(self, unit, prefix)
    !! Print a list of pairs.
    !!
    !! The printing is done as:
    !!
    !!     prefix key - pair
    !!
    class(pair_t), intent(in) :: self
    !! ``pair_t`` to print.
    integer, intent(in), optional :: unit
    !! Optional (opened) unit where the printing is done.
    character(len=*), intent(in), optional :: prefix
    !! Optional prefix.

    integer :: u

    u = stdout
    if (present(unit)) u = unit
    
    
    if (allocated(self%key)) then
       if (present(prefix)) write(u, '(A)', advance='no') prefix//' '
       write(u, '(A)') trim(self%key)//' - '//trim(self%val)
       if (associated(self%next)) then
          call pair_print(self%next, prefix=prefix)
       end if
    end if
  end subroutine pair_print
  

  recursive function pair_has_key(self, key) result(res)
    !! Check if the list has a key.
    !!
    !! Returns ``.true.`` if a pair in the list has the key ``key``. Else, returns
    !! ``.false.``.
    class(pair_t), intent(in) :: self
    !! ``pair_t`` object.
    character(len=*), intent(in) :: key
    !! Key to check.

    logical :: res

    res = .false.
    if (allocated(self%key)) then
       if (self%key /= key) then
          if (associated(self%next)) then
             res = pair_has_key(self%next, key)
          end if
       else
          res = .true.
       end if
    end if
  end function pair_has_key


  recursive subroutine pair_clean(self)
    !! Clean the memory of a list of pairs.
    class(pair_t), intent(inout) :: self

    if (associated(self%next)) then
       call pair_clean(self%next)
       deallocate(self%next)
    end if
    self%next => null()
    if (allocated(self%key)) deallocate(self%key)
    if (allocated(self%val)) deallocate(self%val)
  end subroutine pair_clean

end module mod_keyval_pairs
