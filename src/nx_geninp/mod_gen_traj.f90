! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_gen_traj
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2023-03-05
  !!
  !! Newton-X trajectory generator.
  !!
  !! This module is primarily used by the program ``nx_geninp`` to create the trajectory
  !! folders based on the content of the file ``final_output``.
  use mod_constants, only: MAX_STR_SIZE
  use mod_input_parser, only: parser_t, &
       & set => set_config, &
       & set_realloc => set_config_realloc
  use mod_interface, only: &
       & copy, rm, mkdir
  use mod_kinds, only: dp
  use mod_logger, only: &
       & print_conf_ele, check_error
  use mod_tools, only: split_pattern, &
       & nx_init_random, nx_random_number, &
       & file_exists, to_str
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  type :: nx_en_window_t
     !! Energy window for input extraction from ``final_output`` files.
     !!
     real(dp) :: e_min = 0.0_dp
     real(dp) :: e_0 = 0.0_dp
     real(dp) :: e_max = 0.0_dp
  end type nx_en_window_t

  type, public :: nx_gen_traj_t
     !! Parameters for generating trajectories in NX.
     !!
     integer :: nis = 1
     !! Lowest state to consider.
     integer :: screen = 0
     !! Energy restriction criterion:
     !!
     !! - 0 - Do not apply any restriction.
     !! - 1 - Use the original energy restriction written in the final_output files.
     !! - 2 - Apply new energy restriction.
     real(dp) :: e_center_real = 0.0_dp
     !! Center of the energy restriction (in eV).
     character(len=:), allocatable :: e_center_ref
     !! File for extracting the center of energy restriction.
     real(dp) :: e_var = 0.5_dp
     !! Width of the energy restriction, if used (in eV)
     logical :: read_os_from_file = .true.
     !! Flag to indicate if the oscillator strengths should be read from ``final_output``
     !! or not.
     real(dp) :: os_condon = 0.0_dp
     !! If oscillator strengths are not obtained from file, this value is used.
     character(len=10) :: norm = 'local'
     !! Indicate if the norm is taken with the restricted set (``local``) or the full
     !! dataset (``global``)
     integer :: seed = -1
     !! Random number generation
     !!
     !! - -1 - A randomized seed is used
     !! - 0 - A default random-number seed is used
     !! - n - Use this number as a random seed
     integer :: run_is = 0
     !! Indicate if the dynamics observables are computed for a target distribution
     !! different from the sampling distribution (``1``) or for the same distribution
     !! (``0``).
     type(nx_en_window_t) :: window
   contains
     procedure, pass :: init => gen_traj_init
     procedure, pass :: set_energy_window
     procedure, pass :: print => gen_traj_print
     procedure, pass :: max_intensity
     procedure, pass :: generate_dir
  end type nx_gen_traj_t
  interface nx_gen_traj_t
     module procedure initialize_gentraj
  end interface nx_gen_traj_t

contains

  function initialize_gentraj() result(res)
    !! Initialize a ``nx_gen_traj_t`` object.
    !!
    !! The function initializes the allocatable string ``self%e_center_ref`` to the empty
    !! string.
    type(nx_gen_traj_t) :: res

    res%e_center_ref = ''
  end function initialize_gentraj


  subroutine gen_traj_init(self, parser)
    !! Initialize a ``gentraj`` object with the content of ``parser``.
    !!
    class(nx_gen_traj_t), intent(inout) :: self
    !! ``gentraj`` object.
    type(parser_t), intent(in) :: parser
    !! Parser.

    character(len=*), parameter :: sect = 'generate_traj'

    call set(parser, sect, self%nis, 'nis')
    call set(parser, sect, self%screen, 'screen')
    call set(parser, sect, self%e_center_real, 'e_center_real')
    call set_realloc(parser, sect, self%e_center_ref, 'e_center_ref')
    call set(parser, sect, self%e_var, 'e_var')
    call set(parser, sect, self%read_os_from_file, 'read_os_from_file')
    call set(parser, sect, self%os_condon, 'os_condon')
    call set(parser, sect, self%norm, 'norm')
    call set(parser, sect, self%seed, 'seed')
    call set(parser, sect, self%run_is, 'run_is')
  end subroutine gen_traj_init


  subroutine gen_traj_print(self, unit)
    !! Print the content of a ``gentraj`` object.
    !!
    class(nx_gen_traj_t), intent(in) :: self
    !! ``gentraj`` object.
    integer, intent(in), optional :: unit
    !! Optional output unit.

    integer :: out

    out = stdout
    if (present(unit)) out = unit

    call print_conf_ele(self%nis, 'nis', unit=out)
    call print_conf_ele(self%screen, 'screen', unit=out)
    call print_conf_ele(self%e_center_real, 'e_center_real', unit=out)
    call print_conf_ele(self%e_center_ref, 'e_center_ref', unit=out)
    call print_conf_ele(self%e_var, 'e_var', unit=out)
    call print_conf_ele(self%read_os_from_file, 'read_os_from_file', unit=out)
    call print_conf_ele(self%os_condon, 'os_condon', unit=out)
    call print_conf_ele(self%norm, 'norm', unit=out)
    call print_conf_ele(self%seed, 'seed', unit=out)
    call print_conf_ele(self%run_is, 'run_is', unit=out)
  end subroutine gen_traj_print


  subroutine set_energy_window(self, ierr, msg)
    !! Set the energy window for selecting geometries.
    !!
    !! The screening done depends on the ``self%screen`` variable.  Three cases:
    !!
    !! - 0: Do not apply any restrictions ;
    !! - 1: Keep already applied restrictions (only usefule when restrictions where
    !!   applied when running ``initcond.pl``).
    !! - 2: Apply restrictions by setting ``self%window%e_0`` to the vertical excitation
    !!   of the equilibrium geometry (if ``self%e_center_ref`` is set, the corresponding
    !!   file is read), or to ``self%e_center_real`` if no file is provided. Then we have
    !!   ``self%window%e_max = self%window%e_0 - self%e_var`` and
    !!   ``self%window%e_min = self%window%e_0 + self%e_var``.
    !!
    !! In case of abnormal termination: ``ierr < 0``, and ``msg`` is set to a debugging
    !! message.
    !!
    !! In other cases, ``ierr = 0`` and ``msg`` is empty.
    class(nx_gen_traj_t), intent(inout) :: self
    !! ``gentraj`` object.
    integer, intent(inout) :: ierr
    !! Error status
    character(len=:), allocatable, intent(out) :: msg
    !! Error message.

    character(len=256) :: filename

    integer :: nis, u, screen
    logical :: has_eq_energy
    real(dp) :: e_var
    character(len=MAX_STR_SIZE) :: buf
    character(len=256), allocatable :: split(:)

    ierr = 0

    select case(self%screen)
    case(0)
       ! No restriction
       self%window%e_min = -1.0e9_dp
       self%window%e_0 = 0.0_dp
       self%window%e_max = 1.0e9_dp
    case(1)
       ! Keep applied restrictions
       self%window%e_min = 0.0_dp
       self%window%e_0 = 0.0_dp
       self%window%e_max = 0.0_dp
    case(2)
       has_eq_energy = .false.

       if (self%e_center_ref /= '') then

          open(newunit=u, file=self%e_center_ref, action='read')
          PARSE: do
             read(u, '(A)', iostat=ierr) buf
             if (ierr /= 0) exit
             if (index(buf, 'Equilibrium geometry') /= 0) then
                has_eq_energy = .true.
                do
                   read(u, '(A)', iostat=ierr) buf
                   if (ierr /= 0) exit
                   if (index(buf, 'Vertical excitation') /=0) then
                      split = split_pattern(buf)
                      read(split(size(split)), *) self%window%e_0
                      exit PARSE
                   end if
                end do
             end if
          end do PARSE
          close(u)
          if (.not. has_eq_energy) then
             ierr = -1
             msg = 'ERROR: '//trim(self%e_center_ref)//' must contain an "Equilibrium energy" card'
          end if
       else
          ! Reference state was given
          self%window%e_0 = self%e_center_real
       end if

       self%window%e_min = self%window%e_0 - self%e_var
       self%window%e_max = self%window%e_0 + self%e_var
    end select
  end subroutine set_energy_window


  function max_intensity(self) result(res)
    !! Find the maximum intensity in ``final_output``.
    !!
    !! The function returns the highest oscillator strength found in the file
    !! ``final_output``.
    class(nx_gen_traj_t), intent(in) :: self
    !! ``gentraj`` object.

    real(dp) :: res

    integer :: u, ierr
    character(len=MAX_STR_SIZE) :: buf
    character(len=256), allocatable :: split(:)
    real(dp) :: en, osc, delta

    res = 0.0_dp
    open(newunit=u, file='final_output', action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Vertical excitation') /= 0) then
          split = split_pattern(buf)
          read(split(4), *) en
       end if

       if (index(buf, 'Oscillator strength') /= 0) then
          split = split_pattern(buf)
          read(split(size(split)), *) osc

          if (self%norm == 'local') then
             if (en < self%window%e_min .or. en > self%window%e_max) then
                delta = 0.0_dp
             else
                delta = 1.0_dp
             end if
          end if ! local
          if (delta * osc > res) res = osc
       end if
    end do
    close(u)
  end function max_intensity


  subroutine generate_dir(self, nat)
    !! Create the set of trajectory directories.
    !!
    !! The routine selects the initial geometries and velocities for running the
    !! dynamics, based on an energy criterion given by ``self%window``.  For any frame,
    !! if the energy of the frame is within the energy window, then the corresponding
    !! initial conditions have a probablity of being selected equal to ``osc_str /
    !! max_int``, where ``osc_str`` is the oscillator strength of the frame.
    !!
    !! For each selected set of input, a folder ``TRAJECTORIES/TRAJN`` (``N`` is the
    !! trajectory index) is created, with ``geom.orig``, ``veloc.orig``,
    !! ``user_config.nml`` and ``JOB_AD`` or ``JOB_NAD``.
    !!
    !! If no ``JOB_AD`` or ``JOB_NAD`` folder can be found in the current working
    !! directory, then the routine fails and the program will end.
    class(nx_gen_traj_t), intent(in) :: self
    !! ``gentraj`` object.
    integer, intent(in) :: nat
    !! Number of atoms (for easier parsing of ``final_output``).

    integer :: u, ierr, i, ntraj, nfiles, v
    character(len=MAX_STR_SIZE) :: buf
    character(len=256) :: geom(nat), veloc(nat), dirname
    character(len=256), allocatable :: split(:), to_copy(:)
    logical :: is_eq, gen_this, ext
    real(dp) :: en, delta, osc_str, prob, rx

    nfiles = 1 ! user_config.nml
    inquire(file='JOB_NAD', exist=ext)
    if (ext) then
       nfiles = nfiles + 1
    end if

    inquire(file='JOB_AD', exist=ext)
    if (ext) then
       nfiles = nfiles + 1
    end if

    allocate(to_copy(nfiles))
    to_copy(1) = 'user_config.nml'


    ! Initialize random number generation
    call nx_init_random(self%seed)

    ! First geometry we will read is the equilibrium geometry.
    is_eq = .true.

    ntraj = 0
    open(newunit=u, file='final_output', action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Initial condition') /= 0) is_eq = .false.

       if (index(buf, 'Geometry') /= 0) then
          do i=1, nat
             read(u, '(A)') geom(i)
          end do
       end if

       if (index(buf, 'Velocity') /= 0) then
          do i=1, nat
             read(u, '(A)') veloc(i)
          end do
       end if

       if (index(buf, 'Vertical excitation') /= 0) then
          split = split_pattern(buf)
          read(split(4), *) en

          delta = 1.0_dp
          if (en < self%window%e_min .or. en > self%window%e_max) then
             delta = 0.0_dp
          end if
       end if

       if (index(buf, 'Oscillator strength') /= 0) then
          if (self%read_os_from_file) then
             split = split_pattern(buf)
             read(split(3), *) osc_str
          else
             osc_str = self%os_condon
          end if

          ! Now we can make the decision if we keep this geometry ot not, only if no
          ! prior screening has been applied.
          gen_this = .true.
          if (self%screen /= 0) then
             prob = osc_str * delta / self%max_intensity()
             call nx_random_number(rx)
             if (rx > prob) gen_this = .false.
          end if

          if (gen_this .and. .not. is_eq) then
             dirname = 'TRAJECTORIES/TRAJ'//to_str(ntraj)
             ntraj = ntraj + 1
             ierr = mkdir(dirname)
             call check_error(ierr, 101, &
                  & 'GENTRAJ: Cannot create dir '//trim(dirname), system=.true.)

             if (file_exists('JOB_NAD')) then
                ierr = copy('JOB_NAD/', trim(dirname)//'/')
                call check_error(ierr, 101, &
                     & 'GENTRAJ: Error in copying JOB_NAD to '//trim(dirname), &
                     & system=.true.)
             end if

             if (file_exists('JOB_AD')) then
                ierr = copy('JOB_AD/', trim(dirname)//'/')
                call check_error(ierr, 101, &
                     & 'GENTRAJ: Error in copying JOB_AD to '//trim(dirname), &
                     & system=.true.)
             end if

             ierr = copy('user_config.nml', trim(dirname)//'/')
             call check_error(ierr, 101, &
                  & 'GENTRAJ: Error in copying user_config.nml to '//trim(dirname), &
                  & system=.true.)

             open(newunit=v, file=trim(dirname)//'/geom.orig', action='write')
             do i=1, nat
                write(v, '(A)') trim(geom(i))
             end do
             close(v)

             open(newunit=v, file=trim(dirname)//'/veloc.orig', action='write')
             do i=1, nat
                write(v, '(A)') trim(veloc(i))
             end do
             close(v)
          end if
       end if
    end do
    close(u)
  end subroutine generate_dir

end module mod_gen_traj
