program nx_geninp
  !! Interactive input generator for Newton-X.
  !!
  !! This program can be used to generate:
  !!
  !! - a basic input containing the minimal options from the ``nxconfig`` section (a more
  !!   general and complete input can then be obtained by running ``nx_moldyn
  !!   --dry-run``) ;
  !! -
  !! - directories for running trajectories based on initial conditions coming from a
  !!   file with ``final_output`` format created by ``init_cond.pl``.
  use mod_nx_geninp, only: main_loop
  implicit none

  call main_loop
end program nx_geninp
