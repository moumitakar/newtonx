! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_configuration
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2021-03-24
  !!
  !! This module handles the general configuration of the program. It
  !! defines the ``nx_config_t`` type for storing information that
  !! are immutable during the dynamics, such as the number of ataoms,
  !! the number of states included, or the printing level.
  !!
  !! The configuration is read from a file containing the namelist
  !! ``nxconfig``.
  use mod_kinds, only: dp
  use mod_logger, only: print_conf_ele, nx_log, LOG_ERROR, LOG_WARN
  use mod_print_utils, only: to_upper, to_lower
  use mod_constants, only: timeunit, au2ev, MAX_STR_SIZE
  use mod_input_parser, only: &
       & parser_t, &
       & set => set_config, set_realloc => set_config_realloc
  use mod_implemented, only: nx_interfaces_t
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  type, public ::  nx_config_t
     !! General information about the dynamics.
     !!
     !! The parameters from this object should all be immutable during the
     !! dynamics.
     character(len=:), allocatable :: output_path
     !! Folder in which the outputs will be stored (default: `RESULTS`).
     !! Initialized in ``[[constructor]]``.
     character(len=:), allocatable :: debug_path
     !! Folder in which the debug files will be stored (default: `DEBUG`).
     !! Initialized in ``[[constructor]]``.
     character(len=:), allocatable :: h5file
     !! Name of the H5MD output file (default: `dyn.h5`).
     !! Initialized in ``[[constructor]]``.
     character(len=:), allocatable :: progname
     !! Name of the program used to perform electronic structure computations.
     !! Initialized in ``[[constructor]]``.
     character(len=:), allocatable :: methodname
     !! Name of the electronic structure method used (tddft, cc2, ...)
     !! Initialized in ``[[constructor]]``.
     character(len=:), allocatable :: init_input
     !! Name of the folder in which the inputs for the QM computation in the
     !! first step are located (`JOB_NAD` when `dc_couplings` is 1, `JOB_AD` else).
     !! Initialized in ``[[constructor]]``.
     integer :: nat = -1
     !! Number of atoms in the system.
     integer :: nstat = -1
     !! Number of states included.
     integer :: nstatdyn = -1
     !! Initial state of the dynamics.
     integer :: kt = 1
     !! Print output information only every ``kt`` step (default: 1).
     real(dp) :: dt = 0.5_dp
     !! Time-step (in fs.) (default: 0.5 fs).
     real(dp) :: tmax = 100.0_dp
     !! Maximum duration of the dynamics (in fs) (default: 100 fs).
     real(dp) :: thres = 100.0_dp
     !! Threshold for triggering
     !! the time-derivative couplings (in eV) : if \(\Delta E < thres\), then
     !! the time-derivative couplings will be computed (default: 100 eV).
     integer :: killstat = 1
     !! Kill the trajectory if more than ``timekill`` is
     !! spent on state ``killstat`` (1) or let it continue (0).
     real(dp) :: timekill = 0
     !! Kill the trajectory if more than ``timekill`` (in fs)  is
     !! spent on state ``killstat``. `0` means disable this capacity (default: 0 fs)
     real(dp) :: etot_jump = 0.5_dp
     !! Kill the trajectory if the change in total energy in one step is
     !! higher than ``etot_jump`` (in eV) (default: 0.5 eV).
     real(dp) :: etot_drift = 0.5_dp
     !! Kill the trajectory if the change in total energy between the current
     !! step and the initial total energy is higher than ``etot_jump`` (in
     !! eV) (default: 0.5 eV).
     real(dp) :: epot_diff = 0.2_dp
     !! Kill the trajectory if the absolute energy difference between any excited state and ground
     !! state is more than ``epot_diff`` (in eV) (only used for methods that don't
     !! describe crossings with GS well: TDDFT, CC2, ADC2).
     !! (default: 0.2 eV)
     integer :: init_step = 0
     !! First step of the dynamics (default: 0).
     real(dp) :: init_time = 0.0_dp
     !! Time at which the dynamics is started (default: 0.0 fs).
     integer :: lvprt = 3
     !! Debug level (see mod_logger documentation for more details):
     !!
     !! - 1 - Only error messages
     !! - 2 - Warning and error messages
     !! - 3 - (DEFAULT) Info, warning and error messages
     !! - 4 - Same as 3, with supplementary messages
     !! - 5 - Full debug output (warning, huge amount of text can be created ! )
     !!
     !! (default: 3).
     real(dp) :: thrs_cos = 0.98_dp
     !! Threshold for cosine similarity (MO OVL matrix) (default: 0.98).
     real(dp) :: thrs_norm = 2.5_dp
     !! Threshold for F-norm difference (MO OVL matrix) (default: 2.5).
     character(len=:), allocatable :: init_veloc
     !! Name of the file containing the initial velocities (default: `veloc.orig`).
     !! Initialized in ``[[constructor]]``.
     character(len=:), allocatable :: init_geom
     !! Name of the file containing the initial geometry (in NX format) (default:
     !! `wf.orig`).
     !! Initialized in ``[[constructor]]``.
     logical :: use_txt_outputs = .false.
     !! Produce ``txt`` file outputs (set to ``.false.`` by default when
     !! using HDF5).
     integer :: check_mo_ovl = -1
     !! Perform a check on the conditioning of the MO overlap matrix
     !! between times t and t-dt if > 0 (default: -1).
     integer :: dc_method = -1
     !! Type of derivative couplings to use:
     !!
     !! - 0 - Do not compute (or read) the couplings (for adiabatic dynamics for instance)
     !! - 1 - Read non-adiabatic coupling vectors directly from the QM code
     !! - 2 - Read a state-overlap matrix (and compute it if required)
     !! - 3 - Auxiliary method (Baeck An for instance)
     integer :: nxrestart = 0
     !! Indicate if the computation is a restart (1) or not (0) (default: 0).
     integer :: save_cwd = 10
     !! Backup the working directory every ``save_cwd`` step (default: 10).
     logical :: with_adt = .false.
     !! Toggle for adaptive time-step (default: `.false.`).
     logical :: with_zpe = .false.
     !! Toggle for ZPE corrections
     logical :: with_thermo = .false.
     !! Toggle for thermostat inclusion
     logical :: use_locdiab = .false.
     !! Toggle for local diabatizations
     logical :: compute_diabpop = .false.
     !! Toggle diabatic population computation (only available with SBH model).
     logical :: run_complex = .false.
     !! Toggle to indicate if CS-FSSH should be used.
     integer :: gamma_model = 0
     !! CS-FSSH: Model to compute the resonance and imaginary NAD values:
     !!
     !! - 0: produced by the QM method ;
     !! - 1: computed by an external program (path is specified by ``path_gamma_model`` ;
     !! - 2: computed by a model implemented in NX.
     character(len=:), allocatable :: path_gamma_model
     !! CS-FFSH: Path to the executable used for computing gamma (when ``gamma_model = 1``)
     logical :: same_mo = .true.
     !! CS-FSSH: Use the same set of MOs for reference calculation or not.
     real(dp) :: ress_shift = 0.0_dp
   contains
     procedure :: init
     procedure :: destroy
     procedure :: print => print_configuration
     procedure :: set_couplings => conf_set_dc_coupling
     final :: finalizer
  endtype nx_config_t

  interface nx_config_t
     module procedure constructor
  end interface nx_config_t

contains

  function constructor() result(res)
    !! Constructor for the ``nx_config_t`` class.
    !!
    !! The function initializes all allocatable components from the class.
    type(nx_config_t) :: res

    res%output_path = 'RESULTS'
    res%debug_path = 'DEBUG'
    res%h5file = 'dyn.h5'
    res%init_input  = 'JOB_AD'
    res%init_veloc = 'veloc.orig'
    res%init_geom = 'geom.orig'
    res%methodname = ''
    res%progname = ''
    res%path_gamma_model = ''
  end function constructor


  subroutine destroy(self)
    class(nx_config_t), intent(inout) :: self

    deallocate(self%output_path)
    deallocate(self%debug_path)
    deallocate(self%h5file)
    deallocate(self%init_input )
    deallocate(self%init_veloc)
    deallocate(self%init_geom)
    deallocate(self%methodname)
    deallocate(self%progname)
    deallocate(self%path_gamma_model)
  end subroutine destroy


  subroutine finalizer(self)
    type(nx_config_t), intent(inout) :: self

    deallocate(self%output_path)
    deallocate(self%debug_path)
    deallocate(self%h5file)
    deallocate(self%init_input )
    deallocate(self%init_veloc)
    deallocate(self%init_geom)
    deallocate(self%methodname)
    deallocate(self%progname)
    deallocate(self%path_gamma_model)
  end subroutine finalizer


  subroutine init(conf, parser)
    !! Constructor for the ``nx_config_t`` object.
    !!
    class(nx_config_t), intent(inout) :: conf
    type(parser_t), intent(in) :: parser
    !! Name of the initialization file.

    integer :: ierr
    character(len=MAX_STR_SIZE) :: msg

    ! Mandatory parameters
    call set(parser, 'nxconfig', conf%nat, 'nat')
    call set_realloc(parser, 'nxconfig', conf%progname, 'progname')
    call set_realloc(parser, 'nxconfig', conf%methodname, 'methodname')
    call set(parser, 'nxconfig', conf%nstat, 'nstat')
    call set(parser, 'nxconfig', conf%nstatdyn, 'nstatdyn')
    call conf_check_user_input(conf, ierr, msg)
    if (ierr /= 0) then
       call nx_log%log(LOG_ERROR, msg)
       error stop
    end if

    call set(parser, 'nxconfig', conf%tmax, 'tmax')

    ! Log control
    call set(parser, 'nxconfig', conf%lvprt, 'lvprt')
    call set(parser, 'nxconfig', conf%kt, 'kt')

    if (conf%lvprt > 4) conf%save_cwd = -1
    call set(parser, 'nxconfig', conf%save_cwd, 'save_cwd')

    ! Other parameters
    call set(parser, 'nxconfig', conf%dt, 'dt')
    conf%dt = conf%dt / timeunit
    call set(parser, 'nxconfig', conf%thres, 'thres')
    conf%thres = conf%thres / au2ev
    call set(parser, 'nxconfig', conf%killstat, 'killstat')
    call set(parser, 'nxconfig', conf%timekill, 'timekill')
    call set(parser, 'nxconfig', conf%etot_jump, 'etot_jump')
    conf%etot_jump = conf%etot_jump / au2ev
    call set(parser, 'nxconfig', conf%etot_drift, 'etot_drift')
    conf%etot_drift = conf%etot_drift / au2ev

    call conf_set_epot_diff(conf)
    call set(parser, 'nxconfig', conf%epot_diff, 'epot_diff')
    conf%epot_diff = conf%epot_diff / au2ev
    call set(parser, 'nxconfig', conf%thrs_cos, 'thrs_cos')
    call set(parser, 'nxconfig', conf%thrs_norm, 'thrs_norm')

    ! Restart control
    call set(parser, 'nxconfig', conf%init_time, 'init_time')
    call set(parser, 'nxconfig', conf%init_step, 'init_step')
    call set(parser, 'nxconfig', conf%nxrestart, 'nxrestart')

    ! Switches for different parts of the computations or options
    call set(parser, 'nxconfig', conf%use_txt_outputs, 'use_txt_outputs')
    call set(parser, 'nxconfig', conf%with_adt, 'with_adt')
    call set(parser, 'nxconfig', conf%with_zpe, 'with_zpe')
    call set(parser, 'nxconfig', conf%with_thermo, 'with_thermo')
    call set(parser, 'nxconfig', conf%use_locdiab, 'use_locdiab')

    if (conf%methodname == 'cs_fssh') then
       conf%run_complex = .true.
    end if
    call set(parser, 'nxconfig', conf%run_complex, 'run_complex')
    if (conf%run_complex) then
       if (&
            & ( conf%progname == 'analytical' .and. conf%methodname /= 'cs_fssh') .or. &
            & (conf%progname == 'columbus' .and. conf%methodname /= 'mrci') &
            & ) then
          call nx_log%log(LOG_WARN, &
               & 'run_complex is only available with method "cs_fssh": I''ll set it to false !')
          conf%run_complex = .false.
       end if
    end if
    if (conf%run_complex) then
       call set(parser, 'nxconfig', conf%gamma_model, 'gamma_model')
       if (conf%gamma_model == 1) then
          call set_realloc(parser, 'nxconfig', conf%path_gamma_model, 'path_gamma_model')
       end if
       if (conf%gamma_model == 2) then
          call set(parser, 'nxconfig', conf%same_mo, 'same_mo')
          call set(parser, 'nxconfig', conf%ress_shift, 'ress_shift')
       end if
    end if

    ! Set the default value for `dc_coupling` before reading fro namelist
    call conf%set_couplings()
    call set(parser, 'nxconfig', conf%dc_method, 'dc_method')

    ! Set the default input folder before reading
    call conf_init_input(conf)
    call set(parser, 'nxconfig', conf%init_input, 'init_input')

    ! Set the default value for checking MO ovl before reading
    call conf_check_ovl(conf)
    call set(parser, 'nxconfig', conf%check_mo_ovl, 'check_mo_ovl')

    if (conf%methodname == 'sbh') conf%compute_diabpop = .true.
    call set(parser, 'nxconfig', conf%compute_diabpop, 'compute_diabpop')
    if (conf%compute_diabpop .and. conf%methodname /= 'sbh') then
       call nx_log%log(LOG_WARN, &
            & 'Diabatic population computation is only available for SBH: setting it to false !')
       conf%compute_diabpop = .false.
    end if
  end subroutine init


  subroutine print_configuration(this, out)
    !! Print the configuration.
    !!
    class(nx_config_t) :: this
    !! This object.
    integer, intent(in), optional :: out

    character(len=12) :: ffmt
    integer :: output

    type(nx_interfaces_t) :: int

    output = stdout
    if (present(out)) output = out

    write(output, '(A80)') repeat('*', 80)
    write(output, *) 'Molecular Dynamics using ', trim(to_upper(this%progname))
    write(output, *) 'Method: ', trim(to_upper(this%methodname))
    write(output, *) ''

    ffmt = '(f10.3)'

    call print_conf_ele('../'//this%output_path, 'Output directory', unit=output)
    call print_conf_ele(this%debug_path, 'Debug directory', unit=output)
    call print_conf_ele(this%nat, 'Num. atoms', unit=output)
    call print_conf_ele(this%nstat, 'Num. states', unit=output)
    call print_conf_ele(this%nstatdyn, 'Init. state', unit=output)
    call print_conf_ele(this%dt*timeunit, 'Time-step (fs)', elefmt=ffmt, unit=output)
    call print_conf_ele(this%dt, 'Time-step (au)', elefmt=ffmt, unit=output)
    call print_conf_ele(this%tmax, 'Simulation time (tmax)', elefmt=ffmt, unit=output)
    call print_conf_ele(this%init_time, 'Initial time', elefmt=ffmt, unit=output)
    call print_conf_ele(this%init_step, 'Initial step', unit=output)
    call print_conf_ele(this%thres, 'Threshold', elefmt=ffmt, unit=output)
    call print_conf_ele(this%killstat, 'killstat', unit=output)
    call print_conf_ele(this%timekill, 'timekill', elefmt=ffmt, unit=output)
    call print_conf_ele(this%etot_jump, 'Energy jump thr. (au)', &
         & elefmt=ffmt, unit=output)
    call print_conf_ele(this%etot_drift, 'Energy drift thr. (au)', &
         & elefmt=ffmt, unit=output)
    call print_conf_ele(this%thrs_cos, 'Cosine sim. thr.', elefmt=ffmt, unit=output)
    call print_conf_ele(this%thrs_norm, 'F-norm sim. thr.', elefmt=ffmt, unit=output)
    call print_conf_ele(this%lvprt, 'Debug level', unit=output)
    call print_conf_ele(this%init_geom, 'Initial geometry', unit=output)
    call print_conf_ele(this%init_veloc, 'Initial velocity', unit=output)
    call print_conf_ele(this%dc_method, 'Derivative couplings', unit=output)

    if (this%use_locdiab) then
       write(output, *) ''
       write(output, *) '     LOCAL DIABATIZATION REQUESTED'
       write(output, *) ''
    end if

    write(output, '(A80)') repeat('*', 80)
    write(output, *) ' '
  end subroutine print_configuration


  subroutine conf_set_dc_coupling(self)
    !! Guess the method to obtain derivative couplings.
    !!
    !! Set ``self%dc_method`` to:
    !!
    !! - ``0`` when adiabatic dynamics is requested (``thres = 0`` or ``nstat = 1``) ;
    !! - ``1`` if the program requested can compute non-adiabatic coupling vectors
    !!   (/i.e./ Columbus, Analytical models or Tinker MNDO) ;
    !! - ``2`` for other programs, or when local diabatization is requested (with
    !!   ``with_locdiab`` configuration flag).
    class(nx_config_t), intent(inout) :: self
    !! Configuration object.

    real(dp), parameter :: eps = 1E-6_dp

    if (self%thres < eps .or. self%nstat == 1) then
       self%dc_method = 0
       return
    end if

    if (   self%progname == 'analytical' .or. &
         & self%progname == 'columbus' .or. &
         & self%progname == 'tinker_mndo' &
         ) then
       self%dc_method = 1
    else
       self%dc_method = 2
    end if

    if (self%use_locdiab) then
       self%dc_method = 2
    end if
  end subroutine conf_set_dc_coupling


  subroutine conf_init_input(self)
    !! Try to guess the folder from which inputs will be read.
    !!
    !! Set ``self%init_input`` to:
    !!
    !! - ``JOB_NAD`` when non-adiabatic coupling vectors are read directly from QM computation;
    !! - ``JOB_AD`` in every other cases.
    type(nx_config_t), intent(inout) :: self
    !! Configuration object.

    if (self%dc_method == 1) then
       self%init_input = 'JOB_NAD'
    else
       self%init_input = 'JOB_AD'
    end if
  end subroutine  conf_init_input


  subroutine conf_check_ovl(self)
    !! Guess if MO overlap has to be computed.
    !!
    !! This is only relevant for Columbus (the check is mainly used to indicate orbital
    !! rotation in MCSCF-type computations).
    type(nx_config_t), intent(inout) :: self
    !! Configuration object.

    if (self%progname == 'columbus') then
       self%check_mo_ovl = 1
    else
       self%check_mo_ovl = 0
    end if
  end subroutine conf_check_ovl


  subroutine conf_set_epot_diff(self)
    type(nx_config_t), intent(inout) :: self
    !! Configuration object.

    select case(self%progname)
    case('columbus','mopac','analytical','tinker_mndo','exc_mopac', &
         & 'mlatom')
       self%epot_diff = -1.0_dp
    case default
       self%epot_diff = 0.2_dp
    end select
  end subroutine conf_set_epot_diff


  subroutine conf_check_user_input(self, ierr, msg)
    !! Check the consistency of the configuration.
    !!
    !! The routine sets ``ierr`` to ``0`` on success, and ``-1`` in the following cases:
    !!
    !! - ``progname`` or ``methodname`` has not been set ;
    !! - ``progname`` or ``methodname`` are not implemented;
    !! - ``nat`` has not been provided ;
    !! - ``nstat`` has not been provided ;
    !! - ``nstatdyn`` has not been provided.
    type(nx_config_t), intent(in) :: self
    !! Configuration object.
    integer, intent(out) :: ierr
    !! Status code.
    character(len=*) :: msg
    !! Log message.

    type(nx_interfaces_t) :: int
    character(len=MAX_STR_SIZE) :: tmp

    ierr = 0
    msg = 'Errors in "&nxconfig" section:'
    if (.not. allocated(self%progname) .or. .not. (allocated(self%methodname))) then
       ierr = 1
       msg = trim(msg)//NEW_LINE('c')//'   "progname" or "methodname" is not defined'
    else
       if (self%progname == '') then
          if (self%progname /= 'external' .and. self%progname /= 'mlatom') then
             if (self%methodname == '') then
                ierr = 1
                msg = trim(msg)//NEW_LINE('c')//'   "progname" or "methodname" is not defined'
             end if
          end if
       end if
    end if

    int = nx_interfaces_t()
    if ( int%has_method(self%progname, self%methodname) < 0 ) then
       ierr = 1
       msg = ' "progname = '//self%progname//'" or "methodname = '//self%methodname//&
            & '" are not available in NewtonX:'
       msg = trim(msg)//int%print_progs()
    end if

    if (self%nat < 0) then
       ierr = 1
       msg = trim(msg)//NEW_LINE('c')//'   "nat" is not defined'
    end if

    if (self%nstat < 0) then
       ierr = 1
       msg = trim(msg)//NEW_LINE('c')//'   "nstat" is not defined'
    end if

    if (self%nstatdyn < 0) then
       ierr = 1
       msg = trim(msg)//NEW_LINE('c')//'   "nstatdyn" is not defined'
    end if
  end subroutine conf_check_user_input

endmodule mod_configuration
