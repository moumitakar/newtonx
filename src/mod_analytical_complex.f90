! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_analytical_complex
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # Analytical model for CS-FSSH
  !!
  !! This module describes analytical models that can be used to model CS-FSSH
  !! trajectories with a combination of harmonic and exponential potentials, that
  !! interact with gaussian potentials.
  !!
  !! The implementation follows the one described in [Kossoski and
  !! Barbatti](https://doi.org/10.1039/D0SC04197A).
  use mod_constants, only: &
       & MAX_STR_SIZE, au2ev
  use mod_general_potentials, only: &
       & nx_exponential_pot_t, nx_harmonic_pot_t, &
       & nx_imag_pot_t, nx_gaussian_pot_t, nx_potential_t
  use mod_kinds, only: dp
  use mod_input_parser, only: &
       & parser_t, set => set_config, set_alloc => set_config_with_alloc
  use mod_logger, only: &
       & nx_log, &
       & LOG_ERROR, LOG_DEBUG, check_error, &
       & print_conf_ele
  use mod_tools, only: to_str, norm
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  type :: varr
     class(nx_potential_t), pointer :: v
  end type varr
  

  type, public :: nx_cs_fssh_t
     integer :: cs_mod = 1
     type(nx_harmonic_pot_t), allocatable :: h(:)
     type(nx_exponential_pot_t), allocatable :: e(:)
     type(nx_imag_pot_t), allocatable :: vi(:)
     type(nx_gaussian_pot_t), allocatable :: g(:)
     logical :: use_lapack = .false.
   contains
     procedure :: setup => complex_setup
     procedure :: print => complex_print
     procedure :: run => complex_compute
  end type nx_cs_fssh_t

  integer, parameter :: TWO_RHE = 1
  !! Two resonances, first with harmonic model and second with exponential model
  integer, parameter :: TWO_REH = 2
  !! Two resonances, first with harmonic model and second with exponential model
  integer, parameter :: TWO_REE = 3
  !! Two exponential resonances model
  integer, parameter :: TWO_RHH = 4
  !! Two harmonic resonances model
contains

  subroutine complex_setup(self, parser, conffile)
    class(nx_cs_fssh_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser
    character(len=*), intent(in) :: conffile

    type(parser_t) :: parser_complex

    call parser_complex%init(conffile)
    call parser_complex%parse()
    call check_error(parser_complex%status, 10, 'Problem in parsing conffile')

    call set(parser, 'cs_fssh', self%cs_mod, 'cs_mod')
    call set(parser, 'cs_fssh', self%use_lapack, 'use_lapack')

    if (self%cs_mod == TWO_RHE) then
       allocate( self%e(2:2) )
       allocate( self%h(0:1) )
       allocate( self%vi(1:2) )
       allocate( self%g(2) )

       ! Reference (0) and first harmonic potential (1)
       self%h(0) = nx_harmonic_pot_t(3.0_dp, 0.0_dp, 0.0_dp)
       self%h(1) = nx_harmonic_pot_t(3.0_dp, 0.25_dp, 1.0_dp)

       ! Exponential potential (second potential)
       self%e(2) = nx_exponential_pot_t(0.8_dp, -1.0_dp, 2.0_dp)

       ! Real (1) and imaginary (2) part of the interacting potential
       self%g(1) = nx_gaussian_pot_t(0.464445_dp, 0.03_dp, 40.0_dp)
       self%g(2) = nx_gaussian_pot_t(0.0_dp, 0.0_dp, 0.0_dp)

       ! Imaginary part of exponential (1) and harmonic (2) potential
       self%vi(1) = nx_imag_pot_t(0.1_dp)
       self%vi(2) = nx_imag_pot_t(0.2_dp)

    else if (self%cs_mod == TWO_REH) then
       allocate( self%e(1) )
       allocate( self%h(0:1) )
       allocate( self%vi(1:2) )
       allocate( self%g(2) )

       ! Reference (0) and first harmonic potential (1)
       self%h(0) = nx_harmonic_pot_t(3.0_dp, 0.0_dp, 0.0_dp)
       self%h(1) = nx_harmonic_pot_t(3.0_dp, 0.25_dp, 1.0_dp) ! This is potential 2 !!

       ! Exponential potential (second potential)
       self%e(1) = nx_exponential_pot_t(0.8_dp, -1.0_dp, 2.0_dp)

       ! Real (1) and imaginary (2) part of the interacting potential
       self%g(1) = nx_gaussian_pot_t(0.464445_dp, 0.03_dp, 40.0_dp)
       self%g(2) = nx_gaussian_pot_t(0.0_dp, 0.0_dp, 0.0_dp)

       ! Imaginary part of exponential (1) and harmonic (2) potential
       self%vi(1) = nx_imag_pot_t(0.1_dp)
       self%vi(2) = nx_imag_pot_t(0.2_dp)

    else if (self%cs_mod == TWO_REE) then
       allocate( self%e(1:2) )
       allocate( self%h(0:0) )
       allocate( self%vi(1:2) )
       allocate( self%g(2) )

       ! Reference (0) potential
       self%h(0) = nx_harmonic_pot_t(3.0_dp, 0.0_dp, 0.0_dp)

       ! Exponential potential (first and second potential)
       self%e(1) = nx_exponential_pot_t(0.8_dp, -1.0_dp, 2.0_dp)
       self%e(2) = nx_exponential_pot_t(0.6_dp, -1.0_dp, 1.5_dp)

       ! Real (1) and imaginary (2) part of the interacting potential
       self%g(1) = nx_gaussian_pot_t(0.607738_dp, 0.03_dp, 40.0_dp)
       self%g(2) = nx_gaussian_pot_t(0.0_dp, 0.0_dp, 0.0_dp)

       ! Imaginary part of potentials
       self%vi(1) = nx_imag_pot_t(0.1_dp)
       self%vi(2) = nx_imag_pot_t(0.2_dp)

    else if (self%cs_mod == TWO_RHH) then
       allocate( self%h(0:2) )
       allocate( self%vi(1:2) )
       allocate( self%g(2) )

       ! Reference (0) and first harmonic potential (1)
       self%h(0) = nx_harmonic_pot_t(3.0_dp, 0.0_dp, 0.0_dp)
       self%h(1) = nx_harmonic_pot_t(3.0_dp, 0.25_dp, 1.0_dp)
       self%h(2) = nx_harmonic_pot_t(1.0_dp, 0.25_dp, 1.5_dp)

       ! Real (1) and imaginary (2) part of the interacting potential
       self%g(1) = nx_gaussian_pot_t(0.75_dp, 0.03_dp, 40.0_dp)
       self%g(2) = nx_gaussian_pot_t(0.0_dp, 0.0_dp, 0.0_dp)

       ! Imaginary part of exponential (1) and harmonic (2) potential
       self%vi(1) = nx_imag_pot_t(0.1_dp)
       self%vi(2) = nx_imag_pot_t(0.2_dp)
    end if

    call complex_init( self, parser_complex )

    call parser_complex%clean()
  end subroutine complex_setup


  subroutine complex_init(self, parser_complex)
    class(nx_cs_fssh_t), intent(inout) :: self
    type(parser_t), intent(in) :: parser_complex

    real(dp), allocatable :: tmp(:)

    call set_alloc(parser_complex, 'cs_fssh', tmp, 'ref_pot')
    if (allocated(tmp)) then
       self%h(0) = nx_harmonic_pot_t(tmp(1), tmp(2), tmp(3))
    end if

    if (self%cs_mod == TWO_RHE) then
       call set_alloc(parser_complex, 'cs_fssh', tmp, 'harmonic_1')
       if (allocated(tmp)) then
          self%h(1) = nx_harmonic_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'exponential_2')
       if (allocated(tmp)) then
          self%e(2) = nx_exponential_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'vint_r')
       if (allocated(tmp)) then
          self%g(1) = nx_gaussian_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'vint_i')
       if (allocated(tmp)) then
          self%g(2) = nx_gaussian_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'harmonic_1_i')
       if (allocated(tmp)) then
          self%vi(1) = nx_imag_pot_t(tmp(1))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'exponential_2_i')
       if (allocated(tmp)) then
          self%vi(2) = nx_imag_pot_t(tmp(1))
       end if

    else if (self%cs_mod == TWO_REH) then
       call set_alloc(parser_complex, 'cs_fssh', tmp, 'harmonic_2')
       if (allocated(tmp)) then
          self%h(1) = nx_harmonic_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'exponential_1')
       if (allocated(tmp)) then
          self%e(1) = nx_exponential_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'vint_r')
       if (allocated(tmp)) then
          self%g(1) = nx_gaussian_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'vint_i')
       if (allocated(tmp)) then
          self%g(2) = nx_gaussian_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'harmonic_2_i')
       if (allocated(tmp)) then
          self%vi(2) = nx_imag_pot_t(tmp(1))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'exponential_1_i')
       if (allocated(tmp)) then
          self%vi(1) = nx_imag_pot_t(tmp(1))
       end if

    else if (self%cs_mod == TWO_REE) then
       call set_alloc(parser_complex, 'cs_fssh', tmp, 'exponential_1')
       if (allocated(tmp)) then
          self%e(1) = nx_exponential_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'exponential_2')
       if (allocated(tmp)) then
          self%e(2) = nx_exponential_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'vint_r')
       if (allocated(tmp)) then
          self%g(1) = nx_gaussian_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'vint_i')
       if (allocated(tmp)) then
          self%g(2) = nx_gaussian_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'exponential_1_i')
       if (allocated(tmp)) then
          self%vi(1) = nx_imag_pot_t(tmp(1))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'exponential_2_i')
       if (allocated(tmp)) then
          self%vi(2) = nx_imag_pot_t(tmp(1))
       end if

    else if (self%cs_mod == TWO_RHH) then
       call set_alloc(parser_complex, 'cs_fssh', tmp, 'harmonic_1')
       if (allocated(tmp)) then
          self%h(1) = nx_harmonic_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'harmonic_2')
       if (allocated(tmp)) then
          self%h(2) = nx_harmonic_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'vint_r')
       if (allocated(tmp)) then
          self%g(1) = nx_gaussian_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'vint_i')
       if (allocated(tmp)) then
          self%g(2) = nx_gaussian_pot_t(tmp(1), tmp(2), tmp(3))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'harmonic_1_i')
       if (allocated(tmp)) then
          print *, 'FOUND HARMONIC_I_1'
          self%vi(1) = nx_imag_pot_t(tmp(1))
       end if

       call set_alloc(parser_complex, 'cs_fssh', tmp, 'harmonic_2_i')
       if (allocated(tmp)) then
          self%vi(2) = nx_imag_pot_t(tmp(1))
       end if
    end if
  end subroutine complex_init


  subroutine complex_print(self, out)
    class(nx_cs_fssh_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out

    if (self%cs_mod == TWO_RHE) then
       write(output, *) 'CS-FSSH using model: 2RHE'

       call print_conf_ele(self%h(0)%k,    'k_0', unit=output)
       call print_conf_ele(self%h(0)%req,  'r_0', unit=output)
       call print_conf_ele(self%h(0)%vmin, 'V_0', unit=output)

       call print_conf_ele(self%h(1)%k,    'k_1', unit=output)
       call print_conf_ele(self%h(1)%req,  'r_1', unit=output)
       call print_conf_ele(self%h(1)%vmin, 'V_1', unit=output)

       call print_conf_ele(self%e(2)%alpha, 'alpha_2', unit=output)
       call print_conf_ele(self%e(2)%d,  'd_2', unit=output)
       call print_conf_ele(self%e(2)%v0, 'V0_2', unit=output)

       call print_conf_ele(self%vi(1)%gr, 'harmonic_i_1', unit=output)
       call print_conf_ele(self%vi(2)%gr, 'exponential_i_2', unit=output)

       call print_conf_ele(self%g(1)%rc, 'rc_r', unit=output)
       call print_conf_ele(self%g(1)%vmax,  'vmax_r', unit=output)
       call print_conf_ele(self%g(1)%beta, 'beta_r', unit=output)

       call print_conf_ele(self%g(2)%rc, 'rc_i', unit=output)
       call print_conf_ele(self%g(2)%vmax,  'vmax_i', unit=output)
       call print_conf_ele(self%g(2)%beta, 'beta_i', unit=output)

    else if (self%cs_mod == TWO_REH) then
       write(output, *) 'CS-FSSH using model: 2REH'

       call print_conf_ele(self%h(0)%k,    'k_0', unit=output)
       call print_conf_ele(self%h(0)%req,  'r_0', unit=output)
       call print_conf_ele(self%h(0)%vmin, 'V_0', unit=output)

       call print_conf_ele(self%h(1)%k,    'k_2', unit=output)
       call print_conf_ele(self%h(1)%req,  'r_2', unit=output)
       call print_conf_ele(self%h(1)%vmin, 'V_2', unit=output)

       call print_conf_ele(self%e(1)%alpha, 'alpha_1', unit=output)
       call print_conf_ele(self%e(1)%d,  'd_1', unit=output)
       call print_conf_ele(self%e(1)%v0, 'V0_1', unit=output)

       call print_conf_ele(self%vi(1)%gr, 'exponential_i_1', unit=output)
       call print_conf_ele(self%vi(2)%gr, 'harmonic_i_2', unit=output)

       call print_conf_ele(self%g(1)%rc, 'rc_r', unit=output)
       call print_conf_ele(self%g(1)%vmax,  'vmax_r', unit=output)
       call print_conf_ele(self%g(1)%beta, 'beta_r', unit=output)

       call print_conf_ele(self%g(2)%rc, 'rc_i', unit=output)
       call print_conf_ele(self%g(2)%vmax,  'vmax_i', unit=output)
       call print_conf_ele(self%g(2)%beta, 'beta_i', unit=output)

    else if (self%cs_mod == TWO_REE) then
       write(output, *) 'CS-FSSH using model: 2REE'

       call print_conf_ele(self%h(0)%k,    'k_0', unit=output)
       call print_conf_ele(self%h(0)%req,  'r_0', unit=output)
       call print_conf_ele(self%h(0)%vmin, 'V_0', unit=output)

       call print_conf_ele(self%e(1)%alpha, 'alpha_1', unit=output)
       call print_conf_ele(self%e(1)%d,  'd_1', unit=output)
       call print_conf_ele(self%e(1)%v0, 'V0_1', unit=output)

       call print_conf_ele(self%e(2)%alpha, 'alpha_2', unit=output)
       call print_conf_ele(self%e(2)%d,  'd_2', unit=output)
       call print_conf_ele(self%e(2)%v0, 'V0_2', unit=output)

       call print_conf_ele(self%vi(1)%gr, 'exponential_i_1', unit=output)
       call print_conf_ele(self%vi(2)%gr, 'exponential_i_2', unit=output)

       call print_conf_ele(self%g(1)%rc, 'rc_r', unit=output)
       call print_conf_ele(self%g(1)%vmax,  'vmax_r', unit=output)
       call print_conf_ele(self%g(1)%beta, 'beta_r', unit=output)

       call print_conf_ele(self%g(2)%rc, 'rc_i', unit=output)
       call print_conf_ele(self%g(2)%vmax,  'vmax_i', unit=output)
       call print_conf_ele(self%g(2)%beta, 'beta_i', unit=output)

    else if (self%cs_mod == TWO_RHH) then
       write(output, *) 'CS-FSSH using model: 2RHH'

       call print_conf_ele(self%h(0)%k,    'k_0', unit=output)
       call print_conf_ele(self%h(0)%req,  'r_0', unit=output)
       call print_conf_ele(self%h(0)%vmin, 'V_0', unit=output)

       call print_conf_ele(self%h(1)%k,    'k_1', unit=output)
       call print_conf_ele(self%h(1)%req,  'r_1', unit=output)
       call print_conf_ele(self%h(1)%vmin, 'V_1', unit=output)

       call print_conf_ele(self%h(2)%k,    'k_2', unit=output)
       call print_conf_ele(self%h(2)%req,  'r_2', unit=output)
       call print_conf_ele(self%h(2)%vmin, 'V_2', unit=output)

       call print_conf_ele(self%vi(1)%gr, 'harmonic_i_1', unit=output)
       call print_conf_ele(self%vi(2)%gr, 'harmonic_i_2', unit=output)

       call print_conf_ele(self%g(1)%rc, 'rc_r', unit=output)
       call print_conf_ele(self%g(1)%vmax,  'vmax_r', unit=output)
       call print_conf_ele(self%g(1)%beta, 'beta_r', unit=output)

       call print_conf_ele(self%g(2)%rc, 'rc_i', unit=output)
       call print_conf_ele(self%g(2)%vmax,  'vmax_i', unit=output)
       call print_conf_ele(self%g(2)%beta, 'beta_i', unit=output)
    end if

  end subroutine complex_print


  subroutine complex_compute(self, r, epot, grad, nad, nad_i, gam)
    class(nx_cs_fssh_t), intent(inout) :: self
    real(dp), intent(in) :: r
    real(dp), intent(inout) :: epot(:)
    real(dp), intent(inout) :: grad(:)
    real(dp), intent(inout) :: nad(:)
    real(dp), intent(inout) :: nad_i(:)
    real(dp), intent(inout) :: gam(:)

    complex(dp) :: v(2, 2)
    complex(dp) :: dv(2, 2)

    if (self%cs_mod == TWO_RHE) then
       call self%h(0)%compute(r)
       call self%h(1)%compute(r)
       call self%e(2)%compute(r)
       self%vi(1)%vr = self%h(1)%v
       self%vi(2)%vr = self%e(2)%v
       self%vi(1)%v0 = self%h(0)%v
       self%vi(2)%v0 = self%h(0)%v
       self%vi(1)%grad_vr = self%h(1)%dv
       self%vi(2)%grad_vr = self%e(2)%dv
       self%vi(1)%grad_v0 = self%h(0)%dv
       self%vi(2)%grad_v0 = self%h(0)%dv
       call self%vi(1)%compute(r)
       call self%vi(2)%compute(r)
       call self%g(1)%compute(r)
       call self%g(2)%compute(r)

       if (self%use_lapack) then
          v(1, 1) = complex(self%h(1)%v, self%vi(1)%v)
          v(2, 2) = complex(self%e(2)%v, self%vi(2)%v)
          v(2, 1) = complex(self%g(1)%v, self%g(2)%v)
          v(1, 2) = v(2, 1)

          dv(1, 1) = complex(self%h(1)%dv, self%vi(1)%dv)
          dv(2, 2) = complex(self%e(2)%dv, self%vi(2)%dv)
          dv(2, 1) = complex(self%g(1)%dv, self%g(2)%dv)
          dv(1, 2) = dv(2, 1)
       else
          v(1, 1) = complex(self%h(1)%v, -self%vi(1)%v / 2.0_dp)
          v(2, 2) = complex(self%e(2)%v, -self%vi(2)%v / 2.0_dp)
          v(2, 1) = complex(self%g(1)%v, -self%g(2)%v / 2.0_dp)
          v(1, 2) = v(2, 1)

          dv(1, 1) = complex(self%h(1)%dv, -self%vi(1)%dv / 2.0_dp)
          dv(2, 2) = complex(self%e(2)%dv, -self%vi(2)%dv / 2.0_dp)
          dv(2, 1) = complex(self%g(1)%dv, -self%g(2)%dv / 2.0_dp)
          dv(1, 2) = dv(2, 1)
       end if


    else if (self%cs_mod == TWO_REH) then
       call self%h(0)%compute(r)
       call self%h(1)%compute(r)
       call self%e(1)%compute(r)
       self%vi(1)%vr = self%e(1)%v
       self%vi(2)%vr = self%h(1)%v
       self%vi(1)%v0 = self%h(0)%v
       self%vi(2)%v0 = self%h(0)%v
       self%vi(1)%grad_vr = self%e(1)%dv
       self%vi(2)%grad_vr = self%h(1)%dv
       self%vi(1)%grad_v0 = self%h(0)%dv
       self%vi(2)%grad_v0 = self%h(0)%dv
       call self%vi(1)%compute(r)
       call self%vi(2)%compute(r)
       call self%g(1)%compute(r)
       call self%g(2)%compute(r)

       if (self%use_lapack) then
          v(1, 1) = complex(self%e(1)%v, self%vi(1)%v)
          v(2, 2) = complex(self%h(1)%v, self%vi(2)%v)
          v(2, 1) = complex(self%g(1)%v, self%g(2)%v)
          v(1, 2) = v(2, 1)

          dv(1, 1) = complex(self%e(1)%dv, self%vi(1)%dv)
          dv(2, 2) = complex(self%h(1)%dv, self%vi(2)%dv)
          dv(2, 1) = complex(self%g(1)%dv, self%g(2)%dv)
          dv(1, 2) = dv(2, 1)
       else
          v(1, 1) = complex(self%e(1)%v, -self%vi(1)%v / 2.0_dp)
          v(2, 2) = complex(self%h(1)%v, -self%vi(2)%v / 2.0_dp)
          v(2, 1) = complex(self%g(1)%v, -self%g(2)%v / 2.0_dp)
          v(1, 2) = v(2, 1)

          dv(1, 1) = complex(self%e(1)%dv, -self%vi(1)%dv / 2.0_dp)
          dv(2, 2) = complex(self%h(1)%dv, -self%vi(2)%dv / 2.0_dp)
          dv(2, 1) = complex(self%g(1)%dv, -self%g(2)%dv / 2.0_dp)
          dv(1, 2) = dv(2, 1)
       end if


    else if (self%cs_mod == TWO_REE) then
       call self%h(0)%compute(r)
       call self%e(1)%compute(r)
       call self%e(2)%compute(r)
       self%vi(1)%vr = self%e(1)%v
       self%vi(2)%vr = self%e(2)%v
       self%vi(1)%v0 = self%h(0)%v
       self%vi(2)%v0 = self%h(0)%v
       self%vi(1)%grad_vr = self%e(1)%dv
       self%vi(2)%grad_vr = self%e(2)%dv
       self%vi(1)%grad_v0 = self%h(0)%dv
       self%vi(2)%grad_v0 = self%h(0)%dv
       call self%vi(1)%compute(r)
       call self%vi(2)%compute(r)
       call self%g(1)%compute(r)
       call self%g(2)%compute(r)

       if (self%use_lapack) then
          v(1, 1) = complex(self%e(1)%v, self%vi(1)%v)
          v(2, 2) = complex(self%e(2)%v, self%vi(2)%v)
          v(2, 1) = complex(self%g(1)%v, self%g(2)%v)
          v(1, 2) = v(2, 1)

          dv(1, 1) = complex(self%e(1)%dv, self%vi(1)%dv)
          dv(2, 2) = complex(self%e(2)%dv, self%vi(2)%dv)
          dv(2, 1) = complex(self%g(1)%dv, self%g(2)%dv)
          dv(1, 2) = dv(2, 1)
       else
          v(1, 1) = complex(self%e(1)%v, -self%vi(1)%v / 2.0_dp)
          v(2, 2) = complex(self%e(2)%v, -self%vi(2)%v / 2.0_dp)
          v(2, 1) = complex(self%g(1)%v, -self%g(2)%v / 2.0_dp)
          v(1, 2) = v(2, 1)

          dv(1, 1) = complex(self%e(1)%dv, -self%vi(1)%dv / 2.0_dp)
          dv(2, 2) = complex(self%e(2)%dv, -self%vi(2)%dv / 2.0_dp)
          dv(2, 1) = complex(self%g(1)%dv, -self%g(2)%dv / 2.0_dp)
          dv(1, 2) = dv(2, 1)
       end if

    else if (self%cs_mod == TWO_RHH) then
       call self%h(0)%compute(r)
       call self%h(1)%compute(r)
       call self%h(2)%compute(r)
       self%vi(1)%vr = self%h(1)%v
       self%vi(2)%vr = self%h(2)%v
       self%vi(1)%v0 = self%h(0)%v
       self%vi(2)%v0 = self%h(0)%v
       self%vi(1)%grad_vr = self%h(1)%dv
       self%vi(2)%grad_vr = self%h(2)%dv
       self%vi(1)%grad_v0 = self%h(0)%dv
       self%vi(2)%grad_v0 = self%h(0)%dv
       call self%vi(1)%compute(r)
       call self%vi(2)%compute(r)
       call self%g(1)%compute(r)
       call self%g(2)%compute(r)

       if (self%use_lapack) then
          v(1, 1) = complex(self%h(1)%v, self%vi(1)%v)
          v(2, 2) = complex(self%h(2)%v, self%vi(2)%v)
          v(1, 2) = complex(self%g(1)%v, self%g(2)%v)
          v(2, 1) = v(1, 2)
          dv(1, 1) = complex(self%h(1)%dv, self%vi(1)%dv)
          dv(2, 2) = complex(self%h(2)%dv, self%vi(2)%dv)
          dv(1, 2) = complex(self%g(1)%dv, self%g(2)%dv)
          dv(2, 1) = dv(1, 2)
       else
          v(1, 1) = complex(self%h(1)%v, -self%vi(1)%v / 2.0_dp)
          v(2, 2) = complex(self%h(2)%v, -self%vi(2)%v / 2.0_dp)
          v(2, 1) = complex(self%g(1)%v, -self%g(2)%v / 2.0_dp)
          v(1, 2) = v(2, 1)

          dv(1, 1) = complex(self%h(1)%dv, -self%vi(1)%dv / 2.0_dp)
          dv(2, 2) = complex(self%h(2)%dv, -self%vi(2)%dv / 2.0_dp)
          dv(2, 1) = complex(self%g(1)%dv, -self%g(2)%dv / 2.0_dp)
          dv(1, 2) = dv(2, 1)
       end if

    end if

    call nx_log%log(LOG_DEBUG, real(v),   title='Diabatic Re(V) matrix')
    call nx_log%log(LOG_DEBUG, aimag(v),  title='Diabatic Im(V) matrix')
    call nx_log%log(LOG_DEBUG, real(dv),  title='Diabatic Re(dV) matrix')
    call nx_log%log(LOG_DEBUG, aimag(dv), title='Diabatic Im(dV) matrix')

    if (self%use_lapack) then
       call diabatic_to_adiabatic_lapack(v, dv, epot, grad, nad, nad_i, gam)
    else
       call diabatic_to_adiabatic_new(v, dv, epot, grad, nad, nad_i, gam)
    end if

  end subroutine complex_compute


  subroutine diabatic_to_adiabatic(v, dv, epot, grad, nad, nad_i, gam)
    complex(dp), intent(in) :: v(2, 2)
    complex(dp), intent(in) :: dv(2, 2)
    real(dp), intent(inout) :: epot(:)
    real(dp), intent(inout) :: grad(:)
    real(dp), intent(inout) :: nad(:)
    real(dp), intent(inout) :: nad_i(:)
    real(dp), intent(inout) :: gam(:)

    complex(dp)  :: v1
    complex(dp)  :: v2
    complex(dp)  :: v12
    complex(dp)  :: grad1
    complex(dp)  :: grad2
    complex(dp)  :: grad12

    ! Complex adiabatic potentials
    ! real(dp) :: en(2)
    complex(dp) :: v_a1, v_a2, dv_a(2, 2), v_a(2, 2)
    ! Complex adiabatic gradients
    complex(dp) :: grad_a1, grad_a2, grad_a(2, 2)
    ! Complex nonadiabatic coupling
    complex(dp) :: nac_a12

    ! v_a is the adiabatic potential, and c is the transformation matrix
    complex(dp) :: vtest_a(2, 2), c(2, 2)

    real(dp) :: vr_a1, vr_a2, vi_a1, vi_a2
    real(dp) :: gradr_a1, gradr_a2, nacr_a12, naci_a12
    real(dp) :: gradi_a1, gradi_a2

    ! Auxiliary variables
    complex(dp) :: sv, u0, arg, dvv

    complex(dp) :: delta, en1, en2, a, b, d, trans(2, 2), tmp
    real(dp) :: n1, n2

    v1 = v(1, 1)
    v2 = v(2, 2)
    v12 = v(1, 2)

    grad1 = dv(1, 1)
    grad2 = dv(2, 2)
    grad12 = dv(1, 2)

    sv = v1 + v2
    dvv = v1 - v2
    u0 = 2.0_dp * v12
    arg = dvv*dvv + u0*u0
    v_a1 = ( sv - ( arg )**0.5_dp ) / 2.0_dp
    v_a2 = ( sv + ( arg )**0.5_dp ) / 2.0_dp

    vr_a1 = real( v_a1 )
    vr_a2 = real( v_a2 )
    vi_a1 = -2.0_dp * aimag( v_a1 )
    vi_a2 = -2.0_dp * aimag( v_a2 )

    v_a(1, 1) = complex(vr_a1, vi_a1)
    v_a(2, 2) = complex(vr_a2, vi_a2)
    v_a(2, 1) = 0.0_dp
    v_a(1, 2) = v_a(2, 1)

    grad_a1 = ( grad1 + grad2 ) / 2.0_dp - ( (v2-v1) * (grad2-grad1) + 4.0_dp*v12*grad12 ) &
         / sqrt( (v2-v1)**2 + 4.0_dp*v12**2 ) / 2.0_dp
    grad_a2 = ( grad1 + grad2 ) / 2.0_dp + ( (v2-v1) * (grad2-grad1) + 4.0_dp*v12*grad12 ) &
         / sqrt( (v2-v1)**2 + 4.0_dp*v12**2 ) / 2.0_dp

    gradr_a1  = real( grad_a1 )
    gradr_a2  = real( grad_a2 )
    gradi_a1  = -2.0_dp * aimag( grad_a1 )
    gradi_a2  = -2.0_dp * aimag( grad_a2 )

    grad_a(1, 1) = complex(gradr_a1, gradi_a1)
    grad_a(2, 2) = complex(gradr_a2, gradi_a2)
    grad_a(2, 1) = 0.0_dp
    grad_a(1, 2) = grad_a(2, 1)

    nac_a12  = ( dv(1, 2) * (v(2,2)-v(1,1)) - v(1,2) * (dv(2,2)-dv(1,1)) ) &
         & / ( (v(2,2)-v(1,1))**2 + 4.0_dp*v(1,2)**2 )
    nacr_a12 = real( nac_a12 )
    naci_a12 = aimag( nac_a12 )
    nad(1) = -nacr_a12

    nad_i(1) = -naci_a12

    call nx_log%log(LOG_DEBUG, real(v_a),   title='Adiabatic Re(V) matrix')
    call nx_log%log(LOG_DEBUG, aimag(v_a),  title='Adiabatic Im(V) matrix')
    call nx_log%log(LOG_DEBUG, real(grad_a),  title='Adiabatic Re(dV) matrix')
    call nx_log%log(LOG_DEBUG, aimag(grad_a), title='Adiabatic Im(dV) matrix')
    ! block
    !   character(len=MAX_STR_SIZE) :: msg
    !   write(msg, '(A,F20.12)') 'Re(NAC12): ', nad(1)! 2.0_dp * real(dv_a(1, 2))
    !   call nx_log%log(LOG_DEBUG, msg)
    !   write(msg, '(A,F20.12)') 'Im(NAC12): ', nad_i(1)! 2.0_dp * aimag(dv_a(1, 2))
    !   call nx_log%log(LOG_DEBUG, msg)
    ! end block

    epot(1) = vr_a1 / au2ev
    epot(2) = vr_a2 / au2ev

    grad(1) = gradr_a1 / au2ev
    grad(2) = gradr_a2 / au2ev

    nad(1) = -nacr_a12

    nad_i(1) = -naci_a12

    gam(1) = vi_a1 / au2ev
    gam(2) = vi_a2 / au2ev
  end subroutine diabatic_to_adiabatic


  subroutine diabatic_to_adiabatic_new(v, dv, epot, grad, nad, nad_i, gam)
    complex(dp), intent(in) :: v(:, :)
    complex(dp), intent(in) :: dv(:, :)
    real(dp), intent(inout) :: epot(:)
    real(dp), intent(inout) :: grad(:)
    real(dp), intent(inout) :: nad(:)
    real(dp), intent(inout) :: nad_i(:)
    real(dp), intent(inout) :: gam(:)

    complex(dp) :: e1, e2, delta, n1, n2, det
    complex(dp) :: c(2, 2), v_a(2, 2), tmp(2, 2), dv_a(2, 2), cinv(2, 2)

    delta = (v(1, 1) - v(2, 2))**2 + 4 * abs(v(1, 2))**2
    e1 = (v(1, 1) + v(2, 2) - sqrt(delta)) * 0.5_dp
    e2 = (v(1, 1) + v(2, 2) + sqrt(delta)) * 0.5_dp

    n1 = sqrt(abs(v(1, 2))**2 + abs(e1 - v(1, 1))**2)
    n2 = sqrt(abs(v(1, 2))**2 + abs(e2 - v(1, 1))**2)
    
    c(1, 1) = v(1, 2) / n1
    c(1, 2) = v(1, 2) / n2
    c(2, 1) = (e1 - v(1, 1)) / n1
    c(2, 2) = (e2 - v(1, 1)) / n2

    ! det = c(1,1)*c(2,2) - c(1,2)*c(2,1)
    det = v(1, 2) * sqrt(delta) / (n1 * n2)
    cinv(1, 1) = c(2, 2)
    cinv(1, 2) = -c(1, 2)
    cinv(2, 1) = -c(2, 1)
    cinv(2, 2) = c(1, 1)
    cinv = cinv / det

    ! tmp(1, 1) = v(1,1)*c(1,1) + v(1, 2)*c(2, 1)
    ! tmp(1, 2) = v(1,1)*c(1,2) + v(1, 2)*c(2, 2)
    ! tmp(2, 1) = v(2,1)*c(1,1) + v(2, 2)*c(2, 1)
    ! tmp(2, 2) = v(2,1)*c(1,2) + v(2, 2)*c(2, 2)
    ! call nx_log%log(LOG_DEBUG, real(tmp),   title='Re(tmp 1) hand matrix')
    ! 
    ! v_a(1, 1) = c(1,1)*tmp(1,1) + c(2, 1)*tmp(2, 1)
    ! v_a(1, 2) = c(1,1)*tmp(1,2) + c(2, 1)*tmp(2, 2)
    ! v_a(2, 1) = c(1,2)*tmp(1,1) + c(2, 2)*tmp(2, 1)
    ! v_a(2, 2) = c(1,2)*tmp(1,2) + c(2, 2)*tmp(2, 2)
    ! call nx_log%log(LOG_DEBUG, real(v_a),   title='Re(tmp 2) hand matrix')

    tmp = matmul( v, c)
    v_a = matmul( cinv, tmp )

    tmp = matmul(dv, c)
    dv_a = matmul( cinv, tmp)

    ! call nx_log%log(LOG_DEBUG, real(c),   title='Re(c) matrix')
    ! call nx_log%log(LOG_DEBUG, aimag(c),   title='Im(c) matrix')
    call nx_log%log(LOG_DEBUG, real(v_a),   title='Adiabatic Re(V) matrix')
    call nx_log%log(LOG_DEBUG, aimag(v_a),  title='Adiabatic Im(V) matrix')
    call nx_log%log(LOG_DEBUG, real(dv_a),  title='Adiabatic Re(dV) matrix')
    call nx_log%log(LOG_DEBUG, aimag(dv_a), title='Adiabatic Im(dV) matrix')
    
    grad(1) = real(dv_a(1, 1)) / au2ev
    grad(2) = real(dv_a(2, 2)) / au2ev

    epot(1) = real(e1) / au2ev
    epot(2) = real(e2) / au2ev
    gam(1) = -2.0_dp * aimag(e1) / au2ev
    gam(2) = -2.0_dp * aimag(e2) / au2ev

    nad(1) = -real(( dv(1, 2) * (v(2,2)-v(1,1)) - v(1,2) * (dv(2,2)-dv(1,1)) ) &
         & / ( (v(2,2)-v(1,1))**2 + 4.0_dp*v(1,2)**2 ))
    nad_i(1) = -aimag(( dv(1, 2) * (v(2,2)-v(1,1)) - v(1,2) * (dv(2,2)-dv(1,1)) ) &
         & / ( (v(2,2)-v(1,1))**2 + 4.0_dp*v(1,2)**2 ))

    ! nad(1) = real(n2 / (n1*sqrt(delta)) * ( (e1-v(1,1))*(dv(2,2)-dv(1,1)) + dv(2,1)*v(1,2) - &
    !      & dv(1,2)/v(1,2) * ( delta / 2 - v(1,2)**2 - (v(2,2)-v(1,1))*sqrt(delta)*0.5_dp))&
    !      & / (e1 - e2))
    ! nad_i(1) = aimag(n2 / (n1*sqrt(delta)) * ( (e1-v(1,1))*(dv(2,2)-dv(1,1)) + dv(2,1)*v(1,2) - &
    !      & dv(1,2)/v(1,2) * ( delta / 2 - v(1,2)**2 - (v(2,2)-v(1,1))*sqrt(delta)*0.5_dp))&
    !      & / (e1 - e2))

    ! nad(1) = real(dv_a(2, 1) / (e1 - e2))
    ! nad_i(1) = aimag(dv_a(2, 1) / (e1 - e2))
  end subroutine diabatic_to_adiabatic_new
  


  subroutine diabatic_to_adiabatic_lapack(v, dv, epot, grad, nad, nad_i, gam)
    complex(dp), intent(in) :: v(:, :)
    complex(dp), intent(in) :: dv(:, :)
    real(dp), intent(inout) :: epot(:)
    real(dp), intent(inout) :: grad(:)
    real(dp), intent(inout) :: nad(:)
    real(dp), intent(inout) :: nad_i(:)
    real(dp), intent(inout) :: gam(:)

    complex(dp), allocatable :: work(:), dv_a(:, :), rwork(:), v_a(:, :)
    real(dp) :: nacr_a12, naci_a12
    complex(dp) :: nac_a12
    integer :: info
    integer, allocatable :: ipiv(:)
    complex(dp), allocatable :: tmp(:, :), vr(:, :), vl(:, :), en(:)
    integer :: dim, lwork, i, j, k, id1, id2, ncoupl

    dim = size(v, 1)
    allocate(tmp(dim, dim))
    allocate(vr(dim, dim))
    allocate(vl(dim, dim))
    allocate(v_a(dim, dim))
    allocate(dv_a(dim, dim))
    allocate(en(dim))
    allocate(ipiv(dim))

    ! 1. Eigenvalues and eigenvectors of the Hamiltonian.
    tmp = v
    ! Query the optimal workspace (lwork = -1)
    lwork = 2*dim
    allocate(work(lwork))
    allocate(rwork(lwork))
    call zgeev('V', 'V', dim, tmp, dim, en, vl, dim, vr, dim, work, lwork, rwork, info)
    ! lwork = int(work(1))
    ! deallocate(work, rwork)
    ! allocate(work(lwork), rwork(lwork))
    ! call zgeev('V', 'V', dim, tmp, dim, en, vl, dim, vr, dim, work, lwork, rwork, info)


    ! 2. Inverse of the transformation matrix of the right eigenvectors.
    tmp = vr
    call zgetrf(dim, dim, tmp, dim, ipiv, info)
    call zgetri(dim, tmp, dim, ipiv, work, lwork, info)

    dv_a = matmul(tmp, dv)
    dv_a = matmul(dv_a, vr)
    v_a = matmul(tmp, v)
    v_a = matmul(v_a, vr)

    nad(1) = -nacr_a12
    nad_i(1) = -naci_a12

    call nx_log%log(LOG_DEBUG, real(v_a),   title='Adiabatic Re(V) matrix')
    call nx_log%log(LOG_DEBUG, aimag(v_a),  title='Adiabatic Im(V) matrix')
    call nx_log%log(LOG_DEBUG, real(dv_a),  title='Adiabatic Re(dV) matrix')
    call nx_log%log(LOG_DEBUG, aimag(dv_a), title='Adiabatic Im(dV) matrix')
    ! block
    !   character(len=MAX_STR_SIZE) :: msg
    !   write(msg, '(A,F20.12)') 'Re(NAC12): ', nad(1)! 2.0_dp * real(dv_a(1, 2))
    !   call nx_log%log(LOG_DEBUG, msg)
    !   write(msg, '(A,F20.12)') 'Im(NAC12): ', nad_i(1)! 2.0_dp * aimag(dv_a(1, 2))
    !   call nx_log%log(LOG_DEBUG, msg)
    ! end block

    epot(:) = real(en(:)) / au2ev
    gam(:) = aimag(en(:)) / au2ev

    do i=1, size(grad)
       grad(i) = real(dv_a(i, i)) / au2ev
    end do

    ncoupl = dim * (dim-1) / 2
    k = 1
    do i=2, dim
       id1 = dim-i+2
       do j=1, dim-1
          id2 = dim-j
          nad(k) = real( dv_a(id1, id2) / (en(id2) - en(id1)) )
          nad_i(k) = aimag( dv_a(id1, id2) / (en(id2) - en(id1)) )
          k = k + 1
       end do
    end do
  end subroutine diabatic_to_adiabatic_lapack


end module mod_analytical_complex
