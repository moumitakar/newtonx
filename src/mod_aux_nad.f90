! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_aux_nad
  !! Module for approximate time-derivative couplings computations.
  !!
  !! This module implements the following models:
  !!
  !! - Baeck-An model
  !! [10.12688/openreseurope.13624.1](https://doi.org/10.12688/openreseurope.13624.1)
  !!
  use mod_kinds, only: dp
  use mod_trajectory, only: nx_traj_t
  use mod_constants, only: &
       & MAX_STR_SIZE, au2ev
  use mod_logger, only: &
       & nx_log, &
       & LOG_DEBUG, LOG_WARN, LOG_ERROR, &
       & print_conf_ele
  use mod_tools, only: reshape_epot
  use mod_numerics, only: quadratic_regression
  use mod_input_parser, only: &
       & parser_t, set => set_config
  
  implicit none

  type, public :: nx_auxnac_t
     !! Object handling the configuration for the computation of couplings with auxiliary
     !! methods.
     integer :: model = 0
     !! Model to use:
     !!
     !! - 0: Baeck-An couplings
     integer :: lvprt = 2
     !! Debug level (usually inherited from the main configuration)
     !!
     !! - 1, 2: Minimum debug info
     !! - > 2: Full debug

     ! Baeck-An couplings variables
     integer :: ba_smooth = 0
     !! Indicates if backward approximation (0) or quadratic regression (1) should be
     !! used to obtain the second derivatives of the energy difference.
     real(dp) :: ba_dh = 0.01_dp
     !! Threshold for the magnitude of the couplings variation between time-steps.
     real(dp) :: ba_de = 2.0_dp
     !! Threshold for energy difference between two states.
     real(dp) :: ba_dv = 0.1_dp
     !! Threshold for the product between the second derivative and the energy difference.

   contains
     procedure, pass :: init => init_auxnac
     procedure, pass :: print
     procedure, pass :: run
     procedure, pass :: baeck_an
  end type nx_auxnac_t

contains

  subroutine init_auxnac(this, parser, lvprt)
    !! Initialize the ``nx_auxnac_t`` object.
    !!
    !! The initialization is done with the ``auxnac`` namelist found in ``filename``, and
    !! ``lvprt`` can optionnally be specified (if not specified, the value is taken as 1
    !! for minimum debug information).
    class(nx_auxnac_t), intent(inout) :: this
    !! Object to initialize.
    type(parser_t), intent(in) :: parser
    integer, intent(in), optional :: lvprt
    !! Debug level.

    ! integer :: u

    ! integer :: model
    ! integer :: ba_smooth
    ! real(dp) :: ba_dh
    ! real(dp) :: ba_de
    ! real(dp) :: ba_dv
    ! 
    ! namelist /auxnac/ model, ba_smooth, ba_dh, ba_de, ba_dv
    ! 
    ! open(newunit=u, file=filename, action='read')
    ! read(u, nml=auxnac)
    ! close(u)
    ! 
    ! this%model = model
    ! this%ba_smooth = ba_smooth
    ! this%ba_dh = ba_dh
    ! this%ba_de = ba_de / au2ev
    ! this%ba_dv = ba_dv

    ! if (present(lvprt)) then
    !    this%lvprt = lvprt
    ! else
    !    this%lvprt = 1
    ! end if

    call set(parser, 'auxnac', this%model, 'model')
    call set(parser, 'auxnac', this%ba_smooth, 'ba_smooth')
    call set(parser, 'auxnac', this%ba_dh, 'ba_dh')
    call set(parser, 'auxnac', this%ba_de, 'ba_de')
    this%ba_de = this%ba_de / au2ev
    call set(parser, 'auxnac', this%ba_dv, 'ba_dv')
    if (present(lvprt)) then
       this%lvprt = lvprt
    else
       this%lvprt = 1
    end if
  end subroutine init_auxnac


  subroutine print(this)
    !! Print the current configuration.
    !!
    class(nx_auxnac_t), intent(in) :: this
    !! Object to print.

    write(*, '(A80)') repeat('*', 80)
    write(*, *) 'AUXNAD Configuration: '
    write(*, *) ''

    call print_conf_ele(this%model, 'model')

    if (this%model == 0) then
       call print_conf_ele(this%ba_smooth, 'ba_smooth')
       call print_conf_ele(this%ba_dh, 'ba_dh')
       call print_conf_ele(this%ba_de, 'ba_de')
       call print_conf_ele(this%ba_dv, 'ba_dv')
    end if

    write(*, '(A80)') repeat('*', 80)
    write(*, *) ''
  end subroutine print


  subroutine run(this, traj)
    !! Run the computation of the derivative couplings.
    !!
    class(nx_auxnac_t), intent(in) :: this
    !! ``auxnac`` configuration.
    type(nx_traj_t), intent(inout) :: traj
    !! Main NX trajectory object with potential energies, time-step, ...

    if (this%model == 0) then
       call this%baeck_an(traj)
    end if
  end subroutine run


  subroutine baeck_an(this, traj)
    !! Compute the couplings with Baeck-An model.
    !!
    !! This routine will also update ``traj`` with the couplings that have been computed.
    !!
    class(nx_auxnac_t), intent(in) :: this
    !! ``auxnac`` configuration.
    type(nx_traj_t), intent(inout) :: traj
    !! Main NX trajectory object.

    real(dp), allocatable :: couplings(:)
    real(dp), allocatable :: epot(:, :)
    real(dp), allocatable :: times(:)
    integer :: nstat, ncoupl

    nstat = size(traj%epot)
    call reshape_epot(&
         & traj%step, traj%dt, traj%epot, traj%old_epot, nstat, &
         & epot, times&
         &)

    ncoupl = nstat*(nstat - 1) / 2
    allocate(couplings(ncoupl))
    couplings(:) = 0.0_dp

    ! if (this%lvprt > 2) then
    !    write(*, *) ''
    !    write(*, '(A)') '***************************************'
    !    write(*, '(A)') 'Entering Baeck-An couplings calculation'
    !    write(*, '(A)') '***************************************'
    !    write(*, *) ''
    ! end if
    call nx_log%log(LOG_DEBUG, 'Entering Baeck-An couplings calculation')

    if (traj%step > 1) then
       if (this%ba_smooth == 0) then
          call nx_log%log(LOG_DEBUG, 'Using  backward approximation')
          call ba_backward_approx(this, epot, traj%dt, couplings)
       else if (this%ba_smooth == 1) then
          ! times(:) = times(:)
          call nx_log%log(LOG_DEBUG, 'Using quadratic regression')
          call ba_quadratic_regression(this, epot, times, couplings)
       end if
       call ba_transfer_couplings(this, couplings, traj)
    end if

    call nx_log%log(LOG_DEBUG, 'Finished Baeck-An couplings calculation')
  end subroutine baeck_an


  subroutine ba_backward_approx(auxnac, epot, dt, couplings)
    !! Compute the second derivative of the energy difference with backward-approximation.
    !!
    !! The second derivative is computed either as:
    !!
    !! \[ \frac{d^2 \Delta E_{JL} (t)}{dt^2} = \frac{1}{\Delta t^2} \left[ \Delta
    !! E_{JL}(t) - 2\Delta E_{JL} (t-\Delta t) + \Delta E_{JL} (t - 2\Delta t) \right], \]
    !!
    !! or
    !!
    !! \[ \frac{d^2 \Delta E_{JL} (t)}{dt^2} = \frac{1}{\Delta t^2} \left[ 2\Delta
    !! E_{JL}(t) - 5\Delta E_{JL} (t-\Delta t) + 4\Delta E_{JL} (t - 2\Delta t) - \Delta
    !! E_{JL} (t-3\Delta t) \right], \]
    !!
    !! depending on the available potential energies.
    !!
    !! For each couple of states, the conditions on ``ba_de`` and ``ba_dv`` are checked,
    !! as well as the sign of \( \frac{\Delta E''}{\Delta E} \). If one condition is
    !! violated, the corresponding coupling is set to 0.
    type(nx_auxnac_t), intent(in) :: auxnac
    !! Configuration for NAD computation.
    real(dp), intent(in) :: epot(:, :)
    !! Array of potential energies, with columns corresponding to states, and lines
    !! corresponding to the different time-steps in order ``(0, dt, 2dt, 3dt)``.
    real(dp), intent(in) :: dt
    !! Time-step (in fs).
    real(dp), intent(inout) :: couplings(:)
    !! Couplings computed.

    integer :: nstat, i, j, ncoupl
    real(dp) :: sgn, d2e, de0
    real(dp), dimension(:), allocatable :: de
    integer :: ns, cur
    character(len=MAX_STR_SIZE) :: msg

    nstat = size(epot, 2)
    cur = size(epot, 1)
    allocate(de(cur))

    ! Some debug printing
    call nx_log%log(LOG_DEBUG, epot, &
         & title='Potential energies (columns -> states)', &
         & expand=.true.)

    ncoupl = 1
    do i=2, nstat
       do j=1, i-1

          ! Create the array of energy differences as:
          ! de(cur) -> t
          ! de(cur - 1) -> t-dt
          ! ...
          do ns=1, cur
             de(ns) = epot(ns, j) - epot(ns, i)
          end do
          write(msg, '(A,I0,A,I0)') &
               & '* Couplings between states ', i, ' and ', j
          call nx_log%log(LOG_DEBUG, msg)
          write(msg, '(A,I0,A,I0,A)') &
               & 'Energy differences (state ', j, ' - state ', i, ')'
          call nx_log%log(LOG_DEBUG, de, &
               & title=msg &
               &)
          ! ba_de condition
          if ( abs(de(cur)) > auxnac%ba_de ) then
             couplings(ncoupl) = 0.0_dp
             ncoupl = ncoupl + 1
             write(msg, '(A,I0,A)') &
                  & 'ncoupl = ', ncoupl, &
                  & ' : SIGMA = 0 because de > ba_de'
             call nx_log%log(LOG_WARN, msg)
             cycle
          end if

          if (cur <= 3) then
             d2e = (1/dt**2) * (de(1) - 2*de(2) + de(cur))
          else
             d2e = (1/dt**2) * (2*de(cur) - 5*de(3) + 4*de(2) - de(1))
          end if

          write(msg, '(A,F20.12)') 'D2E = ', d2e
          call nx_log%log(LOG_DEBUG, msg)

          ! ba_dv condition
          if (abs( d2e*de(cur) ) > auxnac%ba_dv) then
             couplings(ncoupl) = 0.0_dp
             ncoupl = ncoupl + 1
             write(msg, '(A,I0,A)') &
                  & 'ncoupl = ', ncoupl, &
                  & " : SIGMA = 0 because DE'' * DE > ba_dv"
             call nx_log%log(LOG_WARN, msg)
             cycle
          end if

          de0 = d2e / de(cur)
          if (de0 <= 0) then
             couplings(ncoupl) = 0.0_dp
             ncoupl = ncoupl + 1
             write(msg, '(A,I0,A)') &
                  & 'ncoupl = ', ncoupl, &
                  & " : SIGMA = 0 because DE'' / DE <= 0"
             call nx_log%log(LOG_WARN, msg)
             cycle
          end if

          sgn = de(cur) / abs(de(cur))
          couplings(ncoupl) = (sgn / 2) * sqrt(de0)

          write(msg, '(A,F20.12)') 'Coupling = ', couplings(ncoupl)
          call nx_log%log(LOG_DEBUG, msg)

          ncoupl = ncoupl + 1
       end do
    end do
  end subroutine ba_backward_approx


  subroutine ba_quadratic_regression(auxnac, epot, times, couplings)
    !! Compute the second derivative of the energy difference as a quadratic
    !! approximation.
    !!
    !! The energy difference is estimated as:
    !!
    !! \[ \Delta E_{JL}(t) \approx \beta_0 + \beta_1 dt + \beta_2 dt^2, \]
    !!
    !! so that the second derivative corresponds to:
    !!
    !! \[ \frac{d^2 \Delta_{JL}(t)}{dt^2} \approx 2 \beta_2.\]
    !!
    !! This routine implements an algorithm based on the Vandermonde matrix, by
    !! reformulating the problem in matrix form:
    !!
    !! \[ \begin{bmatrix}
    !!       de(t_1) \\
    !!       de(t_2) \\
    !!       de(t_3) \\
    !!       \dots
    !!    \end{bmatrix} \approx
    !!    \begin{bmatrix}
    !!    1 & t_1 & t_1^2 \\
    !!    1 & t_2 & t_2^2 \\
    !!    1 & t_3 & t_3^2 \\
    !!    \dots
    !!    \end{bmatrix}
    !!    \begin{bmatrix}
    !!       \beta_0 \\
    !!       \beta_1 \\
    !!       \beta_2 \\
    !!    \end{bmatrix},
    !! \]
    !!
    !! or
    !!
    !! \[ \Delta E \approx \mathrm{\mathbf{X}} \beta \]
    !!
    !! The solution is then given by the formula:
    !!
    !! \[ \beta \approx (\mathrm{\mathbf{X}^{T}} \mathrm{\mathbf{X}})^{-1}
    !! \mathrm{\mathbf{X}^{T}} \Delta E \]
    !!
    !! For each couple of states, the conditions on ``ba_de`` and ``ba_dv`` are checked,
    !! as well as the sign of \( \frac{\Delta E''}{\Delta E} \). If one condition is
    !! violated, the corresponding coupling is set to 0.
    type(nx_auxnac_t), intent(in) :: auxnac
    !! Configuration for NAD computation.
    real(dp), intent(in) :: epot(:, :)
    !! Array of potential energies, with columns corresponding to states, and lines
    !! corresponding to the different time-steps in order ``(0, dt, 2dt, 3dt)``.
    real(dp), intent(in) :: times(:)
    !! Array corresponding to the different times at which the energies were computed,
    !! relative to the oldest one (which is 0).
    real(dp), intent(inout) :: couplings(:)
    !! Computed couplings.

    integer :: nstat, i, j, ncoupl
    real(dp) :: sgn, d2e, de0
    real(dp), dimension(:), allocatable :: de, poly
    integer :: ns, cur, info
    character(len=MAX_STR_SIZE) :: msg

    nstat = size(epot, 2)
    cur = size(epot, 1)
    allocate(de(cur))
    de(:) = 0.0_dp

    allocate( poly(3) )
    poly(:) = 0.0_dp

    write(msg, '(A,F20.12)') 'DT = ', times(2)
    call nx_log%log(LOG_DEBUG, msg)

    ! Some debug printing
    call nx_log%log(LOG_DEBUG, epot, &
         & title='Potential energies (columns -> states)', &
         & expand=.true.)

    ncoupl = 1
    do i=2, nstat
       do j=1, i-1
          do ns=1, cur
             de(ns) = epot(ns, j) - epot(ns, i)
          end do

          write(msg, '(A,I0,A,I0)') &
               & '* Couplings between states ', i, ' and ', j
          call nx_log%log(LOG_DEBUG, msg)
          write(msg, '(A,I0,A,I0,A)') &
               & 'Energy differences couple (state ', j, ' - state ', i, ')'
          call nx_log%log(LOG_DEBUG, de, &
               & title=msg &
               &)

          ! ba_de condition
          if ( abs(de(cur)) > auxnac%ba_de ) then
             couplings(ncoupl) = 0.0_dp
             ncoupl = ncoupl + 1
             write(msg, '(A,I0,A)') &
                  & 'ncoupl = ', ncoupl, &
                  & ' : SIGMA = 0 because de > ba_de'
             call nx_log%log(LOG_WARN, msg)
             cycle
          end if

          ! ``poly`` contains the coefficient of the 2nd degree interpolated
          ! polynomial: de(JL) = poly(1) + t*poly(2) + t**2 * poly(3)
          ! The second derivative we want is: d2(de) / dt**2 = 2*poly(3)
          call quadratic_regression(times, 2, de, poly, info)
          if (info /= 0) then
             call nx_log%log(LOG_ERROR, &
                  & 'Problem encountered in quadratic regression')
             error stop
          end if

          d2e = 2*poly(3)

          ! Debug printing
          call nx_log%log(LOG_DEBUG, poly, &
               & title='Polynomial coefficients')
          write(msg, '(A,F20.12)') 'D2E = ', d2e
          call nx_log%log(LOG_DEBUG, msg)

          if (abs( d2e*de(cur) ) > auxnac%ba_dv) then
             couplings(ncoupl) = 0.0_dp
             ncoupl = ncoupl + 1
             write(msg, '(A,I0,A)') &
                  & 'ncoupl = ', ncoupl, &
                  & " : SIGMA = 0 because DE'' * DE > ba_dv"
             call nx_log%log(LOG_WARN, msg)
             cycle
          end if

          de0 = d2e / de(cur)
          if (de0 <= 0) then
             couplings(ncoupl) = 0.0_dp
             ncoupl = ncoupl + 1
             write(msg, '(A,I0,A)') &
                  & 'ncoupl = ', ncoupl, &
                  & " : SIGMA = 0 because DE'' / DE <= 0"
             call nx_log%log(LOG_WARN, msg)
             cycle
          end if

          sgn = de(cur) / abs(de(cur))
          couplings(ncoupl) = (sgn / 2) * sqrt(de0)
          write(msg, '(A,F20.12)') 'Coupling = ', couplings(ncoupl)
          call nx_log%log(LOG_DEBUG, msg)

          ncoupl = ncoupl + 1
       end do
    end do

  end subroutine ba_quadratic_regression


  subroutine ba_transfer_couplings(auxnac, couplings, traj)
    !! Transfer the computed couplings to the main trajectory.
    !!
    !! At this step we check the variation of the couplings between two consecutive
    !! time-steps. If this variation exceeds ``ba_dh``, the couplings are not modified,
    !! and the old ones are kept.
    type(nx_auxnac_t), intent(in) :: auxnac
    !! Configuration for NAD computation.
    real(dp), dimension(:), intent(in) :: couplings
    !! Computed couplings.
    type(nx_traj_t), intent(inout) :: traj
    !! Main NX trajectory.

    integer :: i
    real(dp) :: dnad

    ! Copy current nad into old_nad
    traj%old_nad(:, :, :) = traj%nad(:, :, :)

    ! Now copy the couplings if the ba_dh condition is satisfied
    do i=1, size(couplings)
       dnad = abs(couplings(i)) - abs(traj%old_nad(i, 1, 1))
       if (dnad / traj%dt > auxnac%ba_dh) then
          call nx_log%log(LOG_WARN, 'ba_dh condition: reverting to old_nad !')
          ! res = message('ba_dh condition: reverting to old_nad !!', force=.true.)
          traj%nad(i, :, :) = traj%old_nad(i, :, :)
       else
          traj%nad(i, :, :) = abs(couplings(i))
       end if
    end do


    call nx_log%log(LOG_DEBUG, traj%nad(:, 1, 1), &
         & title='COMPUTED COUPLINGS')
    call nx_log%log(LOG_DEBUG, traj%old_nad(:, 1, 1), &
         & title='OLD COUPLINGS')
  end subroutine ba_transfer_couplings


end module mod_aux_nad
