! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_gaussian

  use mod_kinds, only: dp
  use mod_constants, only: &
       & MAX_CMD_SIZE, MAX_STR_SIZE, au2ev
  use mod_logger, only: &
       & nx_log, &
       & LOG_ERROR, LOG_DEBUG, &
       & call_external, check_error, print_conf_ele
  use mod_tools, only: split_blanks, split_pattern, to_str, remove_blanks
  use mod_print_utils, only: to_lower
  use mod_qminfo, only: nx_qminfo_t
  use mod_qm_t, only: nx_qm_t
  use mod_orbspace, only: nx_orbspace_t
  use mod_trajectory, only: nx_traj_t
  use mod_configuration, only: nx_config_t
  use mod_interface, only: copy, mkdir, gzip
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  integer, parameter :: GAU_ERR_PERL = 301
  integer, parameter :: GAU_ERR_MAIN = 302
  integer, parameter :: GAU_ERR_MISC = 303
  integer, parameter :: GAU_ERR_CONV = 304
  integer, parameter :: GAU_ERR_RWFDUMP = 305


  public :: gau_setup, gau_print
  public :: gau_update_input
  public :: gau_run
  public :: gau_read_dft
  public :: gau_read_orb
  public :: gau_dump_rwf
  public :: gau_get_mos_energies
  public :: gau_get_single_amplitudes
  public :: gau_get_lcao
  public :: gau_extract_overlap

  public :: gau_backup

  public :: gau_parse_input
  public :: gau_generate_route
  public :: gau_init_double_molecule
  public :: gau_prepare_double_molecule

contains

  subroutine gau_setup(nx_qm, parser)
    type(nx_qm_t), intent(inout) :: nx_qm
    type(parser_t), intent(in) :: parser

    ! integer :: mocoef
    ! integer :: prt_mo
    ! integer :: td_st
    ! integer :: kind_g09
    ! integer :: ld_thr
    !
    ! integer :: u
    character(len=MAX_STR_SIZE) :: env
    !
    ! namelist /gaussian/ mocoef, prt_mo, td_st, kind_g09, ld_thr
    !
    ! open(newunit=u, file=configfile, action='read')
    ! read(u, nml=gaussian)
    ! close(u)

    call set(parser, 'gaussian', nx_qm%gau_mocoef, 'mocoef')
    call set(parser, 'gaussian', nx_qm%gau_prt_mo, 'prt_mo')
    call set(parser, 'gaussian', nx_qm%gau_td_st, 'td_st')
    call set(parser, 'gaussian', nx_qm%gau_type, 'kind_g09')
    call set(parser, 'gaussian', nx_qm%gau_ld_thr, 'ld_thr')
    call set(parser, 'gaussian', nx_qm%gau_no_fc, 'no_fc')

    call get_environment_variable("g16root", env)
    nx_qm%ovl_cmd = trim(env)//'/g16/g16 sgaussian.com'
    nx_qm%ovl_out = 'sgaussian.log'
    nx_qm%ovl_post = trim(env)//'/g16/rwfdump overlap/sgaussian.rwf overlap/S_matrix 514R'
  end subroutine gau_setup


  subroutine gau_print(nx_qm, out)
    type(nx_qm_t), intent(in) :: nx_qm
    integer, intent(in), optional :: out

    integer :: output
    character(len=MAX_STR_SIZE) :: env

    output = stdout
    if (present(out)) output = out

    call get_environment_variable("g16root", env)
    call print_conf_ele(trim(env), 'g16root path', unit=output)
    call get_environment_variable("GAUSS_EXEDIR", env)
    call print_conf_ele(trim(env), 'GAUSS_EXEDIR path', unit=output)

    if (nx_qm%gau_mocoef /= -1) then
       call print_conf_ele(nx_qm%gau_mocoef, 'mocoef', unit=output)
    end if

    if (nx_qm%gau_prt_mo /= -1) then
       call print_conf_ele(nx_qm%gau_prt_mo, 'prt_mo', unit=output)
    end if

    if (nx_qm%gau_td_st /= -1) then
       call print_conf_ele(nx_qm%gau_td_st, 'td_st', unit=output)
    end if

    if (nx_qm%gau_type /= -1) then
       call print_conf_ele(nx_qm%gau_type, 'type', unit=output)
    end if

    if (nx_qm%gau_ld_thr /= -1) then
       call print_conf_ele(nx_qm%gau_ld_thr, 'ld_thr', unit=output)
    end if
  end subroutine gau_print


  subroutine gau_update_input(nx_qm, traj, conf)
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf

    integer :: nstatdyn
    integer :: nstat
    integer :: step

    integer :: u, v, ierr, i
    character(len=MAX_CMD_SIZE) :: link0, route, title, other
    character(len=MAX_STR_SIZE) :: buf, tmp
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    character(len=1) :: nl
    integer :: charge, mult
    logical :: ext

    step = traj%step - conf%init_step
    nstatdyn = traj%nstatdyn
    nstat = conf%nstat

    nl = NEW_LINE('c')

    ierr = 0

    if (nstatdyn > 1) then
       nx_qm%gau_typedft = 1
    else if ((nstatdyn == 1) .and. (nstat > 1)) then
       nx_qm%gau_typedft = 2
    else
       nx_qm%gau_typedft = 3
    end if

    call gau_parse_input(trim(nx_qm%job_folder)//'/gaussian.com', &
         & link0, route, title, charge, mult, other)
    route = gau_generate_route(route, nx_qm, traj, conf)
    call nx_log%log(LOG_DEBUG, 'GAUSSIAN NEWROUTE: '//trim(route))

    inquire(file=trim(nx_qm%job_folder)//'/basis', exist=ext)
    if (ext) then
       ierr = copy(trim(nx_qm%job_folder)//'/basis', 'basis')
       call check_error(ierr, GAU_ERR_MISC, 'GAUSSIAN: Cannot copy basis file')
    end if

    inquire(file=trim(nx_qm%job_folder)//'/basis2', exist=ext)
    if (ext) then
       ierr = copy(trim(nx_qm%job_folder)//'/basis2', 'basis2')
       call check_error(ierr, GAU_ERR_MISC, 'GAUSSIAN: Cannot copy basis2 file')
    end if

    if (traj%step == conf%init_step) then
       if (nx_qm%gau_mocoef /= 0) then
          inquire(file=trim(nx_qm%job_folder)//'gaussian.chk.ini', exist=ext)
          if (ext) then
             ierr = copy(trim(nx_qm%job_folder)//'/gaussian.chk.ini', 'gaussian.chk')
             call check_error(ierr, GAU_ERR_MISC, 'GAUSSIAN: Cannot copy gaussian.chk file')
          end if
       end if
    end if

    if (nx_qm%gau_mocoef == 2) then
       inquire(file=trim(nx_qm%job_folder)//'gaussian.chk.ini', exist=ext)
       if (ext) then
          ierr = copy(trim(nx_qm%job_folder)//'/gaussian.chk.ini', 'gaussian.chk')
          call check_error(ierr, GAU_ERR_MISC, 'GAUSSIAN: Cannot copy gaussian.chk file')
       end if
    end if

    if (index(link0, '%chk') == 0) then
       link0 = trim(link0)//nl//'%chk=gaussian.chk'
    end if

    if (index(link0, '%rwf') == 0) then
       link0 = trim(link0)//nl//'%rwf=gaussian.rwf'
    end if

    open(newunit=u, file='gaussian.com', action='write')
    write(u, '(a)') trim(link0)
    write(u, '(a)') trim(route)
    write(u, '(a)') ''
    write(u, '(a)') trim(title)
    write(u, '(a)') ''
    write(u, '(I5,I5)') charge, mult

    open(newunit=v, file='geom', action='read')
    do
       read(v, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(u, '(a)') trim(buf)
    end do
    write(u, '(a)') ' '

    write(u, '(a)') trim(other)
    write(u, '(a)') ' '

    if (nx_qm%gau_typedft == 2) then
       write(u, '(a)') '--link1--'
       write(u, '(a)') trim(link0)

       group = split_pattern(route)
       tmp = ''
       do i=1, size(group)
          if (group(i)(1:2) == 'td' .or. group(i)(1:3) == 'cis') then
             continue
          else
             write(tmp, '(a)') trim(tmp)//' '//trim(group(i))
          end if
       end do
       write(u, '(a)') trim(tmp)//' force Geom=Allcheck'
       write(u, '(a)') ''
       write(u, '(a)') trim(other)
    end if

    close(u)
  end subroutine gau_update_input


  subroutine gau_run()

    character(len=MAX_CMD_SIZE) :: cmd
    integer :: ierr

    ierr = 0

    call get_environment_variable("g16root", cmd)
    cmd = trim(cmd)//'/g16/g16 gaussian.com'

    call call_external(cmd, ierr, outfile='gaussian.log')
    call check_error(ierr, GAU_ERR_MAIN)
  end subroutine gau_run


  subroutine gau_backup(nx_qm, conf, traj, dbg_dir)
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM configuration.
    type(nx_config_t), intent(in) :: conf
    !! Main NX configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Trajectory object.
    character(len=*), intent(in) :: dbg_dir
    !! Name of the directory where the information is saved.

    integer :: ierr
    logical :: mo, bck, ext

    mo = (mod(traj%step, nx_qm%gau_prt_mo) == 0)
    bck = (mod(traj%step, conf%save_cwd) == 0)

    if (mo .or. bck) then
       ierr = mkdir(dbg_dir)
       call check_error(ierr, GAU_ERR_MAIN, &
            & 'Cannot create '//dbg_dir, system=.true.)
    end if

    if (mo) then
       ierr = copy(['gaussian.chk', 'gaussian.log'], dbg_dir)
       call check_error(ierr, GAU_ERR_MAIN, &
            & 'Cannot copy gaussian.chk and gaussian.log to '//dbg_dir, system=.true.)
       ierr = gzip([dbg_dir//'/gaussian.log', dbg_dir//'/gaussian.chk'])
       call check_error(ierr, GAU_ERR_MAIN, &
            & 'Cannot compress files from '//dbg_dir, system=.true.)
    end if

    if (bck) then
       inquire(file='cioverlap/', exist=ext)
       if (ext) then
          ierr = copy(['cioverlap/cioverlap.out ', &
               & 'cioverlap/cis_casida.out' ],&
               & dbg_dir)
          call check_error(ierr, GAU_ERR_MAIN, &
               & 'Cannot copy cio files to '//dbg_dir, system=.true.)
       end if
    end if
  end subroutine gau_backup


  subroutine gau_read_dft(nx_qm, traj, conf, qminfo)
    type(nx_qm_t) :: nx_qm
    !! ``nx_qm`` object.
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf
    type(nx_qminfo_t), intent(inout) :: qminfo

    integer :: u, id, state_id, id1, id2, i
    real(dp) :: en
    integer :: ierr
    character(len=MAX_STR_SIZE) :: buf, namefile
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: step
    integer :: nat
    integer :: nstatdyn

    character(len=64) :: osc_str

    step = traj%step
    nat = conf%nat
    nstatdyn = traj%nstatdyn

    state_id = 1

    ! rgrad(:, :, :) = 0.0_dp

    ! First step: populate the orb object
    if (step == conf%init_step) then
       namefile = 'gaussian'
       call gau_read_orb(nx_qm, namefile)
       call nx_qm%orb%print()
    end if

    qminfo%dim_dataread = 1
    open(newunit=u, file='gaussian.log', action='read')
    do
       read(u, '(A)', iostat=ierr) buf

       if (ierr /= 0) exit

       id = index(buf, 'Convergence criterion not met')
       if (id /= 0) then
          ierr = 1
          call nx_log%log(LOG_ERROR, 'Convergence criterion not met (see gaussian.log)')
          call check_error(ierr, GAU_ERR_CONV)
       end if

       id = index(buf, 'SCF Done:')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(5), *) qminfo%repot(1)
       end if

       id = index(buf, 'Excited State ') ! Capital letter required here !!!
       if (id /= 0) then
          state_id = state_id + 1
          call split_blanks(buf, split)
          read(split(5), *) en
          qminfo%repot(state_id) = qminfo%repot(1) + en / au2ev

          read(split(9), *) osc_str
          read(osc_str(3:), *) nx_qm%osc_str(state_id - 1)

          if (qminfo%dim_dataread == 1) then
             qminfo%dataread(1) = 'Dominant contributions and oscillator strengths'
          end if
          call qminfo%append_data(buf)

          FIND_CONTRIB: do
             read(u, '(A)') buf

             id1 = index(buf, '->')
             id2 = index(buf, '<-')
             if ((id1 /= 0) .or. (id2 /= 0)) then
                call qminfo%append_data(buf)
             else
                exit FIND_CONTRIB
             end if
          end do FIND_CONTRIB

       end if

       id = index(buf, 'Center     Atomic                   Forces')
       if (id /= 0) then
          ! Read two lines (no information in those)
          read(u, *) buf
          read(u, *) buf
          do i=1, nat
             read(u, '(a)') buf
             call split_blanks(buf, split)
             read(split(3), *) qminfo%rgrad(nstatdyn, 1, i)
             read(split(4), *) qminfo%rgrad(nstatdyn, 2, i)
             read(split(5), *) qminfo%rgrad(nstatdyn, 3, i)
          end do
       end if

    end do

    qminfo%rgrad(:, :, :) = -qminfo%rgrad(:, :, :)

    close(u)
  end subroutine gau_read_dft


  subroutine gau_read_orb(nx_qm, namefile)
    type(nx_qm_t), intent(inout) :: nx_qm

    integer :: u, id1, id2, id3, ierr
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), intent(in) :: namefile
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: nocc_a, nocc_b, nvirt_a, nvirt_b
    logical :: check

    open(newunit=u, file=trim(adjustl(namefile))//'.log', &
         & action='read')
    check = .false.
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id1 = index(buf, 'NBasis')
       id2 = index(buf, 'NAE')
       id3 = index(buf, 'NBE')
       if ((id1 /= 0) .and. (id2 /= 0) .and. (id3 /= 0)) then
          call split_blanks(buf, split)
          read(split(2), *) nx_qm%orb%nbas
       end if

       id1 = index(buf, 'NROrb')
       id2 = index(buf, 'NOA')
       id3 = index(buf, 'NOB')
       if ((id1 /= 0) .and. (id2 /= 0) .and. (id3 /= 0)) then
          call split_blanks(buf, split)
          read(split(4), *) nocc_a
          read(split(6), *) nocc_b
          read(split(8), *) nvirt_a
          read(split(10), *) nvirt_b

          nx_qm%orb%nfrozen = nx_qm%orb%nbas - nocc_a - nvirt_a
          nx_qm%orb%nocc = nx_qm%orb%nfrozen + nocc_a
          nx_qm%orb%nocc_b = nocc_b
          nx_qm%orb%nvirt = nvirt_a
          nx_qm%orb%nvirt_b = nvirt_b
          check = .true.
       end if

       id1 = index(buf, 'roots to seek:')
       if (id1 /= 0) then
          call split_blanks(buf, split)
          read(split(7), *) nx_qm%orb%mseek
       end if

    end do
    close(u)

    nx_qm%orb%nelec = nocc_a + nocc_b + 2*nx_qm%orb%nfrozen

    if (nx_qm%qmcode .eq. 'exc_gaussian') then
       open(newunit=u, file=trim(adjustl(namefile))//'.orbinf', &
            status='unknown', form='formatted')
       rewind u

       if (check) then
          write(u,*) nx_qm%orb%nfrozen
          write(u,*) nx_qm%orb%nocc
          write(u,*) nx_qm%orb%nocc_b
          write(u,*) nx_qm%orb%nvirt
          write(u,*) nx_qm%orb%nvirt_b
          write(u,*) nx_qm%orb%nelec
       else
          write(u,*) nx_qm%orb%nelec
       end if
       close(u)

    end if

  end subroutine gau_read_orb


  subroutine gau_parse_input(filename, link0, route, title, charge, mult, other)
    character(len=*), intent(in) :: filename
    character(len=*), intent(out) :: link0
    character(len=*), intent(out) :: route
    character(len=*), intent(out) :: title
    integer, intent(out) :: charge
    integer, intent(out) :: mult
    character(len=*), intent(out) :: other

    integer :: u, ierr, i
    integer :: blank
    character(len=MAX_STR_SIZE) :: buf
    character(len=1) :: nl
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    logical :: read_charge

    link0 = ''
    title = ''
    route = ''
    charge = -1
    mult = -1
    other = ''

    nl = NEW_LINE('c')

    read_charge = .true.
    blank = 0
    open(newunit=u, file=filename, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (buf(1:1) == '%') then
          buf = to_lower(buf)
          link0 = trim(link0)//trim(buf)//nl
       else if (buf(1:1) == '#') then
          buf = to_lower(buf)
          if (buf(2:2) == 'p') then
             group = split_pattern(buf(3:len_trim(buf)))
          else
             group = split_pattern(buf(2:len_trim(buf)))
          end if

          do i=1, size(group)
             if (group(i)(1:2) == 'td') then
                group(i) = 'td'
             else if (group(i)(1:3) == 'cis') then
                group(i) = 'cis'
             end if

             route = trim(route)//' '//trim(group(i))
          end do
       end if

       if (buf == ' ') then
          blank = blank + 1
       end if

       if (blank == 1) then
          read(u, '(a)') title
       end if

       if ((blank == 2) .and. read_charge) then
          read(u, '(a)') buf
          read(buf, *) charge, mult
          read_charge = .false.
       end if

       if ((blank == 2) .and. .not. read_charge) then
          continue
       end if

       if (blank == 3) then
          other = trim(other)//trim(buf)//nl
       end if
    end do
    close(u)

    link0 = link0(1:len_trim(link0)-1)
    other = other(2:len_trim(other))
  end subroutine gau_parse_input


  function gau_generate_route(init_route, qm, traj, conf)  result(res)
    character(len=*) :: init_route
    type(nx_qm_t) :: qm
    type(nx_traj_t) :: traj
    type(nx_config_t) :: conf

    integer :: mocoef
    integer :: td_st
    integer :: ld_thr
    integer :: step
    integer :: nstat
    integer :: nstatdyn
    integer :: init_step
    integer :: typedft

    character(len=:), allocatable :: res

    character(len=MAX_STR_SIZE) :: tmp
    character(len=MAX_STR_SIZE) :: method_input
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    integer :: i
    logical :: nosym_found

    mocoef = qm%gau_mocoef
    td_st = qm%gau_td_st
    ld_thr = qm%gau_ld_thr
    step = traj%step
    nstat = conf%nstat
    nstatdyn = traj%nstatdyn
    init_step = conf%init_step
    typedft = qm%gau_typedft

    tmp = '#p'
    if ((mocoef == 1) .and. (step /= init_step)) then
       tmp = trim(tmp)//' Guess=Read'
    end if
    if (mocoef == 2) then
       tmp = trim(tmp)//' Guess=Read'
    end if

    method_input = ''
    group = split_pattern(init_route)
    nosym_found = .false.
    do i=1, size(group)
       if ((group(i)(1:2) == 'td') .or. (group(i)(1:3) == 'cis')) then
          method_input = trim(group(i))

          if (qm%gau_no_fc > 0) then
             method_input = trim(method_input)//'(full,'
          else
             method_input = trim(method_input)//'('
          end if

          if ((mocoef /= 0) .and. (td_st /= 0)) then
             method_input = trim(method_input)//'read,root='
          else
             method_input = trim(method_input)//'root='
          end if

          method_input = trim(method_input)//to_str(nstatdyn-1)//&
               & ',nstates='//to_str(nstat-1)//')'

       else
          tmp = trim(tmp)//' '//trim(group(i))
       end if

       if (group(i) == 'nosymm') then
          nosym_found = .true.
       end if
    end do

    if (nstat > 1) then
       if (method_input == '') then
          if (qm%gau_no_fc > 0) then
             method_input = 'td(full,root='//to_str(nstatdyn-1)//',nstates='&
                  &//to_str(nstat-1)//')'
          else
             method_input = 'td(root='//to_str(nstatdyn-1)//',nstates='&
                  &//to_str(nstat-1)//')'
          end if
       end if
    end if

    tmp = trim(tmp)//' '//trim(method_input)
    tmp = trim(tmp)//' '//'IOP(3/59='//to_str(ld_thr)//')'

    if (.not. nosym_found) tmp = trim(tmp)//' '//'nosymm'
    if (typedft /= 2) tmp = trim(tmp)//' '//'force'
    res = tmp
  end function gau_generate_route


  subroutine gau_dump_rwf(is_uhf)
    !! Dump the MO coefficients and single amplitudes from rwf file.
    logical, intent(in) ::  is_uhf

    integer :: ierr
    character(len=MAX_CMD_SIZE) :: cmd
    character(len=MAX_CMD_SIZE) :: g16root
    logical :: ext

    ierr = 0
    call get_environment_variable('g16root', g16root)

    cmd = trim(g16root)//'/g16/rwfdump gaussian.rwf eigenvalues 522R'
    call execute_command_line(cmd, exitstat=ierr)
    call check_error(ierr, GAU_ERR_RWFDUMP)

    ! Back up the existing MO_coefs
    inquire(file='MO_coefs_a', exist=ext)
    if (ext) then
       cmd = 'cp MO_coefs_a MO_coefs_a.old'
       call execute_command_line(cmd, exitstat=ierr)
    end if

    cmd = trim(g16root)//'/g16/rwfdump gaussian.rwf MO_coefs_a 524R'
    call execute_command_line(cmd, exitstat=ierr)
    call check_error(ierr, GAU_ERR_RWFDUMP)

    if (is_uhf) then
       inquire(file='MO_coefs_b', exist=ext)
       if (ext) then
          cmd = 'cp MO_coefs_b MO_coefs_b.old'
          call execute_command_line(cmd, exitstat=ierr)
       end if

       cmd = trim(g16root)//'/g16/rwfdump gaussian.rwf MO_coefs_b 526R'
       call execute_command_line(cmd, exitstat=ierr)
       call check_error(ierr, GAU_ERR_RWFDUMP)
    end if


    cmd = trim(g16root)//'/g16/rwfdump gaussian.rwf TDDFT 635R'
    call execute_command_line(cmd, exitstat=ierr)
    call check_error(ierr, GAU_ERR_RWFDUMP)

  end subroutine gau_dump_rwf


  subroutine gau_get_mos_energies(mos_file, mos, is_uhf)
    character(len=*), intent(in) :: mos_file
    real(dp), dimension(:), intent(inout) :: mos
    logical, intent(in) :: is_uhf

    integer :: u, id, mo, ierr, i
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), dimension(:), allocatable :: split
    integer :: nFields, nLines, rem

    mo = 1
    open(newunit=u, file=mos_file, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)

          read(split(6), *) nFields

          ! We consider only the RHF case. In 'eigenvalues' we have both
          ! alpha and beta MOs energies. We only need one of those in RHF.
          if (.not. is_uhf) then
             nFields = nFields / 2
          end if

          nLines = int(nFields / 5)
          rem = mod(nFields, nLines)

          do i=1, nLines
             read(u, *) mos(mo:mo+4)
             mo = mo + 5
          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do i=1, rem
                read(split(i), *) mos(mo)
                mo = mo + 1
             end do
          end if

       end if

    end do
    close(u)
  end subroutine gau_get_mos_energies


  subroutine gau_get_single_amplitudes(orb, sing_file, coptda, tia, tia_b)
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space.
    character(len=*), intent(in) :: sing_file
    !! File to read.
    integer, intent(in) :: coptda
    real(dp), dimension(:, :, :), intent(out) :: tia
    !! Matrix containing the single amplitudes for each excitation.
    !! The last dimension should correspond to the total number of
    !! excitations.
    real(dp), dimension(:, :, :), intent(out), optional :: tia_b

    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: u, ierr, i, j, k, l, m, id, ic
    integer :: mit, leer
    integer :: nLines, rem
    integer :: nocc_a, nocc_b, nvirt_a, nvirt_b, mseek
    real(dp) :: cNorm
    real(dp), dimension(:), allocatable :: coef


    nvirt_a = orb%nvirt
    nvirt_b = orb%nvirt_b
    nocc_b = orb%nocc_b
    nocc_a = orb%nocc - orb%nfrozen
    mseek = orb%mseek

    mit = mseek * (nocc_b*nvirt_b + nocc_a*nvirt_a)
    leer = mseek + 2*mit + 12 ! Number of elements
    nLines = int(leer / 5)
    rem = mod(leer, nLines)
    allocate( coef(leer) )

    ! Read all coefficients into ``coeff`` array. The elements up to ``mit``
    ! correspond to | X + Y >, an thos from ``mit+1`` to ``2*mit`` correspond
    ! to | X - Y >.
    ic = 1
    open(newunit=u, file=sing_file, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          do i=1, nLines
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, 5
                if ((ic > 12) .and. (ic <= leer - mseek )) then
                   read(split(j), *) coef(ic-12)
                end if
                ic = ic + 1
             end do
          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, rem
                read(split(j), *) coef(ic-12)
                ic = ic + 1
             end do
          end if

       end if
    end do
    close(u)

    ! In ``coef`` we have the list of CI coefficient, ordered as nstate lists
    ! of amplitudes, each composed of two lists : amplitudes for alpha and
    ! beta electrons. Each of these lists is in turn composed of the
    ! amplitudes ordered as the lines of the Cia matrix.
    ! Normalization
    cNorm = sqrt(2.0_dp)
    if (present(tia_b)) then
       cNorm = 1
    end if
    open(newunit=u, file='coef_norm', action='write')
    write(u, *) 'cnorm = ', cnorm
    if (coptda == 0) then
       ! Use | X + Y > coefficients
       coef(:) = cNorm * coef(:)
    else if (coptda == 1) then
       ! Use | X > coeffieicnts
       do i=1, mit
          coef(i) = cNorm*( coef(i) + coef(i+mit) ) / 2
       end do
    end if
    close(u)

    ! Now we reorder the single list into the array tia (and tia_b if required).
    l = 1
    m = 0
    do i=1, size(tia, 3)
       ! Populate the alpha matrix for each state
       do j=1, nocc_a
          do k=1, nvirt_a
             tia(k, j, i) = coef(l+m)
             l = l+1
          end do
       end do

       if (present(tia_b)) then
          do j=1, nocc_b
             do k=1, nvirt_b
                tia_b(k, j, i) = coef(l + m)
                m = m+1
             end do
          end do
       else
          m = m + nocc_b*nvirt_b
       end if
    end do

  end subroutine gau_get_single_amplitudes


  subroutine gau_get_lcao(mos_file, nao, lcao)
    !! Extract AO to MO conversion matrix from ``mos_file``.
    !!
    !! LCAO has the ordering (AO, MO).
    character(len=*), intent(in) :: mos_file
    !! File to read.
    integer, intent(in) :: nao
    !! Number of atomic orbitals.
    real(dp), dimension(:, :), intent(out) :: lcao
    !! Resulting AO to MO conversion matrix.

    integer :: u, ierr, id, i, j
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)

    integer :: nele, rem, nbLines, ele

    real(dp), dimension(:), allocatable :: temp

    allocate(temp(nao*nao))
    ele = 1

    ! First read the matrix as a vector
    open(newunit=u, file=mos_file, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nele

          nbLines = int(nele / 5)
          rem = mod(nele, nbLines)

          do i=1, nbLines
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do j=1, 5
                read(split(j), *) temp(ele)
                ele = ele + 1
             end do

          end do

          if (rem /= 0) then
             read(u, '(a)') buf
             call split_blanks(buf, split)
             do i=1, rem
                read(split(i), *) temp(ele)
                ele = ele + 1
             end do
          end if

       end if
    end do
    close(u)

    do i=1, nao
       ele = i
       do j=1, nao
          lcao(i, j) = temp(ele)
          ele = ele + nao
       end do
    end do

  end subroutine gau_get_lcao


  subroutine gau_extract_overlap(source, ovl)
    character(len=*), intent(in) :: source
    !! TURBOMOLE output to read.
    real(dp), dimension(:), intent(out) :: ovl
    !! Resulting symmetric matrix in vector form.

    integer :: u, id, ierr, i, j
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    integer :: rem, nlines, nele, ele

    open(newunit=u, file=source, action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       id = index(buf, 'Dump of file')
       if (id /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nele

          nlines = int(nele / 5)
          rem = mod(nele, nlines)

          if (allocated(split)) deallocate(split)
          allocate(split(5))

          ele = 1
          do i=1, nlines
             read(u, *) split
             do j=1, 5
                read(split(j), *) ovl(ele)
                ele = ele + 1
             end do
          end do

          if (rem /= 0) then
             deallocate(split)
             allocate(split(rem))
             read(u, *) split
             do j=1, rem
                read(split(j), *) ovl(ele)
                ele = ele + 1
             end do
          end if

       end if

    end do
    close(u)
  end subroutine gau_extract_overlap


  subroutine gau_init_double_molecule()

    integer :: u, ierr

    character(len=MAX_STR_SIZE) :: link0
    character(len=MAX_STR_SIZE) :: route
    character(len=MAX_STR_SIZE) :: title
    integer :: charge
    integer :: mult
    character(len=MAX_STR_SIZE) :: other
    character(len=MAX_STR_SIZE) :: basis
    logical :: ext1, ext2

    ierr = mkdir('double_molecule_input')
    call check_error(ierr, GAU_ERR_MISC, &
         & 'GAUSSIAN: Cannot create double_molecule_input directory')

    call gau_parse_input('gaussian.com', link0, route, title, charge, mult, other)

    route = "#p nosymm Geom=NoTest IOp(3/33=1)"

    inquire(file='basis', exist=ext1)
    inquire(file='basis2', exist=ext2)
    if (ext1) then
       open(newunit=u, file='basis', action='read')
       read(u, '(a)') basis
       close(u)

       route = trim(route)//' '//trim(basis)
    else if (ext2) then
       route = trim(route)//' '//'GEN'
       ierr = copy('basis2', 'double_molecule_input/basis2')
       call check_error(ierr, GAU_ERR_MISC, &
            & 'GAUSSIAN: Cannot copy basis2 into double_molecule_input directory')
    end if

    open(newunit=u, file='double_molecule_input/sgaussian.com', action='write')
    write(u, '(a)') "%kjob l302"
    write(u, '(a)') "%rwf=sgaussian"
    write(u, '(a)') "%chk=sgaussian"
    write(u, '(a)') trim(route)
    write(u, '(a)') ""
    write(u, '(a)') "Overlap run"
    write(u, '(a)') ""
    write(u, '(a)') "0 1"
    close(u)
  end subroutine gau_init_double_molecule


  subroutine gau_prepare_double_molecule()

    integer :: u, v, ierr, i
    integer :: nat, at
    character(len=MAX_STR_SIZE) :: buf, gen, gen_ele, orig, added
    character(len=MAX_STR_SIZE), allocatable :: group(:)
    character(len=:), allocatable :: tmp
    logical :: ext
    character(len=1) :: nl

    nl = NEW_LINE('c')

    open(newunit=v, file='overlap/sgaussian.com', action='write')

    ! Header part
    open(newunit=u, file='double_molecule_input/sgaussian.com', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
    end do
    close(u)

    ! Geometry
    nat = 0
    open(newunit=u, file='geom_double', action='read')
    do
       read(u, '(a)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(v, '(a)') trim(buf)
       nat = nat + 1
    end do
    close(u)

    nat = nat / 2

    ! Generated basis
    inquire(file='basis2', exist=ext)
    if (ext) then
       gen = ''
       open(newunit=u, file='basis2', action='read')
       do
          read(u, '(a)', iostat=ierr) buf
          if (ierr /= 0) exit

          tmp = remove_blanks(buf)

          gen_ele = ''

          group = split_pattern(tmp)
          read(group(1), *, iostat=ierr) at
          if (ierr /= 0) then
             gen_ele = trim(tmp)
          else
             orig = tmp(1:len_trim(tmp)-1)
             added = ''
             do i=1, size(group)-1
                at = at + nat
                added = trim(added)//' '//to_str(at)
             end do
             added = trim(added)//' 0'
             gen_ele = trim(orig)//trim(added)
          end if

          gen = trim(gen)//trim(gen_ele)//nl
       end do
       close(u)

       write(v, '(a)') ''
       write(v, '(a)') trim(gen)
    end if

    write(v, '(a)') ''

    close(v)
  end subroutine gau_prepare_double_molecule
end module mod_gaussian
