! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_orca
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-08-27
  !!
  !! Interface with ORCA
  !!
  !! This interface has been tested with ORCA 5.0.3.
  !!
  !! To make it work the ``$ORCA`` environment has to be defined pointing at the location
  !! of the ``orca`` executable.
  !!
  !! The input file should be named ``orca.inp``, and be put in the folder ``JOB_AD``.
  !! The basis set should be specified in a section ``%basis``, and not in the main
  !! instruction line (lines starting with ``!``).  The reason is that it is much easier
  !! to parse in this way !
  !!
  !! Similarly, the ``%tddft`` section should be present.  All options from the given
  !! input will be kept, and NX will only overwrite the ``iroot`` and ``nroots``
  !! parameters by adding another ``%tddft`` section at the end of the input.
  !!
  !! Single excitation amplitudes and transition energies will be extracted from the file
  !! ``orca.cis`` with the help of the C function ``orca_parse_cis``, from source file
  !! ``orca_parse_cis.c``.
  !!
  !! The interface also relies on ``orca_2mkl`` for reading MO energies (from Molden
  !! file), and on ``orca_fragovl`` to obtain overlap matrix and LCAO coefficients.
  use mod_configuration, only: nx_config_t
  use mod_constants, only: MAX_STR_SIZE
  use mod_input_parser, only: parser_t, set => set_config
  use mod_interface, only: rm, mkdir, copy, gzip, to_c_str
  use mod_kinds, only: dp
  use mod_logger, only: &
       & nx_log, &
       & call_external, check_error, print_conf_ele, &
       & LOG_DEBUG, LOG_INFO, LOG_ERROR
  use mod_orbspace, only: nx_orbspace_t
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_tools, only: split_blanks, str_starts_with, to_str, split_pattern
  use mod_trajectory, only: nx_traj_t
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: orca_setup, orca_update_input, orca_read, orca_run
  public :: orca_print, orca_backup

  public :: orca_prepare_cio_files
  public :: orca_get_mos_energies
  public :: orca_get_singles_amplitudes
  public :: orca_get_lcao
  public :: orca_extract_overlap

  integer, parameter :: ORCA_ERR_MAIN = 101

contains

  subroutine orca_setup(nx_qm, parser, conf, control)
    !! Setup ORCA computation.
    type(nx_qm_t) :: nx_qm
    type(parser_t) :: parser
    type(nx_config_t) :: conf
    character(len=*) :: control

    character(len=MAX_STR_SIZE) :: env
    logical :: ext

    call set(parser, 'orca', nx_qm%orca_is_tda, 'is_tda')
    call set(parser, 'orca', nx_qm%orca_prt_mo, 'prt_mo')
    call set(parser, 'orca', nx_qm%orca_mocoef, 'mocoef')

    call get_environment_variable('ORCA', env)
    if (env == '') then
       call nx_log%log(LOG_ERROR, 'ORCA: $ORCA environment not defined !')
    end if
    nx_qm%ovl_cmd = trim(env)//'/orca sorca.inp'
    nx_qm%ovl_out = 'sorca.out'
    nx_qm%ovl_post = &
         & trim(env)//'/orca_fragovl overlap/sorca.gbw overlap/sorca.gbw > orca.ovl_double&
         &.dat'

    if (nx_qm%orca_mocoef == 2) then
       inquire(file=trim(nx_qm%job_folder)//'/orca.gbw', exist=ext)
       if (.not. ext) then
          call nx_log%log(LOG_ERROR, &
               & 'ORCA: MOCOEF = 2 but orca.gbw not found in JOB_AD !')
          error stop
       end if
    end if
  end subroutine orca_setup


  subroutine orca_print(nx_qm, out)
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM object to print.
    integer, intent(in), optional :: out

    integer :: output
    character(len=MAX_STR_SIZE) :: env

    output = stdout
    if (present(out)) output = out

    call get_environment_variable("ORCA", env)
    call print_conf_ele(trim(env), 'ORCA path', unit=output)
    call print_conf_ele(nx_qm%orca_is_tda, 'TDA enabled ?', unit=output)
    call print_conf_ele(nx_qm%orca_prt_mo, 'Frequency of MO print', unit=output)
    call print_conf_ele(nx_qm%orca_prt_mo, 'MOCOEF', unit=output)
  end subroutine orca_print


  subroutine orca_update_input(nx_qm, traj, conf)
    type(nx_qm_t), intent(inout) :: nx_qm
    type(nx_traj_t), intent(in):: traj
    type(nx_config_t), intent(in) :: conf

    integer :: finp, fout, ierr
    character(len=MAX_STR_SIZE) :: buf
    character(len=256) :: rmfiles(5)

    !! Backup GBW file
    if (conf%dc_method == 2) then
       if (traj%step > conf%init_step) then
          ierr = copy('orca.gbw', 'orca.gbw.old')
       end if
    end if

    !! Clean-up folder
    if (nx_qm%orca_mocoef == 2) then
       call nx_log%log(LOG_DEBUG, 'ORCA: MOCOEF = 2 copying orca.gbw from JOB_AD')
       ierr = rm(['orca.gbw'])
       ierr = copy(trim(nx_qm%job_folder)//'/orca.gbw', './')
    else if (nx_qm%orca_mocoef == 0) then
       call nx_log%log(LOG_DEBUG, 'ORCA: MOCOEF = 0 deleting orca.gbw')
       ierr = rm(['orca.gbw'])
    end if

    rmfiles = [ character(len=256) :: &
         & 'orca.out', 'orca.cis', 'orca.densisites', 'orca.engrad', &
         & 'orca_property.txt' &
         & ]
    ierr = rm(rmfiles)
    open(newunit=finp, file=trim(nx_qm%job_folder)//'/orca.inp', action='read')
    open(newunit=fout, file='orca.inp', action='write')

    do
       read(finp, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       write(fout, '(A)') trim(buf)
    end do
    close(finp)

    write(fout, '(A)') '%tddft'
    write(fout, '(A)') ' iroot '//to_str(traj%nstatdyn - 1)
    write(fout, '(A)') ' nroots '//to_str(conf%nstat - 1)
    write(fout, '(A)') 'end'
    write(fout, '(A)') ''
    close(fout)
  end subroutine orca_update_input


  subroutine orca_run(nx_qm)
    type(nx_qm_t), intent(in) :: nx_qm

    character(len=MAX_STR_SIZE) :: env
    integer :: ierr

    call get_environment_variable('ORCA', env)
    call call_external(trim(env)//'/orca orca.inp', ierr, outfile='orca.out')
    call check_error(ierr, 101, 'ERROR in ORCA !!')
  end subroutine orca_run


  subroutine orca_read(nx_qm, traj, conf, qminfo, stat)
    !! Parse the ORCA output.
    !!
    !! This routine recovers energies and gradients from an ORCA TD-DFT (or TD-DFT/TDA)
    !! output.  For energies, ORCA prints the total energy of the state of interest
    !! \(E_{nstatdyn}\) (specified by the ``iroot`` parameter in the ``%tddft`` block,
    !! corresponding to ``nstatdyn -1`` in Newton-X) in the line:
    !!
    !!     FINAL SINGLE POINT ENERGY: ...
    !!
    !! It also prints the excitation energies, relatively to the ground state energy.
    !! The routine thus records all excitation energies, then derives the ground state
    !! energy from \(\Delta E_{nstatdyn}\) and \(E_{nstatdyn}\), and finally the energies
    !! of all states by adding the corresponding \(\Delta E\) to the ground state energy.
    type(nx_qm_t), intent(inout) :: nx_qm
    !! QM type object.
    type(nx_traj_t), intent(in) :: traj
    type(nx_config_t), intent(in) :: conf
    type(nx_qminfo_t), intent(inout) :: qminfo
    !! Information recovered from the output file.
    integer, intent(inout) :: stat
    !! Status indicator.

    character(len=*), parameter :: outfile = 'orca.out'

    integer :: u, ierr, istate, i, idum, j, nstat
    character(len=MAX_STR_SIZE) :: buf
    character(len=MAX_STR_SIZE), allocatable :: split(:)
    character(len=256) :: dum
    real(dp) :: refen
    integer :: nat
    integer :: nstatdyn

    character(len=256) :: in_enstr, out_enstr, fname

    nat = conf%nat
    nstatdyn = traj%nstatdyn

    call orca_parse_cis('orca.cis', 0)
    open(newunit=u, file='energies.dat', action='read')
    do i=2, size(qminfo%repot)
       read(u, *) dum, dum, qminfo%repot(i)
    end do
    close(u)

    in_enstr = 'TD-DFT EXCITED STATES (SINGLETS)'
    if (nx_qm%orca_is_tda) in_enstr = 'TD-DFT/TDA EXCITED STATES (SINGLETS)'
    out_enstr = 'TD-DFT-EXCITATION SPECTRA'
    if (nx_qm%orca_is_tda) out_enstr = 'TD-DFT/TDA-EXCITATION SPECTRA'

    open(newunit=u, file=outfile, action='read')
    READ_ORCA_OUT: do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       ENERGIES: if (index(buf, in_enstr) /= 0) then
          call qminfo%append_data('Excitation informations read from orca.out:')
          istate = 1
          do
             read(u, '(A)') buf
             if (index(buf, 'STATE') /= 0) then
                ! call split_blanks(buf, split)
                call qminfo%append_data(buf)

                ! Fill qminfo%repot(2:) with excitation energies
                ! read(split(4), *) qminfo%repot(istate+1)
                CONTRIB: do
                   read(u, '(A)') buf
                   if (index(buf, '->') /= 0) then
                      call qminfo%append_data(buf)
                   else
                      exit CONTRIB
                   end if
                end do CONTRIB
                istate = istate + 1
             else if (index(buf, out_enstr) /= 0) then
                exit ENERGIES
             end if
          end do
       end if ENERGIES

       ! Save the energy of the state `nstatdyn` as `refen`
       if (index(buf, 'FINAL SINGLE POINT ENERGY') /=0) then
          call qminfo%append_data(buf)
          read(buf, *) dum, dum, dum, dum, refen
       end if

       GRADIENTS: if (index(buf, 'EXCITED STATE GRADIENT DONE') /= 0) then
          do i=1, 6
             read(u, *)
          end do
          do i=1, nat
             read(u, *) idum, dum, dum, (qminfo%rgrad(nstatdyn, j, i), j=1, 3)
          end do
          exit READ_ORCA_OUT
       end if GRADIENTS
    end do READ_ORCA_OUT

    if (traj%step == conf%init_step) then
       call orca_read_orb(nx_qm, u)
       call nx_qm%orb%print()
    end if

    close(u)

    ! Finally, derive the GS energy, and the energies of all states.
    nstat = size(qminfo%repot)
    qminfo%repot(1) = refen - qminfo%repot(nstatdyn)
    qminfo%repot(nstatdyn) = refen
    do i=2, nstat
       if (i /= nstatdyn) then
          qminfo%repot(i) = qminfo%repot(1) + qminfo%repot(i)
       end if
    end do

    stat = 0
  end subroutine orca_read


  subroutine orca_read_orb(nx_qm, unit)
    type(nx_qm_t), intent(inout) :: nx_qm
    integer, intent(in) :: unit

    integer :: ierr, tmp, orb_start, orb_end
    character(len=MAX_STR_SIZE) :: buf
    character(len=256), allocatable :: split(:), split2(:)

    rewind(unit)
    do
       read(unit, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, 'Orbital ranges used for CIS calculation') /= 0) then
          read(unit, '(A)') buf
          split = split_pattern(buf, pattern='Orbitals')
          split = split_pattern(split(2), pattern='to')
          split2 = split_pattern(split(1), pattern='...')
          read(split2(1), *) orb_start
          read(split2(2), *) orb_end
          nx_qm%orb%nfrozen = orb_start
          nx_qm%orb%nocc = orb_end + 1
          nx_qm%orb%nocc_b = nx_qm%orb%nocc

          split2 = split_pattern(split(2), pattern='...')
          read(split2(1), *) orb_start
          read(split2(2), *) orb_end
          nx_qm%orb%nvirt = orb_end - orb_start + 1
       end if

       if (index(buf, 'Number of basis functions') /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nx_qm%orb%nbas
       end if

       if (index(buf, ' Number of Electrons') /= 0) then
          call split_blanks(buf, split)
          read(split(6), *) nx_qm%orb%nelec
       end if
    end do
  end subroutine orca_read_orb


  subroutine orca_prepare_cio_files(prepare_double)
    logical :: prepare_double

    integer :: ierr
    logical :: ext

    call nx_log%log(LOG_INFO, 'Preparation of cioverlap files')
    call execute_command_line('$ORCA/orca_2mkl orca -molden', exitstat=ierr)
    call check_error(ierr, 101, msg='Error in running orca_2mkl')

    inquire(file='orca.ovl.dat', exist=ext)
    if (ext) then
       ierr = copy('orca.ovl.dat', 'orca.ovl.dat.old')
    end if
    call execute_command_line('$ORCA/orca_fragovl orca.gbw orca.gbw > orca.ovl.dat', exitstat=ierr)
    call check_error(ierr, 101, msg='Error in running orca_fragovl')

    if (prepare_double) then
       call execute_command_line('$ORCA/orca_fragovl orca.gbw orca.gbw.old > orca.ovl_double.dat', exitstat=ierr)
       call check_error(ierr, 101, msg='Error in running orca_fragovl')
    end if

  end subroutine orca_prepare_cio_files



  subroutine orca_get_mos_energies(infile, mos)
    character(len=*), intent(in) :: infile
    real(dp), intent(inout) :: mos(:)

    integer :: u, ierr, norb
    CHARACTER(LEN=MAX_STR_SIZE) :: buf
    character(len=12) :: dum

    norb = 0
    open(newunit=u, file=infile, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, '[MO]') /= 0) then
          do
             read(u, '(A)', iostat=ierr) buf
             if (ierr /= 0) exit
             if (index(buf, 'Ene=') /= 0) then
                norb = norb + 1
                read(buf, *) dum, mos(norb)
             end if
          end do
       end if
    end do
    close(u)
  end subroutine orca_get_mos_energies


  subroutine orca_parse_fragovl(filename, label, nao, res)
    character(len=*), intent(in) :: filename
    !! File to read.
    character(len=*), intent(in) :: label
    !! Label to look for in the file.
    integer, intent(in) :: nao
    !! Number of atomic orbitals.
    real(dp), dimension(:, :), intent(out) :: res
    !! Resulting matrix

    integer :: u, ierr, iblock, jline, dum, j
    integer :: nblocks, remainder
    integer :: start_index, end_index
    character(len=MAX_STR_SIZE) :: buf

    nblocks = int(nao / 6)
    remainder = mod(nao, 6)

    open(newunit=u, file=filename, action='read')
    do
       read(u, '(A)', iostat=ierr) buf
       if (ierr /= 0) exit

       if (index(buf, label) /= 0) then
          read(u, '(A)') buf
          read(u, '(A)') buf
          do iblock=1, nblocks
             start_index = (iblock-1) * 6 + 1
             end_index = iblock * 6
             do jline=1, nao
                read(u, '(A)') buf
                read(buf, *) dum, (res(jline, j), j=start_index, end_index)
             end do
             read(u, '(A)') buf
          end do

          if (remainder /= 0) then
             start_index = nblocks*6+1
             end_index = start_index + remainder - 1
             do jline=1, nao
                read(u, '(A)') buf
                read(buf, *) dum, (res(jline, j), j=start_index, end_index)
             end do
          end if
       end if
    end do
    close(u)
  end subroutine orca_parse_fragovl


  subroutine orca_get_lcao(filename, nao, lcao)
    !! Extract the AO to MO conversion matrix from ``filename``.
    !!
    !! LCAO has the ordering (AO, MO).
    character(len=*), intent(in) :: filename
    !! File to read.
    integer, intent(in) :: nao
    !! Number of atomic orbitals.
    real(dp), dimension(:, :), intent(out) :: lcao
    !! Resulting AO to MO conversion matrix.

    call orca_parse_fragovl(filename, 'FRAGMENT A MOs MATRIX', nao, lcao)

  end subroutine orca_get_lcao


  subroutine orca_get_singles_amplitudes(orb, sing_file, is_tda, tia)
    !! Read singles amplitudes from ORCA.
    !!
    type(nx_orbspace_t), intent(in) :: orb
    !! Orbital space.
    character(len=*), intent(in) :: sing_file
    !! File to read.
    logical, intent(in) :: is_tda
    !! Indicate if the computation is TDA or TD-DFT.
    real(dp), dimension(:, :, :), intent(out) :: tia
    !! Matrix containing the single amplitudes for each excitation.
    !! The last dimension should correspond to the total number of
    !! excitations.

    integer :: nvec, header(8), nstates
    integer :: i, u, istate, iocc, ivirt
    logical :: cis_already_dumped
    character(len=256) :: fname

    inquire(file='orca.cis_r1.dat', exist=cis_already_dumped)
    if (.not. cis_already_dumped) then
       call orca_parse_cis('orca.cis', 0)
    end if

    do istate=1, size(tia, 3)
       if (is_tda) then
          i = istate
       else
          i = istate*2 - 1
       end if

       fname = 'orca.cis_r'//to_str(i)//'.dat'
       open(newunit=u, file=fname, action='read')
       do iocc=1, size(tia, 2)
          do ivirt=1, size(tia, 1)
             read(u, *) tia(ivirt, iocc, istate)
          end do
       end do
    end do

  end subroutine orca_get_singles_amplitudes


  ! subroutine orca_init_double_molecule()
  !   integer :: ierr, u, v, charge, mult
  !   character(len=256) :: buf, dum
  ! 
  !   ierr = mkdir('double_molecule_input')
  !   call check_error(ierr, ORCA_ERR_MAIN, &
  !        & 'ORCA: Cannot create double_molecule_input directory')
  ! 
  !   open(newunit=v, file='double_molecule_input/sorca.inp', action='write')
  !   open(newunit=u, file='orca.inp', action='read')
  !   do
  !      read(u, '(A)', iostat=ierr)  buf
  !      if (ierr /= 0) exit
  ! 
  !      if (index(buf, '%basis') /= 0) then
  !         write(v, '(A)') trim(buf)
  !         WRITE_BASIS: do
  !            read(u, '(A)') buf
  !            write(v, '(A)') trim(buf)
  !            if (buf == 'end') exit WRITE_BASIS
  ! 
  !         end do WRITE_BASIS
  !      end if
  ! 
  !      if (index(buf, '*xyzfile') /= 0) then
  !         read(buf, *) dum, charge, mult, dum
  !      end if
  !   end do
  !   close(u)
  ! 
  !   write(v, '(A)') '*xyzfile '//to_str(charge)//' '//to_str(mult)//' sorca.xyz'
  !   close(v)
  ! end subroutine orca_init_double_molecule
  ! 
  ! 
  ! subroutine orca_prepare_double_molecule()
  !   integer :: ierr
  !   ierr = copy('sorca.xyz', 'overlap/sorca.xyz')
  !   ierr = copy('double_molecule_input/sorca.inp', 'overlap/sorca.inp')
  !   ierr = rm(['sorca.xyz'])
  ! end subroutine orca_prepare_double_molecule



  subroutine orca_extract_overlap(filename, nao, aoovl)
    character(len=*), intent(in) :: filename
    !! TURBOMOLE output to read.
    integer, intent(in) :: nao
    !! Number of atomic orbitals.
    real(dp), dimension(:), intent(out) :: aoovl
    !! Resulting symmetric matrix in vector form.

    integer :: u, ierr, i, j, id
    character(len=MAX_STR_SIZE) :: buf
    integer :: dum, start_index, end_index, nele
    real(dp) :: tmp( nao, nao), tmp2(2*nao, 2*nao)

    tmp2 = 0.0_dp
    call orca_parse_fragovl('orca.ovl.dat', 'FRAGMENT-FRAGMENT OVERLAP MATRIX', nao, tmp)
    do i=1, nao
       do j=1, nao
          tmp2(i, j) = tmp(i, j)
       end do
    end do

    call orca_parse_fragovl(filename, 'FRAGMENT-FRAGMENT OVERLAP MATRIX', nao, tmp)
    do i=1, nao
       do j=1, nao
          tmp2(i+nao, j) = tmp(i, j)
          tmp2(i, j+nao) = tmp(i, j)
       end do
    end do

    call orca_parse_fragovl('orca.ovl.dat.old', 'FRAGMENT-FRAGMENT OVERLAP MATRIX', nao,&
         & tmp)
    do i=1, nao
       do j=1, nao
          tmp2(i+nao, j+nao) = tmp(i, j)
       end do
    end do

    id = 1
    do i=1, 2*nao
       do j=1, i
          aoovl(id) = tmp2(i, j)
          id = id + 1
       end do
    end do

    ! do i=1, nao
    !    do j=1, nao
    !       tmp2(nao+i, nao+j) = tmp(i, j)
    !    end do
    ! end do
    ! tmp2( nao+1:2*nao, nao+1:2*nao ) = tmp(:, :)

    ! Get the matrix in easier to use form

    ! tmp2(nao+1:2*nao, 1:nao) = tmp(:, :)
    ! call nx_log%log(LOG_DEBUG, tmp2, title='SOVL MATRIX')

    ! do i=1, 2*nao
    !    do j=1, i
    !       aoovl(j+i-1) = tmp2(i, j)
    !    end do
    ! end do

    ! call nx_log%log(LOG_DEBUG, tmp, title='FRAGOVL_MATRIX')

    ! print *, 'NAO = ', nao
    ! print *, 'DIM AOOVL = ', size(aoovl)
    ! print *, 'STARTING AT ', start_index
    ! print *, ''
    ! start_index = (nao * (nao + 1)) / 2
    ! do i=1, nao
    !    ! print *, 'I = ', i, ': START IS ', start_index
    !    do j=1, nao
    !       ! print '(A,I0,A,I0,A,F24.18)', 'I = ', i, '; J = ', j, '; OVL = ', tmp(j, i)
    !       aoovl(j+start_index) = tmp(j, i)
    !    end do
    !    start_index = start_index + nao + i
    ! end do
    ! call nx_log%log(LOG_DEBUG, aoovl, title='FRAGOVL_VECTOR')

  end subroutine orca_extract_overlap


  subroutine orca_backup(nx_qm, conf, traj, dbg_dir)
    type(nx_qm_t), intent(in) :: nx_qm
    !! QM configuration.
    type(nx_config_t), intent(in) :: conf
    !! Main NX configuration.
    type(nx_traj_t), intent(in) :: traj
    !! Trajectory object.
    character(len=*), intent(in) :: dbg_dir
    !! Name of the directory where the information is saved.

    integer :: ierr
    logical :: mo, bck, ext

    mo = (conf%lvprt > 4 .or. (mod(traj%step, nx_qm%orca_prt_mo) == 0))
    bck = (mod(traj%step, conf%save_cwd) == 0)

    if (mo .or. bck) then
       ierr = mkdir(dbg_dir)
       call check_error(ierr, ORCA_ERR_MAIN, &
            & 'Cannot create '//dbg_dir, system=.true.)
    end if

    if (mo) then
       ierr = copy(['orca.gbw', 'orca.out'], dbg_dir)
       call check_error(ierr, ORCA_ERR_MAIN, &
            & 'Cannot copy orca.gbw and orca.out to '//dbg_dir, system=.true.)
       ierr = gzip([dbg_dir//'/orca.gbw', dbg_dir//'/orca.out'])
       call check_error(ierr, ORCA_ERR_MAIN, &
            & 'Cannot compress files from '//dbg_dir, system=.true.)
    end if

    if (bck) then
       inquire(file='cioverlap/', exist=ext)
       if (ext) then
          ierr = copy(['cioverlap/cioverlap.out ', &
               & 'cioverlap/cis_casida.out' ],&
               & dbg_dir)
          call check_error(ierr, ORCA_ERR_MAIN, &
               & 'Cannot copy cio files to '//dbg_dir, system=.true.)
       end if
    end if
  end subroutine orca_backup


  subroutine orca_parse_cis(cisfile, debug)
    use iso_c_binding, only: c_int
    character(len=*), intent(in) :: cisfile
    !! Name of the file to parse.
    integer, intent(in) :: debug

    character(len=len(cisfile)+1) :: tmp1
    integer(c_int) :: tda_c, res, debug_c

    interface
       function c_orca_parse_cis(sfile, db) bind(C, name='orca_parse_cis')
         use iso_c_binding, only: c_int, c_char
         implicit none
         integer(c_int) :: c_orca_parse_cis
         character(kind=c_char), intent(in) :: sfile(*)
         integer(c_int), value, intent(in) :: db
       end function c_orca_parse_cis
    end interface

    tmp1 = to_c_str(cisfile)

    debug_c = int(debug, c_int)

    res = c_orca_parse_cis(tmp1, debug_c)
  end subroutine orca_parse_cis


end module mod_orca
