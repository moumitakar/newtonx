! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_numerics
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-03-27
  !!
  !! This module defines numeric routines, mostly adapted from
  !! Numerical Recipes, like algorithms for polynomial
  !! interpolations, etc.
  use mod_kinds, only: dp
  IMPLICIT DOUBLE PRECISION(A-H,O-Z)
!         implicit none

  private

  public :: polint, polinterp, uvip3p
  public :: quadratic_regression

contains

  function polinterp(x, xa, ya, deg) result(y)
    !! Wrapper around the numerical recipe routine ``polint``.
    real(dp) :: x
    !! The point where thee polynomial will be evaluated.
    real(dp) :: xa(:)
    !! Points where the function is known.
    real(dp) :: ya(:)
    !! Value of the polynomial at values ``xa``.
    real(dp) :: y
    integer :: deg

    real(dp) :: dy

    call polint(xa, ya, deg, x, y, dy)
  end function polinterp


  SUBROUTINE polint(xa,ya,n,x,y,dy)
    !! Polynomial interpolation at the degree ``n-1``.
    !!
    !! Given arrays ``xa`` and ``ya``, each of length ``n``, and given
    !! a value ``x``, this routine returns a value ``y``, and an error
    !! estimate ``dy``. If \(P(x)\) is the polynomial of degree \(N-1\) such that
    !! ``P(xai) = yai, i = 1,..., n``, then the returned value is \(y
    !! = P(x)\).
    !!
    !! (C) Copr. 1986-92 Numerical Recipes Software
    !! BD (2021-06-03): replace legacy "DO" with modern ones
    INTEGER n,NMAX
    REAL(dp) :: dy,x,y,xa(:),ya(:)
    PARAMETER (NMAX=10)
    INTEGER i,m,ns
    REAL(dp) :: den,dif,dift,ho,hp,w,c(NMAX),d(NMAX)
    real(dp) :: eps
    ns=1
    dif=dabs(x-xa(1))

    do i=1,n
       dift=dabs(x-xa(i))
       if (dift.lt.dif) then
          ns=i
          dif=dift
       endif
       c(i)=ya(i)
       d(i)=ya(i)
    end do

    y=ya(ns)
    ns=ns-1
    do m=1,n-1
       do i=1,n-m
          ho=xa(i)-x
          hp=xa(i+m)-x
          w=c(i+1)-d(i)
          den=ho-hp
          eps=1E-6_dp
          if (dabs(den) .lt. eps) then
             write(11,*) 'ERROR: failure in polint'
             stop 12
          endif
          den=w/den
          d(i)=hp*den
          c(i)=ho*den
       end do
       if (2*ns.lt.n-m)then
          dy=c(ns+1)
       else
          dy=d(ns)
          ns=ns-1
       endif
       y=y+dy
    end do

    return
  END SUBROUTINE polint


  subroutine quadratic_regression(x, deg, y, poly, info)
    !! Perform a quadratic regression of arbitrary degree.
    !!
    !! The regression is performed with the form:
    !!
    !! \[ y_i = \beta_0 + \beta_1 x_i + \beta_2 x_i^2 + \dots + \beta_m x_i^m +
    !! \epsilon_i \quad (i=1, 2, \dots, n), \]
    !!
    !! where \( \epsilon \) is a random error. \(y\) corresponds to array ``y``,
    !! \(x\) corresponds to array ``x``, and ``deg`` is equal to \(m\) (degree of
    !! interpolation). \(n\) is the size of the arrays ``x`` and ``y``.
    !!
    !! This routine implements an algorithm based on the Vandermonde matrix, by
    !! reformulating the problem in matrix form:
    !!
    !! \[ \begin{bmatrix}
    !!       y(x_1) \\
    !!       y(x_2) \\
    !!       y(x_3) \\
    !!       \vots \\
    !!       y(x_n)
    !!    \end{bmatrix} =
    !!    \begin{bmatrix}
    !!    1 & x_1 & x_1^2 & \dots & x_1^m \\
    !!    1 & x_2 & x_2^2 & \dots & x_2^m \\
    !!    1 & x_3 & x_3^2 & \dots & x_3^m \\
    !!    \vdots & \vdots & \vdots & \ddots & \vdots \\
    !!    1 & x_1 & x_n^2 & \dots & x_n^m \\
    !!    \end{bmatrix}
    !!    \begin{bmatrix}
    !!       \beta_0 \\
    !!       \beta_1 \\
    !!       \beta_2 \\
    !!       \vdots \\
    !!       \beta_n
    !!    \end{bmatrix} +
    !!    \begin{bmatrix}
    !!       \epsilon_0 \\
    !!       \epsilon_1 \\
    !!       \epsilon_2 \\
    !!       \vdots \\
    !!       \epsilon_n
    !!    \end{bmatrix},
    !! \]
    !!
    !! or
    !!
    !! \[ y =  \mathrm{\mathbf{X}} \beta + \epsilon \]
    !!
    !! The approximate solution is then given by the formula:
    !!
    !! \[ \beta \approx (\mathrm{\mathbf{X}^{T}} \mathrm{\mathbf{X}})^{-1}
    !! \mathrm{\mathbf{X}^{T}} \Delta E \]
    real(dp), intent(in) :: x(:)
    !! Point samples.
    real(dp), intent(in) :: y(:)
    !! Quantity to fit.
    integer, intent(in) :: deg
    !! Degree of the polynomial obtained,
    real(dp), intent(inout) :: poly(:)
    !! Resulting coeffiecients of the least-square fit.
    integer, intent(out) :: info
    !! Error handler for BLAS and LAPACK routines.

    real(dp), allocatable :: X1(:, :), XT(:, :), XTX(:, :)
    real(dp), allocatable :: work(:)
    integer, allocatable :: ipiv(:)
    
    integer :: i, j, n
    integer :: lwork, lda

    n = size(x)
    lda = deg + 1
    lwork = deg + 1
    allocate(ipiv(deg+1))
    allocate(work(lwork))
    
    ! Prepare Vandermonde matrix
    allocate( X1(n, deg+1) )
    do i=0, deg
       do j=1, n
          X1(j, i+1) = x(j)**i
       end do
    end do

    allocate(XT(deg+1, n))
    XT = transpose(X1)

    allocate(XTX(deg+1, deg+1))
    XTX = matmul(XT, X1)

    ! Invert the resulting matrix
    call DGETRF(deg+1, deg+1, XTX, lda, ipiv, info)
    if (info /= 0) then
       write(*, *) 'XTX Matrix is numerically singular!'
       return
    end if
       
    call DGETRI(deg+1, XTX, lda, ipiv, work, lwork, info)
    if (info /= 0) then
       write(*, *) 'XTX matrix inversion failed!'
       return
    end if

    ! Finally, multiply with the y vector
    poly = matmul( matmul(XTX, XT), y)
  end subroutine quadratic_regression
 

!   subroutine lowdin(n,s,sd,zflag)
!     !
!     !     LOWDIN'S ORTHOGONALIZATION
!     !     N = DIMENSION OF THE VECTOR SPACE
!     !     S = OVERLAP MATRIX
!     !     SD = INVERSE SQUARE ROOT EIGENVALUES
!     !     W,WW = SCRATCH ARRAYS
!     !     AFTER PROCESSING, THE COLUMNS OF S ARE A BASIS OF ORTHO-
!     !     NORMALIZED VECTORS IN THE GIVEN METRIC, REORDERED ACCORDING
!     !     TO THEIR OVERLAP WITH THE ORIGINAL BASIS
!     !
!     !
!     !.. Implicit Declarations ..
!     implicit none
!     !
!     !.. Formal Arguments ..
!     logical, intent(out) :: zflag
!     integer, intent(in) :: n
!     real(dp), dimension(n), intent(inout) :: sd
!     real(dp), dimension(n,n), intent(inout) :: s
!     !
!     !.. Local ..
!     real(dp), dimension(n,n)                :: ev
!     real(dp), allocatable, dimension(:)                :: work
!     integer :: i,ip,iter,j,k,niter,nm,info,lwork
!     real(dp) :: a,b,sij,ski
!     !
!     !
!     !.. Intrinsic Functions ..
!     intrinsic Sqrt
!     !
!     ! ... Executable Statements ...
!     !
!     write(*,*) 'Starting Lowdin orthogonalization ...'
!     !write(*,*) 'Before dsyev'
!     ev = s
!     !call wrtmat(ev,n,n,6)
!     niter = 1000
!     lwork = max(5,n**2)
!     allocate(work(lwork))
!     call dsyev('V','L',n, ev, n, sd, work, lwork, info)
!     deallocate(work)
!     !write(*,*) 'After dsyev',info
!     !call wrtmat(ev,n,n,6)
!     !write(*,*) 'EW:',(sd(i),i=1,n)
!     ! upper or lower?
!     !call real_symmetric_diagonalizer(s,'P',n,1,n,sd,ww,info)
!     do k = 1,n
!        sd(k) = 1.d0 / Sqrt(sd(k))
!     end do
!     do i = 1,n
!        do j = 1,i
!           sij = 0.d0
!           do k = 1,n
!              sij = sij + ev(i,k)*sd(k)*ev(j,k)
!           end do
!           s(i,j) = sij
!           s(j,i) = sij
!        end do
!     end do
!     iter = 0
!     nm = n - 1
!     do
!        iter = iter + 1
!        do i = 1,nm
!           ip = i + 1
!           do j = ip,n
!              a = s(i,i)*s(i,i) + s(j,j)*s(j,j)
!              b = s(i,j)*s(i,j) + s(j,i)*s(j,i)
!              if (a < b) goto 1000
!           end do
!        end do
!        exit
! 1000   do k = 1,n
!           ski = s(k,i)
!           s(k,i) = s(k,j)
!           s(k,j) = ski
!        end do
!        if (iter >= niter) then
!           write (6,10000)
!           zflag = .true.
!           return
!        end if
!     end do
!     zflag = .false.
!     return
!     !
!     ! ... Format Declarations ...
!     !
! 10000 format ( &
!          //5x,"REORDERING OF LOWDIN ORTHOGONALIZED VECTORS ","NOT COMPLETE"//)
  !   end subroutine lowdin

  SUBROUTINE  UVIP3P(NP,ND,XD,YD,NI,XI,YI)
!                        3,neng,eng,sc,nfit,eng1,sc1
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
!! Univariate Interpolation (Improved Akima Method)
!! Hiroshi Akima
! U.S. Department of Commerce, NTIA/ITS
! Version of 89/07/04
!! This subroutine performs univariate interpolation.  It is based
! on the improved A method developed by Hiroshi Akima, 'A method
! of univariate interpolation that has the accuracy of a third-
! degree polynomial,' ACM TOMS, vol. xx, pp. xxx-xxx, 19xx.  (The
! equation numbers referred to in the comments below are those in
! the paper.)
!! In this method, the interpolating function is a piecewise
! function composed of a set of polynomials applicable to
! successive intervals of the given data points.  This method
! uses third-degree polynomials as the default, but the user has
! an option to use higher-degree polynomial to reduce undulations
! in resulting curves.
!! This method has the accuracy of a third-degree polynomial if
! the degree of the polynomials for the interpolating function is
! set to three.
!! The input arguments are
!   NP = degree of the polynomials for the interpolating
!        function,
!   ND = number of input data points
!        (must be equal to 2 or greater),
!   XD = array of dimension ND, containing the abscissas of
!        the input data points
!        (must be in a monotonic increasing order),
!   YD = array of dimension ND, containing the ordinates of
!        the input data points,
!   NI = number of points for which interpolation is desired
!        (must be equal to 1 or greater),
!   XI = array of dimension NI, containing the abscissas of
!        the desired points.
!! The output argument is
!   YI = array of dimension NI, where the ordinates of the
!        desired points are to be stored.
!! If an integer value smaller than 3 is given to the NP argument,
! this subroutine assumes NP = 3.
!! The XI array elements need not be monotonic, but this
! subroutine interpolates faster if the XI array elements are
! given in a monotonic order.
!! If the XI array element is less than XD(1) or greater than
! XD(ND), this subroutine linearly interpolates the YI value.
!!! Specification statement
      DIMENSION   XD(nd),YD(nd),XI(ni), YI(ni)
! Error check
      IF (ND.LE.1)   GO TO 90  ! former statement 10
      IF (NI.LE.0)   GO TO 91
      DO 11  ID=2,ND
        IF (XD(ID).LE.XD(ID-1))     GO TO 92
   11 CONTINUE
! Branches off special cases.
      IF (ND.LE.4)   GO TO 50
! General case  --  Five data points of more
! Calculates some local variables.
      NP0=MAX(3,NP)            ! former statement 20
      NPM1=NP0-1
      RENPM1=NPM1
      RENNM2=NP0*(NP0-2)
! Main calculation for the general case
! First (outermost) DO-loop with respect to the desired points
      DO 39  II=1,NI           ! former statement 30
        IF (II.EQ.1)      IINTPV=-1
        XII=XI(II)
! Locates the interval that includes the desired point by binary
! search.
        IF (XII.LE.XD(1))  THEN
          IINT=0
        ELSE IF (XII.LT.XD(ND))  THEN
          IDMN=1
          IDMX=ND
          IDMD=(IDMN+IDMX)/2
   31     IF (XII.GE.XD(IDMD))  THEN
            IDMN=IDMD
          ELSE
            IDMX=IDMD
          END IF
          IDMD=(IDMN+IDMX)/2
          IF (IDMD.GT.IDMN)    GO TO 31
          IINT=IDMD
        ELSE
          IINT=ND
        END IF
! End of locating the interval of interest
! Interpolation or extrapolation in one of the three subcases
        IF (IINT.LE.0)  THEN
! Subcase 1  --  Linear extrapolation when the abscissa of the
!                desired point is equal to that of the first data
!                point or less.
! Estimates the first derivative when the interval is not the
! same as the one for the previous desired point.  --
! cf. Equation (8)
          IF (IINT.NE.IINTPV)  THEN
            IINTPV=IINT
            X0=XD(1)
            X1=XD(2)-X0
            X2=XD(3)-X0
            X3=XD(4)-X0
            Y0=YD(1)
            Y1=YD(2)-Y0
            Y2=YD(3)-Y0
            Y3=YD(4)-Y0
            DLT=X1*X2*X3*(X2-X1)*(X3-X2)*(X3-X1)
            A1=(((X2*X3)**2)*(X3-X2)*Y1 &
               +((X3*X1)**2)*(X1-X3)*Y2 &
               +((X1*X2)**2)*(X2-X1)*Y3)/DLT
          END IF
! Evaluates the YI value.
          YI(II)=Y0+A1*(XII-X0)
! End of Subcase 1
        ELSE IF (IINT.GE.ND)  THEN
! Subcase 2  --  Linear extrapolation when the abscissa of the
!                desired point is equal to that of the last data
!                point or greater.
! Estimates the first derivative when the interval is not the
! same as the one for the previous desired point.  --
! cf. Equation (8)
          IF (IINT.NE.IINTPV)  THEN
            IINTPV=IINT
            X0=XD(ND)
            X1=XD(ND-1)-X0
            X2=XD(ND-2)-X0
            X3=XD(ND-3)-X0
            Y0=YD(ND)
            Y1=YD(ND-1)-Y0
            Y2=YD(ND-2)-Y0
            Y3=YD(ND-3)-Y0
            DLT=X1*X2*X3*(X2-X1)*(X3-X2)*(X3-X1)
            A1=(((X2*X3)**2)*(X3-X2)*Y1 &
               +((X3*X1)**2)*(X1-X3)*Y2 &
               +((X1*X2)**2)*(X2-X1)*Y3)/DLT
          END IF
! Evaluates the YI value.
          YI(II)=Y0+A1*(XII-X0)
! End of Subcase 2
        ELSE
! Subcase 3  --  Interpolation when the abscissa of the desired
!                point is  between those of the first and last
!                data points.
! Calculates the coefficients of the third-degree polynomial (for
! NP.LE.3) or the factors for the higher-degree polynomials (for
! NP.GT.3), when the interval is not the same as the one for the
! previous desired point.
          IF (IINT.NE.IINTPV)  THEN
            IINTPV=IINT
! The second DO-loop with respect to the two endpoints of the
! interval
            DO 37  IEPT=1,2
! Calculates the estimate of the first derivative at an endpoint.
! Initial setting for calculation
              ID0=IINT+IEPT-1
              X0=XD(ID0)
              Y0=YD(ID0)
              SMPEF=0.0D0
              SMWTF=0.0D0
              SMPEI=0.0D0
              SMWTI=0.0D0
! The third (innermost) DO-loop with respect to the four primary
! estimate of the first derivative
              DO 36  IPE=1,4
! Selects point numbers of four consecutive data points for
! calculating the primary estimate of the first derivative.
                IF (IPE.EQ.1)  THEN
                  ID1=ID0-3
                  ID2=ID0-2
                  ID3=ID0-1
                ELSE IF (IPE.EQ.2)  THEN
                  ID1=ID0+1
                ELSE IF (IPE.EQ.3)  THEN
                  ID2=ID0+2
                ELSE
                  ID3=ID0+3
                END IF
! Checks if any point number falls outside the legitimate range
! (between 1 and ND).  Skips calculation of the primary estimate
! if any does.
                IF (ID1.LT.1.OR.ID2.LT.1.OR.ID3.LT.1.OR. &
                    ID1.GT.ND.OR.ID2.GT.ND.OR.ID3.GT.ND) &
                     GO TO 36
! Calculates the primary estimate of the first derivative  --
! cf. Equation (8)
                X1=XD(ID1)-X0
                X2=XD(ID2)-X0
                X3=XD(ID3)-X0
                Y1=YD(ID1)-Y0
                Y2=YD(ID2)-Y0
                Y3=YD(ID3)-Y0
                DLT=X1*X2*X3*(X2-X1)*(X3-X2)*(X3-X1)
                PE=(((X2*X3)**2)*(X3-X2)*Y1 &
                   +((X3*X1)**2)*(X1-X3)*Y2 &
                   +((X1*X2)**2)*(X2-X1)*Y3)/DLT
! Calculates the volatility factor, VOL, and distance factor,
! SXX, for the primary estimate.  --  cf. Equations (9) and (11)
                SX=X1+X2+X3
                SY=Y1+Y2+Y3
                SXX=X1*X1+X2*X2+X3*X3
                SXY=X1*Y1+X2*Y2+X3*Y3
                DNM=4.0D0*SXX-SX*SX
                B0=(SXX*SY-SX*SXY)/DNM
                B1=(4.0D0*SXY-SX*SY)/DNM
                DY0=-B0
                DY1=Y1-(B0+B1*X1)
                DY2=Y2-(B0+B1*X2)
                DY3=Y3-(B0+B1*X3)
                VOL=DY0*DY0+DY1*DY1+DY2*DY2+DY3*DY3
! Calculates the EPSLN value, which is used to decide whether or
! not the volatility factor, VOL, is essentially zero.
                EPSLN=(YD(ID0)**2+YD(ID1)**2 &
                      +YD(ID2)**2+YD(ID3)**2)*1.0D-12
! Accumulates the weighted primary estimates.  --
! cf. Equations (13) and (14)
                IF (VOL.GT.EPSLN)  THEN
! - For finite weight.
                  WT=1.0D0/(VOL*SXX)
                  SMPEF=SMPEF+PE*WT
                  SMWTF=SMWTF+WT
                ELSE
! - For infinite weight.
                  SMPEI=SMPEI+PE
                  SMWTI=SMWTI+1.0D0
                END IF
   36         CONTINUE
! End of the third DO-loop
! Calculates the final estimate of the first derivative.  --
! cf. Equation (14)
              IF (SMWTI.LT.0.5D0)  THEN
! - When no infinite weights exist.
                YP=SMPEF/SMWTF
              ELSE
! - When infinite weights exist.
                YP=SMPEI/SMWTI
              END IF
              IF (IEPT.EQ.1)  THEN
                YP0=YP
              ELSE
                YP1=YP
              END IF
! End of the calculation of the estimate of the first derivative
! at an endpoint
   37       CONTINUE
! End of the second DO-loop
            IF (NP0.LE.3)  THEN
! Calculates the coefficients of the third-degree polynomial
! (when NP.LE.3).  --  cf. Equation (4)
              DX=XD(IINT+1)-XD(IINT)
              DY=YD(IINT+1)-YD(IINT)
              A0=YD(IINT)
              A1=YP0
              YP1=YP1-YP0
              YP0=YP0-DY/DX
              A2=-(3.0D0*YP0+YP1)/DX
              A3= (2.0D0*YP0+YP1)/(DX*DX)
            ELSE
! Calculates the factors for the higher-degree polynomials
! (when NP.GT.3).  --  cf. Equation (20)
              DX=XD(IINT+1)-XD(IINT)
              DY=YD(IINT+1)-YD(IINT)
              T0=YP0*DX-DY
              T1=YP1*DX-DY
              AA0= (T0+RENPM1*T1)/RENNM2
              AA1=-(RENPM1*T0+T1)/RENNM2
            END IF
          END IF
! End of the calculation of the coefficients of the third-degree
! polynomial (when NP.LE.3) or the factors for the higher-degree
! polynomials (when NP.GT.3), when the interval is not the same
! as the one for the previous desired point.
! Evaluates the YI value.
          IF (NP0.LE.3)  THEN
! - With a third-degree polynomial.  --  cf. Equation (3)
            XX=XII-XD(IINT)
            YI(II)=A0+XX*(A1+XX*(A2+XX*A3))
          ELSE
! - With a higher-degree polynomial.  --  cf. Equation (19)
            U=(XII-XD(IINT))/DX
            UC=1.0D0-U
            V=AA0*((U**NP0)-U)+AA1*((UC**NP0)-UC)
            YI(II)=YD(IINT)+DY*U+V
          END IF
! End of Subcase 3
        END IF
   39 CONTINUE
! End of the first DO-loop
! End of general case
      RETURN
! Special cases  --  Four data points or less
! Preliminary processing for the special cases
   50 X0=XD(1)
      Y0=YD(1)
      X1=XD(2)-X0
      Y1=YD(2)-Y0
      IF (ND.EQ.2)   GO TO 60
      X2=XD(3)-X0
      Y2=YD(3)-Y0
      IF (ND.EQ.3)   GO TO 70
      X3=XD(4)-X0
      Y3=YD(4)-Y0
      GO TO 80
! Special Case 1  --  Two data points
! (Linear interpolation and extrapolation)
   60 A1=Y1/X1
      DO 61  II=1,NI
        YI(II)=Y0+A1*(XI(II)-X0)
   61 CONTINUE
! End of Special Case 1
      RETURN
! Special Case 2  --  Three data points
! (Quadratic interpolation and linear extrapolation)
   70 DLT=X1*X2*(X2-X1)
      A1=(X2*X2*Y1-X1*X1*Y2)/DLT
      A2=(X1*Y2-X2*Y1)/DLT
      A12=2.0D0*A2*X2+A1
      DO 71  II=1,NI
        XX=XI(II)-X0
        IF (XX.LE.0.0D0)  THEN
          YI(II)=Y0+A1*XX
        ELSE IF (XX.LT.X2) THEN
          YI(II)=Y0+XX*(A1+XX*A2)
        ELSE
          YI(II)=Y0+Y2+A12*(XX-X2)
        END IF
   71 CONTINUE
! End of Special Case 2
      RETURN
! Special Case 3  --  Four data points
! (Cubic interpolation and linear extrapolation)
   80 DLT=X1*X2*X3*(X2-X1)*(X3-X2)*(X3-X1)
      A1=(((X2*X3)**2)*(X3-X2)*Y1 &
         +((X3*X1)**2)*(X1-X3)*Y2 &
         +((X1*X2)**2)*(X2-X1)*Y3)/DLT
      A2=(X2*X3*(X2*X2-X3*X3)*Y1 &
         +X3*X1*(X3*X3-X1*X1)*Y2 &
         +X1*X2*(X1*X1-X2*X2)*Y3)/DLT
      A3=(X2*X3*(X3-X2)*Y1 &
         +X3*X1*(X1-X3)*Y2 &
         +X1*X2*(X2-X1)*Y3)/DLT
      A13=(3.0D0*A3*X3+2.0D0*A2)*X3+A1
      DO 81  II=1,NI
        XX=XI(II)-X0
        IF (XX.LE.0.0D0)  THEN
          YI(II)=Y0+A1*XX
        ELSE IF (XX.LT.X3) THEN
          YI(II)=Y0+XX*(A1+XX*(A2+XX*A3))
        ELSE
          YI(II)=Y0+Y3+A13*(XX-X3)
        END IF
   81 CONTINUE
! End of Special Case 3
      RETURN
! Error exit
   90 WRITE (*,99090) ND
      GO TO 99
   91 WRITE (*,99091) NI
      GO TO 99
   92 WRITE (*,99092) ID,XD(ID-1),XD(ID)
   99 WRITE (*,99099)
      RETURN
! Format statements for error messages
99090 FORMAT (1X/ ' ***   Insufficient data points.', &
        7X,'ND =',I3)
99091 FORMAT (1X/ ' ***   No desired points.', &
        7X,'NI =',I3)
99092 FORMAT (1X/ ' ***   Two data points identical or out of ', &
        'sequence.'/ &
        7X,'ID, XD(ID-1), XD(ID) =',I5,2F10.3)
99099 FORMAT (' Error detected in the UVIP3P subroutine'/)
      END subroutine uvip3p

end module mod_numerics
