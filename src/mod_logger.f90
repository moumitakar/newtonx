! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_logger
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-03-27
  !!
  !! Updated: 2022-01-11 Logger object and documentation
  !!
  !! This module defines the ``nx_logger_t`` and ``nx_timer_t`` types. It also exports
  !! the ``nx_log`` object, to be used as the main logger during the execution of the
  !! program.
  !!
  use mod_kinds, only: dp
  use mod_constants, only: MAX_STR_SIZE
  use mod_print_utils, only: &
       & message, date, log_file
  use iso_fortran_env, only: stdout=>output_unit, &
       & stdin=>input_unit, &
       & stderr=>error_unit

  implicit none

  private

  ! Logging utilities
  public :: call_external
  public :: check_error

  ! Logger
  type, public :: nx_logger_t
     !! Logger object for NX.
     !!
     !! A logger is an type holding information about when, how and where to report
     !! information about a running module or program.  A specificity of dynamics run is
     !! that the user may want to only log information every ``kt`` time-steps to reduce
     !! the size of the output data.  This object is thus aware of the current step, and
     !! of the ``kt`` parameter.  Depending on the desired logging level, information
     !! will get printed to ``logunit`` only every ``kt`` step, while error messages
     !! (printed to ``errunit``) will be printed evry time they occur.
     !!
     !! Usually a message consist of a printing level (see below), possibly a time stamp
     !! and an origin (file and procedure), and a title, for this result:
     !!
     !!     [level]<time_stamp> (In file, procedure): title
     !!          Info 1
     !!          Info 2
     !!
     !! where ``Info 1`` and ``Info 2`` are for instance the lines of a matrix.
     !!
     !! Usage:
     !!```fortran
     !! type(nx_logger_t) :: my_logger
     !! my_logger%init(kt, step, lvprt, logfile='my_log.log', errfile='my_err.err')
     !! my_logger%log(LOG_INFO, 'General information message')
     !! my_logger%log(LOG_DEBUG, some_matrix, title='Some matrix to print for debug')
     !! my_logger%log(LOG_ERROR, 'This is an error message')
     !!```
     !!
     !! The ``init`` routine will open the ``logfile`` and ``errfile`` files, and assign
     !! the corresponding unit to the ``logunit`` and ``errunit`` members of the object.
     !! If not specified, ``stdout`` will be taken for ``logunit``, and ``stderr`` will
     !! be taken for ``errunit``.
     !!
     !! Besides the main ``log`` interface routine, the ``log_file`` and ``log_blank``
     !! routine can also be used, to print the content of a file and blank lines respectively.
     !!
     !! It is always possible to force the printing of any element by using the
     !! ``force=.true.`` flag.  At each time-step, it is required to update the internal
     !! ``step`` of the logger with the routine ``update``.
     !!
     !! All type-bound logging routines are wrappers around the ``message`` interface
     !! defined in ``mod_print_utils.f08``, and the implementation details are described
     !! in the corresponding documentation.
     !!
     !! Five logging levels are currently defined and exported by this module, from
     !! highest to lowest priority:
     !!
     !! - ``LOG_ERROR``: Error messages, always printed to ``errunit`` ;
     !! - ``LOG_WARN``: Warning that the user should be aware of, always printed to
     !!   ``logunit`` ;
     !! - ``LOG_INFO``: General information, printed every ``kt`` step to ``logunit`` ;
     !! - ``LOG_TRIVIA``: More detailed information, printed every ``kt`` step to
     !!   ``logunit`` ;
     !! - ``LOG_DEBUG``: Full debug information, printed every ``kt`` step to ``logunit``.
     !!
     integer :: step
     !! Current time step.
     integer :: lvprt
     !! Desired logging level (default: ``LOG_INFO``).
     integer :: kt
     !! Interval at which information should be printed.
     integer :: errunit
     !! File unit for error messages (default: ``stderr``).
     integer :: logunit
     !! File unit for other messages (default: ``stdout``).
     logical :: add_time_stamp
     !! Add time stamps to messages (default: ``.true.``).
   contains
     procedure, pass :: init => logger_init
     procedure, pass :: update => logger_update
     procedure, pass :: log_file => logger_log_file
     generic, public :: log => &
          & logger_log_c, logger_log_c_d1, &
          & logger_log_r64_d1, logger_log_r64_d2, &
          & logger_log_c64_d1, &
          & logger_log_i32_d1, logger_log_i32_d2
     procedure, private :: logger_log_c
     procedure, private :: logger_log_c_d1
     procedure, private :: logger_log_r64_d1
     procedure, private :: logger_log_c64_d1
     procedure, private :: logger_log_r64_d2
     procedure, private :: logger_log_i32_d1
     procedure, private :: logger_log_i32_d2
     procedure, pass :: log_blank => logger_log_blank
  end type nx_logger_t
  type(nx_logger_t), public :: nx_log
  !! Main logger for the Newton-X program.

  integer, parameter, public :: LOG_DEBUG = 10
  !! Debug level priority.
  integer, parameter, public :: LOG_TRIVIA = 20
  !! Trivia level priority.
  integer, parameter, public :: LOG_INFO = 30
  !! Info level priority.
  integer, parameter, public :: LOG_WARN = 40
  !! Warning level priority.
  integer, parameter, public :: LOG_ERROR = 50
  !! Error level priority.

  ! Configuration printing
  public :: print_conf_ele
  interface print_conf_ele
     !! Utility to print configuration elements.
     !!
     module procedure print_conf_ele_i
     module procedure print_conf_ele_r
     module procedure print_conf_ele_s
     module procedure print_conf_ele_log
  end interface print_conf_ele

  ! Error codes (starting at 256)
  integer, parameter, public :: ERR_QM_EXECUTION = 256
  integer, parameter, public :: ERR_CIO_EXECUTION = 257
  integer, parameter, public :: ERR_PERL_SCRIPT = 258
  integer, parameter, public :: ERR_HDF5 = 259

contains

  subroutine logger_init(self, kt, step, lvprt, logfile, errfile, tstamp)
    !! Initialize a logger object.
    !!
    !!
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: kt
    !! Interval at which to log information.
    integer, intent(in) :: step
    !! Current step.
    integer, intent(in) :: lvprt
    !! Level of output desired.
    character(len=*), intent(in), optional :: logfile
    !! File for main log (default: ``none``, log to ``stdout``).
    character(len=*), intent(in), optional :: errfile
    !! File for error messages (default: ``none``, log to ``stderr``).
    logical, intent(in), optional :: tstamp
    !! Add time stamps to logs (default: ``.true.``).

    integer :: logunit, errunit
    logical :: time_stamp

    self%step = step
    self%kt = kt
    self%lvprt = -10*lvprt + 60

    logunit = -1
    errunit = -1
    if (present(logfile)) then
       inquire(file=logfile, number=logunit)
       if (logunit == -1) then
          open(newunit=logunit, file=logfile, position='append', action='write')
       end if
       self%logunit = logunit

       ! Now check for errfile. If present, set it as the error file. Else, we take
       ! logfile as the error file.
       if (present(errfile)) then
          inquire(file=errfile, number=errunit)

          ! We have to check for the value -1 exctly, as ``newunit`` will give a negative
          ! number (as recommanded by the standard).
          if (errunit == -1) then
             open(newunit=errunit, file=errfile, position='append', action='write')
          end if
          self%errunit = errunit
       else
          self%errunit = logunit
       end if

    else
       self%logunit = stdout
       self%errunit = stderr
    end if

    time_stamp = .true.
    if (present(tstamp)) time_stamp = tstamp

    self%add_time_stamp = time_stamp
  end subroutine logger_init


  subroutine logger_update(self, step)
    !! Update the current step.
    !!
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: step
    !! New step.

    self%step = step
  end subroutine logger_update


  subroutine logger_log_file(self, level, filename, title, file, proc, force)
    !! Log the content of a file.
    !!
    !! The content of ``filename`` is printed, with optional ``title``.
    !!
    !! Wrapper around ``message`` function.
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: level
    !! Logging level.
    character(len=*), intent(in) :: filename
    !! File to print.
    character(len=*), intent(in) :: title
    !! Title to print.
    character(len=*), intent(in), optional :: file
    !! File from which the logger is called.
    character(len=*), intent(in), optional :: proc
    !! Procedure calling the logger.
    logical, intent(in), optional :: force
    !! Force the printing at every step.

    logical :: forceprt
    forceprt = .false.
    if (present(force)) forceprt = force

    if (printit(self, level) .or. forceprt) then
       call log_file(filename=filename, unit=self%logunit, &
            & prefix=log_header(level), time_stamp=self%add_time_stamp, &
            & title=title, file=file, proc=proc&
            &)
    end if
  end subroutine logger_log_file


  subroutine logger_log_c(self, level, msg, file, proc, force)
    !! Log a character string.
    !!
    !! Wrapper around ``message`` function.
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: level
    !! Logging level.
    character(len=*), intent(in) :: msg
    !! Message to print.
    character(len=*), intent(in), optional :: file
    !! File from which the logger is called.
    character(len=*), intent(in), optional :: proc
    !! Procedure calling the logger.
    logical, intent(in), optional :: force
    !! Force the printing at every step.

    logical :: forceprt
    forceprt = .false.
    if (present(force)) forceprt = force

    if (printit(self, level) .or. forceprt) then
       call message(msg=msg, unit=self%logunit, prefix=log_header(level), &
            & time_stamp=self%add_time_stamp, file=file, proc=proc)
    end if

  end subroutine logger_log_c


  subroutine logger_log_c_d1(self, level, msg, nlines, title, file, proc, force)
    !! Log an array of character strings.
    !!
    !! Wrapper around ``message`` function.
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: level
    !! Logging level.
    character(len=*), dimension(:), intent(in) :: msg
    !! Message to print.
    integer, intent(in), optional :: nlines
    !! Number of lines from array to print.
    character(len=*), intent(in), optional :: title
    !! Title of the array.
    character(len=*), intent(in), optional :: file
    !! File from which the logger is called.
    character(len=*), intent(in), optional :: proc
    !! Procedure calling the logger.
    logical, intent(in), optional :: force
    !! Force the printing at every step.

    logical :: forceprt
    forceprt = .false.
    if (present(force)) forceprt = force

    if (printit(self, level) .or. forceprt) then
       call message(msg=msg, unit=self%logunit, prefix=log_header(level), &
            & time_stamp=self%add_time_stamp, nlines=nlines, title=title, &
            & file=file, proc=proc)
    end if

  end subroutine logger_log_c_d1


  subroutine logger_log_r64_d1(self, level, data, title, fmt, labels, fac, file,&
       & proc, force)
    !! Log a 1D array of ``dp``.
    !!
    !! Wrapper around ``message`` function.
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: level
    !! Logging level.
    real(dp), dimension(:), intent(in) :: data
    !! Vector to print.
    character(len=*), intent(in), optional :: title
    !! Title of the array.
    character(len=*), intent(in), optional :: fmt
    !! Format for printing.
    character(len=*), intent(in), optional :: labels(:)
    !! Labels for the lines of the vector.
    real(dp), intent(in), optional :: fac
    !! Scaling factor for printing.
    character(len=*), intent(in), optional :: file
    !! File in which the routine is called (default: empty).
    character(len=*), intent(in), optional :: proc
    !! Name of the procedure calling this routine (default: empty).
    logical, intent(in), optional :: force
    !! Force the printing at every step.

    logical :: forceprt
    forceprt = .false.
    if (present(force)) forceprt = force

    if (printit(self, level) .or. forceprt) then
         call message(data, unit=self%logunit, &
              & prefix=log_header(level), time_stamp=self%add_time_stamp, &
              & title=title, fac=fac, labels=labels, fmt=fmt, &
              & file=file, proc=proc)
    end if
  end subroutine logger_log_r64_d1


  subroutine logger_log_c64_d1(self, level, data, title, fmt, labels, fac, file,&
       & proc, force)
    !! Log a 1D array of ``dp`` complex.
    !!
    !! Wrapper around ``message`` function.
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: level
    !! Logging level.
    complex(dp), dimension(:), intent(in) :: data
    !! Vector to print.
    character(len=*), intent(in), optional :: title
    !! Title of the array.
    character(len=*), intent(in), optional :: fmt
    !! Format for printing.
    character(len=*), dimension(:), intent(in), optional :: labels
    !! Labels for the lines of the vector.
    real(dp), intent(in), optional :: fac
    !! Scaling factor for printing.
    character(len=*), intent(in), optional :: file
    !! File in which the routine is called (default: empty).
    character(len=*), intent(in), optional :: proc
    !! Name of the procedure calling this routine (default: empty).
    logical, intent(in), optional :: force
    !! Force the printing at every step.

    logical :: forceprt
    forceprt = .false.
    if (present(force)) forceprt = force

    if (printit(self, level) .or. forceprt) then
       call message(data, unit=self%logunit, &
            & prefix=log_header(level), time_stamp=self%add_time_stamp, &
            & title=title, fac=fac, labels=labels, fmt=fmt, &
            & file=file, proc=proc)
    end if
  end subroutine logger_log_c64_d1


  subroutine logger_log_r64_d2(self, level, data, title, fmt, fac, expand,&
       & transpose, clabels, file, proc, force)
    !! Log a 2D array of ``dp``.
    !!
    !! Wrapper around ``message`` function.
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: level
    !! Logging level.
    real(dp), dimension(:, :), intent(in) :: data
    !! Matrix to print.
    character(len=*), intent(in), optional :: title
    !! Title of the array.
    character(len=*), intent(in), optional :: fmt
    !! Format for printing.
    real(dp), intent(in), optional :: fac
    !! Scaling factor for printing.
    logical, intent(in), optional :: expand
    !! If ``.true.``, expand the matrix in full form.
    logical, intent(in), optional :: transpose
    !! If ``.true.``, print the transpose of the matrix.
    character(len=*), dimension(:), intent(in), optional :: clabels
    !! Labels for the columns.
    character(len=*), intent(in), optional :: file
    !! File in which the routine is called (default: empty).
    character(len=*), intent(in), optional :: proc
    !! Name of the procedure calling this routine (default: empty).
    logical, intent(in), optional :: force
    !! Force the printing at every step.

    logical :: forceprt
    forceprt = .false.
    if (present(force)) forceprt = force

    if (printit(self, level) .or. forceprt) then
       call message(data, unit=self%logunit, &
            & prefix=log_header(level), time_stamp=self%add_time_stamp, &
            & title=title, expand=expand, transpose=transpose, &
            & fac=fac, fmt=fmt, clabels=clabels,&
            & file=file, proc=proc)
    end if
  end subroutine logger_log_r64_d2


  subroutine logger_log_i32_d1(self, level, data, title, fmt, fac, file,&
       & proc, force)
    !! Log a 1D array of ``integer``.
    !!
    !! Wrapper around ``message`` function.
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: level
    !! Logging level.
    integer, dimension(:), intent(in) :: data
    !! Vector to print.
    character(len=*), intent(in), optional :: title
    !! Title of the array.
    character(len=*), intent(in), optional :: fmt
    !! Format for printing.
    real(dp), intent(in), optional :: fac
    !! Scaling factor for printing.
    character(len=*), intent(in), optional :: file
    !! File in which the routine is called (default: empty).
    character(len=*), intent(in), optional :: proc
    !! Name of the procedure calling this routine (default: empty).
    logical, intent(in), optional :: force
    !! Force the printing at every step.

    logical :: forceprt
    forceprt = .false.
    if (present(force)) forceprt = force

    if (printit(self, level) .or. forceprt) then
       call message(data, unit=self%logunit, &
            & prefix=log_header(level), time_stamp=self%add_time_stamp, &
            & title=title, fac=fac, fmt=fmt, &
            & file=file, proc=proc)
    end if
  end subroutine logger_log_i32_d1


  subroutine logger_log_i32_d2(self, level, data, title, fmt, fac, expand,&
       & transpose, clabels, file, proc, force)
    !! Log a 2D array of ``integer``.
    !!
    !! Wrapper around ``message`` function.
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: level
    !! Logging level.
    integer, dimension(:, :), intent(in) :: data
    !! Matrix to print.
    character(len=*), intent(in), optional :: title
    !! Title of the array.
    character(len=*), intent(in), optional :: fmt
    !! Format for printing.
    real(dp), intent(in), optional :: fac
    !! Scaling factor for printing.
    logical, intent(in), optional :: expand
    !! If ``.true.``, expand the matrix in full form.
    logical, intent(in), optional :: transpose
    !! If ``.true.``, print the transpose of the matrix.
    character(len=*), dimension(:), intent(in), optional :: clabels
    !! Labels for the columns.
    character(len=*), intent(in), optional :: file
    !! File in which the routine is called (default: empty).
    character(len=*), intent(in), optional :: proc
    !! Name of the procedure calling this routine (default: empty).
    logical, intent(in), optional :: force
    !! Force the printing at every step.

    logical :: forceprt
    forceprt = .false.
    if (present(force)) forceprt = force

    if (printit(self, level) .or. forceprt) then
       call message(data, unit=self%logunit, &
            & prefix=log_header(level), time_stamp=self%add_time_stamp, &
            & title=title, expand=expand, transpose=transpose, &
            & fac=fac, fmt=fmt, clabels=clabels,&
            & file=file, proc=proc)
    end if
  end subroutine logger_log_i32_d2


  subroutine logger_log_blank(self, level, n, force)
    !! Print a blank line in the log file.
    !!
    class(nx_logger_t), intent(inout) :: self
    !! Logger object.
    integer, intent(in) :: level
    !! Logging level.
    integer, intent(in), optional :: n
    !! Number of blank lines to print (default: 1).
    logical, intent(in), optional :: force
    !! Force the printing at every step.

    logical :: forceprt
    integer :: nroflines, i

    nroflines = 1
    if (present(n)) nroflines = n

    forceprt = .false.
    if (present(force)) forceprt = force
    if (printit(self, level) .or. forceprt) then
       do i=1, nroflines
          write(self%logunit, '(A)') ''
       end do
    end if
  end subroutine logger_log_blank


  logical function printit(logger, prtlevel)
    !! Decide wether the logger should print the information.
    !!
    !! The decision is based both on the ``prtlevel`` of the message (error and warnings
    !! should always be printed), and on the current ``step`` value (print only every
    !! ``kt`` step).
    type(nx_logger_t)  :: logger
    !! Logger object.
    integer :: prtlevel
    !! Level to test.

    if (prtlevel >= 40) then
       ! Always print warnings and errors !
       printit = .true.
    else
       printit = (&
            & mod(logger%step, logger%kt) == 0 &
            & .and. (logger%lvprt <= prtlevel) &
            &)
    end if

  end function printit


  function log_header(level)
    !! Construc the header of the log based on ``level``.
    !!
    integer :: level
    !! Print level of the message.
    character(len=7) :: log_header

    if (level <= 10) then
       log_header = 'DEBUG'
    else if (level <= 20) then
       log_header = 'TRIVIA'
    else if (level <= 30) then
       log_header = 'INFO'
    else if (level <= 40) then
       log_header = 'WARNING'
    else if (level <= 50) then
       log_header = 'ERROR'
    end if

  end function log_header


  subroutine call_external(cmd, ierr, prog, errfile, outfile)
    !! General interface to call an external program.
    !!
    !! This routine is essentially a wrapper around
    !! ``execute_command_line``, so the command is always run by the
    !! shell. By default the routine prints the full name of the
    !! command to be launched. Optionally, in case the command name is
    !! too long, a ``prog`` option can be specified that will be
    !! printed instead.
    !!
    !! Ex:
    !! ``call_external("myperl.pl -o opt1 -b opt2 ...", ierr)``
    !! ``call_external("myperl.pl -o opt1 -b opt2 ...", ierr, "myperl.pl")``
    !!
    !! TODO: System-specific syntax ??
    character(len=*), intent(in) :: cmd
    !! Command that will be run by the shell
    integer, intent(out) :: ierr
    !! Return code from ``execute_command_line``
    character(len=*), intent(in), optional :: prog
    !! Optional short name for the command launched.
    character(len=*), intent(in), optional :: errfile
    !! Error stream for the command (default: no redirection)
    character(len=*), intent(in), optional :: outfile
    !! Output stream for the command (default: no redirection).


    character(len=512) :: name
    ! Name that will be printed: either cmd or prog
    character(len=512) :: d
    ! Date
    character(len=1024) :: msg
    character(len=MAX_STR_SIZE) :: execmd

    integer :: st
    ! cmdstat for execute_command_line

    real(dp) :: time
    integer :: count0, count1, count_rate, count_max

    if (present(prog)) then
       name = prog
    else
       name = cmd
    end if

    ! Build command line
    execmd = trim(cmd)

    if (present(outfile)) then
       if (outfile /= '') execmd = trim(execmd)//' > '//trim(outfile)
    end if

    if (present(errfile)) then
       if (errfile == 'stdout') then
          execmd = trim(execmd)//' 2>&1'
       else if (errfile /= '') then
          execmd = trim(execmd)//' 2> '//trim(errfile)
       end if
    end if

    ! Report
    d = date()
    call nx_log%log(LOG_INFO, 'Start '//trim(execmd))

    ! Execute command
    ! 2020-07-15 BD:
    ! For gcc 7.5.0 (at least), ierr has to be initialized as it has
    ! intent(inout) in the implementation.
    ierr = 0
    ! call cpu_time(start)
    call system_clock(count0, count_rate, count_max)
    call execute_command_line(trim(execmd), exitstat=ierr, cmdstat=st)
    call system_clock(count1)
    time = (1.0*count1 - count0) / count_rate

    ! Report again
    d = date()

    ! The error termination is expected to be catch by ``check_error`` just
    ! after this function !
    if (ierr == 0) then
       write(msg, '(A,F10.3,A)') 'Done in ', time, ' s.'
       call nx_log%log(LOG_INFO, msg)
    else
       write(msg, '(A,A,A,F10.3,A)') &
            & 'Error termination of ', trim(name), ' after ', time, ' s.'
       call nx_log%log(LOG_ERROR, msg)
    end if
  end subroutine call_external


  subroutine check_error(ierr, errcode, msg, file, system)
    !! Check an error code.
    !!
    !! If ``ierr`` (status code from previous call for instance) is
    !! not zero, ``msg`` gets printed in ``STDERR``, and the program
    !! stops with error code ``errcode``.
    integer, intent(in) :: ierr
    !! Output to check.
    integer, intent(in) :: errcode
    !! Expected error code.
    character(len=*), intent(in), optional :: msg
    !! Error Message to print
    character(len=*), intent(in), optional :: file
    logical, optional :: system

    character(len=MAX_STR_SIZE) :: str
    logical :: prt_system
    integer :: err_sys

    str = 'Undefined error encountered'
    if (present(msg)) str = msg

    prt_system = .false.
    if (present(system)) prt_system = system

    if (ierr /= 0) then
       if (prt_system) then
          err_sys = ierrno()
          write(str, *) trim(str)//NEW_LINE('a')//"errno = ", err_sys
       end if
       call nx_log%log(LOG_ERROR, str, file=file)
       call perror('LAST SYSTEM ERROR')
       error stop errcode
    end if

  end subroutine check_error


  subroutine print_conf_ele_i(ele, label, unit, elefmt, prtfmt)
    !! Print an integer configuration element.
    !!
    !! The printing has the following form:
    !!
    !!   label = ele
    !!
    !! where the `` = `` element is automatically added. The printing of the
    !! whole line is optionnally defined by ``prtfmt`` (default: ``(a20, a3,
    !! a10)``) and the format of the configuration element is given by
    !! ``elefmt`` (default: ``(i10)``).
    integer, intent(in) :: ele
    !! Element to print.
    character(len=*), intent(in) :: label
    !! Label
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: elefmt
    character(len=*), intent(in), optional :: prtfmt

    character(len=10) :: i_char
    character(len=256) :: fmt, efmt
    integer :: out

    out = stdout
    if (present(unit)) out = unit

    if (present(prtfmt)) then
       fmt = prtfmt
    else
       fmt = '(a30, a3, a10)'
    end if

    if (present(elefmt)) then
       efmt = elefmt
    else
       efmt = '(i10)'
    end if


    write(i_char, fmt=efmt) ele
    write(out, fmt=fmt) trim(label), ' = ', adjustl(i_char)
  end subroutine print_conf_ele_i


  subroutine print_conf_ele_r(ele, label, unit, elefmt, prtfmt)
    !! Print a real configuration element.
    !!
    !! The printing has the following form:
    !!
    !!   label = ele
    !!
    !! where the `` = `` element is automatically added. The printing of the
    !! whole line is optionnally defined by ``prtfmt`` (default: ``(a20, a3,
    !! a10)``) and the format of the configuration element is given by
    !! ``elefmt`` (default: ``(f10.6)``).
    real(dp), intent(in) :: ele
    character(len=*), intent(in) :: label
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: elefmt
    character(len=*), intent(in), optional :: prtfmt

    character(len=10) :: r_char
    character(len=256) :: fmt, efmt
    integer :: out

    out = stdout
    if (present(unit)) out = unit

    if (present(prtfmt)) then
       fmt = prtfmt
    else
       fmt = '(a30, a3, a10)'
    end if

    if (present(elefmt)) then
       efmt = elefmt
    else
       efmt = '(f10.6)'
    end if

    write(r_char, fmt=efmt) ele
    write(out, fmt=fmt) trim(label), ' = ', adjustl(r_char)
  end subroutine print_conf_ele_r


  subroutine print_conf_ele_s(ele, label, unit, elefmt, prtfmt)
    !! Print a real configuration element.
    !!
    !! The printing has the following form:
    !!
    !!   label = ele
    !!
    !! where the `` = `` element is automatically added. The printing of the
    !! whole line is optionnally defined by ``prtfmt`` (default: ``(a20, a3,
    !! a10)``) and the format of the configuration element is given by
    !! ``elefmt`` (default: ``(a10)``).
    character(len=*), intent(in) :: ele
    character(len=*), intent(in) :: label
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: elefmt
    character(len=*), intent(in), optional :: prtfmt

    character(len=256) :: fmt, efmt
    integer :: out

    out = stdout
    if (present(unit)) out = unit

    if (present(prtfmt)) then
       fmt = prtfmt
    else
       fmt = '(a30, a3, a)'
    end if

    if (present(elefmt)) then
       efmt = elefmt
    else
       efmt = '(a10)'
    end if

    write(out, fmt=fmt) trim(label), ' = ', adjustl(trim(ele))
  end subroutine print_conf_ele_s


  subroutine print_conf_ele_log(ele, label, unit, elefmt, prtfmt)
    !! Print an integer configuration element.
    !!
    !! The printing has the following form:
    !!
    !!   label = ele
    !!
    !! where the `` = `` element is automatically added. The printing of the
    !! whole line is optionnally defined by ``prtfmt`` (default: ``(a20, a3,
    !! a10)``) and the format of the configuration element is given by
    !! ``elefmt`` (default: ``(i10)``).
    logical, intent(in) :: ele
    !! Element to print.
    character(len=*), intent(in) :: label
    !! Label
    integer, intent(in), optional :: unit
    character(len=*), intent(in), optional :: elefmt
    character(len=*), intent(in), optional :: prtfmt

    character(len=10) :: i_char
    character(len=256) :: fmt, efmt
    integer :: out

    out = stdout
    if (present(unit)) out = unit

    if (present(prtfmt)) then
       fmt = prtfmt
    else
       fmt = '(a30, a3, a10)'
    end if

    if (present(elefmt)) then
       efmt = elefmt
    else
       efmt = '(L2)'
    end if


    write(i_char, fmt=efmt) ele
    write(out, fmt=fmt) trim(label), ' = ', adjustl(i_char)
  end subroutine print_conf_ele_log


end module mod_logger
