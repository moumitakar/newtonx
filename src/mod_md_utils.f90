! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_md_utils
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-03-27
  !!
  !! This module contains helper routines that are used in the main
  !! MD program.
  use mod_kinds, only: dp
  use mod_constants, only: MAX_STR_SIZE
  use mod_trajectory, only: nx_traj_t
  use mod_configuration, only: nx_config_t
  use mod_interface, only: &
       & rm, copy, mkdir
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: type_of_dynamics
  public :: require_cio
  public :: is_section_here
  public :: determine_analytical_prog
  public :: create_directories, copy_initial_files, clean_pwd_content

  type, public :: nx_opts_t
     !! Type for storing command line options.
     !!
     character(len=:), allocatable :: input
     logical :: help = .false.
     logical :: dry_run = .false.
     logical :: clean_pwd = .false.
     integer :: status = 0
   contains
     procedure :: parse => nx_parse_cmdline
  end type nx_opts_t

contains

  subroutine nx_parse_cmdline(self)
    class(nx_opts_t), intent(inout) :: self

    integer :: nargs, iarg
    character(len=MAX_STR_SIZE) :: arg
    logical :: skip_next

    self%input = 'UNALLOCATED'

    nargs = command_argument_count()
    skip_next = .false.
    do iarg = 1, nargs
       call get_command_argument(iarg, arg)
       if (len(trim(arg)) == 0) exit

       if (skip_next) then
          skip_next = .false.
          cycle

       else if ((arg == '--help') .or. (arg == '-h')) then
          call nx_usage()
          self%status = -1
          self%help = .true.
          return

       else if ((arg == '--input') .or. (arg == '-i')) then
          call get_command_argument(iarg+1, arg)
          if (allocated(self%input)) deallocate(self%input)
          allocate(character(len=len_trim(arg)) :: self%input)
          self%input = trim(arg)
          skip_next = .true.

       else if ((arg == '--dry-run') .or. (arg == '-d')) then
          self%dry_run = .true.

       else if ((arg == '--clean')) then
          self%clean_pwd = .true.

       else
          write(*, '(A)') 'Unrecognized option: '//trim(arg)
          write(*, '(A)') ''
          call nx_usage()
          self%status = -1
          return

       end if
    end do

  end subroutine nx_parse_cmdline


  subroutine nx_usage(output)
    integer, intent(in), optional :: output

    integer :: out

    character(len=*), parameter :: nl = NEW_LINE('c')
    character(len=*), parameter :: THIS_NAME = 'nx_moldyn'

    out = stdout
    if (present(output)) out = output

    write(out, '(A)') 'Usage:'
    write(out, '(A)') '  '//THIS_NAME//' [-i input] [-d] [-h] [--clean]'
    write(out, '(A)') ''
    write(out, '(A)') 'Options:'
    write(out, '(A)') '  -h, --help      Print this help.'
    write(out, '(A)') '  -i, --inp       Select which input file to use.'
    write(out, '(A)') '                   Default: user_config.nml'
    write(out, '(A)') '  -d, --dry-run   Dry-run: do not run the dynamics, stop after'
    write(out, '(A)') '                   generation of the complete configuration.'
    write(out, '(A)') '  --clean         Clean the content of the working directory'
    write(out, '(A)') '                   before running the dynamics.'
    write(out, '(A)') ''
  end subroutine nx_usage


  subroutine create_directories(config, ierr)
    !! Create the directory structure.
    !!
    !! This routine creates the ``TEMP`` directory for running NX, the
    !! ``config%output_path`` (default ``RESULTS``) and the ``config%debug_path``
    !! (default ``DEBUG``) folders.
    !!
    !! If ``TEMP`` already exists (in case of restart), then it is renamed as
    !! ``TEMP.old``,  if a ``TEMP.old`` folder already exists, it is deleted.
    type(nx_config_t), intent(in) :: config
    !! Main NX configuration.
    integer, intent(inout) :: ierr
    !! Status (0 for normal termination).

    logical :: ext

    inquire(file='TEMP.old', exist=ext)
    if (ext) then
       ierr = rm(['TEMP.old'])
    end if
    ierr = rename('TEMP', 'TEMP.old')
    ierr = mkdir('TEMP')

    inquire(file=config%debug_path, exist=ext)
    if (.not. ext) then
       ierr = mkdir(config%debug_path)
    end if

    inquire(file=config%output_path, exist=ext)
    if (.not. ext) then
       ierr = mkdir(config%output_path)
    end if

    if (config%run_complex) then
       if (config%gamma_model == 2) then
          ierr = mkdir('TEMP/ref_calc/')
       else if (config%gamma_model == 1) then
          ierr = copy(config%path_gamma_model, 'TEMP/')
       end if
    end if
  end subroutine create_directories


  subroutine copy_initial_files(config, configfile, ierr)
    !! Copy input files to ``TEMP``.
    !!
    type(nx_config_t), intent(in) :: config
    !! General NX configuration.
    character(len=*), intent(in) :: configfile
    !! Name of the configuration file.
    integer, intent(inout) :: ierr
    !! Status (0 for normal termination.

    character(len=256) :: to_copy(8)

    if (config%nxrestart == 0) then
       to_copy(1) = config%init_veloc
       to_copy(2) = config%init_geom
       to_copy(3) = 'wf.inp'
       to_copy(4) = configfile
       to_copy(5) = 'JOB_AD/'
       to_copy(6) = 'JOB_NAD/'
       to_copy(7) = 'rndseed'
       to_copy(8) = 'REF/'
    else
       to_copy(1) = config%init_veloc
       to_copy(2) = config%init_geom
       to_copy(3) = 'INFO_RESTART/wf.inp'
       to_copy(4) = 'INFO_RESTART/nx-restart-config.nml'
       to_copy(5) = 'JOB_AD/'
       to_copy(6) = 'JOB_NAD/'
       to_copy(7) = 'INFO_RESTART/rndseed'
       to_copy(8) = 'REF/'
    end if

    ierr = copy(to_copy, 'TEMP/')
    call execute_command_line("cp gamma_state* TEMP/")
  end subroutine copy_initial_files


  subroutine clean_pwd_content(config, ierr)
    !! Clean the working directory.
    !!
    !! This routine will delete folders from a previous dynamics, such as `RESULTS/`,
    !! `DEBUG/` and `TEMP/`.
    type(nx_config_t), intent(in) :: config
    !! Main NX configuration.
    integer, intent(inout) :: ierr
    !! Status code.

    character(len=20) :: array(3)
    print *, 'OUTPUT: '//config%output_path
    array = [ character(len=20) :: &
         & trim(config%output_path), 'TEMP/', 'DEBUG/' &
         & ]
    ierr = rm(array)
  end subroutine clean_pwd_content


  subroutine type_of_dynamics(conf, traj)
    !! Defines the type of dynamics that will be run.
    !!
    !! This subroutine is mostly kept for legacy reasons. Based on
    !! the difference of energy between the current surface and the
    !! one directly above (\(\Delta E_{sup}\)) and the one directly
    !! below (\(\Delta E_{sup}\)), the possibilities are:
    !!
    !! - Type = 1: \(\Delta E_{sup} > thres\) and \(\Delta E_{inf} > thres\);
    !! - Type = 2: \(\Delta E_{sup} > thres\) and \(\Delta E_{inf} <
    !! thres\);
    !! - Type = 3: \(\Delta E_{sup} < thres\) and \(\Delta E_{inf} >
    !! thres\);
    !! - Type = 4: \(\Delta E_{sup} < thres\) and \(\Delta E_{inf} <
    !! thres\);
    type(nx_traj_t), intent(inout) :: traj
    type(nx_config_t), intent(in) :: conf

    real(dp) :: de_inf, de_sup
    real(dp) :: thres
    real(dp), allocatable :: epot(:)
    integer :: nstatdyn, nstat

    allocate( epot(size(traj%epot)) )
    nstatdyn = traj%nstatdyn
    nstat = conf%nstat
    thres = conf%thres
    epot = traj%epot

    if (nstatdyn-1 >= 1) then
       de_inf = abs( epot(nstatdyn) - epot(nstatdyn - 1) )
    else
       de_inf = abs( epot(nstatdyn) + 10 )
    endif

    if (nstatdyn + 1 <= nstat) then
       de_sup = abs( epot(nstatdyn + 1) - epot(nstatdyn) )
    else
       de_sup = abs( epot(nstatdyn) + 10 )
    endif

    if ((de_sup > thres) .and. (de_inf > thres)) then
       traj%typeofdyn = 1
    else if ((de_sup > thres) .and. (de_inf <= thres)) then
       traj%typeofdyn = 2
    else if ((de_sup <= thres) .and. (de_inf > thres)) then
       traj%typeofdyn = 3
    else
       traj%typeofdyn = 4
    endif
  endsubroutine type_of_dynamics


  function require_cio(config_file) result(call_cio)
    !! DEPRECATED !!!
    !! Determine if cioverlap routines should be called.
    !!
    !! We determine this by looking at the configuration file. We
    !! should call cioverlap only if '&cioverlap' is found in the
    !! configuration file !
    character(len=*) :: config_file
    !! Configuration file
    logical :: call_cio
    !! .true. if cioverlap should called, .false. if not

    integer :: u, id, io
    character(len=512) :: buf

    call_cio = .false.
    open(newunit=u, file=config_file, action='read')
    do
       read(u, *, iostat=io) buf
       if (io /= 0) exit

       id = index(buf, '&cioverlap')
       if (id /= 0) then
          call_cio = .true.
       end if
    end do

    close(u)
  end function require_cio


  function is_section_here(config_file, section) result(res)
    !! Determine if ``section``  is present in config file.
    !!
    !! This function is used to determine if some code has to be
    !! called, like cioverlap or adaptive time_step.
    character(len=*) :: config_file
    !! Configuration file.
    character(len=*) :: section
    !! Section to look for (including '&' !).
    logical :: res
    !! ``.true.`` if ``section`` is found.

    integer :: u, id, io
    character(len=512) :: buf

    res = .false.
    open(newunit=u, file=config_file, action='read')
    do
       read(u, *, iostat=io) buf
       if (io /= 0) exit

       id = index(buf, section)
       if (id /= 0) then
          res = .true.
       end if
    end do

    close(u)
  end function is_section_here



  subroutine determine_analytical_prog(config_file, new_name)
    character(len=*), intent(in) :: config_file
    character(len=*), intent(inout) :: new_name

    integer :: u, id, io
    character(len=512) :: buf

    open(newunit=u, file=config_file, action='read')
    do
       read(u, *, iostat=io) buf
       if (io /= 0) exit

       id = index(buf, '&sbh')
       if (id /= 0) then
          new_name = 'sbh'
       end if

       id = index(buf, '&recohmodel')
       if (id /= 0) then
          new_name = 'recohmodel'
       end if

    end do

    close(u)

  end subroutine determine_analytical_prog


endmodule mod_md_utils
