! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_qm_general
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-08-27

  !! This module handles the part related to electronic structure
  !! computations in NX. In particular, is is meant as the general
  !! interface between NX and the electronic structure program, by
  !! deciding which QM software has to be called, and setup the
  !! programs accordingly.
  !!
  !! In particular, the QM-specific code should export functions for
  !! the followinfg tasks:
  !!
  !! - Setting up the computation, i.e. putting relevant information
  !!   in the ``nx_qm`` object.
  !! - Printing information about the current job.
  !! - Updating the QM input (new state, change in geometry, ...)
  !! - Running the QM computation.
  !! - Reading information from the computation (typically potential
  !!   energies, gradients, NAD) and putting those in arrays.
  !!
  !! All communication with the general trajectory of ``nx_traj_t``
  !! type is handled by this module only, to avoid making QM-specific
  !! code dependent on ``mod_configuration`` and ``mod_trajectory``
  !! modules.
  use mod_kinds, only: dp
  use mod_constants, only: MAX_STR_SIZE
  use mod_logger, only: &
       & nx_log, &
       & LOG_INFO, LOG_ERROR, LOG_DEBUG, &
       & call_external, check_error
  use mod_interface, only: mkdir, copy, nx_chmod
  use mod_tools, only: phase_adjust_h_vec, to_str
  use mod_qm_t, only: nx_qm_t
  use mod_qminfo, only: nx_qminfo_t
  use mod_configuration, only: nx_config_t
  use mod_trajectory, only: nx_traj_t
  use mod_turbomole, only: &
       & tm_setup, tm_print, tm_update_input, tm_run, tm_read, tm_backup
  use mod_columbus, only: &
       & col_setup, col_print, col_update_input, col_run,&
       & col_read_outputs, col_initial_phase, col_backup
  use mod_analytical, only: &
       & am_setup, am_print, am_read, am_run, am_update_input, am_diabatic_population
  use mod_tinker_g16_mmp, only: &
       & tg16mmp_setup, tg16mmp_print, tg16mmp_update_input, tg16mmp_run, &
       & tg16mmp_read, tg16mmp_set_qmmm_atoms
  use mod_tinker_mndo, only: &
       & tmndo_setup, tmndo_print, tmndo_update_input, tmndo_run, &
       & tmndo_read, tmndo_set_qmmm_atoms
  use mod_gaussian, only: &
       & gau_setup, gau_print, gau_update_input, gau_run, gau_read_dft, &
       & gau_parse_input, gau_generate_route, gau_backup
  use mod_exc_mopac, only: &
       & excmop_setup, excmop_update_input, excmop_run, excmop_read_outputs, excmop_backup
  use mod_exc_gaussian, only: &
       & excgau_setup, excgau_update_input, excgau_run, excgau_read_outputs
  use mod_mopac, only: &
       & mop_setup, mop_update_input, mop_run, mop_read_outputs, mop_backup
  use mod_external, only: &
       & external_setup, external_run, external_read_outputs
  use mod_mlatom, only: &
       & mlat_setup, mlat_print, mlat_run, mlat_read_outputs
  use mod_orca, only: &
       & orca_setup, orca_update_input, orca_read, orca_run, orca_print, orca_backup
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  public :: qm_setup, qm_print, qm_update_input
  public :: qm_read, qm_run, qm_backup
  public :: qm_do_qmmm, qm_set_qmmm_atoms
  public :: qm_extract_from_output

  integer, parameter :: ERR_QM = 501

contains

  subroutine qm_setup(nx_qm, parser, conf)
    !! Setup the electronic structure program.
    !!
    !! This subroutine is a wrapper around other ``setup``
    !! subroutines, and selects the right one depending on the value
    !! of ``qmcode``. ``configfile`` refers to the main NX
    !! configuration file with general QM parameters, while ``input``
    !! refers to optional information that should be read directly
    !! from QM input file (for instance, ``control`` in TURBOMOLE.
    type(nx_qm_t), intent(inout) :: nx_qm
    !! QM object.
    type(parser_t), intent(in) :: parser
    type(nx_config_t) :: conf
    !! General configuration.

    character(len=MAX_STR_SIZE) :: infile
    ! QM input file containing information for setting up QM.

    integer :: ierr

    ierr = nx_chmod(nx_qm%job_folder, '744')
    call check_error(ierr, 101, &
         & 'Cannot change permissions on '//nx_qm%job_folder, system=.true.)

    select case(nx_qm%qmcode)
    case ("turbomole")
       infile = trim(nx_qm%job_folder)//'/control'
       call tm_setup(nx_qm, parser, conf, infile)
    case ("columbus")
       ! infile = trim(nx_qm%job_folder)//'/control.run'
       call col_setup(nx_qm, parser)
    case ("gaussian")
       call gau_setup(nx_qm, parser)
    case("analytical")
       call am_setup(nx_qm, parser, conf)
    case ("tinker_g16mmp")
       call tg16mmp_setup(nx_qm, parser)
    case ("tinker_mndo")
       call tmndo_setup(nx_qm, parser)
    case ("exc_mopac")
       call excmop_setup(nx_qm, parser, conf)
    case ("exc_gaussian")
       call excgau_setup(nx_qm, conf, parser)
    case ("mopac")
       call mop_setup(nx_qm, conf, parser)
    case ("external")
       call external_setup(nx_qm, parser)
    case("mlatom")
       call mlat_setup(nx_qm, parser, conf)
    case("orca")
       call orca_setup(nx_qm, parser, conf, trim(nx_qm%job_folder)//'/orca.inp')
    end select

  end subroutine qm_setup


  subroutine qm_print(nx_qm, out)
    !! Print the current electronic structure configuration.
    type(nx_qm_t), intent(in) :: nx_qm
    !! Nx_Qm object.
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out

    write(output, '(A80)') repeat('*', 80)
    write(output, *) 'Electronic structure configuration: '
    write(output, *) ' '

    select case(nx_qm%qmcode)
    case ("turbomole")
       call tm_print(nx_qm, output)
    case("columbus")
       call col_print(nx_qm, output)
    case("gaussian")
       call gau_print(nx_qm, output)
    case("analytical")
       call am_print(nx_qm, output)
    case ("tinker_g16mmp")
       call tg16mmp_print(nx_qm, output)
    case ("tinker_mdno")
       call tmndo_print(nx_qm, output)
    case ("exc_mopac")
       write(output, *) 'Using EXASH with semiempirical FOMO-CI (MOPAC2002)'
    case ("exc_gaussian")
       write(output, *) 'Using EXASH with DFT (Gaussian)'
    case ("mopac")
       write(output, *) 'Using semiempirical FOMO-CI (MOPAC2002)'
    case ("external")
       write(output, *) 'Using external script:', nx_qm%ext_path
    case("mlatom")
       call mlat_print(nx_qm, output)
    case("orca")
       call orca_print(nx_qm, output)
    end select

    write(output, '(A80)') repeat('*', 80)
  end subroutine qm_print


  subroutine qm_update_input(nx_qm, traj, conf)
    !! Update the QM input.
    !!
    !! The update is carried out from information from the current
    !! trajectory (``nstatdyn``, ``geom``, etc.), and from the
    !! general configuration.
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    type(nx_traj_t), intent(in) :: traj
    !! Classical trajectory.
    type(nx_config_t), intent(in) :: conf
    !! General configuration.

    ! Check if the oscillator strength array is allocated
    if (.not. allocated(nx_qm% osc_str)) then
       allocate(nx_qm% osc_str(conf%nstat - 1))
    end if
    nx_qm%osc_str(:) = 0.0_dp

    call nx_log%log(LOG_DEBUG, 'QM: Starting updating QM inputs')

    call nx_log%log(LOG_DEBUG, 'QM: Writing coordinates to QM-specific format')
    call traj% write_coordinates( conf%progname )

    call nx_log%log(LOG_DEBUG, 'QM: Generating new QM input files')
    select case(nx_qm%qmcode)
    case ("turbomole")
       call tm_update_input(nx_qm, traj, conf)
    case ("gaussian")
       call gau_update_input(nx_qm, traj, conf)
    case("columbus")
       call col_update_input(nx_qm, traj, conf)
    case("analytical")
       call am_update_input(nx_qm, traj%geom, traj%veloc)
    case ("tinker_g16mmp")
       call tg16mmp_update_input(nx_qm, traj, conf)
    case ("tinker_mndo")
       call tmndo_update_input(nx_qm, traj, conf)
    case ("exc_mopac")
       call excmop_update_input(nx_qm, traj, conf)
    case ("exc_gaussian")
       call excgau_update_input(nx_qm, conf%nat, traj%step)
    case ("mopac")
       call mop_update_input(nx_qm, traj, conf)
    case("mlatom")
       call execute_command_line("rm -f yest_en*.dat yest_grad*.dat")
    case("orca")
       call orca_update_input(nx_qm, traj, conf)
    end select
    call nx_log%log(LOG_DEBUG, 'QM: Generating new QM input files done')
  end subroutine qm_update_input


  subroutine qm_run(nx_qm, conf, traj)
    !! Run the QM code.
    type(nx_qm_t) :: nx_qm
    !! Nx_Qm object.
    type(nx_config_t) :: conf
    !! General configuration.
    type(nx_traj_t) :: traj
    !! Classical trajectory.

    call nx_log%log(LOG_DEBUG, 'QM: Starting QM job')
    select case(nx_qm%qmcode)
    case ("turbomole")
       call tm_run(nx_qm)
    case ("gaussian")
       call gau_run()
    case("columbus")
       call col_run(nx_qm)
    case("analytical")
       call am_run(nx_qm)
    case ("tinker_g16mmp")
       call tg16mmp_run(nx_qm)
    case ("tinker_mndo")
       call tmndo_run(nx_qm)
    case ("exc_mopac")
       call excmop_run(nx_qm)
    case ("exc_gaussian")
       call excgau_run(nx_qm)
    case ("mopac")
       call mop_run()
    case ("external")
       call external_run(nx_qm, conf, traj)
    case("mlatom")
       call mlat_run(nx_qm, conf, traj)
    case("orca")
       call orca_run(nx_qm)
    end select
    call nx_log%log(LOG_DEBUG, 'QM: QM job terminated')
  end subroutine qm_run


  subroutine qm_read(nx_qm, traj, conf, getphase)
    !! Read output from the QM code into the trajectory.
    !!
    !! Nx_Qm routine handles the transfer of information from the
    !! electronic structure program to the classical trajectory.
    type(nx_qm_t) :: nx_qm
    !! Nx_Qm object.
    type(nx_traj_t) :: traj
    !! Classical trajectory.
    type(nx_config_t) :: conf
    !! General configuration.
    integer, optional, intent(in) :: getphase

    ! At most we will read and print 128 lines from the QM output.
    integer :: ierr, i
    logical :: update_nad, correct_phase
    logical :: ci_phase

    type(nx_qminfo_t) :: qminfo

    ci_phase = .false.
    if (present(getphase)) then
       if (getphase == 0) then
          ci_phase = .true.
       end if
    end if

    qminfo = nx_qminfo_t(conf%nstat, conf%nat, conf%dc_method, conf%run_complex)
    call qm_extract_from_output(nx_qm, traj, conf, qminfo)

    if (conf%dc_method == 1) then
       qminfo%update_nad = .true.       
    end if

    if (.not. nx_qm%request_adapt_dt) then
       if (conf%dc_method == 1) then
          if (qminfo%correct_phase) then
             call qm_correct_phase(nx_qm, traj%step, conf%init_step, conf%nstat, &
                  & qminfo%rnad, traj%nad, ci_phase)
          end if
       end if

       call qm_update_traj(conf, nx_qm, traj, qminfo)
    end if

    ! Report
    if (qminfo%dim_dataread > 0) then
       call nx_log%log(&
            & LOG_INFO, qminfo%dataread(2:qminfo%dim_dataread), title=qminfo%dataread(1))
    end if

    if (conf%compute_diabpop) then
       call am_diabatic_population(nx_qm, conf, traj)
    end if

    call qminfo%destroy()

  end subroutine qm_read


  subroutine qm_extract_from_output(nx_qm, traj, conf, qminfo)
    type(nx_qm_t), intent(inout) :: nx_qm
    !! Nx_Qm object.
    type(nx_traj_t), intent(in) :: traj
    !! Classical trajectory.
    type(nx_config_t), intent(in) :: conf
    !! General configuration.
    type(nx_qminfo_t), intent(inout) :: qminfo

    integer :: ierr
    
    select case(nx_qm%qmcode)
    case ("turbomole")
       call tm_read(nx_qm, conf%nat, traj%nstatdyn, qminfo, ierr)
    case ("gaussian")
       call gau_read_dft(nx_qm, traj, conf, qminfo)
    case ("columbus")
       ! At this point we have to use traj%nad, because the update
       ! has not been done yet.
       call col_read_outputs(nx_qm, traj, conf, qminfo)
    case("analytical")
       qminfo%update_nad = .true.
       qminfo%correct_phase = .false.
       call am_read(nx_qm, qminfo)
    case ("tinker_g16mmp")
       call tg16mmp_read(nx_qm, traj, conf, qminfo, ierr)
    case ("tinker_mndo")
       qminfo%update_nad = .true.
       call tmndo_read(nx_qm, conf, traj, qminfo, ierr)
    case("exc_mopac")
       call excmop_read_outputs(nx_qm, traj, conf, conf%nstat, qminfo)
    case("exc_gaussian")
       call excgau_read_outputs(nx_qm, traj, conf, conf%nstat, qminfo)
    case("mopac")
       call mop_read_outputs(nx_qm, traj, conf%nstat, conf%nat, qminfo)
    case ("external")
       call external_read_outputs(nx_qm, traj, conf, qminfo)
    case("mlatom")
       call mlat_read_outputs(nx_qm, traj, conf, qminfo)
    case("orca")
       call orca_read(nx_qm, traj, conf, qminfo, ierr)
    end select
  end subroutine qm_extract_from_output
  

  subroutine qm_backup(nx_qm, conf, traj)
    type(nx_qm_t), intent(in) :: nx_qm
    type(nx_config_t), intent(in) :: conf
    type(nx_traj_t), intent(in) :: traj

    character(len=:), allocatable :: dbg_dir
    logical :: ext

    if (mod(traj%step, conf%save_cwd) == 0) then
       if (traj%is_virtual) then
          dbg_dir = 'debug/'//trim(nx_qm%qmcode)//'.'//to_str(traj%step)
       else
          dbg_dir = trim(nx_qm%debug_path)//'/'//trim(nx_qm%qmcode)//'.'//to_str(traj%step)
       end if

       inquire(file=dbg_dir, exist=ext)
       ! If the directory already exists, it means that we are at the second run, so update
       ! the name accordingly (for instance, a surface hopping occurs)
       if (ext) then
          dbg_dir = dbg_dir//'.2'
       end if

       select case(nx_qm%qmcode)
       case("columbus")
          call col_backup(nx_qm, conf, traj, dbg_dir)
       case("turbomole")
          call tm_backup(conf, traj, dbg_dir)
       case("gaussian")
          call gau_backup(nx_qm, conf, traj, dbg_dir)
       case("mopac")
          call mop_backup(conf, traj, dbg_dir)
       case("exc_mopac")
          call excmop_backup(nx_qm, conf, traj, dbg_dir)
       case("orca")
          call orca_backup(nx_qm, conf, traj, dbg_dir)
       end select
    end if
  end subroutine qm_backup


  subroutine qm_correct_phase(qm, step, init_step, nstat, rnad, old_nad, ci_phase)
    type(nx_qm_t), intent(in) :: qm
    integer, intent(in) :: nstat
    integer, intent(in) :: step
    integer, intent(in) :: init_step
    real(dp), intent(inout) :: rnad(:, :, :)
    real(dp), intent(in) :: old_nad(:, :, :)
    logical, optional, intent(in) :: ci_phase

    integer, allocatable :: phase(:)
    real(dp), allocatable :: cossine(:)

    integer :: i

    if (step == init_step) then
       select case(qm%qmcode)
       case ("columbus")
          if (ci_phase) then
             call col_initial_phase(rnad, nstat)
          end if
       end select
    end if

    allocate(phase(size(rnad, 1)))
    allocate(cossine(size(rnad, 1)))
    call phase_adjust_h_vec(old_nad, rnad, phase, cossine)
    if (qm%is_test .and. step == init_step) then
       do i=1, size(rnad, 1)
          if (rnad(i, 1, 1) < 0) then
             rnad(i, :, :) = -1.0_dp * rnad(i, :, :)
          end if
       end do
    end if
    ! print *, 'GLOBAL PHASE: ', phase
    deallocate(phase)
    deallocate(cossine)
  end subroutine qm_correct_phase



  subroutine qm_update_traj(config, nx_qm, traj, qminfo)
    !! Update the trajectory with the content read from output.
    !!
    type(nx_config_t), intent(in) :: config
    type(nx_qm_t), intent(in) :: nx_qm
    !! Information about electronic structure computation.
    type(nx_traj_t), intent(inout) :: traj
    !! Classical trajectory.
    type(nx_qminfo_t), intent(in) :: qminfo

    integer :: i

    ! If surface hopping occurs, we do not need to read the energies
    ! again: we are only interested in the gradients !!
    if (nx_qm%update_epot == 1) then
       traj%old_epot(3, :) = traj%old_epot(2, :)
       traj%old_epot(2, :) = traj%old_epot(1, :)
       traj%old_epot(1, :) = traj%epot
       traj%epot(:) = qminfo%repot(:)

       if (traj%run_complex) then
          ! The other cases are already dealt with in `mod_csfssh`, in routine
          ! `csfssh_compute_gamma`.
          if (config%gamma_model == 0) then
             traj%old_gamma(3, :) = traj%old_gamma(2, :)
             traj%old_gamma(2, :) = traj%old_gamma(1, :)
             traj%old_gamma(1, :) = traj%gamma
             traj%gamma(:) = qminfo%rgamma(:)
          end if
       end if
    end if

    traj%old_acc(:, :) = traj%acc(:, :)
    traj%old_grad(:, :, :) = traj%grad(:, :, :)

    traj%grad(:, :, :) = qminfo%rgrad(:, :, :)

    do i=1, size(traj%acc, 2)
       traj%acc(:, i) = traj%grad(traj%nstatdyn, :, i)
       traj%acc(:, i) = (-1.0_dp / traj%masses(i)) * traj%acc(:, i)
    end do

    if (qminfo%update_nad) then
       traj%old_nad = traj%nad
       traj%nad(:, :, :) = qminfo%rnad(:, :, :)

       if (traj%run_complex) then
          traj%old_nad_i = traj%nad_i
          traj%nad_i(:, :, :) = qminfo%rnad_i(:, :, :)
       end if
    end if

    if (size(qminfo%repot) > 1) then
       traj%osc(:) = nx_qm%osc_str(:)
    end if

    if (config%progname == "exc_mopac" .or. &
        & config%progname == "exc_gaussian") then
       traj%diaham(:, :) = nx_qm%diaham(:, :)
       traj%diapop(:) = nx_qm%diapop(:)
       traj%diaen(:) = nx_qm%diaen(:)
    end if

    call traj%print_epot_all()
  end subroutine qm_update_traj


  function qm_do_qmmm(nx_qm)
    !! Decide if QM/MM treatment is required
    !!
    type(nx_qm_t), intent(in) :: nx_qm

    logical :: qm_do_qmmm

    select case(nx_qm%qmcode)
      case("tinker_g16mmp", "tinker_mndo")
        qm_do_qmmm = .true.
      case default
        qm_do_qmmm = .false.
    end select

    return

  end function qm_do_qmmm

  subroutine qm_set_qmmm_atoms(nx_qm, traj)
    !! Update the trajectory filling the array is_qm_atom
    !! for QM/MM software
    !!
    type(nx_qm_t), intent(in) :: nx_qm
    !! Information about electronic structure computation.
    type(nx_traj_t), intent(inout) :: traj
    !! Classical trajectory.
    integer :: i, nqm

    select case(nx_qm%qmcode)
      case("tinker_g16mmp")
        call tg16mmp_set_qmmm_atoms(nx_qm, traj%is_qm_atom, size(traj%masses))
      case("tinker_mndo")
        call tmndo_set_qmmm_atoms(nx_qm, traj%is_qm_atom, size(traj%masses))
    end select

    nqm = 0
    do i=1, size(traj%masses)
      if(traj%is_qm_atom(i)) then
        nqm = nqm + 1
      end if
    end do

    traj%nqm_atoms = nqm
    traj%is_qmmm = .not. (nqm == size(traj%masses))

  end subroutine qm_set_qmmm_atoms


  ! QMINFO TYPE


end module mod_qm_general
