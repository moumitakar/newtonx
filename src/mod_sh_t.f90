! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_sh_t
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-03-27
  !!
  !! This module defines the ``nx_sh_t`` type, which holds the
  !! general configuration of the surface hopping procedure.
  use mod_kinds, only: dp
  use mod_input_parser, only: &
       & parser_t, &
       & set => set_config
  use mod_logger, only: &
       & nx_log, LOG_ERROR, &
       & print_conf_ele
  use iso_fortran_env, only: stdout=>output_unit
  implicit none

  private

  public :: nx_sh_t

  type nx_sh_t
     !! Object storing information about the surface hopping
     !! routines, such as the integrator type to be used, or the
     !! number of microiterations and the time-step.
     integer :: vdoth = -1
     !! Indicate the content of the ``nad`` object from ``traj``:
     !!
     !! - 0 - ``nad`` contains non-adiabatic coupling vectors.
     !! - 1 - ``nad`` contains time-derivatives.
     integer :: seed = -1
     !! Random number generation
     !!
     !! - -2 - (FOR TESTS ONLY) Use a home-made PRNG
     !! - -1 - A randomized seed is used
     !! - 0 - A default random-number seed is used
     !! - n - Use this number as a random seed
     integer :: integrator = 1
     !! Integrator type for the time-dependent Schrödinger equation
     !!
     !! - 0 - Finite element method
     !! - 1 - Butcher, 5th order
     !! - 2 - Unitary propagators for local-diabatization method
     !! - 3 - Unitary propagators (Experimental)
     integer :: phase = 1
     !! Integrate the phase along the trajectory (only for debug purposes)
     !! - 0 - No, phase is always 0
     !! - 1 - Yes
     integer :: nohop = 0
     !! Force the hopping at a certain time step
     !!
     !! - 0 - Normal surface hopping
     !! - -1 - Hopping is not allowed at any time
     !! - n - Hopping is forced at (and only at) step ``n`` (``n`` = positive integer)
     integer :: forcesurf = 1
     !! Force the hopping to this surface (GS is 1) if ``nohop`` is set.
     integer :: ms = 20
     !! Number of sub-timesteps for integration of the time-dependent Schrödinger
     !! equation. For local-diabatization (integrator = 2), this value will be used
     !! as a dummy value (the number of integration step is determined automatically).
     integer :: getphase = 1
     !! Phase to use
     !!
     !! - 0 - use phase provided by the overlap of CI vectors (NOT IMPLEMENTED).
     !! - 1 - use phase provided by the scalar product between h(t) and h(t-dt).
     integer :: nrelax = 0
     !! Number of substeps after hopping, in which other hopping is forbiden.
     integer :: tully = 1
     !! Fewest-switch algorithm
     !!
     !! - 0 - Tully
     !! - 1 - Hammes-Schiffer and Tully
     real(dp) :: probmin = 0.0_dp
     !! Do not hop if probability is smaller than ``probmin``.
     integer :: mom = 1
     !! What to do after a frustrated hopping
     !!
     !! - -1 - Invert momentum direction
     !! - 1 - Keep momentum direction
     integer :: adjmom
     !! Condition for accepting a hop, and direction of velocity rescaling (see manual
     !! for more details).
     !!
     !! - 0 - (Large systems, NAC not available) Rescale in the direction of velocity,
     !!   and scale kinetic enrgy in the hopping condition with the number of degrees of freedom.
     !! - 1 - (NAC not available) Rescale in the direction of velocity, and don't
     !!   rescale the kinetic energy in the hopping condition.
     !! - 2 - (NAC available) Rescale in a direction given by the angle ``adjtheta``
     !!   in the plane \((h, g)\).
     real(dp) :: adjtheta = 0.0_dp
     !! Direction (in degrees) in which the velocity is rescaled after a hopping (for
     !! ``adjmom = 2`` only).
     real(dp) :: popdev = 0.05_dp
     !! Kill trajectory if total adiabatic population deviate more than ``popdev`` from
     !! the unity.
     real(dp) :: dts

     ! Decoherence correction
     integer :: decohmod = 1
     !! Decoherence correction model to use.
     !!
     !! - 0 - No decoherence correction
     !! - 1 - EDC energy based decoherence (Granucci and Persico, /JCP/, ^126^ (2007))
     !! - 2 - ODC Overlap based decoherence (Granucci, Persico and Zoccante, /JCP/, ^133^
     !! (2010))
     real(dp) :: decay = 0.1_dp
     !! (EDC model) Decay time.
     real(dp) :: decovlp = 0.05_dp
     !! (ODC model) Gaussian width
     real(dp) :: thrwp = 0.005_dp
     !! (ODC model) Wavefunction overlap threshold ($S_{min}$)
     integer :: iatau = 1
     !! (ODC model) Propagate ancillary gaussian wavepacket every =iatau= steps.

   contains
     procedure :: init
     procedure :: print

  end type nx_sh_t

contains

  subroutine init(this, parser, dc_couplings)
    !! Constructor for ``nx_sh_t`` object.
    !!
    !! The configuration is constructed based on the ``sh`` namelist
    !! that should be present in ``filename``.
    class(nx_sh_t), intent(inout) :: this
    !! This object.
    type(parser_t), intent(in) :: parser
    integer, intent(in) :: dc_couplings

    this%vdoth = 0
    this%adjmom = 2
    if (dc_couplings /= 1) then
       this%vdoth = 1
       this%adjmom = 1
    end if
    
    call set(parser, 'sh', this%seed, 'seed')
    call set(parser, 'sh', this%integrator, 'integrator')
    call set(parser, 'sh', this%phase, 'phase')
    call set(parser, 'sh', this%nohop, 'nohop')
    call set(parser, 'sh', this%forcesurf, 'forcesurf')
    call set(parser, 'sh', this%ms, 'ms')
    call set(parser, 'sh', this%getphase, 'getphase')
    call set(parser, 'sh', this%nrelax, 'nrelax')
    call set(parser, 'sh', this%tully, 'tully')
    call set(parser, 'sh', this%decay, 'decay')
    call set(parser, 'sh', this%probmin, 'probmin')
    call set(parser, 'sh', this%mom, 'mom')
    call set(parser, 'sh', this%adjmom, 'adjmom')
    call set(parser, 'sh', this%adjtheta, 'adjtheta')
    call set(parser, 'sh', this%popdev, 'popdev')
    call set(parser, 'sh', this%decohmod, 'decohmod')
    call set(parser, 'sh', this%decovlp, 'decovlp')
    call set(parser, 'sh', this%thrwp, 'thrwp')
    call set(parser, 'sh', this%iatau, 'iatau')
    
    this%dts = 0
    ! This will be initialized LATER (in mod_sh.f08) !! dts / timeunit

    if (this%nohop > 0) then
       this%nohop = this%nohop * this%ms
    end if

    if (this%integrator == 2) this%ms = 1
  end subroutine init

  subroutine print(self, out)
    class(nx_sh_t), intent(in) :: self
    integer, intent(in), optional :: out

    integer :: output

    output = stdout
    if (present(out)) output = out
    
    write(output, '(A80)') repeat('*', 80)
    write(output, *) 'SURFACE HOPPING CONFIGURATION'
    write(output, *) ''

    call print_conf_ele(self%adjmom, 'adjmom', unit=output)
    call print_conf_ele(self%adjtheta, 'adjtheta', unit=output)
    call print_conf_ele(self%decay, 'decay', unit=output)
    call print_conf_ele(self%decohmod, 'decohmod', unit=output)
    call print_conf_ele(self%decovlp, 'decovlp', unit=output)
    call print_conf_ele(self%forcesurf, 'forcesurf', unit=output)
    call print_conf_ele(self%getphase, 'getphase', unit=output)
    call print_conf_ele(self%iatau, 'iatau', unit=output)
    call print_conf_ele(self%integrator, 'integrator', unit=output)
    call print_conf_ele(self%mom, 'mom', unit=output)
    call print_conf_ele(self%ms, 'ms', unit=output)
    call print_conf_ele(self%nohop, 'nohop', unit=output)
    call print_conf_ele(self%nrelax, 'nrelax', unit=output)
    call print_conf_ele(self%phase, 'phase', unit=output)
    call print_conf_ele(self%popdev, 'popdev', unit=output)
    call print_conf_ele(self%probmin, 'probmin', unit=output)
    call print_conf_ele(self%seed, 'seed', unit=output)
    call print_conf_ele(self%thrwp, 'thrwp', unit=output)
    call print_conf_ele(self%tully, 'tully', unit=output)
    call print_conf_ele(self%vdoth, 'vdoth', unit=output)

    write(output, '(A80)') repeat('*', 80)
    write(output, *) ' '
  end subroutine print
  

end module mod_sh_t


