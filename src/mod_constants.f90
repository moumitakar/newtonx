! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_constants
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-03-27
  !!
  !! Module for storing constants used across the program.
  !!
  !! The constants stored can be of several type:
  !!
  !! - conversion factors ;
  !! - general status of the trajectory, error codes ;
  !! - specification for atoms ;
  !! - common size parameters.
  use mod_kinds, only: dp
  implicit none

  private
  public :: pi, BK, proton, timeunit, au2ev, au2kcalm, au2ang,&
       & deg2rad, au2cm, cm2au, h_planck, c_speed,&
       & light, bk_ev, h_evs, e_charge, e_mass,&
       & eps0, cm2K, eV2nm, fstoau, au2debye, &
       & hbar

  public :: MAX_CMD_SIZE, MAX_STR_SIZE

  public :: convert_status_to_str

  ! Mathematical constants
  real(dp), parameter :: pi = 4*atan(1.0_dp)

  ! DOUBLE PRECISION CONSTANTS AND CONVERSION COEFFICIENTS:
  real(dp), parameter :: BK       = 0.3166813639E-5_dp
  !! Botzmann constant Hartree/kelvin
  real(dp), parameter :: proton   = 1822.888515_dp
  !! (a.m.u.)/(electron mass).
  real(dp), parameter :: timeunit = 24.188843265E-3_dp
  !! a.u. to femtoseconds conversion.
  real(dp), parameter :: fstoau   = 41.34137221718_dp
  !! Femtoseconds to a.u. conversion.
  real(dp), parameter :: au2ev    = 27.21138386_dp
  !! a.u. to eV conversion.
  real(dp), parameter :: au2kcalm = 627.5094740631_dp
  !! a.u. to kcal/mol conversion.
  real(dp), parameter :: au2ang   = 0.52917720859_dp
  !! a.u. to angstrom conversion.
  real(dp), parameter :: deg2rad  = 1.745329252E-2_dp
  !! degree to radian (\(\pi\)/180) conversion.
  real(dp), parameter :: au2cm    = 219474.625_dp
  !! hartree to cm\(^{-1}\) conversion.
  real(dp), parameter :: cm2au    = 4.55633539E-6_dp
  !! cm\(^{-1}\) to hartree conversion.
  real(dp), parameter :: h_planck = 1.5198298508E-16_dp
  !! Planck constant in hartree.sec.
  real(dp), parameter :: c_speed  = 2.99792458E-5_dp
  !! speed of light (cm/fs)
  real(dp), parameter :: au2debye  = 2.541747760_dp

  ! Further constants used in photoabsorption and importance sampling programs
  real(dp), parameter :: light    = 299792458_dp
  !! speed of light m/s
  real(dp), parameter :: bk_ev    = 8.617343E-5_dp
  !! Boltzmann constant eV/kelvin
  real(dp), parameter :: h_evs    = 4.13566733E-15_dp
  !! Planck constant eV.s
  real(dp), parameter :: e_charge = -1.602176487E-19_dp
  !! electron charge C
  real(dp), parameter :: e_mass   = 9.10938215E-31_dp
  !! electron mass kg
  real(dp), parameter :: eps0     = 8.854187817e-12_dp
  !! vacuum permitivity \(C^2*s^2*kg^{-1}*m^{-3}\)
  real(dp), parameter :: cm2K     = 1.438790955_dp
  !! cm-1 to Kelvin conversion.
  real(dp), parameter :: eV2nm    = 1239.84193_dp
  !! eV to nm conversion.

  real(dp), parameter :: hbar = 1.0_dp

  ! CONSTANTS USED IN THE CODE
  integer, parameter :: MAX_STR_SIZE = 1024 ! String-size limit
  integer, parameter :: MAX_CMD_SIZE = 1024 ! Maximum length for command line

  ! ERROR CHECKS
  integer, parameter, public :: ERR_ETOT_JUMP = 90
  integer, parameter, public :: ERR_ETOT_DRIFT = 91

  ! TRAJECTORY STATUS
  integer, parameter, public :: TRAJ_NORMAL_TERM = 1
  integer, parameter, public :: TRAJ_KILLED_EDRIFT = 2
  integer, parameter, public :: TRAJ_KILLED_ETOT0 = 3
  integer, parameter, public :: TRAJ_KILLED_KILLSTAT = 4
  integer, parameter, public :: TRAJ_KILLED_EDIFF = 5

  character(len=2), parameter, public :: ATOM_NAMES(94) = [&
       & "H ", "He", "Li", "Be", "B ", "C ", "N ", "O ", "F ", "Ne", &
       & "Na", "Mg", "Al", "Si", "P ", "S ", "Cl", "Ar", "K ", "Ca", &
       & "Sc", "Ti", "V ", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", &
       & "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y ", "Zr", &
       & "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", &
       & "Sb", "Te", "I ", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd", &
       & "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", &
       & "Lu", "Hf", "Ta", "W ", "Re", "Os", "Ir", "Pt", "Au", "Hg", &
       & "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th", &
       & "Pa", "U ", "Np", "Pu" &
  &]


contains

  pure function convert_status_to_str(status) result(msg)
    !! Convert the status code to a message.
    !!
    !! This function is used to write the termination status of the trajectory in the
    !! H5MD file at the end of the dynamics.
    integer, intent(in) :: status
    !! Status code.
    character(len=:), allocatable :: msg

    select case(status)
    case(TRAJ_NORMAL_TERM)
       msg = "FINISHED"
    case(TRAJ_KILLED_EDRIFT)
       msg = "KILLED (EDRIFT)"
    case(TRAJ_KILLED_ETOT0)
       msg = "KILLED (ETOT0)"
    case(TRAJ_KILLED_KILLSTAT)
       msg = "KILLED (KILLSTAT)"
    case(TRAJ_KILLED_EDIFF)
       msg = "KILLED (EDIFF)"
    end select

  end function convert_status_to_str


end module mod_constants
