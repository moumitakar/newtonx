! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_adaptive_step
  ! original author: Max Pinheiro Jr <maxjr82@gmail.com>
  ! date: 2020-04-07
  ! documentation update: BD - 2022-04-01
  !!
  !! # Adaptive time-step implementation
  !!
  !! This module implements the possibility to restart the current
  !! step with a reduced time-step.  This is useful in case of
  !! orbital rotation for instance.
  !!
  !! ## Implementation
  !!
  !! If required, the module first checks the current number of
  !! subtrajectories started, and the adapted ``dt * ratio``. If the
  !! number of subtrajectories is higher than user-defined
  !! ``max_subtraj``, or if ``dt * ratio`` is lower than hardcoded
  !! ``LOWEST_DT`` (set to ``1e-4``), then the trajectory aborts.
  !!
  !! Else, a new folder ``subtraj_N_T_S``, where:
  !!
  !! - ``N`` is the subtrajectory index ;
  !! - ``T`` is the time at which the subtrajectory is started ;
  !! - ``S`` is the step at which the subtrajectory is started,
  !!
  !! is created, and the content of ``origin`` (apart from ``debug``
  !! and other ``subtraj`` folders) is copied there.  No other
  !! operation is performed (the calling program / module has to
  !! re-roll the trajectory and change directory to ``subtraj_``).
  !!
  !! It is recommended that ``origin`` is set to the main dynamics
  !! directory, so that all subtrajectories have the same starting
  !! point.
  !!
  !! At the end of each step, we can use the ``adt_increase_dt``
  !! routine to check if ``dt`` should be modified. Let's assume the
  !! virtual trajectory was started at ``T0``, with original ``DT``
  !! so that ``T0 + DT = T1``. Let ``dt`` be the current virtual
  !! time-step, and ``t`` the current virtual time. Now we have
  !! several cases:
  !!
  !! - if ``t == T1`` , we copy the content of
  !! the current folder to ``origin``: the virtual trajectory is
  !! over, and we are ready to go back to the main one.
  !! - if ``t < T1``, two things can happen:
  !!
  !!    1. ``t + dt > T1``: We do one more step in the virtual
  !!    trajectory with ``dt = T1 - t``;
  !!    2. ``t + dt <= T1``: We are still in the subtrajectory for the
  !!    next time-step.
  !!
  use mod_kinds, only: dp
  use mod_interface, only: &
       & mkdir, get_list_of_files, copy
  use mod_logger, only: &
       & nx_log, &
       & LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_ERROR, &
       & call_external, check_error
  use mod_trajectory, only: nx_traj_t
  use mod_overlap, only: nx_moovlp_t
  use mod_constants, only: timeunit, MAX_CMD_SIZE
  use mod_tools, only: &
       & to_str
  use mod_qm_t, only: nx_qm_t
  use mod_input_parser, only: &
       & parser_t, set => set_config

  implicit none

  private

  public :: nx_adaptive_ts_t

  real(dp), parameter :: LOWEST_DT = 1e-4

  type nx_adaptive_ts_t
     logical :: call_adapt_dt = .false.
     !! This variable will be true if the ``adapt_dt``
     !! namelist block is found in the conf. input.
     integer      :: model = 0
     !! Models for the ratio on decreasing the time step:
     !!
     !! - 0: a constant value
     !! - 1: dynamical value given by the F-norm of MO overlap
     integer      :: nsubtraj = 0
     !! Current number of subtrajectories for adaptive steps.
     integer      :: max_subtraj = 4
     !! Maximum number of subtrajectories for adaptive steps.
     real(dp) :: tswitch = 0.0_dp
     !! Time at which adaptive time step is called.
     integer :: sswitch = 0
     !! Step at which adaptive time step is called.
     real(dp) :: dt_init = 0.0_dp
     !! Store the original time step defined by the user.
     real(dp) :: dt = 0.0_dp
     !! New time step.
     real(dp) :: ratio = 0.5_dp
     !! Controls the ratio for decreasing the time step (0.1).
     !! Can be a constant or step dependent value (F-norm).
     character(len=:), allocatable :: origin
     !! Original folder where the adaptive scheme is called (usually
     !! the ``TEMP`` directory).
     character(len=:), allocatable :: new_folder
     !! Folder in which the current subtrajectory is computed.
   contains
     procedure :: init
     procedure :: print
     procedure :: create => adt_create_virt_traj
     procedure :: increase_dt => adt_increase_dt
  end type nx_adaptive_ts_t


  character(len=10), parameter :: exclude_from_copy(11) = [&
       & 'curr_iter ', 'runc.error', 'runls     ', &
       & 'COSMO     ', 'GRADIENTS ', 'LISTINGS  ', &
       & 'MOLDEN    ', 'RESTART   ', &
       & 'WORK      ', 'debug     ', 'subtraj   '  &
       & ]

  integer, parameter :: ADT_ERROR = 10

contains

  subroutine init(this, parser)
    !! Initialize the configuration with the content of ``filename``.
    !! The file ``filename`` should contain the namelist ``adapt_dt``.
    class(nx_adaptive_ts_t), intent(inout) :: this
    !! This object.
    type(parser_t), intent(in) :: parser
    ! character(len=*), intent(in) :: filename
    !! File containing the configuration to load

    ! integer :: u
    ! integer :: model
    ! integer :: max_subtraj
    ! real(dp) :: ratio
    ! 
    ! namelist /adapt_dt/ model, max_subtraj, ratio
    ! 
    ! open(newunit=u, file=filename, status='old', action='read')
    ! read(u, nml=adapt_dt)
    ! close(u)
    ! 
    ! this%model = model
    ! this%ratio = ratio
    ! this%max_subtraj = max_subtraj
    ! this%nsubtraj = 0
    ! this%tswitch = 0.0_dp
    ! this%sswitch = 0
    ! this%dt_init = 0.0_dp
    call set(parser, 'adapt_dt', this%model, 'model')
    call set(parser, 'adapt_dt', this%ratio, 'ratio')
    call set(parser, 'adapt_dt', this%max_subtraj, 'max_subtraj')
    
    this%origin = ''
    this%new_folder = ''
  end subroutine init


  subroutine print(this)
    class(nx_adaptive_ts_t), intent(in) :: this

    write(*, *) ' '
    write(*, '(A80)') repeat('*', 80)

    write(*, *) 'Adaptive time step parameters:'
    write(*, *) ''

    write(*, '(A20, I10)') 'Model used:', this%model
    write(*, '(A20, I10)') 'Max number of subtraj:', this%max_subtraj
    write(*, '(A20, F10.3)') 'Ratio to decrease dt:', this%ratio

    write(*, '(A80)') repeat('*', 80)
    write(*, *) ''

  end subroutine print


  subroutine adt_create_virt_traj(self, traj, qm)
    class(nx_adaptive_ts_t), intent(inout) :: self
    type(nx_traj_t), intent(in) :: traj
    type(nx_qm_t), intent(in) :: qm

    character(len=MAX_CMD_SIZE) :: msg, origin, new_folder
    real(dp) :: t_cor, new_dt
    integer :: ierr
    character(len=256), allocatable :: filelist(:)

    self%nsubtraj = self%nsubtraj + 1

    ! Check if number of trajectories stays manageable
    if (self%nsubtraj > self%max_subtraj) then
       write(msg, '(A,I0,A,I0,A)') &
            & 'ADT: Too many subtrajectories (max is ', self%max_subtraj, &
            & ', current requested is number ', self%nsubtraj, ')'
       call nx_log%log(LOG_ERROR, msg)
       error stop
    end if

    ! Check if the time step is not too low
    new_dt = traj%dt * self%ratio
    if (new_dt < LOWEST_DT) then
       write(msg, '(A,F8.3,A,F8.3,A)') &
            & 'ADT: New time step is too low: ', new_dt * timeunit, &
            & ' fs (limit is ', LOWEST_DT, ' fs)'
       call nx_log%log(LOG_ERROR, msg)
       ERROR STOP
    end if
    self%dt = new_dt

    if (self%nsubtraj == 1) then
       t_cor = traj%t - traj%dt * timeunit
       self%tswitch = t_cor
       self%sswitch = traj%step - 1
       write(msg, '(A,F10.3,A,I0,A)') &
            & 'ADT: Starting at t = ', t_cor, ' fs (step ', &
            & self%sswitch, ')'
       call nx_log%log(LOG_WARN, msg)
       self%dt_init = traj%dt
    end if

    new_folder = 'subtraj_'//to_str(self%nsubtraj)//'_'//to_str(t_cor, fmt='(F10.3)') &
         & //'_'//to_str(self%sswitch)

    call nx_log%log(LOG_INFO, 'ADT: Will now run in '//new_folder)
    self%new_folder = trim(new_folder)

    write(msg, '(A,F10.3,A)') 'ADT: New DT is ', self%dt*timeunit, ' fs'
    call nx_log%log(LOG_INFO, msg)

    ierr = getcwd(origin)
    self%origin = trim(origin)

    ierr = mkdir( self%new_folder )
    call check_error(ierr, ADT_ERROR,&
         &  "ADT: Couldn't create directory", system=.true.)

    call get_list_of_files('./', filelist, ierr, &
         & exclude=exclude_from_copy, &
         & full_path=.true.)
    call check_error(ierr, 10, &
         & 'ADT: Cannot read current directory !', system=.true.)

    print *, "NEW_FOLDER: ", self%new_folder
    ierr = copy(filelist, self%new_folder, cpflags='-r')
    call check_error(ierr, 10, &
         & 'ADT: Cannot copy content of directory into subtraj !', system=.true.)

    ! Now some QM specific stuff
    if (qm%qmcode == 'columbus') then
       if (qm%col_mocoef == 1) then
          ierr = copy(self%new_folder//'/mocoef.old', self%new_folder//'/mocoef')
          call check_error(ierr, 10, &
               & 'ADT: Cannot copy content of directory into subtraj !', system=.true.)
       end if
    end if
  end subroutine adt_create_virt_traj


  subroutine adt_increase_dt(self, traj)
    class(nx_adaptive_ts_t), intent(inout) :: self
    type(nx_traj_t), intent(inout) :: traj

    character(len=MAX_CMD_SIZE) :: msg
    integer :: ierr
    character(len=256), allocatable :: filelist(:)

    real(dp) :: orig_next_t, virt_next_t

    virt_next_t = traj%t + traj%dt * timeunit
    orig_next_t = self%tswitch + self%dt_init * timeunit

    if (abs(traj%t - orig_next_t) < 1E-6) then
       ! We reached the switching time, so we go back to the main
       ! trajectory.
       call nx_log%log(LOG_WARN, 'ADT: Going back to main time line&
            & !')
       write(msg, '(A,F10.3,A)') 'ADT: Current time = ', traj%t, ' fs'
       call nx_log%log(LOG_INFO, msg)
       self%nsubtraj = 0
       traj%dt = self%dt_init
       traj%is_virtual = .false.
       traj%step = self%sswitch + 1

       call get_list_of_files('./', filelist, ierr, &
            & exclude=['debug/'], &
            & full_path=.true.)
       call check_error(ierr, 10, 'ADT: Cannot read current directory !')

       ierr = copy(filelist, '../', cpflags='-r')
       call check_error(ierr, 10, 'ADT: Cannot copy content of directory into subtraj !')

       ! call get_environment_variable("NXHOME", env)
       ! env = trim(env)//'/utils/'
       ! write(cmd, *) &
       !      & trim(env)//'/adapt_dt_copy.pl out'
       ! call call_external(cmd, ierr)
       ! if (ierr /= 0) then
       !    call nx_log%log(LOG_ERROR, 'ADT: Problem during copy of files')
       !    error stop
       ! end if

       ierr = chdir(self%origin)
       if ( ierr /= 0 ) then
          call nx_log%log(LOG_ERROR, 'ADT: Cannot go back in directory.')
          ERROR STOP
       end if

    else if (traj%t < orig_next_t) then
       ! We are at a time before the original switch.

       if (virt_next_t > orig_next_t) then
          ! If the next computed time step is after the original
          ! time, then we have to adapt dt once more.
          traj%dt = self%dt_init - self%dt
          write(msg, '(A,F10.3,A)') &
               & 'ADT: Changing time step to ', traj%dt * timeunit, ' fs'
          call nx_log%log(LOG_INFO, msg)

       else
          ! We can continue !
          call nx_log%log(LOG_WARN, &
               & 'ADT: Still in subtrajectory for the next step !')
       end if
    end if
  end subroutine adt_increase_dt

  ! OLD ROUTINES FROM adaptive_step_utils
  ! DO NOT REMOVE !!!! MIGHT BE USEFUL FOR LATER IMPROVEMENTS.
  ! subroutine adt_prepare_virtual_traj(traj, conf, adapt_dt,&
  !      & backup_traj, adt_dir)
  !   !! Prepare a new virtual trajectory.
  !   !!
  !   !! This routine is called whenever a new virtual trajectory is
  !   !! needed, and handles the backup of the current trajectory
  !   !! ``traj`` into the corresponding element of  ``backup_traj``,
  !   !! and the restoration of the ``traj`` parameters from the
  !   !! previous time-step.
  !   !!
  !   !! It also uses the ``adapt_ts.pl`` script to create a new
  !   !! directory where to run the trajectory, and copy the relevant
  !   !! files to this new directory.
  !   type(nx_traj_t), intent(inout) :: traj
  !   !! Current trajectory.
  !   type(nx_config_t), intent(in) :: conf
  !   !! Main configuration.
  !   type(nx_adaptive_ts_t), intent(inout) :: adapt_dt
  !   !! Parameters for the adaptive time step scheme.
  !   type(nx_traj_t), dimension(:), intent(out) :: backup_traj
  !   !! Array containing the backups of the trajectory object.
  !   character(len=*), intent(out) :: adt_dir
  !   !! Name of the directory where we will run the virtual trajectory
  !
  !   character(len=MAX_COMMAND_SIZE) :: env, cmd
  !   character(len=MAX_CMD_SIZE) :: msg
  !   character(len=12) :: cur_time
  !   integer :: ierr
  !   real(dp) :: t_cor
  !   ! This variable holds the time at which the adaptive algorithm is
  !   ! started, as the current traj%t is actually one time step
  !   ! further !
  !
  !   adapt_dt%nsubtraj = adapt_dt%nsubtraj + 1
  !   t_cor = traj%t - traj%dt * timeunit
  !
  !   ierr = 0
  !   ! If the we have too many subtrajectories, kill the computation
  !   if (adapt_dt%nsubtraj > adapt_dt%max_subtraj) then
  !      ierr = 1
  !      ! write(warn, *) 'ERROR: Too many subtrajectories required !'
  !      ! write(warn, '(A, F8.3, A, I0, A)') &
  !      !      & '(t = ', traj%t, ', Ntraj = ', adapt_dt%nsubtraj, ')'
  !      ! STOP warn
  !   end if
  !
  !   call check_error(ierr, ADT_ERROR_NTRAJ, 'Too many subtrajectories')
  !
  !   ! Tell if it is the first time we enter adaptive time step, to
  !   ! ease filtering out the output.
  !   if (adapt_dt%nsubtraj == 1) then
  !      ! First subtrajectory
  !      write(msg, '(A, F10.3, A)') &
  !           'ADT: Starting adaptive time step at t = ', t_cor, 'fs ***'
  !      call nx_log%log(LOG_INFO, msg)
  !      adapt_dt%tswitch = traj%t
  !   end if
  !
  !   ! Backup the current trajectory
  !   ! call traj%copy( backup_traj( adapt_dt%nsubtraj ))
  !
  !   ! Some feedback
  !   write(msg, '(A, I0)') 'ADT: New subtrajectory: #', adapt_dt%nsubtraj
  !   call nx_log%log(LOG_DEBUG, msg)
  !
  !   ! Build the new directory name
  !   write(cur_time, '(F10.3)') t_cor
  !   cur_time = adjustl(cur_time)
  !   write(adt_dir, '(A, I0)') &
  !        & 'adapt_dt_'//trim(cur_time)//'_', adapt_dt%nsubtraj
  !   write(msg, *) ' ADT: New dir: ', trim(adt_dir)
  !   call nx_log%log(LOG_DEBUG, msg)
  !
  !   ! Prepare the virtual trajectory
  !   call nx_log%log(LOG_DEBUG, 'ADT: Creating directory for the adaptive time step')
  !   call get_environment_variable("NXHOME", env)
  !   env = trim(env)//'/utils/'
  !   write(cmd, *) &
  !        & trim(env)//'/adapt_dt_copy.pl in '//&
  !        & trim(conf%progname)//' '//trim(adt_dir)
  !   call call_external(cmd, ierr)
  !   call check_error(ierr, ADT_ERROR_PERL)
  !   ! if (ierr /= 0) STOP "Problem running "//trim(cmd)
  !   write(adt_dir, '(A)') trim(adt_dir)//'/TEMP'
  !
  !   ! Execute auxiliary perl script to clean the current dir
  !   call nx_log%log(LOG_DEBUG, 'ADT: Cleaning working directory')
  !   write(cmd, '(A,A, I0)') trim(env), '/clean_nx.pl ', traj%step
  !   call call_external(cmd, ierr)
  !   call check_error(ierr, ADT_ERROR_PERL)
  !
  !   ! Update trajectory variables with values from previous step
  !   traj%geom = traj%old_geom
  !   traj%veloc = traj%old_veloc
  !   traj%grad = traj%old_grad
  !   traj%acc = traj%old_acc
  !   traj%nad = traj%old_nad
  !   traj%epot(:) = traj%old_epot(1, :)
  !   traj%t = traj%t - traj%dt * timeunit
  !   ! traj%step = traj%step - 1
  !   ! traj%step = 1
  ! end subroutine adt_prepare_virtual_traj
  !
  !
  ! subroutine adt_increase_dt(adapt_dt, traj, conf, work_dir)
  !   !! Increase the time-step if required.
  !   !!
  !   !! This routine is meant to be called at the end of the main
  !   !! loop, if the MO OVL between times \(t\) and \(t+\Delta t\) is
  !   !! well-behaved.  If the current time is still inferior to the
  !   !! time of switch, then we try to divide the current time-step by
  !   !! ``adapt_dt%ratio``.
  !   type(nx_adaptive_ts_t) :: adapt_dt
  !   type(nx_traj_t), intent(inout) :: traj
  !   type(nx_config_t), intent(in) :: conf
  !   character(len=300), intent(in) :: work_dir
  !
  !   character(len=MAX_COMMAND_SIZE) :: env, cmd
  !   character(len=MAX_CMD_SIZE) :: msg
  !
  !   integer :: ierr
  !
  !   real(dp) :: ratio
  !
  !   ratio = adapt_dt%ratio
  !
  !   if (traj%t < adapt_dt%tswitch) then
  !      ! We are still "before" the time where we switched, so we are
  !      ! still in a virtual trajectory.
  !      !
  !      ! We start by copying the relevant files from the
  !      ! subtrajectory directory into the main directory.
  !      call get_environment_variable("NXHOME", env)
  !      env = trim(env)//'/utils/'
  !
  !      call nx_log%log(LOG_DEBUG, 'ADT: Copying files from last adaptive step')
  !      write(cmd, '(A)') &
  !           & trim(env)//'/adapt_dt_copy.pl out '//trim(conf%progname)
  !      call call_external(cmd, ierr)
  !      call check_error(ierr, ADT_ERROR_PERL)
  !
  !      if (traj%t + traj%dt * timeunit / ratio > adapt_dt%tswitch) then
  !         ! In this case, using the adapted time-step results in
  !         ! going too far in time. Thus we need to use as time-step
  !         ! the difference between the current time and the switch.
  !         traj%dt = (adapt_dt%tswitch - traj%t) / timeunit
  !         write(msg, '(A,F8.3)') 'ADT: Increasing time step to ', traj%dt * timeunit
  !         call nx_log%log(LOG_INFO, msg)
  !
  !         adapt_dt%nsubtraj = adapt_dt%nsubtraj - 1
  !         write(msg, '(A,I0)') 'ADT: Moving back to subtrajectory ', adapt_dt%nsubtraj
  !         call nx_log%log(LOG_DEBUG, msg)
  !
  !         ierr =  chdir ( '../../' )
  !         call check_error(ierr, ADT_ERROR_SWITCHING)
  !         ! if ( ierr .ne. 0 ) STOP 'Can not change directory.'
  !      else
  !         ! Here we can proceed to "restore" the time-step
  !         traj%dt = traj%dt / ratio
  !         write(msg, '(A,F8.3)') 'ADT: Increasing time step to ', traj%dt * timeunit
  !         call nx_log%log(LOG_INFO, msg)
  !
  !         adapt_dt%nsubtraj = adapt_dt%nsubtraj - 1
  !         write(msg, '(A,I0)') 'ADT: Moving back to subtrajectory ', adapt_dt%nsubtraj
  !         call nx_log%log(LOG_DEBUG, msg)
  !
  !         ierr =  chdir ( '../../' )
  !         call check_error(ierr, ADT_ERROR_SWITCHING)
  !         ! if ( ierr .ne. 0 ) STOP 'Can not change directory.'
  !      end if
  !   else if (abs(traj%t - adapt_dt%tswitch) < 1E-5) then
  !      ! In this case we can go back to the main trajectory !
  !
  !      call nx_log%log(LOG_INFO, 'ADT: Switching back to the original trajectory')
  !      traj%dt = adapt_dt%dt_init
  !      adapt_dt%nsubtraj = 0
  !      ierr =  chdir ( work_dir )
  !      ! if ( ierr .ne. 0 ) STOP 'Can not change directory.'
  !      call check_error(ierr, ADT_ERROR_SWITCHING)
  !   end if
  !
  ! end subroutine adt_increase_dt



  ! subroutine restore_old_vars(this, traj)
  !   !! Goes back in time by updating the traj variables (geom,
  !   !! grad and epot) with the old values from previous step.
  !   class(nx_adaptive_ts_t), intent(inout) :: this
  !   !! Access variables from the adaptive time step object.
  !   type(nx_traj_t), intent(inout) :: traj
  !   !! Access trajectory variables.
  !   character(len=MAX_COMMAND_SIZE) :: env, cmd
  !   integer :: ierr
  !
  !   if (this%nsubtraj == 0) then
  !      if (traj%t > this%tswitch) then
  !         write(*,*) '**** Switching to adaptive time step at t = ', traj%t, 'fs ****'
  !         this%tswitch = traj%t
  !         ! Save variables state to recover the original time line
  !      else
  !         write(*,*) 'Continuing in the adaptive time step...'
  !      end if
  !   end if
  !
  !   this%nsubtraj = this%nsubtraj + 1
  !   write(*,*) 'Running on subtrajectory ', this%nsubtraj
  !   write (*,*) '  '
  !   if (this%nsubtraj > 4) then
  !      STOP "Too unstable trajectory!"
  !   end if
  !
  !   ! Execute auxiliary perl script to clean the current dir
  !   call get_environment_variable("NX_UTILS", env)
  !   write(*,*) 'Cleaning working directory'
  !   write(cmd, '(A,A, I0)') trim(env), '/clean_nx.pl ', traj%step
  !   call call_external(cmd, ierr)
  !
  !   write(*,*) 'Creating directory for the adaptive time step'
  !   write(cmd, '(A,A,A)') trim(env), '/adapt_dt_copy.pl ', 'in'
  !   call call_external(cmd, ierr)
  !
  !   ! Update trajectory variables with values from previous step
  !   traj%geom = traj%old_geom
  !   traj%veloc = traj%old_veloc
  !   traj%grad = traj%old_grad
  !   traj%acc = traj%old_acc
  !   traj%epot(:) = traj%old_epot(1, :)
  !   traj%t = traj%t - traj%dt
  !
  ! end subroutine restore_old_vars
  !
  !
  ! subroutine reduce_dt(this, traj, check_moovl)
  !
  !   class(nx_adaptive_ts_t) :: this
  !   type(nx_traj_t), intent(inout) :: traj
  !   !! Access trajectory variables.
  !   type(nx_moovlp_check), intent(in) :: check_moovl
  !   !! Access variables related to check_moovl module.
  !
  !   !  Reduce the time step by a user-defined constant or
  !   !  by a pre-computed factor (F-norm)
  !   if (this%adapt_model == 1) then
  !      this%ratio = 1/check_moovl%ndiff
  !   end if
  !
  !   if (traj%dt < 1e-4) then
  !      STOP "Too small time step!"
  !   else
  !      traj%dt = traj%dt * this%ratio
  !      write(*,*) 'Reducing time step to ', traj% dt, 'fs'
  !   end if
  !
  ! end subroutine reduce_dt
  !
  ! subroutine increase_dt(this, traj, work_dir)
  !
  !   class(nx_adaptive_ts_t) :: this
  !   type(nx_traj_t), intent(inout) :: traj
  !   !! Access trajectory variables
  !   character(len=300), intent(in) :: work_dir
  !
  !   character(len=MAX_COMMAND_SIZE) :: env, cmd
  !
  !   integer :: ierr
  !
  !   if (traj%t < this%tswitch) then
  !      ! Call auxiliary perl script
  !      call get_environment_variable("NX_UTILS", env)
  !      write(*,*) 'Copying files from last adaptive step'
  !      write(cmd, '(A,A,A)') trim(env), '/adapt_dt_copy.pl ', 'out'
  !      call call_external(cmd, ierr)
  !
  !      if (traj%t + 2 * traj%dt > this%tswitch) then
  !         traj%dt = this%tswitch - traj%t
  !         write(*,*) ' '
  !         write(*,*) 'Increasing time step to ', traj%dt
  !         this%nsubtraj = this%nsubtraj - 1
  !         write(*,*) 'Moving back to subtrajectory ', this%nsubtraj
  !         write(*,*) ' '
  !         ierr =  chdir ( '../' )
  !         if ( ierr .ne. 0 ) STOP 'Can not change directory.'
  !      else
  !         write(*,*) ' '
  !         write(*,*) 'Increasing time step to ', traj%dt
  !         traj%dt = 2 * traj%dt
  !         this%nsubtraj = this%nsubtraj - 1
  !         write(*,*) 'Moving back to subtrajectory ', this%nsubtraj
  !         write(*,*) ' '
  !         ierr =  chdir ( '../' )
  !         if ( ierr .ne. 0 ) STOP 'Can not change directory.'
  !      end if
  !   else if (traj%t == this%tswitch) then
  !      write(*,*) ' '
  !      write(*,*) 'Switching back to the original trajectory'
  !      write(*,*) ' '
  !      traj%dt = this%dt_init
  !      this%nsubtraj = 0
  !      ierr =  chdir ( work_dir )
  !      if ( ierr .ne. 0 ) STOP 'Can not change directory.'
  !   end if
  !
  ! end subroutine increase_dt

end module mod_adaptive_step
