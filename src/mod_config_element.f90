module mod_config_element
  implicit none

  private

  !! Generic configuration element
  type :: nx_config_ele_abs_t
     character(len=32) :: name
     character(len=:), allocatable :: description
     character(len=:), allocatable :: short_desc
   contains
     procedure, pass :: set_description
     procedure, pass :: set_short_desc
     procedure, pass :: print
  end type nx_config_ele_abs_t

  ! abstract interface
  !    subroutine setter_description(self, desc)
  !      import :: nx_config_ele_abs_t
  !      class(nx_config_ele_abs_t), intent(inout) :: self
  !      character(len=*), intent(in) :: desc
  !    end subroutine setter_description
  ! end interface
  ! 
  ! abstract interface
  !    subroutine setter_short_desc(self, desc)
  !      import :: nx_config_ele_abs_t
  !      class(nx_config_ele_abs_t), intent(inout) :: self
  !      character(len=*), intent(in) :: desc
  !    end subroutine setter_short_desc
  ! end interface
  ! 
  ! abstract interface
  !    subroutine printer(self)
  !      import :: nx_config_ele_abs_t
  !      class(nx_config_ele_abs_t), intent(inout) :: self
  !    end subroutine printer
  ! end interface
  

  
end module mod_config_element
