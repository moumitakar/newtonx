module mod_conical_intersection
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !!
  !! # 2D conical intersection model from Ferretti et al.
  !!
  use mod_constants, only: MAX_STR_SIZE
  use mod_input_parser, only: &
       & parser_t, set => set_config
  use mod_kinds, only: dp
  use mod_logger, only: &
       & nx_log, LOG_DEBUG, LOG_ERROR,&
       & print_conf_ele
  use mod_tools, only: to_str
  use iso_fortran_env, only: stdout => output_unit
  implicit none

  private

  type, public :: nx_conint_t
     real(dp) :: mx = 0.0_dp
     real(dp) :: my = 0.0_dp
     real(dp) :: x = 0.0_dp
     real(dp) :: y = 0.0_dp
     real(dp) :: vx = 0.0_dp
     real(dp) :: vy = 0.0_dp
     real(dp) :: alpha = 3.0_dp
     real(dp) :: beta = 1.5_dp
     real(dp) :: gamma = 0.04_dp
     real(dp) :: delta = 0.01_dp
     real(dp) :: kx = 0.02_dp
     real(dp) :: ky = 0.10_dp
     real(dp) :: x1 = 4.0_dp
     real(dp) :: x2 = 3.0_dp
     real(dp) :: x3 = 3.0_dp
   contains
     procedure :: setup => conint_setup
     procedure :: print => conint_print
     procedure :: run => conint_run
     procedure :: update => conint_update_input
     procedure :: get_h => conint_h
     procedure :: get_dhdx => conint_dhdx
     procedure :: get_dhdy => conint_dhdy
  end type nx_conint_t
  

contains

  subroutine conint_setup(self, parser, masses, geom, veloc)
    !! Setup the 2D conical intersection computation.
    !!
    !! The notation and the process followed in this implementation are as close as
    !! possible from the original implementation of Ferretti et al. (DOI:
    !! [10.1063/1.471791](https://doi.org/10.1063/1.471791) ).
    class(nx_conint_t), intent(inout) :: self
    !! Main QM object.
    type(parser_t), intent(in) :: parser
    !! Input parser.
    real(dp), intent(in) :: masses(:)
    real(dp), intent(in) :: geom(:)
    real(dp), intent(in) :: veloc(:)

    integer :: u
    real(dp) :: dum, ma, mb, r1, r2
    character(len=10) :: aa

    call set(parser, 'con_int', self%alpha, 'alpha')
    call set(parser, 'con_int', self%beta, 'beta')
    call set(parser, 'con_int', self%gamma, 'gamma')
    call set(parser, 'con_int', self%delta, 'delta')
    call set(parser, 'con_int', self%kx, 'kx')
    call set(parser, 'con_int', self%ky, 'ky')
    call set(parser, 'con_int', self%x1, 'x1')
    call set(parser, 'con_int', self%x2, 'x2')
    call set(parser, 'con_int', self%x3, 'x3')

    self%mx = 2*masses(1)
    self%my = self%mx * masses(2) / (self%mx + mb)

    r1 = geom(2) - geom(1)
    r2 = geom(3) - geom(2)

    self%x = (r1 + r2) / 2.0_dp
    self%y = (r1 - r2) / 2.0_dp

    self%vx = (veloc(3) - veloc(1)) / 2.0_dp
    self%vy = (2.0_dp * veloc(2) - veloc(3) - veloc(1)) / 2.0_dp
  end subroutine conint_setup


  subroutine conint_update_input(self, geom, veloc)
    class(nx_conint_t), intent(inout) :: self
    real(dp), intent(in) :: geom(:)
    real(dp), intent(in) :: veloc(:)

    real(dp) :: r1, r2

    r1 = geom(2) - geom(1)
    r2 = geom(3) - geom(2)

    self%x = (r1 + r2) / 2.0_dp
    self%y = (r1 - r2) / 2.0_dp

    self%vx = (veloc(3) - veloc(1)) / 2.0_dp
    self%vy = (2.0_dp * veloc(2) - veloc(3) - veloc(1)) / 2.0_dp
  end subroutine conint_update_input
  


  subroutine conint_print(self, out)
    !! Print the 2D conical intersection configuration.
    !!
    class(nx_conint_t), intent(in) :: self
    !! Nx_Qm object.
    integer, intent(in), optional :: out
    !! Optional: file unit where to print (default: ``STDOUT``).

    integer :: output

    output = stdout
    if (present(out)) output = out

    call print_conf_ele(self%alpha, 'alpha', unit=output)
    call print_conf_ele(self%beta, 'beta', unit=output)
    call print_conf_ele(self%gamma, 'gamma', unit=output)
    call print_conf_ele(self%delta, 'delta', unit=output)
    call print_conf_ele(self%kx, 'kx', unit=output)
    call print_conf_ele(self%ky, 'ky', unit=output)
    call print_conf_ele(self%x1, 'x1', unit=output)
    call print_conf_ele(self%x2, 'x2', unit=output)
    call print_conf_ele(self%x3, 'x3', unit=output)
    call print_conf_ele(self%mx, 'mx', unit=output)
    call print_conf_ele(self%my, 'my', unit=output)
  end subroutine conint_print


  subroutine conint_run(self, epot, grad, nad)
    !! Compute energies, gradients and non-adiabatic couplings.
    !!
    class(nx_conint_t), intent(inout) :: self
    real(dp), intent(inout) :: epot(:)
    real(dp), intent(inout) :: grad(:, :)
    real(dp), intent(inout) :: nad(:)

    real(dp) :: h(2, 2), e(2), hdiag(2, 2), work(2, 2)
    real(dp) :: dhdx(2, 2), dhdy(2, 2)
    real(dp) :: dhdxa(2, 2), dhdxb(2, 2), dhdxc(2, 2), g(2, 2), gg(2, 2)

    integer :: info, i

    ! Build hamiltonian and derivatives in diabatic basis
    h = self%get_h()
    dhdx = self%get_dhdx()
    dhdy = self%get_dhdy()

    dhdxa = - (dhdx + dhdy) / 2.0_dp
    dhdxb = dhdy
    dhdxc = (dhdx - dhdy) / 2.0_dp

    ! Diagonalize to obtain energies and adiabatic basis, stored as hdiag
    hdiag = h
    call dsyev('V', 'L', 2, hdiag, 2, e, work, 5, info)
    if (info /= 0) then
       call nx_log%log(LOG_ERROR, 'Problem in diagonalization: '//to_str(info))
       error stop
    end if

    epot = e

    ! Build the gradients 
    grad = 0.0_dp
    ! first coordinate xa
    gg = matmul(transpose(hdiag), dhdxa)
    g = matmul( gg, hdiag)
    nad(1) = g(1, 2) / (e(1) - e(2))
    grad(1, 1) = g(1, 1)
    grad(2, 1) = g(2, 2)

    ! second coordinate xb
    gg = matmul(transpose(hdiag), dhdxb)
    g = matmul( gg, hdiag)
    nad(2) = g(1, 2) / (e(1) - e(2))
    grad(1, 2) = g(1, 1)
    grad(2, 2) = g(2, 2)

    ! third coordinate xc
    gg = matmul(transpose(hdiag), dhdxc)
    g = matmul( gg, hdiag)
    nad(3) = g(1, 2) / (e(1) - e(2))
    grad(1, 3) = g(1, 1)
    grad(2, 3) = g(2, 2)

    ! Debug print
    block
      character(len=MAX_STR_SIZE) :: msg
      write(msg,'(a,3f15.9)') 'x,y         =',self%x,self%y
      call nx_log%log(LOG_DEBUG, msg)
      write(msg, '(a,3f15.9)') 'h11,h22,h12 =',h(1,1),h(2,2),h(1,2)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'e1,e2       =',e(1),e(2)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'C matrix    =',hdiag(1,1),hdiag(2,1)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') '             ',hdiag(1,2),hdiag(2,2)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'dhdx        =',dhdx(1,1),dhdx(2,1)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') '             ',dhdx(1,2),dhdx(2,2)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'dhdy        =',dhdy(1,1),dhdy(2,1)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') '             ',dhdy(1,2),dhdy(2,2)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'dhdxa       =',dhdxa(1,1),dhdxa(2,1)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') '             ',dhdxa(1,2),dhdxa(2,2)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'dhdxb       =',dhdxb(1,1),dhdxb(2,1)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') '             ',dhdxb(1,2),dhdxb(2,2)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'dhdxc       =',dhdxc(1,1),dhdxc(2,1)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') '             ',dhdxc(1,2),dhdxc(2,2)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'grad(1)     =',(grad(1, i), i=1, 3)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'grad(2)     =',(grad(2, i), i=1, 3)
      call nx_log%log(LOG_DEBUG, msg)
      write(msg,'(a,3f15.9)') 'nad         =',(nad(i), i=1, 3)
      call nx_log%log(LOG_DEBUG, msg)
    end block
  end subroutine conint_run


  function conint_h(self) result(res)
    class(nx_conint_t), intent(in) :: self

    real(dp) :: res(2, 2)

    real(dp) :: eexp

    eexp = exp(-self%alpha * (self%x-self%x3)**2 - self%beta * self%y**2)
    res(1, 1) = (self%kx * (self%x-self%x1)**2 + self%ky * self%y**2) / 2.0_dp
    res(2, 2) = (self%kx * (self%x-self%x2)**2 + self%ky * self%y**2) / 2.0_dp + self%delta
    res(1, 2) = self%gamma * self%y * eexp
    res(2, 1) = res(1, 2)
  end function conint_h


  function conint_dhdx(self) result(res)
    class(nx_conint_t), intent(in) :: self

    real(dp) :: res(2, 2)

    res(1, 1) = self%kx * (self%x-self%x1)
    res(2, 2) = self%kx * (self%x-self%x2)
    res(1, 2) = -2.0_dp * self%gamma * self%alpha * (self%x-self%x3) &
         &      * self%y * exp( -self%beta * self%y**2 - self%alpha * (self%x-self%x3)**2)
    res(2, 1) = res(1, 2)
  end function conint_dhdx


  function conint_dhdy(self) result(res)
    class(nx_conint_t), intent(in) :: self

    real(dp) :: res(2, 2)

    res(1, 1) = self%ky * self%y
    res(2, 2) = self%ky * self%y
    res(1, 2) = (self%gamma - 2.0_dp*self%gamma*self%beta*self%y**2) &
         &      * exp( -self%beta * self%y**2 - self%alpha * (self%x-self%x3)**2)
    res(2, 1) = res(1, 2)
  end function conint_dhdy

end module mod_conical_intersection
  
