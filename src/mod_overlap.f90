! Copyright (C) 2022  Light and Molecules Group

! This program is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.

! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.

! You should have received a copy of the GNU General Public License
! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module mod_overlap
  !! author: Baptiste Demoulin <baptiste.demoulin@univ-amu.fr>
  !! date: 2020-04-21
  !!
  !! This module handles the computation of time-derivative
  !! couplings, that is to say the overlap between times \(t -
  !! dt\) and \(t\). It is used for running the ``cioverlap`` code
  !! for methods that do not have analytical non-adiabatic couplings,
  !! but also more generally to give diagnostics about the accuracy
  !! of the current time-step.
  !!
  !! Therefore it also serve as the basis for the implementation of
  !! the adaptive time-step scheme.
  use mod_async, only: run_async
  use mod_kinds, only: dp
  use mod_constants, only: MAX_CMD_SIZE
  use mod_qm_t, only: nx_qm_t
  use mod_logger, only: &
       & nx_log, &
       & LOG_DEBUG, LOG_INFO, LOG_WARN, &
       & call_external, check_error
  use mod_configuration, only: nx_config_t
  use mod_trajectory, only: nx_traj_t
  use mod_gaussian, only: &
       & gau_extract_overlap, gau_get_lcao, &
       & gau_init_double_molecule, gau_prepare_double_molecule
  use mod_turbomole, only: &
       & tm_extract_overlap, tm_get_lcao, &
       & tm_init_double_molecule, tm_get_orbspace, tm_prepare_double_molecule
  use mod_columbus, only: &
       & col_extract_overlap, col_get_lcao, &
       & col_init_double_molecule, col_prepare_double_molecule
  use mod_orca, only: &
       & orca_get_lcao, orca_extract_overlap
  use mod_interface, only: &
       & mkdir, rm, copy
  use mod_tools, only: to_str
  implicit none

  private

  public :: ovl_prepare_run
  public :: ovl_run
  public :: read_mo_overlap
  public :: evaluate_metrics
  public :: ovl_extract_ao_ovl_and_lcao
  public :: ovl_compute_mo_ovl
  public :: nx_moovlp_t

  integer, parameter :: MAX_COMMAND_SIZE = 512


  type nx_moovlp_t
     real(dp) :: cos_sim
     real(dp) :: norm_sim
     logical :: ill_conditioned = .false.
  end type nx_moovlp_t

  ! Termination codes
  integer, parameter :: OVL_ERROR_PREPARE = 51
  integer, parameter :: OVL_ERROR_RUN = 52
  integer, parameter :: OVL_GAU_RWFDUMP = 53


contains


  subroutine ovl_prepare_run(qmcode)
    !! Prepare the overlapping molecule run.
    !!
    !! This is a wrapper routine to call the corresponding Perl
    !! script, that will handle the creation of the corresponding
    !! inputs in another folder.
    character(len=*), intent(in) :: qmcode
    !! Electronic structure code to use.

    character(len=MAX_COMMAND_SIZE) :: env

    ! Generate inputs for double molecule run
    select case(qmcode)
    case ("turbomole")
       call tm_init_double_molecule()
    case("tinker_g16mmp")
       call gau_init_double_molecule()
    case ("gaussian")
       call gau_init_double_molecule()
    case("columbus")
       call col_init_double_molecule()
    case("orca")
       ! call orca_init_double_molecule()
    end select
  end subroutine ovl_prepare_run


  subroutine ovl_run(traj, qm)
    !! Run the computation on the overlapping molecule.
    type(nx_traj_t), intent(in) :: traj
    !! Trajectory object.
    type(nx_qm_t), intent(inout) :: qm
    !! QM program.

    integer :: ierr
    character(len=MAX_CMD_SIZE) :: env

    logical :: ext

    ! First, do not enter for the following options
    select case(qm%qmcode)
    case(&
         & "sbh", "recohmodel", "onedim_model", &
         & "tinker_mndo","exc_mopac","mopac"&
         &)
       call nx_log%log(LOG_INFO, 'OVL: Skipping double molecule')
       return
    end select

    ! Create the directory structure
    inquire(file='overlap.old/', exist=ext)
    if (ext) then
       inquire(file='overlap/', exist=ext)
       if (ext) then
          ierr = rm(['overlap.old/'])
          call check_error(ierr, OVL_ERROR_PREPARE, 'OVL: Cannot delete overlap.old/ directory')
       end if
    end if

    inquire(file='overlap', exist=ext)
    if (ext) then
       ierr = rename('overlap/', 'overlap.old/')
       call check_error(ierr, OVL_ERROR_PREPARE, 'OVL: Cannot rename overlap directory')
    end if

    ierr = mkdir('overlap')
    call check_error(ierr, OVL_ERROR_PREPARE, 'OVL: Cannot create overlap directory')

    ! Write the coordinates
    call traj% write_merged_coordinates( qm%qmcode )

    ! Prepare the different files, and set the command to be executed according to each
    ! qm code.
    select case(qm%qmcode)
    case("turbomole")
       call tm_prepare_double_molecule()
    case("gaussian", "tinker_g16mmp")
       call gau_prepare_double_molecule()
    case("columbus")
       call col_prepare_double_molecule()
    case("orca")
       ! call orca_prepare_double_molecule()
       return
    case("exc_gaussian")
       block
         character(len=256) :: gaussian_jobs(qm%nchromexc)
         integer :: i

         do i=1, qm%nchromexc
            gaussian_jobs(i) = 'exc_gau'//to_str(i)//'_ovl.com'
         end do

         ierr = copy(gaussian_jobs, 'overlap/')
         call check_error(ierr, OVL_ERROR_PREPARE, 'EXC GAU OVL: Cannot copy input files')
       end block
    end select

    ! And start the computation !
    call nx_log%log(LOG_DEBUG, 'Start overlap <t | t-dt > computation')
    ierr = chdir('overlap')
    call check_error(ierr, OVL_ERROR_PREPARE, 'OVL: Cannot change directory')

    if (size(qm%ovl_async) > 1) then
       call run_async(qm%ovl_async, qm%qmproc, ierr)

    else
       if (qm%ovl_out /= '') then
          call call_external(qm%ovl_cmd, ierr, outfile=qm%ovl_out)
       else
          call call_external(qm%ovl_cmd, ierr)
       end if
    end if
    call check_error(ierr, OVL_ERROR_PREPARE, 'OVL: Problem in execution of QM code')

    ierr = chdir('../')
    call check_error(ierr, OVL_ERROR_PREPARE, 'OVL: Cannot change directory')

    ! Some post-treatment for particular cases
    select case(qm%qmcode)
    case("gaussian", "tinker_g16mmp")
       call execute_command_line(qm%ovl_post, exitstat=ierr)
       call check_error(ierr, OVL_GAU_RWFDUMP)
    case("exc_gaussian")
       block
         character(len=:), allocatable :: name_qm, namefile_ovl, namefile_smat, command
         integer :: i

         call get_environment_variable("g16root", env)
         do i = 1, qm%nchromexc
            name_qm = "exc_gau"
            namefile_ovl = name_qm//to_str(i)//'_ovl.rwf'
            namefile_smat = 'S_matrix'//to_str(i)
            command = qm%ovl_post//' overlap/'//namefile_ovl//&
                 &' overlap/'//namefile_smat//' 514R'
            call execute_command_line(command, exitstat=ierr)
            call check_error(ierr, OVL_GAU_RWFDUMP)
         end do
       end block
    end select

    ! Report back
    call nx_log%log(LOG_DEBUG, 'End overlap computation')
  end subroutine ovl_run


  subroutine ovl_extract_ao_ovl_and_lcao(&
       & progname, lcao, lcao_old, aoovl, readsifs_path &
       & )
    !! Extract the information required to compute the MO overlap.
    !!
    !! The information extracted are the following:
    !!
    !! - ``lcao``: AO to MO matrix at time ``t`` (in AO to MO order) ;
    !! - ``lcao_old``: AO to MO matrix at time ``t - dt`` (in AO to MO order) ;
    !! - ``aoovl``: Orbital overlap in the AO basis.
    !!
    !! Columbus outputs the orbital overlap in sifs format. If using
    !! Columbus, it is (for now) necessary to have an external
    !! program dumping the content of the binary file to a text file.
    !! This program is ``readsifs`` and is available with the
    !! ``cioverlap`` by Jiri Pittner.
    character(len=*), intent(in) :: progname
    !! QM program used.
    real(dp), allocatable, intent(inout) :: lcao(:, :)
    !! AO to MO matrix at time ``t`` in AO to MO order.
    real(dp), allocatable, intent(inout) :: lcao_old(:, :)
    !! AO to MO matrix at time ``t-dt`` in AO to MO order.
    real(dp), allocatable, intent(inout) :: aoovl(:)
    !! Orbital overlap between ``t`` and ``t-dt`` in AO basis (the
    !! symmetric matrix is contracted as a vector).
    character(len=*), intent(in), optional :: readsifs_path
    !! Path to ``readsifs`` program.

    integer :: nao, dim_ovl

    nao = size(lcao, 1)
    dim_ovl = size(aoovl)

    ! Extract AO overlap and MO coefficients
    select case(progname)
    case ("turbomole")
       call tm_extract_overlap('./overlap/dscf.out', dim_ovl, aoovl)
       call tm_get_lcao('./mos', nao, lcao)
       call tm_get_lcao('./mos.old', nao, lcao_old)
    case("gaussian", "tinker_g16mmp", 'exc_gaussian')
       call gau_extract_overlap('./overlap/S_matrix', aoovl)
       call gau_get_lcao('./MO_coefs_a', nao, lcao)
       call gau_get_lcao('./MO_coefs_a.old', nao, lcao_old)
    case("columbus")
       call col_extract_overlap('./overlap/aoints1S', dim_ovl, readsifs_path, aoovl)
       call col_get_lcao('MOCOEFS/mocoef_mc.sp', nao, lcao)
       call col_get_lcao('mocoef.old', nao, lcao_old)
    case("orca")
       call orca_extract_overlap('orca.ovl_double.dat', nao, aoovl)
       call orca_get_lcao('orca.ovl.dat.old', nao, lcao_old)
       call orca_get_lcao('orca.ovl.dat', nao, lcao)
    end select
  end subroutine ovl_extract_ao_ovl_and_lcao


  subroutine ovl_compute_mo_ovl(qm, Smo)
    !! Compute the orbital overlap in the MO basis.
    !!
    !! The routine extracts the \((t, d-dt)\) part of the overlap
    !! matrix in the AO basis. This matrix is symmetric. We want to
    !! convert it to the MO basis, with the lines corresponding to
    !! \(t-dt\) and the columns to \(t\). The effective
    !! transformation is the following, where \((\mu, \nu)\) denote
    !! AO basis, \((i, j)\) denote MO basis, and a tilde over the
    !! symbol denotes time \(t-dt\):
    !!
    !! \[ A_{\tilde{i}j} = U_{\tilde{\mu}\tilde{i}}^{T}
    !! A_{\tilde{\mu}\tilde{\nu}} U_{\tilde{\nu}j}  \]
    !!
    !! where \(A\) is the overlap matrix, and \(U\) is the AO to MO
    !! transformation matrix.
    !!
    !! This routine is consistent with the ``cioverlap`` code, that
    !! performs the same transformation at the beginning of its
    !! exection.
    type(nx_qm_t), intent(in) :: qm
    real(dp), allocatable, intent(inout) :: Smo(:, :)

    real(dp), allocatable :: Sraw(:, :), Sao(:, :)
    real(dp), allocatable :: lcao(:, :)
    real(dp), allocatable :: lcao_old(:, :)
    real(dp), allocatable :: aoovl(:)
    integer :: nao, dim, dim_ovl, i, j, id
    character(len=MAX_CMD_SIZE) :: readsifs_path
    character(len=MAX_CMD_SIZE) :: env

    nao = qm%orb%nbas
    dim = nao*2
    dim_ovl = (dim**2 + dim) / 2

    allocate( aoovl(dim_ovl) )
    allocate( lcao(nao, nao) )
    allocate( lcao_old(nao, nao) )
    if (.not. allocated(Smo)) then
       allocate(Smo(nao, nao))
    end if
    Smo(:, :) = 0.0_dp

    select case(qm%qmcode)
    case("columbus")
       call get_environment_variable(name='CIOVERLAP', value=env)
       readsifs_path = trim(env)//'/' ! Not very general, but it will do for now...
       call ovl_extract_ao_ovl_and_lcao(qm%qmcode, lcao, lcao_old, aoovl, readsifs_path)
    case DEFAULT
       call ovl_extract_ao_ovl_and_lcao(qm%qmcode, lcao, lcao_old, aoovl)
    end select

    ! Form the symmetric matrix Sraw from aoovl
    allocate( Sraw(nao*2, nao*2) )
    id = 1
    do i=1, nao*2
       do j=1, i
          Sraw(i, j) = aoovl(id)
          Sraw(j, i) = aoovl(id)
          id = id+1
       end do
    end do

    ! Extract the relevant part (corresponding to the overlap between
    ! t (first nao lines) and t-dt (last nao columns) of Sraw in Sao
    allocate( Sao(nao, nao) )
    do i=1, nao
       do j=1, nao
          Sao(i, j) = Sraw(i, j+nao)
       end do
    end do
    deallocate(Sraw)

    ! Sao has lines corresponding to t and columns to t-dt
    allocate( Sraw(nao, nao) )
    Sraw(:, :) = 0.0_dp
    call dgemm('n', 'n', nao, nao, nao, 1.0_dp, Sao, nao,&
         & lcao, nao, 0.0_dp, Sraw, nao)
    call dgemm('t', 'n', nao, nao, nao, 1.0_dp, lcao_old, nao, Sraw,&
         & nao, 0.0_dp, Smo, nao)
  end subroutine ovl_compute_mo_ovl


  subroutine read_mo_overlap(cio_output, Smo)
    !! Read the MO overlap matrix from ``cioverlap``.
    !!
    !! The information about MO overlap is stored in ``Smo``.
    character(len=*), intent(in) :: cio_output
    !! Name of the output to read.
    real(dp), dimension(:, :), allocatable, intent(out) :: Smo

    integer :: u, ierr, id, i, j
    character(len=512) :: line
    integer :: dim

    open(newunit=u, file=cio_output, status='old', action='read')
    do
       read(u, '(A)', iostat=ierr) line
       if (ierr /= 0) exit

       id = index(line, ' Smo[0]')
       if (id /= 0) then
          read(u, *, iostat=ierr) dim
          if (.not. (allocated(Smo))) then
             allocate( Smo(dim,dim) )
          end if
          do i=1, dim
             read(u, *) (Smo(i,j), j=1, dim)
          end do
       end if
    end do
    close(u)

  end subroutine read_mo_overlap


  subroutine evaluate_metrics(config, Smo, moovlp)
    !! Evaluate the cosine and F-norm similarity on the given matrix.
    !!
    !! This routine is a wrapper around the functions
    !! ``cosine_similarity`` and ``norm_diff``. It prepares their
    !! run, and reports the results.
    class(nx_config_t), intent (in) :: config
    !! Main configuration.
    real(dp), dimension(:,:), intent (in) :: Smo
    !! MO overlap matrix.
    type(nx_moovlp_t), intent(out) :: moovlp
    !! Stores information about the current MO OVL.

    real(dp), dimension(:), allocatable   :: ovlp_diag
    ! Stores the diagonal of the ovl matrix
    integer :: dim
    ! Number of rows in the MO overlap matrix

    integer :: i
    character(len=1024) :: msg
    logical :: forceprt

    dim = size(Smo, 1)
    allocate (ovlp_diag(1:dim))

    ! Extract the diagonal of MO OVL matrix
    do i=1, dim
       ovlp_diag(i) = Smo(i, i)
    end do

    moovlp%ill_conditioned = .false.

    moovlp%cos_sim = cosine_similarity(ovlp_diag)
    moovlp%norm_sim = norm_diff(Smo)

    write(msg, '(A,A,F9.5,A,F9.5)')&
         & 'Checking integrity of MO overlap matrix'&
         & //NEW_LINE('a'), &
         & '  Cosine similarity (deviation from identity):', moovlp%cos_sim, &
         & NEW_LINE('a')//'  F-norm (deviation from identity):', moovlp%norm_sim
    call nx_log%log(LOG_DEBUG, msg)
    ! write (msg, "(A,F9.5)") &
    !      & 'Cosine similarity (deviation from identity): ', moovlp%cos_sim
    ! call nx_log%log(LOG_INFO, msg)
    !
    ! write (msg, "(A,F9.5)") &
    !      & 'F-norm difference (deviation from identity): ', moovlp%norm_sim
    ! call nx_log%log(LOG_INFO, msg)

    ! msg = '**** Checking integrity of MO overlap matrix ****'//NEW_LINE('a')
    ! res = message(msg)
    !
    ! msg = '|----------- Cosine similarity -----------|'//NEW_LINE('a')
    ! msg = trim(msg)//' MO overlap is deviating from Identity by:'
    ! res = message(msg)
    ! write(msg, "(F9.5)") moovlp%cos_sim
    ! msg = trim(msg)//NEW_LINE('a')
    ! res = message(msg)
    !
    ! msg = '|----------- F-norm difference -----------|'//NEW_LINE('a')
    ! msg = trim(msg)//' MO overlap is deviating from Identity by:'
    ! res = message(msg)
    ! write(msg, "(F9.5)") moovlp%norm_sim
    ! msg = trim(msg)//NEW_LINE('a')
    ! res = message(msg)

    forceprt = .false.
    if ( (moovlp%cos_sim <= config%thrs_cos) .or. &
         & (moovlp%norm_sim >= config%thrs_norm) ) then
       ! forceprt = .true.
       write(msg, '(A,A,F9.5,A,F9.5)')&
            & 'Ill-conditioning of the MO overlap matrix detected!'&
            & //NEW_LINE('a')//NEW_LINE('a'), &
            & '   Deviation of cosine norm:', moovlp%cos_sim, &
            & NEW_LINE('a')//'   Deviation of F-norm:', moovlp%norm_sim

       call nx_log%log(LOG_WARN, msg)
       ! write(*, *) "   *******************************************"
       ! write(*, *) "   WARNING: Ill-conditioning of the MO overlap"
       ! write(*, *) "            matrix detected!"
       ! write(*, *) ''
       ! write(*, '(a30, F9.5)') '   Deviation of cosine norm:', moovlp%cos_sim
       ! write(*, '(a30, F9.5)') '   Deviation of F-norm:', moovlp%norm_sim
       ! write(*, *) "   *******************************************"

       moovlp%ill_conditioned = .true.
    end if



    deallocate (ovlp_diag)

  end subroutine evaluate_metrics


  ! subroutine extract_diagonal(m, diag, n)
  !
  !   real(dp), intent (in), dimension(:, :) :: m
  !   real(dp), intent (out), dimension(:) :: diag
  !   integer, intent (in) :: n
  !   real(dp), dimension (1:size(m,1)*size(m,1)) :: temp
  !
  !   temp = pack(m, .true.)
  !   diag = temp(1:n*n:n+1)
  !
  ! end subroutine extract_diagonal


  pure function cosine_similarity(Sdiag) result(cos_sim)
    !! This metric accounts only for distortions in the
    !! diagonal elements of the MO overlap. The maximum
    !! value for cos_sim is 1.0.
    real(dp), dimension(:), intent(in) :: Sdiag
    !! Diagonal of the MO OVL matrix.
    real(dp) :: cos_sim
    !! Resulting cosine similarity

    integer :: len
    real(dp), allocatable, dimension(:) :: Idiag

    len = size(Sdiag)
    allocate (Idiag(1:len))
    Idiag(1:len) = 1 ! Diagonal of Identity matrix
    cos_sim = dot_product(Idiag, abs(Sdiag)) / (norm2(Idiag) * norm2(Sdiag))

    deallocate (Idiag)

  end function cosine_similarity


  pure function norm_diff(Smatrix) result(n)
    !! This metric takes into account the whole MO overlap matrix,
    !! including the off-diagonal element.
    real(dp), dimension(:,:), intent(in) :: Smatrix
    !! Diagonal of the MO OVL matrix.
    real(dp) :: n
    !! Output Frobenius norm for Mdiff.

    integer :: i,j
    integer, dimension(2) :: dim
    real(dp) :: tmp
    real(dp), allocatable, dimension(:,:) :: Imatrix, Mdiff, P

    dim = shape(Smatrix)
    allocate (Imatrix( dim(1), dim(2) ))
    ForAll(i = 1:dim(1), j = 1:dim(2)) Imatrix(i,j) = (i/j)*(j/i)

    allocate (P( dim(1), dim(2) ))
    allocate (Mdiff( dim(1), dim(2) ))
    Mdiff = Imatrix - abs(Smatrix)

    P = matmul(Mdiff, transpose(Mdiff))
    ! Loop to calculate the Frobenius norm using the trace of matrix P
    tmp = 0.0
    do i=1,dim(1)
       tmp = tmp + P(i,i)
    enddo

    n = sqrt(tmp)

  end function norm_diff


end module mod_overlap
