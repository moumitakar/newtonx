# Newton-X Changelog

## v3.5.0 (2023-03-10)

### Added

- `nx_geninp`: new input generator that replaces `utils/nxinp`.

### Deprecated

- `nxinp` should not be used anymore, and will be removed in an upcoming release.

## v3.4.1 (2023-02-28)

### Fixed

- Bug with `nx_restart` when using HDF5 outputs.

- Proper printing of version number.

## v3.4.0

### Added

- Interface with ORCA package (tested with version 5.0.3).

### Fixed

- `nxinp` bug with the `run_IS` parameter for generating trajectories has been fixed.

- When a trajectory terminates due to the energy gap being too small with the ground state the status
  is properly updated in the HDF5 output.

- `nxinp` bug where the script cannot find the file `default.yml` if installing Newton-X with `make install`
   has been corrected.
   
- `save_cwd` from `user_config.nml` is now correctly taken into account.  Backing up all points 
  from trajectory (`save_cwd = -1`) is defaault only for `lvprt > 4`.

## v3.3.0 (2023-01-26)

### Changed

- The production of output is now independant from the `lvprt` parameter.

- A new section of the input, `&nx_output`, allows to control which output will be produced (see documentation).

### Added

- Interface with [MLatom](http://mlatom.com/).

- Interface with Turbomole 7.6 (**WARNING**: the testuite still has results from 7.3, so will fail
  when used with 7.6 !)

- Add command-line option to clean the working directory (`nx_moldyn --clean`).

### Fixed

- `configure` now reports an error if no BLAS or Lapack library can be found.

- The testsuite is now independant of the phase (non-adiabatic couplings or single amplitudes) computed
  by the QM code.


## v3.2.0 (2022-12-07)

### Changed

- Refactorization of the analytical models.

### Added

- CS-FSSH method ([doi:10.1039/d0sc04197a](https://doi.org/10.1039/d0sc04197a)): available with analytical
  models and Columbus.

- New parameter: `nxconfig.epot_diff` to kill a trajectory when the potential energy difference between 
  the ground state and any excited state becomes inferior to this parameter.  This is default for all
  single reference methods (TD-DFT, ADC2, CC2).
  
### Fixed

- Bug in `mocoef` handling, when `columbus.mocoef = 1`: the program does not complain any more at the first
  step because no `mocoef.old` exist.
  
## v3.1.0

### Changed

- **Breaking changes**: 
  
  - The `$NXHOME/utils/moldyn` script is replaced by the program `nx_moldyn`.
  - The `$NXHOME/utils/nxrestart` script is replaced by the program `nx_restart`.
  - The `$NXHOME/utils/nx_tests` script is replaced by the program `nx_test`.

- **Breaking change**: For using local diabatization and ZPE correction the options `with_locdiab` and `with_zpe` have
  to be specified in the `nxconfig` section of the input file.  Setting `integrator` from `sh` to
  2 for local diabatization, or having a section `zpe_correct` for ZPE correction will be done
  automatically.
  
- **Breaking change**: the `prog` parameter does not exist anymore, it's replaced by the combination of 
  `methodname` and `progname` (see `mod_implemented.f90` for more details).
  
- More streamlined output production: the code has been refactored, and should now be more clear.

- The required directories (`TEMP`, `RESULTS`, ...) are now created as late as possible in the 
  initialization phase.
  
- If `cioverlap` is needed and the `CIOVERLAP` environment is not found, the trajectory will
  be killed before running any QM job.

### Added

- New Fortran-based input parser, based on the same syntax as before.

- All programs now have a command line interface, with help messages provided by the `-h` or
  `--help` flags.
  
- The computation now dies when the combination `progname` and `methodname` does not exist.

### Fixed

- Backup is now (properly) only performed every `save_cwd` step, and not at every step.

- The MOPAC interface now correctly communicates the time to Mopac in fs, not in a.u.

- The printing of hopping information is now correct, even when only 1 step (`ms = 1`) is performed
  during the SH routine.
  
- The dynamics doesn't crash anymore when `use_txt_output = .false.` with the HDF5 outputs, and a
  surface hopping is found (required text files are now properly initialized).
  
- The phase correction is no longer performed with analytical methods.
  
- Various memory leaks corrected.

### Removed

- Perl scripts `moldyn`, `nxrestart` and `nx-tests` are now obsolete, and are replaced by the
  programs `nx_moldyn`, `nx_restart` and `nx_test` respectively.

### Deprecated

- `nxinp` will soon be replaced by another solution.

## v3.0.3

### Fixed

- Gaussian input with `mocoef = 1` and `td_str > 0` are now properly generated.

- Phase correction is no longer applied for analytical models.

- Bug with `make install` has been corrected.

- Various printing bugs.

### Changed

- The `data`, `examples` and `perllib` folders can now be found under `$NXHOME/share/newtonx/`, and
  not `$NXHOME` anymore.

## v3.0.2

### Fixed

- Bug on `make install`: some scripts were still present in the list of files that 
  should be installed.
  
- Test handling: now the environment is properly exported for the different tests when
  using `make check`.

## v3.0.1

- Change in velocity rescale algorithm: the `sh%adjmom` parameter meaning has changed,
  and the `sh%adjtheta` parameter has been added. `sh%adjmom` now defines the
  direction of the velocity rescale (either in the momentum
  direction (0 or 1), or in the `(h, g)` plane (2)).  It also specifies if the
  kinetic energy should be scaled by the number of degrees of
  freedom (0) or not (1) when rescaling in the momentum direction.
  When set to 2, the rescaling is done in the `(h, g)` plane, and
  the kinetic energy considered in the condition is the one in this
  direction only.  In this last case, the angle in the plane is now given by the 
  parameter `sh%adjtheta`.
  
- Bug correction in `nxinp`: now the script shouldn't warn about unexisting configuration
  parameters, and produce correct inputs for all computations.
  
- Bug correction in the testsuite: now `make check` correctly reports the results of them
  tests.
  
