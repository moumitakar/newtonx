Title: Newton-X
project: Newton-X
summary: Newton-X package for mixed quantum-classical dynamics
author: Light and Molecules group
author_description: Group of Pr. Mario Barbatti (ICR, Aix-Marseille Université, France)
src_dir: ./src/
exclude_dir: ./doc/ ./build/
output_dir: ./doc/
page_dir: ./manual/documentation/
project_github: https://gitlab.com/light-and-molecules/newtonx
project_website: https://newtonx.org
github: https://gitlab.com/light-and-molecules/
email: baptiste.demoulin@univ-amu.fr
fpp_extensions: fpp
predocmark: >
media_dir: ./doc/media
docmark_alt: #
predocmark_alt: <
display: public
         protected
         private
source: false
graph: false
search: false
macro: TEST
       LOGIC=.true.
license: gfdl
exclude: mod_initializer.f90
exclude: mod_stopsign.f90
exclude: version.f90.in

{!./README.md!}
{!./CONTRIBUTING.md!}





