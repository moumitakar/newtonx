      subroutine ciovlps()
      use genvar
      use ovlps
      implicit none
      real(8)::delta !function
      allocate(dell(ntot,ntot),cdell(ntot,ntot))
      dell=0.d0;cdell=0.d0
      do j=1,ntot
       do i=1,ntot
        ii=i; jj=j
        dell(i,j) = delta(ii,jj)
        cdell(i,j) = 1.d0-dell(i,j)
       end do
      end do

      allocate(ovlpci(0:nroot,0:nroot));ovlpci=0.d0
      allocate(onlyci(nroot,nroot));onlyci=0.d0
      call odscheme()
      write(ounit,*)'Ovlp of excited states <Psi(t)|Psi(t-dt)>'
      ii=ounit
      call print_mat(nroot,nroot,onlyci,ii)
      ovlpci(1:nroot,1:nroot)=transpose(onlyci(1:nroot,1:nroot))
      write(ounit,*)'ovlp between GS and ES is set to zero here.'
      ovlpci(0,1:nroot)=0.d0; ovlpci(1:nroot,0)=0.d0
      !GS-GS
      ovlpci(0,0)=1.d0 !we dont care about gs for now

      write(ounit,*)'ci_overlap matrix before multiplying by overall &
      phase from prev geo (including GS)' 
      write(ounit,*)nroot+1,nroot+1
      do i=0,nroot
       !write(ounit,'(f20.12,$)')(ovlpci(j,i),j=0,nroot)
       !write(ounit,*)
       write(ounit,'(100f20.12)')(ovlpci(j,i),j=0,nroot)
      end do
      !=====================================================================
      !adjust for overall ci phase wrt prev geo-- this is from final
      !overlap itself and we still need this.
      call nadphase_jiri()
      !====================================================================
      call timestamp(ounit)

      write(ounit,*)'CI overlap matrix ' !what NX needs in the end
      write(ounit,*)nroot+1,nroot+1
      do i=0,nroot
       !write(ounit,'(f20.12,$)')(ovlpci(j,i),j=0,nroot)
       !write(ounit,*)
       write(ounit,'(100f20.12)')(ovlpci(j,i),j=0,nroot)
      end do
      end subroutine ciovlps

      subroutine nadphase_jiri()
      use genvar
      use ovlps
      implicit  none
      real(8)::phases(0:nroot),maxoverlap
      integer::maxoverlapindex
      !pittner's cc code algorithm
      open(unit=ounit2,file='phases',status='unknown')
      if(istep>1) then
      write(ounit,*)'Old phases:'
      open(unit=20,file=trim(oldphases),status='old')
      read(20,*)(phases(j),j=0,nroot)
      close(20)
      write(ounit,*)(phases(j),j=0,nroot)
      do j=0,nroot
       do i=0,nroot
        ovlpci(j,i) = ovlpci(j,i)*phases(j)
       end do
      end do
      end if
      !write current phases
      write(ounit,*)'Current phases'
      do i=0,nroot
       maxoverlap=-1.d0
       maxoverlapindex=-1
       do j=0,nroot
        if(dabs(ovlpci(i,j))>maxoverlap) then
         maxoverlapindex=j
         maxoverlap=dabs(ovlpci(i,j))
        end if
       end do
       if(ovlpci(i,i)<0.d0) then
        phases(i)=-1.d0
        do j=0,nroot
         ovlpci(j,i)=ovlpci(j,i)*(-1.d0)
        end do
       else
        phases(i)=1.d0
       end if
      end do
      write(ounit2,*)(phases(j),j=0,nroot)
      write(ounit,*)(phases(j),j=0,nroot)
      end subroutine nadphase_jiri

      subroutine odscheme()
      use genvar
      use ovlps
      implicit none
      real(8)::cinovket(nocc,(nocc+1):ntot),cinovbra(nocc,(nocc+1):ntot)
      real(8)::cicoefoo(nocc,nocc)
      real(8)::cicoefvv((nocc+1):ntot,(nocc+1):ntot)
      real(8)::tmpcioo(nocc,nvirt),tmpcivv(nocc,nvirt),tmpciket(nocc,nvirt)
      real(8)::tmpcioot(nvirt,nocc),tmpcikett(nvirt,nocc)
      call system_clock(begg,rate)
      onlyci=0.d0
      do ii=1,nroot
       cinovket=0.d0; 
       k=1
       do i=ncore+1,nocc
        do j=nocc+1,ntot
         cinovket(i,j)=cicurr(ii,k);k=k+1
        end do
       end do
       do jj=1,nroot
        cinovbra=0.d0
        k=1
        do i=ncore+1,nocc
         do j=nocc+1,ntot
          cinovbra(i,j)=ciold(jj,k);k=k+1
         end do
        end do
        tmpcioo=0.d0; tmpcivv=0.d0; tmpciket=0.d0
        cicoefoo=0.d0;cicoefvv=0.d0; 
        l=0
        do j=nocc+1,ntot
         l=l+1;
         do i=1,nocc
          tmpcioo(i,l)=cinovket(i,j)
          tmpciket(i,l)=cinovbra(i,j)
         end do
        end do
        l=0; 
        do j=nocc+1,ntot
         l=l+1;
         do i=1,nocc
          tmpcivv(i,l)=cinovket(i,j)
         end do
        end do
        tmpcioot=transpose(tmpcioo); tmpcikett=transpose(tmpciket)
        !call dgemm('N','N',nvirt,nvirt,nocc,&
        !alpha,transpose(tmpcioo),nvirt,tmpciket,nocc,beta,cicoefvv,nvirt)
        !call dgemm('N','N',nocc,nocc,nvirt,&
        !alpha,tmpcivv,nocc,transpose(tmpciket),nvirt,beta,cicoefoo,nocc)
        call dgemm('N','N',nvirt,nvirt,nocc,&
        alpha,tmpcioot,nvirt,tmpciket,nocc,beta,cicoefvv,nvirt)
        call dgemm('N','N',nocc,nocc,nvirt,&
        alpha,tmpcivv,nocc,tmpcikett,nvirt,beta,cicoefoo,nocc)
        !apply Eqn.(13).
        do i=nocc+1,ntot
         do j=nocc+1,ntot
          onlyci(ii,jj) = onlyci(ii,jj)+ (cdell(i,j)*cicoefvv(i,j)*geo12mo(i,j))
         end do
        end do
        do i=1,nocc
         do j=1,nocc
          onlyci(ii,jj) = onlyci(ii,jj) - (cdell(i,j)*cicoefoo(i,j)*geo12mo(j,i))
         end do
        end do
        do i=1,nocc
         do j=nocc+1,ntot
          det = geo12mo(i,i)*geo12mo(j,j)
          onlyci(ii,jj) = onlyci(ii,jj) + (cinovket(i,j)*cinovbra(i,j)*det)
         end do
        end do

       end do!jj
      end do!ii
      call system_clock(endd, rate)
      time=real(endd-begg)/real(rate)
      call print_time(time,'OV ovlps - bdiff odscheme')
      end subroutine odscheme

      subroutine cimatch()
      !The dotproduct of the CIcoefs of each root has to be
      !phase-matched. dot_product means sum of product of the 'same'
      !determinant-- ie over one set of OV pairs only. So O_nov 
      !is unique only when its diagonal--ie when its diagonal
      !elements are 1,-1. Here, we save time by forcing 
      !it to be diagonal by using a sufficiently small time step.
      use genvar
      use ovlps
      implicit none
 101  format(I4,A4,I4,A12,I4)
 102  format(I4,A4,I4,A12,I4,F10.4)

      write(ounit,*)'Inside CImatch============================='
      allocate(cinorm(nroot,nroot));cinorm=0.d0
      !bare ci ovlp
      do i=1,nroot
       do j=1,nroot
        cinorm(i,j)=dot_product(cicurr_raw(i,:),ciold(j,:))
       end do
      end do
      write(ounit,*)'CI coefficient overlap bra-ket -- raw';ii=ounit
      call print_mat(nroot,nroot,cinorm,ii)

      cicurr=0.d0
      write(ounit,*)"Matchci = ",matchci
      if(matchci)then
      if(.not.allocated(ipiv))allocate(ipiv(nocc))
      if(.not.allocated(tmpmo))allocate(tmpmo(nocc,nocc))
      if(.not.allocated(onov))allocate(onov(nov,nov));onov=0.d0
      if(.not.allocated(tmpket))allocate(tmpket(nov,1));tmpket=0.d0
      write(ounit,*)'O_nov mat diagonal'

      do i=1,nroot
       write(ounit,*)'Root:',i,'=============================='
       write(ounit,*)'Original CI vector '
       do ii=1,nov
        write(ounit,'(f6.2,$)')cicurr_raw(i,ii)
       end do
       write(ounit,*)
       do ii=ncore+1,nocc
        do jj=nocc+1,ntot
         inov = maporbtonov(ii,jj)
         onov(inov,inov)=1.d0
         !using mkl routine==============================
         ipiv=0;info=0;det=0.d0
         tmpmo=geo12mo_raw(1:nocc,1:nocc)
         tmpmo(ii,1:nocc)=geo12mo_raw(jj,1:nocc)
         tmpmo(1:nocc,ii)=geo12mo_raw(1:nocc,jj)
         tmpmo(ii,ii)=geo12mo_raw(jj,jj)
         call dgetrf(nocc,nocc,tmpmo,nocc,ipiv,info)
         if(info/=0) then
          write(ounit,*)'Problem with factorisation. Stop in &
          novovlps_exact.'
          write(ounit,*)'The current CI Omat element is:'
          write(ounit,101)ii,' -> ',jj,' in state ',i
          stop 101
         end if
         det=1.d0
         do kk=1,nocc
          if(ipiv(kk)/=kk) then
           det = -det*tmpmo(kk,kk)
          else
           det = det*tmpmo(kk,kk)
          end if
         end do
         !=============================================
         onov(inov,inov)=float(nint(det))
         write(ounit,'(I6,$)')int(onov(inov,inov))
         !if(int(dabs(onov(inov,inov)))/=1) then
         ! write(ounit,*)'Large change in CI coefficients from &
         ! geometry at t-dt to t. Try smaller dt. Exit now.'
         ! write(ounit,*)'The current CI Omat element is:'
         ! write(ounit,102)ii,' -> ',jj,' in state ',i,onov(inov,inov)
         ! stop 102
         !end if
        end do
       end do
       write(ounit,*)
       tmpket=0.d0;
       write(ounit,*)'Flipped CI coefficients'
       do ii=1,nov
        tmpket(ii,1)=onov(ii,ii)*cicurr_raw(i,ii)
        cicoefcurr(i,low(ii),up(ii))=tmpket(ii,1)
        if(onov(ii,ii)<0.d0) &
        write(ounit,'(I3,A4,I3,2F6.2)')low(ii),' -> ',up(ii),cicurr_raw(i,ii),tmpket(ii,1)
       end do
!      write(ounit,*)'Phase-adjusted CI vector'
!      do ii=1,nov
!       write(ounit,'(f6.2,$)')tmpket(ii,1)
!      end do
       write(ounit,*)
       !cicurr(i,:) = tmpket(:,1)
       kk=1
       do ii=ncore+1,nocc
        do jj=nocc+1,ntot
         cicurr(i,kk)=tmpket(kk,1);kk=kk+1
        end do
       end do
      end do !root index
      deallocate(ipiv,tmpmo,onov,tmpket)

      else
       cicurr=0.d0
       do i=1,nroot
        do ii=1,nov
         cicurr(i,ii) = cicurr_raw(i,ii)
        end do
       end do
      end if

      write(ounit,*)
      cinorm=0.d0
      do i=1,nroot
       do j=1,nroot
        cinorm(i,j)=dot_product(cicurr(i,:),ciold(j,:))
       end do
      end do
      write(ounit,*)'CI coefficient overlap bra-ket -- &
      (adjusted or not)'
      ii=ounit
      call print_mat(nroot,nroot,cinorm,ii)
      write(ounit,*)'Finished CImatch=============================='
      end subroutine cimatch

      subroutine mosmatch()
      use genvar
      use ovlps
      implicit none
      real(8),allocatable::tmpmat(:,:)
      write(ounit,*)'In MOsmatch========================='
      write(ounit,*)'Calculating first set of MO ovlps'
      call moovlp(mocurr_raw)
      allocate(omat(ntot,ntot));omat=0.d0
      do j=1,ntot
       do i=1,ntot
        omat(i,j) = float(nint(geo12mo(i,j)))
       end do
      end do
      write(ounit,*)'O matrix'
      ii=ounit
      call print_mat(ntot,ntot,omat,ii)
      allocate(mocurr(ntot,ntot));mocurr=0.d0 !matched MOs
      if(.not.allocated(geo12mo_raw))allocate(geo12mo_raw(ntot,ntot));
      geo12mo_raw=geo12mo !needed for cimatch
      if(matchmos) then
       call dgemm('N','N',ntot,ntot,ntot,&
       alpha,omat,ntot,mocurr_raw,ntot,beta,mocurr,ntot)
       write(ounit,*)'Diff betn old and new MO matrix'
       ii=ounit
       allocate(tmpmat(ntot,ntot));tmpmat=mocurr-mocurr_raw
       call print_mat(ntot,ntot,tmpmat,ii)
       deallocate(tmpmat)
       write(ounit,*)'New MO ovlp - geo12mo array is updated'
       call moovlp(mocurr)
      else
       mocurr=mocurr_raw
       write(ounit,*)'Old geo12mo matrix retained.'
       ii=ounit
       call print_mat(ntot,ntot,geo12mo,ii)
      end if
      write(ounit,*)'Finished mosmatch====================='

      end subroutine mosmatch

      subroutine moovlp(mosin)
      use genvar
      use ovlps
      implicit none
      real(8),intent(inout)::mosin(ntot,ntot)
      real(8),allocatable::tempp1(:,:),tempp2(:,:),tempp3(:,:),tempp4(:,:)
      if(.not.allocated(ovlpmo)) then
      allocate(ovlpmo(ntot2,ntot2));
      end if
      ovlpmo=0.d0 !this is in MO basis

      allocate(tempp1(ntot,ntot));tempp1=0.d0!first index in MO,2nd is AO
      allocate(tempp2(ntot,ntot));tempp2=0.d0!first index in MO,2nd is AO
      allocate(tempp3(ntot,ntot));tempp3=0.d0!first index in MO,2nd is AO
      allocate(tempp4(ntot,ntot));tempp4=0.d0!first index in MO,2nd is AO
      call system_clock(begg,rate)

      do j=1,ntot !MO
       do k=1,ntot !AO,sum over AO
        do l=1,ntot !AO,sum over AO
         tempp1(j,k) = tempp1(j,k) + (mosin(j,l)*ovlpao(k,l))
         tempp2(j,k) = tempp2(j,k) + (moold(j,l)*ovlpao(k+ntot,l+ntot))
         tempp3(j,l) = tempp3(j,l) + (moold(j,k)*ovlpao(l,k+ntot))
         tempp4(j,k) = tempp4(j,k) + (mosin(j,l)*ovlpao(k+ntot,l))
        end do
       end do
      end do

      do i=1,ntot !MO
       do j=1,i !MO
        do k=1,ntot !sum over AO
         ovlpmo(i,j) = ovlpmo(i,j) + (mosin(i,k)*tempp1(j,k))
         ovlpmo(i+ntot,j+ntot)=ovlpmo(i+ntot,j+ntot)+&
         (moold(i,k)*tempp2(j,k))
         ovlpmo(i,j+ntot)=ovlpmo(i,j+ntot)+(mosin(i,k)*tempp3(j,k))
         if(i/=j) then
         ovlpmo(i+ntot,j)=ovlpmo(i+ntot,j)+(moold(i,k)*tempp4(j,k))
         end if
        end do
        ovlpmo(j,i)=ovlpmo(i,j)
        ovlpmo(j+ntot,i+ntot)=ovlpmo(i+ntot,j+ntot)
        ovlpmo(j+ntot,i)=ovlpmo(i,j+ntot)
        ovlpmo(j,i+ntot)=ovlpmo(i+ntot,j)
       end do
      end do
      deallocate(tempp1,tempp2,tempp3,tempp4)
      call system_clock(endd,rate)
      time=real(endd-begg)/real(rate)
      call print_time(time,'2Nx2N MO ovlps--raw')
      !write(munit,*)'MO ovlp'
      !call print_mat(ntot2,ntot2,ovlpmo,munit)

      !store MO ovlp between geos1,2 separately..this is what we need.
      if(.not.allocated(geo12mo))allocate(geo12mo(ntot,ntot));
      geo12mo=0.d0
      do i=1,ntot
       do j=1,ntot
        geo12mo(i,j) = ovlpmo(i,j+ntot)
       end do
      end do
      write(ounit,*)'MO ovlp between current and old geometries'
      ii=ounit
      call print_mat(ntot,ntot,geo12mo,ii)
      end subroutine moovlp

      subroutine gaussin_nx()
      use genvar
      use ovlps
      implicit none
      integer::nj! some integer read in that I dont care about
      integer::nrows,ncols
      real(8),allocatable::tmpci(:,:)
      write(ounit,*)
      write(ounit,*)'=======Using NX generated inputs========'
      write(ounit,*)
      open(unit=10,file=trim(faoin),status='old')
      open(unit=11,file=trim(cicurr_rawf),status='old')
      open(unit=12,file=trim(cioldf),status='old')
      open(unit=13,file=trim(nstat),status='old')
      open(unit=14,file=trim(tstep),status='old')

      write(ounit,*)'Reading from file ',trim(tstep);
      read(14,*)istep,dt
      write(ounit,*)'Current time step:',istep
      write(ounit,*)'Nuclear time step:',dt,' fs';
      write(ounit,*)'Time:',istep*dt

      write(ounit,*)'Reading from file ',trim(faoin);write(ounit,*)
      read(10,*)ntot,nj,nj,ne !nj is ndiscard..I am not using this anywhere.
      read(10,*)ntot2
      write(ounit,*)'No.of AOs:',ntot
      allocate(ovlpao(ntot2,ntot2));ovlpao=0.d0
      do i=1,ntot2
       read(10,*)(ovlpao(i,j),j=1,ntot2)
      end do
      !left upper block is current geometry sao,bottom right is old geo,
      !bottom left therefore has oldgeo AOs along rows and newgeo AOs
      !along columns. top right has oldgeo AOs along columns and
      !newgeo AOs along rows. The top right offdiag block is geo12ao.
      allocate(geo12ao(ntot,ntot));geo12ao=0.d0
      do i=1,ntot
       do j=1,ntot
        geo12ao(i,j) = ovlpao(i,j+ntot)
       end do
      end do
      write(ounit,*)'test Sao'
      nj=ounit
      call print_mat(ntot,ntot,geo12ao,nj)
      read(10,*) !mocoef of current and old geometries
      allocate(mocurr_raw(ntot,ntot),moold(ntot,ntot));mocurr_raw=0.d0;moold=0.d0
      do i=1,ntot
       read(10,*)(mocurr_raw(i,j),j=1,ntot)
      end do
      read(10,*)
      do i=1,ntot
       read(10,*)(moold(i,j),j=1,ntot)
      end do
      mocurr_raw=transpose(mocurr_raw); moold=transpose(moold)

      write(ounit,*)'Reading from file ',trim(nstat);
      read(13,*)ncore,nroot !this includes the gs in NX.
      nroot=nroot-1
      write(ounit,*)'ncore :',ncore
      write(ounit,*)'No.of excited states :',nroot
      write(ounit,*)

      nocc=(ne/2); nvirt=ntot-(ne/2);nov=(nocc-(ncore+1)+1)*(ntot-(nocc+1)+1)
      allocate(low(nov),up(nov),cicurr_raw(nroot,nocc*nvirt),ciold(nroot,nocc*nvirt));
      allocate(cicurr(nroot,nocc*nvirt));cicurr=0.d0
      allocate(cicoefcurr(nroot,ntot,ntot),cicoefold(nroot,ntot,ntot));
      allocate(maporbtonov(ntot,ntot));maporbtonov=0
      low=0;up=0;cicurr_raw=0.d0;ciold=0.d0; cicoefcurr=0.d0;cicoefold=0.d0
      !assign mos to nov
      kk=1
      do ii=ncore+1,nocc
       do jj=nocc+1,ntot
        low(kk)=ii; up(kk)=jj;
        maporbtonov(ii,jj)=kk
        kk=kk+1
       end do
      end do

      write(ounit,*)'Reading from files ',trim(cicurr_rawf),' and &
      ',trim(cioldf);
      write(ounit,*)'Note: Assuming closed shell!'
      write(ounit,*)
      do i=1,nroot
       if(allocated(tmpci))deallocate(tmpci)
       read(11,*)nrows,ncols
       allocate(tmpci(nrows,ncols));tmpci=0.d0
       do ii=1,nrows
        read(11,*)(tmpci(ii,jj),jj=1,ncols)
       end do
       kk=1
       do ii=1,nrows
        do jj=1,ncols
         cicurr_raw(i,kk)=tmpci(ii,jj);
         !cicoefcurr(i,low(kk),up(kk))=tmpci(ii,jj);
         kk=kk+1
        end do
       end do

       if(allocated(tmpci))deallocate(tmpci)
       read(12,*)nrows,ncols
       allocate(tmpci(nrows,ncols));tmpci=0.d0
       do ii=1,nrows
        read(12,*)(tmpci(ii,jj),jj=1,ncols)
       end do
       kk=1
       do ii=1,nrows
        do jj=1,ncols
         ciold(i,kk)=tmpci(ii,jj);
         cicoefold(i,low(kk),up(kk))=tmpci(ii,jj);
         kk=kk+1
        end do
       end do
      end do !i loop

      do i=10,14
       close(i)
      end do
      end subroutine gaussin_nx

      subroutine print_mat(m,n,mat,nj)
      use genvar,only:ao_fmt
      implicit none
      integer,intent(inout)::m,n,nj
      real(8),intent(inout)::mat(m,n)
      character*20::fmtstr
      integer::i,j
!     ao_fmt='(F14.8,$)'
      ao_fmt='(F10.4,$)'
      fmtstr=trim(ao_fmt)
      do i=1,m
       do j=1,n
        write(nj,fmtstr)mat(i,j)
       end do
       write(nj,*)
      end do
      return
      end subroutine print_mat

      subroutine print_time(time,what)
      use genvar,only:ounit
      implicit none
      character(*),intent(in)::what
      real(8),intent(in)::time
      if(time<60.d0) then
       write(ounit,100)"Time taken for ",trim(what),":",time,' seconds'
      elseif(time>60.d0) then
       write(ounit,100)'Time taken for ',trim(what),':',&
       time/60.d0,' minutes'
      elseif(time>3600.d0) then
       write(ounit,100)'Time taken for ',trim(what),':',&
       time/3600.d0,' hours'
      end if
 100  format(1X,3(A),F11.5,(A))
      end subroutine print_time

      real(8) function delta(m,n)
      implicit none
      integer,intent(inout)::m,n
      if(m==n) delta=1.d0
      if(m/=n) delta=0.d0
      end function delta

      subroutine timestamp(unitt)
!*****************************************************************************80
!! TIMESTAMP prints the current YMDHMS date as a time stamp.
!  Example:
!    31 May 2001   9:45:54.872 AM
!  Parameters:  None
        implicit none
        character ( len = 8  ) ampm
        integer ( kind = 4 ) d
        integer ( kind = 4 ) h
        integer ( kind = 4 ) m
        integer ( kind = 4 ) mm
        character ( len = 9  ), parameter, dimension(12) :: month = (/ &
        'January  ', 'February ', 'March    ', 'April    ', &
        'May      ', 'June     ', 'July     ', 'August   ', &
        'September', 'October  ', 'November ', 'December ' /)
        integer ( kind = 4 ) n
        integer ( kind = 4 ) s
        integer ( kind = 4 ) values(8)
        integer ( kind = 4 ) y
        integer :: unitt
        call date_and_time ( values = values )
        y = values(1); m = values(2); d = values(3); h = values(5);
        n = values(6); s = values(7); mm = values(8)
        if ( h < 12 ) then
          ampm = 'AM'
        else if ( h == 12 ) then
          if ( n == 0 .and. s == 0 ) then
            ampm = 'Noon'
          else
            ampm = 'PM'
          end if
        else
          h = h - 12
          if ( h < 12 ) then
            ampm = 'PM'
          else if ( h == 12 ) then
          if ( n == 0 .and. s == 0 ) then
            ampm = 'Midnight'
          else
            ampm = 'AM'
          end if
          end if
        end if
        !write ( *, '(i2,1x,a,1x,i4,2x,i2,a1,i2.2,a1,i2.2,a1,i3.3,1x,a)' ) &
        !    d, trim ( month(m) ), y, h, ':', n, ':', s, '.', mm, trim ( ampm )
        write (unitt, '(1x,i2,1x,a,1x,i4,2x,i2,a1,i2.2,a1,i2.2,1x,a)' ) &
            d, trim ( month(m) ), y, h, ':', n, ':', s, trim ( ampm )
      return
      end subroutine timestamp
