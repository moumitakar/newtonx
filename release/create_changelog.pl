#!/bin/perl -w

use strict;
use warnings;

my $changelog = $ARGV[0];
my $version = $ARGV[1];

my $res = "# Newton-X $version release notes\n\n";

my $is_section = 0;
open(my $fh, '<:encoding(UTF-8)', $changelog);
while(<$fh>) {
    my $line = $_;
    chomp $line;
    if ($line =~ m/^##\s+/) {
	my $test = strip_md_title($line);
	if ($test =~ $version) {
	    $is_section = 1;
	} else {
	    $is_section = 0;
	}

	$line = <$fh>;
    }

    if ($is_section) {
	if ($line =~ m/^###\s/) {
	    $line = strip_md_title($line);
	    $res = $res."## $line\n\n";
	}
	else {
	    $res = $res."$line\n";
	}
    }
}

print STDOUT $res;


sub strip_md_title {
    my ($line) = @_;

    my $res = $line;
    $res =~ s/^#+\s+//;
    return $res;
}
